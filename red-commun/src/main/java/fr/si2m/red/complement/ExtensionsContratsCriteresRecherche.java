package fr.si2m.red.complement;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.util.MultiValueMap;

import fr.si2m.red.RedacUnexpectedException;
import fr.si2m.red.core.repository.CriteresRecherche;
import fr.si2m.red.core.repository.IgnorePourQueryString;
import lombok.Getter;
import lombok.Setter;

/**
 * Critères de recherche de contrats.
 * 
 * @author nortaina
 *
 */
public class ExtensionsContratsCriteresRecherche extends CriteresRecherche {
    /**
     * UID de version.
     */
    private static final long serialVersionUID = -2050758235846669540L;

    /**
     * Indicateur de tri spécifique.
     */
    public static final String TRI_SPECIFIQUE = "spec";

    /**
     * Le numéro de contrat recherché.
     * 
     * @param numContrat
     *            le numéro de contrat recherché
     * @return le numéro de contrat recherché
     */
    @Getter
    @Setter
    private String numContrat;

    /**
     * Les groupes de gestion recherchés.
     * 
     */
    private String[] groupesGestion;

    /**
     * Le numéro de client recherché.
     * 
     * @param numClient
     *            le numéro de client recherché
     * @return le numéro de client recherché
     */
    @Getter
    @Setter
    private String numClient;

    /**
     * L'égibilité DSN recherchée.
     * 
     */
    private String[] eligibiliteContratDsn;

    /**
     * Les familles de contrat recherchées.
     * 
     */
    private String[] familles;

    /**
     * La raison sociale recherchée.
     * 
     * @param raisonSociale
     *            la raison sociale recherchée
     * @return la raison sociale recherchée
     */
    @Getter
    @Setter
    private String raisonSociale;

    /**
     * L'indicateur d'exploitation recherché.
     * 
     */
    private String[] indicExploitation;

    /**
     * Les modes de calcul de cotisation recherchés (nature).
     * 
     */
    private String[] modesCalculCotisation;

    /**
     * Le numéro de SIREN recherché.
     * 
     * @param siren
     *            le numéro de SIREN recherché
     * @return le numéro de SIREN recherché
     */
    @Getter
    @Setter
    private String siren;

    /**
     * L'indicateur d'édition des consignes de paiement recherché.
     * 
     */
    private String[] indicConsignePaiement;

    /**
     * Les modes de nature du contrat recherchée (gestionDirecte).
     * 
     */
    private String[] modeNatureContrat;

    /**
     * Le NIC recherché.
     * 
     * @param nic
     *            le NIC recherché
     * @return le NIC recherché
     */
    @Getter
    @Setter
    private String nic;

    /**
     * L'indicateur de transfert recherché.
     * 
     */
    private String[] indicTransfert;

    /**
     * Le compte d'encaissement recherché.
     * 
     * @param cptEnc
     *            le compte d'encaissement recherché
     * @return le compte d'encaissement recherché
     */
    @Getter
    @Setter
    private String cptEnc;

    /**
     * L'affiliation de l'entreprise recherchée.
     * 
     */
    private Boolean[] entrepriseAffiliee;

    /**
     * La présence de flag VIP recherchée.
     * 
     */
    private String[] vip;

    /**
     * Le filtre sur numéros de contrats VIP si {@link #getVip()} spécifie une recherche sur contrat VIP.
     * 
     */
    @Getter
    @Setter
    @IgnorePourQueryString
    private List<String> filtreContratsVIP = new ArrayList<>();

    /**
     * Le numéro du compte producteur du contrat recherché.
     * 
     * @param cptProd
     *            le numéro du compte producteur du contrat recherché
     * @return le numéro du compte producteur du contrat recherché
     */
    @Getter
    @Setter
    private String cptProd;

    /**
     * Constructeur par défaut des critères.
     */
    public ExtensionsContratsCriteresRecherche() {
        super();
    }

    /**
     * Constructeur de critères par copie de critères donnés.
     * 
     * @param extensionsContratsCriteresRecherche
     *            les critères à copier
     */
    public ExtensionsContratsCriteresRecherche(ExtensionsContratsCriteresRecherche extensionsContratsCriteresRecherche) {
        this();
        try {
            BeanUtils.copyProperties(this, extensionsContratsCriteresRecherche);
        } catch (IllegalAccessException | InvocationTargetException e) {
            throw new RedacUnexpectedException("Erreur lors de la copie de critères de recherche", e);
        }
    }

    /**
     * Les groupes de gestion recherchés.
     * 
     * @return les groupes de gestion recherchés
     */
    public String[] getGroupesGestion() {
        if (groupesGestion == null) {
            return new String[0];
        } else {
            return Arrays.copyOf(groupesGestion, groupesGestion.length);
        }
    }

    /**
     * Les groupes de gestion recherchés sous forme de liste.
     * 
     * @return les groupes de gestion recherchés
     */
    public List<String> getGroupesGestionAsList() {
        if (groupesGestion == null) {
            return new ArrayList<String>();
        } else {
            return Arrays.asList(this.groupesGestion);
        }
    }

    /**
     * Les groupes de gestion recherchés.
     * 
     * @param groupesGestion
     *            les groupes de gestion recherchés
     */
    public void setGroupesGestion(String[] groupesGestion) {
        if (groupesGestion == null) {
            this.groupesGestion = null;
        } else {
            this.groupesGestion = Arrays.copyOf(groupesGestion, groupesGestion.length);
        }
    }

    /**
     * L'égibilité DSN recherchée.
     * 
     * @return l'égibilité DSN recherchée
     */
    public String[] getEligibiliteContratDsn() {
        if (eligibiliteContratDsn == null) {
            return new String[0];
        } else {
            return Arrays.copyOf(eligibiliteContratDsn, eligibiliteContratDsn.length);
        }
    }

    /**
     * L'égibilité DSN recherchée sous forme de liste.
     * 
     * @return l'égibilité DSN recherchée
     */
    public List<String> getEligibiliteContratDsnAsList() {
        if (eligibiliteContratDsn == null) {
            return new ArrayList<String>();
        } else {
            return Arrays.asList(this.eligibiliteContratDsn);
        }
    }

    /**
     * L'égibilité DSN recherchée.
     * 
     * @param eligibiliteContratDsn
     *            l'égibilité DSN recherchée
     */
    public void setEligibiliteContratDsn(String[] eligibiliteContratDsn) {
        if (eligibiliteContratDsn == null) {
            this.eligibiliteContratDsn = null;
        } else {
            this.eligibiliteContratDsn = Arrays.copyOf(eligibiliteContratDsn, eligibiliteContratDsn.length);
        }
    }

    /**
     * Les familles de contrat recherchées.
     * 
     * @return les familles de contrat recherchées
     */
    public String[] getFamilles() {
        if (familles == null) {
            return new String[0];
        } else {
            return Arrays.copyOf(familles, familles.length);
        }
    }

    /**
     * Les familles de contrat recherchées sous forme de liste.
     * 
     * @return les familles de contrat recherchées
     */
    public List<String> getFamillesAsList() {
        if (familles == null) {
            return new ArrayList<>();
        } else {
            return Arrays.asList(this.familles);
        }
    }

    /**
     * Les familles de contrat recherchées.
     * 
     * @param familles
     *            les familles de contrat recherchées
     */
    public void setFamilles(String[] familles) {
        if (familles == null) {
            this.familles = null;
        } else {
            this.familles = Arrays.copyOf(familles, familles.length);
        }
    }

    /**
     * L'indicateur d'exploitation recherché.
     * 
     * @return l'indicateur d'exploitation recherché
     */
    public String[] getIndicExploitation() {
        if (indicExploitation == null) {
            return new String[0];
        } else {
            return Arrays.copyOf(indicExploitation, indicExploitation.length);
        }
    }

    /**
     * L'indicateur d'exploitation recherché sous forme de liste.
     * 
     * @return l'indicateur d'exploitation recherché
     */
    public List<String> getIndicExploitationAsList() {
        if (indicExploitation == null) {
            return new ArrayList<>();
        } else {
            return Arrays.asList(this.indicExploitation);
        }
    }

    /**
     * L'indicateur d'exploitation recherché.
     * 
     * @param indicExploitation
     *            l'indicateur d'exploitation recherché
     */
    public void setIndicExploitation(String[] indicExploitation) {
        if (indicExploitation == null) {
            this.indicExploitation = null;
        } else {
            this.indicExploitation = Arrays.copyOf(indicExploitation, indicExploitation.length);
        }
    }

    /**
     * Les modes de calcul de cotisation recherchés (nature).
     * 
     * @return les modes de calcul de cotisation recherchés
     */
    public String[] getModesCalculCotisation() {
        if (modesCalculCotisation == null) {
            return new String[0];
        } else {
            return Arrays.copyOf(modesCalculCotisation, modesCalculCotisation.length);
        }
    }

    /**
     * Les modes de calcul de cotisation recherchés (nature) sous forme de liste.
     * 
     * @return les modes de calcul de cotisation recherchés
     */
    public List<String> getModesCalculCotisationAsList() {
        if (modesCalculCotisation == null) {
            return new ArrayList<>();
        } else {
            return Arrays.asList(this.modesCalculCotisation);
        }
    }

    /**
     * Les modes de calcul de cotisation recherchés (nature).
     * 
     * @param modesCalculCotisation
     *            les modes de calcul de cotisation recherchés
     */
    public void setModesCalculCotisation(String[] modesCalculCotisation) {
        if (modesCalculCotisation == null) {
            this.modesCalculCotisation = null;
        } else {
            this.modesCalculCotisation = Arrays.copyOf(modesCalculCotisation, modesCalculCotisation.length);
        }
    }

    /**
     * L'indicateur d'édition des consignes de paiement recherché.
     * 
     * @return l'indicateur d'édition des consignes de paiement recherché
     */
    public String[] getIndicConsignePaiement() {
        if (indicConsignePaiement == null) {
            return new String[0];
        } else {
            return Arrays.copyOf(indicConsignePaiement, indicConsignePaiement.length);
        }
    }

    /**
     * L'indicateur d'édition des consignes de paiement recherché sous forme de liste.
     * 
     * @return l'indicateur d'édition des consignes de paiement recherché
     */
    public List<String> getIndicConsignePaiementAsList() {
        if (indicConsignePaiement == null) {
            return new ArrayList<>();
        } else {
            return Arrays.asList(this.indicConsignePaiement);
        }
    }

    /**
     * L'indicateur d'édition des consignes de paiement recherché.
     * 
     * @param indicConsignePaiement
     *            l'indicateur d'édition des consignes de paiement recherché
     */
    public void setIndicConsignePaiement(String[] indicConsignePaiement) {
        if (indicConsignePaiement == null) {
            this.indicConsignePaiement = null;
        } else {
            this.indicConsignePaiement = Arrays.copyOf(indicConsignePaiement, indicConsignePaiement.length);
        }
    }

    /**
     * La nature du contrat recherchée (gestionDirecte).
     * 
     * @return la nature du contrat recherchée
     */
    public String[] getModeNatureContrat() {
        if (modeNatureContrat == null) {
            return new String[0];
        } else {
            return Arrays.copyOf(modeNatureContrat, modeNatureContrat.length);
        }
    }

    /**
     * La nature du contrat recherchée (gestionDirecte) sous forme de liste.
     * 
     * @return la nature du contrat recherchée
     */
    public List<String> getModeNatureContratAsList() {
        if (modeNatureContrat == null) {
            return new ArrayList<String>();
        } else {
            return Arrays.asList(this.modeNatureContrat);
        }
    }

    /**
     * La nature du contrat recherchée (gestionDirecte).
     * 
     * @param modeNatureContrat
     *            la nature du contrat recherchée
     */
    public void setModeNatureContrat(String[] modeNatureContrat) {
        if (modeNatureContrat == null) {
            this.modeNatureContrat = null;
        } else {
            this.modeNatureContrat = Arrays.copyOf(modeNatureContrat, modeNatureContrat.length);
        }
    }

    /**
     * L'indicateur de transfert recherché.
     * 
     * @return l'indicateur de transfert recherché
     */
    public String[] getIndicTransfert() {
        if (indicTransfert == null) {
            return new String[0];
        } else {
            return Arrays.copyOf(indicTransfert, indicTransfert.length);
        }
    }

    /**
     * L'indicateur de transfert recherché sous forme de liste.
     * 
     * @return l'indicateur de transfert recherché
     */
    public List<String> getIndicTransfertAsList() {
        if (indicTransfert == null) {
            return new ArrayList<>();
        } else {
            return Arrays.asList(this.indicTransfert);
        }
    }

    /**
     * L'indicateur de transfert recherché.
     * 
     * @param indicTransfert
     *            l'indicateur de transfert recherché
     */
    public void setIndicTransfert(String[] indicTransfert) {
        if (indicTransfert == null) {
            this.indicTransfert = null;
        } else {
            this.indicTransfert = Arrays.copyOf(indicTransfert, indicTransfert.length);
        }
    }

    /**
     * L'affiliation de l'entreprise recherchée.
     * 
     * @return l'affiliation de l'entreprise recherchée.
     */
    public Boolean[] getEntrepriseAffiliee() {
        if (entrepriseAffiliee == null) {
            return new Boolean[0];
        } else {
            return Arrays.copyOf(entrepriseAffiliee, entrepriseAffiliee.length);
        }
    }

    /**
     * L'affiliation de l'entreprise recherchée sous forme de liste.
     * 
     * @return l'affiliation de l'entreprise recherchée.
     */
    public List<Boolean> getEntrepriseAffilieeAsList() {
        if (entrepriseAffiliee == null) {
            return new ArrayList<Boolean>();
        } else {
            return Arrays.asList(this.entrepriseAffiliee);
        }
    }

    /**
     * L'affiliation de l'entreprise recherchée.
     * 
     * @param entrepriseAffiliee
     *            l'affiliation de l'entreprise recherchée.
     */
    public void setEntrepriseAffiliee(Boolean[] entrepriseAffiliee) {
        if (entrepriseAffiliee == null) {
            this.entrepriseAffiliee = null;
        } else {
            this.entrepriseAffiliee = Arrays.copyOf(entrepriseAffiliee, entrepriseAffiliee.length);
        }
    }

    /**
     * La présence du flag VIP recherchée.
     * 
     * @return la présence du flag VIP recherchée
     */
    public String[] getVip() {
        if (vip == null) {
            return new String[0];
        } else {
            return Arrays.copyOf(vip, vip.length);
        }
    }

    /**
     * La présence du flag VIP recherchée sous forme de liste.
     * 
     * @return la présence du flag VIP recherchée
     */
    public List<String> getVipAsList() {
        if (vip == null) {
            return new ArrayList<String>();
        } else {
            return Arrays.asList(this.vip);
        }
    }

    /**
     * La présence du flag VIP recherchée.
     * 
     * @param vip
     *            la présence du flag VIP recherchée
     */
    public void setVip(String[] vip) {
        if (vip == null) {
            this.vip = null;
        } else {
            this.vip = Arrays.copyOf(vip, vip.length);
        }
    }

    /**
     * Convertit une map de valeurs multiple en critères de recherche de contrats.
     * 
     * @param criteres
     *            les critères sous forme de map de valeurs multiples
     * @return les critères de recherche de contrats extraits
     */
    public static ExtensionsContratsCriteresRecherche from(MultiValueMap<String, Object> criteres) {
        ExtensionsContratsCriteresRecherche criteresRecherche = new ExtensionsContratsCriteresRecherche();
        try {
            BeanUtils.populate(criteresRecherche, criteres);
        } catch (IllegalAccessException | InvocationTargetException e) {
            throw new RedacUnexpectedException("Erreur lors du mapping de critères de recherche", e);
        }

        // Nettoyage des listes
        if (criteresRecherche.getGroupesGestionAsList().contains(StringUtils.EMPTY)) {
            criteresRecherche.setGroupesGestion(null);
        }
        if (criteresRecherche.getEligibiliteContratDsnAsList().contains(StringUtils.EMPTY)) {
            criteresRecherche.setEligibiliteContratDsn(null);
        }
        if (criteresRecherche.getFamillesAsList().contains(StringUtils.EMPTY)) {
            criteresRecherche.setFamilles(null);
        }
        if (criteresRecherche.getModesCalculCotisationAsList().contains(StringUtils.EMPTY)) {
            criteresRecherche.setModesCalculCotisation(null);
        }
        if (criteresRecherche.getModeNatureContratAsList().contains(StringUtils.EMPTY)) {
            criteresRecherche.setModeNatureContrat(null);
        }
        if (criteresRecherche.getVipAsList().contains(StringUtils.EMPTY)) {
            criteresRecherche.setVip(null);
        }

        // Valeurs par défaut

        // Validations
        criteresRecherche.valide();
        return criteresRecherche;
    }

    /**
     * Valide le bon format des critères de recherche.
     */
    public void valide() {
        if (StringUtils.isNotBlank(getNumClient())) {
            if (getNumClient().contains("'")) {
                setNumClient(getNumClient().replace("'", ""));
            }
            try {
                Long.valueOf(getNumClient().trim());
            } catch (NumberFormatException e) {
                throw new IllegalArgumentException("Le numéro de souscripteur n'est pas au bon format", e);
            }
        }
    }

}
