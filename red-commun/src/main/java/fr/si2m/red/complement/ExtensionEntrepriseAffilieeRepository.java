package fr.si2m.red.complement;

import java.util.List;

import fr.si2m.red.core.repository.EntiteImportableRepository;

/**
 * Référentiel des entreprises affiliées.
 * 
 * @author nortaina
 *
 */
public interface ExtensionEntrepriseAffilieeRepository extends EntiteImportableRepository<ExtensionEntrepriseAffiliee> {

    /**
     * Récupération d'un élément de type ExtensionEntrepriseAffiliee à partir de son numéro de siren.
     * 
     * @param numSiren
     *            le numéro de SIREN de l'entreprise affiliée recherchée
     * 
     * @return l'entreprise affiliée si trouvée, null sinon
     */
    ExtensionEntrepriseAffiliee getExtensionEntrepriseAffilieeComplementaire(String numSiren);

    /**
     * Récupération d'un élément de type ExtensionEntrepriseAffiliee à partir de son siren et de son nic.
     * 
     * @param siren
     *            le numéro SIREN
     * @param nic
     *            le NIC
     * 
     * @return l'entreprise affiliée si elle existe, null sinon
     */
    ExtensionEntrepriseAffiliee getExtensionEtablissementAffilieeComplementaire(String siren, String nic);

    /**
     * Retourne les numéros SIREN des entreprises liées à un contrat donné.
     * 
     * @param numContrat
     *            le numéro de contrat
     * @return la liste des numéros SIREN des entreprises liées
     */
    List<String> getSirenEntreprisesAffiliees(String numContrat);

    /**
     * Récupère les numéros de SIRET complets des établissements liés à un numéro de contrat.
     * 
     * @param numContrat
     *            le numéro de contrat
     * @return la liste des SIRET des établissements liés au contrat (concatenations siren + nic)
     */
    List<String> getSiretEtablissementsAffilies(String numContrat);

    /**
     * Récupère les numéros de SIRET complets des établissements liés à un numéro de contrat pour l'affichage IHM.
     * 
     * @param numContrat
     *            le numéro de contrat
     * @return la liste des SIRET des établissements liés au contrat (concatenations siren + nic)
     */
    List<String> getSiretEtablissementsAffiliesPourAffichage(String numContrat);

    /**
     * Récupère les entreprises / établissements liés à un numéro de contrat.
     * 
     * @param numContrat
     *            le numéro de contrat
     * @return la liste des entreprises / établissements liés au contrat (concatenations siren + nic)
     */
    List<ExtensionEntrepriseAffiliee> getEntreprisesAffiliees(String numContrat);

    /**
     * Supprime une entreprise / établissement liée à un numéro de contrat donné à partir de son siren et de son nic.
     * 
     * @param numContrat
     *            le numéro de contrat
     * @param siren
     *            le numéro SIREN
     * @param nic
     *            le NIC
     * @return le nombre d'entreprises / établissements REDAC supprimés
     */
    int supprimeEntrepriseAffiliee(String numContrat, String siren, String nic);
}
