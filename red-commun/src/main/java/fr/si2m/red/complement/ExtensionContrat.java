package fr.si2m.red.complement;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import fr.si2m.red.EntiteImportableBatch;
import fr.si2m.red.core.repository.jpa.conversion.ConvertisseurBooleanON;
import fr.si2m.red.core.repository.jpa.conversion.ConvertisseurBooleanOuiNon;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * Modèle d'extension de contrats.
 * 
 * @author nortaina
 *
 */
@Entity
@Table(name = "EXTENSIONS_CONTRATS")
@Data
@EqualsAndHashCode(callSuper = false, of = { "idTechnique" })
@ToString(callSuper = false, of = { "idTechnique", "numContrat" })
public class ExtensionContrat extends EntiteImportableBatch {

    /**
     * UID de version.
     */
    private static final long serialVersionUID = 85040329188019567L;

    /**
     * L'identifiant technique.
     * 
     * @param idTechnique
     *            l'identifiant technique
     * @return l'identifiant technique
     */
    @Id
    @GeneratedValue
    @Column(name = "ID_TECH")
    private Long idTechnique;

    /**
     * Le numéro du contrat complet.
     * 
     * @param numContrat
     *            le numéro du contrat complet
     * @return le numéro du contrat complet
     * 
     */
    @Column(name = "NOCO")
    private String numContrat;

    /**
     * Date de dernière modification des indicateurs.
     * 
     * @param dateDerniereModificationIndicateurs
     *            Date de dernière modification des indicateurs
     * @return Date de dernière modification des indicateurs
     * 
     */
    @Column(name = "DT_MODIF_IND")
    private Integer dateDerniereModificationIndicateurs;

    /**
     * Gestionnaire ayant réalisé la dernière modification des indicateurs.
     * 
     * @param gestionnaireDerniereModificationIndicateurs
     *            Gestionnaire ayant réalisé la dernière modification des indicateurs
     * @return Gestionnaire ayant réalisé la dernière modification des indicateurs
     * 
     */
    @Column(name = "GEST_MODIF_IND")
    private String gestionnaireDerniereModificationIndicateurs;

    /**
     * Date de début de la période à partir de laquelle les indicateurs ci-après sont à prendre en compte.
     * 
     * @param dateEffetIndicateurs
     *            Date de début de la période à partir de laquelle les indicateurs ci-après sont à prendre en compte
     * @return Date de début de la période à partir de laquelle les indicateurs ci-après sont à prendre en compte
     * 
     */
    @Column(name = "DT_EFFET_IND")
    private Integer dateEffetIndicateurs;

    /**
     * Description libre de la raison du changement réalisé sur les indicateurs.
     * 
     * @param motifChangementIndicateurs
     *            Description libre de la raison du changement réalisé sur les indicateurs.
     * @return Description libre de la raison du changement réalisé sur les indicateurs.
     * 
     */
    @Column(name = "MOTIF_CHGT_IND")
    private String motifChangementIndicateurs;

    /**
     * Valeur actuelle ou à venir de l’indicateur Exploitation DSN.
     * 
     * @param exploitationDSNAsText
     *            Valeur actuelle ou à venir de l’indicateur Exploitation DSN.
     * @return Valeur actuelle ou à venir de l’indicateur Exploitation DSN.
     * 
     */
    @Column(name = "EXPLOIT")
    private String exploitationDSNAsText;

    /**
     * Valeur actuelle ou à venir de l’indicateur Edition des consignes de paiement.
     * 
     * @param editionConsignesPaiementAsText
     *            Valeur actuelle ou à venir de l’indicateur Edition des consignes de paiement.
     * @return Valeur actuelle ou à venir de l’indicateur Edition des consignes de paiement.
     * 
     */
    @Column(name = "EDIT_CONS_PAIMT")
    private String editionConsignesPaiementAsText;

    /**
     * Valeur actuelle ou à venir de l’indicateur Transfert DSN.
     * 
     * @param transfertAsText
     *            Valeur actuelle ou à venir de l’indicateur Transfert DSN
     * @return Valeur actuelle ou à venir de l’indicateur Transfert DSN
     * 
     */
    @Column(name = "TRANSFERT")
    private String transfertAsText;

    /**
     * Variation minimale en nb d’établissement entre 2 périodes de déclaration pour lancer une alerte.
     * 
     * @param seuilVariationAlertesEnNbEtablissements
     *            Variation minimale en nb d’établissement entre 2 périodes de déclaration pour lancer une alerte
     * @return Variation minimale en nb d’établissement entre 2 périodes de déclaration pour lancer une alerte
     * 
     */
    @Column(name = "VAR_ETAB_NB")
    private Integer seuilVariationAlertesEnNbEtablissements;

    /**
     * Variation minimale en % de nb d’établissement entre 2 périodes de déclaration pour lancer une alerte.
     * 
     * @param seuilVariationAlertesEnPourcentageNbEtablissements
     *            Variation minimale en % de nb d’établissement entre 2 périodes de déclaration pour lancer une alerte.
     * @return Variation minimale en % de nb d’établissement entre 2 périodes de déclaration pour lancer une alerte.
     * 
     */
    @Column(name = "VAR_ETAB_PC")
    private Integer seuilVariationAlertesEnPourcentageNbEtablissements;

    /**
     * Variation minimale en nb de salariés entre 2 périodes de déclaration pour lancer une alerte.
     * 
     * @param seuilVariationAlertesEnNbSalaries
     *            Variation minimale en nb de salariés entre 2 périodes de déclaration pour lancer une alerte.
     * @return Variation minimale en nb de salariés entre 2 périodes de déclaration pour lancer une alerte.
     * 
     */
    @Column(name = "VAR_SAL_NB")
    private Integer seuilVariationAlertesEnNbSalaries;

    /**
     * Variation minimale en % de nb de salariés entre 2 périodes de déclaration pour lancer une alerte.
     * 
     * @param seuilVariationAlertesEnPourcentageNbSalaries
     *            Variation minimale en % de nb de salariés entre 2 périodes de déclaration pour lancer une alerte
     * @return Variation minimale en % de nb de salariés entre 2 périodes de déclaration pour lancer une alerte
     * 
     */
    @Column(name = "VAR_SAL_PC")
    private Integer seuilVariationAlertesEnPourcentageNbSalaries;

    /**
     * Indique si la Brique DSN ou si REDAC doit contrôler les types de bases assujettie reçus dans les déclarations de ce contrat.
     * 
     * @param controleTypeBaseAssujettie
     *            Indique si la Brique DSN ou si REDAC doit contrôler les types de bases assujettie reçus dans les déclarations de ce contrat.
     * @return Indique si la Brique DSN ou si REDAC doit contrôler les types de bases assujettie reçus dans les déclarations de ce contrat.
     * 
     */
    @Column(name = "CTL_TYPE_BASE_ASSUJ")
    private String controleTypeBaseAssujettie;

    /**
     * Définit si le contrat et un contrat direct ou bien un contrat délégué.
     * 
     * @param modeNatureContrat
     *            Définit si le contrat et un contrat direct ou bien un contrat délégué
     * @return Définit si le contrat et un contrat direct ou bien un contrat délégué
     * 
     */
    @Column(name = "MODE_NAT_CONT")
    private String modeNatureContrat;

    /**
     * Indique si le contrat est VIP.
     * 
     * @param vipAsText
     *            Indique si le contrat est VIP
     * @return Indique si le contrat est VIP
     * 
     */
    @Column(name = "VIP")
    private String vipAsText;

    /**
     * Indique le mode de prise en compte des effectifs pour les contrats sur effectif.
     * 
     * @param modeReaffectationCategorieEffectifs
     *            Indique le mode de prise en compte des effectifs pour les contrats sur effectif
     * @return Indique le mode de prise en compte des effectifs pour les contrats sur effectif
     * 
     */
    @Column(name = "MODE_REAFF_CAT_EFFECTIF")
    private String modeReaffectationCategorieEffectifs;

    /**
     * Le mode de déclaration du contrat
     * 
     * @param modeDeclaration
     *            le mode de déclaration du contrat (pouvant avoir la valeur A ou C)
     * @return le mode de déclaration du contrat
     */
    @Column(name = "DONNEES_A_INTEGRER")
    private String modeDeclaration;

    /**
     * Indicateur de ligne en cours d'import batch.
     * 
     * @param ligneEnCoursImportBatch
     *            indicateur de ligne en cours d'import batch
     * @return indicateur de ligne en cours d'import batch
     * 
     */
    @Transient
    private boolean ligneEnCoursImportBatch;

    /**
     * L'indicateur d'exploitation DSN.
     * 
     * @return l'indicateur d'exploitation DSN
     */
    public boolean isExploitationDSN() {
        return new ConvertisseurBooleanOuiNon().convertToEntityAttribute(getExploitationDSNAsText());
    }

    /**
     * Indicateur d'état de contrat actif.
     * 
     * @param exploitationDSN
     *            l'indicateur d'état de contrat actif
     */
    public void setExploitationDSN(boolean exploitationDSN) {
        setExploitationDSNAsText(new ConvertisseurBooleanOuiNon().convertToDatabaseColumn(exploitationDSN));
    }

    /**
     * Valeur actuelle ou à venir de l’indicateur Edition des consignes de paiement.
     * 
     * @return Valeur actuelle ou à venir de l’indicateur Edition des consignes de paiement.
     * 
     */
    public boolean isEditionConsignesPaiement() {
        return new ConvertisseurBooleanOuiNon().convertToEntityAttribute(getEditionConsignesPaiementAsText());
    }

    /**
     * Valeur actuelle ou à venir de l’indicateur Edition des consignes de paiement.
     * 
     * @param editionConsignesPaiement
     *            Valeur actuelle ou à venir de l’indicateur Edition des consignes de paiement.
     */
    public void setEditionConsignesPaiement(boolean editionConsignesPaiement) {
        setEditionConsignesPaiementAsText(new ConvertisseurBooleanOuiNon().convertToDatabaseColumn(editionConsignesPaiement));
    }

    /**
     * Valeur actuelle ou à venir de l’indicateur Transfert DSN.
     * 
     * @return Valeur actuelle ou à venir de l’indicateur Transfert DSN
     * 
     */
    public boolean isTransfert() {
        return new ConvertisseurBooleanOuiNon().convertToEntityAttribute(getTransfertAsText());
    }

    /**
     * Valeur actuelle ou à venir de l’indicateur Transfert DSN.
     * 
     * @param transfert
     *            Valeur actuelle ou à venir de l’indicateur Transfert DSN
     */
    public void setTransfert(boolean transfert) {
        setTransfertAsText(new ConvertisseurBooleanOuiNon().convertToDatabaseColumn(transfert));
    }

    /**
     * Indicateur d'état de contrat actif.
     * 
     * @return l'indicateur d'état de contrat actif
     */
    public boolean isVip() {
        return new ConvertisseurBooleanON().convertToEntityAttribute(getVipAsText());
    }

    /**
     * Indicateur d'état de contrat actif.
     * 
     * @param vip
     *            l'indicateur d'état de contrat actif
     */
    public void setVip(boolean vip) {
        setVipAsText(new ConvertisseurBooleanON().convertToDatabaseColumn(vip));
    }

    @Override
    public Long getId() {
        return idTechnique;
    }

}
