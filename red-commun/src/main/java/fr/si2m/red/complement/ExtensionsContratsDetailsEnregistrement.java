package fr.si2m.red.complement;

import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;

import org.apache.commons.beanutils.BeanUtils;
import org.springframework.util.MultiValueMap;

import fr.si2m.red.RedacUnexpectedException;
import lombok.Getter;
import lombok.Setter;

/**
 * Détails d'enregistrement de contrats.
 * 
 * @author poidij
 *
 */
public class ExtensionsContratsDetailsEnregistrement implements Serializable {
    /**
     * UID de version.
     */
    private static final long serialVersionUID = -2050758235846669541L;

    /**
     * Le numéro de contrat à enregistrer.
     * 
     * @param numContrat
     *            le numéro de contrat à enregistrer
     * @return le numéro de contrat à enregistrer
     */
    @Getter
    @Setter
    private String numeroContrat;

    /**
     * La date de dernière modification des indicateurs (à afficher).
     * 
     */
    @Getter
    @Setter
    private String dateDerniereModificationIndicateurs;

    /**
     * Le gestionnaire de la dernière modification des indicateurs (à afficher).
     * 
     */
    @Getter
    @Setter
    private String gestionnaireDerniereModificationIndicateurs;

    /**
     * La date d'effet à enregistrer.
     * 
     */
    @Getter
    @Setter
    private String dateEffet;

    /**
     * Le commentaire utilisateur à enregistrer.
     * 
     * @param commentaireUtilisateur
     *            le commentaire utilisateur à enregistrer
     * @return le commentaire utilisateur à enregistrer
     */
    @Getter
    @Setter
    private String commentaireUtilisateur;

    /**
     * L'indicateur d'exploitation recherché.
     * 
     * @param indicExploitation
     *            l'indicateur d'exploitation recherché
     * @return l'indicateur d'exploitation recherché
     */
    @Getter
    @Setter
    private String indicExploitationSaisi;

    /**
     * L'indicateur d'édition des consignes de paiement recherché.
     * 
     */
    @Getter
    @Setter
    private String indicConsignePaiementSaisi;

    /**
     * L'indicateur de transfert recherché.
     * 
     */
    @Getter
    @Setter
    private String indicTransfertSaisi;

    /**
     * Le compte d'encaissement recherché.
     * 
     * @param cptEnc
     *            le compte d'encaissement recherché
     * @return le compte d'encaissement recherché
     */
    @Getter
    @Setter
    private String nbEtablissements;

    /**
     * Le compte d'encaissement recherché.
     * 
     * @param cptEnc
     *            le compte d'encaissement recherché
     * @return le compte d'encaissement recherché
     */
    @Getter
    @Setter
    private String pcEtablissements;

    /**
     * Le compte d'encaissement recherché.
     * 
     * @param cptEnc
     *            le compte d'encaissement recherché
     * @return le compte d'encaissement recherché
     */
    @Getter
    @Setter
    private String nbSalaries;

    /**
     * L'affiliation de l'entreprise recherchée.
     * 
     */
    @Getter
    @Setter
    private String pcSalaries;

    /**
     * La présence de flag VIP recherchée.
     * 
     */
    @Getter
    @Setter
    private String baseAssuj;

    /**
     * La présence de flag VIP recherchée.
     * 
     */
    @Getter
    @Setter
    private String gestionDirecte;

    /**
     * La présence du flag VIP recherchée.
     * 
     * @return la présence du flag VIP recherchée
     */
    @Getter
    @Setter
    private String vipSaisi;

    /**
     * La présence de flag VIP recherchée.
     * 
     */
    @Getter
    @Setter
    private String modeReaffectation;

    /**
     * Le numéro de SIREN recherché.
     * 
     * @param siren
     *            le numéro de SIREN recherché
     * @return le numéro de SIREN recherché
     */
    @Getter
    @Setter
    private String listeSirenNic;

    /**
     * La présence du flag VIP recherchée.
     * 
     * @return la présence du flag VIP recherchée
     */
    @Getter
    @Setter
    private String enregistrer;

    /**
     * La liste des erreurs détectées.
     * 
     * @return la liste des erreurs détectées
     */
    @Getter
    @Setter
    private String listeErreurs;

    /**
     * Les critères de recherche (format url) entrés à l'écran de recherche.
     * 
     * @return les critères de recherche (format url) entrés à l'écran de recherche
     */
    @Getter
    @Setter
    private String rechercheUrlQueryString;

    /**
     * Le mode de déclaration associé à l'extension contrat
     */
    @Getter
    @Setter
    private String modeDeclaration;

    /**
     * Constructeur par défaut des critères.
     */
    public ExtensionsContratsDetailsEnregistrement() {
    }

    /**
     * Constructeur de critères par copie de critères donnés.
     * 
     * @param extensionsContratsCriteresRecherche
     *            les critères à copier
     */
    public ExtensionsContratsDetailsEnregistrement(ExtensionsContratsDetailsEnregistrement extensionsContratsCriteresRecherche) {
        this();
        try {
            BeanUtils.copyProperties(this, extensionsContratsCriteresRecherche);
        } catch (IllegalAccessException | InvocationTargetException e) {
            throw new RedacUnexpectedException("Erreur lors de la copie de critères de recherche", e);
        }
    }

    /**
     * Convertit une map de valeurs multiple en critères de recherche de contrats.
     * 
     * @param criteres
     *            les critères sous forme de map de valeurs multiples
     * @return les critères de recherche de contrats extraits
     */
    public static ExtensionsContratsDetailsEnregistrement from(MultiValueMap<String, Object> criteres) {
        ExtensionsContratsDetailsEnregistrement criteresRecherche = new ExtensionsContratsDetailsEnregistrement();
        try {
            BeanUtils.populate(criteresRecherche, criteres);
        } catch (IllegalAccessException | InvocationTargetException e) {
            throw new RedacUnexpectedException("Erreur lors du mapping de critères de recherche", e);
        }

        return criteresRecherche;
    }
}
