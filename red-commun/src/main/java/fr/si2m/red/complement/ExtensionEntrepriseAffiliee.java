package fr.si2m.red.complement;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import fr.si2m.red.EntiteImportableBatch;

/**
 * Modèle d'extension d'entreprises affiliées.
 * 
 * @author nortaina
 *
 */
@Entity
@Table(name = "EXTENSIONS_ENTREPRISES_AFFILIEES")
@Data
@EqualsAndHashCode(callSuper = false, of = { "idTechnique" })
@ToString(callSuper = false, of = { "idTechnique", "numContrat", "siren", "nic" })
public class ExtensionEntrepriseAffiliee extends EntiteImportableBatch {

    /**
     * UID de version.
     */
    private static final long serialVersionUID = 8845348469000120482L;

    /**
     * L'identifiant technique.
     * 
     * @param idTechnique
     *            l'identifiant technique
     * @return l'identifiant technique
     */
    @Id
    @GeneratedValue
    @Column(name = "ID_TECH")
    private Long idTechnique;

    /**
     * Le numéro du contrat complet.
     * 
     * @param numContrat
     *            le numéro du contrat complet
     * @return le numéro du contrat complet
     * 
     */
    @Column(name = "NOCO")
    private String numContrat;

    /**
     * Le numéro SIREN de l'entreprise.
     * 
     * @param siren
     *            le numéro SIREN de l'entreprise
     * @return le numéro SIREN de l'entreprise
     * 
     */
    @Column(name = "SIREN")
    private String siren;

    /**
     * Le complément SIRET de l'entreprise.
     * 
     * @param nic
     *            le complément SIRET de l'entreprise
     * @return le complément SIRET de l'entreprise
     * 
     */
    @Column(name = "NIC")
    private String nic;

    /**
     * Indicateur de ligne en cours d'import batch.
     * 
     * @param ligneEnCoursImportBatch
     *            indicateur de ligne en cours d'import batch
     * @return indicateur de ligne en cours d'import batch
     * 
     */
    @Transient
    private boolean ligneEnCoursImportBatch;

    @Override
    public Long getId() {
        return idTechnique;
    }

}
