package fr.si2m.red.complement;

import java.util.List;

import fr.si2m.red.core.repository.EntiteImportableRepository;

/**
 * Référentiel des compléments d'information sur contrats.
 * 
 * @author nortaina
 *
 */
public interface ExtensionContratRepository extends EntiteImportableRepository<ExtensionContrat> {

    /**
     * Récupère une extension de contrat via son numéro de contrat.
     * 
     * @param numContrat
     *            le numéro de contrat
     * @return l'extension de contrat si elle existe, null sinon
     */
    ExtensionContrat getExtensionContrat(String numContrat);

    /**
     * Recherche des extensions de contrats correspondant à certains critères.
     * 
     * @param criteres
     *            les critères de recherche des extensions de contrats
     * @return la liste des extensions de contrats correspondant aux critères de recherche
     * 
     */
    List<ResumeExtensionContrat> rechercheExtensionsContrats(ExtensionsContratsCriteresRecherche criteres);

    /**
     * Compte la totalité des extensions de contrats présents dans le référentiel correspondant à des critères de recherche.
     * 
     * @param criteres
     *            les critères de recherche
     * @return le nombre total d'extensions de contrats présents dans le référentiel correspondant aux critères de recherche
     */
    long compte(ExtensionsContratsCriteresRecherche criteres);

    /**
     * Teste si un contrat donné est référencé dans la table des extensions de contrats.
     * 
     * @param numContrat
     *            Le numéro du contrat à rechercher
     * @return true si le contrat est référencé au moins une fois, false sinon
     */
    boolean existeExtensionContrat(String numContrat);

    /**
     * Teste si un contrat donné est vip.
     * 
     * @param numContrat
     *            Le numéro du contrat à tester
     * @return true si le contrat est vip, false sinon
     */
    boolean isContratVIP(String numContrat);
}
