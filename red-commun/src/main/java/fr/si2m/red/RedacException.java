package fr.si2m.red;

/**
 * Erreur dans un traitement REDAC, pour différencier le type d'exception avec RedacUnexpectedException
 * 
 * @author eudesr
 *
 */
public class RedacException extends RuntimeException {
    /**
     * UID de version.
     */
    private static final long serialVersionUID = 7548007939433643391L;

    /**
     * Constructeur d'une erreur .
     * 
     * @param cause
     *            la cause de l'erreur
     */
    public RedacException(Throwable cause) {
        super(cause);
    }

    /**
     * Constructeur d'une erreur .
     * 
     * @param message
     *            le message de l'erreur
     */
    public RedacException(String message) {
        super(message);
    }

    /**
     * Constructeur d'une erreur .
     * 
     * @param message
     *            le message de l'erreur
     * @param cause
     *            la cause de l'erreur
     */
    public RedacException(String message, Throwable cause) {
        super(message, cause);
    }
}
