package fr.si2m.red;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import lombok.Data;

/**
 * Erreur fonctionnelle bloquante sur une ligne d'un fichier importé dans un batch.<br/>
 * <br/>
 * Utilisée pour tracer un ensemble d'erreurs sur un fichier pour une étape de validation de globale.
 * 
 * @author nortaina
 *
 */
@Data
public class ErreurLigneFichierImport implements Serializable {
    /**
     * UID de version.
     */
    private static final long serialVersionUID = -6116239297748259015L;

    /**
     * Le nom du fichier.
     * 
     * @param nomFichier
     *            le nom du fichier
     * @return le nom du fichier
     */
    private String nomFichier;
    /**
     * Le numéro de ligne.
     * 
     * @param numeroLigne
     *            le numéro de ligne
     * @return le numéro de ligne
     */
    private int numeroLigne = -1;
    /**
     * Le message d'erreur.
     * 
     * @param messageErreur
     *            le message d'erreur
     * @return le message d'erreur
     */
    private String messageErreur;

    @Override
    public String toString() {
        String timestamp = new SimpleDateFormat("yyyyMMdd HH:mm:ss").format(Calendar.getInstance().getTime());
        StringBuilder sb = new StringBuilder(timestamp);
        sb.append(" Fichier ").append(getNomFichier());
        if (getNumeroLigne() > 0) {
            sb.append(", ligne ").append(getNumeroLigne());
        }
        sb.append(" : ").append(getMessageErreur());
        return sb.toString();
    }
}
