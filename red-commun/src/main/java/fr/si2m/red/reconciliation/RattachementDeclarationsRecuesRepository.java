package fr.si2m.red.reconciliation;

import java.util.List;
import java.util.Set;

import fr.si2m.red.core.repository.RedacRepository;
import fr.si2m.red.dsn.AdhesionEtablissementMois;
import fr.si2m.red.dsn.Affiliation;
import fr.si2m.red.dsn.BaseAssujettie;
import fr.si2m.red.dsn.ComposantBaseAssujettie;
import fr.si2m.red.dsn.ComposantVersement;
import fr.si2m.red.dsn.ContratTravail;
import fr.si2m.red.dsn.CotisationEtablissement;
import fr.si2m.red.dsn.ResumeAffiliation;
import fr.si2m.red.dsn.ResumeAffiliationExportExcel;
import fr.si2m.red.dsn.ResumeAffiliationExportExcelAllegesMessage;
import fr.si2m.red.dsn.ResumeAffiliationExportExcelAllegesMontant;
import fr.si2m.red.dsn.ResumeChangementIndividu;
import fr.si2m.red.dsn.Versement;

/**
 * Référentiel des éléments DSN rattachés à des déclarations reçues.
 * 
 * @author poidij
 *
 */
public interface RattachementDeclarationsRecuesRepository extends RedacRepository<RattachementDeclarationsRecues> {

    /**
     * Récupère la liste complète des contrats de travail pour un identifiant de période donné.
     * 
     * @param idPeriode
     *            l'identifiant de la période
     * @param indexPremierResultat
     *            l'index du premier résultat (commençant à 0), 0 si null
     * @param taillePage
     *            la taille maximale d'une page, tous les résultats à partir du premier si null
     * @return la liste des contrats de travail liés
     */
    List<ContratTravail> getContratsTravailRattachesPagines(Long idPeriode, Integer indexPremierResultat, Integer taillePage);

    /**
     * Récupère le nombre de contrats de travail rattachés (avec numéros de contrat distincts) pour un identifiant de période donné.
     * 
     * @param idPeriode
     *            l'identifiant de la période
     * @return le nombre de contrats de travail rattachés (avec numéros de contrat distincts)
     */
    long compteContratsTravailRattachesDistincts(Long idPeriode);

    /**
     * Récupère la liste complète des bases assujetties avec montant de cotisation différent de 0 pour un identifiant de période donné de façon paginée.
     * 
     * @param idPeriode
     *            l'identifiant de la période
     * @param indexPremierResultat
     *            l'index du premier résultat (commençant à 0), 0 si null
     * @param taillePage
     *            la taille maximale d'une page, tous les résultats à partir du premier si null
     * @return la liste des bases assujetties liées
     */
    List<BaseAssujettie> getBasesAssujettiesRattacheesAvecMontantCotisationNonNulPagines(Long idPeriode, Integer indexPremierResultat,
            Integer taillePage);

    /**
     * Récupère la liste complète des bases assujetties pour un identifiant de période donné de façon paginée.
     * 
     * @param idPeriode
     *            l'identifiant de la période
     * @param indexPremierResultat
     *            l'index du premier résultat (commençant à 0), 0 si null
     * @param taillePage
     *            la taille maximale d'une page, tous les résultats à partir du premier si null
     * @return la liste des bases assujetties liées
     */
    List<BaseAssujettie> getAllBasesAssujettiesRattacheesPagines(Long idPeriode, Integer indexPremierResultat, Integer taillePage);

    /**
     * Récupère la liste complète des bases assujetties pour un identifiant de période donné de façon paginée.
     * 
     * @param idPeriode
     *            l'identifiant de la période
     * @param indexPremierResultat
     *            l'index du premier résultat (commençant à 0), 0 si null
     * @param taillePage
     *            la taille maximale d'une page, tous les résultats à partir du premier si null
     * @return la liste des bases assujetties liées
     */
    List<ResumeDeclarationIndividuExcel> getBasesAssujettiesRattacheesPagines(Long idPeriode, int indexPremierResultat, int taillePage);

    /**
     * Récupère l'ensemble des identifiants distincts des individus rattachés à une période donnée.
     * 
     * @param idPeriode
     *            l'identifiant de la période
     * @param indexPremierResultat
     *            l'index du premier résultat (commençant à 0), 0 si null
     * @param taillePage
     *            la taille maximale d'une page, tous les résultats à partir du premier si null
     * @return l'ensemble des identifiants distincts des individus rattachés à la période
     */
    Set<IdentifiantsIndividu> getDistinctIdentifiantsIndividusRattachesPagines(Long idPeriode, Integer indexPremierResultat, Integer taillePage);

    /**
     * Vérifie si une cotisation d'établissement est rattaché à une période donnée.
     * 
     * @param idPeriode
     *            l'identifiant de la période
     * @return true s'il existe une cotisation d'établissement rattaché à la période, false sinon
     */
    boolean existeUneCotisationEtablissementRattachee(Long idPeriode);

    /**
     * Compte le nombre d'établissements rattachés à une période donnée.
     * 
     * @param idPeriode
     *            l'identifiant de la période
     * @return le nombre d'établissements rattachés à la période
     */
    int compteNbEtablissementsRattaches(Long idPeriode);

    /**
     * Compte le nombre de salariés rattachés à une période donnée à partir d'une certaine date.
     * 
     * @param idPeriode
     *            l'identifiant de la période
     * @param dateDebutPeriode
     *            la date de début de la période
     * @return le nombre de salariés rattachés en début de période
     */
    int compteNbSalariesRattachesEnDebutPeriode(Long idPeriode, Integer dateDebutPeriode);

    /**
     * Compte le nombre de salariés rattachés à une période donnée jusqu'à une certaine date.
     * 
     * @param idPeriode
     *            l'identifiant de la période
     * @param dateFinPeriode
     *            la date de fin de la période
     * @return le nombre de salariés rattachés en fin de période
     */
    int compteNbSalariesRattachesEnFinPeriode(Long idPeriode, Integer dateFinPeriode);

    /**
     * Compte le nombre de salariés rattachés à une période donnée.
     * 
     * @param idPeriode
     *            l'identifiant de la période
     * @return le nombre de salariés rattachés à la période
     */
    int compteNbSalariesRattaches(Long idPeriode);

    /**
     * Vérifie qu'il existe des versements avec mode de paiement particulier (03 (prélèvement), 04 (TIP) ou 05 (télérèglement)) rattachées à une période donnée.
     * 
     * @param idPeriode
     *            l'identifiant de la période
     * @return true s'il existe au moins un versement rattaché avec un mode de paiement particulier, false sinon
     */
    boolean existeVersementRattacheAvecModePaiementParticulier(Long idPeriode);

    /**
     * Récupère les composants de versements rattachés à une période donnée pouvant être contrôlés.
     * 
     * @param idPeriode
     *            l'identifiant de la période
     * @param indexPremierResultat
     *            l'index du premier résultat (commençant à 0), 0 si null
     * @param taillePage
     *            la taille maximale d'une page, tous les résultats à partir du premier si null
     * @return les composants de versement rattachés avec une périodicité déclarée différente de "E"
     */
    List<ComposantVersement> getComposantsVersementsRattachesControlablesPagines(Long idPeriode, Integer indexPremierResultat, Integer taillePage);

    /**
     * Récupère les adhésions d'établissements par mois de déclaration rattachées à une période donnée.
     * 
     * @param idPeriode
     *            l'identifiant de la période
     * @return les adhésions rattachées
     */
    List<AdhesionEtablissementMois> getAdhesionsEtablissementsMoisRattaches(Long idPeriode);

    /**
     * Récupère une adhésion d'établissement rattachées à une période donnée pour un SIREN entreprise / un NIC établissement.
     * 
     * @param idPeriode
     *            l'identifiant de la période
     * @param sirenEntreprise
     *            le SIREN de l'entreprise
     * @param nicEtablissement
     *            le NIC de l'établissement
     * @return l'adhésion rattachée
     */
    AdhesionEtablissementMois getDerniereAdhesionEtablissementMoisRattachee(Long idPeriode, String sirenEntreprise, String nicEtablissement);

    /**
     * Récupère l'adhésion d'établissement (déclaration) la plus récente rattachée à une période donnée.
     * 
     * @param idPeriode
     *            l'identifiant de la période
     * @return la déclaration la plus récente rattachée à la période
     */
    AdhesionEtablissementMois getDerniereAdhesionEtablissementMoisRattachee(Long idPeriode);

    /**
     * Récupère le mois declare le plus récent pour une période donnée.
     * 
     * @param idPeriode
     *            l'identifiant de la période
     * @return la déclaration la plus récente déclarée à la période
     */
    Integer getDernierMoisDeclareAdhesionEtablissementMois(Long idPeriode);

    /**
     * Récupère la date de la dernière réception de DSN pour une période donnée.
     * 
     * @param idPeriode
     *            l'identifiant de la période
     */
    Integer getDateDerniereReceptionDSN(Long idPeriode);

    /**
     * Récupère les composants de bases assujetties rattachées à une période donnée de façon paginée.
     * 
     * @param idPeriode
     *            l'identifiant de la période
     * @param indexPremierResultat
     *            l'index du premier résultat (commençant à 0), 0 si null
     * @param taillePage
     *            la taille maximale d'une page, tous les résultats à partir du premier si null
     * @return les composants de bases assujetties rattachées
     */
    List<ComposantBaseAssujettie> getComposantsBasesAssujettiesRattachesPagines(Long idPeriode, Integer indexPremierResultat, Integer taillePage);

    /**
     * Vérifie s'il existe un changement de date de naissance d'un assuré rattaché à une période donnée.
     * 
     * @param idPeriode
     *            l'identifiant de la période
     * @return true s'il existe un changement de date de naissance, false sinon
     */
    boolean existeUnChangementDateNaissanceIndividuRattache(Long idPeriode);

    /**
     * Vérifie s'il existe un changement de NIR d'un assuré rattaché à une période donnée.
     * 
     * @param idPeriode
     *            l'identifiant de la période
     * @return true s'il existe un changement de NIR, false sinon
     */
    boolean existeUnChangementNIRIndividuRattache(Long idPeriode);

    /**
     * Récupère les versements positifs pour une période donnée.
     * 
     * @param idPeriode
     *            l'ID de la période
     * @return les versements positifs liés
     */
    List<Versement> getVersementsPositifsPourPeriode(Long idPeriode);

    /**
     * Récupère les versements pour une période donnée.
     * 
     * @param idPeriode
     *            l'ID de la période
     * @return les versements liés
     */
    List<Versement> getVersementsPourPeriode(Long idPeriode);

    /**
     * Récupère le dernier versement positif pour une période donnée.
     * 
     * @param idPeriode
     *            l'ID de la période
     * @return les versements positifs liés
     */
    Versement getDernierVersementPositifPourPeriode(Long idPeriode);

    /**
     * Récupère la somme des montantVersements pour une période et un mode de paiement donnés. Si le mode de paiement n'est pas précisé, récupère la somme
     * globale.
     * 
     * @param idPeriode
     *            l'ID de la période
     * @param modePaiement
     *            le mode de paiement
     * @return la somme des montantVersements
     */
    Double getSommeMontantsVersement(Long idPeriode, String modePaiement);

    /**
     * Récupère la somme des composants de versement pour une période.
     * 
     * @param idPeriode
     *            l'identifiant de la période
     * @return la somme des composants de versement
     */
    Double getSommeComposantsVersement(Long idPeriode);

    /**
     * Récupère la liste des modes de paiements des versements pour une période donnée.
     * 
     * @param idPeriode
     *            l'ID de la période
     * @return la liste des modes de paiements des versements pour la période
     */
    List<String> getModesPaiementsDistincts(Long idPeriode);

    /**
     * Récupère la liste des affiliations pour une période donnée de façon paginée.
     * 
     * @param idPeriode
     *            l'ID de la période
     * @param indexPremierResultat
     *            l'index du premier résultat (commençant à 0), 0 si null
     * @param taillePage
     *            la taille maximale d'une page, tous les résultats à partir du premier si null
     * @return la liste des affiliations pour la période
     */
    List<Affiliation> getAffiliationsRattachesPagines(Long idPeriode, Integer indexPremierResultat, Integer taillePage);

    /**
     * Récupère la liste des affiliations distinctes pour une période donnée de façon paginée.
     * 
     * @param idPeriode
     *            l'ID de la période
     * @param indexPremierResultat
     *            l'index du premier résultat (commençant à 0), 0 si null
     * @param taillePage
     *            la taille maximale d'une page, tous les résultats à partir du premier si null
     * @return la liste des affiliations distinctes pour la période
     */
    List<ResumeAffiliation> getResumesAffiliationsDistinctesRattachesPagines(Long idPeriode, Integer indexPremierResultat, Integer taillePage);

    /**
     * Récupère la liste des affiliations distinctes pour une période donnée de façon paginée pour l'export Excel.
     * 
     * @param idPeriode
     *            l'ID de la période
     * @param indexPremierResultat
     *            l'index du premier résultat (commençant à 0), 0 si null
     * @param taillePage
     *            la taille maximale d'une page, tous les résultats à partir du premier si null
     * @return la liste des affiliations distinctes pour la période
     */
    List<ResumeAffiliationExportExcel> getResumesAffiliationsDistinctesRattachesPaginesExportExcel(Long idPeriode, Integer dateDebutPeriode,
            Integer dateFinPeriode, Integer indexPremierResultat, Integer taillePage);

    /**
     * Récupère la liste des cotisations des établissements pour une période donnée.
     * 
     * @param idPeriode
     *            l'ID de la période
     * @return la liste des cotisations des établissements pour la période
     */
    List<CotisationEtablissement> getCotisationsEtablissementsRattachees(Long idPeriode);

    /**
     * Compte les affiliations pour une période donnée.
     * 
     * @param idPeriode
     *            l'ID de la période
     * @return le nombre d'affiliations pour la période
     */
    long compteAffiliationsRattachees(Long idPeriode);

    /**
     * Compte les fins de contrat pour une période donnée.
     * 
     * @param idPeriode
     *            l'ID de la période
     * @return le nombre de fins de contrats pour la période
     */
    long compteFinsDeContratsRattachees(Long idPeriode);

    /**
     * Récupère les changements d'un individu identifié par son couple NIR/NTT et rattachée à une période donné.
     * 
     * @param idPeriode
     *            l'identifiant de la période
     * @param identifiantRepertoire
     *            le NIR de l'individu
     * @param numeroTechniqueTemporaire
     *            le NTT de l'individu
     * @return les changements de l'individu rattaché
     */
    List<ResumeChangementIndividu> getChangementsIndividuRattache(Long idPeriode, String identifiantRepertoire, String numeroTechniqueTemporaire);

    /**
     * Récupère la colonne String du changement d'un individu identifié par son couple NIR/NTT et rattachée à une période donné.
     * 
     * @param idPeriode
     *            l'identifiant de la période
     * @param nomColonne
     *            la colonne String a selectionner
     * @param identifiantRepertoire
     *            le NIR de l'individu
     * @param numeroTechniqueTemporaire
     *            le NTT de l'individu
     * @return les changements de l'individu rattaché
     */
    String getColonneChangementsIndividuRattacheString(Long idPeriode, String nomColonne, String identifiantRepertoire,
            String numeroTechniqueTemporaire);

    /**
     * Récupère la colonne String du changement d'un individu identifié par son couple NIR/NTT et rattachée à une période donné.
     * 
     * @param idPeriode
     *            l'identifiant de la période
     * @param nomColonne
     *            la colonne Integer a selectionner
     * @param identifiantRepertoire
     *            le NIR de l'individu
     * @param numeroTechniqueTemporaire
     *            le NTT de l'individu
     * @return les changements de l'individu rattaché
     */
    Integer getColonneChangementsIndividuRattacheInteger(Long idPeriode, String nomColonne, String identifiantRepertoire,
            String numeroTechniqueTemporaire);

    /**
     * Récupère la période d'affectation invalide d'un composant de versement rattaché à une période donnée le cas échéant.
     * 
     * @param idPeriode
     *            l'identifiant de la période
     * @param dateDebutPeriode
     *            la date de début de période
     * @param dateFinPeriode
     *            la date de fin de période
     * @return la période d'affectation d'un composant invalide s'il existe
     */
    String getPeriodeAffectationComposantVersementRattacheInvalide(Long idPeriode, Integer dateDebutPeriode, Integer dateFinPeriode);

    /**
     * Détache les adhésions relatives au dernier lot de rattachement d'une période.
     * 
     * @param idPeriode
     *            l'identifiant de la période
     * @return le nombre d'adhésions détachées
     */
    int detacheAdhesionsDernierLotRattachementPourPeriode(Long idPeriode);

    /**
     * Détache les adhésions relatives au dernier lot de rattachement d'une liste de périodes.
     * 
     * @param idsPeriodes
     *            la liste des identifiants de périodes
     * @return le nombre d'adhésions détachées
     */
    int detacheAdhesionsDernierLotRattachementEnMasse(Set<Long> idsPeriodes);

    /**
     * Vérifie s'il existe une ligne dans RattachementDeclarationRecue pour la période donnée
     * 
     * @param idPeriode
     *            l'identifiant de la période
     * @return true s'il existe au moins une ligne dans RattachementDeclarationRecue pour la période, false sinon
     */
    boolean existeRattachementDeclarationRecue(Long idPeriode);

    /**
     * Récupère l'ensemble des individus avec un identifiant nir/ntt distinct, triés par nom usage, prenom, nir , ntt
     * 
     * @param idPeriode
     *            l'identifiant de la période
     * @param indexPremierResultat
     *            l'index du premier résultat (commençant à 0), 0 si null
     * @param taillePage
     *            la taille maximale d'une page, tous les résultats à partir du premier si null
     * @return l'ensemble des individus resume (nomUsage, prenom,nir,ntt,matricule), triés.
     */
    List<ExportIndividuExcel> getDistinctIndividusTriesRattachesPagines(Long idPeriode, Integer indexPremierResultat, Integer taillePage);

    /**
     * Vérifie pour les périodes données si elles sont orphelines (aucune ligne dans rattachementDeclarationRecue )
     * 
     * @param idsPeriodes
     *            les id des périodes à vérifier
     * @return l'ensemble des périodes orphelines parmi le set donné
     */
    List<Long> getPeriodesOrphelines(Set<Long> idsPeriodes);

    /**
     * Récupère les code population des affiliations rattachées à une période donnée
     * 
     * @param idPeriode
     *            l'identifiant de la période
     * @return les composants de bases assujetties rattachées
     */
    List<String> getCodePopulationAffiliation(Long idPeriode);

    /**
     * Rattachement des adhesions etablissement mois des periodes "autres" vers la période cible dans le cadre d'une fusion - Mise à jour de l'utilisateur
     * 
     * @param idPeriodeCible
     *            idPeriode "cible"
     * @param idsPeriodeAutres
     *            liste des ids de périodes "autres"
     * @param loginUser
     *            utilisateur effectuant la fusion
     * @return le nombre de ligne mise à jour
     */
    int rattachementVersPeriodeCible(Long idPeriodeCible, List<Long> idsPeriodeAutres, String loginUser);

    /**
     * Récupération de la liste des individus allégés avec message de rejet et alerte
     * 
     * @param idPeriodeCible
     *            idPeriode "cible"
     * @param idsPeriodeAutres
     *            liste des ids de périodes "autres"
     * @param loginUser
     *            utilisateur effectuant la fusion
     * @return le nombre de ligne mise à jour
     */
    List<ResumeAffiliationExportExcelAllegesMessage> getIndividusMessages(Long idPeriodeCible);

    /**
     * Récupération de la liste des individus allégés avec les montants présent CQI
     * 
     * @param idPeriodeCible
     *            idPeriode "cible"
     * @param idsPeriodeAutres
     *            liste des ids de périodes "autres"
     * @param loginUser
     *            utilisateur effectuant la fusion
     * @return le nombre de ligne mise à jour
     */
    List<ResumeAffiliationExportExcelAllegesMontant> getIndividusMontants(Long idPeriodeCible);

}
