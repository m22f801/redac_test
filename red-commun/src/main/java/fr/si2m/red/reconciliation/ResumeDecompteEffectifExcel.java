package fr.si2m.red.reconciliation;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * Résumé d'une base assujettie (avec informations consolidées).
 * 
 * @author rupip
 *
 */
@Data
@AllArgsConstructor
public class ResumeDecompteEffectifExcel implements Serializable {

    /**
     * UID de version.
     */

    private static final long serialVersionUID = 1L;

    /**
     * La date de constitution du fichier
     *
     * @param dateConstitution
     * 
     * @return La date de constitution pour l'adhesion rattache a la periode en cours
     */
    private Integer dateConstitution;

    /**
     * Date du mois de rattachement
     *
     * @param moisRattachement
     * 
     * @return Date du mois de rattachement pour l'adhesion rattache a la periode en cours
     */
    private Integer moisRattachement;

    /**
     * Date du mois principal déclaré
     *
     * @param moisDeclare
     * 
     * @return Date du mois principal déclaré pour l'adhesion rattache a la periode en cours
     */
    private Integer moisDeclare;

    /**
     * S21.G00.06.001 SIREN
     *
     * @param sirenEntreprise
     *            S21.G00.06.001 SIREN
     * @return S21.G00.06.001 SIREN
     */
    private String siren;

    /**
     * S21.G00.11.001 NIC
     *
     * @param nicEtablissement
     *            S21.G00.11.001 NIC
     * @return S21.G00.11.001 NIC
     */
    private String nic;

    /**
     * S21.G00.30.001
     *
     * @param identifiantRepertoire
     *            S21.G00.30.001
     * @return S21.G00.30.001
     */
    private String nir;

    /**
     * S21.G00.30.020
     *
     * @param numeroTechniqueTemporaire
     *            S21.G00.30.020
     * @return S21.G00.30.020
     */
    private String ntt;

    /**
     * S21.G00.30.019
     *
     * @param matricule
     *            S21.G00.30.019
     * @return S21.G00.30.019
     */
    private String numAdherent;

    /**
     * S21.G00.30.005
     *
     * @param sexe
     *            S21.G00.30.005
     * @return S21.G00.30.005
     */
    private String sexe;

    /**
     * S21.G00.30.002
     *
     * @param nomFamille
     *            S21.G00.30.002
     * @return S21.G00.30.002
     */
    private String nomFamille;

    /**
     * S21.G00.30.003
     *
     * @param nomUsage
     *            S21.G00.30.003
     * @return S21.G00.30.003
     */
    private String nomUsage;

    /**
     * S21.G00.30.004
     *
     * @param prenom
     *            S21.G00.30.004
     * @return S21.G00.30.004
     */
    private String prenom;

    /**
     * S21.G00.30.006
     *
     * @param dateNaissance
     *            S21.G00.30.006
     * @return S21.G00.30.006
     */
    private Integer dateNaissance;

    /**
     * S21.G00.40.001
     *
     * @param dateDebutContrat
     *            S21.G00.40.001
     * @return S21.G00.40.001
     */
    private Integer dateDebutContrat;

    /**
     * S21.G00.40.010
     *
     * @param dateFinPrevisionnelle
     *            S21.G00.40.010
     * @return S21.G00.40.010
     */
    private Integer dateFinPrevisionnelle;

    /**
     * S21.G00.40.006
     *
     * @param libelleEmploi
     *            S21.G00.40.006
     * @return S21.G00.40.006
     */
    private String libelleEmploi;

    /**
     * S21.G00.40.009
     *
     * @param numeroContratTravail
     *            S21.G00.40.009
     * @return S21.G00.40.009
     */
    private String numeroContrat;

    /**
     * Numéro de catégorie de quittancement.
     * 
     * @param numCategorieQuittancement
     *            Le numéro de la catégorie de quittancement
     * @return Le numéro de la catégorie de quittancement
     */
    private String numCategorieQuittancement;

    /**
     * Date de début de la base.
     * 
     * @param dateDebutBase
     *            La date de début de la base
     * @return La date de début de la base
     */
    private Integer dateDebutBase;

    /**
     * Date de fin de la base.
     * 
     * @param dateFinBase
     *            La date de fin de la base
     * @return La date de fin de la base
     */
    private Integer dateFinBase;

    /**
     * Nombre d'affiliés.
     * 
     * @param nombreAffilies
     *            Le nombre d'affiliés
     * @return Le nombre d'affiliés
     */
    private Integer nombreAffilies;

    /**
     * Ayants Droit Adulte.
     * 
     * @param ayantsDroitAdulte
     *            Les ayants droit adulte
     * @return Les ayants droit adulte
     */
    private Integer ayantsDroitAdulte;

    /**
     * Ayants Droit Autre.
     * 
     * @param ayantsDroitAutre
     *            Les ayants droit autre
     * @return Les ayants droit autre
     */
    private Integer ayantsDroitAutre;

    /**
     * Ayants Droit Enfant.
     * 
     * @param ayantsDroitEnfant
     *            Les ayants droit enfant
     * @return Les ayants droit enfant
     */
    private Integer ayantsDroitEnfant;

    /**
     * Effectif total.
     * 
     * @param totalEffectif
     *            L'effectif total
     * @return L'effectif total
     */
    private Long totalEffectif;

    /**
     * Constructeur par défaut
     */
    public ResumeDecompteEffectifExcel() {
        super();
    }
}
