package fr.si2m.red.reconciliation;

import java.util.List;
import java.util.Map;

import fr.si2m.red.core.repository.RedacRepository;

/**
 * Référentiel des historiques des commentaires des périodes.
 * 
 * @author gahagnont
 *
 */
public interface HistoriqueCommentairePeriodeRepository extends RedacRepository<HistoriqueCommentairePeriode> {

    /**
     * Récupère les commentaires d'une période.
     * 
     * @param idPeriode
     *            l'identifiant du commentaire de la période
     * @return les commentaires pour cette période
     */
    List<HistoriqueCommentairePeriode> getHistoriqueCommentairesPeriode(Long idPeriode);

    /**
     * Récupère les commentaires d'une liste de période.
     * 
     * @param Liste
     *            de idPeriode les identifiants du commentaire de la période
     * @return les commentaires pour cette période
     */
    List<HistoriqueCommentairePeriode> getHistoriqueCommentairesPeriodes(List<Long> idsPeriodes);

    /**
     * Récupère un commentaire donné d'une période.
     * 
     * @param idPeriode
     *            l'identifiant de la période
     * @param dateHeureSaisie
     *            l'horodatage du commentaire recherché
     * @return le commentaire recherché
     */
    HistoriqueCommentairePeriode getHistoriqueCommentairePeriodePourMessage(Long idPeriode, Long dateHeureSaisie);

    /**
     * Récupère le dernier commentaire d'une période donnée.
     * 
     * @param idPeriode
     *            l'identifiant de la période
     * @return le commentaide de la période concernée
     */
    HistoriqueCommentairePeriode getDernierCommentairePourPeriode(Long idPeriode);

    /**
     * Supprime les commentaires pour une période donnée.
     * 
     * @param idPeriode
     *            l'identifiant de la période
     * @return le nombre d'éléments supprimés effectivement dans le référentiel
     */
    int supprimeHistoriqueCommentairePourPeriode(Long idPeriode);

    /**
     * Exécute une procédure stockée MySQL.
     * 
     * @param sql
     *            la commande d'appel de la procédure
     * @param params
     *            les paramètres d'appels de la procédure
     */
    void callMysqlProcedure(String sql, Map<String, Object> params);

    /**
     * Creation d'historique état periode en masse
     * 
     * @param idsPeriodes
     *            liste des periodes ciblées
     * @param message
     *            message user
     * @param utilisateur
     *            user effectuant l'action
     * @param statutCible
     *            statut cible des periodes
     */
    void creationEnMasseOrigineIHM(List<Long> idsPeriodes, String message, String utilisateur);

}
