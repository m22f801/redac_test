package fr.si2m.red.reconciliation;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * Représentation d'une période pour l'export excel périodes
 * 
 * @author eudesr
 *
 */
@Data
@AllArgsConstructor
public class ExportPeriodeExcel implements Serializable {

    /**
     * UID auto généré
     */
    private static final long serialVersionUID = -8891010050598732349L;

    /**
     * L'id technique de la période
     */
    private Long idPeriode;

    /**
     * Le numéro du contrat auquel cette période est rattachée.
     * 
     * @param numeroContrat
     *            Le numéro du contrat complet
     * @return Le numéro du contrat complet
     */
    private String numeroContrat;

    /**
     * Date de début de la période.
     * 
     * @param dateDebutPeriode
     *            La date de début de période
     * @return La date de début de période
     */
    private Integer dateDebutPeriode;

    /**
     * Date de fin de la période.
     * 
     * @param dateFinPeriode
     *            La date de fin de période
     * @return La date de fin de période
     */
    private Integer dateFinPeriode;

    /**
     * Type de la période.
     * 
     * @param typePeriode
     *            Le type de la période
     * @return Le type de la période
     * 
     */
    private String typePeriode;

    /**
     * Etat de la période.
     * 
     * @param etatPeriode
     *            l'état de la période
     * @return l'état de la période
     * 
     */
    private String etatPeriode;

    /**
     * Le groupe de gestion.
     * 
     * @param groupeGestion
     *            le groupe de gestion
     * @return le groupe de gestion
     * 
     */
    private String groupeGestion;

    /**
     * Le numéro de la famille du produit.
     * 
     * @param numFamilleProduit
     *            le numéro de la famille du produit
     * @return le numéro de la famille du produit
     * 
     */
    private String numFamilleProduit;

    /**
     * Le numéro du compte producteur.
     * 
     * @param numCompteProducteur
     *            le numéro du compte producteur
     * @return le numéro du compte producteur
     * 
     */
    private String numCompteProducteur;

    /**
     * Le compte d'encaissement.
     * 
     * @param compteEncaissement
     *            le compte d'encaissement
     * @return le compte d'encaissement
     * 
     */
    private String compteEncaissement;

    /**
     * Le numéro du client (souscripteur du contrat) sur WCLI.
     * 
     * @param numSouscripteur
     *            le numéro du client (souscripteur du contrat) sur WCLI
     * @return le numéro du client (souscripteur du contrat) sur WCLI
     * 
     */
    private Long numSouscripteur;

    /**
     * Le numéro SIREN du client.
     * 
     * @param numSiren
     *            le numéro SIREN du client
     * @return le numéro SIREN du client
     */
    private String numSiren;

    /**
     * Le complément SIRET du client.
     * 
     * @param numSiret
     *            le complément SIRET du client
     * @return le complément SIRET du client
     */
    private String numSiret;

    /**
     * La raison sociale du client.
     * 
     * @param raisonSociale
     *            la raison sociale du client
     * @return la raison sociale du client
     */
    private String raisonSociale;

    /**
     * Indique si le contrat est VIP.
     * 
     * @param vipAsText
     *            Indique si le contrat est VIP
     * @return Indique si le contrat est VIP
     * 
     */
    private String vipAsText;

    /**
     * Le nombre d'établissements rattachés à la période
     */
    private Long nbEtablissementsRattaches;

    /**
     * Nombre de message de niveau signal et d'orige REDAC pour la période
     */
    private String signaltsRedac;

    /**
     * Nombre de message de niveau Rejet et d'orige REDAC pour la période
     */
    private String rejetsRedac;

    /**
     * Nombre de message de niveau signal et d'orige SIAVAL pour la période
     */
    private String signaltsSiAval;

    /**
     * Nombre de message de niveau Rejet et d'orige SIAVAL pour la période
     */
    private String rejetsSiAval;

    /**
     * La date du dernier changement de statut
     */
    private Long derniereDateChangementStatut;

    /**
     * La date de traitement
     * 
     * @param dateTraitement
     *            La date de traitement
     * @return La date de traitement
     */
    private Integer dateTraitement;

    /**
     * Mode de Gestion DSN.
     * 
     * @param modeGestionDSN
     *            Mode de Gestion DSN.
     * @return Mode de Gestion DSN.
     * 
     */
    private String modeGestionDSN;

    /**
     * Valeur actuelle ou à venir de l’indicateur Transfert DSN.
     * 
     * @param transfertDSN
     *            Valeur actuelle ou à venir de l’indicateur Transfert DSN
     * @return Valeur actuelle ou à venir de l’indicateur Transfert DSN
     * 
     */
    private String transfertDSN;

    /**
     * Nombre de salarié rataché à la période
     */
    private int nbSalariesRattaches;

    /**
     * Somme des montants de cotisation pour la période, sur CATEGORIE_QUITTANCEMENT_INDIVIDU
     */
    private Double sommeMontantCotisation;

    /**
     * Somme des montant versements des composants, pour la période.
     */
    private Double sommeComposantVersement;

    /**
     * Liste des modes de paiement distinct pour la période, concatené ( séparé par "," )
     */
    private String modesPaiementDistincts;
    /**
     * CodeUser du gestionnaire affecté au traitement des périodes
     */
    private String affecteA;
    /**
     * Reprise de la valeur du champ « Affecté à » venant d’être calculé
     */
    private String aTraiterPar;

    /**
     * CodeUser de l'utilisateur ayant effectué le traitement des périodes
     */
    private String traitePar;

    /**
     * Constructeur par défaut
     */
    public ExportPeriodeExcel() {
        super();
    }

}
