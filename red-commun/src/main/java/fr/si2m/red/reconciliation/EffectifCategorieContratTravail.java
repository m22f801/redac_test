package fr.si2m.red.reconciliation;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import fr.si2m.red.Entite;

/**
 * Modèle des contrats de travail pour une période, plage et catégorie précise.
 * 
 * @author poidij
 *
 */
@Entity
@Table(name = "EFFECTIF_CATEGORIE_CONTRAT_TRAVAIL")
@Data
@EqualsAndHashCode(callSuper = false, of = { "idTechnique" })
@ToString(callSuper = false, of = { "idTechnique", "idPeriode", "moisRattachement", "idContratTravail", "numCategorieQuittancement", "dateDebutBase" })
public class EffectifCategorieContratTravail extends Entite {
    /**
     * UID de version.
     */
    private static final long serialVersionUID = -3324801079594603125L;

    /**
     * L'identifiant technique.
     * 
     * @param idTechnique
     *            l'identifiant technique
     * @return l'identifiant technique
     */
    @Id
    @GeneratedValue
    @Column(name = "ID_TECH")
    private Long idTechnique;

    /**
     * L'identifiant technique de la période à laquelle ce message est rattaché.
     * 
     * @param idPeriode
     *            l'identifiant technique de la période
     * @return l'identifiant technique de la période
     */
    @Column(name = "ID_PERIODE")
    private Long idPeriode;

    /**
     * Date du mois de rattachement de la période déclarée pour cette adhésion. Clef de la déclaration rattachée.
     * 
     * @param moisRattachement
     *            La date du mois de rattachement de la période déclarée
     * @return La date du mois de rattachement de la période déclarée
     */
    @Column(name = "MOIS_RATTACHEMENT")
    private Integer moisRattachement;

    /**
     * Identifiant du contrat de travail.
     * 
     * @param idContratTravail
     *            L'identifiant du contrat de travail
     * @return L'identifiant du contrat de travail
     */
    @Column(name = "ID_CONTRAT_TRAVAIL")
    private String idContratTravail;

    /**
     * Numéro de catégorie de quittancement.
     * 
     * @param numCategorieQuittancement
     *            Le numéro de la catégorie de quittancement
     * @return Le numéro de la catégorie de quittancement
     */
    @Column(name = "NOCAT")
    private String numCategorieQuittancement;

    /**
     * Date de début de la base.
     * 
     * @param dateDebutBase
     *            La date de début de la base
     * @return La date de début de la base
     */
    @Column(name = "DEBUT_BASE")
    private Integer dateDebutBase;

    /**
     * Date de fin de la base.
     * 
     * @param dateFinBase
     *            La date de fin de la base
     * @return La date de fin de la base
     */
    @Column(name = "FIN_BASE")
    private Integer dateFinBase;

    /**
     * Nombre d'affiliés.
     * 
     * @param nombreAffilies
     *            Le nombre d'affiliés
     * @return Le nombre d'affiliés
     */
    @Column(name = "NOMBRE_AFFILIES")
    private Integer nombreAffilies;

    /**
     * Ayants Droit Adulte.
     * 
     * @param ayantsDroitAdulte
     *            Les ayants droit adulte
     * @return Les ayants droit adulte
     */
    @Column(name = "AYANTS_DROIT_ADULTE")
    private Integer ayantsDroitAdulte;

    /**
     * Ayants Droit Autre.
     * 
     * @param ayantsDroitAutre
     *            Les ayants droit autre
     * @return Les ayants droit autre
     */
    @Column(name = "AYANTS_DROIT_AUTRE")
    private Integer ayantsDroitAutre;

    /**
     * Ayants Droit Enfant.
     * 
     * @param ayantsDroitEnfant
     *            Les ayants droit enfant
     * @return Les ayants droit enfant
     */
    @Column(name = "AYANTS_DROIT_ENFANT")
    private Integer ayantsDroitEnfant;

    /**
     * Effectif total.
     * 
     * @param totalEffectif
     *            L'effectif total
     * @return L'effectif total
     */
    @Column(name = "TOTAL_EFFECTIF")
    private Long totalEffectif;

    /**
     * Montant du forfait.
     * 
     * @param montantForfait
     *            Le montant du forfait
     * @return Le montant du forfait
     */
    @Column(name = "MT_FORFAIT")
    private Double montantForfait;

    /**
     * Estimation de la cotisation.
     * 
     * @param estimationCotisation
     *            L'estimation de la cotisation
     * @return L'estimation de la cotisation
     */
    @Column(name = "ESTIMATION_COTISATION")
    private Double estimationCotisation;

    /**
     * Prorata de la cotisation.
     * 
     * @param prorataCotisation
     *            Le prorata de la cotisation
     * @return Le prorata de la cotisation
     */
    @Column(name = "PRORATA_COTISATION")
    private Double prorataCotisation;

    @Override
    public Long getId() {
        return idTechnique;
    }

}
