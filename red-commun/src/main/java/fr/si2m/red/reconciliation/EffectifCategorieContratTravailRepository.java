package fr.si2m.red.reconciliation;

import java.util.List;

/**
 * Référentiel des effectifs des categories de contrats de travail.
 * 
 * @author poidij
 *
 */
public interface EffectifCategorieContratTravailRepository extends ElementDeclaratifRepository<EffectifCategorieContratTravail> {

    /**
     * Récupère un contrat de travail sur une catégorie / plage via sa clé fonctionnelle.
     * 
     * @param idPeriode
     *            l'identifiant de la période
     * @param moisRattachement
     *            le mois de rattachement
     * @param idContratTravail
     *            l'identifiant du contrat de travail
     * @param numCategorieQuittancement
     *            le numéro de catégorie
     * @param dateDebutBase
     *            la date de début de base
     * @return le contrat de travail s'il existe, null sinon
     */
    EffectifCategorieContratTravail get(Long idPeriode, Integer moisRattachement, String idContratTravail, String numCategorieQuittancement, Integer dateDebutBase);

    /**
     * Récupère les contrats de travails par effectif pour une période et une catégorie de quittancement donnés.
     * 
     * @param idPeriode
     *            l'identifiant de la période
     * @param numCategorieQuittancement
     *            le numéro de catégorie
     * @return la liste des contrats de travails par effectif recherchés
     */
    List<EffectifCategorieContratTravail> getPourPeriodeEtCategorieQuittancement(Long idPeriode, String numCategorieQuittancement);

    /**
     * Récupère les contrats de travails par effectif (avec effectif non nul) pour une période et une catégorie de quittancement donnés.
     * 
     * @param idPeriode
     *            l'identifiant de la période
     * @param numCategorieQuittancement
     *            le numéro de catégorie
     * @return la liste des contrats de travails par effectif (avec effectif non nul) recherchés
     */
    List<EffectifCategorieContratTravail> getPourPeriodeEtCategorieQuittancementAvecTotalEffectifNonNul(Long idPeriode, String numCategorieQuittancement);
}
