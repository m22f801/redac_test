package fr.si2m.red.reconciliation;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import fr.si2m.red.DateRedac;
import fr.si2m.red.Entite;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * Modèle des historiques des assignations de période
 * 
 * @author gahagnont
 *
 */
@Entity
@Table(name = "HISTO_ASSIGNATION_PERIODE")
@Data
@EqualsAndHashCode(callSuper = false, of = { "idTechnique" })
@ToString(callSuper = false, of = { "idTechnique", "idPeriode", "dateHeureAssignation" })
public class HistoriqueAssignationPeriode extends Entite {
    /**
     * UID de version.
     */
    private static final long serialVersionUID = -3324801079594603125L;

    /**
     * identifiant controle modification via l'IHM
     */
    public static final String ORIGINE_CONTROLE_IHM = "IHM";

    /**
     * L'identifiant technique.
     * 
     * @param idTechnique
     *            l'identifiant technique
     * @return l'identifiant technique
     */
    @Id
    @GeneratedValue
    @Column(name = "ID_TECH")
    private Long idTechnique;

    /**
     * L'identifiant technique de la période à laquelle l'assignation est rattaché.
     * 
     * @param idPeriode
     *            l'identifiant technique de l'assignation de la période
     * @return l'identifiant technique de l'assignation de la période
     */
    @Column(name = "ID_PERIODE")
    private Long idPeriode;

    /**
     * Date / Heure / Minute / Seconde de l'assignation
     * 
     * @param dateHeureAssignation
     *            L'horodatage de l'assignation
     * @return L'horodatage de l'assignation
     */
    @Column(name = "DATEHMS_ASSIGNE")
    private Long dateHeureAssignation;

    /**
     * Identifiant de l'utilisateur assigné
     * 
     * @param aTraiterPar
     *            L'identifiant de l'utilisateur assigné
     * @return L'identifiant dl'utilisateur assigné
     */
    @Column(name = "A_TRAITER_PAR")
    private String aTraiterPar;

    /**
     * Récupère la date-heure REDAC de l'assignation en date-heure.
     * 
     * @return la date-heure de l'assignation
     */
    public String getDateHeureAssignationAsDate() {
        if (getDateHeureAssignation() != null) {
            return DateRedac.convertionDateRedacSansValidation(getDateHeureAssignation());
        }
        return null;
    }

    public String getRecupLibelle() {
        switch (aTraiterPar) {
        case "NON_ASSIGNE":
            return "Non assigné";

        default:
            return aTraiterPar;
        }
    }

    @Override
    public Long getId() {
        return idTechnique;
    }

}
