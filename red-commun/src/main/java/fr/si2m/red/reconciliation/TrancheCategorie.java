package fr.si2m.red.reconciliation;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import fr.si2m.red.Entite;

/**
 * Modèle des tranches de categories
 * 
 * @author poidij
 *
 */
@Entity
@Table(name = "TRANCHE_CATEGORIE")
@Data
@EqualsAndHashCode(callSuper = false, of = { "idTechnique" })
@ToString(callSuper = false, of = { "idTechnique", "idPeriode", "numCategorieQuittancement", "individu", "numTranche" })
public class TrancheCategorie extends Entite {
    /**
     * UID de version.
     */
    private static final long serialVersionUID = -3324801079594603125L;

    /**
     * L'identifiant technique.
     * 
     * @param idTechnique
     *            l'identifiant technique
     * @return l'identifiant technique
     */
    @Id
    @GeneratedValue
    @Column(name = "ID_TECH")
    private Long idTechnique;

    /**
     * L'identifiant technique de la période à laquelle ce message est rattaché.
     * 
     * @param idPeriode
     *            l'identifiant technique de la période
     * @return l'identifiant technique de la période
     */
    @Column(name = "ID_PERIODE")
    private Long idPeriode;

    /**
     * Numéro de catégorie de quittancement
     * 
     * @param numCategorieQuittancement
     *            Le numéro de la catégorie de quittancement
     * @return Le numéro de la catégorie de quittancement
     */
    @Column(name = "NOCAT")
    private String numCategorieQuittancement;

    /**
     * NIR/NTT
     * 
     * @param individu
     *            Le NIR/NTT
     * @return Le NIR/NTT
     */
    @Column(name = "INDIVIDU")
    private String individu;

    /**
     * Numéro de tranche
     * 
     * @param numTranche
     *            Le numéro de tranche
     * @return Le numéro de tranche
     */
    @Column(name = "NUM_TRANCHE")
    private Integer numTranche;

    /**
     * Libellé de la tranche
     * 
     * @param libelleTranche
     *            Le libellé de la tranche
     * @return Le libellé de la tranche
     */
    @Column(name = "LIB_TRANCHE")
    private String libelleTranche;

    /**
     * Montant de la tranche
     * 
     * @param montantTranche
     *            Le montant de la tranche
     * @return Le montant de la tranche
     */
    @Column(name = "MT_TRANCHE")
    private Double montantTranche;

    @Override
    public Long getId() {
        return idTechnique;
    }

}
