package fr.si2m.red.reconciliation;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import fr.si2m.red.Entite;

/**
 * @author poidij
 *
 */
@Entity
@Table(name = "RATTACHEMENT_DECLARATIONS_RECUES")
@Data
@EqualsAndHashCode(callSuper = false, of = { "idTechnique" })
@ToString(callSuper = false, of = { "idTechnique", "idPeriode", "idAdhEtabMois" })
public class RattachementDeclarationsRecues extends Entite {
    /**
     * UID de version.
     */
    private static final long serialVersionUID = -3324801079594603125L;

    /**
     * L'identifiant technique.
     * 
     * @param idTechnique
     *            l'identifiant technique
     * @return l'identifiant technique
     */
    @Id
    @GeneratedValue
    @Column(name = "ID_TECH")
    private Long idTechnique;

    /**
     * L'identifiant technique de la période à laquelle ce message est rattaché.
     * 
     * @param idPeriode
     *            l'identifiant technique de la période
     * @return l'identifiant technique de la période
     */
    @Column(name = "ID_PERIODE")
    private Long idPeriode;

    /**
     * L'identifiant technique unique pour chaque ligne de l’ensemble des différents fichiers publiés ADHESION_ETAB_MOIS
     * 
     * @param idAdhEtabMois
     *            L'identifiant technique unique pour chaque ligne de ADHESION_ETAB_MOIS
     * @return L'identifiant technique unique pour chaque ligne de ADHESION_ETAB_MOIS
     */
    @Column(name = "ID_ADH_ETAB_MOIS")
    private String idAdhEtabMois;

    @Override
    public Long getId() {
        return idTechnique;
    }

}
