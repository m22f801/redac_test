package fr.si2m.red.reconciliation;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * Représentation d'une période pour l'export excel périodes
 * 
 * @author eudesr
 *
 */
@Data
@AllArgsConstructor
public class ExportDsnNonTraiteesExcel implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 7202090923650626269L;

    /**
     * L'id technique de la période
     */
    private Double idAdhEtabMois;

    /**
     * Le numéro du contrat auquel cette période est rattachée.
     * 
     * @param numeroContrat
     *            Le numéro du contrat complet
     * @return Le numéro du contrat complet
     */
    private String numeroContrat;

    /**
     * Le numéro SIREN emetteur.
     * 
     * @param numSiren
     *            le numéro SIREN du client
     * @return le numéro SIREN du client
     */
    private String sirenEmetteur;

    /**
     * Le numéro NIC emetteur.
     * 
     * @param numSiren
     *            le numéro SIREN du client
     * @return le numéro SIREN du client
     */
    private String nicEmetteur;

    /**
     * Le numéro nom emetteur.
     * 
     * @param numSiren
     *            le numéro SIREN du client
     * @return le numéro SIREN du client
     */
    private String nomEmetteur;

    /**
     * Le numéro siren entreprise.
     * 
     * @param numSiren
     *            le numéro SIREN du client
     * @return le numéro SIREN du client
     */
    private String sirenEntreprise;

    /**
     * Le numéro nic entreprise.
     * 
     * @param numSiren
     *            le numéro SIREN du client
     * @return le numéro SIREN du client
     */
    private String nicEntreprise;

    /**
     * La raisonSocialeEntreprise entreprise.
     * 
     * @param numSiren
     *            le numéro SIREN du client
     * @return le numéro SIREN du client
     */
    private String raisonSocialeEntreprise;

    /**
     * Le numéro nic etablissement.
     * 
     * @param numSiren
     *            le numéro SIREN du client
     * @return le numéro SIREN du client
     */
    private String nicEtablissement;

    /**
     * Enseigne etablissement.
     * 
     * @param numSiren
     *            le numéro SIREN du client
     * @return le numéro SIREN du client
     */
    private String enseigneEtablissement;

    /**
     * Enseigne etablissement.
     * 
     * @param numSiren
     *            le numéro SIREN du client
     * @return le numéro SIREN du client
     */
    private String moisDeclare;

    /**
     * Enseigne etablissement.
     * 
     * @param numSiren
     *            le numéro SIREN du client
     * @return le numéro SIREN du client
     */
    private String moisRattachement;

    /**
     * Enseigne etablissement.
     * 
     * @param numSiren
     *            le numéro SIREN du client
     * @return le numéro SIREN du client
     */
    private String dateFichier;

    /**
     * Enseigne etablissement.
     * 
     * @param numSiren
     *            le numéro SIREN du client
     * @return le numéro SIREN du client
     */
    private String dtCreation;

    /**
     * Enseigne etablissement.
     * 
     * @param numSiren
     *            le numéro SIREN du client
     * @return le numéro SIREN du client
     */
    private String dtDebutSituation;

    /**
     * Enseigne etablissement.
     * 
     * @param numSiren
     *            le numéro SIREN du client
     * @return le numéro SIREN du client
     */
    private String famille;

    /**
     * Enseigne etablissement.
     * 
     * @param numSiren
     *            le numéro SIREN du client
     * @return le numéro SIREN du client
     */
    private String groupeGestion;

    /**
     * Enseigne etablissement.
     * 
     * @param numSiren
     *            le numéro SIREN du client
     * @return le numéro SIREN du client
     */
    private String etat;

    /**
     * Enseigne etablissement.
     * 
     * @param numSiren
     *            le numéro SIREN du client
     * @return le numéro SIREN du client
     */
    private String cptProd;

    /**
     * Enseigne etablissement.
     * 
     * @param numSiren
     *            le numéro SIREN du client
     * @return le numéro SIREN du client
     */
    private String cptEnc;

    /**
     * S21.G00.30.020
     *
     * @param sommeMontantCotisation
     *            S21.G00.30.020
     * @return S21.G00.30.020
     */
    private Double sommeMontantsCotisation;

    /**
     * S21.G00.30.020
     *
     * @param sommeMontantCotisation
     *            S21.G00.30.020
     * @return S21.G00.30.020
     */
    private Double sommeMontantsVersement;

    /**
     * Enseigne etablissement.
     * 
     * @param numSiren
     *            le numéro SIREN du client
     * @return le numéro SIREN du client
     */
    private String modePaiement;

    /**
     * Enseigne etablissement.
     * 
     * @param numSiren
     *            le numéro SIREN du client
     * @return le numéro SIREN du client
     */
    private Double nbIndividu;

    /**
     * Le groupe de gestion.
     * 
     * @param groupeGestion
     *            le groupe de gestion
     * @return le groupe de gestion
     * 
     */

    /**
     * La présence du flag VIP recherchée.
     * 
     * @param vip
     *            la présence du flag VIP recherchée
     */
    public String formatDateMoisAnneeJour(String moisAnneeJour) {

        String dateAnneeMoisJour = moisAnneeJour.substring(6, 7) + moisAnneeJour.substring(7, 8) + "/" + moisAnneeJour.substring(4, 5)
                + moisAnneeJour.substring(5, 6) + "/" + moisAnneeJour.substring(0, 1) + moisAnneeJour.substring(1, 2) + moisAnneeJour.substring(2, 3)
                + moisAnneeJour.substring(3, 4);

        return dateAnneeMoisJour;
    }

    /**
     * La présence du flag VIP recherchée.
     * 
     * @param vip
     *            la présence du flag VIP recherchée
     */
    public String formatDateCreation(String moisAnneeJour) {

        String dateAnneeMoisJour = moisAnneeJour.substring(8, 9) + moisAnneeJour.substring(9, 10) + "/" + moisAnneeJour.substring(5, 6)
                + moisAnneeJour.substring(6, 7) + "/" + moisAnneeJour.substring(0, 1) + moisAnneeJour.substring(1, 2) + moisAnneeJour.substring(2, 3)
                + moisAnneeJour.substring(3, 4);

        return dateAnneeMoisJour;
    }

    /**
     * La présence du flag VIP recherchée.
     * 
     * @param vip
     *            la présence du flag VIP recherchée
     */
    public String formatDateMoisAnnee(String moisAnnee) {

        String dateAnneeMois = moisAnnee.substring(4, 5) + moisAnnee.substring(5, 6) + "/" + moisAnnee.substring(0, 1) + moisAnnee.substring(1, 2)
                + moisAnnee.substring(2, 3) + moisAnnee.substring(3, 4);

        return dateAnneeMois;
    }

    /**
     * Constructeur par défaut
     */
    public ExportDsnNonTraiteesExcel() {
        super();
    }

}
