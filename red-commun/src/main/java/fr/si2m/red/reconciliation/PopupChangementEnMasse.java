package fr.si2m.red.reconciliation;

import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.beanutils.BeanUtils;
import org.springframework.util.MultiValueMap;

import fr.si2m.red.RedacUnexpectedException;
import lombok.Getter;
import lombok.Setter;

/**
 * Changements en masse (popup)
 * 
 * @author eudesr
 *
 */
public class PopupChangementEnMasse implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 7104970205456924460L;

    /**
     * Sauvegarde des derniers critères de recherches validés
     */
    @Getter
    @Setter
    String parametresRecherche;

    /**
     * Liste des ID périodes à modifier, concaténés et séparé par une virgule, si cases cochées
     */
    @Getter
    @Setter
    String idsPeriodes;

    /**
     * Nombre de périodes impactées
     */
    @Getter
    @Setter
    Integer nbPeriodes;

    /**
     * Représentation en List-Long- des idsPeriodes , toujours utiliser getIdsPeriodesAsLong() pour init / acceder à la liste
     */
    private List<Long> idsPeriodesAsLong;

    /**
     * blocage/déblocage période
     */
    @Getter
    @Setter
    String bloquerPeriodesEnMasse;

    /**
     * Mise en attente, enlever attente retour entreprise
     */
    @Getter
    @Setter
    String attenteRetourEntrepriseEnMasse;

    /**
     * Gestionnaire à affecter en masse au traitement des périodes (Selection liste déroulante "Affecté à")
     */
    @Getter
    @Setter
    String gestionnaireAffecteAEnMasse;

    /**
     * Gestionnaire à affecter en masse au traitement des périodes (Selection liste déroulante "A traiter par")
     */
    @Getter
    @Setter
    String gestionnaireATraiterParEnMasse;

    /**
     * Statut de la période à setter aux périodes séléctionner
     */
    @Getter
    @Setter
    String codeLibelleChangementDeStatutEnMasse;

    /**
     * Commentaire changement en masse
     */
    @Getter
    @Setter
    String commentaireEnMasse;

    /**
     * Constructeur par défaut des changements
     */
    public PopupChangementEnMasse() {
        super();
    }

    /**
     * Constructeur de changements par copie
     * 
     * @param changements
     *            les changements à copier
     */
    public PopupChangementEnMasse(PopupChangementEnMasse changements) {
        this();
        try {
            BeanUtils.copyProperties(this, changements);
        } catch (IllegalAccessException | InvocationTargetException e) {
            throw new RedacUnexpectedException("Erreur lors de la copie des changements demandés", e);
        }
    }

    /**
     * Convertit une map de valeurs multiple en changements.
     * 
     * @param criteres
     *            les critères sous forme de map de valeurs multiples
     * @return les changements
     */
    public static PopupChangementEnMasse from(MultiValueMap<String, Object> criteres) {
        PopupChangementEnMasse changements = new PopupChangementEnMasse();
        try {
            BeanUtils.populate(changements, criteres);
        } catch (IllegalAccessException | InvocationTargetException e) {
            throw new RedacUnexpectedException("Erreur lors du mapping des changements", e);
        }

        return changements;
    }

    /**
     * Initialise et renvoie la liste des IdsPeriodes sous forme de List-Long-
     * 
     * @return la liste des IdsPeriodes comme List-Long-
     */
    public List<Long> getIdsPeriodesAsLong() {

        if (idsPeriodesAsLong == null) {
            setIdsPeriodesAsLong();
        }
        return idsPeriodesAsLong;
    }

    /**
     * Initialise la liste
     */
    private void setIdsPeriodesAsLong() {
        idsPeriodesAsLong = new ArrayList<Long>();
        if (idsPeriodes != null) {
            for (String idPeriode : Arrays.asList(idsPeriodes.split(","))) {
                idsPeriodesAsLong.add(Long.parseLong(idPeriode));
            }
        }

    }

}
