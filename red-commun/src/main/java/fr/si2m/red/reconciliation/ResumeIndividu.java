package fr.si2m.red.reconciliation;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.ToString;

/**
 * Resume individu (numéro technique temporaire ou identifiant de répertoire avec nom, prénom, matricule )
 * 
 * @author eudesr
 *
 */
@Data
@ToString(of = { "matricule", "nomUsage", "prenom", "identifiantRepertoire", "numeroTechniqueTemporaire" })
@AllArgsConstructor
public class ResumeIndividu implements Serializable {

    /**
     * UID de version.
     */
    private static final long serialVersionUID = -8848531146026984960L;

    /**
     * S21.G00.30.019
     *
     * @param matricule
     *            S21.G00.30.019
     * @return S21.G00.30.019
     */
    private String matricule;

    /**
     * S21.G00.30.003
     *
     * @param nomUsage
     *            S21.G00.30.003
     * @return S21.G00.30.003
     */
    private String nomUsage;

    /**
     * S21.G00.30.004
     *
     * @param prenom
     *            S21.G00.30.004
     * @return S21.G00.30.004
     */
    private String prenom;

    /**
     * S21.G00.30.001
     *
     * @param identifiantRepertoire
     *            S21.G00.30.001
     * @return S21.G00.30.001
     */
    private String identifiantRepertoire;

    /**
     * S21.G00.30.020
     *
     * @param numeroTechniqueTemporaire
     *            S21.G00.30.020
     * @return S21.G00.30.020
     */
    private String numeroTechniqueTemporaire;

    /**
     * Constructeur par défaut
     */
    public ResumeIndividu() {
        super();
    }

}
