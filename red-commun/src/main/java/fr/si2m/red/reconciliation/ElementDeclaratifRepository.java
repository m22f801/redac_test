package fr.si2m.red.reconciliation;

import java.util.List;

import fr.si2m.red.Entite;
import fr.si2m.red.core.repository.RedacRepository;

/**
 * Référentiel d'éléments déclaratifs.
 * 
 * @author nortaina
 *
 * @param <T>
 */
public interface ElementDeclaratifRepository<T extends Entite> extends RedacRepository<T> {

    /**
     * Supprime tous les éléments déclaratifs liées à une période reçue en particulier.
     * 
     * @param idPeriode
     *            l'identifiant de la période reçue
     */
    void supprimePourPeriode(Long idPeriode);

    /**
     * Récupère les éléments déclaratifs pour une période donnée.
     * 
     * @param idPeriode
     *            l'ID de la période
     * @return les éléments déclaratifs liés
     */
    List<T> getPourPeriode(Long idPeriode);

    /**
     * Récupère les éléments déclaratifs pour une période donnée de façon paginée.
     * 
     * @param idPeriode
     *            l'ID de la période
     * @param indexPremierResultat
     *            l'index du premier résultat (commençant à 0), 0 si null
     * @param taillePage
     *            la taille maximale d'une page, tous les résultats à partir du premier si null
     * @return les éléments déclaratifs liés
     */
    List<ResumeDecompteEffectifExcel> getDecomptesEffectifPourPeriodePagines(Long idPeriode, Integer indexPremierResultat, Integer taillePage);

    /**
     * Récupère les éléments déclaratifs pour une période donnée de façon paginée.
     * 
     * @param idPeriode
     *            l'ID de la période
     * @param indexPremierResultat
     *            l'index du premier résultat (commençant à 0), 0 si null
     * @param taillePage
     *            la taille maximale d'une page, tous les résultats à partir du premier si null
     * @return les éléments déclaratifs liés
     */
    List<T> getPourPeriodePagines(Long idPeriode, Integer indexPremierResultat, Integer taillePage);

}
