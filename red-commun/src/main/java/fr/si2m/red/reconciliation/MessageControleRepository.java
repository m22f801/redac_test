package fr.si2m.red.reconciliation;

import java.util.List;
import java.util.Map;

import fr.si2m.red.core.repository.RedacRepository;
import fr.si2m.red.dsn.MessageControleExportExcel;
import fr.si2m.red.parametrage.ParamControleSignalRejet;

/**
 * Référentiel des message de contrôle DSN.
 * 
 * @author poidij
 *
 */
public interface MessageControleRepository extends RedacRepository<MessageControle> {

    /**
     * Supprime les messages générés par REDAC pour une période donnée.
     * 
     * @param idPeriode
     *            l'ID de la période
     * @return le nombre de messages REDAC supprimés
     */
    int supprimeMessagesRedacPourPeriode(Long idPeriode);

    /**
     * Test pour savoir si au moins un message de rejet existe dans la table MessageControle à partir de l'identifiant de sa période.
     * 
     * @param idPeriode
     *            l'identifiant de la période
     * @return true si le message existe et false sinon
     */
    boolean existeMessageNiveauRejet(Long idPeriode);

    /**
     * Supprime les messages générés par un SIAVAL pour une période donnée.
     * 
     * @param idPeriode
     *            l'ID de la période
     * @return le nombre de messages SIAVAL supprimés
     */
    int supprimeMessagesSIAvalPourPeriode(Long idPeriode);

    /**
     * Récupère un message pour une période et un contrôle donnés.
     * 
     * @param idPeriode
     *            l'identifiant de période
     * @param identifiantControle
     *            l'identifiant du contrôle
     * @return le message pour la période et le contrôle donnés s'il existe, null sinon
     */
    MessageControle getPourPeriodeEtIdentifiantControle(Long idPeriode, String identifiantControle);

    /**
     * Récupère un éventuel message de période d'affectation invalide pour un certain versement rattaché à une période donnée.
     * 
     * @param idPeriode
     *            l'identifiant de période
     * @param periodeAffectation
     *            l'identifiant de la période d'affectation éventuellementinvalide
     * @return le message s'il existe pour la période d'affectation donnée, null sinon
     */
    MessageControle getMessagesPeriodesAffectationInvalidesPourPeriode(Long idPeriode, String periodeAffectation);

    /**
     * Récupère l'ensemble des messages de contrôle pour une période donnée ne ciblant pas d'individu précis.
     * 
     * @param idPeriode
     *            l'identifiant de période
     * @return les messages liés à la période ne ciblant pas d'individu précis
     */
    List<MessageControle> getGenerauxPourPeriode(Long idPeriode);

    /**
     * Récupère l'ensemble des messages de contrôle pour une période donnée.
     * 
     * @param idPeriode
     *            l'identifiant de période
     * @return les messages liés à la période
     */
    List<MessageControle> getPourPeriode(Long idPeriode);

    /**
     * Récupère les messages de contrôle d'un certain niveau pour une période donnée.
     * 
     * @param idPeriode
     *            l'identifiant de période
     * @param niveauAlerte
     *            le niveau d'alerte
     * @return les messages liés à la période
     */
    List<MessageControle> getPourPeriodeEtNiveauAlerte(Long idPeriode, String niveauAlerte);

    /**
     * Compte le nombre de messages rattachés à une période pour quelques critères donnés.
     * 
     * @param idPeriode
     *            l'identifiant de la période à laquelle les messages sont liés
     * @param niveauAlerte
     *            le niveau d'alerte des message
     * @param origineMessage
     *            l'origine des messages
     * @return le nombre de messages correspondant aux critères
     */
    Long comptePourPeriode(Long idPeriode, String niveauAlerte, String origineMessage);

    /**
     * Récupère les messages de contrôle d'une période pour un individu précis.
     * 
     * @param idPeriode
     *            l'identifiant de la période
     * @param individu
     *            l'individu (NIR ou NTT)
     * @return les messages de contrôle de la période pour l'individu
     */
    List<MessageControle> getPourPeriodeEtIndividu(Long idPeriode, String individu);

    /**
     * Récupère les messages de contrôle d'une période pour un individu précis.
     * 
     * @param idPeriode
     *            l'identifiant de la période
     * @param individu
     *            l'individu
     * @param niveauAlerte
     *            le niveau d'alerte des message
     * @return les messages de contrôle de la période pour l'individu
     */
    List<MessageControle> getPourPeriodeEtIndividuEtNiveauAlerte(Long idPeriode, ExportIndividuExcel individu, String niveauAlerte);

    /**
     * Test pour savoir si au moins un message REJET, pour la base assujettie existe dans la table MessageControle à partir de l'identifiant de sa période.
     * 
     * @param idPeriode
     *            l'identifiant de la période
     * @return true si un message existe et false sinon
     */
    boolean existeMessageRejetPourBaseAssujettiePourPeriode(Long idPeriode);

    /**
     * Test pour savoir si au moins un message de rejet existe dans la table MessageControle à partir de l'identifiant de sa période et de l'identifiant de
     * controle du message pour une origine = GEST
     * 
     * @param idPeriode
     *            l'identifiant de la période
     * @param idControle
     *            l'identifiant de controle du message
     * @return true si le message existe et false sinon
     */
    boolean existeMessageGestPourPeriodeEtControle(Long idPeriode, String idControle);

    /**
     * supprime les messages de rejet dans la table MessageControle à partir de l'identifiant de sa période et de l'identifiant de controle du message pour une
     * origine = GEST
     * 
     * @param idPeriode
     *            l'identifiant de la période
     * @param idControle
     *            l'identifiant de contrôle du message
     * @return le nombre de messages supprimés
     */
    int supprimeMessageGestPourPeriodeEtControle(Long idPeriode, String idControle);

    /**
     * supprime les messages dans la table MessageControle à partir de l'identifiant de sa période
     * 
     * @param idPeriode
     *            l'identifiant de la période
     * @return le nombre de messages supprimés
     */
    int supprimeMessageControlePourPeriode(Long idPeriode);

    /**
     * Récupère un message pour une période , un contrôle et une origine donnés.
     * 
     * @param idPeriode
     *            l'identifiant de période
     * @param identifiantControle
     *            l'identifiant du contrôle
     * @param origine
     *            l'origine du message
     * @return le message pour la période , le contrôle et l'origine donnés s'il existe, null sinon
     */
    MessageControle getPourPeriodeEtIdentifiantControleEtOrigine(Long idPeriode, String identifiantControle, String origine);

    /**
     * Retourne les messages controle signal et rejets concaténés pour un individu, pour une période
     * 
     * @param idPeriode
     *            l'identifiant de la période
     * @param individu
     *            l'identifiant de l'individu ( nir si renseigné, ntt sinon )
     * @return la concaténation des messages SIGNAL et REJET pour l'individu, pour la période
     */
    MessageControleExportExcel getMessagesSignalRejetPourIndividu(Long idPeriode, String individu);

    /**
     * Supprime les messages générés pour une période donnée , sauf ceux de l'origine spécifiée
     * 
     * @param idPeriode
     *            l'ID de la période
     * @param origine
     *            l'origine du message
     * @return le nombre de messages supprimés
     */
    int supprimeMessagesSaufOriginePourPeriode(Long idPeriode, String origine);

    /**
     * Exécute une procédure stockée MySQL.
     * 
     * @param sql
     *            la commande d'appel de la procédure
     * @param params
     *            les paramètres d'appels de la procédure
     */
    void callMysqlProcedure(String sql, Map<String, Object> params);

    /**
     * Mise en attente des périodes en masse
     * 
     * @param idsPeriodes
     *            liste des idsPeriodes ciblés
     * @param paramControleSignalRejet
     *            le paramControleSignalRejet récupéré par F09_RG_6_11
     * @param user
     *            l'user effectuant l'action
     */
    void miseEnAttenteEnMasse(List<Long> idsPeriodes, ParamControleSignalRejet paramControleSignalRejet, String user);

    /**
     * Levée de la mise en attente des périodes en masse
     * 
     * @param idsPeriodes
     *            liste des idsPeriodes ciblés
     * 
     * @return le nombre d'élement supprimés
     */
    int leveeMiseEnAttenteEnMasse(List<Long> idsPeriodes);

    /**
     * Blocage des périodes en masse
     * 
     * @param idsPeriodes
     *            liste des idsPeriodes ciblés
     * @param paramControleSignalRejet
     *            le mparamControleSignalRejet récupéré par F09_RG_6_06
     * @param user
     *            l'user effectuant l'action
     */
    void blocageEnMasse(List<Long> idsPeriodes, ParamControleSignalRejet paramControleSignalRejet, String user);

    /**
     * Déblocage des périodes en masse
     * 
     * @param idsPeriodes
     *            la liste des idPeriodes ciblés
     * @return le nombre d'élement supprimés
     */
    int deblocageEnMasse(List<Long> idsPeriodes);

    /**
     * Supression des messages controles en masse
     * 
     * @param idsPeriodes
     *            la liste des idPeriodes ciblés
     * @param origine
     *            origine des messages à supprimer
     * @param idControle
     *            ID_CONTROLE des messages à supprimer
     */
    int supressionMessageControleEnMasse(List<Long> idsPeriodes, String origine, String idControle);

}
