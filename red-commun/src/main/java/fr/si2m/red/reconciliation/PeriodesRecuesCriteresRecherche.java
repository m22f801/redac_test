package fr.si2m.red.reconciliation;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.util.MultiValueMap;

import fr.si2m.red.RedacUnexpectedException;
import fr.si2m.red.core.repository.CriteresRecherche;
import fr.si2m.red.core.repository.IgnorePourQueryString;
import lombok.Getter;
import lombok.Setter;

/**
 * Critères de recherche de périodes reçues.
 * 
 * @author nortaina
 *
 */
public class PeriodesRecuesCriteresRecherche extends CriteresRecherche {
    /**
     * UID de version.
     */
    private static final long serialVersionUID = 4848566597642428202L;

    /**
     * Indicateur de tri spécifique.
     */
    public static final String TRI_SPECIFIQUE = "spec";

    /**
     * Autre gestionnaire code : "AUTRE_GEST"
     */
    public static final String AUTRE_GEST_CODE = "AUTRE_GEST";
    /**
     * Non affecté code :"NON_AFFECTE"
     */
    public static final String NON_AFFECTE_CODE = "NON_AFFECTE";
    /**
     * Traite auto code : "TRAITE_AUTO"
     */
    public static final String TRAITE_AUTO_CODE = "TRAITE_AUTO";
    /**
     * Non assigné code : "NON_ASSIGNE"
     */
    public static final String NON_ASSIGNE_CODE = "NON_ASSIGNE";

    /**
     * Autre gestionnaire libellé : "Autre gestionnaire"
     */
    public static final String AUTRE_GEST_LIBELLE = "Autre gestionnaire";
    /**
     * Non affecté libellé : "Non affecté"
     */
    public static final String NON_AFFECTE_LIBELLE = "Non affecté";
    /**
     * Traite auto libellé : "Traité automatiquement"
     */
    public static final String TRAITE_AUTO_LIBELLE = "Traité automatiquement";
    /**
     * Non assigné libellé : "Non assigné"
     */
    public static final String NON_ASSIGNE_LIBELLE = "Non assigné";

    /**
     * Les statuts des périodes recherchées.
     * 
     */
    private String[] statuts;

    /**
     * Les groupes de gestion recherchés.
     * 
     */
    private String[] groupesGestion;

    /**
     * Le numéro de client recherché.
     * 
     * @param numClient
     *            le numéro de client recherché
     * @return le numéro de client recherché
     */
    @Getter
    @Setter
    private String numClient;

    /**
     * La présence de types de messages de contrôle recherchée.
     * 
     */
    private String[] messages;

    /**
     * Les familles de contrat recherchées.
     * 
     */
    private String[] familles;

    /**
     * Le numéro de SIREN recherché.
     * 
     * @param siren
     *            le numéro de SIREN recherché
     * @return le numéro de SIREN recherché
     */
    @Getter
    @Setter
    private String siren;

    /**
     * Les idControles des messages controles.
     * 
     */
    private String[] idControles;

    /**
     * Les types de période recherchés.
     * 
     */
    private String[] typesPeriode;

    /**
     * Les trimestres recherchés.
     * 
     */
    private String[] trimestres;

    /**
     * Le NIC recherché.
     * 
     * @param nic
     *            le NIC recherché
     * @return le NIC recherché
     */
    @Getter
    @Setter
    private String nic;

    /**
     * Le numéro de contrat recherché.
     * 
     * @param numContrat
     *            le numéro de contrat recherché
     * @return le numéro de contrat recherché
     */
    @Getter
    @Setter
    private String numContrat;

    /**
     * Le compte d'encaissement recherché.
     * 
     * @param cptEnc
     *            le compte d'encaissement recherché
     * @return le compte d'encaissement recherché
     */
    @Getter
    @Setter
    private String cptEnc;

    /**
     * La raison sociale recherchée.
     * 
     * @param raisonSociale
     *            la raison sociale recherchée
     * @return la raison sociale recherchée
     */
    @Getter
    @Setter
    private String raisonSociale;

    /**
     * La présence de versements recherchée.
     * 
     */
    private Boolean[] versements;

    /**
     * Le numéro du compte producteur du contrat recherché.
     * 
     * @param cptProd
     *            le numéro du compte producteur du contrat recherché
     * @return le numéro du compte producteur du contrat recherché
     */
    @Getter
    @Setter
    private String cptProd;

    /**
     * Affectation des periodes reçues à un utilisateur
     * 
     * @param affecteA
     *            Reprise de la valeur du champ « Affecté à » de la période antérieure Si période antérieure n’existe pas : laissé à vide
     * @return L'utilisateur affecté au traitement
     */
    private String[] affecteA;

    /**
     * Affectation du gestionnaire qui doit traiter les champs.
     * 
     * @param aTraiterPar
     * 
     * @return L'utilisateur qui doit traiter les périodes reçues
     */

    private String[] aTraiterPar;
    /**
     * Affectation de l'utilisateur qui doit traiter les champs
     * 
     * @param traitePar
     *            Champs vide
     * @return L'utilisateur qui a traité les périodes reçues
     */
    private String[] traitePar;

    /**
     * La présence de flag VIP recherchée.
     * 
     */
    private String[] vip;

    /**
     * Le filtre sur numéros de contrats VIP si {@link #getVip()} spécifie une recherche sur contrat VIP.
     * 
     */
    @Getter
    @Setter
    @IgnorePourQueryString
    private List<String> filtreContratsVIP = new ArrayList<>();

    /**
     * Constructeur par défaut des critères.
     */
    public PeriodesRecuesCriteresRecherche() {
        super();
    }

    /**
     * Constructeur de critères par copie de critères donnés.
     * 
     * @param periodesRecuesCriteresRecherche
     *            les critères à copier
     */
    public PeriodesRecuesCriteresRecherche(PeriodesRecuesCriteresRecherche periodesRecuesCriteresRecherche) {
        this();
        try {
            BeanUtils.copyProperties(this, periodesRecuesCriteresRecherche);
        } catch (IllegalAccessException | InvocationTargetException e) {
            throw new RedacUnexpectedException("Erreur lors de la copie de critères de recherche", e);
        }
    }

    /**
     * Les statuts de déclarations recherchés.
     * 
     * @return les statuts de déclarations recherchés
     */
    public String[] getStatuts() {
        if (statuts == null) {
            return new String[0];
        } else {
            return Arrays.copyOf(statuts, statuts.length);
        }
    }

    /**
     * Les statuts de déclarations recherchés.
     * 
     * @return les statuts de déclarations recherchés
     */
    public List<String> getStatutsAsList() {
        if (statuts == null) {
            return new ArrayList<>();
        } else {
            return Arrays.asList(this.statuts);
        }
    }

    /**
     * Les statuts de déclarations recherchés.
     * 
     * @param statuts
     *            les statuts de déclarations recherchés
     */
    public void setStatuts(String[] statuts) {
        if (statuts == null) {
            this.statuts = null;
        } else {
            this.statuts = Arrays.copyOf(statuts, statuts.length);
        }
    }

    /**
     * Les groupes de gestion recherchés.
     * 
     * @return les groupes de gestion recherchés
     */
    public String[] getGroupesGestion() {
        if (groupesGestion == null) {
            return new String[0];
        } else {
            return Arrays.copyOf(groupesGestion, groupesGestion.length);
        }
    }

    /**
     * Les groupes de gestion recherchés sous forme de liste.
     * 
     * @return les groupes de gestion recherchés
     */
    public List<String> getGroupesGestionAsList() {
        if (groupesGestion == null) {
            return new ArrayList<String>();
        } else {
            return Arrays.asList(this.groupesGestion);
        }
    }

    /**
     * Les groupes de gestion recherchés.
     * 
     * @param groupesGestion
     *            les groupes de gestion recherchés
     */
    public void setGroupesGestion(String[] groupesGestion) {
        if (groupesGestion == null) {
            this.groupesGestion = null;
        } else {
            this.groupesGestion = Arrays.copyOf(groupesGestion, groupesGestion.length);
        }
    }

    /**
     * La présence de types de messages de contrôle recherchée.
     * 
     * @return les groupes de gestion recherchés
     */
    public String[] getMessages() {
        if (messages == null) {
            return new String[0];
        } else {
            return Arrays.copyOf(messages, messages.length);
        }
    }

    /**
     * La présence de types de messages de contrôle recherchée.
     * 
     * @return la présence de types de messages de contrôle recherchée
     */
    public List<String> getMessagesAsList() {
        if (messages == null) {
            return new ArrayList<>();
        } else {
            return Arrays.asList(this.messages);
        }
    }

    /**
     * La présence de types de messages de contrôle recherchée.
     * 
     * @param messages
     *            la présence de types de messages de contrôle recherchée
     */
    public void setMessages(String[] messages) {
        if (messages == null) {
            this.messages = null;
        } else {
            this.messages = Arrays.copyOf(messages, messages.length);
        }
    }

    /**
     * Les familles de contrat recherchées.
     * 
     * @return les familles de contrat recherchées
     */
    public String[] getFamilles() {
        if (familles == null) {
            return new String[0];
        } else {
            return Arrays.copyOf(familles, familles.length);
        }
    }

    /**
     * Les familles de contrat recherchées.
     * 
     * @return les familles de contrat recherchées
     */
    public List<String> getFamillesAsList() {
        if (familles == null) {
            return new ArrayList<>();
        } else {
            return Arrays.asList(this.familles);
        }
    }

    /**
     * Les familles de contrat recherchées.
     * 
     * @param familles
     *            les familles de contrat recherchées
     */
    public void setFamilles(String[] familles) {
        if (familles == null) {
            this.familles = null;
        } else {
            this.familles = Arrays.copyOf(familles, familles.length);
        }
    }

    /**
     * Les idControles de messages d'alerte recherchees.
     * 
     * @return Les idControles de messages d'alerte recherchees
     */
    public String[] getIdControles() {
        if (idControles == null) {
            return new String[0];
        } else {
            return Arrays.copyOf(idControles, idControles.length);
        }
    }

    /**
     * Les idControles des messages d'alerte recherchees.
     * 
     * @return Les idControles de messages d'alerte recherchees
     */
    public List<String> getIdControlesAsList() {
        if (idControles == null) {
            return new ArrayList<>();
        } else {
            return Arrays.asList(this.idControles);
        }
    }

    /**
     * Les idControles de messages d'alerte recherchees
     * 
     * @param idControles
     *            les idControles de messages d'alerte recherchees
     */
    public void setIdControles(String[] idControles) {
        if (idControles == null) {
            this.idControles = null;
        } else {
            this.idControles = Arrays.copyOf(idControles, idControles.length);
        }
    }

    /**
     * Les types de période recherchés.
     * 
     * @return les types de période recherchés
     */
    public String[] getTypesPeriode() {
        if (typesPeriode == null) {
            return new String[0];
        } else {
            return Arrays.copyOf(typesPeriode, typesPeriode.length);
        }
    }

    /**
     * Les types de période recherchés.
     * 
     * @return les types de période recherchés
     */
    public List<String> getTypesPeriodeAsList() {
        if (typesPeriode == null) {
            return new ArrayList<>();
        } else {
            return Arrays.asList(this.typesPeriode);
        }
    }

    /**
     * Les types de période recherchés.
     * 
     * @param typesPeriode
     *            les types de période recherchés
     */
    public void setTypesPeriode(String[] typesPeriode) {
        if (typesPeriode == null) {
            this.typesPeriode = null;
        } else {
            this.typesPeriode = Arrays.copyOf(typesPeriode, typesPeriode.length);
        }
    }

    /**
     * Les trimestres recherchés.
     * 
     * @return les modes de calcul de cotisation recherchés
     */
    public String[] getTrimestres() {
        if (trimestres == null) {
            return new String[0];
        } else {
            return Arrays.copyOf(trimestres, trimestres.length);
        }
    }

    /**
     * Les trimestres recherchés.
     * 
     * @return les modes de calcul de cotisation recherchés
     */
    public List<String> getTrimestresAsList() {
        if (trimestres == null) {
            return new ArrayList<>();
        } else {
            return Arrays.asList(this.trimestres);
        }
    }

    /**
     * Les trimestres recherchés.
     * 
     * @param trimestres
     *            les modes de calcul de cotisation recherchés
     */
    public void setTrimestres(String[] trimestres) {
        if (trimestres == null) {
            this.trimestres = null;
        } else {
            this.trimestres = Arrays.copyOf(trimestres, trimestres.length);
        }
    }

    /**
     * La présence de versements recherchée.
     * 
     * @return la présence de versements recherchée
     */
    public Boolean[] getVersements() {
        if (versements == null) {
            return new Boolean[0];
        } else {
            return Arrays.copyOf(versements, versements.length);
        }
    }

    /**
     * La présence de versements recherchée.
     * 
     * @return la présence de versements recherchée
     */
    public List<Boolean> getVersementsAsList() {
        if (versements == null) {
            return new ArrayList<Boolean>();
        } else {
            return Arrays.asList(this.versements);
        }
    }

    /**
     * La présence de versements recherchée.
     * 
     * @param versements
     *            la présence de versements recherchée
     */
    public void setVersements(Boolean[] versements) {
        if (versements == null) {
            this.versements = null;
        } else {
            this.versements = Arrays.copyOf(versements, versements.length);
        }
    }

    /**
     * La présence du flag VIP recherchée.
     * 
     * @return la présence du flag VIP recherchée
     */
    public String[] getVip() {
        if (vip == null) {
            return new String[0];
        } else {
            return Arrays.copyOf(vip, vip.length);
        }
    }

    /**
     * La présence du flag VIP recherchée.
     * 
     * @return la présence du flag VIP recherchée
     */
    public List<String> getVipAsList() {
        if (vip == null) {
            return new ArrayList<String>();
        } else {
            return Arrays.asList(this.vip);
        }
    }

    /**
     * La présence du flag VIP recherchée.
     * 
     * @param vip
     *            la présence du flag VIP recherchée
     */
    public void setVip(String[] vip) {
        if (vip == null) {
            this.vip = null;
        } else {
            this.vip = Arrays.copyOf(vip, vip.length);
        }
    }

    /**
     * Convertit une map de valeurs multiple en critères de recherche de périodes.
     * 
     * @param criteres
     *            les critères sous forme de map de valeurs multiples
     * @return les critères de recherche de périodes extraits
     */
    public static PeriodesRecuesCriteresRecherche from(MultiValueMap<String, Object> criteres) {
        PeriodesRecuesCriteresRecherche criteresRecherche = new PeriodesRecuesCriteresRecherche();
        try {
            BeanUtils.populate(criteresRecherche, criteres);
        } catch (IllegalAccessException | InvocationTargetException e) {
            throw new RedacUnexpectedException("Erreur lors du mapping de critères de recherche", e);
        }

        // Nettoyage des listes
        if (criteresRecherche.getStatutsAsList().contains(StringUtils.EMPTY)) {
            criteresRecherche.setStatuts(null);
        }
        if (criteresRecherche.getGroupesGestionAsList().contains(StringUtils.EMPTY)) {
            criteresRecherche.setGroupesGestion(null);
        }
        if (criteresRecherche.getFamillesAsList().contains(StringUtils.EMPTY)) {
            criteresRecherche.setFamilles(null);
        }
        if (criteresRecherche.getTypesPeriodeAsList().contains(StringUtils.EMPTY)) {
            criteresRecherche.setTypesPeriode(null);
        }
        if (criteresRecherche.getTrimestresAsList().contains(StringUtils.EMPTY)) {
            criteresRecherche.setTrimestres(null);
        }
        if (criteresRecherche.getIdControlesAsList().contains(StringUtils.EMPTY)) {
            criteresRecherche.setIdControles(null);
        }

        // Valeurs par défaut
        if (criteresRecherche.getTriChamp() == null) {
            criteresRecherche.setTriChamp(TRI_SPECIFIQUE);
        }

        // Validations
        criteresRecherche.validate();
        return criteresRecherche;
    }

    /**
     * Construction d'un objet PeriodesRecuesCriteresRecherche à partir de la query string
     * 
     * @param queryString
     *            query string contenant les critères de recherche
     * @param criteres
     *            multimap contenant les critères de la request
     * @return un objet PeriodesRecuesCriteresRecherche
     */
    public static PeriodesRecuesCriteresRecherche from(String queryString, MultiValueMap<String, Object> criteres) {

        if (queryString != null) {
            // split des params
            List<String> listeCrit = Arrays.asList(queryString.split("&"));
            for (String item : listeCrit) {
                // Ajout à la multimap avec split k,v
                String[] sub = item.split("=");
                if (sub.length == 2) {
                    criteres.add(sub[0], sub[1]);
                }
            }
        }

        return from(criteres);
    }

    /**
     * Valide le bon format des critères de recherche.
     */
    public void validate() {
        if (StringUtils.isNotBlank(getNumClient())) {
            if (getNumClient().contains("'")) {
                setNumClient(getNumClient().replace("'", ""));
            }
            try {
                Long.valueOf(getNumClient());
            } catch (NumberFormatException e) {
                throw new IllegalArgumentException("Le numéro de souscripteur n'est pas au bon format", e);
            }
        }
    }

    /**
     * Liste complete des gestionnaires affectées au traitement
     * 
     * @return Liste complete des gestionnaires affectées au traitement
     */
    public List<String> getAffecteAAsList() {
        if (affecteA == null) {
            return new ArrayList<>();
        } else {
            return Arrays.asList(this.affecteA);
        }
    }

    /**
     * Les utilisateurs ayant effectué le traitement
     * 
     * @return La liste des utilisateurs ayant traités les périodes
     */
    public List<String> getTraiteParAsList() {
        if (traitePar == null) {
            return new ArrayList<>();
        } else {
            return Arrays.asList(this.traitePar);
        }
    }

    /**
     * Liste des gestionnaires affectées au traitement
     * 
     * @return les modes de calcul de cotisation recherchés
     */
    public List<String> getaTraiterParAsList() {
        if (aTraiterPar == null) {
            return new ArrayList<>();
        } else {
            return Arrays.asList(this.aTraiterPar);
        }
    }

    /**
     * Les utilisateurs affectées au traitement
     * 
     * @param affecteA
     *            Set la liste d'utilisateurs affectés au traitement
     */
    public void setAffecteA(String[] affecteA) {
        if (affecteA == null) {
            this.affecteA = null;
        } else {
            this.affecteA = Arrays.copyOf(affecteA, affecteA.length);
        }
    }

    /**
     * Les utilisateurs devant traiter les periodes
     * 
     * @param affecteA
     *            Set la liste d'utilisateurs devant traiter les périodes
     */
    public void setaTraiterPar(String[] aTraiterPar) {
        if (aTraiterPar == null) {
            this.aTraiterPar = null;
        } else {
            this.aTraiterPar = Arrays.copyOf(aTraiterPar, aTraiterPar.length);
        }
    }

    /**
     * Les utilisateurs ayant effectué le traitement
     * 
     * @param traitePar
     *            Set la liste des utilisateurs ayant effectué le traitement
     */
    public void setTraitePar(String[] traitePar) {
        if (traitePar == null) {
            this.traitePar = null;
        } else {
            this.traitePar = Arrays.copyOf(traitePar, traitePar.length);
        }
    }

    /**
     * Les utilisateurs affectées au traitement
     * 
     * @return La liste d'utilisateurs affectés au traitement
     */
    public String[] getAffecteA() {
        if (affecteA == null) {
            return new String[0];
        } else {
            return Arrays.copyOf(affecteA, affecteA.length);
        }
    }

    /**
     * Les utilisateurs affectées au traitement
     * 
     * @return La liste d'utilisateurs affectés au traitement
     */
    public String[] getaTraiterPar() {
        if (aTraiterPar == null) {
            return new String[0];
        } else {
            return Arrays.copyOf(aTraiterPar, aTraiterPar.length);
        }
    }

    /**
     * Les utilisateurs ayant effectué le traitement
     * 
     * @return La liste d'utilisateurs ayant traité les listes
     */
    public String[] getTraitePar() {
        if (traitePar == null) {
            return new String[0];
        } else {
            return Arrays.copyOf(traitePar, traitePar.length);
        }
    }

    /**
     * Liste des gestionnaires présents dans ParamUtilisateurGestionnaire affectées au traitement
     * 
     * @return Liste des gestionnaires affectées au traitement présents dans ParamUtilisateurGestionnaire
     */
    public List<String> filtreAffecteAHorsCasParticulier() {
        List<String> utilisateurs = new ArrayList<String>();
        for (String item : getAffecteAAsList()) {
            if (!item.equals(AUTRE_GEST_CODE) && !item.equals(NON_AFFECTE_CODE)) {
                utilisateurs.add(item);
            }
        }

        return utilisateurs;
    }

    // declarer ctes
    /**
     * Liste des gestionnaires non-présents dans ParamUtilisateurGestionnaire affectées au traitement
     * 
     * @return Liste des gestionnaires affectées au traitement non-présents dans ParamUtilisateurGestionnaire
     */
    public List<String> filtreAffecteACasParticulier() {
        List<String> utilisateurs = new ArrayList<String>();
        for (String item : getAffecteAAsList()) {
            if (item.equals(AUTRE_GEST_CODE) || item.equals(NON_AFFECTE_CODE)) {
                utilisateurs.add(item);
            }
        }

        return utilisateurs;
    }

    /**
     * Liste des gestionnaires présents dans ParamUtilisateurGestionnaire devant effectuer le traitement
     * 
     * @return Liste des gestionnaires devant effectuer le traitement présents dans ParamUtilisateurGestionnaire
     */
    public List<String> filtreATraiterParHorsCasParticulier() {
        List<String> utilisateurs = new ArrayList<String>();
        for (String item : getaTraiterParAsList()) {
            if (!item.equals(AUTRE_GEST_CODE) && !item.equals(NON_ASSIGNE_CODE)) {
                utilisateurs.add(item);
            }
        }

        return utilisateurs;
    }

    /**
     * Liste des gestionnaires non-présents dans ParamUtilisateurGestionnaire devant effectuer le traitement
     * 
     * @return Liste des gestionnaires devant effectuer le traitement non-présents dans ParamUtilisateurGestionnaire
     */
    public List<String> filtreATraiterParCasParticulier() {
        List<String> utilisateurs = new ArrayList<String>();
        for (String item : getaTraiterParAsList()) {
            if (item.equals(AUTRE_GEST_CODE) || item.equals(NON_ASSIGNE_CODE)) {
                utilisateurs.add(item);
            }
        }

        return utilisateurs;
    }

    /**
     * Liste des gestionnaires présents dans ParamUtilisateurGestionnaire ayant effectués le traitement
     * 
     * @return Liste des gestionnaires ayant effectués le traitement présents dans ParamUtilisateurGestionnaire
     */
    public List<String> filtreTraiteParHorsCasParticulier() {
        List<String> utilisateurs = new ArrayList<String>();
        for (String item : getTraiteParAsList()) {
            if (!item.equals(AUTRE_GEST_CODE) && !item.equals(TRAITE_AUTO_CODE)) {
                utilisateurs.add(item);
            }
        }

        return utilisateurs;
    }

    /**
     * Liste des gestionnaires non-présents dans ParamUtilisateurGestionnaire ayant effectués le traitement
     * 
     * @return Liste des gestionnairesayant effectués le traitement non-présents dans ParamUtilisateurGestionnaire
     */
    public List<String> filtreTraiteParCasParticulier() {
        List<String> utilisateurs = new ArrayList<String>();
        for (String item : getTraiteParAsList()) {
            if (item.equals(AUTRE_GEST_CODE) || item.equals(TRAITE_AUTO_CODE)) {
                utilisateurs.add(item);
            }
        }

        return utilisateurs;
    }

}
