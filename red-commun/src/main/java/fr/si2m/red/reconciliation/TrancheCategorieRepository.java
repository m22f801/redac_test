package fr.si2m.red.reconciliation;

import java.util.List;
import java.util.Map;

import fr.si2m.red.parametrage.Tranche;

/**
 * Référentiel des tranches de categories.
 * 
 * @author poidij
 *
 */
public interface TrancheCategorieRepository extends ElementDeclaratifRepository<TrancheCategorie> {

    /**
     * Récupère une entité via son identifiant fonctionnel.
     * 
     * @param idPeriode
     *            l'identifiant de la période
     * @param numCategorieQuittancement
     *            le numéro de catégorie de quittancement
     * @param individu
     *            l'individu
     * @param numTranche
     *            le numéro de tranche
     * @return l'entité si trouvée, null sinon
     */
    TrancheCategorie get(Long idPeriode, String numCategorieQuittancement, String individu, Integer numTranche);

    /**
     * Récupère la somme des montantTranches pour des identifiants fonctionnels donnés.
     * 
     * @param idPeriode
     *            l'identifiant de la période
     * @param numCategorieQuittancement
     *            le numéro de catégorie de quittancement
     * @param individu
     *            l'individu
     * @param numTranche
     *            le numéro de tranche
     * @return la somme des montantTranches
     */
    Double getMontantSalaireTranche(Long idPeriode, String numCategorieQuittancement, String individu, Integer numTranche);

    /**
     * Récupère les sommes des montants d'une tranche (numéro / libellé) pour des numéros de catégories de quittancement donnés.
     * 
     * @param idPeriode
     *            l'identifiant de la période
     * @param numTranche
     *            le numéro de tranche
     * @param libelleTranche
     *            le libellé de la tranche
     * @param numerosCategorieQuittancement
     *            les numéros de catégories de quittancement
     * @return les sommes des montants de la tranche, ordonnés selon les indexes de catégories de quittancement donnés
     */
    Map<String, Double> getSommesMontantsTranchePourCategoriesQuittancement(Long idPeriode, Integer numTranche, String libelleTranche, List<String> numerosCategorieQuittancement);

    /**
     * Récupère la somme des montants d'une tranche donnée (numéro / libellé).
     * 
     * @param idPeriode
     *            l'identifiant de la période
     * @param numTranche
     *            le numéro de tranche
     * @param libelleTranche
     *            le libellé de la tranche
     * @return la somme des montants de la tranche
     */
    Double getSommesMontantsTranche(Long idPeriode, Integer numTranche, String libelleTranche);

    /**
     * Récupère les tranches concernées par une période donnée, triées par numéro de tranche.
     * 
     * @param idPeriode
     *            l'identifiant de la période
     * @return les tranches concernées par la période
     */
    List<Tranche> getTranchesDistinctesPourPeriodeTrieesParNumero(Long idPeriode);

}
