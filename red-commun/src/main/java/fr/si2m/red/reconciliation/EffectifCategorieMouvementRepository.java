package fr.si2m.red.reconciliation;

import java.util.List;
import java.util.Map;

/**
 * Référentiel des effectifs des categories de mouvement.
 * 
 * @author poidij
 *
 */
public interface EffectifCategorieMouvementRepository extends ElementDeclaratifRepository<EffectifCategorieMouvement> {

    /**
     * Récupère un mouvement via son identifiant fonctionnel.
     * 
     * @param idPeriode
     *            l'ID de la période
     * @param numCategorieQuittancement
     *            le numéro de catégorie
     * @param dateEffetMouvement
     *            la date d'effet du mouvement
     * @return le mouvement s'il existe, null sinon
     */
    EffectifCategorieMouvement get(Long idPeriode, String numCategorieQuittancement, Integer dateEffetMouvement);

    /**
     * Récupère les mouvements pour une catégorie de quittancement et une période données.
     * 
     * @param idPeriode
     *            l'ID de la période
     * @param numCategorieQuittancement
     *            la catégorie de quittancement
     * @return les mouvements liés
     */
    List<EffectifCategorieMouvement> getPourCategorie(Long idPeriode, String numCategorieQuittancement);

    /**
     * Récupère les dates d'effet distinctes des mouvements d'effectifs sur une période donnée.
     * 
     * @param idPeriode
     *            l'ID de la période
     * @return les dates d'effet distinctes des mouvements d'effectifs sur la période donnée
     */
    List<Integer> getDatesEffetDistinctesPourPeriode(Long idPeriode);

    /**
     * Récupère les nombres de mouvements pour une date d'effet et des catégories de quittancement donnés.
     * 
     * @param idPeriode
     *            l'ID de la période
     * @param dateEffet
     *            la date d'effet des mouvements recherchés
     * @param numerosCategorieQuittancement
     *            les numéros de catégorie de quittancement
     * @return les nombres de mouvements, triés par catégorie de quittancement correspondante
     */
    Map<String, Integer> getNombresMouvementsPourDateEffetEtCategoriesQuittancement(Long idPeriode, Integer dateEffet, List<String> numerosCategorieQuittancement);

    /**
     * Récupère le nombre total de mouvements pour une date d'effet donnée.
     * 
     * @param idPeriode
     *            l'ID de la période
     * @param dateEffet
     *            la date d'effet des mouvements recherchés
     * @return le nombre total de mouvements à une date donnée
     */
    Long getNombreTotalMouvementsPourDateEffet(Long idPeriode, Integer dateEffet);

}
