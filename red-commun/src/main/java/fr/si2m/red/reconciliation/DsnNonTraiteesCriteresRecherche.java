package fr.si2m.red.reconciliation;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.util.MultiValueMap;

import fr.si2m.red.RedacUnexpectedException;
import fr.si2m.red.core.repository.CriteresRecherche;
import fr.si2m.red.core.repository.IgnorePourQueryString;
import lombok.Getter;
import lombok.Setter;

/**
 * Critères de recherche de périodes reçues.
 * 
 * @author nortaina
 *
 */
public class DsnNonTraiteesCriteresRecherche extends CriteresRecherche {
    /**
     * UID de version.
     */
    private static final long serialVersionUID = 4848566597642428202L;

    /**
     * Indicateur de tri spécifique.
     */
    public static final String TRI_SPECIFIQUE = "spec";

    /**
     * Autre gestionnaire code : "AUTRE_GEST"
     */
    public static final String AUTRE_GEST_CODE = "AUTRE_GEST";
    /**
     * Non affecté code :"NON_AFFECTE"
     */
    public static final String NON_AFFECTE_CODE = "NON_AFFECTE";
    /**
     * Traite auto code : "TRAITE_AUTO"
     */
    public static final String TRAITE_AUTO_CODE = "TRAITE_AUTO";
    /**
     * Non assigné code : "NON_ASSIGNE"
     */
    public static final String NON_ASSIGNE_CODE = "NON_ASSIGNE";

    /**
     * Autre gestionnaire libellé : "Autre gestionnaire"
     */
    public static final String AUTRE_GEST_LIBELLE = "Autre gestionnaire";
    /**
     * Non affecté libellé : "Non affecté"
     */
    public static final String NON_AFFECTE_LIBELLE = "Non affecté";
    /**
     * Traite auto libellé : "Traité automatiquement"
     */
    public static final String TRAITE_AUTO_LIBELLE = "Traité automatiquement";
    /**
     * Non assigné libellé : "Non assigné"
     */
    public static final String NON_ASSIGNE_LIBELLE = "Non assigné";

    /**
     * Les statuts des périodes recherchées.
     * 
     */
    private String[] statuts;

    /**
     * Les groupes de gestion recherchés.
     * 
     */
    private String[] groupesGestion;

    /**
     * Les familles de contrat recherchées.
     * 
     */
    private String[] familles;

    /**
     * Le numéro de SIREN recherché.
     * 
     * @param siren
     *            le numéro de SIREN recherché
     * @return le numéro de SIREN recherché
     */
    @Getter
    @Setter
    private String sirenEntreprise;

    /**
     * Le NIC recherché.
     * 
     * @param nic
     *            le NIC recherché
     * @return le NIC recherché
     */
    @Getter
    @Setter
    private String nicEtablissement;

    /**
     * Le numéro de contrat recherché.
     * 
     * @param numContrat
     *            le numéro de contrat recherché
     * @return le numéro de contrat recherché
     */
    @Getter
    @Setter
    private String numContrat;

    /**
     * Le compte d'encaissement recherché.
     * 
     * @param cptEnc
     *            le compte d'encaissement recherché
     * @return le compte d'encaissement recherché
     */
    @Getter
    @Setter
    private String cptEnc;

    /**
     * Le mois moisRattachement
     * 
     * @param moisRattachement
     *            le moisRattachement recherché
     * @return le moisRattachement recherché
     */
    @Getter
    @Setter
    private String moisRattachement;

    /**
     * La raison sociale recherchée.
     * 
     * @param raisonSociale
     *            la raison sociale recherchée
     * @return la raison sociale recherchée
     */
    @Getter
    @Setter
    private String raisonSociale;

    /**
     * La présence de versements recherchée.
     * 
     */
    private Boolean[] versements;

    /**
     * Le numéro du compte producteur du contrat recherché.
     * 
     * @param cptProd
     *            le numéro du compte producteur du contrat recherché
     * @return le numéro du compte producteur du contrat recherché
     */
    @Getter
    @Setter
    private String cptProd;

    /**
     * Constructeur par défaut des critères.
     */
    public DsnNonTraiteesCriteresRecherche() {
        super();
    }

    /**
     * Constructeur de critères par copie de critères donnés.
     * 
     * @param instance
     *            les critères à copier
     */
    public DsnNonTraiteesCriteresRecherche(DsnNonTraiteesCriteresRecherche instance) {
        this();
        try {
            BeanUtils.copyProperties(this, instance);
        } catch (IllegalAccessException | InvocationTargetException e) {
            throw new RedacUnexpectedException("Erreur lors de la copie de critères de recherche", e);
        }
    }

    /**
     * Les statuts de déclarations recherchés.
     * 
     * @return les statuts de déclarations recherchés
     */
    public String[] getStatuts() {
        if (statuts == null) {
            return new String[0];
        } else {
            return Arrays.copyOf(statuts, statuts.length);
        }
    }

    /**
     * Les statuts de déclarations recherchés.
     * 
     * @return les statuts de déclarations recherchés
     */
    public List<String> getStatutsAsList() {
        if (statuts == null) {
            return new ArrayList<>();
        } else {
            return Arrays.asList(this.statuts);
        }
    }

    /**
     * Les statuts de déclarations recherchés.
     * 
     * @param statuts
     *            les statuts de déclarations recherchés
     */
    public void setStatuts(String[] statuts) {
        if (statuts == null) {
            this.statuts = null;
        } else {
            this.statuts = Arrays.copyOf(statuts, statuts.length);
        }
    }

    /**
     * Les groupes de gestion recherchés.
     * 
     * @return les groupes de gestion recherchés
     */
    public String[] getGroupesGestion() {
        if (groupesGestion == null) {
            return new String[0];
        } else {
            return Arrays.copyOf(groupesGestion, groupesGestion.length);
        }
    }

    /**
     * Les groupes de gestion recherchés sous forme de liste.
     * 
     * @return les groupes de gestion recherchés
     */
    public List<String> getGroupesGestionAsList() {
        if (groupesGestion == null) {
            return new ArrayList<String>();
        } else {
            return Arrays.asList(this.groupesGestion);
        }
    }

    /**
     * Les groupes de gestion recherchés.
     * 
     * @param groupesGestion
     *            les groupes de gestion recherchés
     */
    public void setGroupesGestion(String[] groupesGestion) {
        if (groupesGestion == null) {
            this.groupesGestion = null;
        } else {
            this.groupesGestion = Arrays.copyOf(groupesGestion, groupesGestion.length);
        }
    }

    /**
     * Les familles de contrat recherchées.
     * 
     * @return les familles de contrat recherchées
     */
    public String[] getFamilles() {
        if (familles == null) {
            return new String[0];
        } else {
            return Arrays.copyOf(familles, familles.length);
        }
    }

    /**
     * Les familles de contrat recherchées.
     * 
     * @return les familles de contrat recherchées
     */
    public List<String> getFamillesAsList() {
        if (familles == null) {
            return new ArrayList<>();
        } else {
            return Arrays.asList(this.familles);
        }
    }

    /**
     * Les familles de contrat recherchées.
     * 
     * @param familles
     *            les familles de contrat recherchées
     */
    public void setFamilles(String[] familles) {
        if (familles == null) {
            this.familles = null;
        } else {
            this.familles = Arrays.copyOf(familles, familles.length);
        }
    }

    /**
     * La présence de versements recherchée.
     * 
     * @return la présence de versements recherchée
     */
    public Boolean[] getVersements() {
        if (versements == null) {
            return new Boolean[0];
        } else {
            return Arrays.copyOf(versements, versements.length);
        }
    }

    /**
     * La présence de versements recherchée.
     * 
     * @return la présence de versements recherchée
     */
    public List<Boolean> getVersementsAsList() {
        if (versements == null) {
            return new ArrayList<Boolean>();
        } else {
            return Arrays.asList(this.versements);
        }
    }

    /**
     * La présence de versements recherchée.
     * 
     * @param versements
     *            la présence de versements recherchée
     */
    public void setVersements(Boolean[] versements) {
        if (versements == null) {
            this.versements = null;
        } else {
            this.versements = Arrays.copyOf(versements, versements.length);
        }
    }

    /**
     * La présence du flag VIP recherchée.
     * 
     * @param vip
     *            la présence du flag VIP recherchée
     */
    public String getDateMoisAnnee(String moisAnnee) {
        String dateAnneeMois = moisAnnee.substring(3, 4) + moisAnnee.substring(4, 5) + moisAnnee.substring(5, 6) + moisAnnee.substring(6, 7)
                + moisAnnee.substring(0, 1) + moisAnnee.substring(1, 2);

        return dateAnneeMois;
    }

    /**
     * La présence du flag VIP recherchée.
     * 
     * @param vip
     *            la présence du flag VIP recherchée
     */
    public String getDateMoisAnneeJour(String moisAnnee) {

        String dateAnneeMois = moisAnnee.substring(3, 4) + moisAnnee.substring(4, 5) + moisAnnee.substring(5, 6) + moisAnnee.substring(6, 7)
                + moisAnnee.substring(0, 1) + moisAnnee.substring(1, 2);

        return dateAnneeMois + "01";

    }

    /**
     * La présence du flag VIP recherchée.
     * 
     * @param vip
     *            la présence du flag VIP recherchée
     */
    public String formatDateMoisAnnee(String moisAnnee) {

        String dateMoisAnnee = moisAnnee.substring(4, 5) + moisAnnee.substring(5, 6) + "/" + moisAnnee.substring(0, 1) + moisAnnee.substring(1, 2)
                + moisAnnee.substring(2, 3) + moisAnnee.substring(3, 4);

        return dateMoisAnnee;
    }

    /**
     * Convertit une map de valeurs multiple en critères de recherche de périodes.
     * 
     * @param criteres
     *            les critères sous forme de map de valeurs multiples
     * @return les critères de recherche de périodes extraits
     */
    public static DsnNonTraiteesCriteresRecherche from(MultiValueMap<String, Object> criteres) {
        DsnNonTraiteesCriteresRecherche criteresRecherche = new DsnNonTraiteesCriteresRecherche();

        try {
            BeanUtils.populate(criteresRecherche, criteres);
        } catch (IllegalAccessException | InvocationTargetException e) {
            throw new RedacUnexpectedException("Erreur lors du mapping de critères de recherche", e);
        }

        // Nettoyage des listes
        if (criteresRecherche.getStatutsAsList().contains(StringUtils.EMPTY)) {
            criteresRecherche.setStatuts(null);
        }
        if (criteresRecherche.getGroupesGestionAsList().contains(StringUtils.EMPTY)) {
            criteresRecherche.setGroupesGestion(null);
        }
        if (criteresRecherche.getFamillesAsList().contains(StringUtils.EMPTY)) {
            criteresRecherche.setFamilles(null);
        }
        /**
         * Afin de réduire le temps du script, on met le mois précédent comme valeur de recherche
         */
        if (criteresRecherche.getMoisRattachement() == null) {
            Date d = new Date();
            Calendar cal = Calendar.getInstance();
            cal.setTime(d);
            int mois = cal.get(Calendar.MONTH) + 1;
            int an = cal.get(Calendar.YEAR);
            if (mois == 1) {
                mois = 12;
                an = an - 1;
            } else {
                mois--;
            }
            criteresRecherche.setMoisRattachement((mois < 10 ? "0" + Integer.toString(mois):Integer.toString(mois))  + "/" + Integer.toString(an));
        }

        // Valeurs par défaut
        if (criteresRecherche.getTriChamp() == null) {
            criteresRecherche.setTriChamp(TRI_SPECIFIQUE);
        }

        // Validations
        return criteresRecherche;
    }

    /**
     * Construction d'un objet PeriodesRecuesCriteresRecherche à partir de la query string
     * 
     * @param queryString
     *            query string contenant les critères de recherche
     * @param criteres
     *            multimap contenant les critères de la request
     * @return un objet PeriodesRecuesCriteresRecherche
     */
    public static DsnNonTraiteesCriteresRecherche from(String queryString, MultiValueMap<String, Object> criteres) {

        if (queryString != null) {
            // split des params
            List<String> listeCrit = Arrays.asList(queryString.split("&"));
            for (String item : listeCrit) {
                // Ajout à la multimap avec split k,v
                String[] sub = item.split("=");
                if (sub.length == 2) {
                    criteres.add(sub[0], sub[1]);
                }
            }
        }

        return from(criteres);
    }

    /**
     * Valide le bon format des critères de recherche.
     * 
     * public void validate() { if (StringUtils.isNotBlank(getNumClient())) { if (getNumClient().contains("'")) { setNumClient(getNumClient().replace("'", ""));
     * } try { Long.valueOf(getNumClient()); } catch (NumberFormatException e) { throw new IllegalArgumentException(
     * "Le numéro de souscripteur n'est pas au bon format", e); } } }
     */

}
