package fr.si2m.red.reconciliation;

import java.util.List;
import java.util.Map;

import fr.si2m.red.core.repository.RedacRepository;

/**
 * Référentiel des historiques des assignations des périodes.
 * 
 * @author gahagnont
 *
 */
public interface HistoriqueAssignationPeriodeRepository extends RedacRepository<HistoriqueAssignationPeriode> {

    /**
     * Récupère les assignations d'une période.
     * 
     * @param idPeriode
     *            l'identifiant de l'assignation de la période
     * @return les assignations pour cette période
     */
    List<HistoriqueAssignationPeriode> getHistoriqueAssignationsPeriode(Long idPeriode);

    /**
     * Récupère une assignation donnée d'une période.
     * 
     * @param idPeriode
     *            l'identifiant de la période
     * @param dateHeureAssignation
     *            l'horodatage de l'assignation recherchée
     * @return l'assignation recherchée
     */
    HistoriqueAssignationPeriode getHistoriqueAssignationPeriodePourMessage(Long idPeriode, Long dateHeureSaisie);

    /**
     * Récupère la dernière assignation d'une période donnée.
     * 
     * @param idPeriode
     *            l'identifiant de la période
     * @return l'assignation de la période concernée
     */
    HistoriqueAssignationPeriode getDerniereAssignationPourPeriode(Long idPeriode);

    /**
     * Supprime les assignations pour une période donnée.
     * 
     * @param idPeriode
     *            l'identifiant de la période
     * @return le nombre d'éléments supprimés effectivement dans le référentiel
     */
    int supprimeHistoriqueAssignationPourPeriode(Long idPeriode);

    /**
     * Exécute une procédure stockée MySQL.
     * 
     * @param sql
     *            la commande d'appel de la procédure
     * @param params
     *            les paramètres d'appels de la procédure
     */
    void callMysqlProcedure(String sql, Map<String, Object> params);

    /**
     * Creation d'historique d'assignation en masse
     * 
     * @param idsPeriodes
     *            liste des periodes ciblées
     * @param aTraiterPar
     *            gestionnaire assigné
     */
    void creationEnMasseOrigineIHM(List<Long> idsPeriodes, String aTraiterPar, String utilisateur);

}
