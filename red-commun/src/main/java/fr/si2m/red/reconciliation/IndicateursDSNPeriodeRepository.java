package fr.si2m.red.reconciliation;

import fr.si2m.red.core.repository.EntiteImportableRepository;

/**
 * Référentiel des indicateurs DSN par période.
 * 
 * @author nortaina
 *
 */
public interface IndicateursDSNPeriodeRepository extends EntiteImportableRepository<IndicateursDSNPeriode> {

    /**
     * Récupère les indicateurs sur la période la plus récente pour un numéro de contrat donné.
     * 
     * @param numContrat
     *            le numéro de contrat donné
     * 
     * @return les indicateurs sur la période la plus récente pour le numéro de contrat donné
     */
    IndicateursDSNPeriode getIndicateursDernierePeriode(String numContrat);

    /**
     * Récupère les indicateurs pour une période donnée.
     * 
     * @param numContrat
     *            le numéro de contrat donné
     * @param dateDebutPeriode
     *            la date de début de période
     * @param dateFinPeriode
     *            la date de fin de période
     * @return les indicateurs de cette période s'ils existent, null sinon
     */
    IndicateursDSNPeriode getIndicateursPourPeriode(String numContrat, Integer dateDebutPeriode, Integer dateFinPeriode);

    /**
     * Vérifier l'existance d'au moins un indicateur pour une période donnée.
     * 
     * @param numContrat
     *            le numéro de contrat donné
     * @param dateDebutPeriode
     *            la date de début de période
     * @param dateFinPeriode
     *            la date de fin de période
     * @return true si trouvé, false sinon
     */
    boolean existeIndicateursPourPeriode(String numContrat, Integer dateDebutPeriode, Integer dateFinPeriode);

    /**
     * Vérifier l'existance d'au moins un indicateur pour une période actuelle.
     * 
     * @param numContrat
     *            le numéro de contrat donné
     * @param dateDebutPeriode
     *            la date de début de période
     * @return true si trouvé, false sinon
     */

    boolean existeIndicateursPourPeriodeActuelle(String numContrat, Integer dateDebutPeriode);

}
