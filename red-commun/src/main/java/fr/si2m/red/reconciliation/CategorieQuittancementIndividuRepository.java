package fr.si2m.red.reconciliation;

import java.util.List;

/**
 * Référentiel des categories de quittancement des individus.
 * 
 * @author poidij
 *
 */
public interface CategorieQuittancementIndividuRepository extends ElementDeclaratifRepository<CategorieQuittancementIndividu> {
    /**
     * Récupère un quittancement d'individu via son identifiant fonctionnel.
     * 
     * @param idPeriode
     *            l'ID de la période
     * @param numCategorieQuittancement
     *            le numéro de catégorie
     * @param individu
     *            l'individu
     * @return le quittancement recherché s'il existe, null sinon
     */
    CategorieQuittancementIndividu get(Long idPeriode, String numCategorieQuittancement, String individu);

    /**
     * Récupère une catégorie de quittancement pour une période et un numéro de catégorie donnés.
     * 
     * @param idPeriode
     *            l'ID de la période
     * @param numCategorieQuittancement
     *            le numéro de catégorie
     * @return le quittancement recherché s'il existe, null sinon
     */
    CategorieQuittancementIndividu getCategorieQuittancementPourPeriode(Long idPeriode, String numCategorieQuittancement);

    /**
     * Récupère la somme des montants de cotisation déclaré pour une période donnée.
     * 
     * @param idPeriode
     *            l'identifiant de la période
     * @return la somme des montants de cotisation pour une période donnée
     */
    Double getSommeMontantsCotisationDeclare(Long idPeriode);

    /**
     * Récupère la somme des montants de cotisation estimé pour une période donnée
     * 
     * @param idPeriode
     *            l'identifiant de la période
     * @return la somme des montants de cotisation pour une période donnée
     */
    Double getSommeMontantsCotisationEstime(Long idPeriode);

    /**
     * Récupère la somme des montants de cotisation estimé pour une période donnée et un individu
     * 
     * @param idPeriode
     *            l'identifiant de la période
     * @param individu
     *            le nir ou ntt de l'individu
     * @return la somme des montants de cotisation estimés pour une période donnée pour l'individu donné
     */
    Double getSommeMontantsCotisationEstime(Long idPeriode, String individu);

    /**
     * Récupère la somme des montants de cotisation estimés ( pour individu, période, codePopulation)
     * 
     * @param idPeriode
     *            l'identifiant de la période
     * @param individu
     *            le nir ou ntt de l'individu
     * @param codePopulation
     *            codePopulation
     * @return la somme des montants de cotisation estimés pour une période donnée pour l'individu donné et un codePopulation donné
     */
    Double getSommeMontantsCotisationEstimePourPopulation(Long idPeriode, String individu, String codePopulation);

    /**
     * Récupère les numéros de catégories distincts et triés pour une période donnée.
     * 
     * @param idPeriode
     *            l'identifiant de la période
     * @return les numéros de catégories distincts et triés pour la période donnée
     */
    List<String> getNumerosCategorieDistinctsPourPeriode(Long idPeriode);

    /**
     * Récupère les sommes des montants de cotisation pour une période et des catégories de quittancement donnés.
     * 
     * @param idPeriode
     *            l'identifiant de la période
     * @param numerosCategorieQuittancement
     *            les numéros de catégorie de quittancement
     * @return les sommes des montants de cotisation sur la période donnée, triés par numéro de catégorie de quittancement en entrée
     */
    List<Double> getSommesMontantsCotisationPourCategoriesQuittancement(Long idPeriode, List<String> numerosCategorieQuittancement);

    /**
     * Récupère les effectifs en début de période pour des catégories de quittancement donnés.
     * 
     * @param idPeriode
     *            l'identifiant de la période
     * @param numerosCategorieQuittancement
     *            les numéros de catégorie de quittancement
     * @return les effectifs en début de période donnée, triés par numéro de catégorie de quittancement en entrée
     */
    List<Long> getEffectifsDebutPourPeriodeEtCategoriesQuittancement(Long idPeriode, List<String> numerosCategorieQuittancement);

    /**
     * Récupère les effectifs en fin de période pour des catégories de quittancement donnés.
     * 
     * @param idPeriode
     *            l'identifiant de la période
     * @param numerosCategorieQuittancement
     *            les numéros de catégorie de quittancement
     * @return les effectifs en fin de période donnée, triés par numéro de catégorie de quittancement en entrée
     */
    List<Long> getEffectifsFinPourPeriodeEtCategoriesQuittancement(Long idPeriode, List<String> numerosCategorieQuittancement);

    /**
     * Récupère l'effectif total en début de période donnée.
     * 
     * @param idPeriode
     *            l'identifiant de la période
     * @return l'effectif total en début de période donnée
     */
    Long getEffectifTotalDebutPeriode(Long idPeriode);

    /**
     * Récupère l'effectif total en fin de période donnée.
     * 
     * @param idPeriode
     *            l'identifiant de la période
     * @return l'effectif total en fin de période donnée
     */
    Long getEffectifTotalFinPeriode(Long idPeriode);

    /**
     * indique s'il existe une categorie quittancement individu rattaché à la période
     * 
     * @param idPeriode
     *            l'identifiant de période
     * @return true si pour la période donnée, au moins une ligne est trouvée dans catégorie quittancement individu
     */
    boolean existeCategorieQuittancementIndividuPourPeriode(Long idPeriode);
}
