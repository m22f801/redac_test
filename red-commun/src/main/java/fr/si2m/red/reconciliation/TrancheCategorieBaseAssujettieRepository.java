package fr.si2m.red.reconciliation;

import java.util.Map;

/**
 * Référentiel des tranches de categories des bases assujetties.
 * 
 * @author poidij
 *
 */
public interface TrancheCategorieBaseAssujettieRepository extends ElementDeclaratifRepository<TrancheCategorieBaseAssujettie> {

    /**
     * Récupère une entité via son identifiant fonctionnel.
     * 
     * @param idPeriode
     *            l'ID de la période
     * @param moisRattachement
     *            le mois de rattachement
     * @param idAffiliation
     *            l'ID de l'affiliation
     * @param dateDebutRattachementBase
     *            la date de début de rattachement de la base
     * @param numTranche
     *            le numéro de tranche
     * @param numCategorieQuittancement
     *            le numéro de catégorie de quittancement
     * @return l'entité si trouvée, null sinon
     */
    TrancheCategorieBaseAssujettie get(Long idPeriode, Integer moisRattachement, String idAffiliation, Integer dateDebutRattachementBase, Integer numTranche,
            String numCategorieQuittancement);

    /**
     * Calcule le montant de cotisation pour un quittanchement par catégorie en mode de consolidation TOUS pour une période donnée.
     * 
     * @param idPeriode
     *            l'identifiant de la période
     * @param numCategorie
     *            le numéro de catégorie du quittancement
     * @return le montant calculé
     */
    double getMontantCotisationTousPourPeriodeEtNumCategorie(Long idPeriode, String numCategorie);

    /**
     * Calcule le montant de cotisation pour un quittanchement par catégorie en mode de consolidation INDIV pour une période donnée.
     * 
     * @param idPeriode
     *            l'identifiant de la période
     * @param numCategorie
     *            le numéro de catégorie du quittancement
     * @param individu
     *            les identifiants de l'individu
     * @return le montant calculé
     */
    double getMontantCotisationIndivPourPeriodeEtNumCategorieEtIndividu(Long idPeriode, String numCategorie, IdentifiantsIndividu individu);

    /**
     * Calcule le montant de cotisation pour une tranche par catégorie pour une période donnée.
     * 
     * @param numCategorie
     *            le numéro de catégorie du quittancement
     * @param idPeriode
     *            l'identifiant de la période
     * @param numTranche
     *            la tranche
     * @return le montant calculé
     */
    double getMontantTranchePourPeriodeEtNumCategorie(Integer numTranche, Long idPeriode, String numCategorie);

    /**
     * Calcule le montant de cotisation d'un individu pour une tranche par catégorie pour une période donnée.
     * 
     * @param numCategorie
     *            le numéro de catégorie du quittancement
     * @param idPeriode
     *            l'identifiant de la période
     * @param numTranche
     *            la tranche
     * @param individu
     *            l'identifiant de l'individu (identifiant répertoire ou numéro technique temporaire)
     * @return le montant calculé pour l'individu
     */
    double getMontantTranchePourPeriodeEtNumCategorieEtIndividu(Integer numTranche, Long idPeriode, String numCategorie, String individu);

    /**
     * Calcule le montant de cotisation pour une tranche d'une période donnée.
     * 
     * @param idPeriode
     *            l'identifiant de la période
     * @param numTranche
     *            la tranche
     * @return le montant calculé pour la tranche
     */
    double getMontantTranchePourPeriode(Integer numTranche, Long idPeriode);

    /**
     * Calcule le montant de cotisation pour une affiliation, une tranche et une catégorie pour une période donnée.
     * 
     * @param idPeriode
     *            l'identifiant de la période
     * @param idAffiliation
     *            l'identifiant de l'affiliation
     * @param numTranche
     *            la tranche
     * @return le montant calculé
     */
    double getSommeMontantTranchePourAffiliation(Long idPeriode, String idAffiliation, Integer numTranche);

    /**
     * Récupère le libellé de tranche pour une période donnée.
     * 
     * @param idPeriode
     *            l'identifiant de la période
     * @param numTranche
     *            le numéro de la tranche
     * @return le libellé de tranche
     */
    String getLibelleTranchePourPeriodeEtNumTranche(Long idPeriode, int numTranche);

    /**
     * Récupère le montant d'une tranche pour une période et une base assujettie données.
     * 
     * @param idPeriode
     *            l'identifiant de la période
     * @param idBaseAssujettie
     *            l'identifiant de la base assujettie
     * @param numTranche
     *            le numéro de la tranche
     * @return le montant de la tranche pour la base assujettie
     */
    Double getMontantTranchePourBaseAssujettie(Long idPeriode, String idBaseAssujettie, int numTranche);

    /**
     * Récupère les montants des tranches pour une période et une base assujettie données.
     * 
     * @param idPeriode
     *            l'identifiant de la période
     * @param idBaseAssujettie
     *            l'identifiant de la base assujettie
     * @return les montant des tranches pour la base assujettie
     */
    Map<Integer, Double> getMontantsTranchesPourBaseAssujettie(Long idPeriode, String idBaseAssujettie);

}
