package fr.si2m.red.reconciliation;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import fr.si2m.red.Entite;
import fr.si2m.red.dsn.Affiliation;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * Modèle des tranches de categories de bases assujetties.
 * 
 * @author poidij
 *
 */
@Entity
@Table(name = "TRANCHE_CATEGORIE_BASE_ASSUJETTIE")
@Data
@EqualsAndHashCode(callSuper = false, of = { "idTechnique" })
@ToString(callSuper = false, of = { "idTechnique", "idPeriode", "moisRattachement", "idAffiliation", "dateDebutRattachementBase", "numTranche",
        "numCategorieQuittancement" })
public class TrancheCategorieBaseAssujettie extends Entite {
    /**
     * UID de version.
     */
    private static final long serialVersionUID = -3324801079594603125L;

    /**
     * L'identifiant technique.
     * 
     * @param idTechnique
     *            l'identifiant technique
     * @return l'identifiant technique
     */
    @Id
    @GeneratedValue
    @Column(name = "ID_TECH")
    private Long idTechnique;

    /**
     * L'identifiant technique de la période à laquelle ce message est rattaché.
     * 
     * @param idPeriode
     *            l'identifiant technique de la période
     * @return l'identifiant technique de la période
     */
    @Column(name = "ID_PERIODE")
    private Long idPeriode;

    /**
     * Identifiant de la base assujettie.
     *
     * @param idBaseAssujettie
     *            L'identifiant de la base assujettie
     * @return L'identifiant de la base assujettie
     */
    @Column(name = "ID_BASE_ASSUJETTIE")
    private String idBaseAssujettie;

    /**
     * Date du mois de rattachement de la période déclarée pour cette adhésion. Clef de la déclaration rattachée.
     * 
     * @param moisRattachement
     *            La date du mois de rattachement de la période déclarée
     * @return La date du mois de rattachement de la période déclarée
     */
    @Column(name = "MOIS_RATTACHEMENT")
    private Integer moisRattachement;

    /**
     * Identifiant de l'affiliation.
     * 
     * @param idAffiliation
     *            L'identifiant de l'affiliation
     * @return L'identifiant de l'affiliation
     */
    @Column(name = "ID_AFFILIATION")
    private String idAffiliation;

    /**
     * L'affiliation.
     *
     * @param affiliation
     *            L'affiliation
     * @return L'affiliation
     */
    @ManyToOne
    @Fetch(FetchMode.JOIN)
    @JoinColumns({ @JoinColumn(name = "ID_AFFILIATION", insertable = false, updatable = false),
            @JoinColumn(name = "TMP_BATCH", insertable = false, updatable = false) })
    private Affiliation affiliation;

    /**
     * La date de début de rattachement.
     * 
     * @param dateDebutRattachementBase
     *            La date de début de rattachement
     * @return La date de début de rattachement
     */
    @Column(name = "DATE_DEB_RATTACHEMENT")
    private Integer dateDebutRattachementBase;

    /**
     * Numéro de tranche.
     * 
     * @param numTranche
     *            Le numéro de tranche
     * @return Le numéro de tranche
     */
    @Column(name = "NUM_TRANCHE")
    private Integer numTranche;

    /**
     * Numéro de catégorie de quittancement.
     * 
     * @param numCategorieQuittancement
     *            Le numéro de la catégorie de quittancement
     * @return Le numéro de la catégorie de quittancement
     */
    @Column(name = "NOCAT")
    private String numCategorieQuittancement;

    /**
     * Libellé de la tranche.
     * 
     * @param libelleTranche
     *            Le libellé de la tranche
     * @return Le libellé de la tranche
     */
    @Column(name = "LIB_TRANCHE")
    private String libelleTranche;

    /**
     * Montant de la tranche.
     * 
     * @param montantTranche
     *            Le montant de la tranche
     * @return Le montant de la tranche
     */
    @Column(name = "MT_TRANCHE")
    private Double montantTranche;

    /**
     * L'estimation de cotisation en montant
     * 
     * @param estimationCotisation
     *            L'estimation de cotisation
     * @return L'estimation de la cotisation en montant
     */
    @Column(name = "ESTIMATION_COTISATION")
    private Double estimationCotisation;

    @Override
    public Long getId() {
        return idTechnique;
    }
}
