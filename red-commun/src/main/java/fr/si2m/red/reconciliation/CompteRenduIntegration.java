package fr.si2m.red.reconciliation;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import fr.si2m.red.DateRedac;
import fr.si2m.red.Entite;

/**
 * Modèle des compte-rendus d'intégration
 * 
 * @author poidij
 *
 */
@Entity
@Table(name = "COMPTE_RENDU_INTEGRATION")
@Data
@EqualsAndHashCode(callSuper = false, of = { "idTechnique" })
@ToString(callSuper = false, of = { "idTechnique", "idPeriode", "dateTraitement" })
public class CompteRenduIntegration extends Entite {

    /**
     * 
     */
    private static final long serialVersionUID = -3324801079594603125L;

    /**
     * L'identifiant technique.
     * 
     * @param idTechnique
     *            l'identifiant technique
     * @return l'identifiant technique
     */
    @Id
    @GeneratedValue
    @Column(name = "ID_TECH")
    private Long idTechnique;

    /**
     * Identifiant technique de la période à laquelle ce message est rattaché.
     * 
     * @param idPeriode
     *            l'identifiant technique de la période
     * @return l'identifiant technique de la période
     */
    @Column(name = "ID_PERIODE")
    private Long idPeriode;

    /**
     * La date de traitement
     * 
     * @param dateTraitement
     *            La date de traitement
     * @return La date de traitement
     */
    @Column(name = "DATE_TRAITEMENT")
    private Integer dateTraitement;

    /**
     * L'émetteur du CR
     * 
     * @param emetteurCr
     *            L'émetteur du CR
     * @return L'émetteur du CR
     */
    @Column(name = "EMETTEUR")
    private Integer emetteurCr;

    /**
     * Le type de flux
     * 
     * @param typeFlux
     *            Le type de flux
     * @return Le type de flux
     */
    @Column(name = "TYPE_FLUX")
    private Integer typeFlux;

    /**
     * L'acquittement ou rejet de contrat/période.
     * 
     * @param codeNature
     *            L'acquittement ou rejet de contrat/période.
     * @return L'acquittement ou rejet de contrat/période
     */
    @Column(name = "CODE_NATURE")
    private Integer codeNature;

    /**
     * Le groupe de gestion des cotisations
     * 
     * @param groupeGestionCotisation
     *            Le groupe de gestion des cotisations
     * @return Le groupe de gestion des cotisations
     */
    @Column(name = "GRP_GEST_COT")
    private String groupeGestionCotisation;

    /**
     * La référence de paiement
     * 
     * @param referencePaiement
     *            La référence de paiement
     * @return La référence de paiement
     */
    @Column(name = "REF_PAIEMENT")
    private String referencePaiement;

    /**
     * Récupère la date de traitement.
     * 
     * @return la date de traitement
     */
    public Date getDateTraitementAsDate() {
        return DateRedac.convertitEnDate(getDateTraitement());
    }

    @Override
    public Long getId() {
        return idTechnique;
    }

}
