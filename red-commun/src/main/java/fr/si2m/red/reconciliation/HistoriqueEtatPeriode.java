package fr.si2m.red.reconciliation;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import fr.si2m.red.DateRedac;
import fr.si2m.red.Entite;
import fr.si2m.red.dsn.EtatPeriode;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * Modèle des historiques des états de période
 * 
 * @author poidij
 *
 */
@Entity
@Table(name = "HISTO_ETAT_PERIODE")
@Data
@EqualsAndHashCode(callSuper = false, of = { "idTechnique" })
@ToString(callSuper = false, of = { "idTechnique", "idPeriode", "dateHeureChangement" })
public class HistoriqueEtatPeriode extends Entite {
    /**
     * UID de version.
     */
    private static final long serialVersionUID = -3324801079594603125L;

    /**
     * identifiant controle modification via l'IHM
     */
    public static final String ORIGINE_CONTROLE_IHM = "IHM";

    /**
     * L'identifiant technique.
     * 
     * @param idTechnique
     *            l'identifiant technique
     * @return l'identifiant technique
     */
    @Id
    @GeneratedValue
    @Column(name = "ID_TECH")
    private Long idTechnique;

    /**
     * L'identifiant technique de la période à laquelle ce message est rattaché.
     * 
     * @param idPeriode
     *            l'identifiant technique de la période
     * @return l'identifiant technique de la période
     */
    @Column(name = "ID_PERIODE")
    private Long idPeriode;

    /**
     * Date / Heure / Minute / Seconde du changement
     * 
     * @param dateHeureChangement
     *            L'horodatage du changement
     * @return L'horodatage du changement
     */
    @Column(name = "DATEHMS_CHANGEMENT")
    private Long dateHeureChangement;

    /**
     * Nouvel état
     * 
     * @param etatPeriode
     *            Le nouvel état
     * @return Le nouvel état
     */
    @Column(name = "ETAT_PERIODE")
    @Enumerated(EnumType.STRING)
    private EtatPeriode etatPeriode;

    /**
     * Code indiquant l'origine du changement d'état
     * 
     * @param origineChangement
     *            Le code indiquant l'origine du changement d'état
     * @return Le code indiquant l'origine du changement d'état
     */
    @Column(name = "ORIGINE")
    private String origineChangement;

    /**
     * Identifiant de l'utilisateur ayant réalisé la modification
     * 
     * @param identifiantUtilisateur
     *            L'identifiant de l'utilisateur ayant réalisé la modification
     * @return L'identifiant de l'utilisateur ayant réalisé la modification
     */
    @Column(name = "UTILISATEUR")
    private String identifiantUtilisateur;

    /**
     * Commentaire utilisateur
     * 
     * @param commentaireUtilisateur
     *            Le commentaire utilisateur
     * @return Le commentaire utilisateur
     */
    @Column(name = "COMMENTAIRE")
    private String commentaireUtilisateur;

    /**
     * Récupère la date-heure REDAC du changement en date-heure.
     * 
     * @return la date-heure du changement
     */
    public String getDateHeureChangementAsDate() {
        if (getDateHeureChangement() != null) {
            return DateRedac.convertionDateRedacSansValidation(getDateHeureChangement());
        }
        return null;
    }

    @Override
    public Long getId() {
        return idTechnique;
    }

}
