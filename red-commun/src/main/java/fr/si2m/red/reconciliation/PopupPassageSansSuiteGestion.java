package fr.si2m.red.reconciliation;

import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.beanutils.BeanUtils;
import org.springframework.util.MultiValueMap;

import fr.si2m.red.RedacUnexpectedException;
import lombok.Getter;
import lombok.Setter;

/**
 * Changements en masse (popup)
 * 
 * @author eudesr
 *
 */
public class PopupPassageSansSuiteGestion implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1669947413702453791L;

    /**
     * Sauvegarde des derniers critères de recherches validés
     */
    @Getter
    @Setter
    String parametresRecherche;

    /**
     * Liste des ID périodes à modifier, concaténés et séparé par une virgule, si cases cochées
     */
    @Getter
    @Setter
    String idsAdhesion;

    /**
     * Nombre de périodes impactées
     */
    @Getter
    @Setter
    Integer nbAdhesion;

    /**
     * Représentation en List-Long- des idsPeriodes , toujours utiliser getIdsPeriodesAsLong() pour init / acceder à la liste
     */
    private List<String> idsAdhesionAsString;

    /**
     * Constructeur par défaut des changements
     */
    public PopupPassageSansSuiteGestion() {
        super();
    }

    /**
     * Constructeur de changements par copie
     * 
     * @param changements
     *            les changements à copier
     */
    public PopupPassageSansSuiteGestion(PopupPassageSansSuiteGestion changements) {
        this();
        try {
            BeanUtils.copyProperties(this, changements);
        } catch (IllegalAccessException | InvocationTargetException e) {
            throw new RedacUnexpectedException("Erreur lors de la copie des changements demandés", e);
        }
    }

    /**
     * Convertit une map de valeurs multiple en changements.
     * 
     * @param criteres
     *            les critères sous forme de map de valeurs multiples
     * @return les changements
     */
    public static PopupPassageSansSuiteGestion from(MultiValueMap<String, Object> criteres) {
        PopupPassageSansSuiteGestion changements = new PopupPassageSansSuiteGestion();
        try {
            BeanUtils.populate(changements, criteres);
        } catch (IllegalAccessException | InvocationTargetException e) {
            throw new RedacUnexpectedException("Erreur lors du mapping des changements", e);
        }

        return changements;
    }

    /**
     * Initialise et renvoie la liste des IdsPeriodes sous forme de List-Long-
     * 
     * @return la liste des IdsPeriodes comme List-Long-
     */
    public List<String> getIdsAdhesionAsString() {

        if (idsAdhesionAsString == null) {
            setIdsPeriodesAsString();
        }
        return idsAdhesionAsString;
    }

    /**
     * Initialise la liste
     */
    private void setIdsPeriodesAsString() {
        idsAdhesionAsString = new ArrayList<String>();
        if (idsAdhesion != null) {
            for (String idAdhesion : Arrays.asList(idsAdhesion.split(","))) {
                idsAdhesionAsString.add(idAdhesion);
            }
        }

    }

}
