package fr.si2m.red.reconciliation;

import java.util.List;
import java.util.Map;

import fr.si2m.red.core.repository.RedacRepository;

/**
 * Référentiel des historiques des états de périodes.
 * 
 * @author poidij
 *
 */
public interface HistoriqueEtatPeriodeRepository extends RedacRepository<HistoriqueEtatPeriode> {

    /**
     * Récupère l'historique des états d'une période.
     * 
     * @param idPeriode
     *            l'identifiant de la période
     * @return l'historique des états pour cette période
     */
    List<HistoriqueEtatPeriode> getHistoriqueEtatsPeriode(Long idPeriode);

    /**
     * Récupère un historique donné d'une période.
     * 
     * @param idPeriode
     *            l'identifiant de la période
     * @param dateHeureChangement
     *            l'horodatage de l'historique recherché
     * @return l'historique recherché
     */
    HistoriqueEtatPeriode getHistoriqueEtatPeriodePourMessage(Long idPeriode, Long dateHeureChangement);

    /**
     * Récupère le dernier historique d'une période donnée.
     * 
     * @param idPeriode
     *            l'identifiant de la période
     * @return la période concernée
     */
    HistoriqueEtatPeriode getDernierHistoriquePourPeriode(Long idPeriode);

    /**
     * Supprime l'historique d'états pour une période donnée.
     * 
     * @param idPeriode
     *            l'identifiant de la période
     * @return le nombre d'éléments supprimés effectivement dans le référentiel
     */
    int supprimeHistoriqueEtatPeriodePourPeriode(Long idPeriode);

    /**
     * Exécute une procédure stockée MySQL.
     * 
     * @param sql
     *            la commande d'appel de la procédure
     * @param params
     *            les paramètres d'appels de la procédure
     */
    void callMysqlProcedure(String sql, Map<String, Object> params);

    /**
     * Creation d'historique état periode en masse
     * 
     * @param idsPeriodes
     *            liste des periodes ciblées
     * @param message
     *            message user
     * @param utilisateur
     *            user effectuant l'action
     * @param statutCible
     *            statut cible des periodes
     */
    void creationEnMasseOrigineIHM(List<Long> idsPeriodes, String message, String utilisateur, String statutCible);

}
