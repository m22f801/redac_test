package fr.si2m.red.reconciliation;

import fr.si2m.red.core.repository.RedacRepository;

/**
 * Référentiel des périodes DSN reçues et traitées.
 * 
 * @author benitahy
 *
 */
public interface PeriodeRecueTraiteeRepository extends RedacRepository<PeriodeRecueTraitee> {

    /**
     * Vérifie l'existence d'une période reçue traitée par numéro de contrat et date de début de période.
     * 
     * @param numeroContrat
     *            le numéro de contrat
     * @param dateDebutPeriode
     *            la date de début de période
     * @return true si une telle période existe, false sinon
     */
    boolean existe(String numeroContrat, Integer dateDebutPeriode);

}
