package fr.si2m.red.reconciliation;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import fr.si2m.red.DateRedac;
import fr.si2m.red.Entite;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * Modèle des historiques des commentaires de période
 * 
 * @author gahagnont
 *
 */
@Entity
@Table(name = "HISTO_COMMENTAIRE_PERIODE")
@Data
@EqualsAndHashCode(callSuper = false, of = { "idTechnique" })
@ToString(callSuper = false, of = { "idTechnique", "idPeriode", "dateHeureChangement" })
public class HistoriqueCommentairePeriode extends Entite {
    /**
     * UID de version.
     */
    private static final long serialVersionUID = -3324801079594603125L;

    /**
     * identifiant controle modification via l'IHM
     */
    public static final String ORIGINE_CONTROLE_IHM = "IHM";

    /**
     * L'identifiant technique.
     * 
     * @param idTechnique
     *            l'identifiant technique
     * @return l'identifiant technique
     */
    @Id
    @GeneratedValue
    @Column(name = "ID_TECH")
    private Long idTechnique;

    /**
     * L'identifiant technique de la période à laquelle ce commentaire est rattaché.
     * 
     * @param idPeriode
     *            l'identifiant technique de la période
     * @return l'identifiant technique de la période
     */
    @Column(name = "ID_PERIODE")
    private Long idPeriode;

    /**
     * Date / Heure / Minute / Seconde de la saisie
     * 
     * @param dateHeureSaisie
     *            L'horodatage de la saisie
     * @return L'horodatage de la saisie
     */
    @Column(name = "DATEHMS_SAISIE")
    private Long dateHeureSaisie;

    /**
     * Identifiant de l'utilisateur ayant saisi le commentaire
     * 
     * @param identifiantUtilisateur
     *            L'identifiant de l'utilisateur ayant saisi le commentaire
     * @return L'identifiant de l'utilisateur ayant saisi le commentaire
     */
    @Column(name = "UTILISATEUR")
    private String identifiantUtilisateur;

    /**
     * Commentaire utilisateur
     * 
     * @param commentaireUtilisateur
     *            Le commentaire utilisateur
     * @return Le commentaire utilisateur
     */
    @Column(name = "COMMENTAIRE")
    private String commentaireUtilisateur;

    /**
     * Récupère la date-heure REDAC du changement en date-heure.
     * 
     * @return la date-heure du changement
     */
    public String getDateHeureSaisieAsDate() {
        if (getDateHeureSaisie() != null) {
            return DateRedac.convertionDateRedacSansValidation(getDateHeureSaisie());
        }
        return null;
    }

    @Override
    public Long getId() {
        return idTechnique;
    }

}
