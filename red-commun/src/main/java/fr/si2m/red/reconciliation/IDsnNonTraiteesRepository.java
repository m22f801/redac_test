package fr.si2m.red.reconciliation;

import java.util.List;

import fr.si2m.red.core.repository.RedacRepository;

/**
 * Référentiel des DSN non traites.
 * 
 * @author delortj
 *
 */
public interface IDsnNonTraiteesRepository extends RedacRepository<AdhesionEtabMois> {

    /**
     * Récupère la liste des DSN non traitées à destination de l'exportation exel
     * @param criteresRecherche les critères de recherche
     * @return La liste des DSN non traitées correspondant aux critères de recherche
     */
    List<DsnNonTraitees> rechercheDsnNonTraiteesExportExcel(DsnNonTraiteesCriteresRecherche criteresRecherche);

    /**
     * Récupère la liste des DSN non traitées à destination de l'affichage sur l'IHM
     * @param criteresRecherche les critères de recherche
     * @return La liste des DSN non traitées correspondant aux critères de recherche
     */
    List<DsnNonTraitees> rechercheDsnNonTraiteesIHM(DsnNonTraiteesCriteresRecherche criteresRecherche);

    /**
     * Récupère le nombre de DSN non traitées renvoyer par la dernière requête sans prendre en compte la pagination
     * @return Le nombre de DSN non traitées
     */
    long countResults();

    /**
     * Permet de classer sans suite de gestion les DSN non traitée
     * @param idsAdhesions La liste des identifiants adhesion_etablissement_mois concerner
     * @param userIHM L'utilisateur qui opère la mise à jour
     */
    void setAdhesionSSG(List<String> idsAdhesions, String userIHM);

    /**
     * Retourne le nombre d'adhesion vide parmis la liste d'id donnée
     * @param idsAdhesions La liste d'id adhesion à vérifier
     * @return Le nombre d'adhesion vide
     */
    long getAdhesionVide(List<String> idsAdhesions);
}
