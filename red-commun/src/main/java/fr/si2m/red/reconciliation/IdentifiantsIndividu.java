package fr.si2m.red.reconciliation;

import java.io.Serializable;

import org.apache.commons.lang3.StringUtils;

import fr.si2m.red.dsn.Individu;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * Identifiant individu (numéro technique temporaire ou identifiant de répertoire )
 * 
 * @author nortaina, eudesr
 *
 */
@Data
@EqualsAndHashCode(of = { "identifiantRepertoireOuNumeroTechniqueTemporaire" })
@ToString(of = { "identifiantRepertoireOuNumeroTechniqueTemporaire", "isidentifiantRepertoire" })
@AllArgsConstructor
public class IdentifiantsIndividu implements Serializable {

    /**
     * UID de version.
     */
    private static final long serialVersionUID = -5834978406021182871L;

    /**
     * L'identifiant de l'individu dans le répertoire des individus Ou NumeroTechniqueTemporaire
     *
     * @param identifiantRepertoire
     *            l'identifiant de l'individu dans le répertoire des individus Ou NumeroTechniqueTemporaire
     * @return l'identifiant de l'individu dans le répertoire des individus Ou NumeroTechniqueTemporaire
     */
    private String identifiantRepertoireOuNumeroTechniqueTemporaire;

    /**
     * Flag indiquant si identifiantRepertoireOuNumeroTechniqueTemporaire est un identifiant de repetoire ou NTT
     */
    private boolean isidentifiantRepertoire;

    /**
     * Constructeur par défaut
     */
    public IdentifiantsIndividu() {
    }

    /**
     * Constructeur de la classe IdentifiantsIndividu
     * 
     * @param identifiantRepertoire
     *            l'identifiant répertoire de l'invididu
     * @param NumeroTechniqueTemporaire
     *            le numéro technique temporaire de l'individu
     */
    public IdentifiantsIndividu(String identifiantRepertoire, String numeroTechniqueTemporaire) {
        super();

        this.isidentifiantRepertoire = StringUtils.isNotBlank(identifiantRepertoire);

        if (this.isidentifiantRepertoire) {
            this.identifiantRepertoireOuNumeroTechniqueTemporaire = identifiantRepertoire;
        } else {
            this.identifiantRepertoireOuNumeroTechniqueTemporaire = numeroTechniqueTemporaire;
        }
    }

    /**
     * Vérifie que ces identifiants identifient bien un individu donné.
     * 
     * @param individu
     *            l'individu à vérifier
     * @return true si ces identifiants s'appliquent à l'individu
     */
    public boolean identifie(Individu individu) {
        if (this.isIsidentifiantRepertoire()) {
            return StringUtils.equals(this.getIdentifiantRepertoireOuNumeroTechniqueTemporaire(), individu.getIdentifiantRepertoire());
        } else {
            return StringUtils.equals(this.getIdentifiantRepertoireOuNumeroTechniqueTemporaire(), individu.getNumeroTechniqueTemporaire());
        }
    }

}
