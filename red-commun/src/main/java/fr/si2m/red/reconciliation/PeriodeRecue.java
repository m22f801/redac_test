package fr.si2m.red.reconciliation;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import fr.si2m.red.DateRedac;
import fr.si2m.red.Entite;
import fr.si2m.red.core.repository.jpa.conversion.ConvertisseurBooleanON;
import fr.si2m.red.dsn.EtatPeriode;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * Modèle des périodes reçues.
 * 
 * @author poidij
 *
 */
@Entity
@Table(name = "PERIODES_RECUES")
@Data
@EqualsAndHashCode(callSuper = false, of = { "idPeriode" })
@ToString(callSuper = false, of = { "idPeriode" })
public class PeriodeRecue extends Entite {
    /**
    * 
    */
    private static final long serialVersionUID = 8442803727984015042L;

    /**
     * Type de période "déclaration".
     */
    public static final String TYPE_PERIODE_DECLARATION = "DEC";

    /**
     * Type de période "complément".
     */
    public static final String TYPE_PERIODE_COMPLEMENT = "CPL";

    /**
     * Type de période "régularisation".
     */
    public static final String TYPE_PERIODE_REGULARISATION = "REG";

    /**
     * L'identifiant technique de la période.
     * 
     * @param idPeriode
     *            l'identifiant technique
     * @return l'identifiant technique
     */
    @Id
    @GeneratedValue
    @Column(name = "ID_PERIODE")
    private Long idPeriode;

    /**
     * Type de la période.
     * 
     * @param typePeriode
     *            Le type de la période
     * @return Le type de la période
     * 
     */
    @Column(name = "TYPE_PERIODE")
    private String typePeriode;

    /**
     * Date d'ouverture de la période.
     * 
     * @param dateCreation
     *            La date de creation de la déclaration
     * @return La date de creation de la déclaration
     */
    @Column(name = "DATE_CREATION")
    private Integer dateCreation;

    /**
     * Le numéro du contrat auquel cette période est rattachée. Correspond au NOCO
     * 
     * @param numeroContrat
     *            Le numéro du contrat complet
     * @return Le numéro du contrat complet
     */
    @Column(name = "NUMERO_CONTRAT")
    private String numeroContrat;

    /**
     * Date de début de la période.
     * 
     * @param dateDebutPeriode
     *            La date de début de période
     * @return La date de début de période
     */
    @Column(name = "DATE_DEBUT_PERIODE")
    private Integer dateDebutPeriode;

    /**
     * Date de fin de la période.
     * 
     * @param dateFinPeriode
     *            La date de fin de période
     * @return La date de fin de période
     */
    @Column(name = "DATE_FIN_PERIODE")
    private Integer dateFinPeriode;

    /**
     * Indique si le premier mois de la période de déclaration est reçu ou non.
     * 
     * @param premierMoisDecl
     *            L'indication de la réception du premier mois
     * @return L'indication de la réception du premier mois
     */
    @Column(name = "PREMIER_MOIS_DECL")
    private String premierMoisDeclAsText;

    /**
     * Etat de la période.
     * 
     * @param etatPeriode
     *            l'état de la période
     * @return l'état de la période
     * 
     */
    @Column(name = "ETAT_PERIODE")
    @Enumerated(EnumType.STRING)
    private EtatPeriode etatPeriode;

    /**
     * Indique si les éléments calculés pour cette période sont bien à jour ou mérite recalcul.
     * 
     * @param reconsolider
     *            L'indication de la nécessité d'un recalcul
     * @return L'indication de la nécessité d'un recalcul
     */
    @Column(name = "RECONSOLIDER")
    private String reconsoliderAsText;

    /**
     * Date d’échéance appliquée aux établissements de l’entreprise.
     * 
     * @param dateEcheance
     *            La date d'écheance
     * @return La date d'écheance au format AAAAMMJJ
     */
    @Column(name = "DATE_ECHEANCE")
    private Integer dateEcheance;

    /**
     * Affectation des periodes reçues à un utilisateur
     * 
     * @param affecteA
     *            Reprise de la valeur du champ « Affecté à » de la période antérieure Si période antérieure n’existe pas : laissé à vide
     * @return L'utilisateur affectué au traitement
     */
    @Column(name = "AFFECTE_A")
    private String affecteA;

    /**
     * Affectation de l'utilisateur qui doit traiter les champs
     * 
     * @param aTraiterPar
     *            Reprise de la valeur du champ « Affecté à » venant d’être calculé
     * @return L'utilisateur qui doit traiter les champs
     */
    @Column(name = "A_TRAITER_PAR")
    private String aTraiterPar;

    /**
     * Affectation de l'utilisateur qui doit traiter les champs
     * 
     * @param aTraiterPar
     *            Champs vide
     * @return L'utilisateur qui a traité les périodes reçues
     */
    @Column(name = "TRAITE_PAR")
    private String traitePar;

    /**
     * Indique si le premier mois de la période de déclaration est reçu ou non.
     * 
     * @return L'indication de la réception du premier mois
     */
    public boolean isPremierMoisDecl() {
        return new ConvertisseurBooleanON().convertToEntityAttribute(getPremierMoisDeclAsText());
    }

    /**
     * Indique si le premier mois de la période de déclaration est reçu ou non.
     * 
     * @param premierMoisDecl
     *            L'indication de la réception du premier mois
     */
    public void setPremierMoisDecl(boolean premierMoisDecl) {
        setPremierMoisDeclAsText(new ConvertisseurBooleanON().convertToDatabaseColumn(premierMoisDecl));
    }

    /**
     * Indique si les éléments calculés pour cette période sont bien à jour ou mérite recalcul.
     * 
     * @return L'indication de la nécessité d'un recalcul
     */
    public boolean isReconsolider() {
        return new ConvertisseurBooleanON().convertToEntityAttribute(getReconsoliderAsText());
    }

    /**
     * Indique si les éléments calculés pour cette période sont bien à jour ou mérite recalcul.
     * 
     * @param reconsolider
     *            L'indication de la nécessité d'un recalcul
     */
    public void setReconsolider(boolean reconsolider) {
        setReconsoliderAsText(new ConvertisseurBooleanON().convertToDatabaseColumn(reconsolider));
    }

    /**
     * Récupère la date de début de période.
     * 
     * @return la date de début de période
     */
    public Date getDateDebutPeriodeAsDate() {
        return DateRedac.convertitEnDate(getDateDebutPeriode());
    }

    /**
     * Récupère la date de fin de période.
     * 
     * @return la date de fin de période
     */
    public Date getDateFinPeriodeAsDate() {
        return DateRedac.convertitEnDate(getDateFinPeriode());
    }

    /**
     * Récupère la date de création de la période.
     * 
     * @return la date de création de la période
     */
    public Date getDateCreationAsDate() {
        return DateRedac.convertitEnDate(getDateCreation());
    }

    @Override
    public Long getId() {
        return idPeriode;
    }

}
