package fr.si2m.red.reconciliation;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import fr.si2m.red.Entite;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * Modèle des messages de contrôle.
 * 
 * @author poidij
 *
 */
@Entity
@Table(name = "MESSAGE_CONTROLE")
@Data
@EqualsAndHashCode(callSuper = false, of = { "idMessageControle" })
@ToString(callSuper = false, of = { "idMessageControle" })
public class MessageControle extends Entite {
    /**
     * UID de version.
     */
    private static final long serialVersionUID = -3525411674080848357L;

    /**
     * Origine des messages générés par REDAC.
     * 
     * @see #getOrigineMessage()
     */
    public static final String ORIGINE_MESSAGE_REDAC = "REDAC";

    /**
     * Origine des messages générés par un SIAVAL.
     * 
     * @see #getOrigineMessage()
     */
    public static final String ORIGINE_MESSAGE_SIAVAL = "SIAVAL";

    /**
     * Origine des messages générés par un Gestionnaire.
     * 
     * @see #getOrigineMessage()
     */
    public static final String ORIGINE_MESSAGE_GEST = "GEST";

    /**
     * identifiant controle Rejet Gestionnaire
     */
    public static final String IDENTIFIANT_CONTROLE_GEST_REJET = "GEST_REJET";

    /**
     * identifiant controle mise en attente gestionnaire
     */
    public static final String IDENTIFIANT_CONTROLE_GEST_RETOUR_ETP = "GEST_RETOUR_ETP";

    /**
     * Identifiant technique du message de contrôle.
     * 
     * @param idPeriode
     *            l'identifiant technique du message de contrôle
     * @return l'identifiant technique du message de contrôle
     */
    @Id
    @GeneratedValue
    @Column(name = "ID_MSG_CTRL")
    private Long idMessageControle;

    /**
     * L'identifiant technique de la période à laquelle ce message est rattaché.
     * 
     * @param idPeriode
     *            l'identifiant technique de la période
     * @return l'identifiant technique de la période
     */
    @Column(name = "ID_PERIODE")
    private Long idPeriode;

    /**
     * Identifiant technique du contrôle ayant engendré ce compte-rendu.
     * 
     * @param identifiantControle
     *            L'identifiant de controle
     * @return L'identifiant de controle
     */
    @Column(name = "ID_CONTROLE")
    private String identifiantControle;

    /**
     * Origine du message : REDAC ou le SI aval.
     * 
     * @param origineMessage
     *            L'origine du message
     * @return REDAC ou le SI aval
     */
    @Column(name = "ORIGINE")
    private String origineMessage;

    /**
     * Date d’émission du message.
     * 
     * @param dateTraitement
     *            La date d’émission du message
     * @return La date d’émission du message
     */
    @Column(name = "DATE_TRAITEMENT")
    private Integer dateTraitement;

    /**
     * Code correspondant au niveau d’alerte du contrôle.
     * 
     * @param niveauAlerte
     *            Le code du niveau d’alerte
     * @return Le code du niveau d’alerte
     */
    @Column(name = "NIVEAU_ALERTE")
    private String niveauAlerte;

    /**
     * NIR/NTT de l’individu sur lequel porte ce message.
     * 
     * @param individu
     *            Le NIR/NTT de l’individu sur lequel porte ce message
     * @return Le NIR/NTT de l’individu sur lequel porte ce message
     */
    @Column(name = "INDIVIDU")
    private String individu;

    /**
     * Message à publier à l’utilisateur.
     * 
     * @param messageUtilisateur
     *            Le message à publier à l’utilisateur
     * @return Le message à publier à l’utilisateur
     */
    @Column(name = "MESSAGE_UTILISATEUR")
    private String messageUtilisateur;

    /**
     * Paramétrage 1 du message utilisateur.
     * 
     * @param paramMessage1
     *            Le paramétrage 1 du message utilisateur
     * @return Le paramétrage 1 du message utilisateur
     */
    @Column(name = "PARAM1")
    private String paramMessage1;

    /**
     * Paramétrage 2 du message utilisateur.
     * 
     * @param paramMessage2
     *            Le paramétrage 2 du message utilisateur
     * @return Le paramétrage 2 du message utilisateur
     */
    @Column(name = "PARAM2")
    private String paramMessage2;

    /**
     * Paramétrage 3 du message utilisateur.
     * 
     * @param paramMessage3
     *            Le paramétrage 3 du message utilisateur
     * @return Le paramétrage 3 du message utilisateur
     */
    @Column(name = "PARAM3")
    private String paramMessage3;

    /**
     * Paramétrage 4 du message utilisateur.
     * 
     * @param paramMessage4
     *            Le paramétrage 4 du message utilisateur
     * @return Le paramétrage 4 du message utilisateur
     */
    @Column(name = "PARAM4")
    private String paramMessage4;

    @Override
    public Long getId() {
        return idMessageControle;
    }

}
