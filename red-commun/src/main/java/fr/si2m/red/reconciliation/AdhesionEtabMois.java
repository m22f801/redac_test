package fr.si2m.red.reconciliation;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

import fr.si2m.red.EntiteImportableBatch;
import fr.si2m.red.dsn.AdhesionEtablissementMoisId;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * Modèle des périodes reçues.
 * 
 * @author poidij
 *
 */
@Entity
@Table(name = "ADHESION_ETABLISSEMENT_MOIS")
@IdClass(AdhesionEtablissementMoisId.class)
@Data
@EqualsAndHashCode(callSuper = false, of = { "ligneEnCoursImportBatch", "idAdhEtabMois" })
@ToString(callSuper = false, of = { "ligneEnCoursImportBatch", "idAdhEtabMois" })

public class AdhesionEtabMois extends EntiteImportableBatch {

    /**
     * 
     */
    private static final long serialVersionUID = 1232962583319316037L;

    /**
     * Indicateur de ligne temporaire en cours d'import via Batch.
     * 
     * @param ligneEnCoursImportBatch
     *            le nom du SI d'où provient l'information
     * @return le nom du SI d'où provient l'information
     */
    @Id
    @Column(name = "TMP_BATCH")
    private boolean ligneEnCoursImportBatch;

    /**
     * ID_ADH_ETAB_MOIS
     * 
     * @param idEtabMois
     *            l'identifiant technique
     * @return l'identifiant technique
     */
    @Id
    @GeneratedValue
    @Column(name = "ID_ADH_ETAB_MOIS")
    private String idAdhEtabMois;

    /**
     * codeEssaiReel
     * 
     * @param codeEssaiReel
     *            Le type de codeEssaiReel
     * @return Le type de codeEssaiReel
     * 
     */
    @Column(name = "CODE_ESSAI_REEL")
    private String codeEssaiReel;

    /**
     * codeEssaiReel
     * 
     * @param codeEssaiReel
     *            Le type de codeEssaiReel
     * @return Le type de codeEssaiReel
     * 
     */
    @Column(name = "TYPE_ENVOI")
    private String typeEnvoi;

    /**
     * codeEssaiReel
     * 
     * @param codeEssaiReel
     *            Le type de codeEssaiReel
     * @return Le type de codeEssaiReel
     * 
     */
    @Column(name = "SIREN_EMETTEUR")
    private String sirenEmetteur;

    /**
     * codeEssaiReel
     * 
     * @param codeEssaiReel
     *            Le type de codeEssaiReel
     * @return Le type de codeEssaiReel
     * 
     */
    @Column(name = "NIC_EMETEUR")
    private String nicEmetteur;

    /**
     * codeEssaiReel
     * 
     * @param codeEssaiReel
     *            Le type de codeEssaiReel
     * @return Le type de codeEssaiReel
     * 
     */
    @Column(name = "NOM_EMETEUR")
    private String nomEmetteur;

    /**
     * codeEssaiReel
     * 
     * @param codeEssaiReel
     *            Le type de codeEssaiReel
     * @return Le type de codeEssaiReel
     * 
     */
    @Column(name = "VOIE_EMETEUR")
    private String voieEmetteur;

    /**
     * codeEssaiReel
     * 
     * @param codeEssaiReel
     *            Le type de codeEssaiReel
     * @return Le type de codeEssaiReel
     * 
     */
    @Column(name = "CODE_POSTAL_EMETTEUR")
    private String cpEmetteur;

    /**
     * codeEssaiReel
     * 
     * @param codeEssaiReel
     *            Le type de codeEssaiReel
     * @return Le type de codeEssaiReel
     * 
     */
    @Column(name = "COMPLEMENT_CONSTRUCTION_EMETTEUR")
    private String complementConstructionEmetteur;

    /**
     * codeEssaiReel
     * 
     * @param codeEssaiReel
     *            Le type de codeEssaiReel
     * @return Le type de codeEssaiReel
     * 
     */
    @Column(name = "COMPLEMENT_VOIE_EMETTEUR")
    private String complementVoieEmetteur;

    /**
     * codeEssaiReel
     * 
     * @param codeEssaiReel
     *            Le type de codeEssaiReel
     * @return Le type de codeEssaiReel
     * 
     */
    @Column(name = "CIVILITE_CONTACT")
    private String civiliteContact;

    /**
     * codeEssaiReel
     * 
     * @param codeEssaiReel
     *            Le type de codeEssaiReel
     * @return Le type de codeEssaiReel
     * 
     */
    @Column(name = "NOM_CONTACT")
    private String nomContact;

    /**
     * codeEssaiReel
     * 
     * @param codeEssaiReel
     *            Le type de codeEssaiReel
     * @return Le type de codeEssaiReel
     * 
     */
    @Column(name = "MAIL_CONTACT")
    private String mailContact;

    /**
     * codeEssaiReel
     * 
     * @param codeEssaiReel
     *            Le type de codeEssaiReel
     * @return Le type de codeEssaiReel
     * 
     */
    @Column(name = "TEL_CONTACT")
    private String telContact;

    /**
     * codeEssaiReel
     * 
     * @param codeEssaiReel
     *            Le type de codeEssaiReel
     * @return Le type de codeEssaiReel
     * 
     */
    @Column(name = "FAX_CONTACT")
    private String faxContact;

    /**
     * codeEssaiReel
     * 
     * @param codeEssaiReel
     *            Le type de codeEssaiReel
     * @return Le type de codeEssaiReel
     * 
     */
    @Column(name = "TYPE_DECLARATION")
    private String typeDeclaration;

    /**
     * codeEssaiReel
     * 
     * @param codeEssaiReel
     *            Le type de codeEssaiReel
     * @return Le type de codeEssaiReel
     * 
     */
    @Column(name = "NUMERO_ORDRE_DECLARATION")
    private Integer numOrdreDeclaration;

    /**
     * codeEssaiReel
     * 
     * @param codeEssaiReel
     *            Le type de codeEssaiReel
     * @return Le type de codeEssaiReel
     * 
     */
    @Column(name = "MOIS_DECLARE")
    private Integer moisDeclare;

    /**
     * codeEssaiReel
     * 
     * @param codeEssaiReel
     *            Le type de codeEssaiReel
     * @return Le type de codeEssaiReel
     * 
     */
    @Column(name = "MOIS_RATTACHEMENT")
    private Integer moisRattachement;

    /**
     * codeEssaiReel
     * 
     * @param codeEssaiReel
     *            Le type de codeEssaiReel
     * @return Le type de codeEssaiReel
     * 
     */
    @Column(name = "DATE_FICHIER")
    private Integer dateFichier;

    /**
     * codeEssaiReel
     * 
     * @param codeEssaiReel
     *            Le type de codeEssaiReel
     * @return Le type de codeEssaiReel
     * 
     */
    @Column(name = "CHAMP_DECLARATION")
    private String champDeclaration;

    /**
     * codeEssaiReel
     * 
     * @param codeEssaiReel
     *            Le type de codeEssaiReel
     * @return Le type de codeEssaiReel
     * 
     */
    @Column(name = "IDENTIFIANT_METIER")
    private String identifiantMetier;

    /**
     * codeEssaiReel
     * 
     * @param codeEssaiReel
     *            Le type de codeEssaiReel
     * @return Le type de codeEssaiReel
     * 
     */
    @Column(name = "DEVISE_DECLARATION")
    private String deviseDeclaration;

    /**
     * codeEssaiReel
     * 
     * @param codeEssaiReel
     *            Le type de codeEssaiReel
     * @return Le type de codeEssaiReel
     * 
     */
    @Column(name = "SIREN_ENTREPRISE")
    private String sirenEntreprise;

    /**
     * codeEssaiReel
     * 
     * @param codeEssaiReel
     *            Le type de codeEssaiReel
     * @return Le type de codeEssaiReel
     * 
     */
    @Column(name = "NIC_ENTREPRISE")
    private String nicEntreprise;

    /**
     * codeEssaiReel
     * 
     * @param codeEssaiReel
     *            Le type de codeEssaiReel
     * @return Le type de codeEssaiReel
     * 
     */
    @Column(name = "CODE_APEN")
    private String codeApen;

    /**
     * codeEssaiReel
     * 
     * @param codeEssaiReel
     *            Le type de codeEssaiReel
     * @return Le type de codeEssaiReel
     * 
     */
    @Column(name = "VOIE_ENTREPRISE")
    private String voieEntreprise;

    /**
     * codeEssaiReel
     * 
     * @param codeEssaiReel
     *            Le type de codeEssaiReel
     * @return Le type de codeEssaiReel
     * 
     */
    @Column(name = "CODE_POSTAL_ENTREPRISE")
    private String cpEntreprise;

    /**
     * codeEssaiReel
     * 
     * @param codeEssaiReel
     *            Le type de codeEssaiReel
     * @return Le type de codeEssaiReel
     * 
     */
    @Column(name = "LOCALITE_ENTREPRISE")
    private String localiteEntreprise;

    /**
     * codeEssaiReel
     * 
     * @param codeEssaiReel
     *            Le type de codeEssaiReel
     * @return Le type de codeEssaiReel
     * 
     */
    @Column(name = "COMPLEMENT_CONSTRUCTION_ENTREPRISE")
    private String complementConstructionEntreprise;

    /**
     * codeEssaiReel
     * 
     * @param codeEssaiReel
     *            Le type de codeEssaiReel
     * @return Le type de codeEssaiReel
     * 
     */
    @Column(name = "COMPLEMENT_VOIE_ENTREPRISE")
    private String complementVoieEntreprise;

    /**
     * codeEssaiReel
     * 
     * @param codeEssaiReel
     *            Le type de codeEssaiReel
     * @return Le type de codeEssaiReel
     * 
     */
    @Column(name = "EFFECTIF_MOYEN_ENTREPRISE")
    private Integer effectifMoyenEntreprise;

    /**
     * codeEssaiReel
     * 
     * @param codeEssaiReel
     *            Le type de codeEssaiReel
     * @return Le type de codeEssaiReel
     * 
     */
    @Column(name = "PAYS_ENTREPRISE")
    private String paysEntreprise;

    /**
     * codeEssaiReel
     * 
     * @param codeEssaiReel
     *            Le type de codeEssaiReel
     * @return Le type de codeEssaiReel
     * 
     */
    @Column(name = "CODE_DISTRIB_ENTREPRISE")
    private String codeDistribEntreprise;

    /**
     * codeEssaiReel
     * 
     * @param codeEssaiReel
     *            Le type de codeEssaiReel
     * @return Le type de codeEssaiReel
     * 
     */
    @Column(name = "IMPLANTATION_ENTREPRISE")
    private String implantationEntreprise;

    /**
     * codeEssaiReel
     * 
     * @param codeEssaiReel
     *            Le type de codeEssaiReel
     * @return Le type de codeEssaiReel
     * 
     */
    @Column(name = "RAISON_SOCIALE_ENTREPRISE")
    private String raisonSocialEntreprise;

    /**
     * codeEssaiReel
     * 
     * @param codeEssaiReel
     *            Le type de codeEssaiReel
     * @return Le type de codeEssaiReel
     * 
     */
    @Column(name = "NIC_ETABLISSEMENT")
    private String nicEtablissement;

    /**
     * codeEssaiReel
     * 
     * @param codeEssaiReel
     *            Le type de codeEssaiReel
     * @return Le type de codeEssaiReel
     * 
     */
    @Column(name = "CODE_APET")
    private String codeApet;

    /**
     * codeEssaiReel
     * 
     * @param codeEssaiReel
     *            Le type de codeEssaiReel
     * @return Le type de codeEssaiReel
     * 
     */
    @Column(name = "VOIE_ETABLISSEMENT")
    private String voieEtablissement;

    /**
     * codeEssaiReel
     * 
     * @param codeEssaiReel
     *            Le type de codeEssaiReel
     * @return Le type de codeEssaiReel
     * 
     */
    @Column(name = "CODE_POSTAL_ETABLISSEMENT")
    private String cpEtablissement;

    /**
     * codeEssaiReel
     * 
     * @param codeEssaiReel
     *            Le type de codeEssaiReel
     * @return Le type de codeEssaiReel
     * 
     */
    @Column(name = "LOCALITE_ETABLISSEMENT")
    private String lovaliteEtablissement;

    /**
     * codeEssaiReel
     * 
     * @param codeEssaiReel
     *            Le type de codeEssaiReel
     * @return Le type de codeEssaiReel
     * 
     */
    @Column(name = "COMPLEMENT_CONSTRUCTION_ETABLISSEMENT")
    private String complementConstructionEtablissement;

    /**
     * codeEssaiReel
     * 
     * @param codeEssaiReel
     *            Le type de codeEssaiReel
     * @return Le type de codeEssaiReel
     * 
     */
    @Column(name = "COMPLEMENT_VOIE_ETABLISSEMENT")
    private String complementVoieEtablissement;

    /**
     * codeEssaiReel
     * 
     * @param codeEssaiReel
     *            Le type de codeEssaiReel
     * @return Le type de codeEssaiReel
     * 
     */
    @Column(name = "EFFECTIF_FIN_PERIODE_ETABLISSEMENT")
    private Integer effectifFinPeriodeEtablissement;

    /**
     * codeEssaiReel
     * 
     * @param codeEssaiReel
     *            Le type de codeEssaiReel
     * @return Le type de codeEssaiReel
     * 
     */
    @Column(name = "PAYS_ETABLISSEMENT")
    private String paysEtablissement;

    /**
     * codeEssaiReel
     * 
     * @param codeEssaiReel
     *            Le type de codeEssaiReel
     * @return Le type de codeEssaiReel
     * 
     */
    @Column(name = "CODE_DISTRIB_ETABLISSEMENT")
    private String codeDistribEtablissement;

    /**
     * codeEssaiReel
     * 
     * @param codeEssaiReel
     *            Le type de codeEssaiReel
     * @return Le type de codeEssaiReel
     * 
     */
    @Column(name = "NATURE_JURIDIQUE_ETABLISSEMENT")
    private String natureJuridiqueEtablissement;

    /**
     * codeEssaiReel
     * 
     * @param codeEssaiReel
     *            Le type de codeEssaiReel
     * @return Le type de codeEssaiReel
     * 
     */
    @Column(name = "CODE_INSEE")
    private String codeInsee;

    /**
     * codeEssaiReel
     * 
     * @param codeEssaiReel
     *            Le type de codeEssaiReel
     * @return Le type de codeEssaiReel
     * 
     */
    @Column(name = "DATE_ECHEANCE")
    private String dateEcheance;

    /**
     * codeEssaiReel
     * 
     * @param codeEssaiReel
     *            Le type de codeEssaiReel
     * @return Le type de codeEssaiReel
     * 
     */
    @Column(name = "CATEGORIE_JURIDIQUE")
    private String categorieJuridique;

    /**
     * codeEssaiReel
     * 
     * @param codeEssaiReel
     *            Le type de codeEssaiReel
     * @return Le type de codeEssaiReel
     * 
     */
    @Column(name = "ENSEIGNE_ETABLISSEMENT")
    private String enseigneEtablissement;

    /**
     * codeEssaiReel
     * 
     * @param codeEssaiReel
     *            Le type de codeEssaiReel
     * @return Le type de codeEssaiReel
     * 
     */
    @Column(name = "REFERENCE_CONTRAT")
    private String referenceContrat;

    /**
     * codeEssaiReel
     * 
     * @param codeEssaiReel
     *            Le type de codeEssaiReel
     * @return Le type de codeEssaiReel
     * 
     */
    @Column(name = "CODE_ORGANISME")
    private String codeOrganisme;

    /**
     * codeEssaiReel
     * 
     * @param codeEssaiReel
     *            Le type de codeEssaiReel
     * @return Le type de codeEssaiReel
     * 
     */
    @Column(name = "CODE_DELEG_COT")
    private String codeDelegCot;

    /**
     * codeEssaiReel
     * 
     * @param codeEssaiReel
     *            Le type de codeEssaiReel
     * @return Le type de codeEssaiReel
     * 
     */
    @Column(name = "PERSONNEL_COUVERT")
    private String personnelCouvert;

    /**
     * codeEssaiReel
     * 
     * @param codeEssaiReel
     *            Le type de codeEssaiReel
     * @return Le type de codeEssaiReel
     * 
     */
    @Column(name = "USER_CREATION", insertable = false, updatable = false)
    private String userCreation;

    /**
     * codeEssaiReel
     * 
     * @param codeEssaiReel
     *            Le type de codeEssaiReel
     * @return Le type de codeEssaiReel
     * 
     */
    @Column(name = "DT_CREATION", insertable = false, updatable = false)
    private String dtCreation;

    /**
     * codeEssaiReel
     * 
     * @param codeEssaiReel
     *            Le type de codeEssaiReel
     * @return Le type de codeEssaiReel
     * 
     */
    @Column(name = "USER_MISE_A_JOUR", insertable = false, updatable = false)
    private String userMiseAJour;

    /**
     * codeEssaiReel
     * 
     * @param codeEssaiReel
     *            Le type de codeEssaiReel
     * @return Le type de codeEssaiReel
     * 
     */
    @Column(name = "DT_MISE_A_JOUR", insertable = false, updatable = false)
    private String dtMiseAJour;

    /**
     * codeEssaiReel
     * 
     * @param codeEssaiReel
     *            Le type de codeEssaiReel
     * @return Le type de codeEssaiReel
     * 
     */
    @Column(name = "ID_DSN_BRIQUE")
    private Integer idDsnBrique;

    /**
     * codeEssaiReel
     * 
     * @param codeEssaiReel
     *            Le type de codeEssaiReel
     * @return Le type de codeEssaiReel
     * 
     */
    @Column(name = "ID_DSN_ANNULEE")
    private Integer idDsnAnnulee;

    /**
     * Type de la période.
     * 
     * @param typePeriode
     *            Le type de la période
     * @return Le type de la période
     * 
     */
    @Column(name = "DETACHEMENT_TRAITE")
    private String detachementTraite;

    @Override
    public String getId() {
        return idAdhEtabMois;
    }

}
