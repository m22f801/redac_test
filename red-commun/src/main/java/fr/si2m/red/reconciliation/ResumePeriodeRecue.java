package fr.si2m.red.reconciliation;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Formula;
import org.hibernate.annotations.Immutable;

import fr.si2m.red.DateRedac;
import fr.si2m.red.dsn.EtatPeriode;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * Le résumé d'une période reçue.
 * 
 * @author nortaina
 *
 */
@Entity
@Immutable
@Table(name = "PERIODES_RECUES")
@Data
@EqualsAndHashCode(of = "idPeriode")
public class ResumePeriodeRecue {
    /**
     * La liste des statuts modifiables pour une période.
     */
    public static final Set<String> ETATS_MODIFIABLES = new HashSet<>(Arrays.asList(EtatPeriode.ARI.name(), EtatPeriode.INS.name(),
            EtatPeriode.NIN.name(), EtatPeriode.ING.name(), EtatPeriode.SSG.name(), EtatPeriode.TPG.name()));

    /**
     * L'identifiant technique de la période.
     * 
     * @param idPeriode
     *            l'identifiant technique
     * @return l'identifiant technique
     */
    @Id
    @Column(name = "ID_PERIODE")
    private Long idPeriode;
    /**
     * Type de la période.
     * 
     * @param typePeriode
     *            Le type de la période
     * @return Le type de la période
     * 
     */
    @Column(name = "TYPE_PERIODE")
    private String typePeriode;

    /**
     * Etat de la période.
     * 
     * @param etatPeriode
     *            l'état de la période
     * @return l'état de la période
     * 
     */
    @Column(name = "ETAT_PERIODE")
    private String etatPeriode;

    /**
     * Etat de la période.
     * 
     * @param etatPeriode
     *            l'état de la période
     * @return l'état de la période
     * 
     */
    @Formula("(CASE ETAT_PERIODE WHEN 'NIN' THEN 1 WHEN 'INS' THEN 2 WHEN 'ARI' THEN 3 WHEN 'ENC' THEN 4 "
            + "WHEN 'RCP' THEN 5 WHEN 'TPG' THEN 6 WHEN 'SSG' THEN 7 WHEN 'ING' THEN 8 WHEN 'INT' THEN 9 ELSE 10 END)")
    private Integer ordreEtatPeriode;

    /**
     * Etat de la période.
     * 
     * @param etatPeriode
     *            l'état de la période
     * @return l'état de la période
     * 
     */
    @Formula("(CASE WHEN ETAT_PERIODE = 'NIN' AND (SELECT EXISTS (SELECT m.ID_MSG_CTRL FROM MESSAGE_CONTROLE m WHERE m.ID_PERIODE = ID_PERIODE AND m.NIVEAU_ALERTE = 'REJET')) THEN 2 "
            + "WHEN ETAT_PERIODE = 'NIN' AND (SELECT EXISTS (SELECT m.ID_MSG_CTRL FROM MESSAGE_CONTROLE m WHERE m.ID_PERIODE = ID_PERIODE AND m.NIVEAU_ALERTE = 'SIGNAL')) THEN 1 "
            + "ELSE 0 END)")
    private Integer ordreEtatPeriodeNonIntegree;

    /**
     * Le numéro du contrat auquel cette période est rattachée.
     * 
     * @param numeroContrat
     *            Le numéro du contrat complet
     * @return Le numéro du contrat complet
     */
    @Column(name = "NUMERO_CONTRAT")
    private String numeroContrat;
    /**
     * Date de début de la période.
     * 
     * @param dateDebutPeriode
     *            La date de début de période
     * @return La date de début de période
     */
    @Column(name = "DATE_DEBUT_PERIODE")
    private Integer dateDebutPeriode;
    /**
     * Date de fin de la période.
     * 
     * @param dateFinPeriode
     *            La date de fin de période
     * @return La date de fin de période
     */
    @Column(name = "DATE_FIN_PERIODE")
    private Integer dateFinPeriode;

    /**
     * Code user du gestionnaire
     * 
     * @param aTraiterPar
     *            Le code user du gestionnaire
     * @return Le code user du gestionnaire
     */
    @Column(name = "A_TRAITER_PAR")
    private String aTraiterPar;

    /**
     * Code user du gestionnaire
     * 
     * @param traitePar
     *            Le code user du gestionnaire
     * @return Le code user du gestionnaire
     */
    @Column(name = "TRAITE_PAR")
    private String traitePar;

    /**
     * Code user du gestionnaire
     * 
     * @param affecteA
     *            Le code user du gestionnaire
     * @return Le code user du gestionnaire
     */
    @Column(name = "AFFECTE_A")
    private String affecteA;

    /**
     * Le groupe de gestion du contrat rattaché à la période (calculé à partir du référentiel).
     */
    @Formula("(SELECT c.NMGRPGES FROM CONTRATS c WHERE c.NOCO = NUMERO_CONTRAT AND c.TMP_BATCH = 0 "
            + "AND c.DT_DEBUT_SIT <= DATE_FIN_PERIODE AND (coalesce(c.DT_FIN_SIT, 99999999) >= DATE_FIN_PERIODE "
            + "OR (CASE WHEN (c.DT_FIN_SIT = 0) THEN 99999999 ELSE c.DT_FIN_SIT END) >= DATE_FIN_PERIODE) LIMIT 1)")
    private String groupeGestion;

    /**
     * Le libellé court de la famille du contrat rattaché à la période (calculé à partir du référentiel).
     */
    @Formula("(SELECT p.LIBELLE_COURT FROM CONTRATS c, PARAM_CODE_LIBELLE p WHERE c.NOCO = NUMERO_CONTRAT AND c.TMP_BATCH = 0 "
            + "AND p.TBL = 'CONTRATS' AND p.CHAMP = 'NOFAM' AND p.CODE = c.NOFAM "
            + "AND c.DT_DEBUT_SIT <= DATE_FIN_PERIODE AND (coalesce(c.DT_FIN_SIT, 99999999) >= DATE_FIN_PERIODE "
            + "OR (CASE WHEN (c.DT_FIN_SIT = 0) THEN 99999999 ELSE c.DT_FIN_SIT END) >= DATE_FIN_PERIODE) LIMIT 1)")
    private String libelleFamille;

    /**
     * La période représentée (calculé à partir du référentiel).
     */
    @Formula("(SELECT (CASE c.COFRQUIT WHEN 'A' THEN SUBSTR(DATE_DEBUT_PERIODE, 1, 4) "
            + "WHEN 'S' THEN (CASE WHEN (DATE_DEBUT_PERIODE LIKE '____01__') THEN CONCAT(SUBSTR(DATE_DEBUT_PERIODE, 1, 4),'S1') "
            + "ELSE CONCAT(SUBSTR(DATE_DEBUT_PERIODE, 1, 4),'S2') END) "
            + "WHEN 'M' THEN CONCAT(SUBSTR(DATE_DEBUT_PERIODE, 1, 4),'-',SUBSTR(DATE_DEBUT_PERIODE, 5, 2)) "
            + "ELSE (CASE WHEN (DATE_DEBUT_PERIODE LIKE '____01__') THEN CONCAT(SUBSTR(DATE_DEBUT_PERIODE, 1, 4),'T1') "
            + "WHEN (DATE_DEBUT_PERIODE LIKE '____04__') THEN CONCAT(SUBSTR(DATE_DEBUT_PERIODE, 1, 4),'T2') "
            + "WHEN (DATE_DEBUT_PERIODE LIKE '____07__') THEN CONCAT(SUBSTR(DATE_DEBUT_PERIODE, 1, 4),'T3') "
            + "ELSE CONCAT(SUBSTR(DATE_DEBUT_PERIODE, 1, 4),'T4') END) " + "END) FROM CONTRATS c WHERE c.NOCO = NUMERO_CONTRAT AND c.TMP_BATCH = 0 "
            + "AND c.DT_DEBUT_SIT <= DATE_FIN_PERIODE AND (coalesce(c.DT_FIN_SIT, 99999999) >= DATE_FIN_PERIODE "
            + "OR (CASE WHEN (c.DT_FIN_SIT = 0) THEN 99999999 ELSE c.DT_FIN_SIT END) >= DATE_FIN_PERIODE) LIMIT 1)")
    private String periode;

    /**
     * Le libellé court du type de période (calculé à partir du référentiel).
     */
    @Formula("(SELECT p.LIBELLE_COURT FROM PARAM_CODE_LIBELLE p WHERE p.TBL = 'PERIODERECUE' AND p.CHAMP = 'TYPE' AND p.CODE = TYPE_PERIODE)")
    private String libelleType;

    /**
     * Le libellé court du type de période (calculé à partir du référentiel).
     */
    @Formula("(SELECT p.ORDRE FROM PARAM_CODE_LIBELLE p WHERE p.TBL = 'PERIODERECUE' AND p.CHAMP = 'TYPE' AND p.CODE = TYPE_PERIODE)")
    private Long ordreLibelleType;

    /**
     * Le libellé court du type de période (calculé à partir du référentiel).
     */
    @Formula("(SELECT p.LIBELLE_COURT FROM PARAM_CODE_LIBELLE p WHERE p.TBL = 'PERIODERECUE' AND p.CHAMP = 'ETAT' AND p.CODE = ETAT_PERIODE)")
    private String libelleEtat;

    /**
     * La date du dernier changement de statut (calculée à partir du référentiel).
     */
    @Formula("(SELECT h.DATEHMS_CHANGEMENT FROM HISTO_ETAT_PERIODE h WHERE h.ID_PERIODE = ID_PERIODE ORDER BY h.DATEHMS_CHANGEMENT DESC LIMIT 1)")
    private Long derniereDateChangementStatut;

    /**
     * Si la période possède des messages de contrôle de type rejet (calculé à partir du référentiel).
     */
    @Formula("(SELECT EXISTS (SELECT m.ID_MSG_CTRL FROM MESSAGE_CONTROLE m WHERE m.ID_PERIODE = ID_PERIODE AND m.NIVEAU_ALERTE = 'REJET'))")
    private boolean avecMessageRejet;

    /**
     * Si la période possède des messages de contrôle de type signalement (calculé à partir du référentiel).
     */
    @Formula("(SELECT EXISTS (SELECT m.ID_MSG_CTRL FROM MESSAGE_CONTROLE m WHERE m.ID_PERIODE = ID_PERIODE AND m.NIVEAU_ALERTE = 'SIGNAL'))")
    private boolean avecMessageSignal;

    /**
     * Si la période a été bloquée par le gestionnaire
     */
    @Formula("(SELECT EXISTS (SELECT m.ID_MSG_CTRL FROM MESSAGE_CONTROLE m WHERE m.ID_PERIODE = ID_PERIODE  AND m.ID_CONTROLE = 'GEST_REJET' AND m.ORIGINE= 'GEST'))")
    private boolean estBloque;

    /**
     * La raison sociale de l'adhesion etablissement la plus récente rattachée à la période
     */
    @Formula("(SELECT adh.RAISON_SOCIALE_ENTREPRISE FROM ADHESION_ETABLISSEMENT_MOIS adh WHERE adh.TMP_BATCH = 0 AND adh.ID_ADH_ETAB_MOIS IN "
            + "(SELECT r.ID_ADH_ETAB_MOIS FROM RATTACHEMENT_DECLARATIONS_RECUES r WHERE r.ID_PERIODE = ID_PERIODE) "
            + "ORDER BY adh.MOIS_RATTACHEMENT DESC, adh.ID_ADH_ETAB_MOIS DESC LIMIT 1) ")
    private String raisonSociale;

    /**
     * Vérifie si l'état de la période est modifiable ou non.
     * 
     * @return true si l'état de la période est modifiable, false sinon
     */
    public boolean isEtatPeriodeModifiable() {
        return ETATS_MODIFIABLES.contains(getEtatPeriode());
    }

    /**
     * Récupère la date-heure REDAC du dernier changement en date-heure.
     * 
     * @return la date-heure du changement
     */
    public String getDerniereDateChangementStatutAsDate() {
        if (getDerniereDateChangementStatut() != null) {
            return DateRedac.convertionDateRedacSansValidation(getDerniereDateChangementStatut());
        }
        return null;
    }

    /**
     * Renvoi le champ aTraiterPar
     * 
     * @return aTraiterPar
     */
    public String getaTraiterPar() {
        return aTraiterPar;
    }

    /**
     * Set le champ aTraiterPar
     * 
     * @param aTraiterPar
     */
    public void setaTraiterPar(String aTraiterPar) {
        this.aTraiterPar = aTraiterPar;
    }

    /**
     * Renvoi le champ aTraiterPar
     * 
     * @return aTraiterPar
     */
    public String getAffecteA() {
        return affecteA;
    }

    /**
     * Set le champ affecteA
     * 
     * @param affecteA
     */
    public void setAffecteA(String affecteA) {
        this.affecteA = affecteA;
    }

    /**
     * Renvoi le champ traitePar
     * 
     * @return traitePar
     */
    public String getTraitePar() {
        return traitePar;
    }

    /**
     * Set le champ traitePar
     * 
     * @param traitePar
     */
    public void setTraitePar(String traitePar) {
        this.traitePar = traitePar;
    }

}
