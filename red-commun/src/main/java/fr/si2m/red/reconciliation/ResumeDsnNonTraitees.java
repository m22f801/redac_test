package fr.si2m.red.reconciliation;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Formula;
import org.hibernate.annotations.Immutable;

import fr.si2m.red.DateRedac;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * Le résumé d'une adhésion établissement mois.
 * 
 * @author nortaina
 *
 */
@Entity
@Immutable
@Table(name = "ADHESION_ETABLISSEMENT_MOIS")
@EqualsAndHashCode(of = "idAdhEtabMois")
@Data
public class ResumeDsnNonTraitees implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -8073634140422743293L;

    /**
     * L'identifiant de l'établissement.
     * 
     * @param idEtabMois
     *            l'identifiant technique
     * @return l'identifiant technique
     */
    @Id
    @Column(name = "ID_ADH_ETAB_MOIS")
    private String idAdhEtabMois;

    @Id
    @Column(name = "TMP_BATCH")
    private boolean ligneEnCoursImportBatch;
    /**
     * La reférence du contrat.
     * 
     * @param La
     *            ref du contrat La ref du contrat
     * @return La ref du contrat
     * 
     */
    @Column(name = "REFERENCE_CONTRAT")
    private String refContrat;

    /**
     * Le mois de rattachement.
     * 
     * @param La
     *            ref du contrat La ref du contrat
     * @return La ref du contrat
     * 
     */
    @Column(name = "MOIS_RATTACHEMENT")
    private Integer moisRattachement;

    /**
     * Etat de la période.
     * 
     * @param etatPeriode
     *            l'état de la période
     * @return l'état de la période
     * 
     */
    @Column(name = "MOIS_DECLARE")
    private String etatPeriode;

    /**
     * Le siren de l'emetteur
     * 
     * @param siren
     *            siren
     * @return siren
     */
    @Column(name = "SIREN_ENTREPRISE")
    private String sirenEntreprise;

    /**
     * Le siren de l'emetteur
     * 
     * @param siren
     *            siren
     * @return siren
     */
    @Column(name = "NIC_ETABLISSEMENT")
    private String nicEtablissement;

    /**
     * Date de début de la période.
     * 
     * @param dateDebutPeriode
     *            La date de début de période
     * @return La date de début de période
     */

    @Column(name = "RAISON_SOCIALE_ENTREPRISE")
    private String raisonSociale;

    /**
     * Date de fin de la période.
     * 
     * @param dateFinPeriode
     *            La date de fin de période
     * @return La date de fin de période
     */
    @Column(name = "DT_CREATION")
    @Formula("(SELECT date_format(adh.DT_CREATION,'%Y%m%d') FROM ADHESION_ETABLISSEMENT_MOIS adh WHERE adh.ID_ADH_ETAB_MOIS = ID_ADH_ETAB_MOIS)")
    private Integer dateCreation;

    /**
     * Code user du gestionnaire
     * 
     * @param affecteA
     *            Le code user du gestionnaire
     * @return Le code user du gestionnaire
     */
    @Column(name = "DETACHEMENT_TRAITE")
    private String detachementTraite;

    /**
     * Le groupe de gestion du contrat rattaché à la période (calculé à partir du référentiel).
     */
    @Formula("(SELECT c.NMGRPGES FROM CONTRATS c WHERE c.NOCO = REFERENCE_CONTRAT " + " AND c.DT_FIN_SIT is null)")
    private String groupeGestion;

    /**
     * Le libellé court de la famille du contrat rattaché à la période (calculé à partir du référentiel).
     */
    @Formula("(SELECT p.LIBELLE_COURT FROM CONTRATS c, PARAM_CODE_LIBELLE p WHERE c.NOCO = REFERENCE_CONTRAT AND c.TMP_BATCH = 0 "
            + "AND p.TBL = 'CONTRATS' AND p.CHAMP = 'NOFAM' AND p.CODE = c.NOFAM " + " AND c.DT_FIN_SIT is null)")
    private String libelleFamille;

    /**
     * Le libellé court du type de période (calculé à partir du référentiel).
     */
    @Formula("(SELECT p.LIBELLE_COURT FROM PARAM_CODE_LIBELLE p WHERE p.TBL = 'ADHESION_ETAB_MOIS' AND p.CHAMP = 'ETAT' AND p.CODE = COALESCE(DETACHEMENT_TRAITE, 'VIDE'))")
    private String libelleEtat;

    /**
     * La raison sociale de l'adhesion etablissement la plus récente rattachée à la période
     */
    @Formula("(Select SUM(ba.MONTANT_COTISATION) from BASE_ASSUJETTIE ba " + " inner join AFFILIATION af ON (ba.ID_AFFILIATION = af.ID_AFFILIATION) "
            + " inner join CONTRAT_TRAVAIL ct ON (ct.ID_CONTRAT_TRAVAIL = af.ID_CONTRAT_TRAVAIL) "
            + " inner join INDIVIDU ind on (ind.ID_INDIVIDU = ct.ID_INDIVIDU) "
            + " inner join ADHESION_ETABLISSEMENT_MOIS adh on (adh.ID_ADH_ETAB_MOIS = ind.ID_ADH_ETAB_MOIS) "
            + " WHERE adh.ID_ADH_ETAB_MOIS = ID_ADH_ETAB_MOIS)")
    private Double mtCotisation;

    /**
     * Récupère la date-heure REDAC du dernier changement en date-heure.
     * 
     * @return la date-heure du changement
     */
    public String getDateCreationAsDate() {
        if (getDateCreation() != null) {
            return DateRedac.convertionDateRedacSansValidation(getDateCreation());
        }
        return null;
    }

    /**
     * Récupère la date-heure REDAC du dernier changement en date-heure.
     * 
     * @return la date-heure du changement
     */
    public String getMoisRattachementAsDate() {
        String dateMoisAnnee = getMoisRattachement().toString().substring(4, 5) + getMoisRattachement().toString().substring(5, 6) + "/"
                + getMoisRattachement().toString().substring(0, 1) + getMoisRattachement().toString().substring(1, 2)
                + getMoisRattachement().toString().substring(2, 3) + getMoisRattachement().toString().substring(3, 4);

        return dateMoisAnnee;
    }

}
