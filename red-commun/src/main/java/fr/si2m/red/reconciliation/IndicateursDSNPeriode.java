package fr.si2m.red.reconciliation;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import fr.si2m.red.DateRedac;
import fr.si2m.red.EntiteImportableBatch;
import fr.si2m.red.core.repository.jpa.conversion.ConvertisseurBooleanON;
import fr.si2m.red.core.repository.jpa.conversion.ConvertisseurBooleanOuiNon;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * Modèle des indicateurs de contrat sur une période DSN.
 * 
 * @author nortaina
 *
 */
@Entity
@Table(name = "INDICATEURS_DSN_PERIODES")
@Data
@EqualsAndHashCode(callSuper = false, of = { "idTechnique" })
@ToString(callSuper = false, of = { "idTechnique", "numContrat", "debutPeriode", "finPeriode" })
public class IndicateursDSNPeriode extends EntiteImportableBatch {

    /**
     * UID de version.
     */
    private static final long serialVersionUID = 85040329188019567L;

    /**
     * L'identifiant technique.
     * 
     * @param idTechnique
     *            l'identifiant technique
     * @return l'identifiant technique
     */
    @Id
    @GeneratedValue
    @Column(name = "ID_TECH")
    private Long idTechnique;

    /**
     * Le numéro du contrat complet.
     * 
     * @param numContrat
     *            le numéro du contrat complet
     * @return le numéro du contrat complet
     * 
     */
    @Column(name = "NOCO")
    private String numContrat;

    /**
     * La date de début de période.
     * 
     * @param debutPeriode
     *            la date de début de période
     * @return la date de début de période
     * 
     */
    @Column(name = "DEBUT_PERIODE")
    private Integer debutPeriode;

    /**
     * La date de fin de période.
     * 
     * @param finPeriode
     *            la date de fin de période
     * @return la date de fin de période
     * 
     */
    @Column(name = "FIN_PERIODE")
    private Integer finPeriode;

    /**
     * Date de première période avec réception d’une DSN valide pour ce contrat.
     * 
     * @param dateDebutReceptionDSN
     *            Date de première période avec réception d’une DSN valide pour ce contrat
     * @return Date de première période avec réception d’une DSN valide pour ce contrat
     * 
     */
    @Column(name = "DT_DEB_RCPT_DSN")
    private Integer dateDebutReceptionDSN;

    /**
     * Date de dernière modification des indicateurs.
     * 
     * @param dateModifIndicateurs
     *            Date de dernière modification des indicateurs
     * @return Date de dernière modification des indicateurs
     * 
     */
    @Column(name = "DT_MODIF_IND")
    private Integer dateModifIndicateurs;

    /**
     * Gestionnaire ayant réalisé la dernière modification des indicateurs.
     * 
     * @param gestionnaireModifIndicateurs
     *            Gestionnaire ayant réalisé la dernière modification des indicateurs
     * @return Gestionnaire ayant réalisé la dernière modification des indicateurs
     * 
     */
    @Column(name = "GEST_MODIF_IND")
    private String gestionnaireModifIndicateurs;

    /**
     * Description libre de la raison du changement réalisé sur les indicateurs.
     * 
     * @param motifChangementIndicateurs
     *            Description libre de la raison du changement réalisé sur les indicateurs.
     * @return Description libre de la raison du changement réalisé sur les indicateurs.
     * 
     */
    @Column(name = "MOTIF_CHGT_IND")
    private String motifChangementIndicateurs;

    /**
     * Valeur actuelle ou à venir de l’indicateur Exploitation DSN.
     * 
     * @param exploitationDSNAsText
     *            Valeur actuelle ou à venir de l’indicateur Exploitation DSN.
     * @return Valeur actuelle ou à venir de l’indicateur Exploitation DSN.
     * 
     */
    @Column(name = "EXPLOIT")
    private String exploitationDSNAsText;

    /**
     * Date de début de la première période à partir de laquelle l’indicateur EXPLOIT a pris la valeur qu’il a actuellement.
     * 
     * @param dateDebutExploitationDSN
     *            Date de début de la première période à partir de laquelle l’indicateur EXPLOIT a pris la valeur qu’il a actuellement
     * @return Date de début de la première période à partir de laquelle l’indicateur EXPLOIT a pris la valeur qu’il a actuellement
     * 
     */
    @Column(name = "DT_DEB_EXPLOIT")
    private Integer dateDebutExploitationDSN;

    /**
     * Valeur actuelle ou à venir de l’indicateur Edition des consignes de paiement.
     * 
     * @param editionConsignesPaiementDSNAsText
     *            Valeur actuelle ou à venir de l’indicateur Edition des consignes de paiement.
     * @return Valeur actuelle ou à venir de l’indicateur Edition des consignes de paiement.
     * 
     */
    @Column(name = "EDIT_CONS_PAIMT")
    private String editionConsignesPaiementDSNAsText;

    /**
     * Date de début de la première période à partir de laquelle l’indicateur EDIT_CONSIGN a pris la valeur qu’il a actuellement.
     * 
     * @param dateDebutEditionConsignesPaiement
     *            Date de début de la première période à partir de laquelle l’indicateur EDIT_CONSIGN a pris la valeur qu’il a actuellement
     * @return Date de début de la première période à partir de laquelle l’indicateur EDIT_CONSIGN a pris la valeur qu’il a actuellement
     * 
     */
    @Column(name = "DT_DEB_EDIT_CONS_PAIMT")
    private Integer dateDebutEditionConsignesPaiement;

    /**
     * Mode de Gestion DSN.
     * 
     * @param modeGestionDSN
     *            Mode de Gestion DSN.
     * @return Mode de Gestion DSN.
     * 
     */
    @Column(name = "MODE_GEST")
    private String modeGestionDSN;

    /**
     * Date de début de la première période à partir de laquelle l’indicateur MODE_GEST a pris la valeur qu’il a actuellement.
     * 
     * @param dateDebutEditionConsignesPaiement
     *            Date de début de la première période à partir de laquelle l’indicateur MODE_GEST a pris la valeur qu’il a actuellement
     * @return Date de début de la première période à partir de laquelle l’indicateur MODE_GEST a pris la valeur qu’il a actuellement
     * 
     */
    @Column(name = "DT_DEB_MODE_GEST")
    private Integer dateDebutModeGestionDSN;

    /**
     * Valeur actuelle ou à venir de l’indicateur Transfert DSN.
     * 
     * @param transfertAsText
     *            Valeur actuelle ou à venir de l’indicateur Transfert DSN
     * @return Valeur actuelle ou à venir de l’indicateur Transfert DSN
     * 
     */
    @Column(name = "TRANSFERT")
    private String transfertDSNAsText;

    /**
     * Date de début de la première période à partir de laquelle l’indicateur TRANSFERT a pris la valeur qu’il a actuellement.
     * 
     * @param dateDebutTransfert
     *            Date de début de la première période à partir de laquelle l’indicateur TRANSFERT a pris la valeur qu’il a actuellement
     * @return Date de début de la première période à partir de laquelle l’indicateur TRANSFERT a pris la valeur qu’il a actuellement
     * 
     */
    @Column(name = "DT_DEB_TRANSFERT")
    private Integer dateDebutTransfertDSN;

    /**
     * Variation minimale en nb d’établissement entre 2 périodes de déclaration pour lancer une alerte.
     * 
     * @param seuilVariationAlertesEnNbEtablissements
     *            Variation minimale en nb d’établissement entre 2 périodes de déclaration pour lancer une alerte
     * @return Variation minimale en nb d’établissement entre 2 périodes de déclaration pour lancer une alerte
     * 
     */
    @Column(name = "VAR_ETAB_NB")
    private Integer seuilVariationAlertesEnNbEtablissements;

    /**
     * Variation minimale en % de nb d’établissement entre 2 périodes de déclaration pour lancer une alerte.
     * 
     * @param seuilVariationAlertesEnPourcentageNbEtablissements
     *            Variation minimale en % de nb d’établissement entre 2 périodes de déclaration pour lancer une alerte.
     * @return Variation minimale en % de nb d’établissement entre 2 périodes de déclaration pour lancer une alerte.
     * 
     */
    @Column(name = "VAR_ETAB_PC")
    private Integer seuilVariationAlertesEnPourcentageNbEtablissements;

    /**
     * Variation minimale en nb de salariés entre 2 périodes de déclaration pour lancer une alerte.
     * 
     * @param seuilVariationAlertesEnNbSalaries
     *            Variation minimale en nb de salariés entre 2 périodes de déclaration pour lancer une alerte.
     * @return Variation minimale en nb de salariés entre 2 périodes de déclaration pour lancer une alerte.
     * 
     */
    @Column(name = "VAR_SAL_NB")
    private Integer seuilVariationAlertesEnNbSalaries;

    /**
     * Variation minimale en % de nb de salariés entre 2 périodes de déclaration pour lancer une alerte.
     * 
     * @param seuilVariationAlertesEnPourcentageNbSalaries
     *            Variation minimale en % de nb de salariés entre 2 périodes de déclaration pour lancer une alerte
     * @return Variation minimale en % de nb de salariés entre 2 périodes de déclaration pour lancer une alerte
     * 
     */
    @Column(name = "VAR_SAL_PC")
    private Integer seuilVariationAlertesEnPourcentageNbSalaries;

    /**
     * Indique si la Brique DSN ou si REDAC doit contrôler les types de bases assujettie reçus dans les déclarations de ce contrat.
     * 
     * @param controleTypeBaseAssujettie
     *            Indique si la Brique DSN ou si REDAC doit contrôler les types de bases assujettie reçus dans les déclarations de ce contrat.
     * @return Indique si la Brique DSN ou si REDAC doit contrôler les types de bases assujettie reçus dans les déclarations de ce contrat.
     * 
     */
    @Column(name = "CTL_TYPE_BASE_ASSUJ")
    private String controleTypeBaseAssujettie;

    /**
     * Définit si le contrat et un contrat direct ou bien un contrat délégué.
     * 
     * @param modeNatureContrat
     *            Définit si le contrat et un contrat direct ou bien un contrat délégué
     * @return Définit si le contrat et un contrat direct ou bien un contrat délégué
     * 
     */
    @Column(name = "MODE_NAT_CONT")
    private String modeNatureContrat;

    /**
     * Indique si le contrat est VIP.
     * 
     * @param codeVIPAsText
     *            Indique si le contrat est VIP
     * @return Indique si le contrat est VIP
     * 
     */
    @Column(name = "VIP")
    private String codeVIPAsText;

    /**
     * Indique le mode de prise en compte des effectifs pour les contrats sur effectif.
     * 
     * @param modeReaffectationCategorieEffectifs
     *            Indique le mode de prise en compte des effectifs pour les contrats sur effectif
     * @return Indique le mode de prise en compte des effectifs pour les contrats sur effectif
     * 
     */
    @Column(name = "MODE_REAFF_CAT_EFFECTIF")
    private String modeReaffectationCategorieEffectif;

    /**
     * Indicateur de ligne en cours d'import batch.
     * 
     * @param ligneEnCoursImportBatch
     *            indicateur de ligne en cours d'import batch
     * @return indicateur de ligne en cours d'import batch
     * 
     */
    @Column(name = "TMP_BATCH")
    private boolean ligneEnCoursImportBatch;

    /**
     * L'indicateur d'exploitation DSN.
     * 
     * @return l'indicateur d'exploitation DSN
     */
    public boolean isExploitationDSN() {
        return new ConvertisseurBooleanOuiNon().convertToEntityAttribute(getExploitationDSNAsText());
    }

    /**
     * Indicateur d'état de contrat actif.
     * 
     * @param exploitationDSN
     *            l'indicateur d'état de contrat actif
     */
    public void setExploitationDSN(boolean exploitationDSN) {
        setExploitationDSNAsText(new ConvertisseurBooleanOuiNon().convertToDatabaseColumn(exploitationDSN));
    }

    /**
     * Valeur actuelle ou à venir de l’indicateur Edition des consignes de paiement.
     * 
     * @return Valeur actuelle ou à venir de l’indicateur Edition des consignes de paiement.
     * 
     */
    public boolean isEditionConsignesPaiementDSN() {
        return new ConvertisseurBooleanOuiNon().convertToEntityAttribute(getEditionConsignesPaiementDSNAsText());
    }

    /**
     * Valeur actuelle ou à venir de l’indicateur Edition des consignes de paiement.
     * 
     * @param editionConsignesPaiement
     *            Valeur actuelle ou à venir de l’indicateur Edition des consignes de paiement.
     */
    public void setEditionConsignesPaiementDSN(boolean editionConsignesPaiement) {
        setEditionConsignesPaiementDSNAsText(new ConvertisseurBooleanOuiNon().convertToDatabaseColumn(editionConsignesPaiement));
    }

    /**
     * Valeur actuelle ou à venir de l’indicateur Transfert DSN.
     * 
     * @return Valeur actuelle ou à venir de l’indicateur Transfert DSN
     * 
     */
    public boolean isTransfertDSN() {
        return new ConvertisseurBooleanOuiNon().convertToEntityAttribute(getTransfertDSNAsText());
    }

    /**
     * Valeur actuelle ou à venir de l’indicateur Transfert DSN.
     * 
     * @return Valeur actuelle ou à venir de l’indicateur Transfert DSN (O/N)
     * 
     */
    public String getTransfertDSNasTextCourt() {
        return new ConvertisseurBooleanON().convertToDatabaseColumn(isTransfertDSN());
    }

    /**
     * Valeur actuelle ou à venir de l’indicateur Transfert DSN.
     * 
     * @param transfert
     *            Valeur actuelle ou à venir de l’indicateur Transfert DSN
     */
    public void setTransfertDSN(boolean transfert) {
        setTransfertDSNAsText(new ConvertisseurBooleanOuiNon().convertToDatabaseColumn(transfert));
    }

    /**
     * Indicateur d'état de contrat actif.
     * 
     * @return l'indicateur d'état de contrat actif
     */
    public boolean isCodeVIP() {
        return new ConvertisseurBooleanON().convertToEntityAttribute(getCodeVIPAsText());
    }

    /**
     * Indicateur d'état de contrat actif.
     * 
     * @param codeVIP
     *            l'indicateur d'état de contrat actif
     */
    public void setCodeVip(boolean codeVIP) {
        setCodeVIPAsText(new ConvertisseurBooleanON().convertToDatabaseColumn(codeVIP));
    }

    /**
     * Récupère la date de début de réception DSN sous forme d'entier (0 si null).
     * 
     * @return la date de début de réception DSN sous forme d'entier (0 si null)
     */
    public int getDateDebutReceptionDSNAsInt() {
        return getDateDebutReceptionDSN() == null ? 0 : getDateDebutReceptionDSN().intValue();
    }

    /**
     * Récupère la date de début de réception DSN.
     * 
     * @return la date de début de réception DSN
     */
    public Date getDateDebutReceptionDSNAsDate() {
        return DateRedac.convertitEnDate(getDateDebutReceptionDSN());
    }

    /**
     * Récupère la date de début d'exploitation de la DSN.
     * 
     * @return la date de début d'exploitation de la DSN
     */
    public Date getDateDebutExploitationDSNAsDate() {
        return DateRedac.convertitEnDate(getDateDebutExploitationDSN());
    }

    /**
     * Récupère la date de début de l'édition des consignes de paiement.
     * 
     * @return la date de début de l'édition des consignes de paiement
     */
    public Date getDateDebutEditionConsignesPaiementAsDate() {
        return DateRedac.convertitEnDate(getDateDebutEditionConsignesPaiement());
    }

    /**
     * Récupère la date de début du mode de gestion DSN.
     * 
     * @return la date de début du mode de gestion DSN
     */
    public Date getDateDebutModeGestionDSNAsDate() {
        return DateRedac.convertitEnDate(getDateDebutModeGestionDSN());
    }

    /**
     * Récupère la date de début du transfert de la DSN.
     * 
     * @return la date de début du transfert de la DSN
     */
    public Date getDateDebutTransfertDSNAsDate() {
        return DateRedac.convertitEnDate(getDateDebutTransfertDSN());
    }

    /**
     * Récupère la date de dernière modification de la DSN.
     * 
     * @return la date de dernière modification de la DSN
     */
    public Date getDateModifIndicateursAsDate() {
        return DateRedac.convertitEnDate(getDateModifIndicateurs());
    }

    /**
     * Récupère la date de début du mode de gestion DSN sous forme d'entier (0 si null).
     * 
     * @return la date de début du mode de gestion DSN sous forme d'entier (0 si null)
     */
    public int getDateDebutModeGestionDSNAsInt() {
        return getDateDebutModeGestionDSN() == null ? 0 : getDateDebutModeGestionDSN().intValue();
    }

    @Override
    public Long getId() {
        return idTechnique;
    }

}
