/**
 * 
 */
package fr.si2m.red.reconciliation;

import lombok.Getter;

/**
 * Types d'action inititée par la popupChangement en Masse
 * 
 * @author eudesr
 *
 */
public enum TypeActionPopupChangementEnMasse {
    /**
     * Blocage périodes
     */
    BLOCAGE("BLOCAGE"),
    /**
     * Déblocage périodes
     */
    DEBLOCAGE("DEBLOCAGE"),

    /**
     * mise en attente périodes
     */
    ATTENTE("ATTENTE"),
    /**
     * Levée attente périodes
     */
    FIN_ATTENTE("FIN_ATTENTE"),
    /**
     * changement statut périodes
     */
    STATUT("STATUT"),
    /**
     * Assignation gestionnaire périodes
     */
    ASSIGNER("ASSIGNER"),
    /**
     * Affectation gestionnaire périodes
     */
    AFFECTER("AFFECTER"),
    /**
     * Ajout d'un commentaire
     */
    COMMENTAIRE("COMMENTAIRE");

    @Getter
    String designation;

    private TypeActionPopupChangementEnMasse(String designation) {
        this.designation = designation;
    }
}
