package fr.si2m.red.reconciliation;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import fr.si2m.red.Entite;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * Modèle des périodes reçues traitées.
 * 
 * @author benitahy
 *
 */
@Entity
@Table(name = "PERIODES_RECUES_TRAITEES")
@Data
@EqualsAndHashCode(callSuper = false, of = { "idPeriode", "numeroContrat", "dateDebutPeriode" })
@ToString(callSuper = false, of = { "idPeriode", "numeroContrat", "dateDebutPeriode" })
public class PeriodeRecueTraitee extends Entite {

    /**
     * UID de version.
     */
    private static final long serialVersionUID = 7406510255650154363L;

    /**
     * L'identifiant technique.
     * 
     * @param idTechnique
     *            l'identifiant technique
     * @return l'identifiant technique
     */
    @Id
    @GeneratedValue
    @Column(name = "ID_TECH")
    private Long idTechnique;

    /**
     * L'identifiant de la période reçue traitée
     * 
     * @param idPeriode
     *            l'identifiant de la période
     * @return l'identifiant de la période
     */
    @Column(name = "ID_PERIODE")
    private Long idPeriode;

    /**
     * Le numéro du contrat auquel cette période est rattachée. Correspond au NOCO
     * 
     * @param numeroContrat
     *            Le numéro du contrat complet
     * @return Le numéro du contrat complet
     */
    @Column(name = "NUMERO_CONTRAT")
    private String numeroContrat;

    /**
     * Date de début de la période.
     * 
     * @param dateDebutPeriode
     *            La date de début de période
     * @return La date de début de période
     */
    @Column(name = "DATE_DEBUT_PERIODE")
    private Integer dateDebutPeriode;

    @Override
    public Long getId() {
        return idTechnique;
    }

}
