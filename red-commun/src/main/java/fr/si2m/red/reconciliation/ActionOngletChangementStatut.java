package fr.si2m.red.reconciliation;

import lombok.Data;

/**
 * Modèle des actions effectuées dans l'onglet changement de statut
 * 
 * @author inidjelG
 * 
 */
@Data
public class ActionOngletChangementStatut {

    /**
     * CodeUser du gestionnaire affecté au traitement
     */
    private String gestionnaireAffecteA;
    /**
     * CodeUser du gestionnaire devant effectuer le traitement
     */
    private String gestionnaireATraiterPar;
    /**
     * Etat de la checkbox attenteRetourEntreprise (enable (true) / disable (false))
     */
    private boolean attenteRetourEntreprise;
    /**
     * Etat de la checkbox bloquer période (enable (true) / disable (false))
     */
    private boolean bloquerPeriodeCbx;

}
