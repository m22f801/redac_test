package fr.si2m.red.reconciliation;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.ToString;

/**
 * Représentation des informations individu pour l'export Excel Individus OngletSynthese
 * 
 * @author eudesr
 *
 */
@Data
@ToString(of = { "matricule", "nomUsage", "prenom", "identifiantRepertoire", "numeroTechniqueTemporaire", "sommeMontantsCotisation",
        "concatMsgSignal", "concatMsgRejet" })
@AllArgsConstructor
public class ExportIndividuExcel implements Serializable {

    /**
     * UID de version.
     */

    private static final long serialVersionUID = 1L;

    /**
     * S21.G00.30.019
     *
     * @param matricule
     *            S21.G00.30.019
     * @return S21.G00.30.019
     */
    private String matricule;

    /**
     * S21.G00.30.002
     *
     * @param nomUsage
     *            S21.G00.30.002
     * @return S21.G00.30.002
     */
    private String nomFamille;

    /**
     * S21.G00.30.003
     *
     * @param nomUsage
     *            S21.G00.30.003
     * @return S21.G00.30.003
     */
    private String nomUsage;

    /**
     * S21.G00.30.004
     *
     * @param prenom
     *            S21.G00.30.004
     * @return S21.G00.30.004
     */
    private String prenom;

    /**
     * S21.G00.30.001
     *
     * @param identifiantRepertoire
     *            S21.G00.30.001
     * @return S21.G00.30.001
     */
    private String identifiantRepertoire;

    /**
     * S21.G00.30.020
     *
     * @param numeroTechniqueTemporaire
     *            S21.G00.30.020
     * @return S21.G00.30.020
     */
    private String numeroTechniqueTemporaire;

    /**
     * S21.G00.30.020
     *
     * @param sommeMontantCotisation
     *            S21.G00.30.020
     * @return S21.G00.30.020
     */
    private double sommeMontantsCotisation;

    /**
     * S21.G00.30.020
     *
     * @param concatMsgSignal
     *            S21.G00.30.020
     * @return S21.G00.30.020
     */
    private String concatMsgSignal;

    /**
     * S21.G00.30.020
     *
     * @param concatMsgRejet
     *            S21.G00.30.020
     * @return S21.G00.30.020
     */
    private String concatMsgRejet;

    /**
     * Flag indicant une valeur null ( aucune ligne dans CATEGORIE_QUITTANCEMENT_INDIVIDU pour l'indiv ) , nécessaire car sommeMontantsCotisation de type double
     * a la valeur 0 par défaut.
     */
    private boolean sommeMontantsCotisationExistante;

    /**
     * Constructeur par défaut
     */
    public ExportIndividuExcel() {
        super();
    }

}
