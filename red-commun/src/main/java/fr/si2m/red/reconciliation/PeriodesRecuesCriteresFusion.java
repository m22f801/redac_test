package fr.si2m.red.reconciliation;

import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.beanutils.BeanUtils;
import org.springframework.util.MultiValueMap;

import fr.si2m.red.RedacUnexpectedException;
import lombok.Getter;
import lombok.Setter;

/**
 * Critères de fusion de périodes reçues.
 * 
 * @author eudesr
 *
 */
public class PeriodesRecuesCriteresFusion implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 7104970205456924460L;

    /**
     * Sauvegarde des derniers critères de recherches validés
     */
    @Getter
    @Setter
    String parametresRecherche;

    /**
     * Liste des ID périodes à fusionner, concaténés et séparé par une virgule
     */
    @Getter
    @Setter
    String idsPeriodesFusion;

    /**
     * Représentation en List-Long- des idsPeriodesFusion , toujours utiliser getIdsPeriodesFusionAsLong() pour init / acceder à la liste
     */
    private List<Long> idsPeriodesFusionAsLong;

    /**
     * Constructeur par défaut des critères.
     */
    public PeriodesRecuesCriteresFusion() {
        super();
    }

    /**
     * Constructeur de critères par copie de critères donnés.
     * 
     * @param periodesRecuesCriteresFusion
     *            les critères à copier
     */
    public PeriodesRecuesCriteresFusion(PeriodesRecuesCriteresFusion periodesRecuesCriteresFusion) {
        this();
        try {
            BeanUtils.copyProperties(this, periodesRecuesCriteresFusion);
        } catch (IllegalAccessException | InvocationTargetException e) {
            throw new RedacUnexpectedException("Erreur lors de la copie de critères de fusion", e);
        }
    }

    /**
     * Convertit une map de valeurs multiple en critères de fusion de périodes.
     * 
     * @param criteres
     *            les critères sous forme de map de valeurs multiples
     * @return les critères de fusion de périodes extraits
     */
    public static PeriodesRecuesCriteresFusion from(MultiValueMap<String, Object> criteres) {
        PeriodesRecuesCriteresFusion criteresFusion = new PeriodesRecuesCriteresFusion();
        try {
            BeanUtils.populate(criteresFusion, criteres);
        } catch (IllegalAccessException | InvocationTargetException e) {
            throw new RedacUnexpectedException("Erreur lors du mapping de critères de recherche", e);
        }

        return criteresFusion;
    }

    /**
     * Initialise et renvoie la liste des IdsPeriodes sous forme de List-Long-
     * 
     * @return la liste des IdsPeriodes comme List-Long-
     */
    public List<Long> getIdsPeriodesFusionAsLong() {
        if (idsPeriodesFusionAsLong == null) {
            setIdsPeriodesFusionAsLong();
        }
        return idsPeriodesFusionAsLong;
    }

    /**
     * Initialise la liste
     */
    private void setIdsPeriodesFusionAsLong() {
        idsPeriodesFusionAsLong = new ArrayList<>();
        if (idsPeriodesFusion != null) {
            for (String idPeriode : Arrays.asList(idsPeriodesFusion.split(","))) {
                idsPeriodesFusionAsLong.add(Long.parseLong(idPeriode));
            }
        }
    }
}
