package fr.si2m.red.reconciliation;

import java.util.List;
import java.util.Map;

import fr.si2m.red.core.repository.RedacRepository;

/**
 * Référentiel des historiques des mises en attentes / levées d'attente de retour entreprise des périodes.
 * 
 * @author gahagnont
 *
 */
public interface HistoriqueAttenteRetourEtpPeriodeRepository extends RedacRepository<HistoriqueAttenteRetourEtpPeriode> {

    /**
     * Récupère les historiques des mises en attentes / levées d'attente de retour entreprise d'une période donnée.
     * 
     * @param idPeriode
     *            l'identifiant de l'historique de la période
     * @return les historiques des mises en attentes / levées d'attente de retour entreprise pour cette période
     */
    List<HistoriqueAttenteRetourEtpPeriode> getHistoriqueAttenteRetourEtpPeriode(Long idPeriode);

    /**
     * Récupère les historiques des mises en attentes / levées d'attente de retour entreprise d'une liste de période donnée.
     * 
     * @param idsPeriode
     *            les identifiants de l'historique de la période
     * @return les historiques des mises en attentes / levées d'attente de retour entreprise pour cette période
     */
    List<HistoriqueAttenteRetourEtpPeriode> getHistoriqueAttenteRetourEtpPeriodes(List<Long> idsPeriodes);

    /**
     * Récupère un historique d'une mise en attente / levée d'attente de retour entreprise d'une période donnée.
     * 
     * @param idPeriode
     *            l'identifiant de la période
     * @param dateHeureSaisie
     *            l'horodatage de l'historique de mise en attente / levée d'attente de retour entreprise pour cette période
     * @return l'historique recherché
     */
    HistoriqueAttenteRetourEtpPeriode getHistoriqueAttenteRetourEtpPeriodePourMessage(Long idPeriode, Long dateHeureRetour);

    /**
     * Récupère le dernier historique de mise en attente / levée d'attente de retour entreprise d'une période donnée.
     * 
     * @param idPeriode
     *            l'identifiant de la période
     * @return l'historique de la période concernée
     */
    HistoriqueAttenteRetourEtpPeriode getDernierHistoriquePourPeriode(Long idPeriode);

    /**
     * Supprime les historiques des mises en attentes et levées d'attente de retour entreprise d'une période donnée.
     * 
     * @param idPeriode
     *            l'identifiant de la période
     * @return le nombre d'éléments supprimés effectivement dans le référentiel
     */
    int supprimeHistoriqueAttenteRetourEtpPourPeriode(Long idPeriode);

    /**
     * Exécute une procédure stockée MySQL.
     * 
     * @param sql
     *            la commande d'appel de la procédure
     * @param params
     *            les paramètres d'appels de la procédure
     */
    void callMysqlProcedure(String sql, Map<String, Object> params);

    /**
     * Creation d'historique de mise en attente et levée d'attente de retour entreprise periode en masse
     * 
     * @param idsPeriodes
     *            liste des periodes ciblées
     * @param action
     *            libellé action
     * @param utilisateur
     *            user effectuant l'action
     * @param statutCible
     *            statut cible des periodes
     */
    void creationEnMasseOrigineIHM(List<Long> idsPeriodes, TypeActionPopupChangementEnMasse action, String utilisateur);

}
