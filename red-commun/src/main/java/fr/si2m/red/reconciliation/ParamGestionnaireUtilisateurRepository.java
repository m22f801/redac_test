package fr.si2m.red.reconciliation;

import java.util.List;

import fr.si2m.red.core.repository.RedacRepository;
import fr.si2m.red.parametrage.ParamUtilisateurGestionnaire;

/**
 * Référentiel des périodes DSN reçues.
 * 
 * @author delortj
 *
 */
public interface ParamGestionnaireUtilisateurRepository extends RedacRepository<ParamUtilisateurGestionnaire> {

    List<ParamUtilisateurGestionnaire> rechercheGestionnaireSiren(String numSiren);

}
