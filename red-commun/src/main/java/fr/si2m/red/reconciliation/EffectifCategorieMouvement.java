package fr.si2m.red.reconciliation;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import fr.si2m.red.Entite;

/**
 * Modèle des effectifs de categories de mouvements
 * 
 * @author poidij
 *
 */
@Entity
@Table(name = "EFFECTIF_CATEGORIE_MVT")
@Data
@EqualsAndHashCode(callSuper = false, of = { "idTechnique" })
@ToString(callSuper = false, of = { "idTechnique", "idPeriode", "numCategorieQuittancement", "dateEffetMouvement" })
public class EffectifCategorieMouvement extends Entite {
    /**
     * UID de version.
     */
    private static final long serialVersionUID = -3324801079594603125L;

    /**
     * L'identifiant technique.
     * 
     * @param idTechnique
     *            l'identifiant technique
     * @return l'identifiant technique
     */
    @Id
    @GeneratedValue
    @Column(name = "ID_TECH")
    private Long idTechnique;

    /**
     * L'identifiant technique de la période à laquelle ce message est rattaché.
     * 
     * @param idPeriode
     *            l'identifiant technique de la période
     * @return l'identifiant technique de la période
     */
    @Column(name = "ID_PERIODE")
    private Long idPeriode;

    /**
     * Numéro de catégorie de quittancement
     * 
     * @param numCategorieQuittancement
     *            Le numéro de la catégorie de quittancement
     * @return Le numéro de la catégorie de quittancement
     */
    @Column(name = "NOCAT")
    private String numCategorieQuittancement;

    /**
     * Date d'effet du mouvement
     * 
     * @param dateEffetMouvement
     *            La date d'effet du mouvement
     * @return La date d'effet du mouvement
     */
    @Column(name = "DATE_EFFET_MVT")
    private Integer dateEffetMouvement;

    /**
     * Nombre de mouvements
     * 
     * @param nombreMouvement
     *            Le nombre de mouvements
     * @return Le nombre de mouvements
     */
    @Column(name = "NB_MOUVEMENT")
    private Integer nombreMouvement;

    @Override
    public Long getId() {
        return idTechnique;
    }

}
