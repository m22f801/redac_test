package fr.si2m.red.reconciliation;

import java.util.List;
import java.util.Map;
import java.util.Set;

import fr.si2m.red.core.repository.RedacRepository;
import fr.si2m.red.dsn.EtatPeriode;
import fr.si2m.red.dsn.ParametresPartitionPeriodes;
import fr.si2m.red.dsn.Trace;

/**
 * Référentiel des périodes DSN reçues.
 * 
 * @author delortj
 *
 */
public interface PeriodeRecueRepository extends RedacRepository<PeriodeRecue> {

    /**
     * Indique si la date une date de debut de periode correspondant à un debut de de periode d'indicateur DSN calculé pour un contrat, existe dans l'entité
     * PeriodeRecue pour le type de periode 'DEC' et le champ Premier mois de declaration à 'O' (oui).
     * 
     * @param numeroContrat
     *            le numéro de contrat
     * @return vrai ou faux
     */
    boolean existeDeclarationRecuePremierMoisPourDebutPeriode(String numeroContrat);

    /**
     * Récupère le nombre total de périodes en cours de réception.
     * 
     * @return le nombre total de périodes en cours de réception
     */
    long compteToutesPeriodesEnCoursDeReception();

    /**
     * Récupère le nombre de périodes en cours de réception pour des groupes de gestion et des numéro de famille donnés.
     * 
     * @param groupesGestion
     *            les groupes de gestion
     * @param numsFamille
     *            les numéros de famille
     * @param dateDuJour
     *            la date du jour
     * 
     * @return le nombre de périodes en cours de réception pour des groupes de gestion et des numéro de famille donnés
     */
    long comptePeriodesEnCoursDeReception(List<String> groupesGestion, List<Integer> numsFamille, Integer dateDuJour);

    /**
     * Récupère le nombre de périodes à traiter pour des groupes de gestion et des numéro de famille donnés.
     * 
     * @param groupesGestion
     *            les groupes de gestion
     * @param numsFamille
     *            les numéros de famille
     * @param dateDuJour
     *            la date du jour
     * 
     * @return le nombre de périodes à traiter pour des groupes de gestion et des numéro de famille donnés
     */
    long comptePeriodesATraiter(List<String> groupesGestion, List<Integer> numsFamille, Integer dateDuJour);

    /**
     * Indique s'il existe une période de type typePeriode pour l'Adhésion idAdhesion de mois de rattachement moisRattachement et de référence de contrat
     * referenceContrat.
     * 
     * @param typePeriode
     *            le type de période
     * @param referenceContrat
     *            la référence contrat de l'Adhésion
     * @param moisRattachement
     *            le mois de rattachement de l'Adhésion
     * @param etatPeriode
     *            l'état de la période
     * @return true si une telle période existe, false sinon
     */
    PeriodeRecue existePeriodePourRattachement(String typePeriode, String referenceContrat, Integer moisRattachement, EtatPeriode etatPeriode);

    /**
     * Indique s'il existe une période de type typePeriode pour l'Adhésion idAdhesion de mois de rattachement moisRattachement et de référence de contrat
     * referenceContrat.
     * 
     * @param typePeriode
     *            le type de période
     * @param referenceContrat
     *            la référence contrat de l'Adhésion
     * @param moisRattachement
     *            le mois de rattachement de l'Adhésion
     * @return true si une telle période existe, false sinon
     */
    PeriodeRecue existePeriodePourRattachement(String typePeriode, String referenceContrat, Integer moisRattachement);

    /**
     * Récupère la période précédente de même type pour une période donnée, si elle existe.
     * 
     * @param periode
     *            la période
     * @return la période de même type précédent cette période si elle existe, null sinon
     */
    PeriodeRecue getPeriodePrecedenteDeMemeType(PeriodeRecue periode);

    /**
     * Récupère la liste complète des périodes reçues en cours pour une application donnée.
     * 
     * @param applicationConcernee
     *            l'application concernée.
     * @return la liste des périodes reçues en cours pour l'application concernée.
     */
    List<PeriodeRecue> getPeriodesRecuesEncoursPourApplication(String applicationConcernee);

    /**
     * Récupère la période reçue en cours pour un contrat donné et à partir d'une certaine date.
     * 
     * @param numeroContrat
     *            le numéro de contrat
     * @param dateDebutPeriode
     *            la date de début de période
     * @return la période reçue en cours qui correspond aux critères donnés
     */
    PeriodeRecue getPeriodeRecueEncours(String numeroContrat, Integer dateDebutPeriode);

    /**
     * Récupère la période reçue pour un contrat donné et à partir d'une certaine date.
     * 
     * @param numeroContrat
     *            le numéro de contrat
     * @param dateDebutPeriode
     *            la date de début de période
     * @return la période reçue qui correspond aux critères donnés
     */
    PeriodeRecue getPeriodeRecue(String numeroContrat, Integer dateDebutPeriode);

    /**
     * Récupère la période reçue deja traitee, pour un contrat donné et à partir d'une certaine date.
     * 
     * @param numeroContrat
     *            le numéro de contrat
     * @param dateDebutPeriode
     *            la date de début de période
     * @return la période reçue qui correspond aux critères donnés
     */
    PeriodeRecue getPeriodeRecueTraitee(String numeroContrat, Integer dateDebutPeriode);

    /**
     * Récupère les périodes à reconsolider (méthode de test uniquement, attention à la volumétrie des résultats).
     * 
     * @return les périodes à reconsolider
     */
    List<PeriodeRecue> getPeriodesAReconsolider();

    /**
     * Compte le nombre de périodes répondant à des critères de recherche donnés.
     * 
     * @param criteresRecherche
     *            les critères de recherche
     * @return le nombre de périodes correspondant aux critères de recherche
     */
    long compte(PeriodesRecuesCriteresRecherche criteresRecherche);

    /**
     * Recherche une liste de périodes selon des critères de recherche donnés.
     * 
     * @param criteresRecherche
     *            les critères de recherche
     * @return la liste des périodes correspondant aux critères de recherche
     */
    List<ResumePeriodeRecue> recherchePeriodesRecues(PeriodesRecuesCriteresRecherche criteresRecherche);

    /**
     * Récupère le libellé d'une période selon qu'elle soit trimestrielle, annuelle, mensuelle, semestrielle....
     * 
     * @param periode
     *            la période
     * @return le libellé de la période représentée
     */
    String getLibellePeriode(PeriodeRecue periode);

    /**
     * Récupère les périodes liées à un contrat, triées par date de début de période puis par date de création.
     * 
     * @param numContrat
     *            le numéro de contrat
     * @return les périodes liées au contrat
     */
    List<PeriodeRecue> getPeriodesRecuesPourContrat(String numeroContrat);

    /**
     * Récupère les paramètres globaux nécessaires au partitionning du traitement des périodes lors de la reconsolidation sur effectifs.
     * 
     * @return les paramètres globaux du partionning
     */
    ParametresPartitionPeriodes recupereDonneesGlobalesDuPartionning();

    /**
     * Récupère les paramètres (identifiants de périodes) nécessaires au partitionning du traitement des périodes lors de la reconsolidation sur effectifs.
     * 
     * @param listeRangs
     *            la liste des rangs indiquant les identifiants des périodes de début et de fin de chaque tile (après partionnement des périodes en n-tiles)
     * 
     * @return la liste des paramètres (identifiants de périodes) du partionning
     * 
     */
    List<Long> recupereDonneesDuPartionning(String listeRangs);

    /**
     * Récupère le nombre total d'éléments EffectifCategorieContratTravail à calculer, pour le partitionning.
     * 
     * @return
     */
    Long recupereNombreTotalElementCalculesPourPartionning();

    /**
     * Exécute une procédure stockée MySQL.
     * 
     * @param sql
     *            la commande d'appel de la procédure
     * @param params
     *            les paramètres d'appels de la procédure
     */
    void callMysqlProcedure(String sql, Map<String, Object> params);

    /**
     * Récupère les traces du calcul des éléments déclaratifs pour les contrat sur salaires.
     * 
     */
    public List<Trace> recupereTracesSurSalairesBatch405();

    /**
     * Récupère les traces de la verification du nocat pour les contrat sur salaires.
     * 
     */
    public List<Trace> recupereTracesVerificationNocatSurSalaires();

    /**
     * Renvoie la liste des idPeriode avec natureContrat = 2 (sur salaire) et RECONSOLIDER='O'
     * 
     * @return liste des identifiants periodes
     */
    public Set<Long> identifiantsPeriodeContratSurSalaireACalculer();

    /**
     * Récupère la période de type DEC la plus récente associée au contrat
     * 
     * @param numeroContrat
     *            le numéro de contrat
     * @return la période la plus récente associée au contrat
     */
    PeriodeRecue getDernierePeriodeContrat(String numeroContrat);

    /**
     * supprime les lignes PeriodeRecue à partir de l'identifiant de la période
     * 
     * @param idPeriode
     *            l'identifiant de la période
     * @return le nombre de lignes de PeriodeRecue supprimées
     */
    int supprimePeriodeRecue(Long idPeriode);

    /**
     * modifie la PeriodeRecue à partir de l'identifiant de la période
     * 
     * @param idPeriode
     *            l'identifiant de la période
     * @param reconsolider
     *            le flag pour la reconsolidation de la periode
     */
    void modifieReconsoliderPeriodeRecue(Long idPeriode, String reconsolider);

    /**
     * Vérification de l'existence d'au moins une période à reconsolider
     * 
     * @return True si au moins une période a RECONSOLIDER='O'
     */
    boolean existePeriodeAReconsolider();

    /**
     * Recherche une liste de périodes selon des critères de recherche donnés en SQL natif à destination de l'export excel Periodes
     * 
     * @param criteresRecherche
     *            les critères de recherche
     * @return la liste des périodes correspondant aux critères de recherche
     */
    List<ExportPeriodeExcel> recherchePeriodesRecuesNatifExportExcel(PeriodesRecuesCriteresRecherche criteresRecherche);

    /**
     * Recupère les périodes identifiées par la liste fournie
     * 
     * @param idsPeriode
     *            liste d'identifiant periodes
     * @return la liste des périodes associées
     */
    List<PeriodeRecue> getPeriodes(List<Long> idsPeriode);

    /**
     * Récupère la période la plus ancienne (selon DT_CREATION), parmi la liste fournie
     * 
     * @param idsPeriodes
     *            liste d'identifiant periodes
     * @return la période la plus ancienne
     */
    PeriodeRecue getPlusAnciennePeriodeCree(List<Long> idsPeriode);

    /**
     * Retourne la première période du type avec DT_CREATION la plus ancienne parmi la liste fournie
     * 
     * @param type
     *            type de la période
     * @param idsPeriode
     *            liste des identifiants de périodes
     * @return la période du type parmi les idsPeriode données en paramètre
     */
    PeriodeRecue getPlusAnciennePeriodeCreeAvecType(String type, List<Long> idsPeriode);

    /**
     * Retourne l'utilisateur affecté au traitement de la période antérieure pour un même contrat.
     * 
     * @param Contrat
     * 
     * @return Retourne l'utilisateur affecté
     */
    String getAffecteADeLaPeriodeAnterieurePourMemeContrat(String numeroContrat);

    /**
     * Mise à jour du champ AffecteA pour les periodes ciblées
     * 
     * @param idsPeriodes
     * 
     * @param gestionnaireAffecteA
     *            gestionnaire affecté au traitement des périodes en cours
     */
    void modifiegestionnaireAffecteA(List<Long> idsPeriodes, String gestionnaireAffecteA);

    /**
     * Mise à jour du champ ATraiterPar pour les périodes ciblées
     * 
     * @param idsPeriodes
     *            liste des périodes impactés
     * @param gestionnaireATraiterPar
     *            gestionnaire ATraiterPar des périodes en cours
     */
    void modifieGestionnaireATraiterPar(List<Long> idsPeriodes, String gestionnaireATraiterPar);

    /**
     * Mise à jour du champ TraitePar pour les périodes ciblées
     * 
     * @param idsPeriodes
     *            liste des périodes impactés
     * @param userIHM
     *            gestionnaire effectuant l'action
     * @param userCible
     *            user ciblé par l'action assignatin/affectation
     * @param action
     *            le type d'action initiant la modification
     */
    void modifieGestionnaireTraitePar(List<Long> idsPeriodes, String userIHM, String userCible, TypeActionPopupChangementEnMasse action);

    /**
     * Liste des idPeriode ciblés par les critères de recherche
     * 
     * @param criteres
     *            de recherche à transcoder
     * 
     * @return la liste des idPeriodes
     */
    List<Long> getIdPeriodesRecherche(PeriodesRecuesCriteresRecherche criteres);

    /**
     * Liste des status distincts invalides pour le blocage ( hors RCP ) pour les periodes données
     * 
     * @param idPeriodes
     *            liste des idPeriodes
     * @return liste des statuts invalide pour blocage
     */
    List<String> getStatutsInvalidePourBlocage(List<Long> idPeriodes);

    /**
     * Liste distinct des etats des periodes ciblés par les critères de recherche
     * 
     * @param criteres
     *            de recherche à transcoder
     * 
     * @return la liste des etats distincts
     */
    List<String> getStatutPeriodesRecherche(PeriodesRecuesCriteresRecherche criteres);

    /**
     * Récupération des états périodes distinct
     * 
     * @param idPeriodes
     *            id des périodes sélectionnées
     * @return la liste des Etats Periodes distincts
     */

    List<String> getStatutPeriodesRecherche(List<Long> idPeriodes);

    /**
     * Mise à jour du statut des périodes
     * 
     * @param code
     *            code de statut période à affecter aux périodes
     * @param idPeriodes
     *            id des périodes sélectionnées
     */
    void changementStatutPeriodeEnMasse(String code, List<Long> idPeriodes);

    /**
     * Mise à jour du champ auditUtilisateurDerniereModification pour les périodes ciblées
     * 
     * @param idsPeriodes
     *            liste des périodes impactés
     * @param userIHM
     *            gestionnaire effectuant l'action
     * @param userCible
     *            user ciblé par l'action assignatin/affectation
     * @param action
     *            le type d'action initiant la modification
     */
    void modifieAuditUserEnMasse(List<Long> idsPeriodes, String userIHM, String userCible, TypeActionPopupChangementEnMasse action);

}
