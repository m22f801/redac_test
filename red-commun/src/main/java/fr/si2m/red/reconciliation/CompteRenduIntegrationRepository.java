package fr.si2m.red.reconciliation;

import fr.si2m.red.core.repository.RedacRepository;

/**
 * Référentiel des compte-Rendus d'integration.
 * 
 * @author poidij
 *
 */
public interface CompteRenduIntegrationRepository extends RedacRepository<CompteRenduIntegration> {

    /**
     * Supprime un compte-rendu d'intégration pour une période donnée.
     * 
     * @param idPeriode
     *            l'identifiant de la période
     * @return le nombre d'éléments supprimés effectivement dans le référentiel
     */
    int supprimeCompteRenduIntegrationPourPeriode(Long idPeriode);

    /**
     * Récupère le compte-rendu d'intégration d'une période s'il existe.
     * 
     * @param idPeriode
     *            l'identifiant de la période
     * @return le compte-rendu s'il existe, null sinon
     */
    CompteRenduIntegration getPourPeriode(Long idPeriode);
}
