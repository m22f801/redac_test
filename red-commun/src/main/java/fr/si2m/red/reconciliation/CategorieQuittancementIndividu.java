package fr.si2m.red.reconciliation;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import fr.si2m.red.Entite;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * Modèle des categories de quittancement des individus.
 * 
 * @author poidij
 *
 */
@Entity
@Table(name = "CATEGORIE_QUITTANCEMENT_INDIVIDU")
@Data
@EqualsAndHashCode(callSuper = false, of = { "idTechnique" })
@ToString(callSuper = false, of = { "idTechnique", "idPeriode", "numCategorieQuittancement", "individu" })
public class CategorieQuittancementIndividu extends Entite {

    /**
     * UID de version.
     */
    private static final long serialVersionUID = -3324801079594603125L;

    /**
     * L'identifiant technique.
     * 
     * @param idTechnique
     *            l'identifiant technique
     * @return l'identifiant technique
     */
    @Id
    @GeneratedValue
    @Column(name = "ID_TECH")
    private Long idTechnique;

    /**
     * L'identifiant technique de la période à laquelle ce message est rattaché.
     * 
     * @param idPeriode
     *            l'identifiant technique de la période
     * @return l'identifiant technique de la période
     */
    @Column(name = "ID_PERIODE")
    private Long idPeriode;

    /**
     * Numéro de catégorie de quittancement.
     * 
     * @param numCategorieQuittancement
     *            Le numéro de la catégorie de quittancement
     * @return Le numéro de la catégorie de quittancement
     */
    @Column(name = "NOCAT")
    private String numCategorieQuittancement;

    /**
     * NIR/NTT.
     * 
     * @param individu
     *            Le NIR/NTT
     * @return Le NIR/NTT
     */
    @Column(name = "INDIVIDU")
    private String individu;

    /**
     * Libellé de la categorie de quittancement.
     * 
     * @param libelleCategorieQuittancement
     *            Le libellé de la categorie de quittancement
     * @return Le libellé de la categorie de quittancement
     */
    @Column(name = "LICAT")
    private String libelleCategorieQuittancement;

    /**
     * Code nature de base cotisations.
     * 
     * @param codeNatureBaseCotisations
     *            Le Code nature de base cotisations
     * @return Le Code nature de base cotisations
     */
    @Column(name = "CONBCOT")
    private Integer codeNatureBaseCotisations;

    /**
     * Montant de la cotisation.
     * 
     * @param montantCotisation
     *            Le montant de cotisation
     * @return Le montant de la cotisation
     */
    @Column(name = "MT_COTISATION")
    private Double montantCotisation;

    /**
     * Effectif de début.
     * 
     * @param effectifDebut
     *            L'effectif de début
     * @return L'effectif de début
     */
    @Column(name = "EFFECTIF_DEBUT")
    private Long effectifDebut;

    /**
     * Effectif de fin.
     * 
     * @param effectifFin
     *            L'effectif de fin
     * @return L'effectif de fin
     */
    @Column(name = "EFFECTIF_FIN")
    private Long effectifFin;

    /**
     * Estimation Cotisation en montant
     * 
     * @param estimationCotisation
     *            l'estimationCotisation en montant
     * @return l'estimationCotisation en montant
     */
    @Column(name = "ESTIMATION_COTISATION")
    private Double estimationCotisation;

    @Override
    public Long getId() {
        return idTechnique;
    }

}
