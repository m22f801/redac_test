/**
 * 
 */
package fr.si2m.red.reconciliation;

import lombok.Getter;

/**
 * Types de données remontées dans le select des critères de recherche
 * 
 * @author eudesr
 *
 */
public enum TypeSelectCriteresRecherche {
    /**
     * Export Excel Periodes
     */
    EXPORT_PERIODES("EXCEL"),

    /**
     * Liste des ID_PERIODES
     */
    ID_PERIODES("ID_PERIODES"),
    /**
     * Liste des etats
     */
    ETATS("ETATS"),
    /**
     * Count des periodes
     */
    COUNT_PERIODES("COUNT"),

    /**
     * Count des adhesions
     */
    COUNT_ADHESION("COUNT"),
    /**
     * Liste de {@see ResumePeriodeRecue} pour l'IHM
     */
    IHM("IHM");

    @Getter
    String designation;

    private TypeSelectCriteresRecherche(String designation) {
        this.designation = designation;
    }
}
