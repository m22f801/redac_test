package fr.si2m.red.reconciliation;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import fr.si2m.red.DateRedac;
import fr.si2m.red.Entite;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * Modèle des historiques des mises en attentes et levées d'attente de retour entreprise
 * 
 * @author gahagnont
 *
 */
@Entity
@Table(name = "HISTO_ATTENTE_RETOUR_ETP_PERIODE")
@Data
@EqualsAndHashCode(callSuper = false, of = { "idTechnique" })
@ToString(callSuper = false, of = { "idTechnique", "idPeriode", "dateHeureRetour" })
public class HistoriqueAttenteRetourEtpPeriode extends Entite {
    /**
     * UID de version.
     */
    private static final long serialVersionUID = -3324801079594603125L;

    /**
     * identifiant controle modification via l'IHM
     */
    public static final String ORIGINE_CONTROLE_IHM = "IHM";

    /**
     * L'identifiant technique.
     * 
     * @param idTechnique
     *            l'identifiant technique
     * @return l'identifiant technique
     */
    @Id
    @GeneratedValue
    @Column(name = "ID_TECH")
    private Long idTechnique;

    /**
     * L'identifiant technique de la période à laquelle l'attente retour etp est rattachée.
     * 
     * @param idPeriode
     *            l'identifiant technique de la période
     * @return l'identifiant technique de la période
     */
    @Column(name = "ID_PERIODE")
    private Long idPeriode;

    /**
     * Libellé de l'action
     * 
     * @param action
     *            Libellé de l'action
     * @return Libellé de l'action
     */
    @Column(name = "ACTION")
    private String action;

    /**
     * Identifiant de l'utilisateur
     * 
     * @param identifiantUtilisateur
     *            L'identifiant de l'utilisateur
     * @return L'identifiant de l'utilisateur
     */
    @Column(name = "UTILISATEUR")
    private String identifiantUtilisateur;

    /**
     * Date / Heure / Minute / Seconde de retour
     * 
     * @param dateHeureRetour
     *            L'horodatage du retour
     * @return L'horodatage du retour
     */
    @Column(name = "DATEHMS_RETOUR")
    private Long dateHeureRetour;

    /**
     * Récupère la date-heure REDAC de retour en date-heure.
     * 
     * @return la date-heure de retour
     */
    public String getDateHeureRetourAsDate() {
        if (getDateHeureRetour() != null) {
            return DateRedac.convertionDateRedacSansValidation(getDateHeureRetour());
        }
        return null;
    }

    /**
     * Récupère l'action sous forme de phrase (libellé)
     * 
     * @return libellé de l'action
     */
    public String getLibelleAction() {
        switch (action) {
        case "FIN_ATTENTE":
            return "Levée de l'attente de retour entreprise";

        case "ATTENTE":
            return "Mise en attente de retour entreprise";

        case "Fusion : FIN_ATTENTE":
            return "Période fusionnée : Levée de l'attente de retour entreprise";

        case "Fusion : ATTENTE":
            return "Période fusionnée : Mise en attente de retour entreprise";

        default:
            return null;
        }
    }

    @Override
    public Long getId() {
        return idTechnique;
    }

}
