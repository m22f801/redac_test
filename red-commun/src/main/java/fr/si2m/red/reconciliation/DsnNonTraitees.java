package fr.si2m.red.reconciliation;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serializable;

/**
 * Représentation d'une période pour l'export excel périodes
 * 
 * @author eudesr
 *
 */
@Data
@AllArgsConstructor
public class DsnNonTraitees implements Serializable {

    /**
     * ID de version de la classe pour serialisation
     */
    private static final long serialVersionUID = 6715613194810626604L;

    /**
     * Valeur à utiliser pour des DSN Néant
     */
    public static final String DSN_NEANT = "DSN_NEANT";

    /**
     * Libelle représentant le groupe de gestion de la DSN
     * Non null pour : Excel
     */
    private String groupeGestion;

    /**
     * Libelle représentant la famille de la DSN
     * Non null pour : Excel, IHM
     */
    private String famille;

    /**
     * Numéro du contrat concerner par la DSN
     * Non null pour : Excel, IHM
     */
    private String referenceContrat;

    /**
     * Date de début de situation du contrat
     * Format : dd/mm/YYYY
     * Non null pour : Excel
     */
    private String dateSituation;

    /**
     * NCPROD du contrat
     * Non null pour : Excel
     */
    private String ncprod;

    /**
     * NCENC du contrat
     * Non null pour : Excel
     */
    private String ncenc;

    /**
     * Le numéro de siren de l'émetteur
     * Non null pour : Excel
     */
    private String sirenEmetteur;

    /**
     * Le NIC de l'émetteur
     * Non null pour : Excel
     */
    private String nicEmetteur;

    /**
     * Le nom de l'émetteur
     * Non null pour : Excel
     */
    private String nomEmetteur;

    /**
     * Le numéro de siren de l'entreprise
     * Non null pour : Excel, IHM
     */
    private String sirenEntreprise;

    /**
     * Le NIC de l'entreprise
     * Non null pour : Excel
     */
    private String nicEntreprise;

    /**
     * La raison sociale de l'entreprise
     * Non null pour : Excel, IHM
     */
    private String raisonSocialeEntreprise;

    /**
     * Le NIC de l'établissement
     * Non null pour : Excel, IHM
     */
    private String nicEtablissement;

    /**
     * L'enseigne de l'établissement
     * Non null pour : Excel
     */
    private String enseigneEtablissement;

    /**
     * Le mois de déclaration
     * Format : mm/YYYY
     * Non null pour : Excel
     */
    private String moisDeclaration;

    /**
     * Le mois de rattachement
     * Format : mm/YYYY
     * Non null pour : Excel, IHM
     */
    private String moisRattachement;

    /**
     * Date du fichier
     * Format : jj/mm/YYYY
     * Non null pour : Excel, IHM
     */
    private String dateFichier;

    /**
     * La date de création
     * Format : jj/mm/YYYY
     * Non null pour : Excel, IHM
     */
    private String dateCreation;

    /**
     * Le nombre d'individus de la DSN
     * Non null pour : Excel
     */
    private String nombreIndividu; //TODO : Le DSN néant ici et requette

    /**
     * Montant des cotisations
     * Potentiellement DSN_NEANT
     * Non null pour : Excel, IHM
     */
    private String montantCotisation;

    /**
     * Montant des versements effectuer
     * Non null pour : Excel
     */
    private Double montantVersement;

    /**
     * Le mode de paiement utiliser
     * Non null pour : Excel
     */
    private String modePaiement;

    /**
     * L'état de la DSN
     * Non null pour : Excel, IHM
     */
    private String etat;

    /**
     * Id correspondant dans la table adhesion_etablissement_mois
     * Non null pour : IHM
     */
    private String idAdhesionEtablissementMois;

    /**
     * NMGRPGES du contrat
     * Non null pour : IHM
     */
    private String nmgrpges;


    public DsnNonTraitees(){
        super();
    }
}
