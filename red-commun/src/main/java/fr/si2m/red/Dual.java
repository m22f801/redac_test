package fr.si2m.red;

/**
 * Modèle pour mapping de la table système DUAL des SGBD.
 * 
 * @author poidij
 *
 */
public class Dual extends EntiteImportableBatch {

    /**
     * UID de version.
     */
    private static final long serialVersionUID = -1745310495577726043L;

    @Override
    public boolean isLigneEnCoursImportBatch() {
        return false;
    }

    @Override
    public void setLigneEnCoursImportBatch(boolean ligneEnCoursImportBatch) {
        // Rien
    }

    @Override
    public Object getId() {
        return null;
    }

}
