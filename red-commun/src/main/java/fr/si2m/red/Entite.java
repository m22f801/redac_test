package fr.si2m.red;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.Getter;
import lombok.Setter;

/**
 * Définition d'une entité REDAC.
 * 
 * @author nortaina
 *
 */
@MappedSuperclass
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public abstract class Entite implements Serializable {

    /**
     * UID de version.
     */
    private static final long serialVersionUID = 6411773147866257961L;

    /**
     * L'utilisateur créateur de l'entité en base.
     * 
     * @param utilisateurCreation
     *            l'utilisateur créateur de l'entité en base
     * @return l'utilisateur créateur de l'entité en base
     */
    @Getter
    @Setter
    @Column(name = "USER_CREATION", insertable = true, updatable = false)
    private String auditUtilisateurCreation;

    /**
     * La date de création de l'entité en base (cette date est auto-remplie par le référentiel).
     * 
     * @param dateCreation
     *            la date de création de l'entité en base
     * @return la date de création de l'entité en base
     */
    @Column(name = "DT_CREATION", insertable = false, updatable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date auditDateCreation;

    /**
     * L'utilisateur responsable de la dernière modification de l'entité en base.
     * 
     * @param utilisateurDerniereModification
     *            l'utilisateur responsable de la dernière modification de l'entité en base
     * @return l'utilisateur responsable de la dernière modification de l'entité en base
     */
    @Getter
    @Setter
    @Column(name = "USER_MISE_A_JOUR")
    private String auditUtilisateurDerniereModification;

    /**
     * La date de la dernière modification de l'entité en base. updatable à false pour que JPA n'inclue pas la valeur courante dans la requête, et laisse mysql
     * gérer cet aspect.
     * 
     * @param dateDerniereModification
     *            la date de la dernière modification de l'entité en base
     * @return la date de la dernière modification de l'entité en base
     */
    @Column(name = "DT_MISE_A_JOUR", updatable = false, columnDefinition = "TIMESTAMP NULL ON UPDATE CURRENT_TIMESTAMP")
    @Temporal(TemporalType.TIMESTAMP)
    private Date auditDateDerniereModification;

    /**
     * L'ID de l'entité.
     * 
     * @return l'ID de l'entité
     */
    public abstract Object getId();

    /**
     * La date de création de l'entité en base (cette date est auto-remplie par le référentiel).
     * 
     * @return la date de création de l'entité en base
     */
    public Date getAuditDateCreation() {
        return auditDateCreation == null ? null : new Date(auditDateCreation.getTime());
    }

    /**
     * La date de création de l'entité en base (cette date est auto-remplie par le référentiel).
     * 
     * @param auditDateCreation
     *            la date de création de l'entité en base
     */
    public void setAuditDateCreation(Date auditDateCreation) {
        this.auditDateCreation = auditDateCreation == null ? null : new Date(auditDateCreation.getTime());
    }

    /**
     * La date de la dernière modification de l'entité en base.
     * 
     * @return la date de la dernière modification de l'entité en base
     */
    public Date getAuditDateDerniereModification() {
        return auditDateDerniereModification == null ? null : new Date(auditDateDerniereModification.getTime());
    }

    /**
     * La date de la dernière modification de l'entité en base.
     * 
     * @param auditDateDerniereModification
     *            la date de la dernière modification de l'entité en base
     */
    public void setAuditDateDerniereModification(Date auditDateDerniereModification) {
        this.auditDateDerniereModification = auditDateDerniereModification == null ? null : new Date(auditDateDerniereModification.getTime());
    }

}
