package fr.si2m.red.parametrage;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import fr.si2m.red.EntiteImportableBatch;

/**
 * Modèle de Paramètre CategoriesEffectifs DSN, pouvant être représenté notamment à l'intérieur d'un fichier ParamCategoriesEffectifs éligible DSN (au format
 * CSV).
 * 
 * 
 * @author nortaina
 *
 */
@Entity
@Table(name = "PARAM_CATEGORIES_EFFECTIFS")
@IdClass(ParamCategorieEffectifsId.class)
@Data
@EqualsAndHashCode(callSuper = false, of = { "ligneEnCoursImportBatch", "numCategorie", "libelleCategorie" })
@ToString(callSuper = false, of = { "ligneEnCoursImportBatch", "numCategorie", "libelleCategorie" })
public class ParamCategorieEffectifs extends EntiteImportableBatch {
    /**
     * UID de version.
     */
    private static final long serialVersionUID = 2611161146120286149L;

    /**
     * Indicateur de ligne temporaire en cours d'import via Batch.
     * 
     * @param ligneEnCoursImportBatch
     *            le nom du SI d'où provient l'information
     * @return le nom du SI d'où provient l'information
     */
    @Id
    @Column(name = "TMP_BATCH")
    private boolean ligneEnCoursImportBatch;

    /**
     * Le numéro de catégorie / code population.
     * 
     * @param numCategorie
     *            le numéro de catégorie / code population
     * @return le numéro de catégorie / code population
     */
    @Id
    @Column(name = "NOCAT")
    private String numCategorie;

    /**
     * La partie du libellé de catégorie / code population permettant une identification unique du libellé.
     * 
     * @param libelleCategorie
     *            la partie du libellé de catégorie / code population permettant une identification unique du libellé
     * @return la partie du libellé de catégorie / code population permettant une identification unique du libellé
     */
    @Id
    @Column(name = "LICAT")
    private String libelleCategorie;

    /**
     * Le niveau de priorité / poids / importance pour la catégorie de se voir affecter l’affilié.
     * 
     * @param prioriteAffilies
     *            le niveau de priorité / poids / importance pour la catégorie de se voir affecter l’affilié
     * @return le niveau de priorité / poids / importance pour la catégorie de se voir affecter l’affilié
     */
    @Column(name = "PRIO_AFFILIE")
    private Integer prioriteAffilies;

    /**
     * Le niveau de priorité / poids / importance pour la catégorie de se voir affecter les ayants-droit adultes.
     * 
     * @param prioriteAyantsDroitAdultes
     *            le niveau de priorité / poids / importance pour la catégorie de se voir affecter les ayants-droit adultes
     * @return le niveau de priorité / poids / importance pour la catégorie de se voir affecter les ayants-droit adultes
     */
    @Column(name = "PRIO_AYDR_ADULTE")
    private Integer prioriteAyantsDroitAdultes;

    /**
     * Le nombre d'ayants-droit au-dessus duquel les ayants-droit adultes peuvent être affectés à cette catégorie.
     * 
     * @param seuilAyantsDroitAdultes
     *            le nombre d'ayants-droit au-dessus duquel les ayants-droit adultes peuvent être affectés à cette catégorie
     * @return le nombre d'ayants-droit au-dessus duquel les ayants-droit adultes peuvent être affectés à cette catégorie
     */
    @Column(name = "SEUIL_DECPT_AYDR_ADULTE")
    private Integer seuilAyantsDroitAdultes;

    /**
     * Le niveau de priorité / poids / importance pour la catégorie de se voir affecter les ayants-droit autres.
     * 
     * @param prioriteAyantsDroitAutres
     *            le niveau de priorité / poids / importance pour la catégorie de se voir affecter les ayants-droit autres
     * @return le niveau de priorité / poids / importance pour la catégorie de se voir affecter les ayants-droit autres
     */
    @Column(name = "PRIO_AYDR_AUTRE")
    private Integer prioriteAyantsDroitAutres;

    /**
     * Le nombre d'ayants-droit au-dessus duquel les ayants-droit autres peuvent être affectés à cette catégorie.
     * 
     * @param seuilAyantsDroitAutres
     *            le nombre d'ayants-droit au-dessus duquel les ayants-droit autres peuvent être affectés à cette catégorie
     * @return le nombre d'ayants-droit au-dessus duquel les ayants-droit autres peuvent être affectés à cette catégorie
     */
    @Column(name = "SEUIL_DECPT_AYDR_AUTRE")
    private Integer seuilAyantsDroitAutres;

    /**
     * Le niveau de priorité / poids / importance pour la catégorie de se voir affecter les ayants-droit enfants.
     * 
     * @param prioriteAyantsDroitEnfants
     *            le niveau de priorité / poids / importance pour la catégorie de se voir affecter les ayants-droit enfants
     * @return le niveau de priorité / poids / importance pour la catégorie de se voir affecter les ayants-droit enfants
     */
    @Column(name = "PRIO_AYDR_ENFANT")
    private Integer prioriteAyantsDroitEnfants;

    /**
     * Le nombre d'ayants-droit au-dessus duquel les ayants-droit enfants peuvent être affectés à cette catégorie.
     * 
     * @param seuilAyantsDroitEnfants
     *            le nombre d'ayants-droit au-dessus duquel les ayants-droit enfants peuvent être affectés à cette catégorie
     * @return le nombre d'ayants-droit au-dessus duquel les ayants-droit enfants peuvent être affectés à cette catégorie
     */
    @Column(name = "SEUIL_DECPT_AYDR_ENFANT")
    private Integer seuilAyantsDroitEnfants;

    @Override
    public ParamCategorieEffectifsId getId() {
        return new ParamCategorieEffectifsId(ligneEnCoursImportBatch, numCategorie, libelleCategorie);
    }

}
