package fr.si2m.red.parametrage;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * Identifiant d'un paramétrage de CodePays.
 * 
 * @author eudesr
 *
 */
@Data
@EqualsAndHashCode(callSuper = false, of = { "ligneEnCoursImportBatch", "codePays" })
@ToString(callSuper = false, of = { "ligneEnCoursImportBatch", "codePays" })
@NoArgsConstructor
@AllArgsConstructor
public class ParamCodePaysId implements Serializable {

    /**
     * Generated UID
     */
    private static final long serialVersionUID = 4619509513491418523L;

    /**
     * Indicateur de ligne temporaire en cours d'import via Batch.
     * 
     */
    private boolean ligneEnCoursImportBatch;

    /**
     * Code Pays au format DSN
     * 
     * @param codePaysRedac
     *            le code Pays format DSN
     * @return le code Pays format DSN
     */
    private String codePays;

}
