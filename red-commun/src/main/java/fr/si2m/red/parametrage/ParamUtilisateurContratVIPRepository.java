package fr.si2m.red.parametrage;

import java.util.List;

import fr.si2m.red.core.repository.EntiteImportableRepository;

/**
 * Référentiel des contrats VIP auxquels un utilisateur gestionnaire a accès.
 * 
 * @author delortj
 *
 */
public interface ParamUtilisateurContratVIPRepository extends EntiteImportableRepository<ParamUtilisateurContratVIP> {

    /**
     * Récupère les numéros de contrats VIP sur lesquels un utilisateur gestionnaire a des droits.
     * 
     * @param identifiantActiveDirectory
     *            l'identifiant AD du gestionnaire
     * @return les numéros de contrats VIP sur lesquels l'utilisateur gestionnaire a des droits.
     */
    List<String> getContratsVIPPourUtilisateur(String identifiantActiveDirectory);

}
