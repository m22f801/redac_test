package fr.si2m.red.parametrage;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

import fr.si2m.red.EntiteImportableBatch;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * Modèle de Paramètre ControleSignalRejet DSN, pouvant être représenté notamment à l'intérieur d'un fichier ParamControleSignalRejet éligible DSN (au format
 * CSV).
 * 
 * @author nortaina
 *
 */
@Entity
@Table(name = "PARAM_CONTROLE_SIGNAL_REJET")
@IdClass(ParamControleSignalRejetId.class)
@Data
@EqualsAndHashCode(callSuper = false, of = { "ligneEnCoursImportBatch", "identifiantControle", "nofam" })
@ToString(callSuper = false, of = { "ligneEnCoursImportBatch", "identifiantControle", "nofam" })
public class ParamControleSignalRejet extends EntiteImportableBatch {
    /**
     * UID de version.
     */
    private static final long serialVersionUID = -7311018311154187067L;

    /**
     * Identifiant fonctionnel du contrôle sur les contrats résiliés.
     * 
     * @see #getIdentifiantControle()
     */
    public static final String ID_CONTRATS_RESILIES = "RED_RESILIE";

    /**
     * Identifiant fonctionnel du contrôle sur la présence de cotisation d'établissement.
     * 
     * @see #getIdentifiantControle()
     */
    public static final String ID_PRESENCE_COTISATION_ETABLISSEMENT = "RED_COTIS_ETAB";

    /**
     * Identifiant fonctionnel du contrôle de stabilité du nombre d'établissements entre les 2 dernières périodes déclarées.
     * 
     * @see #getIdentifiantControle()
     */
    public static final String ID_VARIATION_NB_ETABLISSEMENTS = "RED_VAR_NB_ETB";

    /**
     * Identifiant fonctionnel du contrôle de stabilité du nombre de salariés entre les 2 dernières périodes déclarées.
     * 
     * @see #getIdentifiantControle()
     */
    public static final String ID_VARIATION_NB_SALARIES = "RED_VAR_NB_SAL";

    /**
     * Identifiant fonctionnel du contrôle d'absence de mode de paiement particulier.
     * 
     * @see #getIdentifiantControle()
     */
    public static final String ID_MODE_PAIEMENT = "RED_MODE_PAIEMT";

    /**
     * Identifiant fonctionnel du contrôle de cohérence de la période d'affectation du composant de versement.
     * 
     * @see #getIdentifiantControle()
     */
    public static final String ID_PERIODE_VERSEMENT = "RED_PERIOD_VRST";

    /**
     * Identifiant fonctionnel du contrôle d'absence de trou dans la déclaration.
     * 
     * @see #getIdentifiantControle()
     */
    public static final String ID_TROU_DECLARATION = "RED_MOIS_ABSENT";

    /**
     * Identifiant fonctionnel du contrôle de conformité des types de bases assujetties déclarés.
     * 
     * @see #getIdentifiantControle()
     */
    public static final String ID_TYPES_BASES_ASSUJETTIES = "RED_TYPE_BASE_ASSUJ";

    /**
     * Identifiant fonctionnel du contrôle sur l'alimentation des tranches / effectifs (qu'ils ne soient pas tous à 0).
     * 
     * @see #getIdentifiantControle()
     */
    public static final String ID_ASSIETTES_VIDES = "RED_ASSIETTES_VIDES";

    /**
     * Identifiant fonctionnel du contrôle d'absence de changement de date de naissance d'un assuré.
     * 
     * @see #getIdentifiantControle()
     */
    public static final String ID_CHANGEMENT_DATE_NAISSANCE_ASSURE = "RED_CHGT_DATE_NAIS";

    /**
     * Identifiant fonctionnel du contrôle d'absence de changement de NIR d'un assuré.
     * 
     * @see #getIdentifiantControle()
     */
    public static final String ID_CHANGEMENT_NIR_ASSURE = "RED_CHGT_NIR";

    /**
     * Identifiant fonctionnel du contrôle des périodes (Contrats et situations tarifaires).
     * 
     * @see #getIdentifiantControle()
     */
    public static final String ID_CHANGEMENT_TARIF = "RED_CHGT_TARIF";

    /**
     * Identifiant fonctionnel du contrôle des périodes (Contrats et situations tarifaires).
     * 
     * @see #getIdentifiantControle()
     */
    public static final String ID_DIFF_COTIS = "RED_DIFF_COTIS";

    /**
     * Niveau d'alerte de type "signal".
     */
    public static final String NIVEAU_ALERTE_SIGNAL = "SIGNAL";

    /**
     * Niveau d'alerte de type "rejet".
     */
    public static final String NIVEAU_ALERTE_REJET = "REJET";

    /**
     * Niveau d'alerte de type "aucun".
     */
    public static final String NIVEAU_ALERTE_AUCUN = "AUCUN";

    /**
     * Indicateur de ligne temporaire en cours d'import via Batch.
     * 
     * @param ligneEnCoursImportBatch
     *            le nom du SI d'où provient l'information
     * @return le nom du SI d'où provient l'information
     */
    @Id
    @Column(name = "TMP_BATCH")
    private boolean ligneEnCoursImportBatch;

    /**
     * Le code identifiant le contrôle.
     * 
     * @param identifiantControle
     *            le code identifiant le contrôle
     * @return le code identifiant le contrôle
     */
    @Id
    @Column(name = "ID_CONTROLE")
    private String identifiantControle;

    /**
     * Le code de la famille du contrat.
     * 
     * @param nofam
     *            le code de la famille du contrat
     * @return le code de la famille du contrat
     */
    @Id
    @Column(name = "NOFAM")
    private String nofam;

    /**
     * Le code correspondant au niveau d'alerte pour ce contrôle.
     * 
     * @param niveauAlerte
     *            le code correspondant au niveau d'alerte pour ce contrôle
     * @return le code correspondant au niveau d'alerte pour ce contrôle
     */
    @Column(name = "NIVEAU_ALERTE")
    private String niveauAlerte;

    /**
     * Le code correspondant au niveau d'alerte pour ce contrôle.
     * 
     * @param messageAlerte
     *            le code correspondant au niveau d'alerte pour ce contrôle
     * @return le code correspondant au niveau d'alerte pour ce contrôle
     */
    @Column(name = "MSG_ALERTE")
    private String messageAlerte;

    @Override
    public ParamControleSignalRejetId getId() {
        return new ParamControleSignalRejetId(ligneEnCoursImportBatch, identifiantControle, nofam);
    }

}
