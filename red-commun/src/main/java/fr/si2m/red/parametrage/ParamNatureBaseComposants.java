package fr.si2m.red.parametrage;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import fr.si2m.red.EntiteImportableBatch;
import fr.si2m.red.core.repository.jpa.conversion.ConvertisseurBooleanON;

/**
 * Modèle de Paramètre NatureBaseComposants DSN, pouvant être représenté notamment à l'intérieur d'un fichier ParamNatureBaseComposants éligible DSN (au format
 * CSV).
 * 
 * @author nortaina
 * 
 */
@Entity
@Table(name = "PARAM_NATURE_BASE_COMPOSANTS")
@IdClass(ParamNatureBaseComposantsId.class)
@Data
@EqualsAndHashCode(callSuper = false, of = { "ligneEnCoursImportBatch", "codeNatureBaseCotisations", "tauxCalculRempliAsText", "numTranche",
        "numTypeComposantBase" })
@ToString(callSuper = false, of = { "ligneEnCoursImportBatch", "codeNatureBaseCotisations", "tauxCalculRempliAsText", "numTranche",
        "numTypeComposantBase" })
public class ParamNatureBaseComposants extends EntiteImportableBatch {
    /**
     * UID de version.
     */
    private static final long serialVersionUID = -5073455464682920180L;

    /**
     * Indicateur de ligne temporaire en cours d'import via Batch.
     * 
     * @param ligneEnCoursImportBatch
     *            le nom du SI d'où provient l'information
     * @return le nom du SI d'où provient l'information
     */
    @Id
    @Column(name = "TMP_BATCH", nullable = false)
    private boolean ligneEnCoursImportBatch;

    /**
     * Le code de nature de base dont la composition de la tranche est décrite par cette entité.
     * 
     * @param codeNatureBaseCotisations
     *            le code de nature de base dont la composition de la tranche est décrite par cette entité
     * @return le code de nature de base dont la composition de la tranche est décrite par cette entité
     */
    @Id
    @Column(name = "CONBCOT", nullable = false)
    private Integer codeNatureBaseCotisations;

    /**
     * Le numéro de tranche pour cette nature de base.
     * 
     * @param numTranche
     *            le numéro de tranche pour cette nature de base
     * @return le numéro de tranche pour cette nature de base
     */
    @Id
    @Column(name = "NUM_TRANCHE", nullable = false)
    private Integer numTranche;

    /**
     * Le numéro du type de composant de base assujettie DSN à utiliser.
     * 
     * @param numTypeComposantBase
     *            le numéro du type de composant de base assujettie DSN à utiliser
     * @return le numéro du type de composant de base assujettie DSN à utiliser
     */
    @Id
    @Column(name = "NUM_TCBA", nullable = false)
    private Integer numTypeComposantBase;

    /**
     * Indique si la tarification du contrat doit avoir un taux de calcul valorisé ou pas pour appliquer ce comportement.
     * 
     * @param tauxCalculRempliAsText
     *            'O' si la tarification du contrat doit avoir un taux de calcul valorisé ou pas pour appliquer ce comportement, 'N' sinon
     * @return 'O' si la tarification du contrat doit avoir un taux de calcul valorisé ou pas pour appliquer ce comportement, 'N' sinon
     */
    @Id
    @Column(name = "TXCALCU_REMPLI", nullable = false)
    private String tauxCalculRempliAsText;

    /**
     * Le mode de consolidation à utiliser entre les différents types de composant de base assujettie DSN d’une même tranche.
     * 
     * @param modeConsolidation
     *            le mode de consolidation à utiliser entre les différents types de composant de base assujettie DSN d’une même tranche
     * @return le mode de consolidation à utiliser entre les différents types de composant de base assujettie DSN d’une même tranche
     */
    @Column(name = "MODE_CONSO")
    private String modeConsolidation;

    /**
     * Indique si la tarification du contrat doit avoir un taux de calcul valorisé ou pas pour appliquer ce comportement.
     * 
     * @return true si la tarification du contrat doit avoir un taux de calcul valorisé ou pas pour appliquer ce comportement
     */
    public boolean isTauxCalculRempli() {
        return new ConvertisseurBooleanON().convertToEntityAttribute(getTauxCalculRempliAsText());
    }

    /**
     * Indique si la tarification du contrat doit avoir un taux de calcul valorisé ou pas pour appliquer ce comportement.
     * 
     * @param tauxCalculRempli
     *            true si la tarification du contrat doit avoir un taux de calcul valorisé ou pas pour appliquer ce comportement
     */
    public void setTauxCalculRempli(boolean tauxCalculRempli) {
        setTauxCalculRempliAsText(new ConvertisseurBooleanON().convertToDatabaseColumn(tauxCalculRempli));
    }

    @Override
    public ParamNatureBaseComposantsId getId() {
        return new ParamNatureBaseComposantsId(ligneEnCoursImportBatch, codeNatureBaseCotisations, tauxCalculRempliAsText, numTranche,
                numTypeComposantBase);
    }

}
