package fr.si2m.red.parametrage;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * Identifiant de paramétrage de contrôle de signal de rejet.
 * 
 * @author nortaina
 *
 */
@Data
@EqualsAndHashCode(callSuper = false, of = { "ligneEnCoursImportBatch", "identifiantControle", "nofam" })
@ToString(callSuper = false, of = { "ligneEnCoursImportBatch", "identifiantControle", "nofam" })
@NoArgsConstructor
@AllArgsConstructor
public class ParamControleSignalRejetId implements Serializable {
    /**
     * UID de version.
     */
    private static final long serialVersionUID = 1021284105594093929L;

    /**
     * Indicateur de ligne temporaire en cours d'import via Batch.
     * 
     * @param ligneEnCoursImportBatch
     *            le nom du SI d'où provient l'information
     * @return le nom du SI d'où provient l'information
     */
    private boolean ligneEnCoursImportBatch;

    /**
     * Le code identifiant le contrôle.
     * 
     * @param identifiantControle
     *            le code identifiant le contrôle
     * @return le code identifiant le contrôle
     */
    private String identifiantControle;

    /**
     * Le code de la famille du contrat.
     * 
     * @param nofam
     *            le code de la famille du contrat
     * @return le code de la famille du contrat
     */
    private String nofam;

}
