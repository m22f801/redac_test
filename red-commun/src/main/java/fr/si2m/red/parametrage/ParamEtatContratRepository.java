package fr.si2m.red.parametrage;

import fr.si2m.red.core.repository.EntiteImportableRepository;

/**
 * Référentiel des états de contrats.
 * 
 * @author nortaina
 *
 */
public interface ParamEtatContratRepository extends EntiteImportableRepository<ParamEtatContrat> {

    /**
     * Teste si l'état de contrat etatContrat existe
     * 
     * @param etatContrat
     *            La valeur de l'état de contrat à rechercher
     * @return true si etatContrat existe, false sinon
     */
    boolean existeParamEtatContrat(Integer etatContrat);

    /**
     * Vérifie qu'un code d'état donné correspond à un état de contrat actif.
     * 
     * @param etatContrat
     *            le code d'état de contrat à vérifier
     * @return true si le code d'état correspond à un état actif, false sinon
     */
    boolean verifieEtatContratActif(Integer etatContrat);

}
