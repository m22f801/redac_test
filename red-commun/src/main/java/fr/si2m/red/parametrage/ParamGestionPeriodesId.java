package fr.si2m.red.parametrage;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * Identifiant d'entités de paramétrage communes à tout un contexte SI.
 * 
 * @author poidij
 * 
 */
@Data
@EqualsAndHashCode(of = { "ligneEnCoursImportBatch", "delaiPublicationFinDeclaration5", "delaiPublicationFinDeclaration15",
        "delaiPublicationCreationDeclaration", "delaiPublicationCreationComplement", "delaiPublicationCreationRegulation" })
@ToString(of = { "ligneEnCoursImportBatch", "delaiPublicationFinDeclaration5", "delaiPublicationFinDeclaration15",
        "delaiPublicationCreationDeclaration", "delaiPublicationCreationComplement", "delaiPublicationCreationRegulation" })
@NoArgsConstructor
@AllArgsConstructor
public class ParamGestionPeriodesId implements Serializable {
    /**
     * UID de version.
     */
    private static final long serialVersionUID = -4649112762665146700L;

    /**
     * Indicateur de ligne temporaire en cours d'import via Batch.
     * 
     * @param ligneEnCoursImportBatch
     *            le nom du SI d'où provient l'information
     * @return le nom du SI d'où provient l'information
     */
    private boolean ligneEnCoursImportBatch;

    /**
     * Le délai en jour à partir duquel publier les périodes de déclaration dont la date d’échéance est au 5 du mois. Ce délai est ajouté à la date de fin des
     * périodes de déclaration dont la date d’échéance est le 5.
     * 
     * @param delaiPublicationFinDeclaration5
     *            le délai en jour à partir duquel publier les périodes de déclaration dont la date d’échéance est au 5 du mois
     * @return le délai en jour à partir duquel publier les périodes de déclaration dont la date d’échéance est au 5 du mois
     */
    private Integer delaiPublicationFinDeclaration5;

    /**
     * Le délai en jour à partir duquel publier les périodes de déclaration dont la date d’échéance est passée. Ce délai est ajouté à la date de fin des
     * périodes de déclaration dont la date d’échéance est le 15.
     * 
     * @param delaiPublicationFinDeclaration15
     *            le délai en jour à partir duquel publier les périodes de déclaration dont la date d’échéance est passée
     * @return le délai en jour à partir duquel publier les périodes de déclaration dont la date d’échéance est passée
     */
    private Integer delaiPublicationFinDeclaration15;

    /**
     * Le délai en jours entre la réception de la première déclaration d’une période de déclarations et l’envoi de la période au SI Quatrem.
     * 
     * @param delaiPublicationCreationDeclaration
     *            le délai en jours entre la réception de la première déclaration d’une période de déclarations et l’envoi de la période au SI Quatrem
     * @return le délai en jours entre la réception de la première déclaration d’une période de déclarations et l’envoi de la période au SI Quatrem
     */
    private Integer delaiPublicationCreationDeclaration;

    /**
     * Le délai en jours entre la réception de la première déclaration d’une période de complément et l’envoi de la période au SI Quatrem.
     * 
     * @param delaiPublicationCreationComplement
     *            le délai en jours entre la réception de la première déclaration d’une période de complément et l’envoi de la période au SI Quatrem
     * @return le délai en jours entre la réception de la première déclaration d’une période de complément et l’envoi de la période au SI Quatrem
     */
    private Integer delaiPublicationCreationComplement;

    /**
     * Le délai en jours entre la réception de la première déclaration d’une période de régularisation et l’envoi de la période au SI Quatrem.
     * 
     * @param delaiPublicationCreationRegulation
     *            le délai en jours entre la réception de la première déclaration d’une période de régularisation et l’envoi de la période au SI Quatrem
     * @return le délai en jours entre la réception de la première déclaration d’une période de régularisation et l’envoi de la période au SI Quatrem
     */
    private Integer delaiPublicationCreationRegulation;

}
