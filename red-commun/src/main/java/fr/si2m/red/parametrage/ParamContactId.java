package fr.si2m.red.parametrage;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * Identifiant d'un paramétrage de contact.
 * 
 * @author nortaina
 *
 */
@Data
@EqualsAndHashCode(callSuper = false, of = { "ligneEnCoursImportBatch", "identifiantContact", "debut" })
@ToString(callSuper = false, of = { "ligneEnCoursImportBatch", "identifiantContact", "debut" })
@NoArgsConstructor
@AllArgsConstructor
public class ParamContactId implements Serializable {
    /**
     * UID de version.
     */
    private static final long serialVersionUID = 6740585021175930539L;

    /**
     * Indicateur de ligne temporaire en cours d'import via Batch.
     * 
     * @param ligneEnCoursImportBatch
     *            le nom du SI d'où provient l'information
     * @return le nom du SI d'où provient l'information
     */
    private boolean ligneEnCoursImportBatch;

    /**
     * L'identifiant du contact.
     * 
     * @param contact
     *            l'identifiant du contact
     * @return l'identifiant du contact
     */
    private String identifiantContact;

    /**
     * La date de début de l'application (au format SSAAMMJJ).
     * 
     * @param debut
     *            la date de début de l'application
     * @return la date de début de l'application
     */
    private Integer debut;
}
