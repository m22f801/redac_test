package fr.si2m.red.parametrage;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

import fr.si2m.red.EntiteImportableBatch;
import fr.si2m.red.core.repository.jpa.conversion.ConvertisseurBooleanOuiNon;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * Données de paramétrage des familles de contrat.
 * 
 * @author nortaina
 *
 */
@Entity
@Table(name = "PARAM_FAMILLE_CONTRAT")
@IdClass(ParamFamilleContratId.class)
@Data
@EqualsAndHashCode(callSuper = false, of = { "ligneEnCoursImportBatch", "numFamille", "groupeGestion" })
@ToString(callSuper = false, of = { "ligneEnCoursImportBatch", "numFamille", "groupeGestion" })
public class ParamFamilleContrat extends EntiteImportableBatch {
    /**
     * UID de version.
     */
    private static final long serialVersionUID = -5749967192793586028L;

    /**
     * Indicateur de ligne temporaire en cours d'import via Batch.
     * 
     * @param ligneEnCoursImportBatch
     *            le nom du SI d'où provient l'information
     * @return le nom du SI d'où provient l'information
     */
    @Id
    @Column(name = "TMP_BATCH")
    private boolean ligneEnCoursImportBatch;

    /**
     * Le numéro de famille du contrat.
     * 
     * @param numFamille
     *            le numéro de famille du contrat
     * @return le numéro de famille du contrat
     */
    @Id
    @Column(name = "NOFAM")
    private Integer numFamille;

    /**
     * La désignation de la famille.
     * 
     * @param designationFamille
     *            la désignation de la famille
     * @return la désignation de la famille
     */
    @Column(name = "DESIGNATION_FAMILLE")
    private String designationFamille;

    /**
     * Le code de la brique DSN correspondant au type de contrat.
     * 
     * @param designationFamille
     *            la désignation de la famille
     * @return la désignation de la famille
     */
    @Column(name = "TYPE_CONT_BRIQUEDSN")
    private String typeContratCodeDSN;

    /**
     * L'application Quatrem en charge de ce contrat.
     * 
     * @param applicationAvalEnCharge
     *            l'application Quatrem en charge de ce contrat
     * @return l'application Quatrem en charge de ce contrat
     */
    @Column(name = "SI_AVAL")
    private String applicationAvalEnCharge;

    /**
     * Le code indiquant le type de consolidation à appliquer pour les contrats sur salaire (TOUS ou INDIV).
     * 
     * @param typeConsolidationSalaire
     *            le code indiquant le type de consolidation à appliquer pour les contrats sur salaire
     * @return le code indiquant le type de consolidation à appliquer pour les contrats sur salaire
     */
    @Column(name = "TYPE_CONSO_SALAIRE")
    private String typeConsolidationSalaire;

    /**
     * Assureur / porteur de risque de cette famille de contrat
     * 
     * @param portefeuille
     *            Assureur / porteur de risque de cette famille de contrat
     * @return le portefeuille ( Assureur / porteur de risque de cette famille de contrat )
     */
    @Column(name = "PORTEFEUILLE")
    private String portefeuille;

    /**
     * Indique si la population doit être affichée dans la publication
     * 
     * @param avecPopulation
     *            Indique si la population doit être affichée dans la publication
     * @return avecPopulation , true si affichage souhaite pour la famille
     */
    @Column(name = "AVEC_POPULATION")
    private String avecPopulation;

    /**
     * Le groupe de gestion
     * 
     * @param groupeGestion
     *            Le groupe de gestion
     * @return Le groupe de gestion
     */
    @Id
    @Column(name = "NMGRPGES")
    private String groupeGestion;

    @Override
    public ParamFamilleContratId getId() {
        return new ParamFamilleContratId(ligneEnCoursImportBatch, numFamille, groupeGestion);
    }

    /**
     * L'indicateur d'affichage de la population
     * 
     * @return l'indicateur d'affichage de la population
     */
    public boolean isAvecPopulation() {
        return new ConvertisseurBooleanOuiNon().convertToEntityAttribute(avecPopulation);
    }
}
