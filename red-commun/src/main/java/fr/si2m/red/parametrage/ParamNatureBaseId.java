package fr.si2m.red.parametrage;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * Identifiant d'un groupe de gestion.
 * 
 * @author nortaina
 * 
 */
@Data
@EqualsAndHashCode(callSuper = false, of = { "ligneEnCoursImportBatch", "codeNatureBaseCotisations", "tauxCalculRempliAsText", "numTranche" })
@ToString(callSuper = false, of = { "ligneEnCoursImportBatch", "codeNatureBaseCotisations", "tauxCalculRempliAsText", "numTranche" })
@NoArgsConstructor
@AllArgsConstructor
public class ParamNatureBaseId implements Serializable {
    /**
     * UID de version.
     */
    private static final long serialVersionUID = 8923696340182621676L;

    /**
     * Indicateur de ligne temporaire en cours d'import via Batch.
     * 
     * @param ligneEnCoursImportBatch
     *            le nom du SI d'où provient l'information
     * @return le nom du SI d'où provient l'information
     */
    private boolean ligneEnCoursImportBatch;

    /**
     * Le code de nature de base des cotisations que la tarification du contrat doit avoir pour appliquer ce comportement.
     * 
     * @param codeNatureBaseCotisations
     *            le code de nature de base des cotisations que la tarification du contrat doit avoir pour appliquer ce comportement
     * @return le code de nature de base des cotisations que la tarification du contrat doit avoir pour appliquer ce comportement
     */
    private Integer codeNatureBaseCotisations;

    /**
     * Indique si la tarification du contrat doit avoir un taux de calcul valorisé ou pas pour appliquer ce comportement.
     * 
     * @param tauxCalculRempliAsText
     *            'O' si la tarification du contrat doit avoir un taux de calcul valorisé ou pas pour appliquer ce comportement, 'N' sinon
     * @return 'O' si la tarification du contrat doit avoir un taux de calcul valorisé ou pas pour appliquer ce comportement, 'N' sinon
     */
    private String tauxCalculRempliAsText;

    /**
     * Le numéro de tranche à transmettre à WQUI/GERD pour alimenter la tranche correspondante. Il est compris entre 1 et 4.
     * 
     * @param numTranche
     *            le numéro de tranche à transmettre à WQUI/GERD pour alimenter la tranche correspondante
     * @return le numéro de tranche à transmettre à WQUI/GERD pour alimenter la tranche correspondante
     */
    private Integer numTranche;
}
