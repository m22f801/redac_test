package fr.si2m.red.parametrage;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import fr.si2m.red.EntiteImportableBatch;
import fr.si2m.red.core.repository.jpa.conversion.ConvertisseurBooleanON;

/**
 * Modèle de Paramètre EtatContrat DSN, pouvant être représenté notamment à l'intérieur d'un fichier ParamEtatContrat éligible DSN (au format CSV). <br/>
 * <br/>
 * Cette entité sert à référencer les états de contrats compréhensibles dans REDAC et s'ils peuvent être considérés comme actifs.
 * 
 * @author nortaina
 *
 */
@Entity
@Table(name = "PARAM_ETAT_CONTRAT")
@IdClass(ParamEtatContratId.class)
@Data
@EqualsAndHashCode(callSuper = false, of = { "ligneEnCoursImportBatch", "etatContrat" })
@ToString(callSuper = false, of = { "ligneEnCoursImportBatch", "etatContrat" })
public class ParamEtatContrat extends EntiteImportableBatch {
    /**
     * UID de version.
     */
    private static final long serialVersionUID = -889036425674255073L;

    /**
     * Indicateur de ligne temporaire en cours d'import via Batch.
     * 
     * @param ligneEnCoursImportBatch
     *            le nom du SI d'où provient l'information
     * @return le nom du SI d'où provient l'information
     */
    @Id
    @Column(name = "TMP_BATCH")
    private boolean ligneEnCoursImportBatch;

    /**
     * Le code d'état de contrat à considérer comme actif ou non.
     * 
     * @param etatContrat
     *            le code d'état de contrat à considérer comme actif ou non
     * @return le code d'état de contrat à considérer comme actif ou non
     */
    @Id
    @Column(name = "COETACO", nullable = false)
    private Integer etatContrat;

    /**
     * Indicateur d'état de contrat actif.
     * 
     * @param actif
     *            l'indicateur d'état de contrat actif
     * @return l'indicateur d'état de contrat actif
     */
    @Column(name = "ACTIF", nullable = false)
    private String actifAsText;

    /**
     * Indicateur d'état de contrat actif.
     * 
     * @return l'indicateur d'état de contrat actif
     */
    public boolean isActif() {
        return new ConvertisseurBooleanON().convertToEntityAttribute(getActifAsText());
    }

    /**
     * Indicateur d'état de contrat actif.
     * 
     * @param actif
     *            l'indicateur d'état de contrat actif
     */
    public void setActif(boolean actif) {
        setActifAsText(new ConvertisseurBooleanON().convertToDatabaseColumn(actif));
    }

    @Override
    public ParamEtatContratId getId() {
        return new ParamEtatContratId(ligneEnCoursImportBatch, etatContrat);
    }

}
