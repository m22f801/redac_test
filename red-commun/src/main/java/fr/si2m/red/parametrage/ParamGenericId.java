package fr.si2m.red.parametrage;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * Identifiant d'entités de paramétrage communes à tout un contexte SI.
 * 
 * @author nortaina
 * 
 */
@Data
@EqualsAndHashCode(of = { "ligneEnCoursImportBatch" })
@ToString(of = { "ligneEnCoursImportBatch" })
@NoArgsConstructor
@AllArgsConstructor
public class ParamGenericId implements Serializable {
    /**
     * UID de version.
     */
    private static final long serialVersionUID = -4649112762665146700L;

    /**
     * Indicateur de ligne temporaire en cours d'import via Batch.
     * 
     * @param ligneEnCoursImportBatch
     *            le nom du SI d'où provient l'information
     * @return le nom du SI d'où provient l'information
     */
    private boolean ligneEnCoursImportBatch;

}
