package fr.si2m.red.parametrage;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * Identifiant d'un contrat VIP sur lequel un utilisateur gestionnaire a des droits.
 * 
 * @author delortj
 * 
 */
@Data
@EqualsAndHashCode(callSuper = false, of = { "ligneEnCoursImportBatch", "codeUtilisateur", "numContrat" })
@ToString(callSuper = false, of = { "ligneEnCoursImportBatch", "codeUtilisateur", "numContrat" })
@NoArgsConstructor
@AllArgsConstructor
public class ParamUtilisateurContratVIPId implements Serializable {

    /**
     * UID de version.
     */
    private static final long serialVersionUID = -4547140678334174131L;

    /**
     * Indicateur de ligne temporaire en cours d'import via Batch.
     * 
     * @param ligneEnCoursImportBatch
     *            le nom du SI d'où provient l'information
     * @return le nom du SI d'où provient l'information
     */
    private boolean ligneEnCoursImportBatch;

    /**
     * Nom de la table (entité) contenant le code à présenter.
     * 
     * @param code
     *            utilisateur
     * @return codeUtilisateur
     */
    private String codeUtilisateur;

    /**
     * Numéro de contrat VIP autorisé à cet utilisateur.
     * 
     * @param numContrat
     *            numero de contrat VIP
     * 
     * @return numContrat numero de contrat VIP
     */
    private String numContrat;

}
