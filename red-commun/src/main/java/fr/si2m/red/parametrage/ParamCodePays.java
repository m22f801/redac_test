package fr.si2m.red.parametrage;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

import fr.si2m.red.EntiteImportableBatch;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * Modèle de Paramètre CodePays représenté à l'intérieur d'un fichier ParamCodePays (au format CSV).
 * 
 * 
 * @author eudesr
 *
 */
@Entity
@Table(name = "PARAM_CODE_PAYS")
@IdClass(ParamCodePaysId.class)
@Data
@EqualsAndHashCode(callSuper = false, of = { "ligneEnCoursImportBatch", "codePays" })
@ToString(callSuper = false, of = { "ligneEnCoursImportBatch", "codePays" })
public class ParamCodePays extends EntiteImportableBatch {

    /**
     * Generated UID
     */
    private static final long serialVersionUID = -5110550708824121805L;

    /**
     * Indicateur de ligne temporaire en cours d'import via Batch.
     * 
     */
    @Id
    @Column(name = "TMP_BATCH")
    private boolean ligneEnCoursImportBatch;

    /**
     * Le code Pays - norme DSN
     */
    @Id
    @Column(name = "CODE_PAYS")
    private String codePays;

    /**
     * Le code Pays norme Insee
     */
    @Column(name = "CODE_INSEE")
    private String codeInsee;

    /**
     * Le libellé du pays
     */
    @Column(name = "LIBELLE_PAYS")
    private String libellePays;

    @Override
    public ParamCodePaysId getId() {
        return new ParamCodePaysId(ligneEnCoursImportBatch, codePays);
    }

}
