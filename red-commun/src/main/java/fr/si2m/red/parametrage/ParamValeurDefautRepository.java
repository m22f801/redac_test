package fr.si2m.red.parametrage;

import fr.si2m.red.core.repository.EntiteImportableRepository;

/**
 * Référentiel des valeurs par défaut.
 * 
 * @author nortaina
 *
 */
public interface ParamValeurDefautRepository extends EntiteImportableRepository<ParamValeurDefaut> {
    /**
     * Récupère le code organisme (chaîne) par défaut.
     * 
     * @return le code organisme (chaîne) par défaut
     */
    String getCodeOrganismeParDefaut();

    /**
     * Récupère le mode de nature de contrat par défaut.
     * 
     * @return le mode de nature de contrat par défaut
     */
    String getModeNatureContratParDefaut();

    /**
     * Retourne l'indicateur de contrat VIP par défaut.
     * 
     * @return l'indicateur de contrat VIP par défaut
     */
    String getVIPParDefaut();

    /**
     * Récupère le mode de réaffectation de catégorie d'effectifs par défaut.
     * 
     * @return le mode de réaffectation de catégorie d'effectifs par défaut
     */
    String getModeReaffectationCategorieEffectifsParDefaut();

    /**
     * Récupère l'intégralité des paramètres par défaut.
     * 
     * @return l'intégralité des paramètres par défaut
     */
    ParamValeurDefaut getParamValeurDefaut();

}
