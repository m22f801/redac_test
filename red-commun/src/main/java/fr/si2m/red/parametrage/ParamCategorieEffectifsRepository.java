package fr.si2m.red.parametrage;

import fr.si2m.red.core.repository.EntiteImportableRepository;

/**
 * Référentiel des paramétrages de catégories d'effectifs.
 * 
 * @author nortaina
 *
 */
public interface ParamCategorieEffectifsRepository extends EntiteImportableRepository<ParamCategorieEffectifs> {

    /**
     * Vérifie qu'il existe une catégorie d'effectifs correspondant à un numéro et un libellé donné.
     * 
     * @param numCategorie
     *            le numéro de catégorie
     * @param libelleCategorie
     *            le libellé de catégorie
     * @return true s'il existe une catégorie correspondant aux paramètres donnés, false sinon
     */
    boolean existeUnParamCategorieEffectifs(String numCategorie, String libelleCategorie);
}
