package fr.si2m.red.parametrage;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

import fr.si2m.red.EntiteImportableBatch;
import fr.si2m.red.core.repository.jpa.conversion.ConvertisseurBooleanON;
import fr.si2m.red.core.repository.jpa.conversion.ConvertisseurBooleanOuiNon;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * Modèle de Paramètre ValeurParDefaut DSN, pouvant être représenté notamment à l'intérieur d'un fichier ParamValeurParDefaut éligible DSN (au format CSV).<br/>
 * <br/>
 * Cette entité sert à spécifier le mode de calcul des cotisations d'une famille de contrat.
 * 
 * @author nortaina
 * 
 */
@Entity
@Table(name = "PARAM_VALEUR_DEFAUT")
@IdClass(ParamValeurDefautId.class)
@Data
@EqualsAndHashCode(callSuper = false, of = { "ligneEnCoursImportBatch", "codeOrganisme", "exploitationDSNAsText",
        "editionConsignePaiementModeDSNAsText", "transfertDeclarationsVersQuatremAutoriseAsText", "seuilVariationAlertesEnNbEtablissements",
        "seuilVariationAlertesEnPourcentageNbEtablissements", "seuilVariationAlertesEnNbSalaries", "seuilVariationAlertesEnPourcentageNbSalaries",
        "modeNatureContrat", "contratVIPAsText", "modeReaffectationCategorieEffectifs", "referenceGestionnaire", "libelleGestionnaire",
        "prenomGestionnaire", "nomGestionnaire", "uniteGestionGestionnaire", "serviceGestionnaire", "centreGestionGestionnaire",
        "seuilDifferenceCotisationEnMontant", "seuilDifferenceCotisationEnPourcentage" })
@ToString(callSuper = false, of = { "ligneEnCoursImportBatch", "codeOrganisme", "exploitationDSNAsText", "editionConsignePaiementModeDSNAsText",
        "transfertDeclarationsVersQuatremAutoriseAsText", "seuilVariationAlertesEnNbEtablissements",
        "seuilVariationAlertesEnPourcentageNbEtablissements", "seuilVariationAlertesEnNbSalaries", "seuilVariationAlertesEnPourcentageNbSalaries",
        "modeNatureContrat", "contratVIPAsText", "modeReaffectationCategorieEffectifs", "referenceGestionnaire", "libelleGestionnaire",
        "prenomGestionnaire", "nomGestionnaire", "uniteGestionGestionnaire", "serviceGestionnaire", "centreGestionGestionnaire",
        "seuilDifferenceCotisationEnMontant", "seuilDifferenceCotisationEnPourcentage" })
public class ParamValeurDefaut extends EntiteImportableBatch {

    /**
     * UID de version.
     */
    private static final long serialVersionUID = 5058667954793244189L;

    /**
     * Indicateur de ligne temporaire en cours d'import via Batch.
     * 
     * @param ligneEnCoursImportBatch
     *            le nom du SI d'où provient l'information
     * @return le nom du SI d'où provient l'information
     */
    @Id
    @Column(name = "TMP_BATCH")
    private boolean ligneEnCoursImportBatch;

    /**
     * Le code d'organisme de référence.
     * 
     * @param codeOrganisme
     *            le code d'organisme de référence
     * @return le code d'organisme de référence
     */
    @Id
    @Column(name = "CODE_ORGANISME")
    private String codeOrganisme;

    /**
     * L'indicateur d'exploitation DSN.
     * 
     * @param exploitationDSN
     *            l'indicateur d'exploitation DSN
     * @return l'indicateur d'exploitation DSN
     */
    @Id
    @Column(name = "EXPLOIT")
    private String exploitationDSNAsText;

    /**
     * L'indicateur d'édition des consignes de paiement en mode de gestion DSN.
     * 
     * @param codeOrganisme
     *            l'indicateur d'édition des consignes de paiement en mode de gestion DSN
     * @return l'indicateur d'édition des consignes de paiement en mode de gestion DSN
     */
    @Id
    @Column(name = "EDIT_CONS_PAIMT")
    private String editionConsignePaiementModeDSNAsText;

    /**
     * L'indicateur d'autorisation des transferts de déclarations vers Quatrem.
     * 
     * @param transfertDeclarationsVersQuatremAutorise
     *            l'indicateur d'autorisation des transferts de déclarations vers Quatrem
     * @return l'indicateur d'autorisation des transferts de déclarations vers Quatrem
     */
    @Id
    @Column(name = "TRANSFERT")
    private String transfertDeclarationsVersQuatremAutoriseAsText;

    /**
     * La variation minimale, en nombre d’établissements, entre deux périodes de déclaration pour lancer une alerte.
     * 
     * @param seuilVariationAlertesEnNbEtablissements
     *            la variation minimale, en nombre d’établissements, entre deux périodes de déclaration pour lancer une alerte
     * @return la variation minimale, en nombre d’établissements, entre deux périodes de déclaration pour lancer une alerte
     */
    @Id
    @Column(name = "VAR_ETAB_NB")
    private Integer seuilVariationAlertesEnNbEtablissements;

    /**
     * La variation minimale, en % de nombre d’établissements, entre deux périodes de déclaration pour lancer une alerte.
     * 
     * @param transfertDeclarationsVersQuatremAutorise
     *            la variation minimale, en % de nombre d’établissements, entre deux périodes de déclaration pour lancer une alerte
     * @return la variation minimale, en % de nombre d’établissements, entre deux périodes de déclaration pour lancer une alerte
     */
    @Id
    @Column(name = "VAR_ETAB_PC")
    private Integer seuilVariationAlertesEnPourcentageNbEtablissements;

    /**
     * La variation minimale, en nombre de salariés, entre deux périodes de déclaration pour lancer une alerte.
     * 
     * @param seuilVariationAlertesEnNbSalaries
     *            la variation minimale, en nombre de salariés, entre deux périodes de déclaration pour lancer une alerte
     * @return la variation minimale, en nombre de salariés, entre deux périodes de déclaration pour lancer une alerte
     */
    @Id
    @Column(name = "VAR_SAL_NB")
    private Integer seuilVariationAlertesEnNbSalaries;

    /**
     * La variation minimale, en % de nombre de salariés, entre deux périodes de déclaration pour lancer une alerte.
     * 
     * @param seuilVariationAlertesEnPourcentageNbSalaries
     *            la variation minimale, en % de nombre de salariés, entre deux périodes de déclaration pour lancer une alerte
     * @return la variation minimale, en % de nombre de salariés, entre deux périodes de déclaration pour lancer une alerte
     */
    @Id
    @Column(name = "VAR_SAL_PC")
    private Integer seuilVariationAlertesEnPourcentageNbSalaries;

    /**
     * L'indicateur de contrat VIP.
     * 
     * @param contratVIP
     *            l'indicateur de contrat VIP
     * @return l'indicateur de contrat VIP
     */
    @Id
    @Column(name = "VIP")
    private String contratVIPAsText;

    /**
     * Indique si un contrat est par défaut un contrat direct ou délégué.
     * 
     * @param modeNatureContrat
     *            si un contrat est par défaut un contrat direct ou délégué
     * @return si un contrat est par défaut un contrat direct ou délégué
     */
    @Id
    @Column(name = "MODE_NAT_CONT")
    private String modeNatureContrat;

    /**
     * Le mode de prise en compte des effectifs pour les contrats sur effectifs.
     * 
     * @param modeReaffectationCategorieEffectifs
     *            le mode de prise en compte des effectifs pour les contrats sur effectifs
     * @return le mode de prise en compte des effectifs pour les contrats sur effectifs
     */
    @Id
    @Column(name = "MODE_REAFF_CAT_EFFECTIF")
    private String modeReaffectationCategorieEffectifs;

    /**
     * La référence du gestionnaire.
     * 
     * @param referenceGestionnaire
     *            la référence du gestionnaire
     * @return la référence du gestionnaire
     */
    @Column(name = "REFERENCE_GEST")
    private String referenceGestionnaire;

    /**
     * Le libellé du gestionnaire.
     * 
     * @param libelleGestionnaire
     *            le libellé du gestionnaire
     * @return le libellé du gestionnaire
     */
    @Column(name = "LIBELLE_GEST")
    private String libelleGestionnaire;

    /**
     * Le prénom du gestionnaire.
     * 
     * @param prenomGestionnaire
     *            le prénom du gestionnaire
     * @return le prénom du gestionnaire
     */
    @Column(name = "PRENOM_GEST")
    private String prenomGestionnaire;

    /**
     * Le nom du gestionnaire.
     * 
     * @param nomGestionnaire
     *            le nom du gestionnaire
     * @return le nom du gestionnaire
     */
    @Column(name = "NOM_GEST")
    private String nomGestionnaire;

    /**
     * L'unité de gestion du gestionnaire.
     * 
     * @param uniteGestionGestionnaire
     *            l'unité de gestion du gestionnaire
     * @return l'unité de gestion du gestionnaire
     */
    @Column(name = "UNITE_GESTION_GEST")
    private String uniteGestionGestionnaire;

    /**
     * Le service du gestionnaire.
     * 
     * @param serviceGestionnaire
     *            le service du gestionnaire
     * @return le service du gestionnaire
     */
    @Column(name = "SERVICE_GEST")
    private String serviceGestionnaire;

    /**
     * Le centre de gestion du gestionnaire.
     * 
     * @param centreGestionGestionnaire
     *            le centre de gestion du gestionnaire
     * @return le centre de gestion du gestionnaire
     */
    @Column(name = "CENTRE_GESTION_GEST")
    private String centreGestionGestionnaire;

    /**
     * Variation minimale, en montant, entre les montants de cotisations déclarés et les montants de cotisation théoriques calculés pour lancer une alerte
     * 
     * @param seuilDifferenceCotisationEnMontant
     *            le seuil en montant
     * @return le seuil en montant
     */
    @Column(name = "SEUIL_DIFF_COT_MT")
    private Integer seuilDifferenceCotisationEnMontant;

    /**
     * Variation minimale, en pourcentage, entre les montants de cotisations déclarés et les montants de cotisation théoriques calculés pour lancer une alerte
     * 
     * @param seuilDifferenceCotisationEnMontant
     *            le seuil en pourcentage
     * @return le seuil en pourcentage
     */
    @Column(name = "SEUIL_DIFF_COT_PC")
    private Integer seuilDifferenceCotisationEnPourcentage;

    /**
     * Indicateur d'état de contrat actif.
     * 
     * @return l'indicateur d'état de contrat actif
     */
    public boolean isContratVIP() {
        return new ConvertisseurBooleanON().convertToEntityAttribute(getContratVIPAsText());
    }

    /**
     * Indicateur d'état de contrat actif.
     * 
     * @param contratVIP
     *            l'indicateur d'état de contrat actif
     */
    public void setContratVIP(boolean contratVIP) {
        setContratVIPAsText(new ConvertisseurBooleanON().convertToDatabaseColumn(contratVIP));
    }

    /**
     * L'indicateur d'exploitation DSN.
     * 
     * @return l'indicateur d'exploitation DSN
     */
    public boolean isExploitationDSN() {
        return new ConvertisseurBooleanOuiNon().convertToEntityAttribute(getExploitationDSNAsText());
    }

    /**
     * Indicateur d'état de contrat actif.
     * 
     * @param exploitationDSN
     *            l'indicateur d'état de contrat actif
     */
    public void setExploitationDSN(boolean exploitationDSN) {
        setExploitationDSNAsText(new ConvertisseurBooleanOuiNon().convertToDatabaseColumn(exploitationDSN));
    }

    /**
     * L'indicateur d'édition des consignes de paiement en mode de gestion DSN.
     * 
     * @return l'indicateur d'édition des consignes de paiement en mode de gestion DSN
     */
    public boolean isEditionConsignePaiementModeDSN() {
        return new ConvertisseurBooleanOuiNon().convertToEntityAttribute(getEditionConsignePaiementModeDSNAsText());
    }

    /**
     * L'indicateur d'édition des consignes de paiement en mode de gestion DSN.
     * 
     * @param editionConsignePaiementModeDSN
     *            l'indicateur d'édition des consignes de paiement en mode de gestion DSN
     */
    public void setEditionConsignePaiementModeDSN(boolean editionConsignePaiementModeDSN) {
        setEditionConsignePaiementModeDSNAsText(new ConvertisseurBooleanOuiNon().convertToDatabaseColumn(editionConsignePaiementModeDSN));
    }

    /**
     * L'indicateur d'autorisation des transferts de déclarations vers Quatrem.
     * 
     * @return l'indicateur d'autorisation des transferts de déclarations vers Quatrem
     */
    public boolean isTransfertDeclarationsVersQuatremAutorise() {
        return new ConvertisseurBooleanOuiNon().convertToEntityAttribute(getTransfertDeclarationsVersQuatremAutoriseAsText());
    }

    /**
     * L'indicateur d'autorisation des transferts de déclarations vers Quatrem.
     * 
     * @param transfertDeclarationsVersQuatremAutorise
     *            l'indicateur d'autorisation des transferts de déclarations vers Quatrem
     */
    public void setTransfertDeclarationsVersQuatremAutorise(boolean transfertDeclarationsVersQuatremAutorise) {
        setTransfertDeclarationsVersQuatremAutoriseAsText(
                new ConvertisseurBooleanOuiNon().convertToDatabaseColumn(transfertDeclarationsVersQuatremAutorise));
    }

    @Override
    public ParamValeurDefautId getId() {
        return new ParamValeurDefautId(ligneEnCoursImportBatch);
    }
}
