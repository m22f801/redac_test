package fr.si2m.red.parametrage;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import fr.si2m.red.EntiteImportableBatch;
import fr.si2m.red.core.repository.jpa.conversion.ConvertisseurBooleanON;

/**
 * Modèle de Paramètres des utilisateurs gestionnaires DSN, pouvant être représenté notamment à l'intérieur d'un fichier ParamUtilisateurGestionnaire DSN (au
 * format CSV).<br/>
 * <br/>
 * Cette entité sert à lister les paramètres des utilisateurs gestionnaires.
 * 
 * @author poidij
 * 
 */
@Entity
@Table(name = "PARAM_UTILISATEUR_GESTIONNAIRE")
@IdClass(ParamUtilisateurGestionnaireId.class)
@Data
@EqualsAndHashCode(callSuper = false, of = { "ligneEnCoursImportBatch", "identifiantActiveDirectory" })
@ToString(callSuper = false, of = { "ligneEnCoursImportBatch", "identifiantActiveDirectory" })
public class ParamUtilisateurGestionnaire extends EntiteImportableBatch {
    /**
     * UID de version.
     */
    private static final long serialVersionUID = -6679275576681186483L;

    /**
     * Indicateur de ligne temporaire en cours d'import via Batch.
     * 
     * @param ligneEnCoursImportBatch
     *            le nom du SI d'où provient l'information
     * @return le nom du SI d'où provient l'information
     */
    @Id
    @Column(name = "TMP_BATCH")
    private boolean ligneEnCoursImportBatch;

    /**
     * Identifiant Active Directory
     * 
     * @param identifiantActiveDirectory
     *            L'identifiant Active Directory
     * @return L'identifiant Active Directory
     */
    @Id
    @Column(name = "CODE_USER")
    private String identifiantActiveDirectory;

    /**
     * Nom de l’utilisateur
     * 
     * @param identifiantActiveDirectory
     *            Le nom de l’utilisateur
     * @return Le nom de l’utilisateur
     */
    @Column(name = "NOM")
    private String nom;

    /**
     * Prénom de l’utilisateur
     * 
     * @param prenom
     *            Le prénom de l’utilisateur
     * @return Le prénom de l’utilisateur
     */
    @Column(name = "PRENOM")
    private String prenom;

    /**
     * Autorisation sur dossiers VIP
     * 
     * @param droitVIPAsText
     *            L'autorisation sur dossiers VIP
     * @return L'autorisation sur dossiers VIP
     */
    @Column(name = "DROIT_VIP")
    private String droitVIPAsText;

    /**
     * Autorisation sur données Individus
     * 
     * @param droitIndividuAsText
     *            L'autorisation sur données Individus
     * @return L'autorisation sur données Individus
     */
    @Column(name = "DROIT_INDIV")
    private String droitIndividuAsText;

    /**
     * Groupe de gestion de l’utilisateur
     * 
     * @param groupeGestion
     *            Le roupe de gestion de l’utilisateur
     * @return Le roupe de gestion de l’utilisateur
     */
    @Column(name = "GRP_GEST")
    private String groupeGestion;

    /**
     * Autoriser l’utilisateur à s'afficher ou non
     * 
     * @param AFFICHE_IHM
     * 
     * @return AFFICHE_IHM
     */
    @Column(name = "AFFICHE_IHM")
    private String afficheIhm;

    /**
     * Type du gestionnaire
     * 
     * @param Type
     *            de gestionnaire
     * 
     * @return Type
     */
    @Column(name = "TYPE")
    private String type;

    /**
     * Groupe de gestion egale
     * 
     * @param GG_EGAL
     * 
     * @return GG_EGAL
     */
    @Column(name = "GG_EGAL")
    private String ggEgal;

    /**
     * Groupe de gestion inclus
     * 
     * @param GG_INCLUS_1
     * 
     * @return GG_INCLUS_1
     */
    @Column(name = "GG_INCLUS_1")
    private String ggInclus1;

    /**
     * Groupe de gestion inclus
     * 
     * @param GG_INCLUS_2
     * 
     * @return GG_INCLUS_2
     */
    @Column(name = "GG_INCLUS_2")
    private String ggInclus2;

    /**
     * Groupe de gestion exclus
     * 
     * @param GG_EXCLUS
     * 
     * @return GG_EXCLUS
     */
    @Column(name = "GG_EXCLUS")
    private String ggExclus;

    /**
     * Niveau Groupe de gestion
     * 
     * @param NIVEAU_1
     * 
     * @return NIVEAU_1
     */
    @Column(name = "NIVEAU_1")
    private String niveau1;

    /**
     * Niveau Groupe de gestion
     * 
     * @param NIVEAU_2
     * 
     * @return NIVEAU_2
     */
    @Column(name = "NIVEAU_2")
    private String niveau2;

    /**
     * Niveau Groupe de gestion
     * 
     * @param NIVEAU_3
     * 
     * @return NIVEAU_3
     */
    @Column(name = "NIVEAU_3")
    private String niveau3;

    /**
     * Autorisation sur données Individus
     * 
     * @return L'autorisation sur données Individus
     */
    public boolean isDroitVIP() {
        return new ConvertisseurBooleanON().convertToEntityAttribute(getDroitVIPAsText());
    }

    /**
     * Autorisation sur données Individus.
     * 
     * @param droitVIP
     *            L'autorisation sur données Individus
     */
    public void setDroitVIP(boolean droitVIP) {
        setDroitVIPAsText(new ConvertisseurBooleanON().convertToDatabaseColumn(droitVIP));
    }

    /**
     * Autorisation sur données Individus.
     * 
     * @return L'autorisation sur données Individus
     */
    public boolean isDroitIndividu() {
        return new ConvertisseurBooleanON().convertToEntityAttribute(getDroitIndividuAsText());
    }

    /**
     * Autorisation sur données Individus.
     * 
     * @param droitIndividu
     *            L'autorisation sur données Individus
     */
    public void setDroitIndividu(boolean droitIndividu) {
        setDroitIndividuAsText(new ConvertisseurBooleanON().convertToDatabaseColumn(droitIndividu));
    }

    @Override
    public ParamUtilisateurGestionnaireId getId() {
        return new ParamUtilisateurGestionnaireId(ligneEnCoursImportBatch, identifiantActiveDirectory);
    }

}
