package fr.si2m.red.parametrage;

import fr.si2m.red.core.repository.EntiteImportableRepository;

/**
 * Référentiel des paramètres de gestion de périodes.
 * 
 * @author nortaina
 *
 */
public interface ParamGestionPeriodesRepository extends EntiteImportableRepository<ParamGestionPeriodes> {

}
