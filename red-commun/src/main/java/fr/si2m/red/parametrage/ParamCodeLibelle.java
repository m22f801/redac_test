package fr.si2m.red.parametrage;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

import fr.si2m.red.EntiteImportableBatch;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * Modèle de Paramètre CodeLibelle DSN, pouvant être représenté notamment à l'intérieur d'un fichier ParamCodeLibelle éligible DSN (au format CSV). <br/>
 * <br/>
 * Cette entité sert à mapper les différentes valeurs possibles d'un champ d'une table vers un libellé plus compréhensible.
 * 
 * @author poidij
 *
 */
@Entity
@Table(name = "PARAM_CODE_LIBELLE")
@IdClass(ParamCodeLibelleId.class)
@Data
@EqualsAndHashCode(callSuper = false, of = { "ligneEnCoursImportBatch", "table", "champ", "code" })
@ToString(callSuper = false, of = { "ligneEnCoursImportBatch", "table", "champ", "code" })
public class ParamCodeLibelle extends EntiteImportableBatch {
    /**
     * UID de version.
     */
    private static final long serialVersionUID = 8846408768775204715L;

    /**
     * Table pour libellés sur un contrat.
     */
    public static final String TABLE_CONTRAT = "CONTRATS";

    /**
     * Table pour libellés sur un contrat.
     */
    public static final String TABLE_ADHESION_ETAB_MOIS = "ADHESION_ETAB_MOIS";

    /**
     * Table pour libellés sur une période reçue.
     */
    public static final String TABLE_PERIODE_RECUE = "PERIODERECUE";

    /**
     * Table pour libellés sur les indicateurs DSN d'une période.
     */
    public static final String TABLE_INDICATEURS_DSN_PERIODE = "INDICATEURSDSNPERIODE";

    /**
     * Table pour libellés sur un versement.
     */
    public static final String TABLE_VERSEMENT = "VERSEMENT";

    /**
     * Table pour libellés sur un contrat.
     */
    public static final String TABLE_EXTENSION_CONTRAT = "EXTENSIONCONTRAT";

    /**
     * Table pour libellés sur un contrat.
     */
    public static final String TABLE_TARIF = "TARIFS";

    /**
     * Indicateur de ligne temporaire en cours d'import via Batch.
     * 
     * @param ligneEnCoursImportBatch
     *            le nom du SI d'où provient l'information
     * @return le nom du SI d'où provient l'information
     */
    @Id
    @Column(name = "TMP_BATCH")
    private boolean ligneEnCoursImportBatch;

    /**
     * Le nom de la table contenant le code à mapper vers un libellé.
     * 
     * @param table
     *            le nom de la table contenant le code
     * 
     * @return le nom de la table contenant le code
     * 
     */
    @Id
    @Column(name = "TBL")
    private String table;

    /**
     * Le nom du champ de la table contenant le code à mapper vers un libellé.
     * 
     * @param champ
     *            le nom du champ de la table contenant le code
     * 
     * @return le nom du champ de la table contenant le code
     * 
     */
    @Id
    @Column(name = "CHAMP")
    private String champ;

    /**
     * La valeur du code pour ce libellé.
     * 
     * @param code
     *            la valeur du code pour ce libellé
     * 
     * @return la valeur du code pour ce libellé
     * 
     */
    @Id
    @Column(name = "CODE")
    private String code;

    /**
     * L'ordre d'affichage de la valeur dans les listes de sélection.
     * 
     * @param ordre
     *            l'ordre d'affichage de la valeur dans les listes de sélection
     * 
     * @return l'ordre d'affichage de la valeur dans les listes de sélection
     * 
     */
    @Column(name = "ORDRE")
    private Long ordre;

    /**
     * Le libellé court ciblé par ce code.
     * 
     * @param libelleCourt
     *            le libellé court ciblé par ce code
     * @return le libellé court ciblé par ce code
     * 
     */
    @Column(name = "LIBELLE_COURT")
    private String libelleCourt;

    /**
     * Le libellé long ciblé par ce code.
     * 
     * @param libelleLong
     *            le libellé long ciblé par ce code
     * @return le libellé long ciblé par ce code
     * 
     */
    @Column(name = "LIBELLE_LONG")
    private String libelleLong;

    /**
     * Le libellé d'édition ciblé par ce code.
     * 
     * @param libelleEdition
     *            le libellé d'édition ciblé par ce code
     * @return le libellé d'édition ciblé par ce code
     * 
     */
    @Column(name = "LIBELLE_EDITION")
    private String libelleEdition;

    @Override
    public ParamCodeLibelleId getId() {
        return new ParamCodeLibelleId(ligneEnCoursImportBatch, table, champ, code);
    }

}
