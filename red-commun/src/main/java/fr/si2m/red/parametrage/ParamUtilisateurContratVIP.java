package fr.si2m.red.parametrage;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import fr.si2m.red.EntiteImportableBatch;

/**
 * Modèle de Paramètre utilisateur contrats vip DSN , pouvant être représenté notamment à l'intérieur d'un fichier ParamUtilisateurContratsVIP éligible DSN (au
 * format CSV).<br/>
 * <br/>
 * Cette entité sert à lister les contrats VIP sur lsequels un utilisateur gestionnaire a des droits.
 * 
 */
@Entity
@Table(name = "PARAM_UTILISATEUR_CONTRATS_VIP")
@IdClass(ParamUtilisateurContratVIPId.class)
@Data
@EqualsAndHashCode(callSuper = false, of = { "ligneEnCoursImportBatch", "codeUtilisateur", "numContrat" })
@ToString(callSuper = false, of = { "ligneEnCoursImportBatch", "codeUtilisateur", "numContrat" })
public class ParamUtilisateurContratVIP extends EntiteImportableBatch {

    /**
     * 
     */
    private static final long serialVersionUID = 6240379317051430186L;

    /**
     * Indicateur de ligne temporaire en cours d'import via Batch.
     * 
     * @param ligneEnCoursImportBatch
     *            le nom du SI d'où provient l'information
     * @return le nom du SI d'où provient l'information
     */
    @Id
    @Column(name = "TMP_BATCH")
    private boolean ligneEnCoursImportBatch;

    /**
     * Nom de la table (entité) contenant le code à présenter.
     * 
     * @param code
     *            utilisateur
     * @return codeUtilisateur
     */
    @Id
    @Column(name = "CODE_USER")
    private String codeUtilisateur;

    /**
     * Numéro de contrat VIP autorisé à cet utilisateur.
     * 
     * @param numContrat
     *            numero de contrat VIP
     * 
     * @return numContrat numero de contrat VIP
     */
    @Id
    @Column(name = "NOCO")
    private String numContrat;

    @Override
    public ParamUtilisateurContratVIPId getId() {
        return new ParamUtilisateurContratVIPId(ligneEnCoursImportBatch, codeUtilisateur, numContrat);
    }

}
