package fr.si2m.red.parametrage;

import java.util.List;

import fr.si2m.red.core.repository.EntiteImportableRepository;

/**
 * Référentiel des paramètres des utilisateurs gestionnaires.
 * 
 * @author poidij,eudesr
 *
 */
public interface ParamUtilisateurGestionnaireRepository extends EntiteImportableRepository<ParamUtilisateurGestionnaire> {

    /**
     * Récupère un utilisateur gestionnaire via son identifiant AD.
     * 
     * @param identifiantActiveDirectory
     *            l'identifiant AD
     * @return l'utilisateur gestionnaire correspondant à l'identifiant AD
     */
    ParamUtilisateurGestionnaire getGestionnaire(String identifiantActiveDirectory);

    /**
     * Retourne le nom et le prenom de l'utilisateur
     * 
     * @param codeUser
     *            l'identifiant AD de l'utilisateur
     * 
     * @return Retourne le nom et le prenom de l'utilisateur format "NOM PRENOM"
     */
    String getNomEtPrenomAvecCodeUser(String codeUser);

    /**
     * Récupère la liste des user resumé : Codeuser,Nom,Prénom
     *
     * @return la liste des utilisateurs ( resumé : Codeuser,Nom,Prénom )
     */
    List<ResumeParamUtilisateurGestionnaire> getListeNomPrenomIdActiveDirectory();

    /**
     * Récupère la liste des utilisateur format "NOM PRENOM - CODEUSER" , avec choix du premier item de la liste
     * 
     * @param premiereValeurListelibelle
     *            Nom associé au premier élément de la liste
     * @param premiereValeurListeCode
     *            CodeUser associé au premier élément de la liste
     * @return la liste user, format "NOM PRENOM - CODEUSER"
     */
    List<ResumeParamUtilisateurGestionnaire> getListeNomsEtCodeUsers(String premiereValeurListelibelle, String premiereValeurListeCode);

    /**
     * Récupère la liste des utilisateur format "NOM PRENOM - CODEUSER" , avec exclusion de l'item "Autre Gestionnaire"
     * 
     * @param premiereValeurListelibelle
     *            Nom associé au premier élément de la liste
     * @param premiereValeurListeCode
     *            CodeUser associé au premier élément de la liste
     * @return la liste user, format "NOM PRENOM - CODEUSER"
     */
    List<ResumeParamUtilisateurGestionnaire> getlistesAffecteAEtAtraiterParSansAutreGest(String premiereValeurListelibelle,
            String premiereValeurListeCode);

    /**
     * Récupère la liste des utilisateur format "NOM PRENOM - CODEUSER"
     * 
     * @return la liste user, format "NOM PRENOM - CODEUSER"
     */
    List<ResumeParamUtilisateurGestionnaire> getListeNomPrenomIdActiveDirectoryPourOngletChangementStatut();
}
