package fr.si2m.red.parametrage;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * Résumé d'un gestionnaire
 * 
 * @author eudesr
 *
 */
@Data
@AllArgsConstructor
public class ResumeParamUtilisateurGestionnaire {

    /**
     * Identifiant Active Directory
     * 
     * @param identifiantActiveDirectory
     *            L'identifiant Active Directory
     * @return L'identifiant Active Directory
     */
    private String identifiantActiveDirectory;

    /**
     * Nom de l’utilisateur
     * 
     * @param identifiantActiveDirectory
     *            Le nom de l’utilisateur
     * @return Le nom de l’utilisateur
     */
    private String nom;

    /**
     * Prénom de l’utilisateur
     * 
     * @param prenom
     *            Le prénom de l’utilisateur
     * @return Le prénom de l’utilisateur
     */
    private String prenom;

    /**
     * Constructeur identifiant + nom
     * 
     * @param identifiantActiveDirectory
     *            identifiant du gestionnaire
     * @param nom
     *            nom du gestionnaire
     */
    public ResumeParamUtilisateurGestionnaire(String identifiantActiveDirectory, String nom) {
        super();
        this.identifiantActiveDirectory = identifiantActiveDirectory;
        this.nom = nom;
    }

    /**
     * Constructeur avec identifiant
     * 
     * @param identifiantActiveDirectory
     *            l'identifiant du gestionnaire
     */
    public ResumeParamUtilisateurGestionnaire(String identifiantActiveDirectory) {
        super();
        this.identifiantActiveDirectory = identifiantActiveDirectory;
    }

    /**
     * Constructeur par défaut
     */
    public ResumeParamUtilisateurGestionnaire() {
        super();
    }

}
