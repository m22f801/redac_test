package fr.si2m.red.parametrage;

import java.util.List;

import fr.si2m.red.core.repository.EntiteImportableRepository;

/**
 * Référentiel des paramétrages de nature de base de composants.
 * 
 * @author nortaina
 *
 */
public interface ParamNatureBaseComposantsRepository extends EntiteImportableRepository<ParamNatureBaseComposants> {

    /**
     * Recherche les composants natures base ayant les paramètres indiqués.
     * 
     * @param natureBaseCotisations
     *            le code de cotisation du composant de la nature Base
     * @param tauxCalculRempli
     *            le taux de calcul rempli
     * @return les composants natures base ayant les paramètres indiqués
     */
    List<ParamNatureBaseComposants> getParamNatureBaseComposants(Integer natureBaseCotisations, String tauxCalculRempli);

    /**
     * Recherche les composants natures base ayant les paramètres indiqués.
     * 
     * @param natureBaseCotisations
     *            le code de cotisation du composant de la nature Base
     * @param numTranche
     *            le numéro de la tranche
     * @param tauxCalculRempli
     *            le taux de calcul rempli
     * @return les composants natures base ayant les paramètres indiqués
     */
    List<ParamNatureBaseComposants> getParamNatureBaseComposants(Integer natureBaseCotisations, Integer numTranche, String tauxCalculRempli);

    /**
     * Recherche les types de composants de bases pour le mode de consolidation donné.
     * 
     * @param natureBaseCotisations
     *            le code de cotisation du composant de la nature Base
     * @param numTranche
     *            le numéro de la tranche
     * @param tauxCalculRempli
     *            le taux de calcul rempli
     * @param modeConsolidation
     *            le mode de consolidation
     * @return les composants natures base ayant les paramètres indiqués
     */
    List<Integer> getTypesComposantsBasesPourModeConsolidation(Integer natureBaseCotisations, Integer numTranche, String tauxCalculRempli, String modeConsolidation);
}
