package fr.si2m.red.parametrage;

import java.util.List;

import fr.si2m.red.core.repository.EntiteImportableRepository;

/**
 * Référentiel des paramètres de nature de base.
 * 
 * @author nortaina
 *
 */
public interface ParamNatureBaseRepository extends EntiteImportableRepository<ParamNatureBase> {

    /**
     * Teste si une natureBaseCotisations existe.
     * 
     * @param natureBaseCotisations
     *            la cotisation de la nature base à tester
     * @return true si la cotisation de la nature base existe, false sinon
     */
    boolean existeUnParametragePourNatureBaseCotisations(Integer natureBaseCotisations);

    /**
     * Recherche une nature de base par clé fonctionnelle.
     * 
     * @param natureBaseCotisations
     *            le code de cotisation de la nature Base
     * @param tauxCalculRempli
     *            le taux de calcul rempli
     * @param numTranche
     *            le numéro de tranche
     * @return la nature de base si elle existe, null sinon
     */
    ParamNatureBase getParamNatureBase(Integer natureBaseCotisations, String tauxCalculRempli, Integer numTranche);

    /**
     * Recupère les nature de base pour une nature de base de cotisations donnée.
     * 
     * @param codeNatureBaseCotisations
     *            la nature de base de cotisations
     * @param tauxCalculRempli
     *            indication sur le taux de calcul rempli
     * @return les nature de base pour la nature de base de cotisations donnée
     */
    List<ParamNatureBase> getPourNatureBaseCotisationsEtTauxCalculRempli(Integer codeNatureBaseCotisations, boolean tauxCalculRempli);

    /**
     * Récupère les tranches distinctes pour une nature de base de cotisations donnée.
     * 
     * @param codeNatureBaseCotisations
     *            le code de nature de base
     * @return les tranches distinctes pour la nature de base de cotisations donnée
     */
    List<Tranche> getTranchesDistinctesPourNatureBaseCotisations(Integer codeNatureBaseCotisations);

}
