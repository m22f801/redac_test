package fr.si2m.red.parametrage;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * Identifiant du modèle de Paramètre SirenFaux DSN, pouvant être représenté notamment à l'intérieur d'un fichier ParamSirenFaux éligible DSN (au format
 * CSV).
 * 
 * @author eudesr
 * 
 * @see ParamSirenFaux
 */
@Data
@EqualsAndHashCode(of = { "ligneEnCoursImportBatch", "siren" })
@ToString(of = { "ligneEnCoursImportBatch","siren" })
@NoArgsConstructor
@AllArgsConstructor
public class ParamSirenFauxId implements Serializable {
    /**
     * UID de version.
     */
    private static final long serialVersionUID = 3936349721258202798L;

    /**
     * Indicateur de ligne temporaire en cours d'import via Batch.
     * 
     * @param ligneEnCoursImportBatch
     *            le nom du SI d'où provient l'information
     * @return le nom du SI d'où provient l'information
     */
    private boolean ligneEnCoursImportBatch;

    /**
     * La valeur du siren pour ce libellé.
     * 
     * @param siren
     *            la valeur du siren pour ce libellé
     * 
     * @return la valeur du siren pour ce libellé
     * 
     */
    private String siren;

}
