package fr.si2m.red.parametrage;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

import fr.si2m.red.EntiteImportableBatch;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * Modèle de Paramètre GestionPeriodes DSN, pouvant être représenté notamment à l'intérieur d'un fichier ParamGestionPeriodes éligible DSN (au format CSV).
 * <br/>
 * 
 * @author nortaina
 *
 */
@Entity
@Table(name = "PARAM_GESTION_PERIODES")
@IdClass(ParamGestionPeriodesId.class)
@Data
@EqualsAndHashCode(callSuper = false, of = { "ligneEnCoursImportBatch", "delaiPublicationFinDeclaration5", "delaiPublicationFinDeclaration15",
        "delaiPublicationCreationDeclaration", "delaiPublicationCreationComplement", "delaiPublicationCreationRegulation" })
@ToString(callSuper = false, of = { "ligneEnCoursImportBatch", "delaiPublicationFinDeclaration5", "delaiPublicationFinDeclaration15",
        "delaiPublicationCreationDeclaration", "delaiPublicationCreationComplement", "delaiPublicationCreationRegulation" })
public class ParamGestionPeriodes extends EntiteImportableBatch {
    /**
     * UID de version.
     */
    private static final long serialVersionUID = 4677525531120958375L;

    /**
     * Indicateur de ligne temporaire en cours d'import via Batch.
     * 
     * @param ligneEnCoursImportBatch
     *            le nom du SI d'où provient l'information
     * @return le nom du SI d'où provient l'information
     */
    @Id
    @Column(name = "TMP_BATCH")
    private boolean ligneEnCoursImportBatch;

    /**
     * Le délai en jour à partir duquel publier les périodes de déclaration dont la date d’échéance est au 5 du mois. Ce délai est ajouté à la date de fin des
     * périodes de déclaration dont la date d’échéance est le 5.
     * 
     * @param delaiPublicationFinDeclaration5
     *            le délai en jour à partir duquel publier les périodes de déclaration dont la date d’échéance est au 5 du mois
     * @return le délai en jour à partir duquel publier les périodes de déclaration dont la date d’échéance est au 5 du mois
     */
    @Id
    @Column(name = "DELAI_PUBLI_FIN_DECL5")
    private Integer delaiPublicationFinDeclaration5;

    /**
     * Le délai en jour à partir duquel publier les périodes de déclaration dont la date d’échéance est passée. Ce délai est ajouté à la date de fin des
     * périodes de déclaration dont la date d’échéance est le 15.
     * 
     * @param delaiPublicationFinDeclaration15
     *            le délai en jour à partir duquel publier les périodes de déclaration dont la date d’échéance est passée
     * @return le délai en jour à partir duquel publier les périodes de déclaration dont la date d’échéance est passée
     */
    @Id
    @Column(name = "DELAI_PUBLI_FIN_DECL15")
    private Integer delaiPublicationFinDeclaration15;

    /**
     * Le délai en jours entre la réception de la première déclaration d’une période de déclarations et l’envoi de la période au SI Quatrem.
     * 
     * @param delaiPublicationCreationDeclaration
     *            le délai en jours entre la réception de la première déclaration d’une période de déclarations et l’envoi de la période au SI Quatrem
     * @return le délai en jours entre la réception de la première déclaration d’une période de déclarations et l’envoi de la période au SI Quatrem
     */
    @Id
    @Column(name = "DELAI_PUBLI_CREA_DECL")
    private Integer delaiPublicationCreationDeclaration;

    /**
     * Le délai en jours entre la réception de la première déclaration d’une période de complément et l’envoi de la période au SI Quatrem.
     * 
     * @param delaiPublicationCreationComplement
     *            le délai en jours entre la réception de la première déclaration d’une période de complément et l’envoi de la période au SI Quatrem
     * @return le délai en jours entre la réception de la première déclaration d’une période de complément et l’envoi de la période au SI Quatrem
     */
    @Id
    @Column(name = "DELAI_PUBLI_CREA_COMPL")
    private Integer delaiPublicationCreationComplement;

    /**
     * Le délai en jours entre la réception de la première déclaration d’une période de régularisation et l’envoi de la période au SI Quatrem.
     * 
     * @param delaiPublicationCreationRegulation
     *            le délai en jours entre la réception de la première déclaration d’une période de régularisation et l’envoi de la période au SI Quatrem
     * @return le délai en jours entre la réception de la première déclaration d’une période de régularisation et l’envoi de la période au SI Quatrem
     */
    @Id
    @Column(name = "DELAI_PUBLI_CREA_REGUL")
    private Integer delaiPublicationCreationRegulation;

    @Override
    public ParamGestionPeriodesId getId() {
        return new ParamGestionPeriodesId(ligneEnCoursImportBatch, delaiPublicationFinDeclaration5, delaiPublicationFinDeclaration15,
                delaiPublicationCreationDeclaration, delaiPublicationCreationComplement, delaiPublicationCreationRegulation);
    }
}
