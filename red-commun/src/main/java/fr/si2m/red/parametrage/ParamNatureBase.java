package fr.si2m.red.parametrage;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import fr.si2m.red.EntiteImportableBatch;
import fr.si2m.red.core.repository.jpa.conversion.ConvertisseurBooleanON;

/**
 * Modèle de Paramètre NatureBase DSN, pouvant être représenté notamment à l'intérieur d'un fichier ParamNatureBase éligible DSN (au format CSV).
 * 
 * @author nortaina
 * 
 */
@Entity
@Table(name = "PARAM_NATURE_BASE")
@IdClass(ParamNatureBaseId.class)
@Data
@EqualsAndHashCode(callSuper = false, of = { "ligneEnCoursImportBatch", "codeNatureBaseCotisations", "tauxCalculRempliAsText", "numTranche" })
@ToString(callSuper = false, of = { "ligneEnCoursImportBatch", "codeNatureBaseCotisations", "tauxCalculRempliAsText", "numTranche" })
public class ParamNatureBase extends EntiteImportableBatch {
    /**
     * UID de version.
     */
    private static final long serialVersionUID = -3526576482611855974L;

    /**
     * Indicateur de ligne temporaire en cours d'import via Batch.
     * 
     * @param ligneEnCoursImportBatch
     *            le nom du SI d'où provient l'information
     * @return le nom du SI d'où provient l'information
     */
    @Id
    @Column(name = "TMP_BATCH", nullable = false)
    private boolean ligneEnCoursImportBatch;

    /**
     * Le code de nature de base des cotisations que la tarification du contrat doit avoir pour appliquer ce comportement.
     * 
     * @param codeNatureBaseCotisations
     *            le code de nature de base des cotisations que la tarification du contrat doit avoir pour appliquer ce comportement
     * @return le code de nature de base des cotisations que la tarification du contrat doit avoir pour appliquer ce comportement
     */
    @Id
    @Column(name = "CONBCOT", nullable = false)
    private Integer codeNatureBaseCotisations;

    /**
     * Indique si la tarification du contrat doit avoir un taux de calcul valorisé ou pas pour appliquer ce comportement.
     * 
     * @param tauxCalculRempliAsText
     *            'O' si la tarification du contrat doit avoir un taux de calcul valorisé ou pas pour appliquer ce comportement, 'N' sinon
     * @return 'O' si la tarification du contrat doit avoir un taux de calcul valorisé ou pas pour appliquer ce comportement, 'N' sinon
     */
    @Id
    @Column(name = "TXCALCU_REMPLI", nullable = false)
    private String tauxCalculRempliAsText;

    /**
     * Le numéro de tranche à transmettre à WQUI/GERD pour alimenter la tranche correspondante. Il est compris entre 1 et 4.
     * 
     * @param numTranche
     *            le numéro de tranche à transmettre à WQUI/GERD pour alimenter la tranche correspondante
     * @return le numéro de tranche à transmettre à WQUI/GERD pour alimenter la tranche correspondante
     */
    @Id
    @Column(name = "NUM_TRANCHE", nullable = false)
    private Integer numTranche;

    /**
     * Le libellé de la tranche à transmettre à WQUI/GERD pour l'alimenter.
     * 
     * @param libelleTranche
     *            le libellé de la tranche à transmettre à WQUI/GERD pour l'alimenter
     * @return le libellé de la tranche à transmettre à WQUI/GERD pour l'alimenter
     */
    @Column(name = "LIB_TRANCHE")
    private String libelleTranche;

    /**
     * Le nom du champ contenant le taux à présenter dans la fiche de paramétrage.
     * 
     * @param tauxUtilise
     *            le nom du champ contenant le taux à présenter dans la fiche de paramétrage
     * @return le nom du champ contenant le taux à présenter dans la fiche de paramétrage
     */
    @Column(name = "TAUX_UTILISE")
    private String tauxUtilise;

    /**
     * Le nom du champ contenant le montant à présenter dans la fiche de paramétrage.
     * 
     * @param montantUtilise
     *            le nom du champ contenant le montant à présenter dans la fiche de paramétrage
     * @return le nom du champ contenant le montant à présenter dans la fiche de paramétrage
     */
    @Column(name = "MT_UTILISE")
    private String montantUtilise;

    /**
     * Le complément de désignation dans la fiche de paramétrage.
     * 
     * @param complementDesignation
     *            le complément de désignation dans la fiche de paramétrage
     * @return le complément de désignation dans la fiche de paramétrage
     */
    @Column(name = "CPLT_DESIGN")
    private String complementDesignation;

    /**
     * Indique si la tarification du contrat doit avoir un taux de calcul valorisé ou pas pour appliquer ce comportement.
     * 
     * @return true si la tarification du contrat doit avoir un taux de calcul valorisé ou pas pour appliquer ce comportement
     */
    public boolean isTauxCalculRempli() {
        return new ConvertisseurBooleanON().convertToEntityAttribute(getTauxCalculRempliAsText());
    }

    /**
     * Indique si la tarification du contrat doit avoir un taux de calcul valorisé ou pas pour appliquer ce comportement.
     * 
     * @param tauxCalculRempli
     *            true si la tarification du contrat doit avoir un taux de calcul valorisé ou pas pour appliquer ce comportement
     */
    public void setTauxCalculRempli(boolean tauxCalculRempli) {
        setTauxCalculRempliAsText(new ConvertisseurBooleanON().convertToDatabaseColumn(tauxCalculRempli));
    }

    @Override
    public ParamNatureBaseId getId() {
        return new ParamNatureBaseId(ligneEnCoursImportBatch, codeNatureBaseCotisations, tauxCalculRempliAsText, numTranche);
    }

}
