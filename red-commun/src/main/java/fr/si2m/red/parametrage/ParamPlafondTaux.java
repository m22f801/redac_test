package fr.si2m.red.parametrage;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import fr.si2m.red.EntiteImportableBatch;

/**
 * Modèle de Paramètre PlafondsTaux DSN, pouvant être représenté notamment à l'intérieur d'un fichier ParamPlafondsTaux éligible DSN (au format CSV).<br/>
 * <br/>
 * Cette entité sert à lister les plafonds des taux.
 * 
 * @author nortaina
 * 
 */
@Entity
@Table(name = "PARAM_PLAFONDS_TAUX")
@IdClass(ParamPlafondTauxId.class)
@Data
@EqualsAndHashCode(callSuper = false, of = { "ligneEnCoursImportBatch", "debut" })
@ToString(callSuper = false, of = { "ligneEnCoursImportBatch", "debut" })
public class ParamPlafondTaux extends EntiteImportableBatch {
    /**
     * UID de version.
     */
    private static final long serialVersionUID = -6679275576681186483L;

    /**
     * Indicateur de ligne temporaire en cours d'import via Batch.
     * 
     * @param ligneEnCoursImportBatch
     *            le nom du SI d'où provient l'information
     * @return le nom du SI d'où provient l'information
     */
    @Id
    @Column(name = "TMP_BATCH")
    private boolean ligneEnCoursImportBatch;

    /**
     * La date de début de validité des taux et montants de cette ligne.
     * 
     * @param debut
     *            la date de début de validité des taux et montants de cette ligne
     * @return la date de début de validité des taux et montants de cette ligne
     */
    @Id
    @Column(name = "DEBUT")
    private Integer debut;

    /**
     * La date de fin de validité des taux et montants de cette ligne.
     * 
     * @param fin
     *            la date de fin de validité des taux et montants de cette ligne
     * @return la date de fin de validité des taux et montants de cette ligne
     */
    @Column(name = "FIN")
    private Integer fin;

    /**
     * Le plafond mensuel de sécurité sociale.
     * 
     * @param plafondMensuelSecuriteSociale
     *            le plafond mensuel de sécurité sociale
     * @return le plafond mensuel de sécurité sociale
     */
    @Column(name = "PMSS")
    private Double plafondMensuelSecuriteSociale;

    @Override
    public ParamPlafondTauxId getId() {
        return new ParamPlafondTauxId(ligneEnCoursImportBatch, debut);
    }

}
