package fr.si2m.red.parametrage;

import java.util.List;
import java.util.Map;

import fr.si2m.red.core.repository.EntiteImportableRepository;

/**
 * Référentiel des Paramètres CodeLibellé.
 * 
 * @author poidij
 *
 */
public interface ParamCodeLibelleRepository extends EntiteImportableRepository<ParamCodeLibelle> {

    /**
     * Récupère le libellé court pour une table, un champ et un code particuliers.
     * 
     * @param table
     *            le nom de table
     * @param champ
     *            le nom de champ
     * @param code
     *            le nom du code
     * @return le libellé court pour les paramètres donnés
     */
    String getLibelleCourt(String table, String champ, String code);

    /**
     * Récupère le libellé d'édition pour une table, un champ et un code particuliers.
     * 
     * @param table
     *            le nom de table
     * @param champ
     *            le nom de champ
     * @param code
     *            le nom du code
     * @return le libellé d'édition pour les paramètres donnés
     */
    String getLibelleEdition(String table, String champ, String code);

    /**
     * Récupère la liste des libellés disponibles pour une table et un champ particulier.
     * 
     * @param table
     *            le nom de table
     * @param champ
     *            le nom de champ
     * @return la liste des libellés pour les paramètres donnés
     */
    List<ParamCodeLibelle> get(String table, String champ);

    /**
     * Récupère la liste des libellés edition pour une table et un champ particulier, sous la forme d'une map <code,libelle édition>
     * 
     * @param table
     *            le nom de table
     * @param champ
     *            le nom de champ
     * @return une map <code,libelle édition>
     */
    public Map<String, String> getMap(String table, String champ);

}
