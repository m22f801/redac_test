package fr.si2m.red.parametrage;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * Identifiant des paramètres des utilisateurs gestionnaires.
 * 
 * @author poidij
 * 
 */
@Data
@EqualsAndHashCode(callSuper = false, of = { "ligneEnCoursImportBatch", "identifiantActiveDirectory" })
@ToString(callSuper = false, of = { "ligneEnCoursImportBatch", "identifiantActiveDirectory" })
@NoArgsConstructor
@AllArgsConstructor
public class ParamUtilisateurGestionnaireId implements Serializable {
    /**
     * UID de version.
     */
    private static final long serialVersionUID = -6679275576681186483L;

    /**
     * Indicateur de ligne temporaire en cours d'import via Batch.
     * 
     * @param ligneEnCoursImportBatch
     *            le nom du SI d'où provient l'information
     * @return le nom du SI d'où provient l'information
     */
    private boolean ligneEnCoursImportBatch;

    /**
     * Identifiant Active Directory
     * 
     * @param identifiantActiveDirectory
     *            L'identifiant Active Directory
     * @return L'identifiant Active Directory
     */
    private String identifiantActiveDirectory;

}
