package fr.si2m.red.parametrage;

import java.util.List;
import java.util.Map;

import fr.si2m.red.core.repository.EntiteImportableRepository;

/**
 * Référentiel des contrôles de signaux de rejets.
 * 
 * @author nortaina
 *
 */
public interface ParamControleSignalRejetRepository extends EntiteImportableRepository<ParamControleSignalRejet> {

    /**
     * Récupère la totalité des contrôles globaux (NOFAM='ALL').
     * 
     * @return les contrôles globaux
     */
    Map<String, ParamControleSignalRejet> getControlesGlobaux();

    /**
     * Récupère la totalité des contrôles par identifiantControle Distinct
     * 
     * @return les contrôles ordonees par msg alerte
     */

    List<ParamControleSignalRejet> getControlesOrdoneesDistinct();

    /**
     * Récupère le ParamSignalRejet associé au contrôle et nofam passé en paramètre
     * 
     * @param controle
     *            l'identifiant de controle passé en paramètre
     * @param nofam
     *            le nofam passé en paramètre
     * @return
     */
    ParamControleSignalRejet getParamControleSignalRejetPourControleEtNofam(String controle, String nofam);

    /**
     * Récupère le ParamSignalRejet associé au idControle passé en paramètre
     * 
     * @param idControle
     *            idControle passé en paramètre
     * @return le controle
     */
    ParamControleSignalRejet getControlePourIdControle(String idControle);

}
