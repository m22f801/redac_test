package fr.si2m.red.parametrage;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import fr.si2m.red.EntiteImportableBatch;

/**
 * Modèle de Paramètre GroupeGestion DSN, pouvant être représenté notamment à l'intérieur d'un fichier ParamGroupeGestion éligible DSN (au format CSV).<br/>
 * <br/>
 * Cette entité sert à référencer un groupe de gestion.
 * 
 * @author nortaina
 * 
 */
@Entity
@Table(name = "PARAM_GROUPE_GESTION")
@IdClass(ParamGroupeGestionId.class)
@Data
@EqualsAndHashCode(callSuper = false, of = { "ligneEnCoursImportBatch", "numGroupeGestion" })
@ToString(callSuper = false, of = { "ligneEnCoursImportBatch", "numGroupeGestion" })
public class ParamGroupeGestion extends EntiteImportableBatch {
    /**
     * UID de version.
     */
    private static final long serialVersionUID = 7366939100336224988L;

    /**
     * Indicateur de ligne temporaire en cours d'import via Batch.
     * 
     * @param ligneEnCoursImportBatch
     *            le nom du SI d'où provient l'information
     * @return le nom du SI d'où provient l'information
     */
    @Id
    @Column(name = "TMP_BATCH")
    private boolean ligneEnCoursImportBatch;

    /**
     * Le numéro du groupe de gestion.
     * 
     * @param numGroupeGestion
     *            le numéro du groupe de gestion
     * @return le numéro du groupe de gestion
     */
    @Id
    @Column(name = "NMGRPGES")
    private String numGroupeGestion;

    /**
     * L'adresse mail pour les alertes du groupe.
     * 
     * @param mailAlerte
     *            l'adresse mail pour les alertes du groupe
     * @return l'adresse mail pour les alertes du groupe
     */
    @Column(name = "MAIL_ALERTE")
    private String mailAlerte;

    @Override
    public ParamGroupeGestionId getId() {
        return new ParamGroupeGestionId(ligneEnCoursImportBatch, numGroupeGestion);
    }
}
