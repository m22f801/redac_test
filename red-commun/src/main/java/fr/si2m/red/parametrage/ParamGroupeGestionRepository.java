package fr.si2m.red.parametrage;

import fr.si2m.red.core.repository.EntiteImportableRepository;

/**
 * Référentiel des valeurs par défaut.
 * 
 * @author nortaina
 *
 */
public interface ParamGroupeGestionRepository extends EntiteImportableRepository<ParamGroupeGestion> {

}
