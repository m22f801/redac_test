package fr.si2m.red.parametrage;

import fr.si2m.red.core.repository.EntiteImportableRepository;

/**
 * Référentiel des paramétrages de siren faux
 * 
 * @author eudesr
 *
 */
public interface ParamSirenFauxRepository extends EntiteImportableRepository<ParamSirenFaux> {

    /**
     * Vérifie l'existence d'un siren dans la table PARAM_SIREN_FAUX
     * 
     * @param siren
     *            le siren à tester
     * @return true si le siren existe
     */
    boolean existeSiren(String siren);

}
