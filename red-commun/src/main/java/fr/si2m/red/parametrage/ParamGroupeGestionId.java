package fr.si2m.red.parametrage;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * Identifiant d'un groupe de gestion.
 * 
 * @author nortaina
 * 
 */
@Data
@EqualsAndHashCode(callSuper = false, of = { "ligneEnCoursImportBatch", "numGroupeGestion" })
@ToString(callSuper = false, of = { "ligneEnCoursImportBatch", "numGroupeGestion" })
@NoArgsConstructor
@AllArgsConstructor
public class ParamGroupeGestionId implements Serializable {
    /**
     * UID de version.
     */
    private static final long serialVersionUID = 8923696340182621676L;

    /**
     * Indicateur de ligne temporaire en cours d'import via Batch.
     * 
     * @param ligneEnCoursImportBatch
     *            le nom du SI d'où provient l'information
     * @return le nom du SI d'où provient l'information
     */
    private boolean ligneEnCoursImportBatch;

    /**
     * Le numéro du groupe de gestion.
     * 
     * @param numGroupeGestion
     *            le numéro du groupe de gestion
     * @return le numéro du groupe de gestion
     */
    private String numGroupeGestion;
}
