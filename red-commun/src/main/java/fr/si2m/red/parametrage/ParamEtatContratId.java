package fr.si2m.red.parametrage;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * Modèle de Paramètre EtatContrat DSN, pouvant être représenté notamment à l'intérieur d'un fichier ParamEtatContrat éligible DSN (au format CSV). <br/>
 * <br/>
 * Cette entité sert à référencer les états de contrats compréhensibles dans REDAC et s'ils peuvent être considérés comme actifs.
 * 
 * @author nortaina
 *
 */
@Data
@EqualsAndHashCode(of = { "ligneEnCoursImportBatch", "etatContrat" })
@ToString(of = { "ligneEnCoursImportBatch", "etatContrat" })
@NoArgsConstructor
@AllArgsConstructor
public class ParamEtatContratId implements Serializable {
    /**
     * UID de version.
     */
    private static final long serialVersionUID = -889036425674255073L;

    /**
     * Indicateur de ligne temporaire en cours d'import via Batch.
     * 
     * @param ligneEnCoursImportBatch
     *            le nom du SI d'où provient l'information
     * @return le nom du SI d'où provient l'information
     */
    private boolean ligneEnCoursImportBatch;

    /**
     * Le code d'état de contrat à considérer comme actif ou non.
     * 
     * @param etatContrat
     *            le code d'état de contrat à considérer comme actif ou non
     * @return le code d'état de contrat à considérer comme actif ou non
     */
    private Integer etatContrat;
}
