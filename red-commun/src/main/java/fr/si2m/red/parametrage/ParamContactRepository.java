package fr.si2m.red.parametrage;

import fr.si2m.red.core.repository.EntiteImportableRepository;

/**
 * Référentiel des contacts.
 * 
 * @author nortaina
 *
 */
public interface ParamContactRepository extends EntiteImportableRepository<ParamContact> {

}
