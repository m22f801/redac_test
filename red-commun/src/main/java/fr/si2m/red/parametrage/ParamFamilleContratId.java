package fr.si2m.red.parametrage;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * Identifiant d'une famille de contrat.
 * 
 * @author nortaina
 *
 */
@Data
@EqualsAndHashCode(of = { "numFamille", "ligneEnCoursImportBatch", "groupeGestion" })
@ToString(of = { "numFamille", "ligneEnCoursImportBatch", "groupeGestion" })
@NoArgsConstructor
@AllArgsConstructor
public class ParamFamilleContratId implements Serializable {
    /**
     * UID de version.
     */
    private static final long serialVersionUID = 1278645097338215142L;

    /**
     * Indicateur de ligne temporaire en cours d'import via Batch.
     * 
     * @param ligneEnCoursImportBatch
     *            le nom du SI d'où provient l'information
     * @return le nom du SI d'où provient l'information
     */
    private boolean ligneEnCoursImportBatch;

    /**
     * Le numéro de famille du contrat.
     * 
     * @param numFamille
     *            le numéro de famille du contrat
     * @return le numéro de famille du contrat
     */
    private Integer numFamille;

    private String groupeGestion;

}
