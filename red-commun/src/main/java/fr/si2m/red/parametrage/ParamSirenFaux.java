package fr.si2m.red.parametrage;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;
import fr.si2m.red.EntiteImportableBatch;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * Modèle de Paramètre SirenFaux DSN, pouvant être représenté notamment à l'intérieur d'un fichier ParamSirenFaux  (au format CSV). <br/>
 * <br/>
 * Cette entité sert à mapper les différentes valeurs possibles d'un champ d'une table vers un libellé plus compréhensible.
 * 
 * @author eudesr
 *
 */
@Entity
@Table(name = "PARAM_SIREN_FAUX")
@IdClass(ParamSirenFauxId.class)
@Data
@EqualsAndHashCode(callSuper = false, of = { "ligneEnCoursImportBatch", "siren" })
@ToString(callSuper = false, of = { "ligneEnCoursImportBatch", "siren" })
public class ParamSirenFaux extends EntiteImportableBatch {
    /**
     * UID de version.
     */
    private static final long serialVersionUID = 8846408768775204715L;

    /**
     * Indicateur de ligne temporaire en cours d'import via Batch.
     * 
     * @param ligneEnCoursImportBatch
     *            le nom du SI d'où provient l'information
     * @return le nom du SI d'où provient l'information
     */
    @Id
    @Column(name = "TMP_BATCH")
    private boolean ligneEnCoursImportBatch;

    /**
     * La valeur du siren faux
     * 
     * @param code
     *            la valeur du siren faux
     * 
     * @return la valeur du siren faux
     * 
     */
    @Id
    @Column(name = "SIREN")
    private String siren;

    @Override
    public ParamSirenFauxId getId() {
        return new ParamSirenFauxId(ligneEnCoursImportBatch, siren);
    }

}
