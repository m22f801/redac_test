package fr.si2m.red.parametrage;

import java.util.List;

import fr.si2m.red.core.repository.EntiteImportableRepository;

/**
 * Référentiel des familles de contrat.
 * 
 * @author nortaina
 *
 */
public interface ParamFamilleContratRepository extends EntiteImportableRepository<ParamFamilleContrat> {

    /**
     * Récupère la désignation d'une famille de contrat.
     * 
     * @param numFamille
     *            le numéro de famille du contrat
     * 
     * @return la désignation de la famille de contrat si elle existe, null sinon
     */
    String getDesignationFamille(Integer numFamille, String groupeGestion);

    /**
     * Retourne le code de l'application en charge d'une famille de produit.
     * 
     * @param numFamille
     *            le numéro de famille
     * 
     * @return applicationAvalEnCharge - application en charge de cette famille dans le SI Quatrem
     */
    String getApplicationSiAval(Integer numFamille, String groupeGestion);

    /**
     * Récupère les numéros de familles correspondant à une liste de types de désignations.
     * 
     * @param typesDesignations
     *            les types de désignations pour la recherche
     * @return les numéros de familles correspondants
     */
    List<Integer> getNumFamillesPourTypesDeDesignation(List<String> typesDesignations);

    /**
     * Retourne le portefeuille en charge d'une famille de produit.
     * 
     * @param numFamille
     *            le numéro de famille
     * 
     * @return portefeuille - l'assureur en charge de cette famille dans le SI Quatrem
     */
    String getPortefeuille(Integer numFamille, String groupeGestion);

    /**
     * Retourne l'existance du couple Nofam/Groupe de gestion
     * 
     * @param numFamille
     *            le numéro de famille
     * @param groupeGestion
     *            le numéro de groupe de gestion
     * @return existance du couple numFamille/groupe gestion
     */

    boolean existeCoupleNofamNmgrpges(Integer numFamille, String groupeGestion);

}
