package fr.si2m.red.parametrage;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * Identifiant des plafonds des taux.
 * 
 * @author nortaina
 * 
 */
@Data
@EqualsAndHashCode(callSuper = false, of = { "ligneEnCoursImportBatch", "debut" })
@ToString(callSuper = false, of = { "ligneEnCoursImportBatch", "debut" })
@NoArgsConstructor
@AllArgsConstructor
public class ParamPlafondTauxId implements Serializable {
    /**
     * UID de version.
     */
    private static final long serialVersionUID = -6679275576681186483L;

    /**
     * Indicateur de ligne temporaire en cours d'import via Batch.
     * 
     * @param ligneEnCoursImportBatch
     *            le nom du SI d'où provient l'information
     * @return le nom du SI d'où provient l'information
     */
    private boolean ligneEnCoursImportBatch;

    /**
     * La date de début de validité des taux et montants de cette ligne.
     * 
     * @param debut
     *            la date de début de validité des taux et montants de cette ligne
     * @return la date de début de validité des taux et montants de cette ligne
     */
    private Integer debut;

}
