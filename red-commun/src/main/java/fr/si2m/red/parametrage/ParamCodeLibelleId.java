package fr.si2m.red.parametrage;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * Identifiant du modèle de Paramètre CodeLibelle DSN, pouvant être représenté notamment à l'intérieur d'un fichier ParamCodeLibelle éligible DSN (au format
 * CSV).
 * 
 * @author poidij
 * 
 * @see ParamCodeLibelle
 */
@Data
@EqualsAndHashCode(of = { "ligneEnCoursImportBatch", "table", "champ", "code" })
@ToString(of = { "ligneEnCoursImportBatch", "table", "champ", "code" })
@NoArgsConstructor
@AllArgsConstructor
public class ParamCodeLibelleId implements Serializable {
    /**
     * UID de version.
     */
    private static final long serialVersionUID = 3936349721258202798L;

    /**
     * Indicateur de ligne temporaire en cours d'import via Batch.
     * 
     * @param ligneEnCoursImportBatch
     *            le nom du SI d'où provient l'information
     * @return le nom du SI d'où provient l'information
     */
    private boolean ligneEnCoursImportBatch;

    /**
     * Le nom de la table contenant le code à mapper vers un libellé.
     * 
     * @param table
     *            le nom de la table contenant le code
     * 
     * @return le nom de la table contenant le code
     * 
     */
    private String table;

    /**
     * Le nom du champ de la table contenant le code à mapper vers un libellé.
     * 
     * @param champ
     *            le nom du champ de la table contenant le code
     * 
     * @return le nom du champ de la table contenant le code
     * 
     */
    private String champ;

    /**
     * La valeur du code pour ce libellé.
     * 
     * @param code
     *            la valeur du code pour ce libellé
     * 
     * @return la valeur du code pour ce libellé
     * 
     */
    private String code;

}
