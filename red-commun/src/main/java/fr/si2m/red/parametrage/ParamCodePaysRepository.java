package fr.si2m.red.parametrage;

import fr.si2m.red.core.repository.EntiteImportableRepository;

/**
 * Référentiel des Paramètres CodePays.
 * 
 * @author eudesr
 *
 */
public interface ParamCodePaysRepository extends EntiteImportableRepository<ParamCodePays> {

    /**
     * Retourne le code pays Insee à partir du code pays DSN
     * 
     * @param codePays
     *            codepays DSN
     * @return codePaysInsee
     */
    String getCodePaysInsee(String codePays);

}
