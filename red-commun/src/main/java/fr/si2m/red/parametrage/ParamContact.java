package fr.si2m.red.parametrage;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import fr.si2m.red.DateRedac;
import fr.si2m.red.EntiteImportableBatch;

/**
 * Modèle de Paramètre Contact DSN, pouvant être représenté notamment à l'intérieur d'un fichier ParamContact éligible DSN (au format CSV). <br/>
 * <br/>
 * Cette entité sert à lister les contacts pour REDAC.
 * 
 * @author nortaina
 *
 */
@Entity
@Table(name = "PARAM_CONTACT")
@IdClass(ParamContactId.class)
@Data
@EqualsAndHashCode(callSuper = false, of = { "ligneEnCoursImportBatch", "identifiantContact", "debut" })
@ToString(callSuper = false, of = { "ligneEnCoursImportBatch", "identifiantContact", "debut" })
public class ParamContact extends EntiteImportableBatch {
    /**
     * UID de version.
     */
    private static final long serialVersionUID = -8389106135411195721L;

    /**
     * Indicateur de ligne temporaire en cours d'import via Batch.
     * 
     * @param ligneEnCoursImportBatch
     *            le nom du SI d'où provient l'information
     * @return le nom du SI d'où provient l'information
     */
    @Id
    @Column(name = "TMP_BATCH")
    private boolean ligneEnCoursImportBatch;

    /**
     * Le code de mise à jour du contact.
     * 
     * @param codeMaj
     *            le code de mise à jour du contact
     * @return le code de mise à jour du contact
     */
    @Column(name = "CODE_MAJ")
    private String codeMaj;

    /**
     * L'identifiant du contact.
     * 
     * @param contact
     *            l'identifiant du contact
     * @return l'identifiant du contact
     */
    @Id
    @Column(name = "CONTACT")
    private String identifiantContact;

    /**
     * La date de début de l'application (au format SSAAMMJJ).
     * 
     * @param debut
     *            la date de début de l'application
     * @return la date de début de l'application
     */
    @Id
    @Column(name = "DEBUT")
    private Integer debut;

    /**
     * La date de fin de l'application (au format SSAAMMJJ).
     * 
     * @param fin
     *            la date de fin de l'application
     * @return la date de fin de l'application
     */
    @Column(name = "FIN")
    private Integer fin;

    /**
     * Le type d'enregistrement.
     * 
     * @param typeEnregistrement
     *            le type d'enregistrement
     * @return le type d'enregistrement
     */
    @Column(name = "TYPE_ENREG")
    private String typeEnregistrement;

    /**
     * L'identifiant de l'entreprise.
     * 
     * @param identifiantEntreprise
     *            l'identifiant de l'entreprise
     * @return l'identifiant de l'entreprise
     */
    @Column(name = "ENTREPRISE")
    private String identifiantEntreprise;

    /**
     * L'identifiant de l'établissement.
     * 
     * @param identifiantEtablissement
     *            l'identifiant de l'établissement
     * @return l'identifiant de l'établissement
     */
    @Column(name = "ETABLISSEMENT")
    private String identifiantEtablissement;

    /**
     * Le rôle de gestionnaire.
     * 
     * @param roleGestionnaire
     *            le rôle de gestionnaire
     * @return le rôle de gestionnaire
     */
    @Column(name = "ROLE_GEST")
    private String roleGestionnaire;

    /**
     * Le rôle technique.
     * 
     * @param roleTechnique
     *            le rôle technique
     * @return le rôle technique
     */
    @Column(name = "ROLE_TECH")
    private String roleTechnique;

    /**
     * Le nom du contact.
     * 
     * @param nomContact
     *            le nom du contact
     * @return le nom du contact
     */
    @Column(name = "NOM_CONTACT")
    private String nomContact;

    /**
     * Le numéro de téléphone du contact.
     * 
     * @param telephone
     *            le numéro de téléphone du contact
     * @return le numéro de téléphone du contact
     */
    @Column(name = "TELEPHONE")
    private String telephone;

    /**
     * Le complément du numéro de téléphone du contact.
     * 
     * @param complementTelephone
     *            le complément du numéro de téléphone du contact
     * @return le complément du numéro de téléphone du contact
     */
    @Column(name = "COMPL_TEL")
    private String complementTelephone;

    /**
     * Le numéro de fax du contact.
     * 
     * @param fax
     *            le numéro de fax du contact
     * @return le numéro de fax du contact
     */
    @Column(name = "FAX")
    private String fax;

    /**
     * L'email du contact.
     * 
     * @param email
     *            l'email du contact
     * @return l'email du contact
     */
    @Column(name = "EMAIL")
    private String email;

    /**
     * Date de début formatée.
     * 
     * @return Date de début formatée
     */
    public String getDebutFormatee() {
        return DateRedac.formate(DateRedac.EXPORT_FORMAT_PAR_DEFAUT, debut);
    }

    /**
     * Date de fin formatée.
     * 
     * @return Date de fin formatée
     */
    public String getFinFormatee() {
        return DateRedac.formate(DateRedac.EXPORT_FORMAT_PAR_DEFAUT, fin);
    }

    @Override
    public ParamContactId getId() {
        return new ParamContactId(ligneEnCoursImportBatch, identifiantContact, debut);
    }
}
