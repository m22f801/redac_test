package fr.si2m.red.parametrage;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * Identifiant de paramétrage de catégories d'effectifs.
 * 
 * @author nortaina
 *
 */
@Data
@EqualsAndHashCode(callSuper = false, of = { "ligneEnCoursImportBatch", "numCategorie", "libelleCategorie" })
@ToString(callSuper = false, of = { "ligneEnCoursImportBatch", "numCategorie", "libelleCategorie" })
@NoArgsConstructor
@AllArgsConstructor
public class ParamCategorieEffectifsId implements Serializable {
    /**
     * UID de version.
     */
    private static final long serialVersionUID = 612375192448298774L;

    /**
     * Indicateur de ligne temporaire en cours d'import via Batch.
     * 
     * @param ligneEnCoursImportBatch
     *            le nom du SI d'où provient l'information
     * @return le nom du SI d'où provient l'information
     */
    private boolean ligneEnCoursImportBatch;

    /**
     * Le numéro de catégorie / code population.
     * 
     * @param numCategorie
     *            le numéro de catégorie / code population
     * @return le numéro de catégorie / code population
     */
    private String numCategorie;

    /**
     * La partie du libellé de catégorie / code population permettant une identification unique du libellé.
     * 
     * @param libelleCategorie
     *            la partie du libellé de catégorie / code population permettant une identification unique du libellé
     * @return la partie du libellé de catégorie / code population permettant une identification unique du libellé
     */
    private String libelleCategorie;

}
