package fr.si2m.red.parametrage;

import fr.si2m.red.core.repository.EntiteImportableRepository;

/**
 * Référentiel des familles de contrat.
 * 
 * @author nortaina
 *
 */
public interface ParamFamilleModeCalculContratRepository extends EntiteImportableRepository<ParamFamilleModeCalculContrat> {

    /**
     * Retourne la valeur du contrôle types bases assujetties pour un numero de famille produit donné et pour un mode de calcul de cotisation.
     * 
     * @param numFamille
     *            le numéro de la famille
     * @param groupeGestion
     *            le groupe de gestion
     * @param modeCalculCotisation
     *            le mode de calcul de cotisation
     * @return le mode de contrôle des types de bases assujetties
     */

    String getModeControleTypesBasesAssujetties(Integer numFamille, String groupeGestion, Integer modeCalculCotisation);

    /**
     * Retourne la valeur du mode de déclaration pour un numero de famille produit donné et pour un mode de calcul de cotisation.
     * 
     * @param numFamille
     *            le numéro de la famille
     * @param modeCalculCotisation
     *            le mode de calcul de cotisation
     * @return le mode de déclaration
     */
    String getModeDeclaration(Integer numFamille, Integer modeCalculCotisation);

}
