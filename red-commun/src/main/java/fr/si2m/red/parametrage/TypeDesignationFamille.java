package fr.si2m.red.parametrage;

import lombok.Getter;

/**
 * Types de désignations de familles de produits gérés par REDAC.
 * 
 * @author nortaina
 *
 */
public enum TypeDesignationFamille {
    /**
     * Type Santé.
     */
    SANTE("Sant_"),
    /**
     * Type Prévoyance.
     */
    PREVOYANCE("Pr_voyance"),
    /**
     * Type Retraite.
     */
    RETRAITE("Retraite");

    /**
     * Le mot-clé présent dans une désignation de famille correspondant à ce type.
     * 
     * @return le mot-clé présent dans une désignation de famille correspondant à ce type
     */
    @Getter
    private final String motCleDesignation;

    private TypeDesignationFamille(String motCleDesignation) {
        this.motCleDesignation = motCleDesignation;
    }
}
