package fr.si2m.red.parametrage;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * Identifiant de paramétrage de nature de base de composants.
 * 
 * @author nortaina
 * 
 */
@Data
@EqualsAndHashCode(callSuper = false, of = { "ligneEnCoursImportBatch", "codeNatureBaseCotisations", "tauxCalculRempliAsText", "numTranche",
        "numTypeComposantBase" })
@ToString(callSuper = false, of = { "ligneEnCoursImportBatch", "codeNatureBaseCotisations", "tauxCalculRempliAsText", "numTranche",
        "numTypeComposantBase" })
@NoArgsConstructor
@AllArgsConstructor
public class ParamNatureBaseComposantsId implements Serializable {
    /**
     * UID de version.
     */
    private static final long serialVersionUID = 1697894393683946486L;

    /**
     * Indicateur de ligne temporaire en cours d'import via Batch.
     * 
     * @param ligneEnCoursImportBatch
     *            le nom du SI d'où provient l'information
     * @return le nom du SI d'où provient l'information
     */
    private boolean ligneEnCoursImportBatch;

    /**
     * Le code de nature de base dont la composition de la tranche est décrite par cette entité.
     * 
     * @param codeNatureBaseCotisations
     *            le code de nature de base dont la composition de la tranche est décrite par cette entité
     * @return le code de nature de base dont la composition de la tranche est décrite par cette entité
     */
    private Integer codeNatureBaseCotisations;

    /**
     * Indique si la tarification du contrat doit avoir un taux de calcul valorisé ou pas pour appliquer ce comportement.
     * 
     * @param tauxCalculRempliAsText
     *            'O' si la tarification du contrat doit avoir un taux de calcul valorisé ou pas pour appliquer ce comportement, 'N' sinon
     * @return 'O' si la tarification du contrat doit avoir un taux de calcul valorisé ou pas pour appliquer ce comportement, 'N' sinon
     */
    private String tauxCalculRempliAsText;

    /**
     * Le numéro de tranche pour cette nature de base.
     * 
     * @param numTranche
     *            le numéro de tranche pour cette nature de base
     * @return le numéro de tranche pour cette nature de base
     */
    private Integer numTranche;

    /**
     * Le numéro du type de composant de base assujettie DSN à utiliser.
     * 
     * @param numTypeComposantBase
     *            le numéro du type de composant de base assujettie DSN à utiliser
     * @return le numéro du type de composant de base assujettie DSN à utiliser
     */
    private Integer numTypeComposantBase;

}
