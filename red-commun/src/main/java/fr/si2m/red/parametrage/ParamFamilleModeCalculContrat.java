package fr.si2m.red.parametrage;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

import fr.si2m.red.EntiteImportableBatch;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * Modèle de Paramètre FamilleModeCalculContrat DSN, pouvant être représenté notamment à l'intérieur d'un fichier ParamFamilleModeCalculContrat éligible DSN (au
 * format CSV). <br/>
 * <br/>
 * Cette entité sert à spécifier le mode de calcul des cotisations d'une famille de contrat.
 * 
 * @author nortaina
 * 
 * @see ParamFamilleContrat
 */
@Entity
@Table(name = "PARAM_FAMILLE_MODE_CALCUL_CONTRAT")
@IdClass(ParamFamilleModeCalculContratId.class)
@Data
@EqualsAndHashCode(callSuper = false, of = { "ligneEnCoursImportBatch", "numFamille", "modeCalculCotisation", "modeDeclaration" })
@ToString(callSuper = false, of = { "ligneEnCoursImportBatch", "numFamille", "modeCalculCotisation", "modeDeclaration" })
public class ParamFamilleModeCalculContrat extends EntiteImportableBatch {
    /**
     * UID de version.
     */
    private static final long serialVersionUID = 5972972313559850796L;

    /**
     * Indicateur de ligne temporaire en cours d'import via Batch.
     * 
     * @param ligneEnCoursImportBatch
     *            le nom du SI d'où provient l'information
     * @return le nom du SI d'où provient l'information
     */
    @Id
    @Column(name = "TMP_BATCH")
    private boolean ligneEnCoursImportBatch;

    /**
     * Le numéro de famille du contrat.
     * 
     * @param numFamille
     *            le numéro de famille du contrat
     * @return le numéro de famille du contrat
     */
    @Id
    @Column(name = "NOFAM")
    private Integer numFamille;

    /**
     * Le mode de calcul de la cotisation du contrat.
     * 
     * @param modeCalculCotisation
     *            le mode de calcul de la cotisation du contrat
     * @return le mode de calcul de la cotisation du contrat
     */
    @Column(name = "MOCALCOT")
    private Integer modeCalculCotisation;

    /**
     * Le mode indiquant si la Brique DSN ou si REDAC doit contrôler les types de bases assujettie reçus dans les déclarations de ce contrat.
     * 
     * @param modeCalculCotisation
     *            le code indiquant si la Brique DSN ou si REDAC doit contrôler les types de bases assujettie reçus dans les déclarations de ce contrat
     * @return le code indiquant si la Brique DSN ou si REDAC doit contrôler les types de bases assujettie reçus dans les déclarations de ce contrat
     */
    @Column(name = "CTL_TYPE_BASE_ASSUJ")
    private String modeControleTypesBasesAssujetties;

    /**
     * Le mode de déclaration du contrat
     * 
     * @param modeDeclaration
     *            le mode de déclaration du contrat (pouvant avoir la valeur A ou C)
     * @return le mode de déclaration du contrat
     */
    @Column(name = "DONNEES_A_INTEGRER")
    private String modeDeclaration;

    @Override
    public ParamFamilleModeCalculContratId getId() {
        return new ParamFamilleModeCalculContratId(ligneEnCoursImportBatch, numFamille, modeCalculCotisation, modeDeclaration);
    }
}
