package fr.si2m.red.parametrage;

import fr.si2m.red.core.repository.EntiteImportableRepository;

/**
 * Référentiel des paramètres de plafonds de taux.
 * 
 * @author nortaina
 *
 */
public interface ParamPlafondTauxRepository extends EntiteImportableRepository<ParamPlafondTaux> {

    /**
     * Récupère le plafond mensuel de la sécurité sociale valable à une date donnée.
     * 
     * @param date
     *            la date
     * @return le plafond mensuel de la sécurité sociale valable à cette date
     */
    Double getPlafondMensuelSecuriteSociale(Integer date);

}
