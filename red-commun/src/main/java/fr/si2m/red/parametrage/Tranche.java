package fr.si2m.red.parametrage;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * Définition d'une tranche définie dans ParamNatureBase.
 * 
 * @author nortaina
 *
 */
@Data
@AllArgsConstructor
public class Tranche {
    /**
     * Le numéro de tranche à transmettre à WQUI/GERD pour alimenter la tranche correspondante. Il est compris entre 1 et 4.
     * 
     * @param numTranche
     *            le numéro de tranche à transmettre à WQUI/GERD pour alimenter la tranche correspondante
     * @return le numéro de tranche à transmettre à WQUI/GERD pour alimenter la tranche correspondante
     */
    private Integer numTranche;

    /**
     * Le libellé de la tranche à transmettre à WQUI/GERD pour l'alimenter.
     * 
     * @param libelleTranche
     *            le libellé de la tranche à transmettre à WQUI/GERD pour l'alimenter
     * @return le libellé de la tranche à transmettre à WQUI/GERD pour l'alimenter
     */
    private String libelleTranche;
}
