package fr.si2m.red;

/**
 * Erreur inattendue dans un traitement REDAC.
 * 
 * @author nortaina
 *
 */
public class RedacUnexpectedException extends RuntimeException {
    /**
     * UID de version.
     */
    private static final long serialVersionUID = 7548007939433643391L;

    /**
     * Constructeur d'une erreur inattendue.
     * 
     * @param cause
     *            la cause de l'erreur
     */
    public RedacUnexpectedException(Throwable cause) {
        super(cause);
    }

    /**
     * Constructeur d'une erreur inattendue.
     * 
     * @param message
     *            le message de l'erreur
     */
    public RedacUnexpectedException(String message) {
        super(message);
    }

    /**
     * Constructeur d'une erreur inattendue.
     * 
     * @param message
     *            le message de l'erreur
     * @param cause
     *            la cause de l'erreur
     */
    public RedacUnexpectedException(String message, Throwable cause) {
        super(message, cause);
    }
}
