package fr.si2m.red.dsn;

import java.io.Serializable;

import lombok.Data;
import lombok.NonNull;

/**
 * Plage de rattachement.
 * 
 * @author nortaina
 *
 */
@Data
public class PlageRattachement implements Serializable {
    /**
     * UID de version.
     */
    private static final long serialVersionUID = -8136228404760884291L;

    @NonNull
    private Integer dateDebutPlage;
    @NonNull
    private Integer dateFinPlage;
}
