package fr.si2m.red.dsn;

/**
 * Etats d'une periode.
 * 
 * @author nortaina
 *
 */
public enum EtatPeriode {
    /**
     * En réception.
     */
    RCP,
    /**
     * A réintégrer.
     */
    ARI,
    /**
     * En cours d’intégration.
     */
    ENC,
    /**
     * Intégrée.
     */
    INT,
    /**
     * Intégrée avec signalement.
     */
    INS,
    /**
     * Intégrée signalements acquittés gestion.
     */
    ING,
    /**
     * Non intégré.
     */
    NIN,
    /**
     * Traité par gestion.
     */
    TPG,
    /**
     * Sans suite gestion.
     */
    SSG;


    /**
     * Récupère le code de l'état.
     * 
     * @return le code de l'état
     */
    public String getCodeEtat() {
        return name();
    }

}
