package fr.si2m.red.dsn;

import fr.si2m.red.core.repository.EntiteImportableRepository;

/**
 * Référentiel des cotisations d'établissement.
 * 
 * @author poidij
 *
 */
public interface CotisationEtablissementRepository extends EntiteImportableRepository<CotisationEtablissement> {

    /**
     * Indique l'existence d'une cotisation en base.
     * 
     * @param idCotisationEtablissement
     *            l'identifiant de la cotisation
     * @return true s'il existe au moins une cotisation ayant cet identifiant, false sinon
     */
    boolean existeUneCotisationEtablissement(String idCotisationEtablissement);

}
