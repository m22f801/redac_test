package fr.si2m.red.dsn;

import fr.si2m.red.core.repository.EntiteImportableRepository;

/**
 * Référentiel des rémunérations.
 * 
 * @author poidij
 *
 */
public interface RemunerationRepository extends EntiteImportableRepository<Remuneration> {

    /**
     * Indique l'existence d'une rémunération en base.
     * 
     * @param idRemuneration
     *            l'identifiant de la rémunération
     * @return true s'il existe au moins une rémunération ayant cet identifiant, false sinon
     */
    boolean existeUneRemuneration(String idRemuneration);

}
