package fr.si2m.red.dsn;

import fr.si2m.red.core.repository.EntiteImportableRepository;

/**
 * Référentiel des versements des individus.
 * 
 * @author poidij
 *
 */
public interface VersementIndividuRepository extends EntiteImportableRepository<VersementIndividu> {

    /**
     * Indique l'existence d'un versement individu en base
     * 
     * @param idVersementIndividu
     *            l'identifiant du versement individu
     * @return true s'il existe au moins un versement individu ayant cet identifiant, false sinon
     */
    boolean existeUnVersementIndividu(String idVersementIndividu);
}
