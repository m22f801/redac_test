package fr.si2m.red.dsn;

import fr.si2m.red.core.repository.EntiteImportableRepository;

/**
 * Référentiel des contacts.
 * 
 * @author poidij
 *
 */
public interface ContactRepository extends EntiteImportableRepository<Contact> {

    /**
     * Indique l'existence d'un contact en base.
     * 
     * @param idContact
     *            l'identifiant du contact
     * @return true s'il existe au moins un contact ayant cet identifiant, false sinon
     */
    boolean existeUnContact(String idContact);

}
