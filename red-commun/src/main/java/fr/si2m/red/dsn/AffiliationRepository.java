package fr.si2m.red.dsn;

import java.util.List;

import fr.si2m.red.core.repository.EntiteImportableRepository;

/**
 * Référentiel des affiliations.
 * 
 * @author poidij
 *
 */
public interface AffiliationRepository extends EntiteImportableRepository<Affiliation> {

    /**
     * Indique l'existence d'une Affiliation en base.
     * 
     * @param idAffiliation
     *            l'identifiant de l'affiliation
     * @return true s'il existe au moins une affiliation ayant cet identifiant, false sinon
     */
    boolean existeUneAffiliation(String idAffiliation);

    /**
     * Récupère l'ensemble des affiliations liées à un contrat de travail.
     * 
     * @param idContratTravail
     *            l'identifiant du contrat de travail
     * @return l'ensemble des affiliations liées
     */
    List<Affiliation> getPourContratTravail(String idContratTravail);
}
