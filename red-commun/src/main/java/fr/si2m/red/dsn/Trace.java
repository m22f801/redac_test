package fr.si2m.red.dsn;

import lombok.Data;

/**
 * Trace à logger (flux 4 - batch 405)
 * 
 * @author poidij
 *
 */
@Data
public class Trace {

    /**
     * Identifiant de période
     *
     * @param idPeriode
     *            l'identifiant de période
     * @return l'identifiant de période
     */
    private Long idPeriode;

    /**
     * La trace à logger
     *
     * @param message
     *            la trace à logger
     * @return la trace à logger
     */
    private String message;

}
