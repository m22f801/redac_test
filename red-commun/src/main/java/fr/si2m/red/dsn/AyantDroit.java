package fr.si2m.red.dsn;

import java.math.BigInteger;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;
import javax.persistence.Transient;

import fr.si2m.red.EntiteImportableBatch;
import fr.si2m.red.core.repository.jpa.conversion.ConvertisseurDatesDDMMYYYY;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * Modèle des ayants droit.
 * 
 * @author poidij
 *
 */
@Entity
@Table(name = "AYANT_DROIT")
@IdClass(AyantDroitId.class)
@Data
@EqualsAndHashCode(callSuper = false, of = { "ligneEnCoursImportBatch", "idAyantDroit" })
@ToString(callSuper = false, of = { "ligneEnCoursImportBatch", "idAyantDroit" })
public class AyantDroit extends EntiteImportableBatch {

    /**
     * UID de version.
     */
    private static final long serialVersionUID = 822362569368101808L;

    /**
     * Indicateur de ligne temporaire en cours d'import via Batch.
     * 
     * @param ligneEnCoursImportBatch
     *            le nom du SI d'où provient l'information
     * @return le nom du SI d'où provient l'information
     */
    @Id
    @Column(name = "TMP_BATCH")
    private boolean ligneEnCoursImportBatch;

    /**
     * Identifiant de l'ayant droit
     *
     * @param idAyantDroit
     *            L'identifiant de l'ayant droit
     * @return L'identifiant de l'ayant droit
     */
    @Id
    @Column(name = "ID_AYANT_DROIT")
    private String idAyantDroit;

    /**
     * Identifiant de l'affiliation
     *
     * @param idAffiliation
     *            L'identifiant de l'affiliation
     * @return L'identifiant de l'affiliation
     */
    @Column(name = "ID_AFFILIATION")
    private String idAffiliation;

    /**
     * S21.G00.73.001
     *
     * @param regimeAlsaceMoselle
     *            S21.G00.73.001
     * @return S21.G00.73.001
     */
    @Column(name = "REGIME_ALSACE_MOSELLE")
    private String regimeAlsaceMoselle;

    /**
     * S21.G00.73.002
     *
     * @param codeOption
     *            S21.G00.73.002
     * @return S21.G00.73.002
     */
    @Column(name = "CODE_OPTION")
    private String codeOption;

    /**
     * S21.G00.73.003
     *
     * @param type
     *            S21.G00.73.003
     * @return S21.G00.73.003
     */
    @Column(name = "TYPE")
    private String type;

    /**
     * S21.G00.73.004
     *
     * @param dateDebutRattachementOuvrantDroit
     *            S21.G00.73.004
     * @return S21.G00.73.004
     */
    @Column(name = "DATE_DEB_RATTACHEMENT")
    private Integer dateDebutRattachementOuvrantDroit;

    /**
     * S21.G00.73.005
     *
     * @param dateNaissance
     *            S21.G00.73.005
     * @return S21.G00.73.005
     */
    @Column(name = "DATE_NAISSANCE")
    private Integer dateNaissance;

    /**
     * S21.G00.73.006
     *
     * @param nomFamille
     *            S21.G00.73.006
     * @return S21.G00.73.006
     */
    @Column(name = "NOM_FAMILLE")
    private String nomFamille;

    /**
     * S21.G00.73.007
     *
     * @param numeroInscriptionRepertoire
     *            S21.G00.73.007
     * @return S21.G00.73.007
     */
    @Column(name = "NIR")
    private String numeroInscriptionRepertoire;

    /**
     * S21.G00.73.008
     *
     * @param nirOuvrantDroitRegimeBaseMaladie
     *            S21.G00.73.008
     * @return S21.G00.73.008
     */
    @Column(name = "NIR_OUVRANT_DROIT")
    private String nirOuvrantDroitRegimeBaseMaladie;

    /**
     * S21.G00.73.009
     *
     * @param prenom
     *            S21.G00.73.009
     * @return S21.G00.73.009
     */
    @Column(name = "PRENOM")
    private String prenom;

    /**
     * S21.G00.73.010
     *
     * @param codeOrganismeAffiliationAssuranceMaladie
     *            S21.G00.73.010
     * @return S21.G00.73.010
     */
    @Column(name = "CODE_ORG_ASS_MALADIE")
    private String codeOrganismeAffiliationAssuranceMaladie;

    /**
     * S21.G00.73.011
     *
     * @param dateFinRattachementOuvrantDroit
     *            S21.G00.73.011
     * @return S21.G00.73.011
     */
    @Column(name = "DATE_FIN_RATTACHEMENT")
    private Integer dateFinRattachementOuvrantDroit;

    /**
     * Date de début de rattachement de l'ouvrant droit (telle que lue dans le fichier).
     * 
     */
    @Transient
    private String dateDebutRattachementOuvrantDroitAsText;

    /**
     * Date de naissance (telle que lue dans le fichier).
     * 
     */
    @Transient
    private String dateNaissanceAsText;

    /**
     * Date de fin de rattachement de l'ouvrant droit (telle que lue dans le fichier).
     * 
     */
    @Transient
    private String dateFinRattachementOuvrantDroitAsText;

    /**
     * Identifiant de la DSN dans la brique
     */
    @Column(name = "ID_DSN_BRIQUE")
    private BigInteger idDsnBrique;

    /**
     * Clé primaire de la table dans la brique
     */
    @Column(name = "PK_ENTITEE_BRIQUE")
    private BigInteger cleEntiteeBrique;

    /**
     * Date de début de rattachement de l'ouvrant droit (telle que lue dans le fichier).
     * 
     * @return la date de début de rattachement de l'ouvrant droit
     */
    public String getDateDebutRattachementOuvrantDroitAsText() {
        if (dateDebutRattachementOuvrantDroitAsText != null) {
            return dateDebutRattachementOuvrantDroitAsText;
        }
        return new ConvertisseurDatesDDMMYYYY().convertToEntityAttribute(getDateDebutRattachementOuvrantDroit());
    }

    /**
     * Valorise la date de début de rattachement de l'ouvrant droit (telle que lue dans le fichier).
     * 
     * @param dateDebutRattachementOuvrantDroit
     *            la date de début de rattachement de l'ouvrant droit à valoriser
     */
    public void setDateDebutRattachementOuvrantDroitAsText(String dateDebutRattachementOuvrantDroit) {
        this.dateDebutRattachementOuvrantDroitAsText = dateDebutRattachementOuvrantDroit;
        setDateDebutRattachementOuvrantDroit(new ConvertisseurDatesDDMMYYYY().convertToDatabaseColumn(dateDebutRattachementOuvrantDroit));
    }

    /**
     * Date de naissance (telle que lue dans le fichier).
     * 
     * @return la date de naissance
     */
    public String getDateNaissanceAsText() {
        if (dateNaissanceAsText != null) {
            return dateNaissanceAsText;
        }
        return new ConvertisseurDatesDDMMYYYY().convertToEntityAttribute(getDateNaissance());
    }

    /**
     * Valorise la date de naissance (telle que lue dans le fichier).
     * 
     * @param dateNaissance
     *            la date de naissance à valoriser
     */
    public void setDateNaissanceAsText(String dateNaissance) {
        this.dateNaissanceAsText = dateNaissance;
        setDateNaissance(new ConvertisseurDatesDDMMYYYY().convertToDatabaseColumn(dateNaissance));
    }

    /**
     * Date de fin de rattachement de l'ouvrant droit (telle que lue dans le fichier).
     * 
     * @return la date de fin de rattachement de l'ouvrant droit
     */
    public String getDateFinRattachementOuvrantDroitAsText() {
        if (dateFinRattachementOuvrantDroitAsText != null) {
            return dateFinRattachementOuvrantDroitAsText;
        }
        return new ConvertisseurDatesDDMMYYYY().convertToEntityAttribute(getDateFinRattachementOuvrantDroit());
    }

    /**
     * Valorise la date de fin de rattachement de l'ouvrant droit (telle que lue dans le fichier).
     * 
     * @param dateFinRattachementOuvrantDroit
     *            la date de fin de rattachement de l'ouvrant droit à valoriser
     */
    public void setDateFinRattachementOuvrantDroitAsText(String dateFinRattachementOuvrantDroit) {
        this.dateFinRattachementOuvrantDroitAsText = dateFinRattachementOuvrantDroit;
        setDateFinRattachementOuvrantDroit(new ConvertisseurDatesDDMMYYYY().convertToDatabaseColumn(dateFinRattachementOuvrantDroit));
    }

    @Override
    public AyantDroitId getId() {
        return new AyantDroitId(ligneEnCoursImportBatch, idAyantDroit);
    }

}
