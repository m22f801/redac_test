package fr.si2m.red.dsn;

import fr.si2m.red.core.repository.EntiteImportableRepository;

/**
 * Référentiel des arrêts de travail.
 * 
 * @author poidij
 *
 */
public interface ArretTravailRepository extends EntiteImportableRepository<ArretTravail> {

    /**
     * Récupère le dernier arrêt de travail connu lié à un contrat de travail donné.
     * 
     * @param idContratTravail
     *            l'identifiant du contrat de travail
     * @return l'arrêt de travail récupéré
     */
    ArretTravail getDernierArretTravailPourContratTravail(String idContratTravail);

    /**
     * Indique l'existence d'un Arret de Travail en base.
     * 
     * @param idArretTravail
     *            l'identifiant de l'arrêt
     * @return true s'il existe au moins un arrêt ayant cet identifiant, false sinon
     */
    boolean existeUnArretTravail(String idArretTravail);
}
