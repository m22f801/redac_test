package fr.si2m.red.dsn;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * Modèle des composants de versement
 * 
 * @author poidij
 *
 */
@Data
@EqualsAndHashCode(callSuper = false, of = { "ligneEnCoursImportBatch", "idComposantVersement" })
@ToString(callSuper = false, of = { "ligneEnCoursImportBatch", "idComposantVersement" })
@NoArgsConstructor
@AllArgsConstructor
public class ComposantVersementId implements Serializable {

    /**
     * UID de version.
     */
    private static final long serialVersionUID = 822362569368101808L;

    /**
     * Indicateur de ligne temporaire en cours d'import via Batch.
     * 
     * @param ligneEnCoursImportBatch
     *            le nom du SI d'où provient l'information
     * @return le nom du SI d'où provient l'information
     */
    private boolean ligneEnCoursImportBatch;

    /**
     * Identifiant du composant de versement
     *
     * @param idComposantVersement
     *            L'identifiant du composant de versement
     * @return L'identifiant du composant de versement
     */
    private String idComposantVersement;

}
