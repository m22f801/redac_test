package fr.si2m.red.dsn;

import java.math.BigInteger;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import fr.si2m.red.EntiteImportableBatch;
import fr.si2m.red.core.repository.jpa.conversion.ConvertisseurNumeriqueDecimal;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * Modèle des composants de base assujettie
 * 
 * @author poidij
 *
 */
@Entity
@Table(name = "COMPOSANT_BASE_ASSUJETTIE")
@IdClass(ComposantBaseAssujettieId.class)
@Data
@EqualsAndHashCode(callSuper = false, of = { "ligneEnCoursImportBatch", "idComposantBaseAssujettie" })
@ToString(callSuper = false, of = { "ligneEnCoursImportBatch", "idComposantBaseAssujettie" })
public class ComposantBaseAssujettie extends EntiteImportableBatch {

    /**
     * UID de version.
     */
    private static final long serialVersionUID = 822362569368101808L;

    /**
     * Indicateur de ligne temporaire en cours d'import via Batch.
     * 
     * @param ligneEnCoursImportBatch
     *            le nom du SI d'où provient l'information
     * @return le nom du SI d'où provient l'information
     */
    @Id
    @Column(name = "TMP_BATCH")
    private boolean ligneEnCoursImportBatch;

    /**
     * Identifiant du composant de base assujettie
     *
     * @param idComposantBaseAssujettie
     *            L'identifiant du composant de base assujettie
     * @return L'identifiant du composant de base assujettie
     */
    @Id
    @Column(name = "ID_COMPO_BASE_ASSUJETTIE")
    private String idComposantBaseAssujettie;

    /**
     * Identifiant de la base assujettie
     *
     * @param idBaseAssujettie
     *            L'identifiant de la base assujettie
     * @return L'identifiant de la base assujettie
     */
    @Column(name = "ID_BASE_ASSUJETTIE")
    private String idBaseAssujettie;

    /**
     * La base assujettie.
     *
     * @param baseAssujettie
     *            la base assujettie
     * @return la base assujettie
     */
    @ManyToOne
    @Fetch(FetchMode.JOIN)
    @JoinColumns({ @JoinColumn(name = "ID_BASE_ASSUJETTIE", insertable = false, updatable = false),
            @JoinColumn(name = "TMP_BATCH", insertable = false, updatable = false) })
    private BaseAssujettie baseAssujettie;

    /**
     * S21.G00.79.001
     *
     * @param typeComposantBaseAssujettie
     *            S21.G00.79.001
     * @return S21.G00.79.001
     */
    @Column(name = "TYPE_COMPO_BASE_ASSUJ")
    private String typeComposantBaseAssujettie;

    /**
     * S21.G00.79.004
     *
     * @param montantComposantBaseAssujettie
     *            S21.G00.79.004
     * @return S21.G00.79.004
     */
    @Column(name = "MONTANT_COMPO_BASE_ASSUJ")
    private Double montantComposantBaseAssujettie;

    /**
     * Montant du composant de base assujettie (tel que lu dans le fichier).
     *
     */
    @Transient
    private String montantComposantBaseAssujettieAsText;

    /**
     * Identifiant de la DSN dans la brique
     */
    @Column(name = "ID_DSN_BRIQUE")
    private BigInteger idDsnBrique;

    /**
     * Clé primaire de la table dans la brique
     */
    @Column(name = "PK_ENTITEE_BRIQUE")
    private BigInteger cleEntiteeBrique;

    /**
     * Renvoie le montant du composant de base assujettie (tel que lu dans le fichier).
     * 
     * @return le montant du composant de base assujettie
     */
    public String getMontantComposantBaseAssujettieAsText() {
        String value = this.montantComposantBaseAssujettieAsText;
        if (StringUtils.isBlank(value) && getMontantComposantBaseAssujettie() != null) {
            value = new ConvertisseurNumeriqueDecimal().convertToEntityAttribute(getMontantComposantBaseAssujettie());
        }
        return value;
    }

    /**
     * Valorise le montant du composant de base assujettie (tel que lu dans le fichier).
     * 
     * @param montantComposantBaseAssujettie
     *            le montant du composant de base assujettie à valoriser
     */
    public void setMontantComposantBaseAssujettieAsText(String montantComposantBaseAssujettie) {
        this.montantComposantBaseAssujettieAsText = montantComposantBaseAssujettie;
        setMontantComposantBaseAssujettie(new ConvertisseurNumeriqueDecimal().convertToDatabaseColumn(montantComposantBaseAssujettie));
    }

    @Override
    public ComposantBaseAssujettieId getId() {
        return new ComposantBaseAssujettieId(ligneEnCoursImportBatch, idComposantBaseAssujettie);
    }

}
