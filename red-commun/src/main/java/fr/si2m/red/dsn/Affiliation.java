package fr.si2m.red.dsn;

import java.math.BigInteger;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import fr.si2m.red.EntiteImportableBatch;
import fr.si2m.red.core.repository.jpa.conversion.ConvertisseurNumeriqueInteger;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * Modèle des affiliations.
 * 
 * @author poidij
 *
 */
@Entity
@Table(name = "AFFILIATION")
@IdClass(AffiliationId.class)
@Data
@EqualsAndHashCode(callSuper = false, of = { "ligneEnCoursImportBatch", "idAffiliation" })
@ToString(callSuper = false, of = { "ligneEnCoursImportBatch", "idAffiliation" })
public class Affiliation extends EntiteImportableBatch {

    /**
     * UID de version.
     */
    private static final long serialVersionUID = 822362569368101808L;

    /**
     * Indicateur de ligne temporaire en cours d'import via Batch.
     * 
     * @param ligneEnCoursImportBatch
     *            le nom du SI d'où provient l'information
     * @return le nom du SI d'où provient l'information
     */
    @Id
    @Column(name = "TMP_BATCH")
    private boolean ligneEnCoursImportBatch;

    /**
     * Identifiant de l'affiliation.
     *
     * @param idAffiliation
     *            L'identifiant de l'affiliation
     * @return L'identifiant de l'affiliation
     */
    @Id
    @Column(name = "ID_AFFILIATION")
    private String idAffiliation;

    /**
     * Identifiant du contrat de travail.
     *
     * @param idContratTravail
     *            L'identifiant du contrat de travail
     * @return L'identifiant du contrat de travail
     */
    @Column(name = "ID_CONTRAT_TRAVAIL")
    private String idContratTravail;

    /**
     * Le contrat de travail.
     *
     * @param contratTravail
     *            Le contrat de travail
     * @return Le contrat de travail
     */
    @ManyToOne
    @Fetch(FetchMode.JOIN)
    @JoinColumns({ @JoinColumn(name = "ID_CONTRAT_TRAVAIL", insertable = false, updatable = false),
            @JoinColumn(name = "TMP_BATCH", insertable = false, updatable = false) })
    private ContratTravail contratTravail;

    /**
     * S21.G00.70.004
     *
     * @param codeOption
     *            S21.G00.70.004
     * @return S21.G00.70.004
     */
    @Column(name = "CODE_OPTION")
    private String codeOption;

    /**
     * S21.G00.70.005
     *
     * @param codePopulation
     *            S21.G00.70.005
     * @return S21.G00.70.005
     */
    @Column(name = "CODE_POPULATION")
    private String codePopulation;

    /**
     * S21.G00.70.007
     *
     * @param nombreEnfantsCharge
     *            S21.G00.70.007
     * @return S21.G00.70.007
     */
    @Column(name = "NOMBRE_ENFANTS_CHARGE")
    private Integer nombreEnfantsCharge;

    /**
     * S21.G00.70.008
     *
     * @param nombreAyantDroitAdultes
     *            S21.G00.70.008
     * @return S21.G00.70.008
     */
    @Column(name = "NOMBRE_AYDR_ADULTES")
    private Integer nombreAyantDroitAdultes;

    /**
     * S21.G00.70.009
     *
     * @param nombreAyantDroit
     *            S21.G00.70.009
     * @return S21.G00.70.009
     */
    @Column(name = "NOMBRE_AYDR")
    private Integer nombreAyantDroit;

    /**
     * S21.G00.70.010
     *
     * @param nombreAyantDroitAutres
     *            S21.G00.70.010
     * @return S21.G00.70.010
     */
    @Column(name = "NOMBRE_AYDR_AUTRES")
    private Integer nombreAyantDroitAutres;

    /**
     * S21.G00.70.011
     *
     * @param nombreAyantDroitEnfants
     *            S21.G00.70.011
     * @return S21.G00.70.011
     */
    @Column(name = "NOMBRE_AYDR_ENFANTS")
    private Integer nombreAyantDroitEnfants;

    /**
     * S21.G00.70.014
     *
     * @param dateDebutAffiliation;
     *            S21.G00.70.014
     * @return S21.G00.70.014
     */
    @Column(name = "DATE_DEBUT_AFFILIATION")
    private Integer dateDebutAffiliation;

    /**
     * S21.G00.70.015
     *
     * @param dateFinAffiliation;
     *            S21.G00.70.015
     * @return S21.G00.70.015
     */
    @Column(name = "DATE_FIN_AFFILIATION")
    private Integer dateFinAffiliation;

    /**
     * Le nombre d'enfants à charge lu dans un fichier d'import.
     *
     * @param nombreEnfantsChargeAsText
     * 
     * @return le nombre d'enfants à charge lu dans un fichier d'import
     */
    @Transient
    private String nombreEnfantsChargeAsText;

    /**
     * Le nombre d'adultes ayant droit lu dans un fichier d'import.
     *
     * @param nombreAyantDroitAdultesAsText
     * 
     * @return le nombre d'adultes ayant droit lu dans un fichier d'import
     */
    @Transient
    private String nombreAyantDroitAdultesAsText;

    /**
     * Le nombre d'ayant droit lu dans un fichier d'import.
     *
     * @param nombreAyantDroitAsText
     * 
     * @return le nombre d'ayant droit lu dans un fichier d'import
     */
    @Transient
    private String nombreAyantDroitAsText;

    /**
     * Le nombre d'autres ayant droit lu dans un fichier d'import.
     *
     * @param nombreAyantDroitAutresAsText
     * 
     * @return le nombre d'autres ayant droit lu dans un fichier d'import
     */
    @Transient
    private String nombreAyantDroitAutresAsText;

    /**
     * Le nombre d'enfants ayant droit lu dans un fichier d'import.
     *
     * @param nombreAyantDroitEnfantsAsText
     * 
     * @return le nombre d'enfants ayant droit lu dans un fichier d'import
     */
    @Transient
    private String nombreAyantDroitEnfantsAsText;

    /**
     * Identifiant de la DSN dans la brique
     */
    @Column(name = "ID_DSN_BRIQUE")
    private BigInteger idDsnBrique;

    /**
     * Clé primaire de la table dans la brique
     */
    @Column(name = "PK_ENTITEE_BRIQUE")
    private BigInteger cleEntiteeBrique;

    /**
     * Nombre d'enfants à charge (tel que lu dans le fichier).
     *
     * @return le nombre d'enfants à charge
     */
    public String getNombreEnfantsChargeAsText() {
        if (nombreEnfantsChargeAsText != null) {
            return nombreEnfantsChargeAsText;
        } else {
            return new ConvertisseurNumeriqueInteger().convertToEntityAttribute(getNombreEnfantsCharge());
        }
    }

    /**
     * Valorise le nombre d'enfants à charge (tel que lu dans le fichier).
     * 
     * @param nombreEnfantsCharge
     *            nombre d'enfants à charge à valoriser
     */
    public void setNombreEnfantsChargeAsText(String nombreEnfantsCharge) {
        this.nombreEnfantsChargeAsText = nombreEnfantsCharge;
        setNombreEnfantsCharge(new ConvertisseurNumeriqueInteger().convertToDatabaseColumn(nombreEnfantsCharge));
    }

    /**
     * Nombre d'adultes ayant droit (tel que lu dans le fichier).
     *
     * @return le nombre d'adultes ayant droit
     */
    public String getNombreAyantDroitAdultesAsText() {
        if (nombreAyantDroitAdultesAsText != null) {
            return nombreAyantDroitAdultesAsText;
        } else {
            return new ConvertisseurNumeriqueInteger().convertToEntityAttribute(getNombreAyantDroitAdultes());
        }
    }

    /**
     * Valorise le nombre d'adultes ayant droit (tel que lu dans le fichier).
     * 
     * @param nombreAyantDroitAdultes
     *            nombre d'adultes ayant droit à valoriser
     */
    public void setNombreAyantDroitAdultesAsText(String nombreAyantDroitAdultes) {
        this.nombreAyantDroitAdultesAsText = nombreAyantDroitAdultes;
        setNombreAyantDroitAdultes(new ConvertisseurNumeriqueInteger().convertToDatabaseColumn(nombreAyantDroitAdultes));
    }

    /**
     * Nombre d'ayant droit (tel que lu dans le fichier).
     *
     * @return le nombre d'ayant droit
     */
    public String getNombreAyantDroitAsText() {
        if (nombreAyantDroitAsText != null) {
            return nombreAyantDroitAsText;
        } else {
            return new ConvertisseurNumeriqueInteger().convertToEntityAttribute(getNombreAyantDroit());
        }
    }

    /**
     * Valorise le nombre d'ayant droit (tel que lu dans le fichier).
     * 
     * @param nombreAyantDroit
     *            nombre d'ayant droit à valoriser
     */
    public void setNombreAyantDroitAsText(String nombreAyantDroit) {
        this.nombreAyantDroitAsText = nombreAyantDroit;
        setNombreAyantDroit(new ConvertisseurNumeriqueInteger().convertToDatabaseColumn(nombreAyantDroit));
    }

    /**
     * Nombre d'autres ayant droit (tel que lu dans le fichier).
     *
     * @return le nombre d'autres ayant droit
     */
    public String getNombreAyantDroitAutresAsText() {
        if (nombreAyantDroitAutresAsText != null) {
            return nombreAyantDroitAutresAsText;
        } else {
            return new ConvertisseurNumeriqueInteger().convertToEntityAttribute(getNombreAyantDroitAutres());
        }
    }

    /**
     * Valorise le nombre d'autres ayant droit (tel que lu dans le fichier).
     * 
     * @param nombreAyantDroitAutres
     *            nombre d'autres ayant droit à valoriser
     */
    public void setNombreAyantDroitAutresAsText(String nombreAyantDroitAutres) {
        this.nombreAyantDroitAutresAsText = nombreAyantDroitAutres;
        setNombreAyantDroitAutres(new ConvertisseurNumeriqueInteger().convertToDatabaseColumn(nombreAyantDroitAutres));
    }

    /**
     * Nombre d'enfants ayant droit (tel que lu dans le fichier).
     *
     * @return le nombre d'enfants ayant droit
     */
    public String getNombreAyantDroitEnfantsAsText() {
        if (nombreAyantDroitEnfantsAsText != null) {
            return nombreAyantDroitEnfantsAsText;
        } else {
            return new ConvertisseurNumeriqueInteger().convertToEntityAttribute(getNombreAyantDroitEnfants());
        }
    }

    /**
     * Valorise le nombre d'enfants ayant droit (tel que lu dans le fichier).
     * 
     * @param nombreAyantDroitEnfants
     *            nombre d'enfants ayant droit à valoriser
     */
    public void setNombreAyantDroitEnfantsAsText(String nombreAyantDroitEnfants) {
        this.nombreAyantDroitEnfantsAsText = nombreAyantDroitEnfants;
        setNombreAyantDroitEnfants(new ConvertisseurNumeriqueInteger().convertToDatabaseColumn(nombreAyantDroitEnfants));
    }

    @Override
    public AffiliationId getId() {
        return new AffiliationId(ligneEnCoursImportBatch, idAffiliation);
    }

}
