package fr.si2m.red.dsn;

import fr.si2m.red.core.repository.EntiteImportableRepository;

/**
 * Référentiel des adhésions établissement mois.
 * 
 * @author poidij
 *
 */
public interface AdhesionEtablissementMoisRepository extends EntiteImportableRepository<AdhesionEtablissementMois> {

    /**
     * Indique l'existence d'une Adhesion dans le référentiel.
     * 
     * @param idAdhEtabMois
     *            l'identifiant de l'adhésion
     * @return true s'il existe au moins une adhésion ayant cet identifiant, false sinon
     */
    boolean existeUneAdhesion(String idAdhEtabMois);

    /**
     * Supprime les adhésions de test ou correspondant à des contrats délégués.
     * 
     * @return le nombre d'adhésions supprimées
     */
    int supprimeAdhesionsDeTestOuDeContratsDelegues();

}
