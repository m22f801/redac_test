package fr.si2m.red.dsn;

import java.math.BigInteger;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import fr.si2m.red.EntiteImportableBatch;
import fr.si2m.red.core.repository.jpa.conversion.ConvertisseurDatesDDMMYYYY;
import fr.si2m.red.core.repository.jpa.conversion.ConvertisseurNumeriqueDecimal;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * Modèle des contrats de travail.
 * 
 * @author poidij
 *
 */
@Entity
@Table(name = "CONTRAT_TRAVAIL")
@IdClass(ContratTravailId.class)
@Data
@EqualsAndHashCode(callSuper = false, of = { "ligneEnCoursImportBatch", "idContratTravail" })
@ToString(callSuper = false, of = { "ligneEnCoursImportBatch", "idContratTravail" })
public class ContratTravail extends EntiteImportableBatch {
    /**
     * UID de version.
     */
    private static final long serialVersionUID = 822362569368101808L;

    /**
     * Indicateur de ligne temporaire en cours d'import via Batch.
     * 
     * @param ligneEnCoursImportBatch
     *            le nom du SI d'où provient l'information
     * @return le nom du SI d'où provient l'information
     */
    @Id
    @Column(name = "TMP_BATCH")
    private boolean ligneEnCoursImportBatch;

    /**
     * Identifiant du contrat de travail.
     *
     * @param idContratTravail
     *            L'identifiant du contrat de travail
     * @return L'identifiant du contrat de travail
     */
    @Id
    @Column(name = "ID_CONTRAT_TRAVAIL")
    private String idContratTravail;

    /**
     * Identifiant de l'individu.
     *
     * @param idIndividu
     *            L'identifiant de l'individu
     * @return L'identifiant de l'individu
     */
    @Column(name = "ID_INDIVIDU")
    private String idIndividu;

    /**
     * L'individu.
     *
     * @param individu
     *            L'individu
     * @return L'individu
     */
    @ManyToOne
    @Fetch(FetchMode.JOIN)
    @JoinColumns({ @JoinColumn(name = "ID_INDIVIDU", insertable = false, updatable = false),
            @JoinColumn(name = "TMP_BATCH", insertable = false, updatable = false) })
    private Individu individu;

    /**
     * S21.G00.40.001
     *
     * @param dateDebutContrat
     *            S21.G00.40.001
     * @return S21.G00.40.001
     */
    @Column(name = "DATE_DEBUT_CONTRAT")
    private Integer dateDebutContrat;

    /**
     * S21.G00.40.002
     *
     * @param statutConventionnel
     *            S21.G00.40.002
     * @return S21.G00.40.002
     */
    @Column(name = "STATUT_CONVENTIONNEL")
    private String statutConventionnel;

    /**
     * S21.G00.40.003
     *
     * @param codeStatutRC
     *            S21.G00.40.003
     * @return S21.G00.40.003
     */
    @Column(name = "CODE_STATUT_RC")
    private String codeStatutRC;

    /**
     * S21.G00.40.004
     *
     * @param codeProfession
     *            S21.G00.40.004
     * @return S21.G00.40.004
     */
    @Column(name = "CODE_PROFESSION")
    private String codeProfession;

    /**
     * S21.G00.40.005
     *
     * @param complementProfession
     *            S21.G00.40.005
     * @return S21.G00.40.005
     */
    @Column(name = "COMPLEMENT_PROFESSION")
    private String complementProfession;

    /**
     * S21.G00.40.006
     *
     * @param libelleEmploi
     *            S21.G00.40.006
     * @return S21.G00.40.006
     */
    @Column(name = "LIBELLE_EMPLOI")
    private String libelleEmploi;

    /**
     * S21.G00.40.007
     *
     * @param natureContrat
     *            S21.G00.40.007
     * @return S21.G00.40.007
     */
    @Column(name = "NATURE_CONTRAT")
    private String natureContrat;

    /**
     * S21.G00.40.008
     *
     * @param dispositifPolitique
     *            S21.G00.40.008
     * @return S21.G00.40.008
     */
    @Column(name = "DISPOSITIF_POLITIQUE")
    private String dispositifPolitique;

    /**
     * S21.G00.40.009
     *
     * @param numeroContratTravail
     *            S21.G00.40.009
     * @return S21.G00.40.009
     */
    @Column(name = "NUMERO_CONTRAT")
    private String numeroContratTravail;

    /**
     * S21.G00.40.010
     *
     * @param dateFinPrevisionnelle
     *            S21.G00.40.010
     * @return S21.G00.40.010
     */
    @Column(name = "DATE_FIN_PREVISIONNELLE")
    private Integer dateFinPrevisionnelle;

    /**
     * S21.G00.40.011
     *
     * @param uniteMesure
     *            S21.G00.40.011
     * @return S21.G00.40.011
     */
    @Column(name = "UNITE_MESURE")
    private String uniteMesure;

    /**
     * S21.G00.40.012
     *
     * @param quotiteCategorie
     *            S21.G00.40.012
     * @return S21.G00.40.012
     */
    @Column(name = "QUOTITE_CATEGORIE")
    private Double quotiteCategorie;

    /**
     * S21.G00.40.013
     *
     * @param quotite
     *            S21.G00.40.013
     * @return S21.G00.40.013
     */
    @Column(name = "QUOTITE")
    private Double quotite;

    /**
     * S21.G00.40.014
     *
     * @param modaliteTemps
     *            S21.G00.40.014
     * @return S21.G00.40.014
     */
    @Column(name = "MODALITE_TEMPS")
    private String modaliteTemps;

    /**
     * S21.G00.40.016
     *
     * @param regimeLocal
     *            S21.G00.40.016
     * @return S21.G00.40.016
     */
    @Column(name = "REGIME_LOCAL")
    private String regimeLocal;

    /**
     * S21.G00.40.017
     *
     * @param codeConventionCollective
     *            S21.G00.40.017
     * @return S21.G00.40.017
     */
    @Column(name = "CODE_CONVENTION_COLLECTIVE")
    private String codeConventionCollective;

    /**
     * S21.G00.40.018
     *
     * @param regimeMaladie
     *            S21.G00.40.018
     * @return S21.G00.40.018
     */
    @Column(name = "REGIME_MALADIE")
    private String regimeMaladie;

    /**
     * S21.G00.40.019
     *
     * @param lieutravail
     *            S21.G00.40.019
     * @return S21.G00.40.019
     */
    @Column(name = "LIEU_TRAVAIL")
    private String lieutravail;

    /**
     * S21.G00.40.020
     *
     * @param regimeVieillesse
     *            S21.G00.40.020
     * @return S21.G00.40.020
     */
    @Column(name = "REGIME_VIEILLESSE")
    private String regimeVieillesse;

    /**
     * S21.G00.40.021
     *
     * @param motifRecours
     *            S21.G00.40.021
     * @return S21.G00.40.021
     */
    @Column(name = "MOTIF_RECOURS")
    private String motifRecours;

    /**
     * S21.G00.40.023
     *
     * @param tauxFraisProfessionnels
     *            S21.G00.40.023
     * @return S21.G00.40.023
     */
    @Column(name = "TAUX_FRAIS_PROFESSIONNELS")
    private Double tauxFraisProfessionnels;

    /**
     * S21.G00.40.024
     *
     * @param detacheExpatrie
     *            S21.G00.40.024
     * @return S21.G00.40.024
     */
    @Column(name = "DETACHE_EXPATRIE")
    private String detacheExpatrie;

    /**
     * S21.G00.40.025
     *
     * @param motifExclusionDSN
     *            S21.G00.40.025
     * @return S21.G00.40.025
     */
    @Column(name = "MOTIF_EXCLUSION_DSN")
    private String motifExclusionDSN;

    /**
     * S21.G00.40.026
     *
     * @param statutEmploi
     *            S21.G00.40.026
     * @return S21.G00.40.026
     */
    @Column(name = "STATUT_EMPLOI")
    private String statutEmploi;

    /**
     * S21.G00.40.035
     *
     * @param codeGestionnaireRisque
     *            S21.G00.40.035
     * @return S21.G00.40.035
     */
    @Column(name = "CODE_GESTIONNAIRE_RISQUE")
    private String codeGestionnaireRisque;

    /**
     * S21.G00.40.036
     *
     * @param codeEmploisMultiples
     *            S21.G00.40.036
     * @return S21.G00.40.036
     */
    @Column(name = "CODE_EMPLOIS_MULTIPLES")
    private String codeEmploisMultiples;

    /**
     * S21.G00.40.037
     *
     * @param codeEmployeursMultiples
     *            S21.G00.40.037
     * @return S21.G00.40.037
     */
    @Column(name = "CODE_EMPLOYEURS_MULTIPLES")
    private String codeEmployeursMultiples;

    /**
     * S21.G00.40.041
     *
     * @param positionnementConventionCollective
     *            S21.G00.40.041
     * @return S21.G00.40.041
     */
    @Column(name = "POSITION_CONVENTION_COLLECTIVE")
    private String positionnementConventionCollective;

    /**
     * S21.G00.40.042
     *
     * @param codeStatutCategorieI
     *            S21.G00.40.042
     * @return S21.G00.40.042
     */
    @Column(name = "CODE_STATUT_CATEGORIEL")
    private String codeStatutCategorieI;

    /**
     * S21.G00.40.043
     *
     * @param tauxAccidentTravail
     *            S21.G00.40.043
     * @return S21.G00.40.043
     */
    @Column(name = "TAUX_ACCIDENT_TRAVAIL")
    private Double tauxAccidentTravail;

    /**
     * S21.G00.40.044
     *
     * @param salarieTempsPartielCotisantTempsPlein
     *            S21.G00.40.044
     * @return S21.G00.40.044
     */
    @Column(name = "TPS_PARTIEL_COTIS_PLEIN")
    private String salarieTempsPartielCotisantTempsPlein;

    /**
     * S21.G00.40.045
     *
     * @param remunerationPourboire
     *            S21.G00.40.045
     * @return S21.G00.40.045
     */
    @Column(name = "REMUNERATION_POURBOIRE")
    private String remunerationPourboire;

    /**
     * S21.G00.40.046
     *
     * @param siretEtablissementUtilisateur
     *            S21.G00.40.046
     * @return S21.G00.40.046
     */
    @Column(name = "SIRET_ETABLISSEMENT")
    private String siretEtablissementUtilisateur;

    /**
     * S21.G00.40.400
     *
     * @param enseigneEtablissement
     *            S21.G00.40.400
     * @return S21.G00.40.400
     */
    @Column(name = "ENSEIGNE_ETABLISSEMENT")
    private String enseigneEtablissement;

    /**
     * S21.G00.62.001
     *
     * @param dateFinContrat
     *            S21.G00.62.001
     * @return S21.G00.62.001
     */
    @Column(name = "DATE_FIN_CONTRAT")
    private Integer dateFinContrat;

    /**
     * S21.G00.62.002
     *
     * @param motifRupture
     *            S21.G00.62.002
     * @return S21.G00.62.002
     */
    @Column(name = "MOTIF_RUPTURE")
    private String motifRupture;

    /**
     * Quotite de catégorie (telle que lue dans le fichier).
     *
     */
    @Transient
    private String quotiteCategorieAsText;

    /**
     * Quotite (telle que lue dans le fichier).
     *
     */
    @Transient
    private String quotiteAsText;

    /**
     * Taux lié aux frais professionnels (tel que lu dans le fichier).
     *
     */
    @Transient
    private String tauxFraisProfessionnelsAsText;

    /**
     * Taux lié à un accident de travail (tel que lu dans le fichier).
     *
     */
    @Transient
    private String tauxAccidentTravailAsText;

    /**
     * Date de début de contrat (telle que lue dans le fichier).
     *
     */
    @Transient
    private String dateDebutContratAsText;

    /**
     * Date de fin prévisionnelle (telle que lue dans le fichier).
     *
     */
    @Transient
    private String dateFinPrevisionnelleAsText;

    /**
     * Date de fin de contrat (telle que lue dans le fichier).
     *
     */
    @Transient
    private String dateFinContratAsText;

    /**
     * Identifiant de la DSN dans la brique
     */
    @Column(name = "ID_DSN_BRIQUE")
    private BigInteger idDsnBrique;

    /**
     * Clé primaire de la table dans la brique
     */
    @Column(name = "PK_ENTITEE_BRIQUE")
    private BigInteger cleEntiteeBrique;

    /**
     * Renvoie la quotité de catégorie (telle que lue dans le fichier).
     * 
     * @return la quotité de catégorie
     */
    public String getQuotiteCategorieAsText() {
        String value = this.quotiteCategorieAsText;
        if (StringUtils.isBlank(value) && getQuotiteCategorie() != null) {
            value = new ConvertisseurNumeriqueDecimal().convertToEntityAttribute(getQuotiteCategorie());
        }
        return value;
    }

    /**
     * Valorise la quotité de catégorie (telle que lue dans le fichier).
     * 
     * @param quotiteCategorie
     *            la quotité de catégorie à valoriser
     */
    public void setQuotiteCategorieAsText(String quotiteCategorie) {
        this.quotiteCategorieAsText = quotiteCategorie;
        setQuotiteCategorie(new ConvertisseurNumeriqueDecimal().convertToDatabaseColumn(quotiteCategorie));
    }

    /**
     * Renvoie la quotité (telle que lue dans le fichier).
     * 
     * @return la quotité
     */
    public String getQuotiteAsText() {
        String value = this.quotiteAsText;
        if (StringUtils.isBlank(value) && getQuotite() != null) {
            value = new ConvertisseurNumeriqueDecimal().convertToEntityAttribute(getQuotite());
        }
        return value;
    }

    /**
     * Valorise la quotité (telle que lue dans le fichier).
     * 
     * @param quotite
     *            la quotité à valoriser
     */
    public void setQuotiteAsText(String quotite) {
        this.quotiteAsText = quotite;
        setQuotite(new ConvertisseurNumeriqueDecimal().convertToDatabaseColumn(quotite));
    }

    /**
     * Renvoie le taux lié aux frais professionnels (tel que lu dans le fichier).
     * 
     * @return le taux lié aux frais professionnels
     */
    public String getTauxFraisProfessionnelsAsText() {
        String value = this.tauxFraisProfessionnelsAsText;
        if (StringUtils.isBlank(value) && getTauxFraisProfessionnels() != null) {
            value = new ConvertisseurNumeriqueDecimal().convertToEntityAttribute(getTauxFraisProfessionnels());
        }
        return value;
    }

    /**
     * Valorise le taux lié aux frais professionnels (tel que lu dans le fichier).
     * 
     * @param tauxFraisProfessionnels
     *            le taux lié aux frais professionnels à valoriser
     */
    public void setTauxFraisProfessionnelsAsText(String tauxFraisProfessionnels) {
        this.tauxFraisProfessionnelsAsText = tauxFraisProfessionnels;
        setTauxFraisProfessionnels(new ConvertisseurNumeriqueDecimal().convertToDatabaseColumn(tauxFraisProfessionnels));
    }

    /**
     * Renvoie le taux lié à un accident de travail (tel que lu dans le fichier).
     * 
     * @return le taux lié à un accident de travail
     */
    public String getTauxAccidentTravailAsText() {
        String value = this.tauxAccidentTravailAsText;
        if (StringUtils.isBlank(value) && getTauxAccidentTravail() != null) {
            value = new ConvertisseurNumeriqueDecimal().convertToEntityAttribute(getTauxAccidentTravail());
        }
        return value;
    }

    /**
     * Valorise le taux lié à un accident de travail (tel que lu dans le fichier).
     * 
     * @param tauxAccidentTravail
     *            le taux lié à un accident de travail à valoriser
     */
    public void setTauxAccidentTravailAsText(String tauxAccidentTravail) {
        this.tauxAccidentTravailAsText = tauxAccidentTravail;
        setTauxAccidentTravail(new ConvertisseurNumeriqueDecimal().convertToDatabaseColumn(tauxAccidentTravail));
    }

    /**
     * Date de début de contrat (telle que lue dans le fichier).
     * 
     * @return la date de début de contrat
     */
    public String getDateDebutContratAsText() {
        if (dateDebutContratAsText != null) {
            return dateDebutContratAsText;
        }
        return new ConvertisseurDatesDDMMYYYY().convertToEntityAttribute(getDateDebutContrat());
    }

    /**
     * Valorise la date de début de contrat (telle que lue dans le fichier).
     * 
     * @param dateDebutContrat
     *            la date de début de contrat à valoriser
     */
    public void setDateDebutContratAsText(String dateDebutContrat) {
        this.dateDebutContratAsText = dateDebutContrat;
        setDateDebutContrat(new ConvertisseurDatesDDMMYYYY().convertToDatabaseColumn(dateDebutContrat));
    }

    /**
     * Date de fin prévisionnelle (telle que lue dans le fichier).
     * 
     * @return la date de fin prévisionnelle
     */
    public String getDateFinPrevisionnelleAsText() {
        if (dateFinPrevisionnelleAsText != null) {
            return dateFinPrevisionnelleAsText;
        }
        return new ConvertisseurDatesDDMMYYYY().convertToEntityAttribute(getDateFinPrevisionnelle());
    }

    /**
     * Valorise la date de fin prévisionnelle (telle que lue dans le fichier).
     * 
     * @param dateFinPrevisionnelle
     *            la date de fin prévisionnelle à valoriser
     */
    public void setDateFinPrevisionnelleAsText(String dateFinPrevisionnelle) {
        this.dateFinPrevisionnelleAsText = dateFinPrevisionnelle;
        setDateFinPrevisionnelle(new ConvertisseurDatesDDMMYYYY().convertToDatabaseColumn(dateFinPrevisionnelle));
    }

    /**
     * Date de fin de contrat (telle que lue dans le fichier).
     * 
     * @return la date de fin de contrat
     */
    public String getDateFinContratAsText() {
        if (dateFinContratAsText != null) {
            return dateFinContratAsText;
        }
        return new ConvertisseurDatesDDMMYYYY().convertToEntityAttribute(getDateFinContrat());
    }

    /**
     * Valorise la date de fin de contrat (telle que lue dans le fichier).
     * 
     * @param dateFinContrat
     *            la date de fin de contrat à valoriser
     */
    public void setDateFinContratAsText(String dateFinContrat) {
        this.dateFinContratAsText = dateFinContrat;
        setDateFinContrat(new ConvertisseurDatesDDMMYYYY().convertToDatabaseColumn(dateFinContrat));
    }

    @Override
    public ContratTravailId getId() {
        return new ContratTravailId(ligneEnCoursImportBatch, idContratTravail);
    }

}
