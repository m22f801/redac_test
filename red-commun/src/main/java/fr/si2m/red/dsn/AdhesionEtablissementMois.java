package fr.si2m.red.dsn;

import java.math.BigInteger;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.si2m.red.EntiteImportableBatch;
import fr.si2m.red.core.repository.jpa.conversion.ConvertisseurDatesDDMMYYYY;
import fr.si2m.red.core.repository.jpa.conversion.ConvertisseurDatesYYYYMM;
import fr.si2m.red.core.repository.jpa.conversion.ConvertisseurNumeriqueInteger;
import fr.si2m.red.core.repository.jpa.conversion.ConvertisseurNumeriqueLong;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * Modèle des adhésions établissement mois
 * 
 * @author poidij
 *
 */
@Entity
@Table(name = "ADHESION_ETABLISSEMENT_MOIS")
@IdClass(AdhesionEtablissementMoisId.class)
@Data
@EqualsAndHashCode(callSuper = false, of = { "ligneEnCoursImportBatch", "idAdhEtabMois" })
@ToString(callSuper = false, of = { "ligneEnCoursImportBatch", "idAdhEtabMois" })
public class AdhesionEtablissementMois extends EntiteImportableBatch {

    private static final Logger LOGGER = LoggerFactory.getLogger(AdhesionEtablissementMois.class);

    /**
     * UID de version.
     */
    private static final long serialVersionUID = 822362569368101808L;

    /**
     * Indicateur de ligne temporaire en cours d'import via Batch.
     * 
     * @param ligneEnCoursImportBatch
     *            le nom du SI d'où provient l'information
     * @return le nom du SI d'où provient l'information
     */
    @Id
    @Column(name = "TMP_BATCH")
    private boolean ligneEnCoursImportBatch;

    /**
     * Identifiant technique unique pour chaque ligne de l’ensemble des différents fichiers publiés ADHESION_ETAB_MOIS
     *
     * @param idAdhEtabMois
     *            Identifiant technique unique pour chaque ligne de l’ensemble des différents fichiers publiés ADHESION_ETAB_MOIS
     * @return Identifiant technique unique pour chaque ligne de l’ensemble des différents fichiers publiés ADHESION_ETAB_MOIS
     */
    @Id
    @Column(name = "ID_ADH_ETAB_MOIS")
    private String idAdhEtabMois;

    /**
     * S10.G00.00.005 Code envoi du fichier d’essai ou réel
     *
     * @param codeEssaiReel
     *            S10.G00.00.005 Code envoi du fichier d’essai ou réel
     * @return S10.G00.00.005 Code envoi du fichier d’essai ou réel
     */
    @Column(name = "CODE_ESSAI_REEL")
    private String codeEssaiReel;

    /**
     * S10.G00.00.008 Type de l’envoi
     *
     * @param typeEnvoi
     *            S10.G00.00.008 Type de l’envoi
     * @return S10.G00.00.008 Type de l’envoi
     */
    @Column(name = "TYPE_ENVOI")
    private String typeEnvoi;

    /**
     * S10.G00.01.001 Siren de l'émetteur de l'envoi
     *
     * @param sirenEmetteur
     *            S10.G00.01.001 Siren de l'émetteur de l'envoi
     * @return S10.G00.01.001 Siren de l'émetteur de l'envoi
     */
    @Column(name = "SIREN_EMETTEUR")
    private String sirenEmetteur;

    /**
     * S10.G00.01.002 NIC de l'émetteur de l'envoi
     *
     * @param nicEmetteur
     *            S10.G00.01.002 NIC de l'émetteur de l'envoi
     * @return S10.G00.01.002 NIC de l'émetteur de l'envoi
     */
    @Column(name = "NIC_EMETEUR")
    private String nicEmetteur;

    /**
     * S10.G00.01.003 Nom ou raison sociale de l'émetteur
     *
     * @param nomEmetteur
     *            S10.G00.01.003 Nom ou raison sociale de l'émetteur
     * @return S10.G00.01.003 Nom ou raison sociale de l'émetteur
     */
    @Column(name = "NOM_EMETTEUR")
    private String nomEmetteur;

    /**
     * S10.G00.01.004 Numéro, extension, nature et libellé de la voie
     *
     * @param voieEmetteur
     *            S10.G00.01.004 Numéro, extension, nature et libellé de la voie
     * @return S10.G00.01.004 Numéro, extension, nature et libellé de la voie
     */
    @Column(name = "VOIE_EMETTEUR")
    private String voieEmetteur;

    /**
     * S10.G00.01.005 Code postal
     *
     * @param codePostalEmetteur
     *            S10.G00.01.005 Code postal
     * @return S10.G00.01.005 Code postal
     */
    @Column(name = "CODE_POSTAL_EMETTEUR")
    private String codePostalEmetteur;

    /**
     * S10.G00.01.006 Localité
     *
     * @param localiteEmetteur
     *            S10.G00.01.006 Localité
     * @return S10.G00.01.006 Localité
     */
    @Column(name = "LOCALITE_EMETTEUR")
    private String localiteEmetteur;

    /**
     * S10.G00.01.007 Code pays
     *
     * @param paysEmetteur
     *            S10.G00.01.007 Code pays
     * @return S10.G00.01.007 Code pays
     */
    @Column(name = "PAYS_EMETTEUR")
    private String paysEmetteur;

    /**
     * S10.G00.01.008 Code de distribution à l'étranger
     *
     * @param codeDistributionEmetteur
     *            S10.G00.01.008 Code de distribution à l'étranger
     * @return S10.G00.01.008 Code de distribution à l'étranger
     */
    @Column(name = "CODE_DISTRIB_EMETTEUR")
    private String codeDistributionEmetteur;

    /**
     * S10.G00.01.009 Complément de la localisation de la construction
     *
     * @param complementConstructionEmetteur
     *            S10.G00.01.009 Complément de la localisation de la construction
     * @return S10.G00.01.009 Complément de la localisation de la construction
     */
    @Column(name = "COMPLEMENT_CONSTRUCTION_EMETTEUR")
    private String complementConstructionEmetteur;

    /**
     * S10.G00.01.010 Service de distribution, complément de localisation de la voie
     *
     * @param complementVoieEmetteur
     *            S10.G00.01.010 Service de distribution, complément de localisation de la voie
     * @return S10.G00.01.010 Service de distribution, complément de localisation de la voie
     */
    @Column(name = "COMPLEMENT_VOIE_EMETTEUR")
    private String complementVoieEmetteur;

    /**
     * S10.G00.02.001 Code civilité
     *
     * @param civiliteContact
     *            S10.G00.02.001 Code civilité
     * @return S10.G00.02.001 Code civilité
     */
    @Column(name = "CIVILITE_CONTACT")
    private String civiliteContact;

    /**
     * S10.G00.02.002 Nom et prénom de la personne à contacter
     *
     * @param nomContact
     *            S10.G00.02.002 Nom et prénom de la personne à contacter
     * @return S10.G00.02.002 Nom et prénom de la personne à contacter
     */
    @Column(name = "NOM_CONTACT")
    private String nomContact;

    /**
     * S10.G00.02.004 Adresse mél du contact émetteur
     *
     * @param mailContact
     *            S10.G00.02.004 Adresse mél du contact émetteur
     * @return S10.G00.02.004 Adresse mél du contact émetteur
     */
    @Column(name = "MAIL_CONTACT")
    private String mailContact;

    /**
     * S10.G00.02.005 Adresse téléphonique
     *
     * @param telContact
     *            S10.G00.02.005 Adresse téléphonique
     * @return S10.G00.02.005 Adresse téléphonique
     */
    @Column(name = "TEL_CONTACT")
    private String telContact;

    /**
     * S10.G00.02.006 Adresse fax
     *
     * @param faxContact
     *            S10.G00.02.006 Adresse fax
     * @return S10.G00.02.006 Adresse fax
     */
    @Column(name = "FAX_CONTACT")
    private String faxContact;

    /**
     * S20.G00.05.002 Type de la déclaration (uniquement 01, 02, 03 ou 05)
     *
     * @param typeDeclaration
     *            S20.G00.05.002 Type de la déclaration (uniquement 01, 02, 03 ou 05)
     * @return S20.G00.05.002 Type de la déclaration (uniquement 01, 02, 03 ou 05)
     */
    @Column(name = "TYPE_DECLARATION")
    private String typeDeclaration;

    /**
     * S20.G00.05.004 Numéro d'ordre de la déclaration
     *
     * @param numeroOrdreDeclaration
     *            S20.G00.05.004 Numéro d'ordre de la déclaration
     * @return S20.G00.05.004 Numéro d'ordre de la déclaration
     */
    @Column(name = "NUMERO_ORDRE_DECLARATION")
    private Long numeroOrdreDeclaration;

    /**
     * S20.G00.05.005 Date du mois principal déclaré
     *
     * @param moisDeclare
     *            S20.G00.05.005 Date du mois principal déclaré
     * @return S20.G00.05.005 Date du mois principal déclaré
     */
    @Column(name = "MOIS_DECLARE")
    private Integer moisDeclare;

    /**
     * Date du mois de rattachement
     *
     * @param moisRattachement
     *            Date du mois de rattachement
     * @return Date du mois de rattachement
     */
    @Column(name = "MOIS_RATTACHEMENT")
    private Integer moisRattachement;

    /**
     * S20.G00.05.007 Date de constitution du fichier
     *
     * @param dateFichier
     *            S20.G00.05.007 Date de constitution du fichier
     * @return S20.G00.05.007 Date de constitution du fichier
     */
    @Column(name = "DATE_FICHIER")
    private Integer dateFichier;

    /**
     * S20.G00.05.008 Champ de la déclaration
     *
     * @param champDeclaration
     *            S20.G00.05.008 Champ de la déclaration
     * @return S20.G00.05.008 Champ de la déclaration
     */
    @Column(name = "CHAMP_DECLARATION")
    private String champDeclaration;

    /**
     * S20.G00.05.009 Identifiant de l'évènement (P1) ==> Identifiant métier (P2/P3)
     *
     * @param identifiantMetier
     *            S20.G00.05.009 Identifiant de l'évènement (P1) ==> Identifiant métier (P2/P3)
     * @return S20.G00.05.009 Identifiant de l'évènement (P1) ==> Identifiant métier (P2/P3)
     */
    @Column(name = "IDENTIFIANT_METIER")
    private String identifiantMetier;

    /**
     * S20.G00.05.010 Devise du paiement
     *
     * @param deviseDeclaration
     *            S20.G00.05.010 Devise du paiement
     * @return S20.G00.05.010 Devise du paiement
     */
    @Column(name = "DEVISE_DECLARATION")
    private String deviseDeclaration;

    /**
     * S21.G00.06.001 SIREN
     *
     * @param sirenEntreprise
     *            S21.G00.06.001 SIREN
     * @return S21.G00.06.001 SIREN
     */
    @Column(name = "SIREN_ENTREPRISE")
    private String sirenEntreprise;

    /**
     * S21.G00.06.002 NIC du siège
     *
     * @param nicEntreprise
     *            S21.G00.06.002 NIC du siège
     * @return S21.G00.06.002 NIC du siège
     */
    @Column(name = "NIC_ENTREPRISE")
    private String nicEntreprise;

    /**
     * S21.G00.06.003 Code APEN
     *
     * @param codeAPEN
     *            S21.G00.06.003 Code APEN
     * @return S21.G00.06.003 Code APEN
     */
    @Column(name = "CODE_APEN")
    private String codeAPEN;

    /**
     * S21.G00.06.004 Numéro, extension, nature et libellé de la voie
     *
     * @param voieEntreprise
     *            S21.G00.06.004 Numéro, extension, nature et libellé de la voie
     * @return S21.G00.06.004 Numéro, extension, nature et libellé de la voie
     */
    @Column(name = "VOIE_ENTREPRISE")
    private String voieEntreprise;

    /**
     * S21.G00.06.005 Code postal
     *
     * @param codePostalEntreprise
     *            S21.G00.06.005 Code postal
     * @return S21.G00.06.005 Code postal
     */
    @Column(name = "CODE_POSTAL_ENTREPRISE")
    private String codePostalEntreprise;

    /**
     * S21.G00.06.006 Localité
     *
     * @param localiteEntreprise
     *            S21.G00.06.006 Localité
     * @return S21.G00.06.006 Localité
     */
    @Column(name = "LOCALITE_ENTREPRISE")
    private String localiteEntreprise;

    /**
     * S21.G00.06.007 Complément de la localisation de la construction
     *
     * @param complementConstructionEntreprise
     *            S21.G00.06.007 Complément de la localisation de la construction
     * @return S21.G00.06.007 Complément de la localisation de la construction
     */
    @Column(name = "COMPLEMENT_CONSTRUCTION_ENTREPRISE")
    private String complementConstructionEntreprise;

    /**
     * S21.G00.06.008 Service de distribution, complément de localisation de la voie
     *
     * @param complementVoieEntreprise
     *            S21.G00.06.008 Service de distribution, complément de localisation de la voie
     * @return S21.G00.06.008 Service de distribution, complément de localisation de la voie
     */
    @Column(name = "COMPLEMENT_VOIE_ENTREPRISE")
    private String complementVoieEntreprise;

    /**
     * S21.G00.06.009 Effectif moyen de l'entreprise au 31 décembre
     *
     * @param effectifMoyen
     *            S21.G00.06.009 Effectif moyen de l'entreprise au 31 décembre
     * @return S21.G00.06.009 Effectif moyen de l'entreprise au 31 décembre
     */
    @Column(name = "EFFECTIF_MOYEN_ENTREPRISE")
    private Integer effectifMoyen;

    /**
     * S21.G00.06.010 Code pays
     *
     * @param paysEntreprise
     *            S21.G00.06.010 Code pays
     * @return S21.G00.06.010 Code pays
     */
    @Column(name = "PAYS_ENTREPRISE")
    private String paysEntreprise;

    /**
     * S21.G00.06.011 Code de distribution à l'étranger
     *
     * @param codeDistributionEntreprise
     *            S21.G00.06.011 Code de distribution à l'étranger
     * @return S21.G00.06.011 Code de distribution à l'étranger
     */
    @Column(name = "CODE_DISTRIB_ENTREPRISE")
    private String codeDistributionEntreprise;

    /**
     * S21.G00.06.012 Implantation de l'entreprise
     *
     * @param implantationEntreprise
     *            S21.G00.06.012 Implantation de l'entreprise
     * @return S21.G00.06.012 Implantation de l'entreprise
     */
    @Column(name = "IMPLANTATION_ENTREPRISE")
    private String implantationEntreprise;

    /**
     * S21.G00.06.903 Raison sociale de l'entreprise
     *
     * @param raisonSociale
     *            S21.G00.06.903 Raison sociale de l'entreprise
     * @return S21.G00.06.903 Raison sociale de l'entreprise
     */
    @Column(name = "RAISON_SOCIALE_ENTREPRISE")
    private String raisonSociale;

    /**
     * S21.G00.11.001 NIC
     *
     * @param nicEtablissement
     *            S21.G00.11.001 NIC
     * @return S21.G00.11.001 NIC
     */
    @Column(name = "NIC_ETABLISSEMENT")
    private String nicEtablissement;

    /**
     * S21.G00.11.002 Code APET
     *
     * @param codeAPET
     *            S21.G00.11.002 Code APET
     * @return S21.G00.11.002 Code APET
     */
    @Column(name = "CODE_APET")
    private String codeAPET;

    /**
     * S21.G00.11.003 Numéro, extension, nature et libellé de la voie
     *
     * @param voieEtablissement
     *            S21.G00.11.003 Numéro, extension, nature et libellé de la voie
     * @return S21.G00.11.003 Numéro, extension, nature et libellé de la voie
     */
    @Column(name = "VOIE_ETABLISSEMENT")
    private String voieEtablissement;

    /**
     * S21.G00.11.004 Code postal
     *
     * @param codePostalEtablissement
     *            S21.G00.11.004 Code postal
     * @return S21.G00.11.004 Code postal
     */
    @Column(name = "CODE_POSTAL_ETABLISSEMENT")
    private String codePostalEtablissement;

    /**
     * S21.G00.11.005 Localité
     *
     * @param localiteEtablissement
     *            S21.G00.11.005 Localité
     * @return S21.G00.11.005 Localité
     */
    @Column(name = "LOCALITE_ETABLISSEMENT")
    private String localiteEtablissement;

    /**
     * S21.G00.11.006 Complément de la localisation de la construction
     *
     * @param complementConstructionEtablissement
     *            S21.G00.11.006 Complément de la localisation de la construction
     * @return S21.G00.11.006 Complément de la localisation de la construction
     */
    @Column(name = "COMPLEMENT_CONSTRUCTION_ETABLISSEMENT")
    private String complementConstructionEtablissement;

    /**
     * S21.G00.11.007 Service de distribution, complément de localisation de la voie
     *
     * @param complementVoieEtablissement
     *            S21.G00.11.007 Service de distribution, complément de localisation de la voie
     * @return S21.G00.11.007 Service de distribution, complément de localisation de la voie
     */
    @Column(name = "COMPLEMENT_VOIE_ETABLISSEMENT")
    private String complementVoieEtablissement;

    /**
     * S21.G00.11.008 Effectif de fin de période déclarée de l'établissement
     *
     * @param effectifFinPeriode
     *            S21.G00.11.008 Effectif de fin de période déclarée de l'établissement
     * @return S21.G00.11.008 Effectif de fin de période déclarée de l'établissement
     */
    @Column(name = "EFFECTIF_FIN_PERIODE_ETABLISSEMENT")
    private Integer effectifFinPeriode;

    /**
     * S21.G00.11.015 Code pays
     *
     * @param paysEtablissement
     *            S21.G00.11.015 Code pays
     * @return S21.G00.11.015 Code pays
     */
    @Column(name = "PAYS_ETABLISSEMENT")
    private String paysEtablissement;

    /**
     * S21.G00.11.016 Code de distribution à l'étranger
     *
     * @param codeDistributionEtablissement
     *            S21.G00.11.016 Code de distribution à l'étranger
     * @return S21.G00.11.016 Code de distribution à l'étranger
     */
    @Column(name = "CODE_DISTRIB_ETABLISSEMENT")
    private String codeDistributionEtablissement;

    /**
     * S21.G00.11.017 Nature juridique de l'employeur
     *
     * @param natureJuridique
     *            S21.G00.11.017 Nature juridique de l'employeur
     * @return S21.G00.11.017 Nature juridique de l'employeur
     */
    @Column(name = "NATURE_JURIDIQUE_ETABLISSEMENT")
    private String natureJuridique;

    /**
     * S21.G00.11.110 Code INSEE Commune de l’établissement d’affectation
     *
     * @param codeINSEE
     *            S21.G00.11.110 Code INSEE Commune de l’établissement d’affectation
     * @return S21.G00.11.110 Code INSEE Commune de l’établissement d’affectation
     */
    @Column(name = "CODE_INSEE")
    private String codeINSEE;

    /**
     * S21.G00.11.111 Date d'échéance appliquée à l'établissement
     *
     * @param dateEcheance
     *            S21.G00.11.111 Date d'échéance appliquée à l'établissement
     * @return S21.G00.11.111 Date d'échéance appliquée à l'établissement
     */
    @Column(name = "DATE_ECHEANCE")
    private Integer dateEcheance;

    /**
     * S21.G00.11.112 Catégorie juridique (niveau III) de l’établissement d’affectation
     *
     * @param categorieJuridique
     *            S21.G00.11.112 Catégorie juridique (niveau III) de l’établissement d’affectation
     * @return S21.G00.11.112 Catégorie juridique (niveau III) de l’établissement d’affectation
     */
    @Column(name = "CATEGORIE_JURIDIQUE")
    private String categorieJuridique;

    /**
     * S21.G00.11.904 Enseigne de l'établissement d'affectation
     *
     * @param enseigneEtablissement
     *            S21.G00.11.904 Enseigne de l'établissement d'affectation
     * @return S21.G00.11.904 Enseigne de l'établissement d'affectation
     */
    @Column(name = "ENSEIGNE_ETABLISSEMENT")
    private String enseigneEtablissement;

    /**
     * S21.G00.15.001 Référence du contrat de Prévoyance, contrôlé et éventuellement recyclé ou transcodé par la Brique DSN (notamment pour les contrats de
     * délégataires de cotisation).
     *
     * @param referenceContrat
     *            S21.G00.15.001 Référence du contrat de Prévoyance, contrôlé et éventuellement recyclé ou transcodé par la Brique DSN (notamment pour les
     *            contrats de délégataires de cotisation).
     * @return S21.G00.15.001 Référence du contrat de Prévoyance, contrôlé et éventuellement recyclé ou transcodé par la Brique DSN (notamment pour les contrats
     *         de délégataires de cotisation).
     */
    @Column(name = "REFERENCE_CONTRAT")
    private String referenceContrat;

    /**
     * S21.G00.15.002 Code organisme de Prévoyance
     *
     * @param codeOrganisme
     *            S21.G00.15.002 Code organisme de Prévoyance
     * @return S21.G00.15.002 Code organisme de Prévoyance
     */
    @Column(name = "CODE_ORGANISME")
    private String codeOrganisme;

    /**
     * Code délégataire de gestion transcodé à la valeur du SI aval, ou vide si aucun (issu de S21.G00.15.003).
     *
     * @param codeDelegataireGestion
     *            Code délégataire de gestion transcodé à la valeur du SI aval, ou vide si aucun (issu de S21.G00.15.003).
     * @return Code délégataire de gestion transcodé à la valeur du SI aval, ou vide si aucun (issu de S21.G00.15.003).
     */
    @Column(name = "CODE_DELEG_COT")
    private String codeDelegataireGestion;

    /**
     * S21.G00.15.004 Personnel couvert
     *
     * @param personnelCouvert
     *            S21.G00.15.004 Personnel couvert
     * @return S21.G00.15.004 Personnel couvert
     */
    @Column(name = "PERSONNEL_COUVERT")
    private String personnelCouvert;

    /**
     * Un mois déclaré lu dans un fichier d'import.
     * 
     * @param moisDeclareAsText
     *            un mois déclaré lu dans un fichier d'import
     * 
     * @return un mois déclaré lu dans un fichier d'import
     */
    @Transient
    private String moisDeclareAsText;

    /**
     * Un mois de rattachement lu dans un fichier d'import.
     * 
     * @param moisRattachementAsText
     *            un mois de rattachement lu dans un fichier d'import
     * 
     * @return un mois de rattachement lu dans un fichier d'import
     */
    @Transient
    private String moisRattachementAsText;

    /**
     * Une date de fichier lu dans un fichier d'import.
     * 
     * @param moisDeclareAsText
     *            une date de fichier lu dans un fichier d'import
     * 
     * @return une date de fichier lu dans un fichier d'import
     */
    @Transient
    private String dateFichierAsText;

    /**
     * Une date d'échéance lue dans un fichier d'import.
     *
     * @param dateEcheanceAsText
     *            une date d'échéance lue dans un fichier d'import
     * @return une date d'échéance lue dans un fichier d'import
     */
    @Transient
    private String dateEcheanceAsText;

    /**
     * Un numéro d'ordre de la déclaration lu dans un fichier d'import.
     *
     * @param numeroOrdreDeclarationAsText
     *            numéro d'ordre de la déclaration lu dans un fichier d'import
     * @return numéro d'ordre de la déclaration lu dans un fichier d'import
     */
    @Transient
    private String numeroOrdreDeclarationAsText;

    /**
     * Effectif moyen de l'entreprise au 31 décembre lu dans un fichier d'import.
     *
     * @param effectifMoyenAsText
     *            effectif moyen de l'entreprise au 31 décembre lu dans un fichier d'import
     * @return effectif moyen de l'entreprise au 31 décembre lu dans un fichier d'import
     */
    @Transient
    private String effectifMoyenAsText;

    /**
     * Effectif de fin de période déclarée de l'établissement lu dans un fichier d'import.
     *
     * @param effectifFinPeriodeAsText
     *            effectif de fin de période déclarée de l'établissement lu dans un fichier d'import
     * @return effectif de fin de période déclarée de l'établissement lu dans un fichier d'import
     */
    @Transient
    private String effectifFinPeriodeAsText;

    /**
     * Identifiant de la DSN dans la brique
     */
    @Column(name = "ID_DSN_BRIQUE")
    private BigInteger idDsnBrique;

    /**
     * Identifiant de la DSN annulée
     */
    @Column(name = "ID_DSN_ANNULEE")
    private BigInteger idDsnAnnulee;

    /**
     * Mois déclaré (tel que lu dans un fichier d'import).
     * 
     * @return le mois déclaré
     */
    public String getMoisDeclareAsText() {
        if (moisDeclareAsText != null) {
            return moisDeclareAsText;
        } else {
            return new ConvertisseurDatesDDMMYYYY().convertToEntityAttribute(getMoisDeclare());
        }
    }

    /**
     * Valorise le mois déclaré (tel que lu dans un fichier d'import).
     * 
     * @param moisDeclare
     *            le mois déclaré à valoriser
     */
    public void setMoisDeclareAsText(String moisDeclare) {
        this.moisDeclareAsText = moisDeclare;
        setMoisDeclare(new ConvertisseurDatesDDMMYYYY().convertToDatabaseColumn(moisDeclare));
    }

    /**
     * Mois de rattachement (tel que lu dans un fichier d'import).
     * 
     * @return le mois de rattachement
     */
    public String getMoisRattachementAsText() {
        if (moisRattachementAsText != null) {
            return moisRattachementAsText;
        } else {
            return new ConvertisseurDatesYYYYMM().convertToEntityAttribute(getMoisRattachement());
        }
    }

    /**
     * Valorise le mois de rattachement (tel que lu dans un fichier d'import).
     * 
     * @param moisRattachement
     *            le mois de rattachement à valoriser
     */
    public void setMoisRattachementAsText(String moisRattachement) {
        this.moisRattachementAsText = moisRattachement;
        setMoisRattachement(new ConvertisseurDatesYYYYMM().convertToDatabaseColumn(moisRattachement));
    }

    /**
     * Date d'échéance (telle que lue dans le fichier).
     * 
     * @return la date d'échéance
     */
    public String getDateEcheanceAsText() {
        if (dateEcheanceAsText != null) {
            return dateEcheanceAsText;
        } else {
            return getDateEcheance().toString();
        }
    }

    /**
     * Valorise la date d'échéance (telle que lue dans le fichier).
     * 
     * @param dateEcheance
     *            la date d'échéance à valoriser
     */
    public void setDateEcheanceAsText(String dateEcheance) {
        this.dateEcheanceAsText = dateEcheance;
        Integer result = null;
        try {
            result = Integer.valueOf(dateEcheance);
        } catch (Exception e) {
            LOGGER.warn("La conversion de la valeur '" + dateEcheance + "' en Integer a échoué : ", e);
        }

        setDateEcheance(result);
    }

    /**
     * Date du fichier (telle que lue dans le fichier).
     * 
     * @return la date du fichier
     */
    public String getDateFichierAsText() {
        if (dateFichierAsText != null) {
            return dateFichierAsText;
        } else {
            return new ConvertisseurDatesDDMMYYYY().convertToEntityAttribute(getDateFichier());
        }
    }

    /**
     * Valorise la date du fichier (telle que lue dans le fichier).
     * 
     * @param dateFichier
     *            la date du fichier à valoriser
     */
    public void setDateFichierAsText(String dateFichier) {
        this.dateFichierAsText = dateFichier;
        setDateFichier(new ConvertisseurDatesDDMMYYYY().convertToDatabaseColumn(dateFichier));
    }

    /**
     * Numéro d'ordre de la déclaration (tel que lu dans le fichier).
     * 
     * @return le numéro d'ordre de la déclaration
     */
    public String getNumeroOrdreDeclarationAsText() {
        if (numeroOrdreDeclarationAsText != null) {
            return numeroOrdreDeclarationAsText;
        } else {
            return new ConvertisseurNumeriqueLong().convertToEntityAttribute(getNumeroOrdreDeclaration());
        }
    }

    /**
     * Valorise le numéro d'ordre de la déclaration (tel que lu dans le fichier).
     * 
     * @param numeroOrdreDeclaration
     *            le numéro d'ordre de la déclaration à valoriser
     */
    public void setNumeroOrdreDeclarationAsText(String numeroOrdreDeclaration) {
        this.numeroOrdreDeclarationAsText = numeroOrdreDeclaration;
        setNumeroOrdreDeclaration(new ConvertisseurNumeriqueLong().convertToDatabaseColumn(numeroOrdreDeclaration));
    }

    /**
     * Effectif moyen de l'entreprise au 31 décembre (tel que lu dans le fichier).
     *
     * @return effectif moyen de l'entreprise au 31 décembre
     */
    public String getEffectifMoyenAsText() {
        if (effectifMoyenAsText != null) {
            return effectifMoyenAsText;
        } else {
            return new ConvertisseurNumeriqueInteger().convertToEntityAttribute(getEffectifMoyen());
        }
    }

    /**
     * Valorise l'effectif moyen de l'entreprise au 31 décembre (tel que lu dans le fichier).
     * 
     * @param effectifMoyen
     *            effectif moyen de l'entreprise au 31 décembre à valoriser
     */
    public void setEffectifMoyenAsText(String effectifMoyen) {
        this.effectifMoyenAsText = effectifMoyen;
        setEffectifMoyen(new ConvertisseurNumeriqueInteger().convertToDatabaseColumn(effectifMoyen));
    }

    /**
     * Effectif de fin de période déclarée de l'établissement (tel que lu dans le fichier).
     * 
     * @return l'effectif de fin de période déclarée de l'établissement
     */
    public String getEffectifFinPeriodeAsText() {
        if (effectifFinPeriodeAsText != null) {
            return effectifFinPeriodeAsText;
        } else {
            return new ConvertisseurNumeriqueInteger().convertToEntityAttribute(getEffectifFinPeriode());
        }
    }

    /**
     * Valorise l'effectif de fin de période déclarée de l'établissement (tel que lu dans le fichier).
     * 
     * @param effectifFinPeriode
     *            l'effectif de fin de période déclarée de l'établissement à valoriser
     */
    public void setEffectifFinPeriodeAsText(String effectifFinPeriode) {
        this.effectifFinPeriodeAsText = effectifFinPeriode;
        setEffectifFinPeriode(new ConvertisseurNumeriqueInteger().convertToDatabaseColumn(effectifFinPeriode));
    }

    @Override
    public AdhesionEtablissementMoisId getId() {
        return new AdhesionEtablissementMoisId(ligneEnCoursImportBatch, idAdhEtabMois);
    }

}
