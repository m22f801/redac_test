package fr.si2m.red.dsn;

import java.math.BigInteger;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import fr.si2m.red.EntiteImportableBatch;
import fr.si2m.red.core.repository.jpa.conversion.ConvertisseurDatesDDMMYYYY;
import fr.si2m.red.core.repository.jpa.conversion.ConvertisseurNumeriqueDecimal;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * Modèle des bases assujetties
 * 
 * @author poidij
 *
 */
@Entity
@Table(name = "BASE_ASSUJETTIE")
@IdClass(BaseAssujettieId.class)
@Data
@EqualsAndHashCode(callSuper = false, of = { "ligneEnCoursImportBatch", "idBaseAssujettie" })
@ToString(callSuper = false, of = { "ligneEnCoursImportBatch", "idBaseAssujettie" })
public class BaseAssujettie extends EntiteImportableBatch {

    /**
     * UID de version.
     */
    private static final long serialVersionUID = 822362569368101808L;

    /**
     * Indicateur de ligne temporaire en cours d'import via Batch.
     * 
     * @param ligneEnCoursImportBatch
     *            le nom du SI d'où provient l'information
     * @return le nom du SI d'où provient l'information
     */
    @Id
    @Column(name = "TMP_BATCH")
    private boolean ligneEnCoursImportBatch;

    /**
     * Identifiant de la base assujettie.
     *
     * @param idBaseAssujettie
     *            L'identifiant de la base assujettie
     * @return L'identifiant de la base assujettie
     */
    @Id
    @Column(name = "ID_BASE_ASSUJETTIE")
    private String idBaseAssujettie;

    /**
     * Identifiant de l'affiliation.
     *
     * @param idAffiliation
     *            L'identifiant de l'affiliation
     * @return L'identifiant de l'affiliation
     */
    @Column(name = "ID_AFFILIATION")
    private String idAffiliation;

    /**
     * L'affiliation.
     *
     * @param affiliation
     *            L'affiliation
     * @return L'affiliation
     */
    @ManyToOne
    @Fetch(FetchMode.JOIN)
    @JoinColumns({ @JoinColumn(name = "ID_AFFILIATION", insertable = false, updatable = false),
            @JoinColumn(name = "TMP_BATCH", insertable = false, updatable = false) })
    private Affiliation affiliation;

    /**
     * S21.G00.78.002
     *
     * @param dateDebutRattachement
     *            S21.G00.78.002
     * @return S21.G00.78.002
     */
    @Column(name = "DATE_DEB_RATTACHEMENT")
    private Integer dateDebutRattachement;

    /**
     * S21.G00.78.003
     *
     * @param dateFinRattachement
     *            S21.G00.78.003
     * @return S21.G00.78.003
     */
    @Column(name = "DATE_FIN_RATTACHEMENT")
    private Integer dateFinRattachement;

    /**
     * Le montant de la cotisation.
     *
     * @param montantCotisation
     *            le montant de la cotisation
     * @return le montant de la cotisation
     */
    @Column(name = "MONTANT_COTISATION")
    private Double montantCotisation;

    /**
     * Montant de côtisation (tel que lu dans le fichier).
     *
     */
    @Transient
    private String montantCotisationAsText;

    /**
     * Montant de côtisation (tel que lu dans le fichier).
     *
     */
    @Transient
    private String dateDebutRattachementAsText;

    /**
     * Montant de côtisation (tel que lu dans le fichier).
     *
     */
    @Transient
    private String dateFinRattachementAsText;

    /**
     * Identifiant de la DSN dans la brique
     */
    @Column(name = "ID_DSN_BRIQUE")
    private BigInteger idDsnBrique;

    /**
     * Clé primaire de la table dans la brique
     */
    @Column(name = "PK_ENTITEE_BRIQUE")
    private BigInteger cleEntiteeBrique;

    /**
     * Renvoie le montant de côtisation (tel que lu dans le fichier).
     * 
     * @return le montant de côtisation
     */
    public String getMontantCotisationAsText() {
        if (montantCotisationAsText != null) {
            return montantCotisationAsText;
        }
        return new ConvertisseurNumeriqueDecimal().convertToEntityAttribute(getMontantCotisation());
    }

    /**
     * Valorise le montant de côtisation (tel que lu dans le fichier).
     * 
     * @param montantCotisation
     *            le montant de côtisation à valoriser
     */
    public void setMontantCotisationAsText(String montantCotisation) {
        this.montantCotisationAsText = montantCotisation;
        setMontantCotisation(new ConvertisseurNumeriqueDecimal().convertToDatabaseColumn(montantCotisation));
    }

    /**
     * Date de début de rattachement (telle que lue dans le fichier).
     * 
     * @return la date de début de rattachement
     */
    public String getDateDebutRattachementAsText() {
        if (dateDebutRattachementAsText != null) {
            return dateDebutRattachementAsText;
        }
        return new ConvertisseurDatesDDMMYYYY().convertToEntityAttribute(getDateDebutRattachement());
    }

    /**
     * Valorise la date de début de rattachement (telle que lue dans le fichier).
     * 
     * @param dateDebutRattachement
     *            la date de début de rattachement à valoriser
     */
    public void setDateDebutRattachementAsText(String dateDebutRattachement) {
        this.dateDebutRattachementAsText = dateDebutRattachement;
        setDateDebutRattachement(new ConvertisseurDatesDDMMYYYY().convertToDatabaseColumn(dateDebutRattachement));
    }

    /**
     * Date de fin de rattachement (telle que lue dans le fichier).
     * 
     * @return la date de fin de rattachement
     */
    public String getDateFinRattachementAsText() {
        if (dateFinRattachementAsText != null) {
            return dateFinRattachementAsText;
        }
        return new ConvertisseurDatesDDMMYYYY().convertToEntityAttribute(getDateFinRattachement());
    }

    /**
     * Valorise la date de fin de rattachement (telle que lue dans le fichier).
     * 
     * @param dateFinRattachement
     *            la date de fin de rattachement à valoriser
     */
    public void setDateFinRattachementAsText(String dateFinRattachement) {
        this.dateFinRattachementAsText = dateFinRattachement;
        setDateFinRattachement(new ConvertisseurDatesDDMMYYYY().convertToDatabaseColumn(dateFinRattachement));
    }

    @Override
    public BaseAssujettieId getId() {
        return new BaseAssujettieId(ligneEnCoursImportBatch, idBaseAssujettie);
    }

}
