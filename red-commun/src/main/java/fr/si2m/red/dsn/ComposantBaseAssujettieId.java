package fr.si2m.red.dsn;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * Modèle des composants de base assujettie
 * 
 * @author poidij
 *
 */
@Data
@EqualsAndHashCode(callSuper = false, of = { "ligneEnCoursImportBatch", "idComposantBaseAssujettie" })
@ToString(callSuper = false, of = { "ligneEnCoursImportBatch", "idComposantBaseAssujettie" })
@NoArgsConstructor
@AllArgsConstructor
public class ComposantBaseAssujettieId implements Serializable {
    /**
     * UID de version.
     */
    private static final long serialVersionUID = 822362569368101808L;

    /**
     * Indicateur de ligne temporaire en cours d'import via Batch.
     * 
     * @param ligneEnCoursImportBatch
     *            le nom du SI d'où provient l'information
     * @return le nom du SI d'où provient l'information
     */
    private boolean ligneEnCoursImportBatch;

    /**
     * Identifiant du composant de base assujettie
     *
     * @param idComposantBaseAssujettie
     *            L'identifiant du composant de base assujettie
     * @return L'identifiant du composant de base assujettie
     */
    private String idComposantBaseAssujettie;

}
