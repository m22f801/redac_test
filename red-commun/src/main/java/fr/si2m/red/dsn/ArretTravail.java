package fr.si2m.red.dsn;

import java.math.BigInteger;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;
import javax.persistence.Transient;

import fr.si2m.red.EntiteImportableBatch;
import fr.si2m.red.core.repository.jpa.conversion.ConvertisseurDatesDDMMYYYY;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * Modèle des arrêts de travail
 * 
 * @author poidij
 *
 */
@Entity
@Table(name = "ARRET_TRAVAIL")
@IdClass(ArretTravailId.class)
@Data
@EqualsAndHashCode(callSuper = false, of = { "ligneEnCoursImportBatch", "idArretTravail" })
@ToString(callSuper = false, of = { "ligneEnCoursImportBatch", "idArretTravail" })
public class ArretTravail extends EntiteImportableBatch {

    /**
     * UID de version.
     */
    private static final long serialVersionUID = 822362569368101808L;

    /**
     * Indicateur de ligne temporaire en cours d'import via Batch.
     * 
     * @param ligneEnCoursImportBatch
     *            le nom du SI d'où provient l'information
     * @return le nom du SI d'où provient l'information
     */
    @Id
    @Column(name = "TMP_BATCH")
    private boolean ligneEnCoursImportBatch;

    /**
     * Identifiant de l'arrêt de travail
     *
     * @param idArretTravail
     *            L'identifiant de l'arrêt de travail
     * @return L'identifiant de l'arrêt de travail
     */
    @Id
    @Column(name = "ID_ARRET_TRAVAIL")
    private String idArretTravail;

    /**
     * Identifiant du contrat de travail
     *
     * @param idContratTravail
     *            L'identifiant du contrat de travail
     * @return L'identifiant du contrat de travail
     */
    @Column(name = "ID_CONTRAT_TRAVAIL")
    private String idContratTravail;

    /**
     * S21.G00.60.001
     *
     * @param motifArret
     *            S21.G00.60.001
     * @return S21.G00.60.001
     */
    @Column(name = "MOTIF_ARRET")
    private String motifArret;

    /**
     * S21.G00.60.002
     *
     * @param dateDernierJour
     *            S21.G00.60.002
     * @return S21.G00.60.002
     */
    @Column(name = "DERNIER_JOUR")
    private Integer dateDernierJour;

    /**
     * S21.G00.60.003
     *
     * @param dateFinPrevisionnelleArret
     *            S21.G00.60.003
     * @return S21.G00.60.003
     */
    @Column(name = "DATE_FIN_PREVISIONNELLE")
    private Integer dateFinPrevisionnelleArret;

    /**
     * S21.G00.60.010
     *
     * @param dateReprise
     *            S21.G00.60.010
     * @return S21.G00.60.010
     */
    @Column(name = "DATE_REPRISE")
    private Integer dateReprise;

    /**
     * S21.G00.60.011
     *
     * @param motifReprise
     *            S21.G00.60.011
     * @return S21.G00.60.011
     */
    @Column(name = "MOTIF_REPRISE")
    private String motifReprise;

    /**
     * Date du dernier jour (telle que lue dans le fichier).
     *
     */
    @Transient
    private String dateDernierJourAsText;

    /**
     * Date de fin prévisionnelle d'arrêt (telle que lue dans le fichier).
     *
     */
    @Transient
    private String dateFinPrevisionnelleArretAsText;

    /**
     * Date de reprise (telle que lue dans le fichier).
     *
     */
    @Transient
    private String dateRepriseAsText;

    /**
     * Identifiant de la DSN dans la brique
     */
    @Column(name = "ID_DSN_BRIQUE")
    private BigInteger idDsnBrique;

    /**
     * Clé primaire de la table dans la brique
     */
    @Column(name = "PK_ENTITEE_BRIQUE")
    private BigInteger cleEntiteeBrique;

    /**
     * Date du dernier jour (telle que lue dans le fichier).
     * 
     * @return la date du dernier jour
     */
    public String getDateDernierJourAsText() {
        if (dateDernierJourAsText != null) {
            return dateDernierJourAsText;
        } else {
            return new ConvertisseurDatesDDMMYYYY().convertToEntityAttribute(getDateDernierJour());
        }
    }

    /**
     * Valorise la date du dernier jour (telle que lue dans le fichier).
     * 
     * @param dateDernierJour
     *            la date du dernier jour à valoriser
     */
    public void setDateDernierJourAsText(String dateDernierJour) {
        this.dateDernierJourAsText = dateDernierJour;
        setDateDernierJour(new ConvertisseurDatesDDMMYYYY().convertToDatabaseColumn(dateDernierJour));
    }

    /**
     * Date de fin prévisionnelle d'arrêt (telle que lue dans le fichier).
     * 
     * @return la date de fin prévisionnelle d'arrêt
     */
    public String getDateFinPrevisionnelleArretAsText() {
        if (dateFinPrevisionnelleArretAsText != null) {
            return dateFinPrevisionnelleArretAsText;
        } else {
            return new ConvertisseurDatesDDMMYYYY().convertToEntityAttribute(getDateFinPrevisionnelleArret());
        }
    }

    /**
     * Valorise la date de fin prévisionnelle d'arrêt (telle que lue dans le fichier).
     * 
     * @param dateFinPrevisionnelleArret
     *            la date de fin prévisionnelle d'arrêt à valoriser
     */
    public void setDateFinPrevisionnelleArretAsText(String dateFinPrevisionnelleArret) {
        this.dateFinPrevisionnelleArretAsText = dateFinPrevisionnelleArret;
        setDateFinPrevisionnelleArret(new ConvertisseurDatesDDMMYYYY().convertToDatabaseColumn(dateFinPrevisionnelleArret));
    }

    /**
     * Date de reprise (telle que lue dans le fichier).
     * 
     * @return la date de reprise
     */
    public String getDateRepriseAsText() {
        if (dateRepriseAsText != null) {
            return dateRepriseAsText;
        } else {
            return new ConvertisseurDatesDDMMYYYY().convertToEntityAttribute(getDateReprise());
        }
    }

    /**
     * Valorise la date de reprise (telle que lue dans le fichier).
     * 
     * @param dateReprise
     *            la date de reprise à valoriser
     */
    public void setDateRepriseAsText(String dateReprise) {
        this.dateRepriseAsText = dateReprise;
        setDateReprise(new ConvertisseurDatesDDMMYYYY().convertToDatabaseColumn(dateReprise));
    }

    @Override
    public ArretTravailId getId() {
        return new ArretTravailId(ligneEnCoursImportBatch, idArretTravail);
    }

}
