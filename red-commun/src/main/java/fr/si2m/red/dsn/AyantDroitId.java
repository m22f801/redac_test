package fr.si2m.red.dsn;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * Modèle des ayants droit
 * 
 * @author poidij
 *
 */
@Data
@EqualsAndHashCode(callSuper = false, of = { "ligneEnCoursImportBatch", "idAyantDroit" })
@ToString(callSuper = false, of = { "ligneEnCoursImportBatch", "idAyantDroit" })
@NoArgsConstructor
@AllArgsConstructor
public class AyantDroitId implements Serializable {

    /**
     * UID de version.
     */
    private static final long serialVersionUID = 822362569368101808L;

    /**
     * Indicateur de ligne temporaire en cours d'import via Batch.
     * 
     * @param ligneEnCoursImportBatch
     *            le nom du SI d'où provient l'information
     * @return le nom du SI d'où provient l'information
     */
    private boolean ligneEnCoursImportBatch;

    /**
     * Identifiant de l'ayant droit
     *
     * @param idAyantDroit
     *            L'identifiant de l'ayant droit
     * @return L'identifiant de l'ayant droit
     */
    private String idAyantDroit;

}
