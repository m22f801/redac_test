package fr.si2m.red.dsn;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * Modèle des contrats de travail
 * 
 * @author poidij
 *
 */
@Data
@EqualsAndHashCode(callSuper = false, of = { "ligneEnCoursImportBatch", "idContratTravail" })
@ToString(callSuper = false, of = { "ligneEnCoursImportBatch", "idContratTravail" })
@NoArgsConstructor
@AllArgsConstructor
public class ContratTravailId implements Serializable {
    /**
     * UID de version.
     */
    private static final long serialVersionUID = 822362569368101808L;

    /**
     * Indicateur de ligne temporaire en cours d'import via Batch.
     * 
     * @param ligneEnCoursImportBatch
     *            le nom du SI d'où provient l'information
     * @return le nom du SI d'où provient l'information
     */
    private boolean ligneEnCoursImportBatch;

    /**
     * Identifiant du contrat de travail
     *
     * @param idContratTravail
     *            L'identifiant du contrat de travail
     * @return L'identifiant du contrat de travail
     */
    private String idContratTravail;

}
