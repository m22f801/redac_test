package fr.si2m.red.dsn;

import java.util.List;
import java.util.Map;

import fr.si2m.red.core.repository.EntiteImportableRepository;

/**
 * Référentiel des composants de versements.
 * 
 * @author poidij
 *
 */
public interface ComposantVersementRepository extends EntiteImportableRepository<ComposantVersement> {

    /**
     * Recherche le composant le plus récent du versement idVersement pour le type de population.
     * 
     * @param idVersement
     *            l'identifiant du versement
     * @param numCategorieQuittancement
     *            le type de population du composant à rechercher
     * @return le composant le plus récent du versement pour le type de population
     */
    ComposantVersement getComposantVersementPourNumCategorie(String idVersement, String numCategorieQuittancement);

    /**
     * Récupère les types de population distincts ciblés par un versement.
     * 
     * @param idVersement
     *            l'identifiant du versement
     * @return les types de populations disctincts
     */
    List<String> getTypesPopulationDistincts(String idVersement);

    /**
     * Récupère les périodes d'affectation distinctes ciblées par un versement.
     * 
     * @param idVersement
     *            l'identifiant du versement
     * @return les périodes d'affectation disctinctes
     */
    List<String> getPeriodesAffectationDistinctes(String idVersement);

    /**
     * Récupère les identifiants de sous-fonds pour une période d'affectation.
     * 
     * @param idVersement
     *            l'identifiant du versement
     * @param periodeAffectation
     *            la période d'affectation
     * @return les identifiants de sous-fonds, indexés par type de population
     */
    Map<String, String> getIdentifiantsSousFondsPourPeriodeAffectation(String idVersement, String periodeAffectation);

    /**
     * Récupère les sommes de montants versés pour une période d'affectation.
     * 
     * @param idVersement
     *            l'identifiant du versement
     * @param periodeAffectation
     *            la période d'affectation
     * @return les sommes de montants versés, indexés par type de population
     */
    Map<String, Double> getSommesMontantsVersesPourPeriodeAffectation(String idVersement, String periodeAffectation);

    /**
     * Indique l'existence d'un composant en base.
     * 
     * @param idComposantVersement
     *            l'identifiant du composant
     * @return true s'il existe au moins un composant ayant cet identifiant, false sinon
     */
    boolean existeUnComposantVersement(String idComposantVersement);

    /**
     * Récupère la date de début de rattachement calculée d'un versement.
     * 
     * @param idVersement
     *            l'identifiant du versement
     * @return la date de début de rattachement
     */
    Integer getDateDebutPeriodeRattachement(String idVersement);

    /**
     * Récupère la date de fin de rattachement calculée d'un versement.
     * 
     * @param idVersement
     *            l'identifiant du versement
     * @return la date de fin de rattachement
     */
    Integer getDateFinPeriodeRattachement(String idVersement);
}
