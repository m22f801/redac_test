package fr.si2m.red.dsn;

import java.util.List;

import fr.si2m.red.core.repository.EntiteImportableRepository;

/**
 * Référentiel des bases assujetties.
 * 
 * @author poidij
 *
 */
public interface BaseAssujettieRepository extends EntiteImportableRepository<BaseAssujettie> {

    /**
     * Indique l'existence d'une Base Assujettie en base.
     * 
     * @param idBaseAssujettie
     *            l'identifiant de la base assujettie
     * @return true s'il existe au moins une base assujettie ayant cet identifiant, false sinon
     */
    boolean existeUneBaseAssujettie(String idBaseAssujettie);

    /**
     * Récupère l'ensemble des plages distinctes de rattachement pour un contrat de travail donné.
     * 
     * @param idContratTravail
     *            l'identifiant du contrat de travail
     * @return l'ensemble des plages distinctes de rattachement
     */
    List<PlageRattachement> getPlagesRattachementsPourContratTravailAvecMontantCotisationNonNul(String idContratTravail);

    /**
     * Récupère l'ensemble des bases assujetties d'un contrat de travail avec un montant de cotisation non nul correspondant à une plage donnée.
     * 
     * @param idContratTravail
     *            l'identifiant du contrat de travail
     * @param plage
     *            la plage
     * @return l'ensemble des bases pour le contrat de travail avec montant de cotisation non nul pour cette plage
     */
    List<BaseAssujettie> getBasesAssujettiesAvecMontantCotisationNonNul(String idContratTravail, Integer dateDebutRattachement);

    /**
     * Indique l'existence d'une Base Assujettie en base rattachée à la période
     * 
     * @param idPeriode
     *            l'identifiant de la période
     * @return true s'il existe au moins une base assujettie rattachée à cette période
     */
    boolean existeBaseAssujettiePourPeriode(Long idPeriode);
}
