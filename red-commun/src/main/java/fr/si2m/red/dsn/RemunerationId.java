package fr.si2m.red.dsn;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * Modèle des rémunérations
 * 
 * @author poidij
 *
 */
@Data
@EqualsAndHashCode(callSuper = false, of = { "ligneEnCoursImportBatch", "idRemuneration" })
@ToString(callSuper = false, of = { "ligneEnCoursImportBatch", "idRemuneration" })
@NoArgsConstructor
@AllArgsConstructor
public class RemunerationId implements Serializable {
    /**
     * UID de version.
     */
    private static final long serialVersionUID = 822362569368101808L;

    /**
     * Indicateur de ligne temporaire en cours d'import via Batch.
     * 
     * @param ligneEnCoursImportBatch
     *            le nom du SI d'où provient l'information
     * @return le nom du SI d'où provient l'information
     */
    private boolean ligneEnCoursImportBatch;

    /**
     * Identifiant de la rémunération
     *
     * @param idRemuneration
     *            L'identifiant de la rémunération
     * @return L'identifiant de la rémunération
     */
    private String idRemuneration;

}
