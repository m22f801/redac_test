package fr.si2m.red.dsn;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * Modèle des arrêts de travail
 * 
 * @author poidij
 *
 */
@Data
@EqualsAndHashCode(callSuper = false, of = { "ligneEnCoursImportBatch", "idArretTravail" })
@ToString(callSuper = false, of = { "ligneEnCoursImportBatch", "idArretTravail" })
@NoArgsConstructor
@AllArgsConstructor
public class ArretTravailId implements Serializable {

    /**
     * UID de version.
     */
    private static final long serialVersionUID = 822362569368101808L;

    /**
     * Indicateur de ligne temporaire en cours d'import via Batch.
     * 
     * @param ligneEnCoursImportBatch
     *            le nom du SI d'où provient l'information
     * @return le nom du SI d'où provient l'information
     */
    private boolean ligneEnCoursImportBatch;

    /**
     * Identifiant de l'arrêt de travail
     *
     * @param idArretTravail
     *            L'identifiant de l'arrêt de travail
     * @return L'identifiant de l'arrêt de travail
     */
    private String idArretTravail;

}
