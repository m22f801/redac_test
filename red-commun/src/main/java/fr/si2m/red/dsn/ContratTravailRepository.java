package fr.si2m.red.dsn;

import fr.si2m.red.core.repository.EntiteImportableRepository;

/**
 * Référentiel des contrats de travail.
 * 
 * @author poidij
 *
 */
public interface ContratTravailRepository extends EntiteImportableRepository<ContratTravail> {

    /**
     * Indique l'existence d'un contrat de travail en base.
     * 
     * @param idContratTravail
     *            l'identifiant du contrat de travail
     * @return true s'il existe au moins un contrat de travail ayant cet identifiant, false sinon
     */
    boolean existeUnContratTravail(String idContratTravail);

    /**
     * Récupère le contrat de travail le plus récent rattaché à un individu donné.
     * 
     * @param idIndividu
     *            l'identifiant de l'individu
     * @return le contrat de travail le plus récent rattaché à l'individu
     */
    ContratTravail getDernierContratTravailPourIndividu(String idIndividu);
}
