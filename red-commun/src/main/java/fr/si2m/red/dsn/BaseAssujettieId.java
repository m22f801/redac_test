package fr.si2m.red.dsn;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * Modèle des bases assujetties
 * 
 * @author poidij
 *
 */
@Data
@EqualsAndHashCode(callSuper = false, of = { "ligneEnCoursImportBatch", "idBaseAssujettie" })
@ToString(callSuper = false, of = { "ligneEnCoursImportBatch", "idBaseAssujettie" })
@NoArgsConstructor
@AllArgsConstructor
public class BaseAssujettieId implements Serializable {

    /**
     * UID de version.
     */
    private static final long serialVersionUID = 822362569368101808L;

    /**
     * Indicateur de ligne temporaire en cours d'import via Batch.
     * 
     * @param ligneEnCoursImportBatch
     *            le nom du SI d'où provient l'information
     * @return le nom du SI d'où provient l'information
     */
    private boolean ligneEnCoursImportBatch;

    /**
     * Identifiant de la base assujettie
     *
     * @param idBaseAssujettie
     *            L'identifiant de la base assujettie
     * @return L'identifiant de la base assujettie
     */
    private String idBaseAssujettie;

}
