package fr.si2m.red.dsn;

import java.math.BigInteger;
import java.util.Arrays;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.Formula;

import fr.si2m.red.DateRedac;
import fr.si2m.red.EntiteImportableBatch;
import fr.si2m.red.core.repository.jpa.conversion.ConvertisseurDatesDDMMYYYY;
import fr.si2m.red.core.repository.jpa.conversion.ConvertisseurNumeriqueDecimal;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * Modèle des versements.
 * 
 * @author poidij
 *
 */
@Entity
@Table(name = "VERSEMENT")
@IdClass(VersementId.class)
@Data
@EqualsAndHashCode(callSuper = false, of = { "ligneEnCoursImportBatch", "idVersement" })
@ToString(callSuper = false, of = { "ligneEnCoursImportBatch", "idVersement" })
public class Versement extends EntiteImportableBatch {
    /**
     * UID de version.
     */
    private static final long serialVersionUID = 822362569368101808L;

    /**
     * Indicateur de ligne temporaire en cours d'import via Batch.
     * 
     * @param ligneEnCoursImportBatch
     *            le nom du SI d'où provient l'information
     * @return le nom du SI d'où provient l'information
     */
    @Id
    @Column(name = "TMP_BATCH")
    private boolean ligneEnCoursImportBatch;

    /**
     * Identifiant du versement.
     *
     * @param idVersement
     *            L'identifiant du versement
     * @return L'identifiant du versement
     */
    @Id
    @Column(name = "ID_VERSEMENT")
    private String idVersement;

    /**
     * Identifiant technique de l'adhésion.
     *
     * @param idAdhEtabMois
     *            Identifiant technique de l'adhésion
     * @return Identifiant technique de l'adhésion
     */
    @Column(name = "ID_ADH_ETAB_MOIS")
    private String idAdhEtabMois;

    /**
     * L'adhésion.
     *
     * @param adhesionEtablissementMois
     *            l'adhésion
     * @return l'adhésion
     */
    @ManyToOne
    @Fetch(FetchMode.JOIN)
    @JoinColumns({ @JoinColumn(name = "ID_ADH_ETAB_MOIS", insertable = false, updatable = false),
            @JoinColumn(name = "TMP_BATCH", insertable = false, updatable = false) })
    private AdhesionEtablissementMois adhesionEtablissementMois;

    /**
     * S21.G00.20.001
     *
     * @param identifiantOrganismeProtectionSociale
     *            S21.G00.20.001
     * @return S21.G00.20.001
     */
    @Column(name = "ID_OPS")
    private String identifiantOrganismeProtectionSociale;

    /**
     * S21.G00.20.002
     *
     * @param entiteAffectation
     *            S21.G00.20.002
     * @return S21.G00.20.002
     */
    @Column(name = "ENTITE_AFFECTATION")
    private String entiteAffectation;

    /**
     * S21.G00.20.003
     *
     * @param bic
     *            S21.G00.20.003
     * @return S21.G00.20.003
     */
    @Column(name = "BIC")
    private String bic;

    /**
     * S21.G00.20.004
     *
     * @param iban
     *            S21.G00.20.004
     * @return S21.G00.20.004
     */
    @Column(name = "IBAN")
    private String iban;

    /**
     * S21.G00.20.005
     *
     * @param montantVersement
     *            S21.G00.20.005
     * @return S21.G00.20.005
     */
    @Column(name = "MONTANT_VERSEMENT")
    private Double montantVersement;

    /**
     * S21.G00.20.008
     *
     * @param codeDelegataireGestion
     *            S21.G00.20.008
     * @return S21.G00.20.008
     */
    @Column(name = "COT_DELEG_COT_VRS")
    private String codeDelegataireGestion;

    /**
     * Le mode de paiement.
     *
     * @param modePaiement
     *            le mode de paiement
     * @return le mode de paiement
     */
    @Column(name = "MODE_PAIEMENT")
    private String modePaiement;

    /**
     * Le mode de paiement.
     *
     * @param modePaiement
     *            le mode de paiement
     * @return le mode de paiement
     */
    @Formula("(SELECT concat(MODE_PAIEMENT, ' ', p.LIBELLE_COURT) FROM PARAM_CODE_LIBELLE p WHERE p.TBL = 'VERSEMENT' AND p.CHAMP = 'MODE_PAIEMENT' AND p.CODE = MODE_PAIEMENT)")
    private String libelleModePaiement;

    /**
     * S21.G00.20.011
     *
     * @param datePaiement
     *            S21.G00.20.011
     * @return S21.G00.20.011
     */
    @Column(name = "DATE_PAIEMENT")
    private Integer datePaiement;

    /**
     * S21.G00.20.012
     *
     * @param siretPayeur
     *            S21.G00.20.012
     * @return S21.G00.20.012
     */
    @Column(name = "SIRET_PAYEUR")
    private String siretPayeur;

    /**
     * Code identifiant des fonds.
     *
     * @param codeIdentifiantFonds
     *            Le code identifiant des fonds
     * @return Le code identifiant des fonds
     */
    @Column(name = "CODE_ID_FONDS")
    private String codeIdentifiantFonds;

    /**
     * Référence de paiement.
     *
     * @param referencePaiement
     *            La référence de paiement
     * @return La référence de paiement
     */
    @Column(name = "REF_PAIEMENT")
    private String referencePaiement;

    /**
     * Montant de versement (tel que lu dans le fichier).
     *
     */
    @Transient
    private String montantVersementAsText;

    /**
     * Date de paiement (telle que lue dans le fichier).
     *
     */
    @Transient
    private String datePaiementAsText;

    /**
     * Identifiant de la DSN dans la brique
     */
    @Column(name = "ID_DSN_BRIQUE")
    private BigInteger idDsnBrique;

    /**
     * Clé primaire de la table dans la brique
     */
    @Column(name = "PK_ENTITEE_BRIQUE")
    private BigInteger cleEntiteeBrique;

    /**
     * Renvoie le montant de versement (tel que lu dans le fichier).
     * 
     * @return le montant de versement
     */
    public String getMontantVersementAsText() {
        String value = this.montantVersementAsText;
        if (StringUtils.isBlank(value) && getMontantVersement() != null) {
            value = new ConvertisseurNumeriqueDecimal().convertToEntityAttribute(getMontantVersement());
        }
        return value;
    }

    /**
     * Valorise le montant de versement (tel que lu dans le fichier).
     * 
     * @param montantVersement
     *            le montant de versement à valoriser
     */
    public void setMontantVersementAsText(String montantVersement) {
        this.montantVersementAsText = montantVersement;
        setMontantVersement(new ConvertisseurNumeriqueDecimal().convertToDatabaseColumn(montantVersement));
    }

    /**
     * Date de paiement (telle que lue dans le fichier).
     * 
     * @return la date de paiement
     */
    public String getDatePaiementAsText() {
        if (datePaiementAsText != null) {
            return datePaiementAsText;
        }
        return new ConvertisseurDatesDDMMYYYY().convertToEntityAttribute(getDatePaiement());
    }

    /**
     * Valorise la date de paiement (telle que lue dans le fichier).
     * 
     * @param datePaiement
     *            la date de paiement à valoriser
     */
    public void setDatePaiementAsText(String datePaiement) {
        this.datePaiementAsText = datePaiement;
        setDatePaiement(new ConvertisseurDatesDDMMYYYY().convertToDatabaseColumn(datePaiement));
    }

    /**
     * Récupère la date de paiement.
     * 
     * @return la date de paiement
     */
    public Date getDatePaiementAsDate() {
        return DateRedac.convertitEnDate(getDatePaiement());
    }

    /**
     * Récupère la date de paiement.
     * 
     * @return la date de paiement
     */
    public String getDatePaiementSansFormatage() {
        return DateRedac.convertionDateRedacSansValidation(getDatePaiement());
    }

    /**
     * Vérifie si le mode de paiement du versement est particulier.
     * 
     * @return true si le mode de paiement est particulier, false sinon
     */
    public boolean isModePaiementParticulier() {
        return Arrays.asList("03", "04", "05").contains(getModePaiement());
    }

    @Override
    public VersementId getId() {
        return new VersementId(ligneEnCoursImportBatch, idVersement);
    }

}
