package fr.si2m.red.dsn;

import java.util.List;
import java.util.Map;

import fr.si2m.red.core.repository.EntiteImportableRepository;

/**
 * Référentiel des composants de bases assujetties.
 * 
 * @author poidij
 *
 */
public interface ComposantBaseAssujettieRepository extends EntiteImportableRepository<ComposantBaseAssujettie> {

    /**
     * Récupère un composant de base assujettie pour une base assujettie et un type de composant donné.
     * 
     * @param idBaseAssujettie
     *            l'identifiant de base assujettie
     * @param typeComposantBase
     *            le type de composant
     * @return le composant de base assujettie correspondant
     */
    ComposantBaseAssujettie get(String idBaseAssujettie, Integer typeComposantBase);

    /**
     * Récupère la somme des montants de composants de base assujettie pour une base assujettie et des types de composants donnés.
     * 
     * @param idBaseAssujettie
     *            l'identifiant de base assujettie
     * @param typesComposantsBases
     *            les types de composant
     * @return la somme des montants des composants de base assujettie correspondante
     */
    Double getSommeMontantsComposantsBaseAssujettie(String idBaseAssujettie, List<String> typesComposantsBases);
    
    /**
     * Récupère la somme des montants de composants de base assujettie pour une base assujettie et des types de composants donnés.
     * 
     * @param idAffiliation
     *            l'identifiant de l'affiliation
     * @param tcba
     *            type de composant de base assujettie
     * @return la somme des montants des composants de base assujettie correspondante
     */
    Double getSommeMontantsComposantsBaseAssujettiePourAffiliationEtTCBA(String idAffiliation, String tcba);

    /**
     * Récupère les montants de composants de base assujettie regroupés par types.
     * 
     * @param idBaseAssujettie
     *            l'identifiant de base assujettie
     * @return les montants des composants indexés par type
     */
    Map<String, Double> getMontantsPourBase(String idBaseAssujettie);

    /**
     * Indique l'existence d'un composant en base.
     * 
     * @param idComposantBaseAssujettie
     *            l'identifiant du composant
     * @return true s'il existe au moins un composant ayant cet identifiant, false sinon
     */
    boolean existeUnComposantBaseAssujettie(String idComposantBaseAssujettie);

}
