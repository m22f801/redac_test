package fr.si2m.red.dsn;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * Modèle des versements des individus
 * 
 * @author poidij
 *
 */
@Data
@EqualsAndHashCode(callSuper = false, of = { "ligneEnCoursImportBatch", "idVersementIndividu" })
@ToString(callSuper = false, of = { "ligneEnCoursImportBatch", "idVersementIndividu" })
@NoArgsConstructor
@AllArgsConstructor
public class VersementIndividuId implements Serializable {
    /**
     * UID de version.
     */
    private static final long serialVersionUID = 822362569368101808L;

    /**
     * Indicateur de ligne temporaire en cours d'import via Batch.
     * 
     * @param ligneEnCoursImportBatch
     *            le nom du SI d'où provient l'information
     * @return le nom du SI d'où provient l'information
     */
    private boolean ligneEnCoursImportBatch;

    /**
     * Identifiant du versement de l'individu
     *
     * @param idVersementIndividu
     *            L'identifiant du versement de l'individu
     * @return L'identifiant du versement de l'individu
     */
    private String idVersementIndividu;

}
