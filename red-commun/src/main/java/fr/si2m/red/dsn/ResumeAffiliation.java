package fr.si2m.red.dsn;

import fr.si2m.red.DateRedac;
import lombok.Data;

/**
 * Résumé d'une affiliation (avec informations consolidées). Utilisé pour l'IHM - onglet individus
 * 
 * @author nortaina
 *
 */
@Data
public class ResumeAffiliation {

    /**
     * S21.G00.70.005
     *
     * @param codePopulation
     *            S21.G00.70.005
     * @return S21.G00.70.005
     */
    private String codePopulation;

    /**
     * S21.G00.40.001
     *
     * @param dateDebutContrat
     *            S21.G00.40.001
     * @return S21.G00.40.001
     */
    private Integer dateDebutContrat;

    /**
     * S21.G00.40.010
     *
     * @param dateFinPrevisionnelle
     *            S21.G00.40.010
     * @return S21.G00.40.010
     */
    private Integer dateFinPrevisionnelle;

    /**
     * S21.G00.30.001
     *
     * @param identifiantRepertoire
     *            S21.G00.30.001
     * @return S21.G00.30.001
     */
    private String identifiantRepertoire;

    /**
     * S21.G00.30.002
     *
     * @param nomFamille
     *            S21.G00.30.002
     * @return S21.G00.30.002
     */
    private String nomFamille;

    /**
     * S21.G00.30.003
     *
     * @param nomUsage
     *            S21.G00.30.003
     * @return S21.G00.30.003
     */
    private String nomUsage;

    /**
     * S21.G00.30.004
     *
     * @param prenom
     *            S21.G00.30.004
     * @return S21.G00.30.004
     */
    private String prenom;

    /**
     * S21.G00.30.019
     *
     * @param matricule
     *            S21.G00.30.019
     * @return S21.G00.30.019
     */
    private String matricule;

    /**
     * S21.G00.30.020
     *
     * @param numeroTechniqueTemporaire
     *            S21.G00.30.020
     * @return S21.G00.30.020
     */
    private String numeroTechniqueTemporaire;

    /**
     * Date début contrat, sans validation calendaire
     * 
     * @return une string représentant la date
     */
    public String getDateDebutContratSansValidation() {
        return DateRedac.convertionDateRedacSansValidation(getDateDebutContrat());
    }

    /**
     * Date début contrat, sans validation calendaire
     * 
     * @return une string représentant la date
     */
    public String getDateFinPrevisionnelleSansValidation() {
        return DateRedac.convertionDateRedacSansValidation(getDateFinPrevisionnelle());
    }

}
