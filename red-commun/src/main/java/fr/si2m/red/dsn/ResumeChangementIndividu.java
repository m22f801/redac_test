package fr.si2m.red.dsn;

import lombok.Data;

/**
 * Résumé d'un changement individu
 * 
 * @author eudesr
 *
 */
@Data
public class ResumeChangementIndividu {

    /**
     * S21.G00.31.001**
     * 
     * @param dateModification
     *            S21.G00.31.001
     * @return S21.G00.31.001
     */
    private Integer dateModification;

    /**
     * S21.G00.31.008
     *
     * @param ancienIdentifiant
     *            S21.G00.31.008
     * @return S21.G00.31.008
     */
    private String ancienIdentifiant;

    /**
     * S21.G00.31.009
     *
     * @param ancienNomFamille
     *            S21.G00.31.009
     * @return S21.G00.31.009
     */
    private String ancienNomFamille;

    /**
     * S21.G00.31.010
     *
     * @param ancienPrenom
     *            S21.G00.31.010
     * @return S21.G00.31.010
     */
    private String ancienPrenom;

    /**
     * S21.G00.31.011
     *
     * @param ancienneDateNaissance
     *            S21.G00.31.011
     * @return S21.G00.31.011
     */
    private Integer ancienneDateNaissance;

}
