package fr.si2m.red.dsn;

import java.math.BigInteger;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.Formula;

import fr.si2m.red.DateRedac;
import fr.si2m.red.EntiteImportableBatch;
import fr.si2m.red.core.repository.jpa.conversion.ConvertisseurDatesDDMMYYYY;
import fr.si2m.red.core.repository.jpa.conversion.ConvertisseurNumeriqueDecimal;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * Modèle des cotisationEtablissement
 * 
 * @author poidij
 *
 */
@Entity
@Table(name = "COTISATION_ETABLISSEMENT")
@IdClass(CotisationEtablissementId.class)
@Data
@EqualsAndHashCode(callSuper = false, of = { "ligneEnCoursImportBatch", "idCotisationEtablissement" })
@ToString(callSuper = false, of = { "ligneEnCoursImportBatch", "idCotisationEtablissement" })
public class CotisationEtablissement extends EntiteImportableBatch {
    /**
     * UID de version.
     */
    private static final long serialVersionUID = 822362569368101808L;

    /**
     * Indicateur de ligne temporaire en cours d'import via Batch.
     * 
     * @param ligneEnCoursImportBatch
     *            le nom du SI d'où provient l'information
     * @return le nom du SI d'où provient l'information
     */
    @Id
    @Column(name = "TMP_BATCH")
    private boolean ligneEnCoursImportBatch;

    /**
     * Identifiant de la cotisationEtablissement
     *
     * @param idContact
     *            L'identifiant de la cotisationEtablissement
     * @return L'identifiant de la cotisationEtablissement
     */
    @Id
    @Column(name = "ID_COTIS_ETAB")
    private String idCotisationEtablissement;

    /**
     * Identifiant technique de l'adhésion
     *
     * @param idAdhEtabMois
     *            Identifiant technique de l'adhésion
     * @return Identifiant technique de l'adhésion
     */
    @Column(name = "ID_ADH_ETAB_MOIS")
    private String idAdhEtabMois;

    /**
     * L'adhésion.
     *
     * @param adhesionEtablissementMois
     *            l'adhésion
     * @return l'adhésion
     */
    @ManyToOne
    @Fetch(FetchMode.JOIN)
    @JoinColumns({ @JoinColumn(name = "ID_ADH_ETAB_MOIS", insertable = false, updatable = false),
            @JoinColumn(name = "TMP_BATCH", insertable = false, updatable = false) })
    private AdhesionEtablissementMois adhesionEtablissementMois;

    /**
     * S21.G00.82.001
     *
     * @param valeurCotisation
     *            S21.G00.82.001
     * @return S21.G00.82.001
     */
    @Column(name = "VALEUR_COTISATION")
    private Double valeurCotisation;

    /**
     * S21.G00.82.002
     *
     * @param codeCotisation
     *            S21.G00.82.002
     * @return S21.G00.82.002
     */
    @Column(name = "CODE_COTISATION")
    private String codeCotisation;

    /**
     * Libellé du code de cotisation.
     * 
     * @param libelleCodeCotisation
     *            le libellé du code de cotisation
     * @return le libellé du code de cotisation
     */
    @Formula("(SELECT p.LIBELLE_COURT FROM PARAM_CODE_LIBELLE p WHERE p.TBL = 'COTIS_ETAB' AND p.CHAMP = 'CODE_COTISATION' AND p.CODE = CODE_COTISATION)")
    private String libelleCodeCotisation;

    /**
     * S21.G00.82.003
     *
     * @param dateDebutPeriodeRattachement
     *            S21.G00.82.003
     * @return S21.G00.82.003
     */
    @Column(name = "DATE_DEB_PERIODE_RATTACHEMENT")
    private Integer dateDebutPeriodeRattachement;

    /**
     * S21.G00.82.004
     *
     * @param dateFinPeriodeRattachement
     *            S21.G00.82.004
     * @return S21.G00.82.004
     */
    @Column(name = "DATE_FIN_PERIODE_RATTACHEMENT")
    private Integer dateFinPeriodeRattachement;

    /**
     * Valeur de côtisation (telle que lue dans le fichier).
     *
     */
    @Transient
    private String valeurCotisationAsText;

    /**
     * Date de début de période de rattachement (telle que lue dans le fichier).
     *
     */
    @Transient
    private String dateDebutPeriodeRattachementAsText;

    /**
     * Date de fin de période de rattachement (telle que lue dans le fichier).
     *
     */
    @Transient
    private String dateFinPeriodeRattachementAsText;

    /**
     * Identifiant de la DSN dans la brique
     */
    @Column(name = "ID_DSN_BRIQUE")
    private BigInteger idDsnBrique;

    /**
     * Clé primaire de la table dans la brique
     */
    @Column(name = "PK_ENTITEE_BRIQUE")
    private BigInteger cleEntiteeBrique;

    /**
     * Renvoie la valeur de côtisation (telle que lue dans le fichier).
     * 
     * @return la valeur de côtisation
     */
    public String getValeurCotisationAsText() {
        if (valeurCotisationAsText != null) {
            return valeurCotisationAsText;
        }
        return new ConvertisseurNumeriqueDecimal().convertToEntityAttribute(getValeurCotisation());
    }

    /**
     * Valorise la valeur de côtisation (telle que lue dans le fichier).
     * 
     * @param valeurCotisation
     *            la valeur de côtisation à valoriser
     */
    public void setValeurCotisationAsText(String valeurCotisation) {
        this.valeurCotisationAsText = valeurCotisation;
        setValeurCotisation(new ConvertisseurNumeriqueDecimal().convertToDatabaseColumn(valeurCotisation));
    }

    /**
     * Date de début de période de rattachement (telle que lue dans le fichier).
     * 
     * @return le mois déclaré
     */
    public String getDateDebutPeriodeRattachementAsText() {
        if (dateDebutPeriodeRattachementAsText != null) {
            return dateDebutPeriodeRattachementAsText;
        }
        return new ConvertisseurDatesDDMMYYYY().convertToEntityAttribute(getDateDebutPeriodeRattachement());
    }

    /**
     * Valorise la date de début de période de rattachement (telle que lue dans le fichier).
     * 
     * @param dateDebutPeriodeRattachement
     *            la date de début de période de rattachement
     */
    public void setDateDebutPeriodeRattachementAsText(String dateDebutPeriodeRattachement) {
        this.dateDebutPeriodeRattachementAsText = dateDebutPeriodeRattachement;
        setDateDebutPeriodeRattachement(new ConvertisseurDatesDDMMYYYY().convertToDatabaseColumn(dateDebutPeriodeRattachement));
    }

    /**
     * Récupère la date de début de période de rattachement.
     * 
     * @return la date de début de périodee de rattachement
     */
    public Date getDateDebutPeriodeRattachementAsDate() {
        return DateRedac.convertitEnDate(getDateDebutPeriodeRattachement());
    }

    /**
     * Date de fin de période de rattachement (telle que lue dans le fichier).
     * 
     * @return la date de fin de période de rattachement
     */
    public String getDateFinPeriodeRattachementAsText() {
        if (dateFinPeriodeRattachementAsText != null) {
            return dateFinPeriodeRattachementAsText;
        }
        return new ConvertisseurDatesDDMMYYYY().convertToEntityAttribute(getDateFinPeriodeRattachement());
    }

    /**
     * Valorise la date de fin de période de rattachement (telle que lue dans le fichier).
     * 
     * @param dateFinPeriodeRattachement
     *            la date de fin de période de rattachement
     */
    public void setDatefinPeriodeRattachementAsText(String dateFinPeriodeRattachement) {
        this.dateFinPeriodeRattachementAsText = dateFinPeriodeRattachement;
        setDateFinPeriodeRattachement(new ConvertisseurDatesDDMMYYYY().convertToDatabaseColumn(dateFinPeriodeRattachement));
    }

    /**
     * Récupère la date de fin de période de rattachement.
     * 
     * @return la date de fin de périodee de rattachement
     */
    public Date getDateFinPeriodeRattachementAsDate() {
        return DateRedac.convertitEnDate(getDateFinPeriodeRattachement());
    }

    @Override
    public CotisationEtablissementId getId() {
        return new CotisationEtablissementId(ligneEnCoursImportBatch, idCotisationEtablissement);
    }

}
