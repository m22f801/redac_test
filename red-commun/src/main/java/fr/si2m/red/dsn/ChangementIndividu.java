package fr.si2m.red.dsn;

import java.math.BigInteger;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import fr.si2m.red.EntiteImportableBatch;
import fr.si2m.red.core.repository.jpa.conversion.ConvertisseurDatesDDMMYYYY;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * Modèle des changements des individus
 * 
 * @author poidij
 *
 */
@Entity
@Table(name = "CHANGEMENT_INDIVIDU")
@IdClass(ChangementIndividuId.class)
@Data
@EqualsAndHashCode(callSuper = false, of = { "ligneEnCoursImportBatch", "idChangementIndividu" })
@ToString(callSuper = false, of = { "ligneEnCoursImportBatch", "idChangementIndividu" })
public class ChangementIndividu extends EntiteImportableBatch {

    /**
     * UID de version.
     */
    private static final long serialVersionUID = 822362569368101808L;

    /**
     * Indicateur de ligne temporaire en cours d'import via Batch.
     * 
     * @param ligneEnCoursImportBatch
     *            le nom du SI d'où provient l'information
     * @return le nom du SI d'où provient l'information
     */
    @Id
    @Column(name = "TMP_BATCH")
    private boolean ligneEnCoursImportBatch;

    /**
     * Identifiant du changement de l'individu
     *
     * @param idChangementIndividu
     *            L'identifiant du changement de l'individu
     * @return L'identifiant du changement de l'individu
     */
    @Id
    @Column(name = "ID_CHGT_INDIVIDU")
    private String idChangementIndividu;

    /**
     * Identifiant de l'individu
     *
     * @param idIndividu
     *            L'identifiant de l'individu
     * @return L'identifiant de l'individu
     */
    @Column(name = "ID_INDIVIDU")
    private String idIndividu;

    /**
     * L'individu.
     *
     * @param individu
     *            L'individu
     * @return L'individu
     */
    @ManyToOne
    @Fetch(FetchMode.JOIN)
    @JoinColumns({ @JoinColumn(name = "ID_INDIVIDU", insertable = false, updatable = false),
            @JoinColumn(name = "TMP_BATCH", insertable = false, updatable = false) })
    private Individu individu;

    /**
     * S21.G00.31.001
     *
     * @param dateModification
     *            S21.G00.31.001
     * @return S21.G00.31.001
     */
    @Column(name = "DATE_MODIFICATION")
    private Integer dateModification;

    /**
     * S21.G00.31.008
     *
     * @param ancienIdentifiant
     *            S21.G00.31.008
     * @return S21.G00.31.008
     */
    @Column(name = "ANCIEN_IDENTIFIANT")
    private String ancienIdentifiant;

    /**
     * S21.G00.31.009
     *
     * @param ancienNomFamille
     *            S21.G00.31.009
     * @return S21.G00.31.009
     */
    @Column(name = "ANCIEN_NOM_FAMILLE")
    private String ancienNomFamille;

    /**
     * S21.G00.31.010
     *
     * @param ancienPrenom
     *            S21.G00.31.010
     * @return S21.G00.31.010
     */
    @Column(name = "ANCIEN_PRENOM")
    private String ancienPrenom;

    /**
     * S21.G00.31.011
     *
     * @param ancienneDateNaissance
     *            S21.G00.31.011
     * @return S21.G00.31.011
     */
    @Column(name = "ANCIENNE_DATE_NAISSANCE")
    private Integer ancienneDateNaissance;

    /**
     * Date de modification (telle que lue dans le fichier).
     * 
     */
    @Transient
    private String dateModificationAsText;

    /**
     * Ancienne date de Naissance (telle que lue dans le fichier).
     * 
     */
    @Transient
    private String ancienneDateNaissanceAsText;

    /**
     * Identifiant de la DSN dans la brique
     */
    @Column(name = "ID_DSN_BRIQUE")
    private BigInteger idDsnBrique;

    /**
     * Clé primaire de la table dans la brique
     */
    @Column(name = "PK_ENTITEE_BRIQUE")
    private BigInteger cleEntiteeBrique;

    /**
     * Date de modification (telle que lue dans le fichier).
     * 
     * @return la date de modification
     */
    public String getDateModificationAsText() {
        if (dateModificationAsText != null) {
            return dateModificationAsText;
        }
        return new ConvertisseurDatesDDMMYYYY().convertToEntityAttribute(getDateModification());
    }

    /**
     * Valorise la date de modification (telle que lue dans le fichier).
     * 
     * @param dateModification
     *            la date de modification à valoriser
     */
    public void setDateModificationAsText(String dateModification) {
        this.dateModificationAsText = dateModification;
        setDateModification(new ConvertisseurDatesDDMMYYYY().convertToDatabaseColumn(dateModification));
    }

    /**
     * Ancienne date de Naissance (telle que lue dans le fichier).
     * 
     * @return l'ancienne date de Naissance
     */
    public String getAncienneDateNaissanceAsText() {
        if (ancienneDateNaissanceAsText != null) {
            return ancienneDateNaissanceAsText;
        }
        return new ConvertisseurDatesDDMMYYYY().convertToEntityAttribute(getAncienneDateNaissance());
    }

    /**
     * Valorise l'ancienne date de Naissance (telle que lue dans le fichier).
     * 
     * @param ancienneDateNaissance
     *            l'ancienne date de Naissance à valoriser
     */
    public void setAncienneDateNaissanceAsText(String ancienneDateNaissance) {
        this.ancienneDateNaissanceAsText = ancienneDateNaissance;
        setAncienneDateNaissance(new ConvertisseurDatesDDMMYYYY().convertToDatabaseColumn(ancienneDateNaissance));
    }

    @Override
    public ChangementIndividuId getId() {
        return new ChangementIndividuId(ligneEnCoursImportBatch, idChangementIndividu);
    }

}
