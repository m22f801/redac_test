package fr.si2m.red.dsn;

import java.math.BigInteger;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.Formula;

import fr.si2m.red.EntiteImportableBatch;
import fr.si2m.red.core.repository.jpa.conversion.ConvertisseurNumeriqueDecimal;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * Modèle des composants de versement
 * 
 * @author poidij
 *
 */
@Entity
@Table(name = "COMPOSANT_VERSEMENT")
@IdClass(ComposantVersementId.class)
@Data
@EqualsAndHashCode(callSuper = false, of = { "ligneEnCoursImportBatch", "idComposantVersement" })
@ToString(callSuper = false, of = { "ligneEnCoursImportBatch", "idComposantVersement" })
public class ComposantVersement extends EntiteImportableBatch {

    /**
     * UID de version.
     */
    private static final long serialVersionUID = 822362569368101808L;

    /**
     * Indicateur de ligne temporaire en cours d'import via Batch.
     * 
     * @param ligneEnCoursImportBatch
     *            le nom du SI d'où provient l'information
     * @return le nom du SI d'où provient l'information
     */
    @Id
    @Column(name = "TMP_BATCH")
    private boolean ligneEnCoursImportBatch;

    /**
     * Identifiant du composant de versement
     *
     * @param idComposantVersement
     *            L'identifiant du composant de versement
     * @return L'identifiant du composant de versement
     */
    @Id
    @Column(name = "ID_COMPO_VERST")
    private String idComposantVersement;

    /**
     * Identifiant du versement
     *
     * @param idVersement
     *            L'identifiant du versement
     * @return L'identifiant du versement
     */
    @Column(name = "ID_VERSEMENT")
    private String idVersement;

    /**
     * Le versement.
     *
     * @param versement
     *            le versement
     * @return le versement
     */
    @ManyToOne
    @Fetch(FetchMode.JOIN)
    @JoinColumns({ @JoinColumn(name = "ID_VERSEMENT", insertable = false, updatable = false),
            @JoinColumn(name = "TMP_BATCH", insertable = false, updatable = false) })
    private Versement versement;

    /**
     * S21.G00.55.001
     *
     * @param montantVerse
     *            S21.G00.55.001
     * @return S21.G00.55.001
     */
    @Column(name = "MONTANT_VERSE")
    private Double montantVerse;

    /**
     * S21.G00.55.002
     *
     * @param typePopulation
     *            S21.G00.55.002
     * @return S21.G00.55.002
     */
    @Column(name = "TYPE_POPULATION")
    private String typePopulation;

    /**
     * S21.G00.55.003
     *
     * @param codeAffectation
     *            S21.G00.55.003
     * @return S21.G00.55.003
     */
    @Column(name = "CODE_AFFECTATION")
    private String codeAffectation;

    /**
     * La période d'affectation.
     *
     * @param periodeAffectation
     *            la période d'affectation
     * @return la période d'affectation
     */
    @Column(name = "PERIODE_AFFECTATION")
    private String periodeAffectation;

    /**
     * Pondération de la période d'affectation.
     *
     * @param poidsPeriodeAffectation
     *            le poids de la période d'affectation de ce composant de versement
     * @return le poids de la période d'affectation de ce composant de versement
     */
    @Formula("(CASE WHEN (PERIODE_AFFECTATION LIKE '____A__') THEN 60"
            + " WHEN (PERIODE_AFFECTATION LIKE '____S__') THEN (SUBSTRING(PERIODE_AFFECTATION, 6) * 27)"
            + " WHEN (PERIODE_AFFECTATION LIKE '____T__') THEN (SUBSTRING(PERIODE_AFFECTATION, 6) * 13)"
            + " WHEN (PERIODE_AFFECTATION LIKE '____M__') THEN (SUBSTRING(PERIODE_AFFECTATION, 6) * 4)" + " ELSE 45 END)")
    private String poidsPeriodeAffectation;

    /**
     * Code de l'identifiant du sous-fond.
     *
     * @param codeIdentifiantSousFonds
     *            Le code de l'identifiant du sous fond
     * @return Le code de l'identifiant du sous fond
     */
    @Column(name = "CODE_ID_SOUSFONDS")
    private String codeIdentifiantSousFonds;

    /**
     * Date de début de la période d'affectation.
     *
     * @param dateDebutPeriodeAffectation
     *            La date de début de la période d'affectation
     * @return La date de début de la période d'affectation
     */
    @Column(name = "DT_DEB_PERIODE_AFFECTATION")
    private Integer dateDebutPeriodeAffectation;

    /**
     * Date de fin de la période d'affectation.
     *
     * @param dateFinPeriodeAffectation
     *            La date de fin de la période d'affectation
     * @return La date de fin de la période d'affectation
     */
    @Column(name = "DT_FIN_PERIODE_AFFECTATION")
    private Integer dateFinPeriodeAffectation;

    /**
     * Montant versé (tel que lu dans le fichier).
     *
     */
    @Transient
    private String montantVerseAsText;

    /**
     * Identifiant de la DSN dans la brique
     */
    @Column(name = "ID_DSN_BRIQUE")
    private BigInteger idDsnBrique;

    /**
     * Clé primaire de la table dans la brique
     */
    @Column(name = "PK_ENTITEE_BRIQUE")
    private BigInteger cleEntiteeBrique;

    /**
     * Renvoie le montant versé (tel que lu dans le fichier).
     * 
     * @return le montant versé
     */
    public String getMontantVerseAsText() {
        String value = this.montantVerseAsText;
        if (StringUtils.isBlank(value) && getMontantVerse() != null) {
            value = new ConvertisseurNumeriqueDecimal().convertToEntityAttribute(getMontantVerse());
        }
        return value;
    }

    /**
     * Valorise le montant versé (tel que lu dans le fichier).
     * 
     * @param montantVerse
     *            le montant versé à valoriser
     */
    public void setMontantVerseAsText(String montantVerse) {
        this.montantVerseAsText = montantVerse;
        setMontantVerse(new ConvertisseurNumeriqueDecimal().convertToDatabaseColumn(montantVerse));
    }

    @Override
    public ComposantVersementId getId() {
        return new ComposantVersementId(ligneEnCoursImportBatch, idComposantVersement);
    }

}
