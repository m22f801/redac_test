package fr.si2m.red.dsn;

import java.math.BigInteger;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.apache.commons.lang3.StringUtils;

import fr.si2m.red.EntiteImportableBatch;
import fr.si2m.red.core.repository.jpa.conversion.ConvertisseurDatesDDMMYYYY;
import fr.si2m.red.core.repository.jpa.conversion.ConvertisseurDonneeSalaire;
import fr.si2m.red.core.repository.jpa.conversion.ConvertisseurNumeriqueDecimal;
import fr.si2m.red.core.repository.jpa.conversion.ConvertisseurNumeriqueInteger;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * Modèle des versements des individus
 * 
 * @author poidij
 *
 */
@Entity
@Table(name = "VERSEMENT_INDIVIDU")
@IdClass(VersementIndividuId.class)
@Data
@EqualsAndHashCode(callSuper = false, of = { "ligneEnCoursImportBatch", "idVersementIndividu" })
@ToString(callSuper = false, of = { "ligneEnCoursImportBatch", "idVersementIndividu" })
public class VersementIndividu extends EntiteImportableBatch {
    /**
     * UID de version.
     */
    private static final long serialVersionUID = 822362569368101808L;

    /**
     * Indicateur de ligne temporaire en cours d'import via Batch.
     * 
     * @param ligneEnCoursImportBatch
     *            le nom du SI d'où provient l'information
     * @return le nom du SI d'où provient l'information
     */
    @Id
    @Column(name = "TMP_BATCH")
    private boolean ligneEnCoursImportBatch;

    /**
     * Identifiant du versement de l'individu
     *
     * @param idVersementIndividu
     *            L'identifiant du versement de l'individu
     * @return L'identifiant du versement de l'individu
     */
    @Id
    @Column(name = "ID_VERS_INDIV")
    private String idVersementIndividu;

    /**
     * Identifiant de l'individu
     *
     * @param idIndividu
     *            L'identifiant de l'individu
     * @return L'identifiant de l'individu
     */
    @Column(name = "ID_INDIVIDU")
    private String idIndividu;

    /**
     * S21.G00.50.001
     *
     * @param dateVersement
     *            S21.G00.50.001
     * @return S21.G00.50.001
     */
    @Column(name = "DATE_VERSEMENT")
    private Integer dateVersement;

    /**
     * S21.G00.50.002
     *
     * @param remunerationNette
     *            S21.G00.50.002
     * @return S21.G00.50.002
     */
    @Column(name = "REMUNERATION_NETTE")
    @Convert(converter = ConvertisseurDonneeSalaire.class)
    private Double remunerationNette;

    /**
     * S21.G00.50.003
     *
     * @param numeroVersement
     *            S21.G00.50.003
     * @return S21.G00.50.003
     */
    @Column(name = "NUMERO_VERSEMENT")
    private Integer numeroVersement;

    /**
     * Montant de côtisation (tel que lu dans le fichier).
     *
     */
    @Transient
    private String remunerationNetteAsText;

    /**
     * Date de versement (telle que lue dans le fichier).
     *
     */
    @Transient
    private String dateVersementAsText;

    /**
     * Le numéro de versement lu dans un fichier d'import.
     *
     * @param numeroVersementAsText
     * 
     * @return le numéro de versement lu dans un fichier d'import
     */
    @Transient
    private String numeroVersementAsText;

    /**
     * Identifiant de la DSN dans la brique
     */
    @Column(name = "ID_DSN_BRIQUE")
    private BigInteger idDsnBrique;

    /**
     * Clé primaire de la table dans la brique
     */
    @Column(name = "PK_ENTITEE_BRIQUE")
    private BigInteger cleEntiteeBrique;

    /**
     * Renvoie la remuneration nette (telle que lue dans le fichier).
     * 
     * @return la remuneration nette
     */
    public String getRemunerationNetteAsText() {
        String value = this.remunerationNetteAsText;
        if (StringUtils.isBlank(value) && getRemunerationNette() != null) {
            value = new ConvertisseurNumeriqueDecimal().convertToEntityAttribute(getRemunerationNette());
        }
        return value;
    }

    /**
     * Valorise la remuneration nette (telle que lue dans le fichier).
     * 
     * @param remunerationNette
     *            la remuneration nette à valoriser
     */
    public void setRemunerationNetteAsText(String remunerationNette) {
        this.remunerationNetteAsText = remunerationNette;
        setRemunerationNette(new ConvertisseurNumeriqueDecimal().convertToDatabaseColumn(remunerationNette));
    }

    /**
     * Date de versement (telle que lue dans le fichier).
     * 
     * @return la date de versement
     */
    public String getDateVersementAsText() {
        if (dateVersementAsText != null) {
            return dateVersementAsText;
        }
        return new ConvertisseurDatesDDMMYYYY().convertToEntityAttribute(getDateVersement());
    }

    /**
     * Valorise la date de versement (telle que lue dans le fichier).
     * 
     * @param dateVersement
     *            la date de versement à valoriser
     */
    public void setDateVersementAsText(String dateVersement) {
        this.dateVersementAsText = dateVersement;
        setDateVersement(new ConvertisseurDatesDDMMYYYY().convertToDatabaseColumn(dateVersement));
    }

    /**
     * Numéro de versement (tel que lu dans le fichier).
     *
     * @return le numéro de versement
     */
    public String getNumeroVersementAsText() {
        if (numeroVersementAsText != null) {
            return numeroVersementAsText;
        } else {
            return new ConvertisseurNumeriqueInteger().convertToEntityAttribute(getNumeroVersement());
        }
    }

    /**
     * Valorise le numéro de versement (tel que lu dans le fichier).
     * 
     * @param numeroVersement
     *            numéro de versement à valoriser
     */
    public void setNumeroVersementAsText(String numeroVersement) {
        this.numeroVersementAsText = numeroVersement;
        setNumeroVersement(new ConvertisseurNumeriqueInteger().convertToDatabaseColumn(numeroVersement));
    }

    @Override
    public VersementIndividuId getId() {
        return new VersementIndividuId(ligneEnCoursImportBatch, idVersementIndividu);
    }

}
