package fr.si2m.red.dsn;

import fr.si2m.red.core.repository.EntiteImportableRepository;

/**
 * Référentiel des ayants droit.
 * 
 * @author poidij
 *
 */
public interface AyantDroitRepository extends EntiteImportableRepository<AyantDroit> {

    /**
     * Indique l'existence d'un Ayant Droit en base.
     * 
     * @param idAyantDroit
     *            l'identifiant de l'ayant droit
     * @return true s'il existe au moins un ayant droit ayant cet identifiant, false sinon
     */
    boolean existeUnAyantDroit(String idAyantDroit);

}
