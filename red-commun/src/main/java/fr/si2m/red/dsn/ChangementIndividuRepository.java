package fr.si2m.red.dsn;

import java.util.List;

import fr.si2m.red.core.repository.EntiteImportableRepository;

/**
 * Référentiel des changements des individus.
 * 
 * @author poidij
 *
 */
public interface ChangementIndividuRepository extends EntiteImportableRepository<ChangementIndividu> {

    /**
     * Récupère les éventuels changements sur un individu donné.
     * 
     * @param idIndividu
     *            l'identifiant de l'individu
     * @return les éventuels changements sur l'individu
     */
    List<ChangementIndividu> getPourIndividu(String idIndividu);

    /**
     * Indique l'existence d'un Changement Individu en base.
     * 
     * @param idChangementIndividu
     *            l'identifiant du changement
     * @return true s'il existe au moins un changement ayant cet identifiant, false sinon
     */
    boolean existeUnChangementIndividu(String idChangementIndividu);

}
