package fr.si2m.red.dsn;

import fr.si2m.red.DateRedac;
import lombok.Data;

/**
 * Résumé d'une affiliation (avec informations consolidées).
 * 
 * @author nortaina
 *
 */
@Data
public class ResumeAffiliationExportExcel {

    /**
     * S21.G00.70.005
     *
     * @param motifRupture
     *            S21.G00.70.005
     * @return S21.G00.70.005
     */
    private String motifRupture;

    /**
     * S21.G00.70.005
     *
     * @param codePopulation
     *            S21.G00.70.005
     * @return S21.G00.70.005
     */
    private String codePopulation;

    /**
     * S21.G00.40.001
     *
     * @param dateDebutContrat
     *            S21.G00.40.001
     * @return S21.G00.40.001
     */
    private Integer dateDebutContrat;

    /**
     * S21.G00.40.010
     *
     * @param dateFinPrevisionnelle
     *            S21.G00.40.010
     * @return S21.G00.40.010
     */
    private Integer dateFinPrevisionnelle;

    /**
     * S21.G00.30.001
     *
     * @param identifiantRepertoire
     *            S21.G00.30.001
     * @return S21.G00.30.001
     */
    private String identifiantRepertoire;

    /**
     * S21.G00.30.002
     *
     * @param nomFamille
     *            S21.G00.30.002
     * @return S21.G00.30.002
     */
    private String nomFamille;

    /**
     * S21.G00.30.003
     *
     * @param nomUsage
     *            S21.G00.30.003
     * @return S21.G00.30.003
     */
    private String nomUsage;

    /**
     * S21.G00.30.004
     *
     * @param prenom
     *            S21.G00.30.004
     * @return S21.G00.30.004
     */
    private String prenom;

    /**
     * S21.G00.30.005
     *
     * @param sexe
     *            S21.G00.30.005
     * @return S21.G00.30.005
     */
    private String sexe;

    /**
     * S21.G00.30.006
     *
     * @param dateNaissance
     *            S21.G00.30.006
     * @return S21.G00.30.006
     */
    private Integer dateNaissance;

    /**
     * S21.G00.30.019
     *
     * @param matricule
     *            S21.G00.30.019
     * @return S21.G00.30.019
     */
    private String matricule;

    /**
     * S21.G00.30.020
     *
     * @param numeroTechniqueTemporaire
     *            S21.G00.30.020
     * @return S21.G00.30.020
     */
    private String numeroTechniqueTemporaire;

    /**
     * Le montant sur la tranche 1.
     *
     * @param montantTranche1
     *            le montant sur la tranche 1
     * @return le montant sur la tranche 1
     */
    private Double montantTranche1;

    /**
     * Le montant sur la tranche 2.
     *
     * @param montantTranche2
     *            le montant sur la tranche 2
     * @return le montant sur la tranche 2
     */
    private Double montantTranche2;

    /**
     * Le montant sur la tranche 3.
     *
     * @param montantTranche3
     *            le montant sur la tranche 3
     * @return le montant sur la tranche 3
     */
    private Double montantTranche3;

    /**
     * Le montant sur la tranche 4.
     *
     * @param montantTranche4
     *            le montant sur la tranche 4
     * @return le montant sur la tranche 4
     */
    private Double montantTranche4;

    /**
     * Le montant composant base assujettie, type 10.
     *
     * @param montantCBA10
     *            le montant composant base assujettie, type 10
     * @return le montant composant base assujettie, type 10
     */
    private Double montantCBA10;

    /**
     * Le montant composant base assujettie, type 11.
     *
     * @param montantCBA11
     *            le montant composant base assujettie, type 11
     * @return le montant composant base assujettie, type 11
     */
    private Double montantCBA11;

    /**
     * Le montant composant base assujettie, type 12.
     *
     * @param montantCBA2
     *            le montant composant base assujettie, type 12
     * @return le montant composant base assujettie, type 12
     */
    private Double montantCBA12;

    /**
     * Le montant composant base assujettie, type 13.
     *
     * @param montantCBA13
     *            le montant composant base assujettie, type 13
     * @return le montant composant base assujettie, type 13
     */
    private Double montantCBA13;

    /**
     * Le montant composant base assujettie, type 14.
     *
     * @param montantCBA14
     *            le montant composant base assujettie, type 14
     * @return le montant composant base assujettie, type 14
     */
    private Double montantCBA14;

    /**
     * Le montant composant base assujettie, type 15.
     *
     * @param montantCBA15
     *            le montant composant base assujettie, type 15
     * @return le montant composant base assujettie, type 15
     */
    private Double montantCBA15;

    /**
     * Le montant composant base assujettie, type 16.
     *
     * @param montantCBA16
     *            le montant composant base assujettie, type 16
     * @return le montant composant base assujettie, type 16
     */
    private Double montantCBA16;

    /**
     * Le montant composant base assujettie, type 17.
     *
     * @param montantCBA17
     *            le montant composant base assujettie, type 17
     * @return le montant composant base assujettie, type 17
     */
    private Double montantCBA17;

    /**
     * Le montant composant base assujettie, type 18.
     *
     * @param montantCBA18
     *            le montant composant base assujettie, type 18
     * @return le montant composant base assujettie, type 18
     */
    private Double montantCBA18;

    /**
     * Le montant composant base assujettie, type 19.
     *
     * @param montantCBA19
     *            le montant composant base assujettie, type 19
     * @return le montant composant base assujettie, type 19
     */
    private Double montantCBA19;

    /**
     * Le montant composant base assujettie, type 20.
     *
     * @param montantCBA20
     *            le montant composant base assujettie, type 20
     * @return le montant composant base assujettie, type 20
     */
    private Double montantCBA20;

    /**
     * Le montant composant base assujettie, type 21.
     *
     * @param montantCBA21
     *            le montant composant base assujettie, type 21
     * @return le montant composant base assujettie, type 21
     */
    private Double montantCBA21;

    /**
     * Le montant composant base assujettie, type 24.
     *
     * @param montantCBA24
     *            le montant composant base assujettie, type 24
     * @return le montant composant base assujettie, type 24
     */
    private Double montantCBA24;

    /**
     * Mois de rattachement
     *
     * @param moisRattachement
     *            Mois de rattachement de l'adhésion de l'affiliation courante
     * @return Mois de rattachement de l'adhésion de l'affiliation courante
     */
    private Integer moisRattachement;

    /**
     * Changement dans la periode
     *
     * @param chgtPeriode
     *            Indicateur du changement dans la periode
     * @return Indicateur du changement dans la periode
     */
    private String chgtPeriode;

    /**
     * Libellé Population SI source
     *
     * 
     * @param libPopulation
     *            Libelle population pour l'affiliation
     * @return Libelle population pour l'affiliation
     */
    private String libPopulation;

    /**
     * S21.G00.70.014
     *
     * @param dateDebutAffiliation;
     *            S21.G00.70.014
     * @return S21.G00.70.014
     */

    private Integer dateDebutAffiliation;

    /**
     * S21.G00.70.015
     *
     * @param dateFinAffiliation;
     *            S21.G00.70.015
     * @return S21.G00.70.015
     */
    private Integer dateFinAffiliation;

    /**
     * Somme des montants de cotisation
     *
     * @param sommeMontantsCotisation
     * 
     * @return Somme des montants de cotisation
     */
    private Double sommeMontantsCotisation;

    /**
     * Conversion en String JJ/MM/AAAA de DateDebutAffiliation dans validation calendaire
     * 
     * @return String JJ/MM/AAAA de DateDebutAffiliation
     */
    public String getDateDebutAffiliationSansValidation() {
        return DateRedac.convertionDateRedacSansValidation(getDateDebutAffiliation());
    }

    /**
     * Conversion en String JJ/MM/AAAA de DateFinAffiliation dans validation calendaire
     * 
     * @return String JJ/MM/AAAA de DateFinAffiliation
     */
    public String getDateFinAffiliationSansValidation() {
        return DateRedac.convertionDateRedacSansValidation(getDateFinAffiliation());
    }

}
