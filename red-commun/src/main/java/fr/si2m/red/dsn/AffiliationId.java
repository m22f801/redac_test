package fr.si2m.red.dsn;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * Modèle des affiliations
 * 
 * @author poidij
 *
 */
@Data
@EqualsAndHashCode(callSuper = false, of = { "ligneEnCoursImportBatch", "idAffiliation" })
@ToString(callSuper = false, of = { "ligneEnCoursImportBatch", "idAffiliation" })
@NoArgsConstructor
@AllArgsConstructor
public class AffiliationId implements Serializable {

    /**
     * UID de version.
     */
    private static final long serialVersionUID = 822362569368101808L;

    /**
     * Indicateur de ligne temporaire en cours d'import via Batch.
     * 
     * @param ligneEnCoursImportBatch
     *            le nom du SI d'où provient l'information
     * @return le nom du SI d'où provient l'information
     */
    private boolean ligneEnCoursImportBatch;

    /**
     * Identifiant de l'affiliation
     *
     * @param idAffiliation
     *            L'identifiant de l'affiliation
     * @return L'identifiant de l'affiliation
     */
    private String idAffiliation;

}
