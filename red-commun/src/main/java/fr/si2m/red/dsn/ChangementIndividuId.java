package fr.si2m.red.dsn;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * Modèle des changements des individus
 * 
 * @author poidij
 *
 */
@Data
@EqualsAndHashCode(callSuper = false, of = { "ligneEnCoursImportBatch", "idChangementIndividu" })
@ToString(callSuper = false, of = { "ligneEnCoursImportBatch", "idChangementIndividu" })
@NoArgsConstructor
@AllArgsConstructor
public class ChangementIndividuId implements Serializable {

    /**
     * UID de version.
     */
    private static final long serialVersionUID = 822362569368101808L;

    /**
     * Indicateur de ligne temporaire en cours d'import via Batch.
     * 
     * @param ligneEnCoursImportBatch
     *            le nom du SI d'où provient l'information
     * @return le nom du SI d'où provient l'information
     */
    private boolean ligneEnCoursImportBatch;

    /**
     * Identifiant du changement de l'individu
     *
     * @param idChangementIndividu
     *            L'identifiant du changement de l'individu
     * @return L'identifiant du changement de l'individu
     */
    private String idChangementIndividu;

}
