package fr.si2m.red.dsn;

import fr.si2m.red.core.repository.EntiteImportableRepository;

/**
 * Référentiel des individus.
 * 
 * @author poidij
 *
 */
public interface IndividuRepository extends EntiteImportableRepository<Individu> {

    /**
     * Indique l'existence d'un Individu en base
     * 
     * @param idIndividu
     *            l'identifiant de l'individu
     * @return true s'il existe au moins un individu ayant cet identifiant, false sinon
     */
    boolean existeUnIndividu(String idIndividu);

    /**
     * Récupère l'individu rattaché à une période donnée pour le mois de rattachement le plus récent.
     * 
     * @param idPeriode
     *            l'identifiant de la période
     * @param individu
     *            l'identifiant de répertoire ou le numéro technique temporaire de la catégorie de quittancement rattachée à l'individu
     * @return l'individu rattaché à la période pour le mois de rattachement le plus récent
     */
    Individu getPourPeriode(Long idPeriode, String individu);

}
