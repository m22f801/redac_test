package fr.si2m.red.dsn;

import lombok.Data;

/**
 * Résumé des messages controles SIGNAL et REJET pour un individu
 * 
 * @author eudesr
 *
 */
@Data
public class MessageControleExportExcel {

    /**
     * Les messages avec un niveau REJET , concaténés
     */
    private String messageUtilisateurRejet;

    /**
     * Les messages avec un niveau SIGNAL, concaténés
     */
    private String messageUtilisateurSignal;

}
