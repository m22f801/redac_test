package fr.si2m.red.dsn;

import lombok.Data;

/**
 * Paramètres pour le partionning du traitement des périodes (reconsolidation sur salaires).
 * 
 * @author poidij
 *
 */
@Data
public class ParametresPartitionPeriodes {

    /**
     * Nombre de périodes à traiter
     *
     * @param nbPeriodes
     *            nombre de périodes à traiter
     * @return nombre de périodes à traiter
     */
    private Integer nbPeriodes;

    /**
     * Identifiant de période minimal parmi les périodes à traiter
     *
     * @param minIdPeriode
     *            identifiant de période minimal parmi les périodes à traiter
     * @return identifiant de période minimal parmi les périodes à traiter
     */
    private Long minIdPeriode;

    /**
     * Identifiant de période maximal parmi les périodes à traiter
     *
     * @param maxIdPeriode
     *            identifiant de période maximal parmi les périodes à traiter
     * @return identifiant de période maximal parmi les périodes à traiter
     */
    private Long maxIdPeriode;

}
