package fr.si2m.red.dsn;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * Modèle des adhésions établissement mois
 * 
 * @author poidij
 *
 */
@Data
@EqualsAndHashCode(callSuper = false, of = { "ligneEnCoursImportBatch", "idAdhEtabMois" })
@ToString(callSuper = false, of = { "ligneEnCoursImportBatch", "idAdhEtabMois" })
@NoArgsConstructor
@AllArgsConstructor
public class AdhesionEtablissementMoisId implements Serializable {

    /**
     * UID de version.
     */
    private static final long serialVersionUID = 822362569368101808L;

    /**
     * Indicateur de ligne temporaire en cours d'import via Batch.
     * 
     * @param ligneEnCoursImportBatch
     *            le nom du SI d'où provient l'information
     * @return le nom du SI d'où provient l'information
     */
    private boolean ligneEnCoursImportBatch;

    /**
     * Identifiant technique unique pour chaque ligne de l’ensemble des différents fichiers publiés ADHESION_ETAB_MOIS
     *
     * @param idAdhEtabMois
     *            Identifiant technique unique pour chaque ligne de l’ensemble des différents fichiers publiés ADHESION_ETAB_MOIS
     * @return Identifiant technique unique pour chaque ligne de l’ensemble des différents fichiers publiés ADHESION_ETAB_MOIS
     */
    private String idAdhEtabMois;

}
