package fr.si2m.red.dsn;

import java.math.BigInteger;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

import fr.si2m.red.EntiteImportableBatch;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * Modèle des contacts
 * 
 * @author poidij
 *
 */
@Entity
@Table(name = "CONTACT")
@IdClass(ContactId.class)
@Data
@EqualsAndHashCode(callSuper = false, of = { "ligneEnCoursImportBatch", "idContact" })
@ToString(callSuper = false, of = { "ligneEnCoursImportBatch", "idContact" })
public class Contact extends EntiteImportableBatch {

    /**
     * UID de version.
     */
    private static final long serialVersionUID = 822362569368101808L;

    /**
     * Indicateur de ligne temporaire en cours d'import via Batch.
     * 
     * @param ligneEnCoursImportBatch
     *            le nom du SI d'où provient l'information
     * @return le nom du SI d'où provient l'information
     */
    @Id
    @Column(name = "TMP_BATCH")
    private boolean ligneEnCoursImportBatch;

    /**
     * Identifiant du contact
     *
     * @param idContact
     *            l'identifiant du contact
     * @return l'identifiant du contact
     */
    @Id
    @Column(name = "ID_CONTACT")
    private String idContact;

    /**
     * Identifiant technique de l'adhésion
     *
     * @param idAdhEtabMois
     *            Identifiant technique de l'adhésion
     * @return Identifiant technique de l'adhésion
     */
    @Column(name = "ID_ADH_ETAB_MOIS")
    private String idAdhEtabMois;

    /**
     * S20.G00.07.001 Nom et prénom du contact
     *
     * @param nomContact
     *            S20.G00.07.001 Nom et prénom du contact
     * @return S20.G00.07.001 Nom et prénom du contact
     */
    @Column(name = "NOM_CONTACT")
    private String nomContact;

    /**
     * S20.G00.07.002 Adresse téléphonique
     *
     * @param telContact
     *            S20.G00.07.002 Adresse téléphonique
     * @return S20.G00.07.002 Adresse téléphonique
     */
    @Column(name = "TEL_CONTACT")
    private String telContact;

    /**
     * S20.G00.07.003 Adresse mél du contact
     *
     * @param mailContact
     *            S20.G00.07.003 Adresse mél du contact
     * @return S20.G00.07.003 Adresse mél du contact
     */
    @Column(name = "MAIL_CONTACT")
    private String mailContact;

    /**
     * S20.G00.07.004 Type
     *
     * @param typeContact
     *            S20.G00.07.004 Type
     * @return S20.G00.07.004 Type
     */
    @Column(name = "TYPE_CONTACT")
    private String typeContact;

    /**
     * Identifiant de la DSN dans la brique
     */
    @Column(name = "ID_DSN_BRIQUE")
    private BigInteger idDsnBrique;

    /**
     * Clé primaire de la table dans la brique
     */
    @Column(name = "PK_ENTITEE_BRIQUE")
    private BigInteger cleEntiteeBrique;

    @Override
    public ContactId getId() {
        return new ContactId(ligneEnCoursImportBatch, idContact);
    }

}
