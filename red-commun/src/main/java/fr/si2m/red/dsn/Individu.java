package fr.si2m.red.dsn;

import java.math.BigInteger;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import fr.si2m.red.EntiteImportableBatch;
import fr.si2m.red.core.repository.jpa.conversion.ConvertisseurDatesDDMMYYYY;
import fr.si2m.red.core.repository.jpa.conversion.ConvertisseurNumeriqueInteger;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * Modèle des individus.
 * 
 * @author poidij
 *
 */
@Entity
@Table(name = "INDIVIDU")
@IdClass(IndividuId.class)
@Data
@EqualsAndHashCode(callSuper = false, of = { "ligneEnCoursImportBatch", "idIndividu" })
@ToString(callSuper = false, of = { "ligneEnCoursImportBatch", "idIndividu" })
public class Individu extends EntiteImportableBatch {
    /**
     * UID de version.
     */
    private static final long serialVersionUID = 822362569368101808L;

    /**
     * Indicateur de ligne temporaire en cours d'import via Batch.
     * 
     * @param ligneEnCoursImportBatch
     *            le nom du SI d'où provient l'information
     * @return le nom du SI d'où provient l'information
     */
    @Id
    @Column(name = "TMP_BATCH")
    private boolean ligneEnCoursImportBatch;

    /**
     * Identifiant de l'individu.
     *
     * @param idIndividu
     *            L'identifiant de l'individu
     * @return L'identifiant de l'individu
     */
    @Id
    @Column(name = "ID_INDIVIDU")
    private String idIndividu;

    /**
     * Identifiant technique de l'adhésion.
     *
     * @param idAdhEtabMois
     *            Identifiant technique de l'adhésion
     * @return Identifiant technique de l'adhésion
     */
    @Column(name = "ID_ADH_ETAB_MOIS")
    private String idAdhEtabMois;

    /**
     * L'adhésion.
     *
     * @param adhesionEtablissementMois
     *            l'adhésion
     * @return l'adhésion
     */
    @ManyToOne
    @Fetch(FetchMode.JOIN)
    @JoinColumns({ @JoinColumn(name = "ID_ADH_ETAB_MOIS", insertable = false, updatable = false),
            @JoinColumn(name = "TMP_BATCH", insertable = false, updatable = false) })
    private AdhesionEtablissementMois adhesionEtablissementMois;

    /**
     * S21.G00.30.001
     *
     * @param identifiantRepertoire
     *            S21.G00.30.001
     * @return S21.G00.30.001
     */
    @Column(name = "IDENTIFIANT_REPERTOIRE")
    private String identifiantRepertoire;

    /**
     * S21.G00.30.002
     *
     * @param nomFamille
     *            S21.G00.30.002
     * @return S21.G00.30.002
     */
    @Column(name = "NOM_FAMILLE")
    private String nomFamille;

    /**
     * S21.G00.30.003
     *
     * @param nomUsage
     *            S21.G00.30.003
     * @return S21.G00.30.003
     */
    @Column(name = "NOM_USAGE")
    private String nomUsage;

    /**
     * S21.G00.30.004
     *
     * @param prenom
     *            S21.G00.30.004
     * @return S21.G00.30.004
     */
    @Column(name = "PRENOM")
    private String prenom;

    /**
     * S21.G00.30.005
     *
     * @param sexe
     *            S21.G00.30.005
     * @return S21.G00.30.005
     */
    @Column(name = "SEXE")
    private String sexe;

    /**
     * S21.G00.30.006
     *
     * @param dateNaissance
     *            S21.G00.30.006
     * @return S21.G00.30.006
     */
    @Column(name = "DATE_NAISSANCE")
    private Integer dateNaissance;

    /**
     * S21.G00.30.007
     *
     * @param lieuNaissance
     *            S21.G00.30.007
     * @return S21.G00.30.007
     */
    @Column(name = "LIEU_NAISSANCE")
    private String lieuNaissance;

    /**
     * S21.G00.30.008
     *
     * @param voie
     *            S21.G00.30.008
     * @return S21.G00.30.008
     */
    @Column(name = "VOIE")
    private String voie;

    /**
     * S21.G00.30.009
     *
     * @param codePostal
     *            S21.G00.30.009
     * @return S21.G00.30.009
     */
    @Column(name = "CODE_POSTAL")
    private String codePostal;

    /**
     * S21.G00.30.010
     *
     * @param localite
     *            S21.G00.30.010
     * @return S21.G00.30.010
     */
    @Column(name = "LOCALITE")
    private String localite;

    /**
     * S21.G00.30.011
     *
     * @param codePays
     *            S21.G00.30.011
     * @return S21.G00.30.011
     */
    @Column(name = "CODE_PAYS")
    private String codePays;

    /**
     * S21.G00.30.012
     *
     * @param codeDistribution
     *            S21.G00.30.012
     * @return S21.G00.30.012
     */
    @Column(name = "CODE_DISTRIBUTION")
    private String codeDistribution;

    /**
     * S21.G00.30.014
     *
     * @param departementNaissance
     *            S21.G00.30.014
     * @return S21.G00.30.014
     */
    @Column(name = "DEPARTEMENT_NAISSANCE")
    private String departementNaissance;

    /**
     * S21.G00.30.015
     *
     * @param paysNaissance
     *            S21.G00.30.015
     * @return S21.G00.30.015
     */
    @Column(name = "PAYS_NAISSANCE")
    private String paysNaissance;

    /**
     * S21.G00.30.016
     *
     * @param complementConstruction
     *            S21.G00.30.016
     * @return S21.G00.30.016
     */
    @Column(name = "COMPLEMENT_CONSTRUCTION")
    private String complementConstruction;

    /**
     * S21.G00.30.017
     *
     * @param complementVoie
     *            S21.G00.30.017
     * @return S21.G00.30.017
     */
    @Column(name = "COMPLEMENT_VOIE")
    private String complementVoie;

    /**
     * S21.G00.30.018
     *
     * @param adresseMail
     *            S21.G00.30.018
     * @return S21.G00.30.018
     */
    @Column(name = "MAIL")
    private String adresseMail;

    /**
     * S21.G00.30.019
     *
     * @param matricule
     *            S21.G00.30.019
     * @return S21.G00.30.019
     */
    @Column(name = "MATRICULE")
    private String matricule;

    /**
     * S21.G00.30.020
     *
     * @param numeroTechniqueTemporaire
     *            S21.G00.30.020
     * @return S21.G00.30.020
     */
    @Column(name = "NTT")
    private String numeroTechniqueTemporaire;

    /**
     * S21.G00.30.021
     *
     * @param nombreEnfantCharge
     *            S21.G00.30.021
     * @return S21.G00.30.021
     */
    @Column(name = "NOMBRE_ENFANTS")
    private Integer nombreEnfantCharge;

    /**
     * S21.G00.30.022
     *
     * @param statutEtranger
     *            S21.G00.30.022
     * @return S21.G00.30.022
     */
    @Column(name = "STATUT_ETRANGER")
    private String statutEtranger;

    /**
     * S21.G00.30.023
     *
     * @param embauche
     *            S21.G00.30.023
     * @return S21.G00.30.023
     */
    @Column(name = "EMBAUCHE")
    private String embauche;

    /**
     * Date de naissance (telle que lue dans le fichier).
     * 
     * @param dateNaissanceAsText
     * 
     * @return le date de naissance lue dans un fichier d'import
     */
    @Transient
    private String dateNaissanceAsText;

    /**
     * Le nombre d'enfants à charge lu dans un fichier d'import.
     *
     * @param nombreEnfantChargeAsText
     * 
     * @return le nombre d'enfants à charge lu dans un fichier d'import
     */
    @Transient
    private String nombreEnfantChargeAsText;

    /**
     * Identifiant de la DSN dans la brique
     */
    @Column(name = "ID_DSN_BRIQUE")
    private BigInteger idDsnBrique;

    /**
     * Clé primaire de la table dans la brique
     */
    @Column(name = "PK_ENTITEE_BRIQUE")
    private BigInteger cleEntiteeBrique;

    /**
     * Date de naissance (telle que lue dans le fichier).
     * 
     * @return la date de naissance
     */
    public String getDateNaissanceAsText() {
        if (dateNaissanceAsText != null) {
            return dateNaissanceAsText;
        }
        return new ConvertisseurDatesDDMMYYYY().convertToEntityAttribute(getDateNaissance());
    }

    /**
     * Valorise la date de naissance (telle que lue dans le fichier).
     * 
     * @param dateNaissance
     *            la date de naissance à valoriser
     */
    public void setDateNaissanceAsText(String dateNaissance) {
        this.dateNaissanceAsText = dateNaissance;
        setDateNaissance(new ConvertisseurDatesDDMMYYYY().convertToDatabaseColumn(dateNaissance));
    }

    /**
     * Nombre d'enfants à charge (tel que lu dans le fichier).
     *
     * @return le nombre d'enfants à charge
     */
    public String getNombreEnfantChargeAsText() {
        if (nombreEnfantChargeAsText != null) {
            return nombreEnfantChargeAsText;
        } else {
            return new ConvertisseurNumeriqueInteger().convertToEntityAttribute(getNombreEnfantCharge());
        }
    }

    /**
     * Valorise le nombre d'enfants à charge (tel que lu dans le fichier).
     * 
     * @param nombreEnfantCharge
     *            nombre d'enfants à charge à valoriser
     */
    public void setNombreEnfantChargeAsText(String nombreEnfantCharge) {
        this.nombreEnfantChargeAsText = nombreEnfantCharge;
        setNombreEnfantCharge(new ConvertisseurNumeriqueInteger().convertToDatabaseColumn(nombreEnfantCharge));
    }

    @Override
    public IndividuId getId() {
        return new IndividuId(ligneEnCoursImportBatch, idIndividu);
    }

}
