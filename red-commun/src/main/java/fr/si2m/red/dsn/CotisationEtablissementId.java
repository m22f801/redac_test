package fr.si2m.red.dsn;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * Modèle des cotisationEtablissement
 * 
 * @author poidij
 *
 */
@Data
@EqualsAndHashCode(callSuper = false, of = { "ligneEnCoursImportBatch", "idCotisationEtablissement" })
@ToString(callSuper = false, of = { "ligneEnCoursImportBatch", "idCotisationEtablissement" })
@NoArgsConstructor
@AllArgsConstructor
public class CotisationEtablissementId implements Serializable {
    /**
     * UID de version.
     */
    private static final long serialVersionUID = 822362569368101808L;

    /**
     * Indicateur de ligne temporaire en cours d'import via Batch.
     * 
     * @param ligneEnCoursImportBatch
     *            le nom du SI d'où provient l'information
     * @return le nom du SI d'où provient l'information
     */
    private boolean ligneEnCoursImportBatch;

    /**
     * Identifiant de la cotisationEtablissement
     *
     * @param idCotisationEtablissement
     *            L'identifiant de la cotisationEtablissement
     * @return L'identifiant de la cotisationEtablissement
     */
    private String idCotisationEtablissement;

}
