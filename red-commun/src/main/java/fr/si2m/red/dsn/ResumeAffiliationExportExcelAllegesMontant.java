package fr.si2m.red.dsn;

import lombok.Data;

/**
 * Résumé d'une affiliation (avec informations consolidées).
 * 
 * @author nortaina
 *
 */
@Data
public class ResumeAffiliationExportExcelAllegesMontant {

    /**
     * S21.G00.30.001
     *
     * @param identifiantRepertoire
     *            S21.G00.30.001
     * @return S21.G00.30.001
     */
    private String identifiantRepertoire;

    /**
     * S21.G00.30.020
     *
     * @param numeroTechniqueTemporaire
     *            S21.G00.30.020
     * @return S21.G00.30.020
     */
    private String numeroTechniqueTemporaire;

    /**
     * S21.G00.30.019
     *
     * @param matricule
     *            S21.G00.30.019
     * @return S21.G00.30.019
     */
    private String matricule;

    /**
     * S21.G00.30.005
     *
     * @param sexe
     *            S21.G00.30.005
     * @return S21.G00.30.005
     */
    private String sexe;

    /**
     * S21.G00.30.002
     *
     * @param nomFamille
     *            S21.G00.30.002
     * @return S21.G00.30.002
     */
    private String nomFamille;

    /**
     * S21.G00.30.003
     *
     * @param nomUsage
     *            S21.G00.30.003
     * @return S21.G00.30.003
     */
    private String nomUsage;

    /**
     * S21.G00.30.004
     *
     * @param prenom
     *            S21.G00.30.004
     * @return S21.G00.30.004
     */
    private String prenom;

    /**
     * S21.G00.30.006
     *
     * @param dateNaissance
     *            S21.G00.30.006
     * @return S21.G00.30.006
     */
    private Integer dateNaissance;

    /**
     * S21.G00.40.001
     *
     * @param dateDebutContrat
     *            S21.G00.40.001
     * @return S21.G00.40.001
     */
    private Integer dateDebutContrat;

    /**
     * S21.G00.40.010
     *
     * @param dateFinPrevisionnelle
     *            S21.G00.40.010
     * @return S21.G00.40.010
     */
    private Integer dateFinPrevisionnelle;

    /**
     * S21.G00.70.005
     *
     * @param codePopulation
     *            S21.G00.70.005
     * @return S21.G00.70.005
     */
    private String codePopulation;

    /**
     * Libellé Population SI source
     *
     * 
     * @param libPopulation
     *            Libelle population pour l'affiliation
     * @return Libelle population pour l'affiliation
     */
    private String libPopulation;

    /**
     * Le montant de cotisation dans categorie quittencement individu.
     *
     * @param montantTranche1
     *            le montant sur la tranche 1
     * @return le montant sur la tranche 1
     */
    private Double montantCot;

    /**
     * Flag indicant une valeur null ( aucune ligne dans CATEGORIE_QUITTANCEMENT_INDIVIDU pour l'indiv ) , nécessaire car sommeMontantsCotisation de type double
     * a la valeur 0 par défaut.
     */
    private boolean montantCotisationExistant;

}
