package fr.si2m.red.dsn;

import java.math.BigInteger;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.apache.commons.lang3.StringUtils;

import fr.si2m.red.EntiteImportableBatch;
import fr.si2m.red.core.repository.jpa.conversion.ConvertisseurDatesDDMMYYYY;
import fr.si2m.red.core.repository.jpa.conversion.ConvertisseurDonneeSalaire;
import fr.si2m.red.core.repository.jpa.conversion.ConvertisseurNumeriqueDecimal;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * Modèle des rémunérations.
 * 
 * @author poidij
 *
 */
@Entity
@Table(name = "REMUNERATION")
@IdClass(RemunerationId.class)
@Data
@EqualsAndHashCode(callSuper = false, of = { "ligneEnCoursImportBatch", "idRemuneration" })
@ToString(callSuper = false, of = { "ligneEnCoursImportBatch", "idRemuneration" })
public class Remuneration extends EntiteImportableBatch {
    /**
     * UID de version.
     */
    private static final long serialVersionUID = 822362569368101808L;

    /**
     * Indicateur de ligne temporaire en cours d'import via Batch.
     * 
     * @param ligneEnCoursImportBatch
     *            le nom du SI d'où provient l'information
     * @return le nom du SI d'où provient l'information
     */
    @Id
    @Column(name = "TMP_BATCH")
    private boolean ligneEnCoursImportBatch;

    /**
     * Identifiant de la rémunération.
     *
     * @param idRemuneration
     *            L'identifiant de la rémunération
     * @return L'identifiant de la rémunération
     */
    @Id
    @Column(name = "ID_REMUNERATION")
    private String idRemuneration;

    /**
     * Identifiant du versement de l'individu.
     *
     * @param idVersementIndividu
     *            L'identifiant du versement de l'individu
     * @return L'identifiant du versement de l'individu
     */
    @Column(name = "ID_VERS_INDIV")
    private String idVersementIndividu;

    /**
     * La date de début de période de paie.
     *
     * @param dateDebutPeriodePaie
     *            date de début de période de paie
     * @return date de début de période de paie
     */
    @Column(name = "DATE_DEB_PERIODE_PAIE")
    private Integer dateDebutPeriodePaie;

    /**
     * La date de fin de période de paie.
     *
     * @param dateFinPeriodePaie
     *            date de fin de période de paie
     * @return date de fin de période de paie
     */
    @Column(name = "DATE_FIN_PERIODE_PAIE")
    private Integer dateFinPeriodePaie;

    /**
     * Le numéro du contrat de travail.
     *
     * @param numeroContratTravail
     *            numéro du contrat de travail
     * @return numéro du contrat de travail
     */
    @Column(name = "NUMERO_CONTRAT")
    private String numeroContratTravail;

    /**
     * Le type de contrat de travail.
     *
     * @param typeContratTravail
     *            type de contrat de travail
     * @return type de contrat de travail
     */
    @Column(name = "TYPE_CONTRAT")
    private String typeContratTravail;

    /**
     * Le montant de la paie.
     *
     * @param montantPaie
     *            montant de la paie
     * @return montant de la paie
     */
    @Column(name = "MONTANT")
    @Convert(converter = ConvertisseurDonneeSalaire.class)
    private Double montantPaie;

    /**
     * Montant de paie (tel que lu dans le fichier).
     *
     */
    @Transient
    private String montantPaieAsText;

    /**
     * Date de début de période de paie (telle que lue dans le fichier).
     *
     */
    @Transient
    private String dateDebutPeriodePaieAsText;

    /**
     * Date de fin de période de paie (telle que lue dans le fichier).
     * 
     */
    @Transient
    private String dateFinPeriodePaieAsText;

    /**
     * Identifiant de la DSN dans la brique
     */
    @Column(name = "ID_DSN_BRIQUE")
    private BigInteger idDsnBrique;

    /**
     * Clé primaire de la table dans la brique
     */
    @Column(name = "PK_ENTITEE_BRIQUE")
    private BigInteger cleEntiteeBrique;

    /**
     * Renvoie le montant de paie (tel que lu dans le fichier).
     * 
     * @return le montant de paie
     */
    public String getMontantPaieAsText() {
        String value = this.montantPaieAsText;
        if (StringUtils.isBlank(value) && getMontantPaie() != null) {
            value = new ConvertisseurNumeriqueDecimal().convertToEntityAttribute(getMontantPaie());
        }
        return value;
    }

    /**
     * Valorise le montant de paie (tel que lu dans le fichier).
     * 
     * @param montantPaie
     *            le montant de paie à valoriser
     */
    public void setMontantPaieAsText(String montantPaie) {
        this.montantPaieAsText = montantPaie;
        setMontantPaie(new ConvertisseurNumeriqueDecimal().convertToDatabaseColumn(montantPaie));
    }

    /**
     * Date de début de période de paie (telle que lue dans le fichier).
     * 
     * @return la date de début de période de paie
     */
    public String getDateDebutPeriodePaieAsText() {
        if (dateDebutPeriodePaieAsText != null) {
            return dateDebutPeriodePaieAsText;
        }
        return new ConvertisseurDatesDDMMYYYY().convertToEntityAttribute(getDateDebutPeriodePaie());
    }

    /**
     * Valorise la date de début de période de paie (telle que lue dans le fichier).
     * 
     * @param dateDebutPeriodePaie
     *            la date de début de période de paie à valoriser
     */
    public void setDateDebutPeriodePaieAsText(String dateDebutPeriodePaie) {
        this.dateDebutPeriodePaieAsText = dateDebutPeriodePaie;
        setDateDebutPeriodePaie(new ConvertisseurDatesDDMMYYYY().convertToDatabaseColumn(dateDebutPeriodePaie));
    }

    /**
     * Date de fin de période de paie (telle que lue dans le fichier).
     * 
     * @return la date de fin de période de paie
     */
    public String getDateFinPeriodePaieAsText() {
        if (dateFinPeriodePaieAsText != null) {
            return dateFinPeriodePaieAsText;
        }
        return new ConvertisseurDatesDDMMYYYY().convertToEntityAttribute(getDateFinPeriodePaie());
    }

    /**
     * Valorise la fin du fichier (telle que lue dans le fichier).
     * 
     * @param dateFinPeriodePaie
     *            la date de fin de période de paie à valoriser
     */
    public void setDateFinPeriodePaieAsText(String dateFinPeriodePaie) {
        this.dateFinPeriodePaieAsText = dateFinPeriodePaie;
        setDateFinPeriodePaie(new ConvertisseurDatesDDMMYYYY().convertToDatabaseColumn(dateFinPeriodePaie));
    }

    @Override
    public RemunerationId getId() {
        return new RemunerationId(ligneEnCoursImportBatch, idRemuneration);
    }

}
