package fr.si2m.red.dsn;

import fr.si2m.red.core.repository.EntiteImportableRepository;

/**
 * Référentiel des versements.
 * 
 * @author poidij
 *
 */
public interface VersementRepository extends EntiteImportableRepository<Versement> {

    /**
     * Indique l'existence d'un Versement en base.
     * 
     * @param idVersement
     *            l'identifiant du versement
     * @return true s'il existe au moins un versement ayant cet identifiant, sinon false
     */
    boolean existeUnVersement(String idVersement);
}
