/**
 * 
 */
package fr.si2m.red;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.MappedSuperclass;
import javax.persistence.Transient;

import lombok.Getter;
import lombok.Setter;

/**
 * Définition d'une entité importable via un batch REDAC.
 * 
 * @author nortaina
 *
 */
@MappedSuperclass
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public abstract class EntiteImportableBatch extends Entite {
    /**
     * UID de version.
     */
    private static final long serialVersionUID = -9182314041675053347L;

    /**
     * Le nom du fichier utilisé pour l'import.
     * 
     * @param nomFichierImport
     *            le nom du fichier utilisé pour l'import
     * @return le nom du fichier utilisé pour l'import
     */
    @Transient
    @Getter
    @Setter
    private String nomFichierImport;

    /**
     * Le numéro de la ligne importée pour cette entité.
     * 
     * @param ligneImport
     *            le numéro de la ligne importée pour cette entité
     * @return le numéro de la ligne importée pour cette entité
     */
    @Transient
    @Getter
    @Setter
    private int ligneImport;

    /**
     * Si la ligne importé est syntaxiquement valide ou si elle n'a pu être lue.
     * 
     * @param ligneSyntaxiquementValide
     *            true si la ligne importé est syntaxiquement valide ou false si elle n'a pu être lue
     * @return true si la ligne importé est syntaxiquement valide ou false si elle n'a pu être lue
     */
    @Transient
    @Getter
    @Setter
    private boolean ligneSyntaxiquementValide = true;

    /**
     * La liste des anomalies sur la ligne importé si elle comporte des erreurs.
     * 
     * @param erreursImport
     *            la liste des anomalies sur la ligne importé si elle comporte des erreurs
     * @return la liste des anomalies sur la ligne importé si elle comporte des erreurs
     */
    @Transient
    @Getter
    @Setter
    private List<ErreurLigneFichierImport> erreursImport = new ArrayList<ErreurLigneFichierImport>();

    /**
     * La liste des anomalies non bloquantes sur la ligne importé si elle comporte des erreurs non bloquantes.
     * 
     * @param avertissementsImport
     *            la liste des anomalies non bloquantes sur la ligne importé si elle comporte des erreurs non bloquantes
     * @return la liste des anomalies sur la ligne importé si elle comporte des erreurs non bloquantes
     */
    @Transient
    @Getter
    @Setter
    private List<ErreurLigneFichierImport> avertissementsImport = new ArrayList<ErreurLigneFichierImport>();

    /**
     * Le marquage d'une donnée temporaire.
     * 
     * @return true si l'entité est une ligne temporaire en cours d'import, false sinon
     */
    public abstract boolean isLigneEnCoursImportBatch();

    /**
     * Le marquage d'une donnée temporaire.
     * 
     * @param ligneEnCoursImportBatch
     *            true si l'entité est une ligne temporaire en cours d'import, false sinon
     */
    public abstract void setLigneEnCoursImportBatch(boolean ligneEnCoursImportBatch);

    /**
     * Enregistre une anomalie sur un import de cette entité via un batch.
     * 
     * @param messageErreur
     *            la description de l'anomalie
     */
    public void enregistreErreurSurImport(String messageErreur) {
        ErreurLigneFichierImport erreur = new ErreurLigneFichierImport();
        erreur.setNomFichier(nomFichierImport);
        erreur.setNumeroLigne(ligneImport);
        erreur.setMessageErreur(messageErreur);
        this.erreursImport.add(erreur);
    }

    /**
     * Enregistre une anomalie non bloquante sur un import de cette entité via un batch.
     * 
     * @param messageAvertissement
     *            la description de l'anomalie
     */
    public void enregistreAvertissementSurImport(String messageAvertissement) {
        ErreurLigneFichierImport erreur = new ErreurLigneFichierImport();
        erreur.setNomFichier(nomFichierImport);
        erreur.setNumeroLigne(ligneImport);
        erreur.setMessageErreur(messageAvertissement);
        this.avertissementsImport.add(erreur);
    }

    /**
     * Vérifie que l'entité présente est valide pour import.<br/>
     * Même si elle comporte des anomalies non bloquantes, une entité est toujours considéré comme valide en import.
     * 
     * @return true si l'import de la ligne a été validée, false sinon
     */
    public boolean isImportValide() {
        return ligneSyntaxiquementValide && (erreursImport == null || erreursImport.isEmpty());
    }
}
