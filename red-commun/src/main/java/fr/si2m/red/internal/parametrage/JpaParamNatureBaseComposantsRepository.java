package fr.si2m.red.internal.parametrage;

import java.util.List;

import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import fr.si2m.red.core.repository.jpa.JpaEntiteImportableRepository;
import fr.si2m.red.parametrage.ParamNatureBaseComposants;
import fr.si2m.red.parametrage.ParamNatureBaseComposantsRepository;

/**
 * Base de données des paramètres de nature de base de composants.
 * 
 * @author nortaina
 *
 */
@Repository
public class JpaParamNatureBaseComposantsRepository extends JpaEntiteImportableRepository<ParamNatureBaseComposants>
        implements ParamNatureBaseComposantsRepository {

    /**
     * Le nom de paramètre utilisé pour la nature de la base des cotisations.
     */
    private static final String PARAM_NATURE_BASE_COTISATION = "natureBaseCotisations";

    /**
     * Le nom de paramètre utilisé pour le taux de calcul rempli.
     */
    private static final String PARAM_TAUX_CALCUL_REMPLI = "tauxCalculRempli";

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public List<ParamNatureBaseComposants> getParamNatureBaseComposants(Integer natureBaseCotisations, String tauxCalculRempli) {
        TypedQuery<ParamNatureBaseComposants> query = getEntityManager().createQuery(
                "SELECT p FROM ParamNatureBaseComposants p WHERE CONBCOT = :natureBaseCotisations AND TXCALCU_REMPLI = :tauxCalculRempli",
                ParamNatureBaseComposants.class);
        query.setParameter(PARAM_NATURE_BASE_COTISATION, natureBaseCotisations);
        query.setParameter(PARAM_TAUX_CALCUL_REMPLI, tauxCalculRempli);
        List<ParamNatureBaseComposants> composants = query.getResultList();
        for (ParamNatureBaseComposants composant : composants) {
            getEntityManager().detach(composant);
        }
        return composants;
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public List<ParamNatureBaseComposants> getParamNatureBaseComposants(Integer natureBaseCotisations, Integer numTranche, String tauxCalculRempli) {
        TypedQuery<ParamNatureBaseComposants> query = getEntityManager()
                .createQuery(
                        "SELECT p FROM ParamNatureBaseComposants p WHERE p.codeNatureBaseCotisations = :natureBaseCotisations AND p.numTranche = :numTranche "
                                + "AND p.tauxCalculRempliAsText = :tauxCalculRempli AND p.ligneEnCoursImportBatch IS FALSE",
                        ParamNatureBaseComposants.class);
        query.setParameter(PARAM_NATURE_BASE_COTISATION, natureBaseCotisations);
        query.setParameter("numTranche", numTranche);
        query.setParameter(PARAM_TAUX_CALCUL_REMPLI, tauxCalculRempli);
        List<ParamNatureBaseComposants> composants = query.getResultList();
        for (ParamNatureBaseComposants composant : composants) {
            getEntityManager().detach(composant);
        }
        return composants;
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public List<Integer> getTypesComposantsBasesPourModeConsolidation(Integer natureBaseCotisations, Integer numTranche, String tauxCalculRempli,
            String modeConsolidation) {
        TypedQuery<Integer> query = getEntityManager().createQuery(
                "SELECT p.numTypeComposantBase FROM ParamNatureBaseComposants p WHERE p.codeNatureBaseCotisations = :natureBaseCotisations AND p.numTranche = :numTranche "
                        + "AND p.tauxCalculRempliAsText = :tauxCalculRempli AND p.modeConsolidation = :modeConsolidation AND p.ligneEnCoursImportBatch IS FALSE",
                Integer.class);
        query.setParameter(PARAM_NATURE_BASE_COTISATION, natureBaseCotisations);
        query.setParameter("numTranche", numTranche);
        query.setParameter(PARAM_TAUX_CALCUL_REMPLI, tauxCalculRempli);
        query.setParameter("modeConsolidation", modeConsolidation);
        return query.getResultList();
    }

}
