package fr.si2m.red.internal.dsn;

import java.util.List;

import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import fr.si2m.red.core.repository.jpa.JpaEntiteImportableRepository;
import fr.si2m.red.dsn.ContratTravail;
import fr.si2m.red.dsn.ContratTravailRepository;

/**
 * Base de données des entités {@link ContratTravail}, connectée via JPA.
 * 
 * @author poidij
 *
 */
@Repository
public class JpaContratTravailRepository extends JpaEntiteImportableRepository<ContratTravail> implements ContratTravailRepository {

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public boolean existeUnContratTravail(String idContratTravail) {
        TypedQuery<Long> query = getEntityManager().createQuery("SELECT count(*) FROM ContratTravail c WHERE c.idContratTravail = :idContratTravail",
                Long.class);
        query.setParameter("idContratTravail", idContratTravail);
        List<Long> result = query.getResultList();
        if (result.isEmpty() || result.get(0) == null) {
            return false;
        }
        return result.get(0) > 0;
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public ContratTravail getDernierContratTravailPourIndividu(String idIndividu) {
        String jpql = "SELECT ct FROM ContratTravail ct WHERE ct.idIndividu = :idIndividu" + " ORDER BY ct.dateDebutContrat DESC";
        TypedQuery<ContratTravail> query = getEntityManager().createQuery(jpql, ContratTravail.class).setMaxResults(1);
        query.setParameter("idIndividu", idIndividu);
        List<ContratTravail> resultats = query.getResultList();
        return getPremierResultatSiExiste(resultats);
    }

}
