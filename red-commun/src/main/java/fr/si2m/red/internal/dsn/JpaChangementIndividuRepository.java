package fr.si2m.red.internal.dsn;

import java.util.List;

import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import fr.si2m.red.core.repository.jpa.JpaEntiteImportableRepository;
import fr.si2m.red.dsn.ChangementIndividu;
import fr.si2m.red.dsn.ChangementIndividuRepository;

/**
 * Base de données des entités {@link ChangementIndividu}, connectée via JPA.
 * 
 * @author poidij
 *
 */
@Repository
public class JpaChangementIndividuRepository extends JpaEntiteImportableRepository<ChangementIndividu> implements ChangementIndividuRepository {

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public List<ChangementIndividu> getPourIndividu(String idIndividu) {
        String jpql = "SELECT c FROM ChangementIndividu c WHERE c.idIndividu = :idIndividu AND c.ligneEnCoursImportBatch IS FALSE ORDER BY c.dateModification DESC";
        TypedQuery<ChangementIndividu> query = getEntityManager().createQuery(jpql, ChangementIndividu.class);
        query.setParameter("idIndividu", idIndividu);
        return query.getResultList();
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public boolean existeUnChangementIndividu(String idChangementIndividu) {
        TypedQuery<Long> query = getEntityManager().createQuery("SELECT count(*) FROM ChangementIndividu c WHERE c.idChangementIndividu = :idChangementIndividu", Long.class);
        query.setParameter("idChangementIndividu", idChangementIndividu);
        List<Long> result = query.getResultList();
        if (result.isEmpty() || result.get(0) == null) {
            return false;
        }
        return result.get(0) > 0;
    }

}
