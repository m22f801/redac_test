package fr.si2m.red.internal.dsn;

import java.util.List;

import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import fr.si2m.red.core.repository.jpa.JpaEntiteImportableRepository;
import fr.si2m.red.dsn.AdhesionEtablissementMois;
import fr.si2m.red.dsn.AdhesionEtablissementMoisRepository;

/**
 * Base de données des entités {@link AdhesionEtablissementMois}, connectée via JPA.
 * 
 * @author poidij
 *
 */
@Repository
public class JpaAdhesionEtablissementMoisRepository extends JpaEntiteImportableRepository<AdhesionEtablissementMois>
        implements AdhesionEtablissementMoisRepository {

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public boolean existeUneAdhesion(String idAdhEtabMois) {
        TypedQuery<Long> query = getEntityManager()
                .createQuery("SELECT count(*) FROM AdhesionEtablissementMois a WHERE a.idAdhEtabMois = :idAdhEtabMois", Long.class);
        query.setParameter("idAdhEtabMois", idAdhEtabMois);
        List<Long> result = query.getResultList();
        if (result.isEmpty() || result.get(0) == null) {
            return false;
        }
        return result.get(0) > 0;
    }

    @Override
    @Transactional(propagation = Propagation.MANDATORY)
    public int supprimeAdhesionsDeTestOuDeContratsDelegues() {
        Query query = getEntityManager().createQuery(
                "DELETE FROM AdhesionEtablissementMois a WHERE a.ligneEnCoursImportBatch is true AND (a.codeEssaiReel = '01' OR trim(a.codeDelegataireGestion) != '')");
        return query.executeUpdate();
    }

}
