package fr.si2m.red.internal.parametrage;

import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import fr.si2m.red.core.repository.jpa.JpaEntiteImportableRepository;
import fr.si2m.red.parametrage.ParamEtatContrat;
import fr.si2m.red.parametrage.ParamEtatContratId;
import fr.si2m.red.parametrage.ParamEtatContratRepository;

/**
 * Base de données des entités {@link ParamEtatContrat}, connectée via JPA.
 * 
 * @author nortaina
 *
 */
@Repository
public class JpaParamEtatContratRepository extends JpaEntiteImportableRepository<ParamEtatContrat> implements ParamEtatContratRepository {

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public boolean existeParamEtatContrat(Integer etatContrat) {
        String jpql = "SELECT COUNT(p) FROM ParamEtatContrat p WHERE p.etatContrat = :etatContrat AND p.ligneEnCoursImportBatch IS FALSE";
        TypedQuery<Long> query = getEntityManager().createQuery(jpql, Long.class);
        query.setParameter("etatContrat", etatContrat);
        return query.getSingleResult() > 0;
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public boolean verifieEtatContratActif(Integer etatContrat) {
        ParamEtatContratId id = new ParamEtatContratId(false, etatContrat);
        ParamEtatContrat paramEtatContrat = getEntityManager().find(ParamEtatContrat.class, id);
        boolean actif = false;
        if (paramEtatContrat != null) {
            actif = paramEtatContrat.isActif();
            getEntityManager().detach(paramEtatContrat);
        }
        return actif;
    }

}
