package fr.si2m.red.internal.parametrage;

import java.util.List;

import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import fr.si2m.red.core.repository.jpa.JpaEntiteImportableRepository;
import fr.si2m.red.parametrage.ParamPlafondTaux;
import fr.si2m.red.parametrage.ParamPlafondTauxRepository;

/**
 * Référentiel des plafonds des taux.
 * 
 * @author nortaina
 *
 */
@Repository
public class JpaParamPlafondTauxRepository extends JpaEntiteImportableRepository<ParamPlafondTaux> implements ParamPlafondTauxRepository {

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public Double getPlafondMensuelSecuriteSociale(Integer dateEffet) {
        String jpql = "SELECT p.plafondMensuelSecuriteSociale FROM ParamPlafondTaux p "
                + "WHERE p.debut <= :dateEffet AND (coalesce(p.fin, 99999999) >= :dateEffet OR (CASE WHEN (p.fin = 0) THEN 99999999 ELSE p.fin END) >= :dateEffet)";
        TypedQuery<Double> query = getEntityManager().createQuery(jpql, Double.class).setMaxResults(1);
        query.setParameter("dateEffet", dateEffet);
        List<Double> resultats = query.getResultList();
        if (resultats.isEmpty()) {
            return null;
        }
        return resultats.get(0);
    }

}
