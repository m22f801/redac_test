package fr.si2m.red.internal.reconciliation;

import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import fr.si2m.red.DateRedac;
import fr.si2m.red.core.repository.jpa.JpaRepository;
import fr.si2m.red.reconciliation.HistoriqueCommentairePeriode;
import fr.si2m.red.reconciliation.HistoriqueCommentairePeriodeRepository;

/**
 * Base de données des entités {@link HistoriqueCommentairePeriode}, connectée via JPA.
 * 
 * @author gahagnont
 *
 */
@Repository
public class JpaHistoriqueCommentairePeriodeRepository extends JpaRepository<HistoriqueCommentairePeriode>
        implements HistoriqueCommentairePeriodeRepository {

    private static final String PARAM_ID_PERIODE = "idPeriode";
    private static final String PARAM_LISTE_ID_PERIODES = "idsPeriodes";

    @Override
    public Class<HistoriqueCommentairePeriode> getClassePrototypeEntite() {
        return HistoriqueCommentairePeriode.class;
    }

    @Override
    @Transactional(propagation = Propagation.MANDATORY, rollbackFor = Throwable.class)
    public List<HistoriqueCommentairePeriode> getHistoriqueCommentairesPeriode(Long idPeriode) {
        String jpql = "FROM HistoriqueCommentairePeriode h WHERE h.idPeriode=:idPeriode ORDER BY h.dateHeureSaisie DESC";
        TypedQuery<HistoriqueCommentairePeriode> query = getEntityManager().createQuery(jpql, HistoriqueCommentairePeriode.class);
        query.setParameter(PARAM_ID_PERIODE, idPeriode);
        return query.getResultList();
    }

    @Override
    @Transactional(propagation = Propagation.MANDATORY, rollbackFor = Throwable.class)
    public HistoriqueCommentairePeriode getHistoriqueCommentairePeriodePourMessage(Long idPeriode, Long dateHeureSaisie) {
        String jpql = "FROM HistoriqueCommentairePeriode h WHERE h.idPeriode=:idPeriode AND h.dateHeureSaisie = :dateHeureSaisie";
        TypedQuery<HistoriqueCommentairePeriode> query = getEntityManager().createQuery(jpql, HistoriqueCommentairePeriode.class).setMaxResults(1);
        query.setParameter(PARAM_ID_PERIODE, idPeriode);
        query.setParameter("dateHeureSaisie", dateHeureSaisie);
        return getPremierResultatSiExiste(query.getResultList());
    }

    @Override
    @Transactional(propagation = Propagation.MANDATORY, rollbackFor = Throwable.class)
    public HistoriqueCommentairePeriode getDernierCommentairePourPeriode(Long idPeriode) {
        String jpql = "FROM HistoriqueCommentairePeriode h WHERE h.idPeriode=:idPeriode ORDER BY h.dateHeureSaisie DESC";
        TypedQuery<HistoriqueCommentairePeriode> query = getEntityManager().createQuery(jpql, HistoriqueCommentairePeriode.class).setMaxResults(1);
        query.setParameter(PARAM_ID_PERIODE, idPeriode);
        return getPremierResultatSiExiste(query.getResultList());
    }

    @Override
    public int supprimeHistoriqueCommentairePourPeriode(Long idPeriode) {
        String jpql = "DELETE FROM HistoriqueCommentairePeriode h WHERE h.idPeriode = :idPeriode";
        Query query = getEntityManager().createQuery(jpql);
        query.setParameter(PARAM_ID_PERIODE, idPeriode);
        return query.executeUpdate();

    }

    @Override
    @Transactional(propagation = Propagation.MANDATORY)
    public void callMysqlProcedure(String sql, Map<String, Object> params) {
        getJdbcTemplate().update(sql, params);
    }

    @Override
    public void creationEnMasseOrigineIHM(List<Long> idsPeriodes, String message, String utilisateur) {
        String sql = "call Creation_HistoriqueCommentairePeriode_en_masse( :" + PARAM_LISTE_ID_PERIODES + ","
                + ":datehms , :utilisateur , :commentaire , :userCreation )";
        Map<String, Object> params = new HashMap<>();
        params.put(PARAM_LISTE_ID_PERIODES, StringUtils.join(idsPeriodes, ","));
        params.put("datehms", DateRedac.convertitEnDateHeureRedac(Calendar.getInstance().getTime()));
        params.put("utilisateur", utilisateur);
        params.put("commentaire", message);
        params.put("userCreation", utilisateur);

        callMysqlProcedure(sql, params);

    }

    @Override
    public List<HistoriqueCommentairePeriode> getHistoriqueCommentairesPeriodes(List<Long> idsPeriodes) {
        String jpql = "FROM HistoriqueCommentairePeriode h where h.idPeriode in ( " + StringUtils.join(idsPeriodes, ",")
                + " ) ORDER BY h.dateHeureSaisie DESC";
        TypedQuery<HistoriqueCommentairePeriode> query = getEntityManager().createQuery(jpql, HistoriqueCommentairePeriode.class);
        return query.getResultList();
    }
}
