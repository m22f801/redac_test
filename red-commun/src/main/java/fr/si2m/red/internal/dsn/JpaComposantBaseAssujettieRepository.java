package fr.si2m.red.internal.dsn;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.TypedQuery;

import org.apache.commons.collections.keyvalue.DefaultMapEntry;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import fr.si2m.red.core.repository.jpa.JpaEntiteImportableRepository;
import fr.si2m.red.dsn.ComposantBaseAssujettie;
import fr.si2m.red.dsn.ComposantBaseAssujettieRepository;

/**
 * Base de données des entités {@link ComposantBaseAssujettie}, connectée via JPA.
 * 
 * @author poidij
 *
 */
@Repository
public class JpaComposantBaseAssujettieRepository extends JpaEntiteImportableRepository<ComposantBaseAssujettie>
        implements ComposantBaseAssujettieRepository {

    /**
     * Le nom de paramètre utilisé pour l'identifiant de base assujettie.
     */
    private static final String PARAM_ID_BASE_ASSUJ = "idBaseAssujettie";

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public ComposantBaseAssujettie get(String idBaseAssujettie, Integer typeComposantBase) {
        String jpql = "SELECT c FROM ComposantBaseAssujettie c WHERE c.idBaseAssujettie = :idBaseAssujettie AND c.typeComposantBaseAssujettie = :typeComposantBase "
                + "AND c.ligneEnCoursImportBatch IS FALSE";
        TypedQuery<ComposantBaseAssujettie> query = getEntityManager().createQuery(jpql, ComposantBaseAssujettie.class).setMaxResults(1);
        query.setParameter(PARAM_ID_BASE_ASSUJ, idBaseAssujettie);
        query.setParameter("typeComposantBase", String.valueOf(typeComposantBase));
        List<ComposantBaseAssujettie> resultats = query.getResultList();
        return getPremierResultatSiExiste(resultats);
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public Double getSommeMontantsComposantsBaseAssujettie(String idBaseAssujettie, List<String> typesComposantsBases) {
        String jpql = "SELECT SUM(c.montantComposantBaseAssujettie) FROM ComposantBaseAssujettie c WHERE c.idBaseAssujettie = :idBaseAssujettie "
                + "AND c.typeComposantBaseAssujettie IN (:typesComposantsBases) AND c.ligneEnCoursImportBatch IS FALSE";
        TypedQuery<Double> query = getEntityManager().createQuery(jpql, Double.class).setMaxResults(1);
        query.setParameter(PARAM_ID_BASE_ASSUJ, idBaseAssujettie);
        query.setParameter("typesComposantsBases", typesComposantsBases);
        return getPremierResultatSiExiste(query.getResultList());
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public Double getSommeMontantsComposantsBaseAssujettiePourAffiliationEtTCBA(String idAffiliation, String tcba) {
        String jpql = "SELECT SUM(c.montantComposantBaseAssujettie) FROM ComposantBaseAssujettie c " + "LEFT JOIN c.baseAssujettie ba "
                + "LEFT JOIN  ba.affiliation a " + "WHERE a.idAffiliation = :idAffiliation "
                + "AND c.typeComposantBaseAssujettie = :tcba AND c.ligneEnCoursImportBatch IS FALSE";
        TypedQuery<Double> query = getEntityManager().createQuery(jpql, Double.class).setMaxResults(1);
        query.setParameter("idAffiliation", idAffiliation);
        query.setParameter("tcba", tcba);
        return getPremierResultatSiExiste(query.getResultList());
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public Map<String, Double> getMontantsPourBase(String idBaseAssujettie) {
        String jpql = "SELECT new org.apache.commons.collections.keyvalue.DefaultMapEntry(c.typeComposantBaseAssujettie, c.montantComposantBaseAssujettie) FROM ComposantBaseAssujettie c WHERE c.idBaseAssujettie = :idBaseAssujettie "
                + "AND c.ligneEnCoursImportBatch IS FALSE GROUP BY c.typeComposantBaseAssujettie";
        TypedQuery<DefaultMapEntry> query = getEntityManager().createQuery(jpql, DefaultMapEntry.class);
        query.setParameter(PARAM_ID_BASE_ASSUJ, idBaseAssujettie);
        List<DefaultMapEntry> couples = query.getResultList();
        Map<String, Double> resultats = new HashMap<>();
        for (DefaultMapEntry couple : couples) {
            resultats.put((String) couple.getKey(), (Double) couple.getValue());
        }
        return resultats;
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public boolean existeUnComposantBaseAssujettie(String idComposantBaseAssujettie) {
        TypedQuery<Long> query = getEntityManager().createQuery(
                "SELECT count(*) FROM ComposantBaseAssujettie c WHERE c.idComposantBaseAssujettie = :idComposantBaseAssujettie", Long.class);
        query.setParameter("idComposantBaseAssujettie", idComposantBaseAssujettie);
        List<Long> result = query.getResultList();
        if (result.isEmpty() || result.get(0) == null) {
            return false;
        }
        return result.get(0) > 0;
    }

}
