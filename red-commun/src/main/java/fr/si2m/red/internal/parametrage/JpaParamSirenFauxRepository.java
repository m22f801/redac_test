package fr.si2m.red.internal.parametrage;

import java.util.List;

import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;

import fr.si2m.red.core.repository.jpa.JpaEntiteImportableRepository;
import fr.si2m.red.parametrage.ParamSirenFaux;
import fr.si2m.red.parametrage.ParamSirenFauxRepository;

/**
 * Base de données des paramètres de siren faux
 * 
 * @author eudesr
 *
 */
@Repository
public class JpaParamSirenFauxRepository extends JpaEntiteImportableRepository<ParamSirenFaux> implements ParamSirenFauxRepository {

    @Override
    public boolean existeSiren(String siren) {
        TypedQuery<Long> query = getEntityManager().createQuery("SELECT count(*) FROM ParamSirenFaux WHERE SIREN = :siren", Long.class);
        query.setParameter("siren", siren);
        List<Long> result = query.getResultList();
        if (result.get(0) == null) {
            return false;
        }
        return result.get(0) > 0;
    }

}
