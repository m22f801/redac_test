package fr.si2m.red.internal.parametrage;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import fr.si2m.red.core.repository.jpa.JpaEntiteImportableRepository;
import fr.si2m.red.parametrage.ParamUtilisateurGestionnaire;
import fr.si2m.red.parametrage.ParamUtilisateurGestionnaireId;
import fr.si2m.red.parametrage.ParamUtilisateurGestionnaireRepository;
import fr.si2m.red.parametrage.ResumeParamUtilisateurGestionnaire;
import fr.si2m.red.reconciliation.PeriodesRecuesCriteresRecherche;

/**
 * Référentiel des paramètres des utilisateurs gestionnaires.
 * 
 * @author poidij,eudesr
 *
 */
@Repository
public class JpaParamUtilisateurGestionnaireRepository extends JpaEntiteImportableRepository<ParamUtilisateurGestionnaire>
        implements ParamUtilisateurGestionnaireRepository {

    private static final String PARAM_CODE_USER = "codeUser";

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public ParamUtilisateurGestionnaire getGestionnaire(String identifiantActiveDirectory) {
        return get(new ParamUtilisateurGestionnaireId(false, identifiantActiveDirectory));
    }

    @Override
    public String getNomEtPrenomAvecCodeUser(String codeUser) {

        String sql = "SELECT NOM, PRENOM FROM PARAM_UTILISATEUR_GESTIONNAIRE WHERE CODE_USER = :" + PARAM_CODE_USER;

        Map<String, Object> params = new HashMap<>();
        params.put(PARAM_CODE_USER, codeUser);
        return getPremierResultatSiExiste(getJdbcTemplate().query(sql, params, new RowMapper<String>() {

            @Override
            public String mapRow(ResultSet rs, int rowNum) throws SQLException {
                return rs.getString(1) + " " + rs.getString(2);
            }
        }));
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public List<ResumeParamUtilisateurGestionnaire> getListeNomPrenomIdActiveDirectory() {
        // ajout de la donne affiche IHM à oui pour ne garder que les gestionnaire autorisés a travailler
        String sql = "SELECT p.NOM,p.PRENOM,p.CODE_USER  FROM  PARAM_UTILISATEUR_GESTIONNAIRE p WHERE AFFICHE_IHM = 'OUI' ORDER BY p.NOM ASC ";

        return getJdbcTemplate().query(sql, new RowMapper<ResumeParamUtilisateurGestionnaire>() {

            @Override
            public ResumeParamUtilisateurGestionnaire mapRow(ResultSet rs, int rowNum) throws SQLException {
                return new ResumeParamUtilisateurGestionnaire(rs.getString("CODE_USER"), rs.getString("NOM"), rs.getString("PRENOM"));
            }
        });
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public List<ResumeParamUtilisateurGestionnaire> getListeNomPrenomIdActiveDirectoryPourOngletChangementStatut() {
        String sql = "SELECT p.NOM,p.PRENOM,p.CODE_USER  FROM  PARAM_UTILISATEUR_GESTIONNAIRE p WHERE AFFICHE_IHM = 'OUI' ORDER BY p.NOM ASC ";

        return getJdbcTemplate().query(sql, new RowMapper<ResumeParamUtilisateurGestionnaire>() {

            @Override
            public ResumeParamUtilisateurGestionnaire mapRow(ResultSet rs, int rowNum) throws SQLException {
                return new ResumeParamUtilisateurGestionnaire(rs.getString("CODE_USER"), rs.getString("NOM"), rs.getString("PRENOM") + " - ");
            }
        });
    }

    @Override
    public List<ResumeParamUtilisateurGestionnaire> getListeNomsEtCodeUsers(String premiereValeurListelibelle, String premiereValeurListeCode) {
        List<ResumeParamUtilisateurGestionnaire> listeUsers = getListeNomPrenomIdActiveDirectory();
        ResumeParamUtilisateurGestionnaire nonRenseigneUser = new ResumeParamUtilisateurGestionnaire(premiereValeurListeCode,
                premiereValeurListelibelle);
        ResumeParamUtilisateurGestionnaire autreGestUser = new ResumeParamUtilisateurGestionnaire(PeriodesRecuesCriteresRecherche.AUTRE_GEST_CODE,
                PeriodesRecuesCriteresRecherche.AUTRE_GEST_LIBELLE);
        ResumeParamUtilisateurGestionnaire champsVidePourPlaceholder = new ResumeParamUtilisateurGestionnaire();

        int dernierIndex = listeUsers.size() + 1;
        listeUsers.add(0, nonRenseigneUser);
        listeUsers.add(dernierIndex, autreGestUser);
        listeUsers.add(0, champsVidePourPlaceholder);

        return listeUsers;
    }

    @Override
    public List<ResumeParamUtilisateurGestionnaire> getlistesAffecteAEtAtraiterParSansAutreGest(String premiereValeurListelibelle,
            String premiereValeurListeCode) {
        List<ResumeParamUtilisateurGestionnaire> listeUsers = getListeNomPrenomIdActiveDirectoryPourOngletChangementStatut();
        ResumeParamUtilisateurGestionnaire premierItem = new ResumeParamUtilisateurGestionnaire(premiereValeurListeCode, premiereValeurListelibelle);
        ResumeParamUtilisateurGestionnaire champsVide = new ResumeParamUtilisateurGestionnaire();

        listeUsers.add(0, premierItem);
        listeUsers.add(0, champsVide);

        return listeUsers;
    }

}
