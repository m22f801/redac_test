package fr.si2m.red.internal.dsn;

import java.util.List;

import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import fr.si2m.red.core.repository.jpa.JpaEntiteImportableRepository;
import fr.si2m.red.dsn.Affiliation;
import fr.si2m.red.dsn.AffiliationRepository;

/**
 * Base de données des entités {@link Affiliation}, connectée via JPA.
 * 
 * @author poidij
 *
 */
@Repository
public class JpaAffiliationRepository extends JpaEntiteImportableRepository<Affiliation> implements AffiliationRepository {

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public boolean existeUneAffiliation(String idAffiliation) {
        TypedQuery<Long> query = getEntityManager().createQuery("SELECT count(*) FROM Affiliation a WHERE a.idAffiliation = :idAffiliation",
                Long.class);
        query.setParameter("idAffiliation", idAffiliation);
        List<Long> result = query.getResultList();
        if (result.isEmpty() || result.get(0) == null) {
            return false;
        }
        return result.get(0) > 0;
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public List<Affiliation> getPourContratTravail(String idContratTravail) {
        TypedQuery<Affiliation> query = getEntityManager().createQuery(
                "SELECT a FROM Affiliation a WHERE a.idContratTravail = :idContratTravail AND a.ligneEnCoursImportBatch IS FALSE", Affiliation.class);
        query.setParameter("idContratTravail", idContratTravail);
        return query.getResultList();
    }
}
