package fr.si2m.red.internal.reconciliation;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import fr.si2m.red.core.repository.jpa.JpaRepository;
import fr.si2m.red.parametrage.ParamUtilisateurGestionnaire;
import fr.si2m.red.reconciliation.ParamGestionnaireUtilisateurRepository;

/**
 * Base de données des entités {@link ParamUtilisateurGestionnaire}, connectée via JPA.
 * 
 * @author poidij
 *
 */
@Repository
public class JpaParamUtilisateurGestionnaire extends JpaRepository<ParamUtilisateurGestionnaire> implements ParamGestionnaireUtilisateurRepository {

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public List<ParamUtilisateurGestionnaire> rechercheGestionnaireSiren(String numSiren) {
        String sql = "call Recherche_gestionnaire_siren(:numSiren)";
        Map<String, Object> params = new HashMap<>();
        params.put("numSiren", numSiren);
        return getJdbcTemplate().query(sql, params, new ParamUtilisateurGestionnaireRowMapper());
    }

    public static class ParamUtilisateurGestionnaireRowMapper implements RowMapper<ParamUtilisateurGestionnaire> {
        @Override
        public ParamUtilisateurGestionnaire mapRow(ResultSet rs, int rowNum) throws SQLException {
            ParamUtilisateurGestionnaire pg = new ParamUtilisateurGestionnaire();
            pg.setIdentifiantActiveDirectory(rs.getString("OUT_CODE_USER"));
            pg.setNom(rs.getString("OUT_NOM_USER"));
            pg.setPrenom(rs.getString("OUT_PRENOM_USER"));
            pg.setNiveau1(rs.getString("OUT_NIVEAU_1"));
            pg.setNiveau2(rs.getString("OUT_NIVEAU_2"));
            pg.setNiveau3(rs.getString("OUT_NIVEAU_3"));
            return pg;
        }
    }

    @Override
    public Class<ParamUtilisateurGestionnaire> getClassePrototypeEntite() {
        return ParamUtilisateurGestionnaire.class;
    }

}