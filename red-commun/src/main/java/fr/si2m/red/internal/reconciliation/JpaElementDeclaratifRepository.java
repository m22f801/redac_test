package fr.si2m.red.internal.reconciliation;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import fr.si2m.red.Entite;
import fr.si2m.red.core.repository.jpa.JpaRepository;
import fr.si2m.red.reconciliation.ElementDeclaratifRepository;
import fr.si2m.red.reconciliation.ResumeDecompteEffectifExcel;

/**
 * Services de manipulation de données transverses aux éléments déclaratifs.
 * 
 * @author nortaina
 * 
 * @param <T>
 *            le type d'entité géré
 *
 */
public abstract class JpaElementDeclaratifRepository<T extends Entite> extends JpaRepository<T> implements ElementDeclaratifRepository<T> {

    /**
     * Le nom de paramètre utilisé pour l'identifiant de période.
     */
    private static final String PARAM_ID_PERIODE = "idPeriode";

    /**
     * Nom du paramètre utilisé pour le premier index à récupérer sur une requête
     */
    private static final String PARAM_INDEX_PREMIER_RESULTAT = "indexPremierResultat";

    /**
     * Paramètre taille de la page
     */
    private static final String PARAM_TAILLE_PAGE = "taillePage";

    @Override
    @Transactional(propagation = Propagation.MANDATORY)
    public void supprimePourPeriode(Long idPeriode) {
        Query query = getEntityManager()
                .createQuery("DELETE FROM " + getClassePrototypeEntite().getSimpleName() + " WHERE idPeriode = :" + PARAM_ID_PERIODE);
        query.setParameter(PARAM_ID_PERIODE, idPeriode);
        query.executeUpdate();
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public List<T> getPourPeriode(Long idPeriode) {
        String jpql = "SELECT ct FROM " + getClassePrototypeEntite().getSimpleName() + " ct WHERE ct.idPeriode = :" + PARAM_ID_PERIODE;
        TypedQuery<T> query = getEntityManager().createQuery(jpql, getClassePrototypeEntite());
        query.setParameter(PARAM_ID_PERIODE, idPeriode);
        return query.getResultList();
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public List<ResumeDecompteEffectifExcel> getDecomptesEffectifPourPeriodePagines(Long idPeriode, Integer indexPremierResultat,
            Integer taillePage) {
        String sql = "SELECT adh.DATE_FICHIER AS DATE_CONSTITUTION, adh.MOIS_RATTACHEMENT AS MOIS_RATTACHEMENT,"
                + " adh.MOIS_DECLARE AS MOIS_DECLARE, adh.SIREN_ENTREPRISE AS SIREN, adh.NIC_ETABLISSEMENT AS NIC,"

                + " i.IDENTIFIANT_REPERTOIRE AS NIR, i.NTT AS NTT, i.MATRICULE AS NUM_ADHERENT, i.SEXE AS SEXE, i.NOM_FAMILLE AS NOM_FAMILLE,"
                + " i.NOM_USAGE AS NOM_USAGE, i.PRENOM AS PRENOM, i.DATE_NAISSANCE AS DATE_NAISSANCE,"

                + " ct.DATE_DEBUT_CONTRAT AS DATE_DEBUT_CONTRAT, ct.DATE_FIN_PREVISIONNELLE AS DATE_FIN_PREVISIONNELLE,"
                + " ct.LIBELLE_EMPLOI AS LIBELLE_EMPLOI, ct.NUMERO_CONTRAT AS NUMERO_CONTRAT,"

                + " ecct.NOCAT AS NOCAT, ecct.DEBUT_BASE AS DATE_DEBUT_BASE, ecct.FIN_BASE AS DATE_FIN_BASE, ecct.NOMBRE_AFFILIES AS NOMBRE_AFFILIES,"
                + " ecct.AYANTS_DROIT_ADULTE AS AYANTS_DROIT_ADULTE, ecct.AYANTS_DROIT_AUTRE AS AYANTS_DROIT_AUTRE,"
                + " ecct.AYANTS_DROIT_ENFANT AS AYANTS_DROIT_ENFANT, ecct.TOTAL_EFFECTIF AS TOTAL_EFFECTIF"

                + " FROM EFFECTIF_CATEGORIE_CONTRAT_TRAVAIL ecct"
                + " LEFT JOIN CONTRAT_TRAVAIL ct on ecct.ID_CONTRAT_TRAVAIL=ct.ID_CONTRAT_TRAVAIL AND ct.TMP_BATCH=ecct.TMP_BATCH"
                + " LEFT JOIN INDIVIDU i ON i.ID_INDIVIDU=ct.ID_INDIVIDU AND i.TMP_BATCH=ct.TMP_BATCH"
                + " LEFT JOIN ADHESION_ETABLISSEMENT_MOIS adh ON adh.ID_ADH_ETAB_MOIS=i.ID_ADH_ETAB_MOIS AND adh.TMP_BATCH=i.TMP_BATCH"

                + " WHERE ecct.ID_PERIODE = :" + PARAM_ID_PERIODE
                + " ORDER BY adh.ID_ADH_ETAB_MOIS,i.NOM_FAMILLE,i.NOM_USAGE,i.PRENOM LIMIT :taillePage OFFSET :indexPremierResultat";

        Map<String, Object> params = new HashMap<>();
        params.put(PARAM_INDEX_PREMIER_RESULTAT, indexPremierResultat);
        params.put(PARAM_TAILLE_PAGE, taillePage);
        params.put(PARAM_ID_PERIODE, idPeriode);
        return getJdbcTemplate().query(sql, params, new ResumeDecompteEffectifExcelRowMapper());

    }

    /**
     * Mapper de lignes de la declaration individu vers Excel.
     * 
     * @author rupip
     *
     */
    public static class ResumeDecompteEffectifExcelRowMapper implements RowMapper<ResumeDecompteEffectifExcel> {

        @Override
        public ResumeDecompteEffectifExcel mapRow(ResultSet rs, int rowNum) throws SQLException {
            ResumeDecompteEffectifExcel decompteEffectif = new ResumeDecompteEffectifExcel();

            // Adhésion
            decompteEffectif.setDateConstitution(getInteger(rs, "DATE_CONSTITUTION"));
            decompteEffectif.setMoisRattachement(getInteger(rs, "MOIS_RATTACHEMENT"));
            decompteEffectif.setMoisDeclare(getInteger(rs, "MOIS_DECLARE"));
            decompteEffectif.setSiren(rs.getString("SIREN"));
            decompteEffectif.setNic(rs.getString("NIC"));

            // Individu
            decompteEffectif.setNir(rs.getString("NIR"));
            decompteEffectif.setNtt(rs.getString("NTT"));
            decompteEffectif.setNumAdherent(rs.getString("NUM_ADHERENT"));
            decompteEffectif.setSexe(rs.getString("SEXE"));
            decompteEffectif.setNomFamille(rs.getString("NOM_FAMILLE"));
            decompteEffectif.setNomUsage(rs.getString("NOM_USAGE"));
            decompteEffectif.setPrenom(rs.getString("PRENOM"));
            decompteEffectif.setDateNaissance(getInteger(rs, "DATE_NAISSANCE"));

            // Contrat travail
            decompteEffectif.setDateDebutContrat(getInteger(rs, "DATE_DEBUT_CONTRAT"));
            decompteEffectif.setDateFinPrevisionnelle(getInteger(rs, "DATE_FIN_PREVISIONNELLE"));
            decompteEffectif.setLibelleEmploi(rs.getString("LIBELLE_EMPLOI"));
            decompteEffectif.setNumeroContrat(rs.getString("NUMERO_CONTRAT"));

            // Décompte des effectifs
            decompteEffectif.setNumCategorieQuittancement(rs.getString("NOCAT"));
            decompteEffectif.setDateDebutBase(getInteger(rs, "DATE_DEBUT_BASE"));
            decompteEffectif.setDateFinBase(getInteger(rs, "DATE_FIN_BASE"));
            decompteEffectif.setNombreAffilies(getInteger(rs, "NOMBRE_AFFILIES"));
            decompteEffectif.setAyantsDroitAdulte(getInteger(rs, "AYANTS_DROIT_ADULTE"));
            decompteEffectif.setAyantsDroitAutre(getInteger(rs, "AYANTS_DROIT_AUTRE"));
            decompteEffectif.setAyantsDroitEnfant(getInteger(rs, "AYANTS_DROIT_ENFANT"));
            decompteEffectif.setTotalEffectif(rs.getLong("TOTAL_EFFECTIF"));

            return decompteEffectif;
        }
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public List<T> getPourPeriodePagines(Long idPeriode, Integer indexPremierResultat, Integer taillePage) {
        String jpql = "SELECT ct FROM " + getClassePrototypeEntite().getSimpleName() + " ct WHERE ct.idPeriode = :" + PARAM_ID_PERIODE
                + " ORDER BY ct.idTechnique";
        TypedQuery<T> query = getEntityManager().createQuery(jpql, getClassePrototypeEntite());
        appliqueParametragePagination(query, indexPremierResultat, taillePage);
        query.setParameter(PARAM_ID_PERIODE, idPeriode);
        return query.getResultList();
    }
}
