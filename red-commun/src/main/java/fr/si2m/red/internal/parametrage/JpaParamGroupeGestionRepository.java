package fr.si2m.red.internal.parametrage;

import org.springframework.stereotype.Repository;

import fr.si2m.red.core.repository.jpa.JpaEntiteImportableRepository;
import fr.si2m.red.parametrage.ParamGroupeGestion;
import fr.si2m.red.parametrage.ParamGroupeGestionRepository;

/**
 * Base de données des entités ParamGroupeGestion, connectée via JPA.
 * 
 * @author nortaina
 *
 */
@Repository
public class JpaParamGroupeGestionRepository extends JpaEntiteImportableRepository<ParamGroupeGestion> implements ParamGroupeGestionRepository {

}
