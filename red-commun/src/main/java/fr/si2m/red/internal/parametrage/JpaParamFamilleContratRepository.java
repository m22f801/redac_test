package fr.si2m.red.internal.parametrage;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.persistence.TypedQuery;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import fr.si2m.red.core.repository.jpa.JpaEntiteImportableRepository;
import fr.si2m.red.parametrage.ParamFamilleContrat;
import fr.si2m.red.parametrage.ParamFamilleContratId;
import fr.si2m.red.parametrage.ParamFamilleContratRepository;
import fr.si2m.red.parametrage.TypeDesignationFamille;

/**
 * Base de données des entités ParamFamilleContrat, connectée via JPA.
 * 
 * @author nortaina
 *
 */
@Repository
public class JpaParamFamilleContratRepository extends JpaEntiteImportableRepository<ParamFamilleContrat> implements ParamFamilleContratRepository {

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public String getDesignationFamille(Integer numFamille, String groupeGestion) {
        ParamFamilleContrat paramFamilleContrat = getEntityManager().find(ParamFamilleContrat.class,
                new ParamFamilleContratId(false, numFamille, groupeGestion));
        if (paramFamilleContrat != null) {
            getEntityManager().detach(paramFamilleContrat);
            return paramFamilleContrat.getDesignationFamille();
        } else {
            return null;
        }
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public String getApplicationSiAval(Integer numFamille, String groupeGestion) {
        ParamFamilleContrat paramFamilleContrat = getEntityManager().find(ParamFamilleContrat.class,
                new ParamFamilleContratId(false, numFamille, groupeGestion));
        if (paramFamilleContrat != null) {
            getEntityManager().detach(paramFamilleContrat);
            return paramFamilleContrat.getApplicationAvalEnCharge();
        } else {
            return null;
        }
    }

    @Override
    public List<Integer> getNumFamillesPourTypesDeDesignation(List<String> typesDesignations) {
        if (typesDesignations == null || typesDesignations.isEmpty()) {
            return new ArrayList<Integer>();
        }
        StringBuilder jpqlBuilder = new StringBuilder("SELECT p.numFamille FROM ParamFamilleContrat p WHERE p.ligneEnCoursImportBatch IS FALSE AND ");
        Iterator<String> it = typesDesignations.iterator();
        while (it.hasNext()) {
            String rawDesignation = it.next();
            if (!StringUtils.isBlank(rawDesignation)) {
                TypeDesignationFamille typeDesignation = TypeDesignationFamille.valueOf(rawDesignation);
                jpqlBuilder.append("LOWER(p.designationFamille) LIKE LOWER('%").append(typeDesignation.getMotCleDesignation()).append("%')");
                if (it.hasNext()) {
                    jpqlBuilder.append(" OR ");
                }
            }
        }
        TypedQuery<Integer> query = getEntityManager().createQuery(jpqlBuilder.toString(), Integer.class);
        return query.getResultList();
    }

    @Override
    public String getPortefeuille(Integer numFamille, String groupeGestion) {
        ParamFamilleContrat paramFamilleContrat = getEntityManager().find(ParamFamilleContrat.class,
                new ParamFamilleContratId(false, numFamille, groupeGestion));
        if (paramFamilleContrat != null) {
            getEntityManager().detach(paramFamilleContrat);
            return paramFamilleContrat.getPortefeuille();
        } else {
            return null;
        }
    }

    @Override
    public boolean existeCoupleNofamNmgrpges(Integer numFamille, String groupeGestion) {
        ParamFamilleContrat paramFamilleContrat = getEntityManager().find(ParamFamilleContrat.class,
                new ParamFamilleContratId(false, numFamille, groupeGestion));
        if (paramFamilleContrat != null) {
            getEntityManager().detach(paramFamilleContrat);
            return true;
        } else {
            return false;
        }
    }
}
