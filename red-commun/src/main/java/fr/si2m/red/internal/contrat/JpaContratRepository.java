package fr.si2m.red.internal.contrat;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.TypedQuery;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import fr.si2m.red.contrat.Contrat;
import fr.si2m.red.contrat.ContratId;
import fr.si2m.red.contrat.ContratRepository;
import fr.si2m.red.contrat.ResumeContrat;
import fr.si2m.red.core.repository.jpa.JpaEntiteImportableRepository;
import fr.si2m.red.parametrage.ParamFamilleContratRepository;
import lombok.Setter;

/**
 * Base de données des entités contrat, connectée via JPA.
 * 
 * @author nortaina
 *
 */
@Repository
public class JpaContratRepository extends JpaEntiteImportableRepository<Contrat> implements ContratRepository {

    private static final String PARAM_NUM_CONTRAT = "numContrat";
    private static final String PARAM_DATE_DEBUT = "dateDebutSituationLigne";
    private static final String PARAM_ID_PERIODE = "idPeriode";

    /**
     * Le référentiel des familles de contrats connues.
     * 
     * @return le référentiel des familles de contrats connues
     */
    @Autowired
    @Setter
    private ParamFamilleContratRepository paramFamilleContratRepository;

    @Override
    @Transactional(propagation = Propagation.MANDATORY)
    public Contrat getContrat(String numContrat, Integer dateDebutSituationLigne) {
        return get(new ContratId(false, numContrat, dateDebutSituationLigne));
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public Integer getDateDebutMinimumContratTemporaire(String numContrat) {
        TypedQuery<Integer> query = getEntityManager().createQuery(
                "SELECT c.dateDebutSituationLigne FROM Contrat c WHERE c.numContrat = :numContrat and c.ligneEnCoursImportBatch IS TRUE ORDER BY c.dateDebutSituationLigne",
                Integer.class).setMaxResults(1);
        query.setParameter(PARAM_NUM_CONTRAT, numContrat);
        List<Integer> result = query.getResultList();
        return getPremierResultatSiExiste(result);
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public Integer getDateDebutDeLaSituationContratSuivante(String numContrat, Integer dateDebutSituationLigne) {
        TypedQuery<Integer> query = getEntityManager().createQuery(
                "SELECT c.dateDebutSituationLigne FROM Contrat c WHERE c.numContrat = :numContrat "
                        + "and c.dateDebutSituationLigne > :dateDebutSituationLigne and c.ligneEnCoursImportBatch IS TRUE ORDER BY c.dateDebutSituationLigne",
                Integer.class).setMaxResults(1);
        query.setParameter(PARAM_NUM_CONTRAT, numContrat);
        query.setParameter(PARAM_DATE_DEBUT, dateDebutSituationLigne);
        return getPremierResultatSiExiste(query.getResultList());
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public Contrat getContratValidePourDate(String numContrat, Integer dateValidite) {
        String jpql = "SELECT c FROM Contrat c WHERE c.numContrat = :numContrat AND c.ligneEnCoursImportBatch IS FALSE AND c.dateDebutSituationLigne <= :dateValidite "
                + "AND coalesce(c.dateFinSituationLigne, 99999999) >= :dateValidite ORDER BY c.dateDebutSituationLigne DESC";
        TypedQuery<Contrat> query = getEntityManager().createQuery(jpql, Contrat.class).setMaxResults(1);
        query.setParameter(PARAM_NUM_CONTRAT, numContrat);
        query.setParameter("dateValidite", dateValidite);
        return getPremierResultatSiExiste(query.getResultList());
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public Contrat getDerniereSituationContrat(String numContrat) {
        TypedQuery<Contrat> query = getEntityManager().createQuery(
                "SELECT c FROM Contrat c WHERE c.numContrat = :numContrat and c.ligneEnCoursImportBatch IS FALSE "
                        + "and coalesce(c.dateFinSituationLigne, 99999999) = "
                        + "(SELECT MAX(coalesce(co.dateFinSituationLigne, 99999999)) FROM Contrat co WHERE co.numContrat = :numContrat)",
                Contrat.class).setMaxResults(1);
        query.setParameter(PARAM_NUM_CONTRAT, numContrat);
        return getPremierResultatSiExiste(query.getResultList());
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public long compteContratsEligiblesDSN(String groupeGestion, Integer numFamilleProduit) {
        StringBuilder queryBuilder = new StringBuilder("SELECT COUNT(c) FROM Contrat c WHERE c.ligneEnCoursImportBatch IS FALSE "
                + "AND (c.eligibiliteContratDsn = 'SRT' OR c.eligibiliteContratDsn = 'SRN') AND coalesce(c.dateFinSituationLigne, 99999999) = "
                + "(SELECT MAX(coalesce(co.dateFinSituationLigne, 99999999)) FROM Contrat co WHERE co.numContrat = c.numContrat)");
        if (StringUtils.isNotBlank(groupeGestion)) {
            queryBuilder.append(" AND c.groupeGestion = :groupeGestion");
        }
        if (numFamilleProduit != null) {
            queryBuilder.append(" AND c.numFamilleProduit = :numFamilleProduit");
        }
        TypedQuery<Long> query = getEntityManager().createQuery(queryBuilder.toString(), Long.class);
        if (StringUtils.isNotBlank(groupeGestion)) {
            query.setParameter("groupeGestion", groupeGestion);
        }
        if (numFamilleProduit != null) {
            query.setParameter("numFamilleProduit", numFamilleProduit);
        }
        return query.getSingleResult();
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public long compteContratsEligiblesDSN(List<String> groupesGestion, List<Integer> numsFamilleProduit, Integer dateDuJour) {

        StringBuilder query = new StringBuilder(
                "SELECT COUNT(*) FROM contrats c WHERE c.TMP_BATCH IS FALSE AND c.ELIGDSN <> 'NON' AND c.DT_DEBUT_SIT <= :dateDuJour "
                        + "AND (:dateDuJour <= coalesce(c.DT_FIN_SIT, 99999999) OR :dateDuJour <= (CASE WHEN (c.DT_FIN_SIT = 0) THEN 99999999 ELSE c.DT_FIN_SIT END))");

        if (groupesGestion != null && !groupesGestion.isEmpty()) {
            query.append(" AND c.NMGRPGES IN (:groupesGestion)");
        }
        if (numsFamilleProduit != null && !numsFamilleProduit.isEmpty()) {
            query.append(" AND c.NOFAM IN (:numsFamilleProduit)");
        }

        Map<String, Object> params = new HashMap<String, Object>();
        params.put("dateDuJour", dateDuJour);
        if (groupesGestion != null && !groupesGestion.isEmpty()) {
            params.put("groupesGestion", groupesGestion);
        }
        if (numsFamilleProduit != null && !numsFamilleProduit.isEmpty()) {

            params.put("numsFamilleProduit", numsFamilleProduit);
        }

        return getPremierResultatSiExiste(getJdbcTemplate().query(query.toString(), params, new RowMapper<Long>() {

            @Override
            public Long mapRow(ResultSet rs, int rowNum) throws SQLException {
                return rs.getLong(1);
            }
        }));

    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public String getTypeConsolidationSalaire(String numContrat, Integer dateEffet) {
        String jpql = "SELECT p.typeConsolidationSalaire FROM ParamFamilleContrat p, Contrat c WHERE p.numFamille = c.numFamilleProduit AND p.groupeGestion = c.groupeGestion "
                + "AND p.ligneEnCoursImportBatch IS FALSE AND c.ligneEnCoursImportBatch IS FALSE "
                + "AND c.numContrat = :numContrat AND c.dateDebutSituationLigne <= :dateEffet "
                + "AND (coalesce(c.dateFinSituationLigne, 99999999) >= :dateEffet "
                + "OR (CASE WHEN (c.dateFinSituationLigne = 0) THEN 99999999 ELSE c.dateFinSituationLigne END) >= :dateEffet)";
        TypedQuery<String> query = getEntityManager().createQuery(jpql, String.class).setMaxResults(1);
        query.setParameter(PARAM_NUM_CONTRAT, numContrat);
        query.setParameter("dateEffet", dateEffet);
        return getPremierResultatSiExiste(query.getResultList());
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public Contrat getPlusAncienNonActifSurPeriode(String numContrat, Integer dateDebutPeriode, Integer dateFinPeriode) {
        String jpql = "SELECT c FROM ParamEtatContrat p, Contrat c WHERE c.numContrat = :numContrat AND p.etatContrat = c.etatContrat "
                + "AND p.ligneEnCoursImportBatch IS FALSE AND c.ligneEnCoursImportBatch IS FALSE "
                + "AND p.actifAsText = 'N' AND c.dateDebutSituationLigne <= :dateFinPeriode "
                + "AND (coalesce(c.dateFinSituationLigne, 99999999) >= :dateDebutPeriode "
                + "OR (CASE WHEN (c.dateFinSituationLigne = 0) THEN 99999999 ELSE c.dateFinSituationLigne END) >= :dateDebutPeriode) "
                + "ORDER BY c.dateDebutSituationLigne ASC";
        TypedQuery<Contrat> query = getEntityManager().createQuery(jpql, Contrat.class).setMaxResults(1);
        query.setParameter(PARAM_NUM_CONTRAT, numContrat);
        query.setParameter("dateDebutPeriode", dateDebutPeriode);
        query.setParameter("dateFinPeriode", dateFinPeriode);
        return getPremierResultatSiExiste(query.getResultList());
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public boolean existeContrat(String numContrat) {
        TypedQuery<Long> query = getEntityManager()
                .createQuery("SELECT count(*) FROM Contrat c WHERE c.numContrat = :numContrat and c.ligneEnCoursImportBatch IS FALSE", Long.class);
        query.setParameter(PARAM_NUM_CONTRAT, numContrat);
        List<Long> result = query.getResultList();

        if (result.isEmpty() || result.get(0) == null) {
            return false;
        }
        return result.get(0) > 0;
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public Integer getDateResiliation(String numContrat) {
        TypedQuery<Integer> query = getEntityManager().createQuery(
                "SELECT c.dateDebutSituationLigne FROM Contrat c WHERE c.numContrat = :numContrat AND c.etatContrat in ('07', '08') AND c.ligneEnCoursImportBatch IS FALSE ORDER BY c.dateDebutSituationLigne DESC",
                Integer.class).setMaxResults(1);
        query.setParameter(PARAM_NUM_CONTRAT, numContrat);
        List<Integer> result = query.getResultList();
        return getPremierResultatSiExiste(result);
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public Contrat getSituationContratPrecedente(String numContrat, Integer dateDebutSituationLigne) {
        TypedQuery<Contrat> query = getEntityManager().createQuery(
                "SELECT c FROM Contrat c WHERE c.numContrat = :numContrat "
                        + "and c.dateDebutSituationLigne < :dateDebutSituationLigne and c.ligneEnCoursImportBatch IS FALSE ORDER BY c.dateDebutSituationLigne DESC",
                Contrat.class).setMaxResults(1);
        query.setParameter(PARAM_NUM_CONTRAT, numContrat);
        query.setParameter(PARAM_DATE_DEBUT, dateDebutSituationLigne);
        return getPremierResultatSiExiste(query.getResultList());
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public Contrat getSituationContratSuivante(String numContrat, Integer dateDebutSituationLigne) {
        TypedQuery<Contrat> query = getEntityManager().createQuery(
                "SELECT c FROM Contrat c WHERE c.numContrat = :numContrat "
                        + "and c.dateDebutSituationLigne > :dateDebutSituationLigne and c.ligneEnCoursImportBatch IS FALSE ORDER BY c.dateDebutSituationLigne",
                Contrat.class).setMaxResults(1);
        query.setParameter(PARAM_NUM_CONTRAT, numContrat);
        query.setParameter(PARAM_DATE_DEBUT, dateDebutSituationLigne);
        return getPremierResultatSiExiste(query.getResultList());
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public ResumeContrat getContratPourPeriodePublieeGERD(Long idPeriode) {
        String sql = "SELECT c.DTEFFCO FROM PERIODES_RECUES p, CONTRATS c, PARAM_FAMILLE_CONTRAT pfc"
                + " WHERE p.ETAT_PERIODE = 'ENC' AND p.NUMERO_CONTRAT = c.NOCO AND c.NOFAM = pfc.NOFAM"
                + " AND (p.DATE_FIN_PERIODE <= coalesce(c.DT_FIN_SIT, 99999999) OR p.DATE_FIN_PERIODE <= (CASE WHEN (c.DT_FIN_SIT = 0) THEN 99999999 ELSE c.DT_FIN_SIT END))"
                + " AND p.DATE_FIN_PERIODE >= c.DT_DEBUT_SIT " + " AND pfc.SI_AVAL = 'GERD'" + " AND p.ID_PERIODE =:" + PARAM_ID_PERIODE
                + " ORDER BY c.DTEFFCO DESC";

        Map<String, Object> params = new HashMap<>();
        params.put(PARAM_ID_PERIODE, idPeriode);
        return getPremierResultatSiExiste(getJdbcTemplate().query(sql, params, new ContratPeriodePublieeRowMapper()));
    }

    /**
     * Mapper de lignes de résumés d'individu.
     * 
     * @author eudesr
     *
     */
    public static class ContratPeriodePublieeRowMapper implements RowMapper<ResumeContrat> {

        @Override
        public ResumeContrat mapRow(ResultSet rs, int rowNum) throws SQLException {
            ResumeContrat contrat = new ResumeContrat();
            contrat.setDteffco(rs.getInt("DTEFFCO"));
            return contrat;
        }

    }

    @Override
    public String getRaisonSocialeClientContrat(String numContrat) {
        String sql = "SELECT cl.LRSO AS LRSO FROM CLIENTS cl WHERE cl.NOCLI= (SELECT c.NOCLI FROM CONTRATS c WHERE c.NOCO=:numContrat"
                + " AND c.DT_FIN_SIT IS NULL LIMIT 1) LIMIT 1";
        Map<String, Object> params = new HashMap<>();
        params.put(PARAM_NUM_CONTRAT, numContrat);
        return getPremierResultatSiExiste(getJdbcTemplate().query(sql, params, new RaisonSocialeClientRowMapper()));
    }

    /**
     * Mapper la raison sociale de l'individu
     * 
     * @author eudesr
     *
     */
    public static class RaisonSocialeClientRowMapper implements RowMapper<String> {

        @Override
        public String mapRow(ResultSet rs, int rowNum) throws SQLException {
            return rs.getString("LRSO");
        }

    }

    /**
     * Mapper du nombre de contrats retournés
     * 
     * @author richardg
     *
     */
    public static class CountContratDansPeriodeRowMapper implements RowMapper<Integer> {

        @Override
        public Integer mapRow(ResultSet rs, int rowNum) throws SQLException {
            return rs.getInt(1); // On retourne la valeur de la colonne 1 de la requete (le count)
        }
    }

    @Override
    public Integer getNombreContratActifPourPeriodeDonnee(Integer dateDebutPeriode, Integer dateFinPeriode, String numContrat) {
        String sql = "SELECT COUNT(DISTINCT TXAPPCOT) from contrats c WHERE c.NOCO = :numContrat "
                + " AND ((DT_DEBUT_SIT >= :dateDebutPeriode AND DT_DEBUT_SIT <= :dateFinPeriode) "
                + " OR (DT_DEBUT_SIT < :dateDebutPeriode AND COALESCE(DT_FIN_SIT,99999999) >= :dateDebutPeriode))";

        Map<String, Object> params = new HashMap<>();
        params.put("dateDebutPeriode", dateDebutPeriode);
        params.put("dateFinPeriode", dateFinPeriode);
        params.put(PARAM_NUM_CONTRAT, numContrat);

        return getPremierResultatSiExiste(getJdbcTemplate().query(sql, params, new CountContratDansPeriodeRowMapper()));
    }

    @Override
    public Double getTauxAppelContratValidePourDate(String numContrat, Integer date) {
        String jpql = "SELECT c.tauxAppel FROM Contrat c WHERE c.numContrat = :numContrat AND c.ligneEnCoursImportBatch IS FALSE AND c.dateDebutSituationLigne <= :dateValidite "
                + "AND coalesce(c.dateFinSituationLigne, 99999999) >= :dateValidite ORDER BY c.dateDebutSituationLigne DESC";
        TypedQuery<Double> query = getEntityManager().createQuery(jpql, Double.class).setMaxResults(1);
        query.setParameter(PARAM_NUM_CONTRAT, numContrat);
        query.setParameter("dateValidite", date);
        return getPremierResultatSiExiste(query.getResultList());
    }

}
