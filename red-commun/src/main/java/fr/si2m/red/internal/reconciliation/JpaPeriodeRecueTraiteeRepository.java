package fr.si2m.red.internal.reconciliation;

import java.util.List;

import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import fr.si2m.red.core.repository.jpa.JpaRepository;
import fr.si2m.red.reconciliation.PeriodeRecueTraitee;
import fr.si2m.red.reconciliation.PeriodeRecueTraiteeRepository;

/**
 * Base de données des entités {@link PeriodeRecueTraitee}, connectée via JPA.
 * 
 * @author benitahy
 *
 */
@Repository
public class JpaPeriodeRecueTraiteeRepository extends JpaRepository<PeriodeRecueTraitee> implements PeriodeRecueTraiteeRepository {

    @Override
    public Class<PeriodeRecueTraitee> getClassePrototypeEntite() {
        return PeriodeRecueTraitee.class;
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public boolean existe(String numeroContrat, Integer dateDebutPeriode) {
        String jpql = "SELECT COUNT(p) FROM PeriodeRecueTraitee p WHERE p.numeroContrat = :numeroContrat AND p.dateDebutPeriode = :dateDebutPeriode";
        TypedQuery<Long> query = getEntityManager().createQuery(jpql, Long.class);
        query.setParameter("numeroContrat", numeroContrat);
        query.setParameter("dateDebutPeriode", dateDebutPeriode);
        List<Long> result = query.getResultList();
        if (result.isEmpty() || result.get(0) == null) {
            return false;
        }
        return result.get(0) > 0;
    }
}
