package fr.si2m.red.internal.parametrage;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import fr.si2m.red.core.repository.jpa.JpaEntiteImportableRepository;
import fr.si2m.red.parametrage.ParamControleSignalRejet;
import fr.si2m.red.parametrage.ParamControleSignalRejetRepository;

/**
 * Base de données des entités {@link ParamControleSignalRejet}, connectée via JPA.
 * 
 * @author nortaina
 *
 */
@Repository
public class JpaParamControleSignalRejetRepository extends JpaEntiteImportableRepository<ParamControleSignalRejet>
        implements ParamControleSignalRejetRepository {

    private static final String JPA_SELECT_PARAM_CONTROLE_SIGNAL_REJET = "SELECT p FROM ParamControleSignalRejet p WHERE p.ligneEnCoursImportBatch IS FALSE ";

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public Map<String, ParamControleSignalRejet> getControlesGlobaux() {
        String jpql = new StringBuilder(JPA_SELECT_PARAM_CONTROLE_SIGNAL_REJET).append(" AND p.nofam = 'ALL'").toString();

        TypedQuery<ParamControleSignalRejet> query = getEntityManager().createQuery(jpql, ParamControleSignalRejet.class);
        List<ParamControleSignalRejet> controlesGlobaux = query.getResultList();
        Map<String, ParamControleSignalRejet> controlesGlobauxIndexes = new HashMap<>(controlesGlobaux.size());
        for (ParamControleSignalRejet controle : controlesGlobaux) {
            controlesGlobauxIndexes.put(controle.getIdentifiantControle(), controle);
        }
        return controlesGlobauxIndexes;

    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public List<ParamControleSignalRejet> getControlesOrdoneesDistinct() {
        String jpql = "SELECT p FROM ParamControleSignalRejet p WHERE p.ligneEnCoursImportBatch IS FALSE  GROUP BY p.identifiantControle ORDER BY p.messageAlerte";

        TypedQuery<ParamControleSignalRejet> query = getEntityManager().createQuery(jpql, ParamControleSignalRejet.class);

        return query.getResultList();

    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public ParamControleSignalRejet getControlePourIdControle(String idControle) {
        String jpql = new StringBuilder(JPA_SELECT_PARAM_CONTROLE_SIGNAL_REJET).append(" AND p.identifiantControle= :idControle").toString();

        TypedQuery<ParamControleSignalRejet> query = getEntityManager().createQuery(jpql, ParamControleSignalRejet.class);
        query.setParameter("idControle", idControle);

        return getPremierResultatSiExiste(query.getResultList());

    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public ParamControleSignalRejet getParamControleSignalRejetPourControleEtNofam(String controle, String nofam) {
        String jpql = new StringBuilder(JPA_SELECT_PARAM_CONTROLE_SIGNAL_REJET).append(" AND p.identifiantControle= :controle and p.nofam= :nofam")
                .toString();

        TypedQuery<ParamControleSignalRejet> query = getEntityManager().createQuery(jpql, ParamControleSignalRejet.class);
        query.setParameter("controle", controle);
        query.setParameter("nofam", nofam);
        return getPremierResultatSiExiste(query.getResultList());
    }

}
