package fr.si2m.red.internal.dsn;

import java.util.List;

import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import fr.si2m.red.core.repository.jpa.JpaEntiteImportableRepository;
import fr.si2m.red.dsn.Remuneration;
import fr.si2m.red.dsn.RemunerationRepository;

/**
 * Base de données des entités {@link Remuneration}, connectée via JPA.
 * 
 * @author poidij
 *
 */
@Repository
public class JpaRemunerationRepository extends JpaEntiteImportableRepository<Remuneration> implements RemunerationRepository {

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public boolean existeUneRemuneration(String idRemuneration) {
        TypedQuery<Long> query = getEntityManager().createQuery("SELECT count(*) FROM Remuneration r WHERE r.idRemuneration = :idRemuneration", Long.class);
        query.setParameter("idRemuneration", idRemuneration);
        List<Long> result = query.getResultList();
        if (result.isEmpty() || result.get(0) == null) {
            return false;
        }
        return result.get(0) > 0;
    }

}
