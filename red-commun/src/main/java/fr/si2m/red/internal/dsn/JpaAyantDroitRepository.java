package fr.si2m.red.internal.dsn;

import java.util.List;

import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import fr.si2m.red.core.repository.jpa.JpaEntiteImportableRepository;
import fr.si2m.red.dsn.AyantDroit;
import fr.si2m.red.dsn.AyantDroitRepository;

/**
 * Base de données des entités {@link AyantDroit}, connectée via JPA.
 * 
 * @author poidij
 *
 */
@Repository
public class JpaAyantDroitRepository extends JpaEntiteImportableRepository<AyantDroit> implements AyantDroitRepository {

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public boolean existeUnAyantDroit(String idAyantDroit) {
        TypedQuery<Long> query = getEntityManager().createQuery("SELECT count(*) FROM AyantDroit a WHERE a.idAyantDroit = :idAyantDroit", Long.class);
        query.setParameter("idAyantDroit", idAyantDroit);
        List<Long> result = query.getResultList();
        if (result.isEmpty() || result.get(0) == null) {
            return false;
        }
        return result.get(0) > 0;
    }

}
