package fr.si2m.red.internal.dsn;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.TypedQuery;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import fr.si2m.red.core.repository.jdbc.JdbcRepository;
import fr.si2m.red.core.repository.jpa.JpaEntiteImportableRepository;
import fr.si2m.red.dsn.BaseAssujettie;
import fr.si2m.red.dsn.BaseAssujettieRepository;
import fr.si2m.red.dsn.PlageRattachement;

/**
 * Base de données des entités {@link BaseAssujettie}, connectée via JPA.
 * 
 * @author poidij
 *
 */
@Repository
public class JpaBaseAssujettieRepository extends JpaEntiteImportableRepository<BaseAssujettie> implements BaseAssujettieRepository {

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public boolean existeUneBaseAssujettie(String idBaseAssujettie) {
        TypedQuery<Long> query = getEntityManager().createQuery("SELECT count(*) FROM BaseAssujettie b WHERE b.idBaseAssujettie = :idBaseAssujettie",
                Long.class);
        query.setParameter("idBaseAssujettie", idBaseAssujettie);
        List<Long> result = query.getResultList();
        if (result.isEmpty() || result.get(0) == null) {
            return false;
        }
        return result.get(0) > 0;
    }

    @Override
    public List<PlageRattachement> getPlagesRattachementsPourContratTravailAvecMontantCotisationNonNul(String idContratTravail) {
        String sql = "SELECT DISTINCT DATE_DEB_RATTACHEMENT as dtDebRattachement, MAX(DATE_FIN_RATTACHEMENT) as dtFinRattachement FROM BASE_ASSUJETTIE b "
                + "LEFT JOIN AFFILIATION a ON a.ID_AFFILIATION=b.ID_AFFILIATION "
                + "WHERE a.ID_CONTRAT_TRAVAIL = :idContratTravail AND b.MONTANT_COTISATION <> 0" + " group by b.DATE_DEB_RATTACHEMENT";

        Map<String, Object> params = new HashMap<>();
        params.put("idContratTravail", idContratTravail);

        return getJdbcTemplate().query(sql, params, new PlageRattachementRowMapper());
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public List<BaseAssujettie> getBasesAssujettiesAvecMontantCotisationNonNul(String idContratTravail, Integer dateDebutRattachement) {
        String jpql = "SELECT b FROM BaseAssujettie b LEFT JOIN b.affiliation a "
                + "WHERE a.idContratTravail = :idContratTravail AND b.montantCotisation <> 0 "
                + "AND b.dateDebutRattachement = :dateDebutRattachement";
        TypedQuery<BaseAssujettie> query = getEntityManager().createQuery(jpql, BaseAssujettie.class);
        query.setParameter("idContratTravail", idContratTravail);
        query.setParameter("dateDebutRattachement", dateDebutRattachement);
        return query.getResultList();
    }

    @Override
    public boolean existeBaseAssujettiePourPeriode(Long idPeriode) {
        String sql = "SELECT COUNT(*) FROM BASE_ASSUJETTIE ba INNER JOIN AFFILIATION a  ON ba.ID_AFFILIATION=a.ID_AFFILIATION "
                + " INNER JOIN  CONTRAT_TRAVAIL ct ON a.ID_CONTRAT_TRAVAIL=ct.ID_CONTRAT_TRAVAIL"
                + " INNER JOIN INDIVIDU i ON ct.ID_INDIVIDU=i.ID_INDIVIDU"
                + " INNER JOIN ADHESION_ETABLISSEMENT_MOIS adh ON i.ID_ADH_ETAB_MOIS=adh.ID_ADH_ETAB_MOIS"
                + " INNER JOIN RATTACHEMENT_DECLARATIONS_RECUES r ON adh.ID_ADH_ETAB_MOIS=r.ID_ADH_ETAB_MOIS"
                + " WHERE r.ID_PERIODE= :idPeriode AND a.TMP_BATCH IS FALSE";

        Map<String, Object> params = new HashMap<>();
        params.put("idPeriode", idPeriode);
        List<Long> result = getJdbcTemplate().query(sql, params, new BaseAssujetieCountRowMapper());

        if (result.isEmpty() || result.get(0) == null) {
            return false;
        }
        return result.get(0) > 0;
    }

    /**
     * Mapper le résultat du count
     * 
     * @author eudesr
     *
     */
    public static class BaseAssujetieCountRowMapper implements RowMapper<Long> {

        @Override
        public Long mapRow(ResultSet rs, int rowNum) throws SQLException {
            return rs.getLong(1);
        }

    }

    /**
     * Mapper le résultat du plage rattachement
     * 
     * @author rupip
     *
     */
    public static class PlageRattachementRowMapper implements RowMapper<PlageRattachement> {

        @Override
        public PlageRattachement mapRow(ResultSet rs, int rowNum) throws SQLException {

            int dateDebut = JdbcRepository.getInteger(rs, "dtDebRattachement");
            int dateFin = JdbcRepository.getInteger(rs, "dtFinRattachement");

            return new PlageRattachement(dateDebut, dateFin);
        }

    }
}
