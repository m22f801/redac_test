package fr.si2m.red.internal.contrat;

import java.util.List;

import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import fr.si2m.red.contrat.Intermediaire;
import fr.si2m.red.contrat.IntermediaireRepository;
import fr.si2m.red.core.repository.jpa.JpaEntiteImportableRepository;

/**
 * Base de données des entités {@link Intermediaire}, connectée via JPA.
 * 
 * @author nortaina
 *
 */
@Repository
public class JpaIntermediaireRepository extends JpaEntiteImportableRepository<Intermediaire> implements IntermediaireRepository {

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public boolean existeUnNcprod(String valeur) {
        TypedQuery<Long> query = getEntityManager().createQuery("SELECT count(*) FROM Intermediaire WHERE NCPROD = :valeurChamp", Long.class);
        query.setParameter("valeurChamp", valeur);
        List<Long> result = query.getResultList();
        if (result.isEmpty() || result.get(0) == null) {
            return false;
        }
        return result.get(0) > 0;
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public Intermediaire getIntermediaire(String codeIntermediaire) {
        TypedQuery<Intermediaire> query = getEntityManager().createQuery("SELECT i FROM Intermediaire i WHERE  codeIntermediaire = :ncprod ",
                Intermediaire.class);
        query.setParameter("ncprod", codeIntermediaire);
        List<Intermediaire> result = query.getResultList();
        if (result.isEmpty() || result.get(0) == null) {
            return null;
        }
        return result.get(0);
    }

}
