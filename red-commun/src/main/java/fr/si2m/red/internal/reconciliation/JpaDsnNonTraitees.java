package fr.si2m.red.internal.reconciliation;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.*;

import fr.si2m.red.reconciliation.*;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Repository;

import fr.si2m.red.core.repository.jpa.JpaRepository;

import javax.persistence.Query;

/**
 * Base de données des entités {@link DsnNonTraitees}, connectée via JPA.
 */
@Repository
public class JpaDsnNonTraitees extends JpaRepository<AdhesionEtabMois> implements IDsnNonTraiteesRepository {
    private static final Logger LOGGER = LoggerFactory.getLogger(JpaDsnNonTraitees.class);

    @Override
    public Class<AdhesionEtabMois> getClassePrototypeEntite() {
        return AdhesionEtabMois.class;
    }

    /**
     * Retourne les DSN non traitée pour un affichage IHM
     * @param criteres Les critère de recherche
     * @return La liste des DSN non traitée correspondante
     */
    @Override
    public List<DsnNonTraitees> rechercheDsnNonTraiteesIHM(DsnNonTraiteesCriteresRecherche criteres) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        String sql = prepareRecherche(criteres, TypeSelectCriteresRecherche.IHM, params);
        LOGGER.debug("Recherche de DSN non traitées avec les critères suivants : {}", criteres);

        List<DsnNonTraitees> dsnNonTraitees = getJdbcTemplate().query(sql, params, new ExportDsnNonTraiteesExcelRowMapper());

        LOGGER.debug("Recherche de DSN non traitées terminées avec {} résultats", dsnNonTraitees.size());

        return dsnNonTraitees;
    }

    /**
     * Retourne les DSN non traitée pour un affichage Excel
     * @param criteres Les critère de recherche
     * @return La liste des DSN non traitée correspondante
     */
    @Override
    public List<DsnNonTraitees> rechercheDsnNonTraiteesExportExcel(DsnNonTraiteesCriteresRecherche criteres) {
        // Search
        MapSqlParameterSource params = new MapSqlParameterSource();
        String sql = prepareRecherche(criteres, TypeSelectCriteresRecherche.EXPORT_PERIODES, params);

        LOGGER.debug("Recherche de DSN non traitées avec les critères suivants : {}", criteres);

        List<DsnNonTraitees> dsnNonTraitees = getJdbcTemplate().query(sql, params, new ExportDsnNonTraiteesExcelRowMapper());

        LOGGER.debug("Recherche de DSN non traitées terminées avec {} résultats", dsnNonTraitees.size());

        return dsnNonTraitees;
    }

    /**
     * Retourne le nombre de lignes de la dernière requête sans prendre en compte la pagination
     * @return le nombre de lignes
     */
    @Override
    public long countResults() {
        return getJdbcTemplate().queryForObject("SELECT FOUND_ROWS() AS count;", new HashMap<String, Object>(), Long.class);
    }

    /**
     * Génère un identifiant unique pour permettre d'identifier un paramère dans une requête préparée
     * @return Un identifiant unique
     */
    private String genUUID() { return UUID.randomUUID().toString().replace("-","");}

    /**
     * Permet de préparer une recherche sur DSN non traitée
     * @param criteres Les critères de recherche pour cette requête
     * @param utilisation L'utilisation du résultat (IHM ou EXCEL)
     * @param params Un dictionnaire dans lequel seras placer les labels et les valeurs pour les paramètres nommer
     * @return La chaine de la requête ou null si utilisation n'est pas valide
     */
    private String prepareRecherche(DsnNonTraiteesCriteresRecherche criteres, TypeSelectCriteresRecherche utilisation, MapSqlParameterSource params) {
        StringBuilder query = new StringBuilder();

        String selectionFields;
        params.addValue("param_dsn_neant", DsnNonTraitees.DSN_NEANT, Types.VARCHAR);
        if (utilisation == TypeSelectCriteresRecherche.EXPORT_PERIODES) {
            selectionFields =   "pcl_gg.LIBELLE_EDITION AS GROUPE_GES, " +
                    "            pcl_fam.LIBELLE_EDITION AS FAMILLE, " +
                    "            adhesion_etablissement_mois.REFERENCE_CONTRAT AS REFERENCE_CONTRAT, " +
                    "            DATE_FORMAT(sq_dt_deb_sit.DT_MIN, '%d/%m/%Y') AS DT_SITUATION, " +
                    "            contrats.NCPROD AS CPT_NCPROD, " +
                    "            contrats.NCENC AS CPT_NCENC, " +
                    "            adhesion_etablissement_mois.SIREN_EMETTEUR AS SIREN_EMETTEUR, " +
                    "            adhesion_etablissement_mois.NIC_EMETEUR AS NIC_EMETTEUR, " +
                    "            adhesion_etablissement_mois.NOM_EMETTEUR AS NOM_EMETTEUR, " +
                    "            adhesion_etablissement_mois.SIREN_ENTREPRISE AS SIREN_ENTREPRISE, " +
                    "            adhesion_etablissement_mois.NIC_ENTREPRISE AS NIC_ENTREPRISE, " +
                    "            adhesion_etablissement_mois.RAISON_SOCIALE_ENTREPRISE AS RAISON_SOCIALE_ENTREPRISE, " +
                    "            adhesion_etablissement_mois.NIC_ETABLISSEMENT AS NIC_ETABLISSEMENT, " +
                    "            adhesion_etablissement_mois.ENSEIGNE_ETABLISSEMENT AS ENSEIGNE_ETABLISSEMENT, " +
                    "            DATE_FORMAT(adhesion_etablissement_mois.MOIS_DECLARE, '%m/%Y') AS MOIS_DECLARE, " +
                    "            DATE_FORMAT(adhesion_etablissement_mois.MOIS_RATTACHEMENT, '%m/%Y') AS MOIS_RATTACHEMENT, " +
                    "            DATE_FORMAT(adhesion_etablissement_mois.DATE_FICHIER, '%d/%m/%Y') AS DATE_FICHIER, " +
                    "            DATE_FORMAT(adhesion_etablissement_mois.DT_CREATION, '%d/%m/%Y') AS DT_CREATION, " +

                    "            (SELECT COUNT(DISTINCT COALESCE(IDENTIFIANT_REPERTOIRE, NTT)) " +
                    "               FROM individu " +
                    "              WHERE individu.ID_ADH_ETAB_MOIS = adhesion_etablissement_mois.ID_ADH_ETAB_MOIS) AS NB_INDIVIDU, " +

                    "            (SELECT COALESCE(SUM(base_assujettie.MONTANT_COTISATION), :param_dsn_neant) " +
                    "               FROM base_assujettie " +
                    "         INNER JOIN affiliation ON base_assujettie.ID_AFFILIATION = affiliation.ID_AFFILIATION " +
                    "         INNER JOIN contrat_travail ON affiliation.ID_CONTRAT_TRAVAIL = contrat_travail.ID_CONTRAT_TRAVAIL " +
                    "         INNER JOIN individu ON contrat_travail.ID_INDIVIDU = individu.ID_INDIVIDU " +
                    "              WHERE individu.ID_ADH_ETAB_MOIS = adhesion_etablissement_mois.ID_ADH_ETAB_MOIS) AS MT_COTISATION, " +
                    " " +
                    "            (SELECT SUM(composant_versement.MONTANT_VERSE) " +
                    "               FROM composant_versement " +
                    "         INNER JOIN versement ON composant_versement.ID_VERSEMENT = versement.ID_VERSEMENT " +
                    "              WHERE versement.ID_ADH_ETAB_MOIS = adhesion_etablissement_mois.ID_ADH_ETAB_MOIS) AS MT_VERSEMENT, " +

                    "            (SELECT CASE COUNT(DISTINCT versement.MODE_PAIEMENT) " +
                    "                        WHEN 1 THEN (SELECT param_code_libelle.LIBELLE_EDITION " +
                    "                                       FROM param_code_libelle " +
                    "                                      WHERE param_code_libelle.TBL = 'VERSEMENT' AND param_code_libelle.CHAMP = 'MODE_PAIEMENT' AND param_code_libelle.CODE = versement.MODE_PAIEMENT) " +
                    "                        WHEN 0 THEN '' " +
                    "                        ELSE 'MIXTE' " +
                    "                     END " +
                    "               FROM versement WHERE versement.ID_ADH_ETAB_MOIS = adhesion_etablissement_mois.ID_ADH_ETAB_MOIS) AS MODE_PAIEMENT, " +

                    "            pcl_etat.LIBELLE_EDITION AS ETAT, " +
                    "            null AS ID_ADH_ETAB_MOIS, " +
                    "            null AS NMGRPGES ";
        } else if (utilisation == TypeSelectCriteresRecherche.IHM) {
            selectionFields ="   adhesion_etablissement_mois.ID_ADH_ETAB_MOIS AS ID_ADH_ETAB_MOIS, " +
                    "            contrats.NMGRPGES AS NMGRPGES," +
                    "            pcl_fam.LIBELLE_COURT AS FAMILLE, " +
                    "            adhesion_etablissement_mois.REFERENCE_CONTRAT AS REFERENCE_CONTRAT, " +
                    "            DATE_FORMAT(adhesion_etablissement_mois.MOIS_RATTACHEMENT, '%m/%Y') AS MOIS_RATTACHEMENT, " +
                    "            adhesion_etablissement_mois.SIREN_ENTREPRISE AS SIREN_ENTREPRISE, " +
                    "            adhesion_etablissement_mois.NIC_ETABLISSEMENT AS NIC_ETABLISSEMENT, " +
                    "            adhesion_etablissement_mois.RAISON_SOCIALE_ENTREPRISE AS RAISON_SOCIALE_ENTREPRISE, " +
                    "            DATE_FORMAT(adhesion_etablissement_mois.DT_CREATION, '%d/%m/%Y') AS DT_CREATION, " +

                    "            (SELECT COALESCE(SUM(base_assujettie.MONTANT_COTISATION), :param_dsn_neant) " +
                    "               FROM base_assujettie " +
                    "         INNER JOIN affiliation ON base_assujettie.ID_AFFILIATION = affiliation.ID_AFFILIATION " +
                    "         INNER JOIN contrat_travail ON affiliation.ID_CONTRAT_TRAVAIL = contrat_travail.ID_CONTRAT_TRAVAIL " +
                    "         INNER JOIN individu ON contrat_travail.ID_INDIVIDU = individu.ID_INDIVIDU " +
                    "              WHERE individu.ID_ADH_ETAB_MOIS = adhesion_etablissement_mois.ID_ADH_ETAB_MOIS) AS MT_COTISATION, " +

                    "            pcl_etat.LIBELLE_COURT AS ETAT, " +

                    "            null AS GROUPE_GES, " +
                    "            null AS DT_SITUATION, " +
                    "            null AS CPT_NCPROD, " +
                    "            null AS CPT_NCENC, " +
                    "            null AS SIREN_EMETTEUR, " +
                    "            null AS NIC_EMETTEUR, " +
                    "            null AS NOM_EMETTEUR, " +
                    "            null AS NIC_ENTREPRISE, " +
                    "            null AS ENSEIGNE_ETABLISSEMENT, " +
                    "            null AS MOIS_DECLARE, " +
                    "            null AS DATE_FICHIER, " +
                    "            null AS NB_INDIVIDU, " +
                    "            null AS MT_VERSEMENT, " +
                    "            null AS MODE_PAIEMENT ";
        } else {
            return null;
        }

        query.append("SELECT SQL_CALC_FOUND_ROWS " + selectionFields +
                "FROM adhesion_etablissement_mois " +
                "LEFT JOIN rattachement_declarations_recues ON rattachement_declarations_recues.ID_ADH_ETAB_MOIS = adhesion_etablissement_mois.ID_ADH_ETAB_MOIS " +
                "LEFT JOIN contrats ON contrats.NOCO = adhesion_etablissement_mois.REFERENCE_CONTRAT AND contrats.DT_FIN_SIT IS NULL " +
                (utilisation == TypeSelectCriteresRecherche.EXPORT_PERIODES ? "LEFT JOIN param_code_libelle pcl_gg ON pcl_gg.CODE = contrats.NMGRPGES AND pcl_gg.TBL = 'CONTRATS' AND pcl_gg.CHAMP = 'NMGRPGES' " : "") +
                "LEFT JOIN param_code_libelle pcl_fam ON pcl_fam.CODE = contrats.NOFAM AND pcl_fam.TBL = 'CONTRATS' AND pcl_fam.CHAMP = 'NOFAM' " +
                (utilisation == TypeSelectCriteresRecherche.EXPORT_PERIODES ? "LEFT JOIN (SELECT contrats.NOCO, MIN(contrats.DT_DEBUT_SIT) AS DT_MIN FROM contrats GROUP BY contrats.NOCO) sq_dt_deb_sit ON sq_dt_deb_sit.NOCO = adhesion_etablissement_mois.REFERENCE_CONTRAT " : "") +
                "LEFT JOIN param_code_libelle pcl_etat ON pcl_etat.TBL = 'ADHESION_ETAB_MOIS' AND pcl_etat.CHAMP = 'ETAT' AND pcl_etat.CODE = COALESCE(adhesion_etablissement_mois.DETACHEMENT_TRAITE, 'VIDE') " +
                "    WHERE rattachement_declarations_recues.ID_ADH_ETAB_MOIS IS NULL ");



        String parameterName;

        // Critère "Etat"
        if (criteres.getStatutsAsList().size() > 0) {
            query.append(" AND (");
            boolean first = true;
            for (String etat:criteres.getStatutsAsList()) {
                if (!first) {
                    query.append(" OR ");
                }
                first = false;

                if (etat.equals("VIDE")) {
                    query.append("adhesion_etablissement_mois.DETACHEMENT_TRAITE is null ");
                } else {
                    parameterName = genUUID();
                    params.addValue(parameterName, etat, Types.VARCHAR);
                    query.append("adhesion_etablissement_mois.DETACHEMENT_TRAITE = :" + parameterName + " ");
                }
            }
            query.append(") ");
        }

        // Critère "Contrat"
        if (StringUtils.isNotBlank(criteres.getNumContrat())) {
            params.addValue("param_num_contrat", criteres.getNumContrat(), Types.VARCHAR);
            query.append(" AND adhesion_etablissement_mois.REFERENCE_CONTRAT LIKE CONCAT(:param_num_contrat , '%') ");
        }

        // Critère "mois rattachement"
        if (StringUtils.isNotBlank(criteres.getMoisRattachement())) {
            params.addValue("param_mois_rattachement", criteres.getDateMoisAnneeJour(criteres.getMoisRattachement()), Types.VARCHAR);
            query.append(" AND adhesion_etablissement_mois.MOIS_RATTACHEMENT = :param_mois_rattachement ");
        }

        // Critère "Versement"
        if (criteres.getVersementsAsList().size() == 1) {
            String subqueryVersement = "SELECT v.ID_VERSEMENT"
                                      + " FROM VERSEMENT v "
                                     + " WHERE adhesion_etablissement_mois.ID_ADH_ETAB_MOIS = v.ID_ADH_ETAB_MOIS ";

            // Les versements sont gérer comme des boolean depuis l'IHM, aucun = false, au moins 1 = true
            if (criteres.getVersementsAsList().get(0)) {
                query.append(" AND EXISTS (" + subqueryVersement + ") ");
            } else {
                query.append(" AND NOT EXISTS (" + subqueryVersement + ")");
            }
        }

        // Critère "Grpe Gestion"
        if (!criteres.getGroupesGestionAsList().isEmpty()) {
            query.append(" AND contrats.NMGRPGES IN (");
            Iterator<String> groupes = criteres.getGroupesGestionAsList().iterator();
            while (groupes.hasNext()) {
                String groupeGestion = groupes.next();
                parameterName = genUUID();
                params.addValue(parameterName, groupeGestion, Types.VARCHAR);
                query.append(" :" + parameterName + (groupes.hasNext() ? " , ":" "));
            }
            query.append(") ");
        }

        // Critère "Famille"
        if (!criteres.getFamillesAsList().isEmpty()) {
            query.append(" AND contrats.NOFAM IN (");
            Iterator<String> familles = criteres.getFamillesAsList().iterator();
            while (familles.hasNext()) {
                String famille = familles.next();
                parameterName = genUUID();
                params.addValue(parameterName, famille, Types.VARCHAR);
                query.append(" :" + parameterName + (familles.hasNext() ? " , ":" "));
            }
            query.append(") ");
        }

        // Critère "Cpt Enc"
        if (StringUtils.isNotBlank(criteres.getCptEnc())) {
            params.addValue("param_cpt_enc", criteres.getCptEnc(), Types.VARCHAR);
            query.append(" AND contrats.NCENC LIKE CONCAT(:param_cpt_enc , '%') ");
        }

        // Critère "Cpt prod"
        if (StringUtils.isNotBlank(criteres.getCptProd())) {
            params.addValue("param_ncprod", criteres.getCptProd(), Types.VARCHAR);
            query.append(" AND contrats.NCPROD LIKE CONCAT(:param_ncprod , '%') ");
        }

        // Critère "Siren entreprise"
        if (StringUtils.isNotBlank(criteres.getSirenEntreprise())) {
            params.addValue("param_siren_entreprise", criteres.getSirenEntreprise(), Types.VARCHAR);
            query.append(" AND adhesion_etablissement_mois.SIREN_ENTREPRISE LIKE CONCAT(:param_siren_entreprise , '%') ");
        }

        // Critère "nic"
        if (StringUtils.isNotBlank(criteres.getNicEtablissement())) {
            params.addValue("param_nic_etablissement", criteres.getNicEtablissement(), Types.VARCHAR);
            query.append(" AND adhesion_etablissement_mois.NIC_ETABLISSEMENT LIKE CONCAT(:param_nic_etablissement , '%') ");
        }

        // Critère "raison social"
        if (StringUtils.isNotBlank(criteres.getRaisonSociale())) {
            params.addValue("param_raison_sociale_entreprise", criteres.getRaisonSociale(), Types.VARCHAR);
            query.append(" AND adhesion_etablissement_mois.RAISON_SOCIALE_ENTREPRISE LIKE CONCAT('%', :param_raison_sociale_entreprise , '%') ");
        }



        String champ;
        switch (criteres.getTriChamp()) {
            case "groupeGestion":
                champ = "NMGRPGES";
                break;
            case "libelleFamille":
                champ = "FAMILLE";
                break;
            case "refContrat":
                champ = "REFERENCE_CONTRAT";
                break;
            case "moisRattachement":
                champ = "adhesion_etablissement_mois.MOIS_RATTACHEMENT";
                break;
            case "sirenEntreprise":
                champ = "SIREN_ENTREPRISE";
                break;
            case "nicEtablissement":
                champ = "NIC_ETABLISSEMENT";
                break;
            case "raisonSociale":
                champ = "RAISON_SOCIALE_ENTREPRISE";
                break;
            case "dateCreation":
                champ = "adhesion_etablissement_mois.DT_CREATION";
                break;
            case "mtCotisation":
                champ = "CASE MT_COTISATION " +
                        "WHEN :param_dsn_neant THEN -9999999999999 " +
                        "ELSE MT_COTISATION + 0 " +
                        "END ";
                break;
            case "libelleEtat":
                champ = "ETAT";
                break;
            default:
                champ = null;
        }
        if (champ == null) {
            query.append(" ORDER BY COALESCE(adhesion_etablissement_mois.DETACHEMENT_TRAITE, -1) ASC, adhesion_etablissement_mois.DT_CREATION DESC, adhesion_etablissement_mois.REFERENCE_CONTRAT ASC, adhesion_etablissement_mois.MOIS_RATTACHEMENT DESC ");
        } else {
            query.append(" ORDER BY " + champ + (criteres.isTriAsc() ? " ASC":" DESC") + ", COALESCE(adhesion_etablissement_mois.DETACHEMENT_TRAITE, -1) ASC, adhesion_etablissement_mois.DT_CREATION DESC, adhesion_etablissement_mois.REFERENCE_CONTRAT ASC, adhesion_etablissement_mois.MOIS_RATTACHEMENT DESC ");
        }

        //Pagination pour l'IHM
        if (TypeSelectCriteresRecherche.IHM.equals(utilisation) && criteres.getPageTaille() >= 0) {
            params.addValue("param_limit", criteres.getPageTaille(), Types.INTEGER);
            query.append(" LIMIT :param_limit ");
            if (criteres.getPageNumero() >= 1) {
                params.addValue("param_offset", (criteres.getPageNumero() -1) * criteres.getPageTaille(), Types.INTEGER);
                query.append(" OFFSET :param_offset");
            }
        }

        return query.toString();
    }

    /**
     * Permet de classer sans suite de gestion les DSN non traitée
     * @param idsAdhesions La liste des identifiants adhesion_etablissement_mois concerner
     * @param userIHM L'utilisateur qui opère la mise à jour
     */
    @Override
    public void setAdhesionSSG(List<String> idsAdhesions, String userIHM) {
        String jpql = "UPDATE AdhesionEtabMois adh SET adh.userMiseAJour = :user , adh.detachementTraite = 'SSG' WHERE adh.idAdhEtabMois IN ( :idsAdhesions ) ";
        Query query = getEntityManager().createQuery(jpql);
        query.setParameter("idsAdhesions", idsAdhesions);
        query.setParameter("user", userIHM);
        query.executeUpdate();
    }

    /**
     * Retourne le nombre d'adhesion vide parmi la liste d'id donnée
     * @param idsAdhesions La liste d'id adhesion à vérifier
     * @return Le nombre d'adhesion vide
     */
    @Override
    public long getAdhesionVide(List<String> idsAdhesions) {
        Iterator<String> ids = idsAdhesions.iterator();
        MapSqlParameterSource params = new MapSqlParameterSource();
        StringBuilder in = new StringBuilder();
        while (ids.hasNext()) {
            String paramName = genUUID();
            String id = ids.next();
            in.append(" :").append(paramName);
            params.addValue(paramName, id, Types.VARCHAR);
            if (ids.hasNext()) {
                in.append(" , ");
            }
        }
        String sql = "SELECT COUNT(DISTINCT adh.ID_ADH_ETAB_MOIS) AS COUNT_ADHESION " +
                "       FROM ADHESION_ETABLISSEMENT_MOIS adh " +
                "      WHERE adh.ID_ADH_ETAB_MOIS IN (" + in + ") " +
                "            AND adh.DETACHEMENT_TRAITE is not null";


        return getJdbcTemplate().queryForObject(sql, params, Long.class);
    }

    /**
     * Mapper de lignes de DSN non traitées.
     */
    public static class ExportDsnNonTraiteesExcelRowMapper implements RowMapper<DsnNonTraitees> {
        @Override
        public DsnNonTraitees mapRow(ResultSet rs, int rowNum) throws SQLException {
            DsnNonTraitees dsnNonTraitees = new DsnNonTraitees();
            dsnNonTraitees.setGroupeGestion(rs.getString("GROUPE_GES"));
            dsnNonTraitees.setFamille(rs.getString("FAMILLE"));
            dsnNonTraitees.setReferenceContrat(rs.getString("REFERENCE_CONTRAT"));
            dsnNonTraitees.setDateSituation(rs.getString("DT_SITUATION"));
            dsnNonTraitees.setNcprod(rs.getString("CPT_NCPROD"));
            dsnNonTraitees.setNcenc(rs.getString("CPT_NCENC"));
            dsnNonTraitees.setSirenEmetteur(rs.getString("SIREN_EMETTEUR"));
            dsnNonTraitees.setNicEmetteur(rs.getString("NIC_EMETTEUR"));
            dsnNonTraitees.setNomEmetteur(rs.getString("NOM_EMETTEUR"));
            dsnNonTraitees.setSirenEntreprise(rs.getString("SIREN_ENTREPRISE"));
            dsnNonTraitees.setNicEntreprise(rs.getString("NIC_ENTREPRISE"));
            dsnNonTraitees.setRaisonSocialeEntreprise(rs.getString("RAISON_SOCIALE_ENTREPRISE"));
            dsnNonTraitees.setNicEtablissement(rs.getString("NIC_ETABLISSEMENT"));
            dsnNonTraitees.setEnseigneEtablissement(rs.getString("ENSEIGNE_ETABLISSEMENT"));
            dsnNonTraitees.setMoisDeclaration(rs.getString("MOIS_DECLARE"));
            dsnNonTraitees.setMoisRattachement(rs.getString("MOIS_RATTACHEMENT"));
            dsnNonTraitees.setDateFichier(rs.getString("DATE_FICHIER"));
            dsnNonTraitees.setDateCreation(rs.getString("DT_CREATION"));
            dsnNonTraitees.setNombreIndividu(rs.getInt("NB_INDIVIDU") == 0 ? DsnNonTraitees.DSN_NEANT : rs.getString("NB_INDIVIDU"));
            dsnNonTraitees.setMontantCotisation(rs.getString("MT_COTISATION"));
            double montantVersement = rs.getDouble("MT_VERSEMENT");
            dsnNonTraitees.setMontantVersement(rs.wasNull() ? null:montantVersement);
            dsnNonTraitees.setModePaiement(rs.getString("MODE_PAIEMENT"));
            dsnNonTraitees.setEtat(rs.getString("ETAT"));
            dsnNonTraitees.setIdAdhesionEtablissementMois(rs.getString("ID_ADH_ETAB_MOIS"));
            dsnNonTraitees.setNmgrpges(rs.getString("NMGRPGES"));

            return dsnNonTraitees;
        }
    }
}