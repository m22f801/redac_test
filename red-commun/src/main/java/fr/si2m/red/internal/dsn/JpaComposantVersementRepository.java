package fr.si2m.red.internal.dsn;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.TypedQuery;

import org.apache.commons.collections.keyvalue.DefaultMapEntry;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import fr.si2m.red.core.repository.jpa.JpaEntiteImportableRepository;
import fr.si2m.red.dsn.ComposantVersement;
import fr.si2m.red.dsn.ComposantVersementRepository;

/**
 * Base de données des entités {@link ComposantVersement}, connectée via JPA.
 * 
 * @author poidij
 *
 */
@Repository
public class JpaComposantVersementRepository extends JpaEntiteImportableRepository<ComposantVersement> implements ComposantVersementRepository {

    /**
     * Le nom de paramètre utilisé pour l'identifiant de composant de versement.
     */
    private static final String PARAM_ID_VERSEMENT = "idVersement";

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public ComposantVersement getComposantVersementPourNumCategorie(String idVersement, String numCategorieQuittancement) {
        String jpql = "SELECT cv FROM ComposantVersement cv WHERE cv.idVersement = :idVersement AND cv.typePopulation = :numCategorieQuittancement ORDER BY cv.poidsPeriodeAffectation DESC";
        TypedQuery<ComposantVersement> query = getEntityManager().createQuery(jpql, ComposantVersement.class).setMaxResults(1);
        query.setParameter(PARAM_ID_VERSEMENT, idVersement);
        query.setParameter("numCategorieQuittancement", numCategorieQuittancement);
        List<ComposantVersement> result = query.getResultList();
        return getPremierResultatSiExiste(result);
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public List<String> getTypesPopulationDistincts(String idVersement) {
        String jpql = "SELECT DISTINCT cv.typePopulation FROM ComposantVersement cv WHERE cv.idVersement = :idVersement ORDER BY cv.typePopulation";
        TypedQuery<String> query = getEntityManager().createQuery(jpql, String.class);
        query.setParameter(PARAM_ID_VERSEMENT, idVersement);
        return query.getResultList();
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public List<String> getPeriodesAffectationDistinctes(String idVersement) {
        String jpql = "SELECT DISTINCT cv.periodeAffectation FROM ComposantVersement cv WHERE cv.idVersement = :idVersement ORDER BY cv.poidsPeriodeAffectation";
        TypedQuery<String> query = getEntityManager().createQuery(jpql, String.class);
        query.setParameter(PARAM_ID_VERSEMENT, idVersement);
        return query.getResultList();
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public Map<String, String> getIdentifiantsSousFondsPourPeriodeAffectation(String idVersement, String periodeAffectation) {
        String jpql = "SELECT DISTINCT new org.apache.commons.collections.keyvalue.DefaultMapEntry(cv.typePopulation, cv.codeIdentifiantSousFonds) FROM ComposantVersement cv "
                + " WHERE cv.idVersement = :idVersement AND cv.periodeAffectation = :periodeAffectation GROUP BY cv.typePopulation ORDER BY cv.typePopulation";
        TypedQuery<DefaultMapEntry> query = getEntityManager().createQuery(jpql, DefaultMapEntry.class);
        query.setParameter(PARAM_ID_VERSEMENT, idVersement);
        query.setParameter("periodeAffectation", periodeAffectation);
        List<DefaultMapEntry> couples = query.getResultList();
        Map<String, String> resultats = new HashMap<>();
        for (DefaultMapEntry couple : couples) {
            resultats.put((String) couple.getKey(), (String) couple.getValue());
        }
        return resultats;
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public Map<String, Double> getSommesMontantsVersesPourPeriodeAffectation(String idVersement, String periodeAffectation) {
        String jpql = "SELECT new org.apache.commons.collections.keyvalue.DefaultMapEntry(cv.typePopulation, SUM(cv.montantVerse)) FROM ComposantVersement cv "
                + " WHERE cv.idVersement = :idVersement AND cv.periodeAffectation = :periodeAffectation GROUP BY cv.typePopulation ORDER BY cv.typePopulation";
        TypedQuery<DefaultMapEntry> query = getEntityManager().createQuery(jpql, DefaultMapEntry.class);
        query.setParameter(PARAM_ID_VERSEMENT, idVersement);
        query.setParameter("periodeAffectation", periodeAffectation);
        List<DefaultMapEntry> couples = query.getResultList();
        Map<String, Double> resultats = new HashMap<>();
        for (DefaultMapEntry couple : couples) {
            resultats.put((String) couple.getKey(), (Double) couple.getValue());
        }
        return resultats;
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public boolean existeUnComposantVersement(String idComposantVersement) {
        TypedQuery<Long> query = getEntityManager().createQuery("SELECT count(*) FROM ComposantVersement c WHERE c.idComposantVersement = :idComposantVersement", Long.class);
        query.setParameter("idComposantVersement", idComposantVersement);
        List<Long> result = query.getResultList();
        if (result.isEmpty() || result.get(0) == null) {
            return false;
        }
        return result.get(0) > 0;
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public Integer getDateDebutPeriodeRattachement(String idVersement) {
        String jpql = "SELECT MIN(cv.dateDebutPeriodeAffectation) FROM ComposantVersement cv WHERE cv.idVersement = :idVersement";
        TypedQuery<Integer> query = getEntityManager().createQuery(jpql, Integer.class);
        query.setParameter(PARAM_ID_VERSEMENT, idVersement);
        return getPremierResultatSiExiste(query.getResultList());
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public Integer getDateFinPeriodeRattachement(String idVersement) {
        String jpql = "SELECT MAX(cv.dateFinPeriodeAffectation) FROM ComposantVersement cv WHERE cv.idVersement = :idVersement";
        TypedQuery<Integer> query = getEntityManager().createQuery(jpql, Integer.class);
        query.setParameter(PARAM_ID_VERSEMENT, idVersement);
        return getPremierResultatSiExiste(query.getResultList());
    }

}
