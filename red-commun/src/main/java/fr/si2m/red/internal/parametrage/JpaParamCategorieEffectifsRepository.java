package fr.si2m.red.internal.parametrage;

import java.util.List;

import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import fr.si2m.red.core.repository.jpa.JpaEntiteImportableRepository;
import fr.si2m.red.parametrage.ParamCategorieEffectifs;
import fr.si2m.red.parametrage.ParamCategorieEffectifsRepository;

/**
 * Base de données des entités {@link ParamCategorieEffectifs}, connectée via JPA.
 * 
 * @author nortaina
 *
 */
@Repository
public class JpaParamCategorieEffectifsRepository extends JpaEntiteImportableRepository<ParamCategorieEffectifs> implements
        ParamCategorieEffectifsRepository {

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public boolean existeUnParamCategorieEffectifs(String numCategorie, String libelleCategorie) {
        TypedQuery<Long> query = getEntityManager().createQuery(
                "SELECT count(*) FROM ParamCategorieEffectifs WHERE NOCAT = :numCategorie and LICAT = :libelleCategorie", Long.class);
        query.setParameter("numCategorie", numCategorie);
        query.setParameter("libelleCategorie", libelleCategorie);
        List<Long> result = query.getResultList();
        if (result.isEmpty() || result.get(0) == null) {
            return false;
        }
        return result.get(0) > 0;
    }

}
