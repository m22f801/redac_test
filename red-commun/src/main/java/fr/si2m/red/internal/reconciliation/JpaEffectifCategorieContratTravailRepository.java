package fr.si2m.red.internal.reconciliation;

import java.util.List;

import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import fr.si2m.red.reconciliation.EffectifCategorieContratTravail;
import fr.si2m.red.reconciliation.EffectifCategorieContratTravailRepository;

/**
 * Base de données des entités {@link EffectifCategorieContratTravail}, connectée via JPA.
 * 
 * @author poidij
 *
 */
@Repository
public class JpaEffectifCategorieContratTravailRepository extends JpaElementDeclaratifRepository<EffectifCategorieContratTravail> implements
        EffectifCategorieContratTravailRepository {
    private static final String PARAM_ID_PERIODE = "idPeriode";
    private static final String PARAM_NUM_CATEGORIE_QUITTANCEMENT = "numCategorieQuittancement";

    private static final String JPA_SELECT_CT_POUR_ID_PERIODE_ET_CATEGORIE = "SELECT ct FROM EffectifCategorieContratTravail ct WHERE ct.idPeriode = :"
            + PARAM_ID_PERIODE + " AND ct.numCategorieQuittancement = :" + PARAM_NUM_CATEGORIE_QUITTANCEMENT;

    @Override
    public Class<EffectifCategorieContratTravail> getClassePrototypeEntite() {
        return EffectifCategorieContratTravail.class;
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public EffectifCategorieContratTravail get(Long idPeriode, Integer moisRattachement, String idContratTravail, String numCategorieQuittancement,
            Integer dateDebutBase) {
        String jpql = JPA_SELECT_CT_POUR_ID_PERIODE_ET_CATEGORIE
                + " AND ct.moisRattachement = :moisRattachement AND ct.idContratTravail = :idContratTravail AND ct.dateDebutBase = :dateDebutBase";
        TypedQuery<EffectifCategorieContratTravail> query = getEntityManager().createQuery(jpql, EffectifCategorieContratTravail.class)
                .setMaxResults(1);
        query.setParameter(PARAM_ID_PERIODE, idPeriode);
        query.setParameter(PARAM_NUM_CATEGORIE_QUITTANCEMENT, numCategorieQuittancement);
        query.setParameter("moisRattachement", moisRattachement);
        query.setParameter("idContratTravail", idContratTravail);
        query.setParameter("dateDebutBase", dateDebutBase);
        List<EffectifCategorieContratTravail> resultats = query.getResultList();
        return getPremierResultatSiExiste(resultats);
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public List<EffectifCategorieContratTravail> getPourPeriodeEtCategorieQuittancement(Long idPeriode, String numCategorieQuittancement) {
        TypedQuery<EffectifCategorieContratTravail> query = getEntityManager().createQuery(JPA_SELECT_CT_POUR_ID_PERIODE_ET_CATEGORIE,
                EffectifCategorieContratTravail.class);
        query.setParameter(PARAM_ID_PERIODE, idPeriode);
        query.setParameter(PARAM_NUM_CATEGORIE_QUITTANCEMENT, numCategorieQuittancement);
        return query.getResultList();
    }

    @Override
    public List<EffectifCategorieContratTravail> getPourPeriodeEtCategorieQuittancementAvecTotalEffectifNonNul(Long idPeriode,
            String numCategorieQuittancement) {
        String jpql = JPA_SELECT_CT_POUR_ID_PERIODE_ET_CATEGORIE + " AND ct.totalEffectif <> 0";
        TypedQuery<EffectifCategorieContratTravail> query = getEntityManager().createQuery(jpql, EffectifCategorieContratTravail.class);
        query.setParameter(PARAM_ID_PERIODE, idPeriode);
        query.setParameter(PARAM_NUM_CATEGORIE_QUITTANCEMENT, numCategorieQuittancement);
        return query.getResultList();
    }
}
