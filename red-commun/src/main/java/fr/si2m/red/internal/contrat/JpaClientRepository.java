package fr.si2m.red.internal.contrat;

import java.util.List;

import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import fr.si2m.red.contrat.Client;
import fr.si2m.red.contrat.ClientId;
import fr.si2m.red.contrat.ClientRepository;
import fr.si2m.red.core.repository.jpa.JpaEntiteImportableRepository;

/**
 * Base de données des entités {@link Client}, connectée via JPA.
 * 
 * @author nortaina
 *
 */
@Repository
public class JpaClientRepository extends JpaEntiteImportableRepository<Client> implements ClientRepository {

    private static final String NUM_CLIENT = "numClient";

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public boolean existeUnClientTemporaire(Long numClient) {
        TypedQuery<Long> query = getEntityManager().createQuery("SELECT count(*) FROM Client WHERE NOCLI = :numClient AND TMP_BATCH IS TRUE",
                Long.class);
        query.setParameter(NUM_CLIENT, numClient);
        List<Long> result = query.getResultList();
        if (result.isEmpty() || result.get(0) == null) {
            return false;
        }
        return result.get(0) > 0;
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public Client getClientDerniereModification(String numSiren) {
        TypedQuery<Client> query = getEntityManager().createQuery(
                "SELECT c FROM Client c WHERE NOSIREN = :numSiren ORDER BY dateDerniereModification DESC", Client.class).setMaxResults(1);
        query.setParameter("numSiren", numSiren);
        List<Client> result = query.getResultList();
        if (result.isEmpty()) {
            return null;
        }
        return result.get(0);
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public Client getEtablissementDerniereModification(String numSiren, String nic) {
        TypedQuery<Client> query = getEntityManager().createQuery(
                "SELECT c FROM Client c WHERE NOSIREN = :numSiren AND NOSIRET = :nic ORDER BY dateDerniereModification DESC", Client.class)
                .setMaxResults(1);
        query.setParameter("numSiren", numSiren);
        query.setParameter("nic", nic);
        List<Client> result = query.getResultList();
        if (result.isEmpty()) {
            return null;
        }
        return result.get(0);
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public String getSiren(Long numClient) {
        ClientId id = new ClientId(false, numClient);
        Client client = getEntityManager().find(Client.class, id);
        String numSiren = null;
        if (client != null) {
            numSiren = client.getNumSiren();
            getEntityManager().detach(client);
        }
        return numSiren;

    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public String getSiret(Long numClient) {
        ClientId id = new ClientId(false, numClient);
        Client client = getEntityManager().find(Client.class, id);
        String numSiretComplet = null;
        if (client != null) {
            numSiretComplet = client.getNumSiren() + client.getNumSiret();
            getEntityManager().detach(client);
        }
        return numSiretComplet;

    }

}
