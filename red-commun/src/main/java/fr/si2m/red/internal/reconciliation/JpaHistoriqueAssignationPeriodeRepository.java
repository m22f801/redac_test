package fr.si2m.red.internal.reconciliation;

import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import fr.si2m.red.DateRedac;
import fr.si2m.red.core.repository.jpa.JpaRepository;
import fr.si2m.red.reconciliation.HistoriqueAssignationPeriode;
import fr.si2m.red.reconciliation.HistoriqueAssignationPeriodeRepository;

/**
 * Base de données des entités {@link HistoriqueAssignationPeriode}, connectée via JPA.
 * 
 * @author gahagnont
 *
 */
@Repository
public class JpaHistoriqueAssignationPeriodeRepository extends JpaRepository<HistoriqueAssignationPeriode>
        implements HistoriqueAssignationPeriodeRepository {

    private static final String PARAM_ID_PERIODE = "idPeriode";
    private static final String PARAM_LISTE_ID_PERIODES = "idsPeriodes";

    @Override
    public Class<HistoriqueAssignationPeriode> getClassePrototypeEntite() {
        return HistoriqueAssignationPeriode.class;
    }

    @Override
    @Transactional(propagation = Propagation.MANDATORY, rollbackFor = Throwable.class)
    public List<HistoriqueAssignationPeriode> getHistoriqueAssignationsPeriode(Long idPeriode) {
        String jpql = "FROM HistoriqueAssignationPeriode h WHERE h.idPeriode=:idPeriode ORDER BY h.dateHeureAssignation DESC";
        TypedQuery<HistoriqueAssignationPeriode> query = getEntityManager().createQuery(jpql, HistoriqueAssignationPeriode.class);
        query.setParameter(PARAM_ID_PERIODE, idPeriode);
        return query.getResultList();
    }

    @Override
    @Transactional(propagation = Propagation.MANDATORY, rollbackFor = Throwable.class)
    public HistoriqueAssignationPeriode getHistoriqueAssignationPeriodePourMessage(Long idPeriode, Long dateHeureAssignation) {
        String jpql = "FROM HistoriqueAssignationPeriode h WHERE h.idPeriode=:idPeriode AND h.dateHeureAssignation = :dateHeureAssignation";
        TypedQuery<HistoriqueAssignationPeriode> query = getEntityManager().createQuery(jpql, HistoriqueAssignationPeriode.class).setMaxResults(1);
        query.setParameter(PARAM_ID_PERIODE, idPeriode);
        query.setParameter("dateHeureAssignation", dateHeureAssignation);
        return getPremierResultatSiExiste(query.getResultList());
    }

    @Override
    @Transactional(propagation = Propagation.MANDATORY, rollbackFor = Throwable.class)
    public HistoriqueAssignationPeriode getDerniereAssignationPourPeriode(Long idPeriode) {
        String jpql = "FROM HistoriqueAssignationPeriode h WHERE h.idPeriode=:idPeriode ORDER BY h.dateHeureAssignation DESC";
        TypedQuery<HistoriqueAssignationPeriode> query = getEntityManager().createQuery(jpql, HistoriqueAssignationPeriode.class).setMaxResults(1);
        query.setParameter(PARAM_ID_PERIODE, idPeriode);
        return getPremierResultatSiExiste(query.getResultList());
    }

    @Override
    public int supprimeHistoriqueAssignationPourPeriode(Long idPeriode) {
        String jpql = "DELETE FROM HistoriqueAssignationPeriode h WHERE h.idPeriode = :idPeriode";
        Query query = getEntityManager().createQuery(jpql);
        query.setParameter(PARAM_ID_PERIODE, idPeriode);
        return query.executeUpdate();

    }

    @Override
    @Transactional(propagation = Propagation.MANDATORY)
    public void callMysqlProcedure(String sql, Map<String, Object> params) {
        getJdbcTemplate().update(sql, params);
    }

    @Override
    public void creationEnMasseOrigineIHM(List<Long> idsPeriodes, String aTraiterPar, String utilisateur) {
        String sql = "call Creation_HistoriqueAssignationPeriode_en_masse( :" + PARAM_LISTE_ID_PERIODES + ","
                + ":datehms , :aTraiterPar , :userCreation )";
        Map<String, Object> params = new HashMap<>();
        params.put(PARAM_LISTE_ID_PERIODES, StringUtils.join(idsPeriodes, ","));
        params.put("datehms", DateRedac.convertitEnDateHeureRedac(Calendar.getInstance().getTime()));
        params.put("aTraiterPar", aTraiterPar);
        params.put("userCreation", utilisateur);

        callMysqlProcedure(sql, params);

    }

}
