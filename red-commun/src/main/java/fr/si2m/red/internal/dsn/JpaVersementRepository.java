package fr.si2m.red.internal.dsn;

import java.util.List;

import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import fr.si2m.red.core.repository.jpa.JpaEntiteImportableRepository;
import fr.si2m.red.dsn.Versement;
import fr.si2m.red.dsn.VersementRepository;

/**
 * Base de données des entités {@link Versement}, connectée via JPA.
 * 
 * @author poidij
 *
 */
@Repository
public class JpaVersementRepository extends JpaEntiteImportableRepository<Versement> implements VersementRepository {

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public boolean existeUnVersement(String idVersement) {
        TypedQuery<Long> query = getEntityManager().createQuery("SELECT count(*) FROM Versement v WHERE v.idVersement = :idVersement", Long.class);
        query.setParameter("idVersement", idVersement);
        List<Long> result = query.getResultList();
        if (result.isEmpty() || result.get(0) == null) {
            return false;
        }
        return result.get(0) > 0;
    }

}
