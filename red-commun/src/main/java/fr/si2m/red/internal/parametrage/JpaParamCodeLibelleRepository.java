package fr.si2m.red.internal.parametrage;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.TypedQuery;

import org.apache.commons.collections.keyvalue.DefaultMapEntry;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import fr.si2m.red.core.repository.jpa.JpaEntiteImportableRepository;
import fr.si2m.red.parametrage.ParamCodeLibelle;
import fr.si2m.red.parametrage.ParamCodeLibelleRepository;

/**
 * Base de données des entités ParamCodeLibelle, connectée via JPA.
 * 
 * @author nortaina
 *
 */
@Repository
public class JpaParamCodeLibelleRepository extends JpaEntiteImportableRepository<ParamCodeLibelle> implements ParamCodeLibelleRepository {

    private static final String PARAM_TABLE = "table";
    private static final String PARAM_CHAMP = "champ";
    private static final String PARAM_CODE = "code";

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public String getLibelleCourt(String table, String champ, String code) {
        String jpql = "SELECT libelleCourt FROM ParamCodeLibelle WHERE table = :table AND champ = :champ AND code = :code AND ligneEnCoursImportBatch IS FALSE";
        TypedQuery<String> query = getEntityManager().createQuery(jpql, String.class);
        query.setParameter(PARAM_TABLE, table);
        query.setParameter(PARAM_CHAMP, champ);
        query.setParameter(PARAM_CODE, code);
        return getPremierResultatSiExiste(query.getResultList());
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    // @Cacheable(value = "cacheLibellesEdition", key = "{ #table, #champ, #code }")
    public String getLibelleEdition(String table, String champ, String code) {
        String jpql = "SELECT libelleEdition FROM ParamCodeLibelle WHERE table = :table AND champ = :champ AND code = :code AND ligneEnCoursImportBatch IS FALSE";
        TypedQuery<String> query = getEntityManager().createQuery(jpql, String.class);
        query.setParameter(PARAM_TABLE, table);
        query.setParameter(PARAM_CHAMP, champ);
        query.setParameter(PARAM_CODE, code);
        return getPremierResultatSiExiste(query.getResultList());
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public List<ParamCodeLibelle> get(String table, String champ) {
        String jpql = "SELECT p FROM ParamCodeLibelle p WHERE p.table = :table AND p.champ = :champ AND ligneEnCoursImportBatch IS FALSE ORDER BY p.ordre ASC";
        TypedQuery<ParamCodeLibelle> query = getEntityManager().createQuery(jpql, ParamCodeLibelle.class);
        query.setParameter(PARAM_TABLE, table);
        query.setParameter(PARAM_CHAMP, champ);
        return query.getResultList();
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public Map<String, String> getMap(String table, String champ) {
        String jpql = "SELECT new org.apache.commons.collections.keyvalue.DefaultMapEntry(p.code,p.libelleEdition) FROM ParamCodeLibelle p WHERE p.table = :table AND p.champ = :champ AND ligneEnCoursImportBatch IS FALSE ORDER BY p.ordre ASC";

        TypedQuery<DefaultMapEntry> query = getEntityManager().createQuery(jpql, DefaultMapEntry.class);
        query.setParameter(PARAM_TABLE, table);
        query.setParameter(PARAM_CHAMP, champ);

        List<DefaultMapEntry> couples = query.getResultList();
        Map<String, String> resultats = new HashMap<>();
        for (DefaultMapEntry couple : couples) {
            resultats.put((String) couple.getKey(), (String) couple.getValue());
        }
        return resultats;
    }

}
