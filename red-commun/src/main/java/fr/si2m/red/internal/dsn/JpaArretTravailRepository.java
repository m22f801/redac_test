package fr.si2m.red.internal.dsn;

import java.util.List;

import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import fr.si2m.red.core.repository.jpa.JpaEntiteImportableRepository;
import fr.si2m.red.dsn.ArretTravail;
import fr.si2m.red.dsn.ArretTravailRepository;

/**
 * Base de données des entités {@link ArretTravail}, connectée via JPA.
 * 
 * @author poidij
 *
 */
@Repository
public class JpaArretTravailRepository extends JpaEntiteImportableRepository<ArretTravail> implements ArretTravailRepository {

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public ArretTravail getDernierArretTravailPourContratTravail(String idContratTravail) {
        String jpql = "SELECT at FROM ArretTravail at WHERE at.idContratTravail = :idContratTravail ORDER BY at.dateDernierJour DESC";
        TypedQuery<ArretTravail> query = getEntityManager().createQuery(jpql, ArretTravail.class).setMaxResults(1);
        query.setParameter("idContratTravail", idContratTravail);
        List<ArretTravail> resultats = query.getResultList();
        return getPremierResultatSiExiste(resultats);
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public boolean existeUnArretTravail(String idArretTravail) {
        TypedQuery<Long> query = getEntityManager().createQuery("SELECT count(*) FROM ArretTravail a WHERE a.idArretTravail = :idArretTravail", Long.class);
        query.setParameter("idArretTravail", idArretTravail);
        List<Long> result = query.getResultList();
        if (result.isEmpty() || result.get(0) == null) {
            return false;
        }
        return result.get(0) > 0;
    }

}
