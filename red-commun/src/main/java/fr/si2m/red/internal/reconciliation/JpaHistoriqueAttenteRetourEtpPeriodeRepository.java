package fr.si2m.red.internal.reconciliation;

import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import fr.si2m.red.DateRedac;
import fr.si2m.red.core.repository.jpa.JpaRepository;
import fr.si2m.red.reconciliation.HistoriqueAttenteRetourEtpPeriode;
import fr.si2m.red.reconciliation.HistoriqueAttenteRetourEtpPeriodeRepository;
import fr.si2m.red.reconciliation.TypeActionPopupChangementEnMasse;

/**
 * Base de données des entités {@link HistoriqueAttenteRetourEtpPeriode}, connectée via JPA.
 * 
 * @author gahagnont
 *
 */
@Repository
public class JpaHistoriqueAttenteRetourEtpPeriodeRepository extends JpaRepository<HistoriqueAttenteRetourEtpPeriode>
        implements HistoriqueAttenteRetourEtpPeriodeRepository {

    private static final String PARAM_ID_PERIODE = "idPeriode";
    private static final String PARAM_LISTE_ID_PERIODES = "idsPeriodes";

    @Override
    public Class<HistoriqueAttenteRetourEtpPeriode> getClassePrototypeEntite() {
        return HistoriqueAttenteRetourEtpPeriode.class;
    }

    @Override
    @Transactional(propagation = Propagation.MANDATORY, rollbackFor = Throwable.class)
    public List<HistoriqueAttenteRetourEtpPeriode> getHistoriqueAttenteRetourEtpPeriode(Long idPeriode) {
        String jpql = "FROM HistoriqueAttenteRetourEtpPeriode h WHERE h.idPeriode=:idPeriode ORDER BY h.dateHeureRetour DESC";
        TypedQuery<HistoriqueAttenteRetourEtpPeriode> query = getEntityManager().createQuery(jpql, HistoriqueAttenteRetourEtpPeriode.class);
        query.setParameter(PARAM_ID_PERIODE, idPeriode);
        return query.getResultList();
    }

    @Override
    @Transactional(propagation = Propagation.MANDATORY, rollbackFor = Throwable.class)
    public HistoriqueAttenteRetourEtpPeriode getHistoriqueAttenteRetourEtpPeriodePourMessage(Long idPeriode, Long dateHeureRetour) {
        String jpql = "FROM HistoriqueAttenteRetourEtpPeriode h WHERE h.idPeriode=:idPeriode AND h.dateHeureRetour = :dateHeureRetour";
        TypedQuery<HistoriqueAttenteRetourEtpPeriode> query = getEntityManager().createQuery(jpql, HistoriqueAttenteRetourEtpPeriode.class)
                .setMaxResults(1);
        query.setParameter(PARAM_ID_PERIODE, idPeriode);
        query.setParameter("dateHeureRetour", dateHeureRetour);
        return getPremierResultatSiExiste(query.getResultList());
    }

    @Override
    @Transactional(propagation = Propagation.MANDATORY, rollbackFor = Throwable.class)
    public HistoriqueAttenteRetourEtpPeriode getDernierHistoriquePourPeriode(Long idPeriode) {
        String jpql = "FROM HistoriqueAttenteRetourEtpPeriode h WHERE h.idPeriode=:idPeriode ORDER BY h.dateHeureRetour DESC";
        TypedQuery<HistoriqueAttenteRetourEtpPeriode> query = getEntityManager().createQuery(jpql, HistoriqueAttenteRetourEtpPeriode.class)
                .setMaxResults(1);
        query.setParameter(PARAM_ID_PERIODE, idPeriode);
        return getPremierResultatSiExiste(query.getResultList());
    }

    @Override
    public int supprimeHistoriqueAttenteRetourEtpPourPeriode(Long idPeriode) {
        String jpql = "DELETE FROM HistoriqueAttenteRetourEtpPeriode h WHERE h.idPeriode = :idPeriode";
        Query query = getEntityManager().createQuery(jpql);
        query.setParameter(PARAM_ID_PERIODE, idPeriode);
        return query.executeUpdate();

    }

    @Override
    @Transactional(propagation = Propagation.MANDATORY)
    public void callMysqlProcedure(String sql, Map<String, Object> params) {
        getJdbcTemplate().update(sql, params);
    }

    @Override
    public void creationEnMasseOrigineIHM(List<Long> idsPeriodes, TypeActionPopupChangementEnMasse action, String utilisateur) {
        String sql = "call Creation_HistoriqueAttenteRetourEtpPeriode_en_masse( :" + PARAM_LISTE_ID_PERIODES + ","
                + ":datehms , :utilisateur , :action , :userCreation )";
        Map<String, Object> params = new HashMap<>();
        params.put(PARAM_LISTE_ID_PERIODES, StringUtils.join(idsPeriodes, ","));
        params.put("datehms", DateRedac.convertitEnDateHeureRedac(Calendar.getInstance().getTime()));
        params.put("utilisateur", utilisateur);
        params.put("action", action.getDesignation());
        params.put("userCreation", utilisateur);

        callMysqlProcedure(sql, params);

    }

    @Override
    public List<HistoriqueAttenteRetourEtpPeriode> getHistoriqueAttenteRetourEtpPeriodes(List<Long> idsPeriodes) {
        String jpql = "FROM HistoriqueAttenteRetourEtpPeriode h where h.idPeriode in (" + StringUtils.join(idsPeriodes, ",")
                + ") ORDER BY h.idPeriode DESC";
        TypedQuery<HistoriqueAttenteRetourEtpPeriode> query = getEntityManager().createQuery(jpql, HistoriqueAttenteRetourEtpPeriode.class);
        return query.getResultList();
    }

}
