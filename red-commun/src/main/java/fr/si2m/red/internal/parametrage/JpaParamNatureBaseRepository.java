package fr.si2m.red.internal.parametrage;

import java.util.List;

import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import fr.si2m.red.core.repository.jpa.JpaEntiteImportableRepository;
import fr.si2m.red.parametrage.ParamNatureBase;
import fr.si2m.red.parametrage.ParamNatureBaseId;
import fr.si2m.red.parametrage.ParamNatureBaseRepository;
import fr.si2m.red.parametrage.Tranche;

/**
 * Base de données des paramètres de nature de base.
 * 
 * @author nortaina
 *
 */
@Repository
public class JpaParamNatureBaseRepository extends JpaEntiteImportableRepository<ParamNatureBase> implements ParamNatureBaseRepository {

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public boolean existeUnParametragePourNatureBaseCotisations(Integer natureBaseCotisations) {
        TypedQuery<Long> query = getEntityManager().createQuery("SELECT count(*) FROM ParamNatureBase WHERE CONBCOT = :natureBaseCotisations",
                Long.class);
        query.setParameter("natureBaseCotisations", natureBaseCotisations);
        List<Long> result = query.getResultList();
        if (result.get(0) == null) {
            return false;
        }
        return result.get(0) > 0;
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public ParamNatureBase getParamNatureBase(Integer natureBaseCotisations, String tauxCalculRempli, Integer numTranche) {
        ParamNatureBaseId id = new ParamNatureBaseId(false, natureBaseCotisations, tauxCalculRempli, numTranche);
        ParamNatureBase paramNatureBase = getEntityManager().find(ParamNatureBase.class, id);
        if (paramNatureBase != null) {
            getEntityManager().detach(paramNatureBase);
        }
        return paramNatureBase;
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public List<ParamNatureBase> getPourNatureBaseCotisationsEtTauxCalculRempli(Integer codeNatureBaseCotisations, boolean tauxCalculRempli) {
        String jpql = "SELECT p FROM ParamNatureBase p WHERE p.codeNatureBaseCotisations = :codeNatureBaseCotisations AND p.tauxCalculRempliAsText =:tauxCalculRempliAsText";
        TypedQuery<ParamNatureBase> query = getEntityManager().createQuery(jpql, ParamNatureBase.class);
        query.setParameter("codeNatureBaseCotisations", codeNatureBaseCotisations);
        query.setParameter("tauxCalculRempliAsText", tauxCalculRempli ? "O" : "N");
        return query.getResultList();
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public List<Tranche> getTranchesDistinctesPourNatureBaseCotisations(Integer codeNatureBaseCotisations) {
        String jpql = "SELECT DISTINCT new fr.si2m.red.parametrage.Tranche(p.numTranche, p.libelleTranche) FROM ParamNatureBase p "
                + "WHERE p.codeNatureBaseCotisations = :codeNatureBaseCotisations";
        TypedQuery<Tranche> query = getEntityManager().createQuery(jpql, Tranche.class);
        query.setParameter("codeNatureBaseCotisations", codeNatureBaseCotisations);
        return query.getResultList();
    }

}
