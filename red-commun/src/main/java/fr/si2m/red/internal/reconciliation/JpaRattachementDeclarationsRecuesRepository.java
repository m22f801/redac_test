package fr.si2m.red.internal.reconciliation;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.apache.commons.lang3.StringUtils;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import fr.si2m.red.core.repository.jpa.JpaRepository;
import fr.si2m.red.dsn.AdhesionEtablissementMois;
import fr.si2m.red.dsn.Affiliation;
import fr.si2m.red.dsn.BaseAssujettie;
import fr.si2m.red.dsn.ComposantBaseAssujettie;
import fr.si2m.red.dsn.ComposantVersement;
import fr.si2m.red.dsn.ContratTravail;
import fr.si2m.red.dsn.CotisationEtablissement;
import fr.si2m.red.dsn.ResumeAffiliation;
import fr.si2m.red.dsn.ResumeAffiliationExportExcel;
import fr.si2m.red.dsn.ResumeAffiliationExportExcelAllegesMessage;
import fr.si2m.red.dsn.ResumeAffiliationExportExcelAllegesMontant;
import fr.si2m.red.dsn.ResumeChangementIndividu;
import fr.si2m.red.dsn.Versement;
import fr.si2m.red.parametrage.ParamControleSignalRejet;
import fr.si2m.red.reconciliation.ExportIndividuExcel;
import fr.si2m.red.reconciliation.IdentifiantsIndividu;
import fr.si2m.red.reconciliation.RattachementDeclarationsRecues;
import fr.si2m.red.reconciliation.RattachementDeclarationsRecuesRepository;
import fr.si2m.red.reconciliation.ResumeDeclarationIndividuExcel;

/**
 * Base de données des éléments rattachés à une période / déclaration, connectée via JPA.
 * 
 * @author poidij
 *
 */
@Repository
public class JpaRattachementDeclarationsRecuesRepository extends JpaRepository<RattachementDeclarationsRecues>
        implements RattachementDeclarationsRecuesRepository {

    /**
     * Nom de la colonne récupérant le matricule dans les rowMapper
     */
    private static final String ROW_MATRICULE = "MATRICULE";

    /**
     * Nom de la colonne récupérant le prénom dans les rowMapper
     */
    private static final String ROW_PRENOM = "PRENOM";

    /**
     * Nom de la colonne récupérant le nom d'usage dans les rowMapper
     */
    private static final String ROW_NOM_USAGE = "NOM_USAGE";

    /**
     * Nom de la colonne récupérant l'identifiant repertoire dans les rowMapper
     */
    private static final String ROW_IDENTIFIANT_REPERTOIRE = "IDENTIFIANT_REPERTOIRE";

    /**
     * Nom de la colonne récupérant la date debut contrat dans les rowMapper
     */
    private static final String ROW_DATE_DEBUT_CONTRAT = "DATE_DEBUT_CONTRAT";

    /**
     * Nom de la colonne récupérant la date fin previsionnelle dans les rowMapper
     */
    private static final String ROW_DATE_FIN_PREVISIONNELLE = "DATE_FIN_PREVISIONNELLE";

    /**
     * Nom de la colonne récupérant la code population dans les rowMapper
     */
    private static final String ROW_CODE_POPULATION = "CODE_POPULATION";

    /**
     * Nom de la colonne récupérant la code population dans les rowMapper
     */
    private static final String ROW_DATE_DEBUT_AFFILIATION = "DATE_DEBUT_AFFILIATION";

    /**
     * Nom de la colonne récupérant la code population dans les rowMapper
     */
    private static final String ROW_DATE_FIN_AFFILIATION = "DATE_FIN_AFFILIATION";

    /**
     * Nom de la colonne récuperant la somme des montants de cotisations
     */
    private static final String ROW_SOMME_MONTANTS_COTISATION = "SOMME_MONTANTS_COTISATION";

    /**
     * Nom de la colonne récupérant le nom famille dans les rowMapper
     */
    private static final String ROW_NOM_FAMILLE = "NOM_FAMILLE";

    /**
     * Nom du paramètre utilisé pour le numéro technique temporaire (NTT)
     */
    private static final String PARAM_NUMERO_TECHNIQUE_TEMPORAIRE = "numeroTechniqueTemporaire";

    /**
     * Nom du paramètre utilisé pour l'identifiant répertoire (NIR)
     */
    private static final String PARAM_IDENTIFIANT_REPERTOIRE = "identifiantRepertoire";
    /**
     * Le nom de paramètre utilisé pour l'identifiant de période.
     */
    private static final String PARAM_ID_PERIODE = "idPeriode";

    /**
     * Nom du paramètre utilisé pour le premier index à récupérer sur une requête
     */
    private static final String PARAM_INDEX_PREMIER_RESULTAT = "indexPremierResultat";

    /**
     * Paramètre taille de la page
     */
    private static final String PARAM_TAILLE_PAGE = "taillePage";

    /**
     * Nom du paramètre utilisé pour les messages d'alertes.
     */
    private static final String PARAM_ALERT_SIGNAL = "alertSignal";

    /**
     * Nom du paramètre utilisé pour les messages de rejets.
     */
    private static final String PARAM_ALERT_REJET = "alertRejet";

    /**
     * Condition sur le NTT de l'invididu
     */
    private static final String JPQL_CLAUSE_NUMERO_TECHNIQUE_TEMPORAIRE = "i.numeroTechniqueTemporaire = :" + PARAM_NUMERO_TECHNIQUE_TEMPORAIRE
            + " AND ";

    /**
     * Condition sur le NIR de l'individu
     */
    private static final String JPQL_CLAUSE_IDENTIFIANT_REPERTOIRE = "i.identifiantRepertoire = :" + PARAM_IDENTIFIANT_REPERTOIRE + " AND ";

    /**
     * La clause à ajouter systématiquement pour le lien entre une période et une adhésion établissement mois rattachée pour les requêtes en JPQL.
     */
    private static final String JPQL_CLAUSE_ADHESION_ETABLISSEMENT_MOIS_RATTACHEE_A_PERIODE = "adh.ligneEnCoursImportBatch IS FALSE AND adh.idAdhEtabMois IN "
            + "(SELECT r.idAdhEtabMois FROM RattachementDeclarationsRecues r WHERE r.idPeriode = :" + PARAM_ID_PERIODE + ")";

    /**
     * La clause à ajouter systématiquement pour le lien entre une période et une adhésion établissement mois rattachée pour les requêtes en SQL natif.
     */
    private static final String SQL_CLAUSE_ADHESION_ETABLISSEMENT_MOIS_RATTACHEE_A_PERIODE = "adh.TMP_BATCH IS FALSE AND adh.ID_ADH_ETAB_MOIS IN (SELECT r.ID_ADH_ETAB_MOIS "
            + "FROM RATTACHEMENT_DECLARATIONS_RECUES r WHERE r.ID_PERIODE = :" + PARAM_ID_PERIODE + ")";

    /**
     * La clause de jointure pour le lien entre un individu et une adhésion établissement pour les recherches d'individus en SQL natif.
     */
    private static final String SQL_JOINTURE_ADHESION_ETABLISSEMENT_MOIS_POUR_INDIVIDU = " LEFT JOIN ADHESION_ETABLISSEMENT_MOIS adh ON adh.ID_ADH_ETAB_MOIS = i.ID_ADH_ETAB_MOIS AND adh.TMP_BATCH = i.TMP_BATCH WHERE ";

    /**
     * La fin de requête pour les recherches d'individus en SQL natif.
     */
    private static final String SQL_PARTIE_REQUETE_INDIV_DISTINCTS = ") as _INDIV_DISTINCTS";

    /**
     * La sous-requête de filtrage des situations tarifaires liées à une une periode d'un contrat donnée pour les recherches de LICAT en SQL natif.
     */
    private static final String SQL_PARTIE_REQUETE_TARIF_LICAT = " (SELECT t.LICAT FROM TARIFS t inner join PERIODES_RECUES p on t.NOCO=p.NUMERO_CONTRAT WHERE t.DT_DEBUT_SIT <= coalesce(p.DATE_FIN_PERIODE, 0) "
            + " AND (coalesce(p.DATE_FIN_PERIODE, 0) <= (coalesce(t.DT_FIN_SIT, 99999999)) OR coalesce(p.DATE_FIN_PERIODE, 0) <=(CASE WHEN (t.DT_FIN_SIT = 0) THEN 99999999 ELSE t.DT_FIN_SIT END)) AND t.NOCAT=a.CODE_POPULATION AND p.ID_PERIODE= :";

    /**
     * Le début de requête pour les recherches des versements en JPQL natif.
     */
    private static final String JPQL_PARTIE_REQUETE_VERSEMENTS = "SELECT v FROM Versement v LEFT JOIN v.adhesionEtablissementMois adh WHERE ";

    /**
     * Jointure contrat travail - individu - adh
     */
    private static final String SQL_JOIN_CONTRAT_TRAVAIL_INDIVIDU_ADH = " LEFT JOIN a.contratTravail ct LEFT JOIN ct.individu i LEFT JOIN i.adhesionEtablissementMois adh ";

    /**
     * Persiste un rattachement de déclarations recues en base.
     * 
     * @param rattachement
     *            le rattachement à persister
     */
    @Transactional(propagation = Propagation.MANDATORY)
    public void creeUnRattachement(RattachementDeclarationsRecues rattachement) {
        getEntityManager().persist(rattachement);
        reinitialiseContexteJpa();
    }

    @Override
    public Class<RattachementDeclarationsRecues> getClassePrototypeEntite() {
        return RattachementDeclarationsRecues.class;
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public List<ContratTravail> getContratsTravailRattachesPagines(Long idPeriode, Integer indexPremierResultat, Integer taillePage) {
        String jpql = "SELECT c FROM ContratTravail c LEFT JOIN c.individu i LEFT JOIN i.adhesionEtablissementMois adh WHERE "
                + JPQL_CLAUSE_ADHESION_ETABLISSEMENT_MOIS_RATTACHEE_A_PERIODE + " ORDER BY c.idContratTravail";
        TypedQuery<ContratTravail> query = getEntityManager().createQuery(jpql, ContratTravail.class);
        appliqueParametragePagination(query, indexPremierResultat, taillePage);
        query.setParameter(PARAM_ID_PERIODE, idPeriode);
        return query.getResultList();
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public long compteContratsTravailRattachesDistincts(Long idPeriode) {
        String sql = "SELECT COUNT(DISTINCT adh.NIC_ETABLISSEMENT, adh.SIREN_ENTREPRISE, i.IDENTIFIANT_REPERTOIRE, i.NTT, c.NUMERO_CONTRAT)"
                + " FROM CONTRAT_TRAVAIL c LEFT JOIN INDIVIDU i ON c.ID_INDIVIDU=i.ID_INDIVIDU AND c.TMP_BATCH=i.TMP_BATCH LEFT JOIN ADHESION_ETABLISSEMENT_MOIS adh ON i.ID_ADH_ETAB_MOIS=adh.ID_ADH_ETAB_MOIS AND i.TMP_BATCH=adh.TMP_BATCH"
                + " WHERE adh.TMP_BATCH IS FALSE AND adh.ID_ADH_ETAB_MOIS IN (SELECT r.ID_ADH_ETAB_MOIS FROM RATTACHEMENT_DECLARATIONS_RECUES r WHERE r.ID_PERIODE = :"
                + PARAM_ID_PERIODE + ")";

        Map<String, Object> params = new HashMap<>();

        params.put(PARAM_ID_PERIODE, idPeriode);

        // mapper
        return getPremierResultatSiExiste(getJdbcTemplate().query(sql, params, new RowMapper<Long>() {
            @Override
            public Long mapRow(ResultSet rs, int rowNum) throws SQLException {
                return rs.getLong(1);
            }
        }));

    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public List<BaseAssujettie> getBasesAssujettiesRattacheesAvecMontantCotisationNonNulPagines(Long idPeriode, Integer indexPremierResultat,
            Integer taillePage) {
        String jpql = "SELECT ba FROM BaseAssujettie ba LEFT JOIN ba.affiliation a " + SQL_JOIN_CONTRAT_TRAVAIL_INDIVIDU_ADH
                + " WHERE ba.montantCotisation <> 0 AND " + JPQL_CLAUSE_ADHESION_ETABLISSEMENT_MOIS_RATTACHEE_A_PERIODE
                + " ORDER BY ba.idBaseAssujettie";
        TypedQuery<BaseAssujettie> query = getEntityManager().createQuery(jpql, BaseAssujettie.class);
        appliqueParametragePagination(query, indexPremierResultat, taillePage);
        query.setParameter(PARAM_ID_PERIODE, idPeriode);
        return query.getResultList();
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public List<BaseAssujettie> getAllBasesAssujettiesRattacheesPagines(Long idPeriode, Integer indexPremierResultat, Integer taillePage) {
        String jpql = "SELECT ba FROM BaseAssujettie ba LEFT JOIN ba.affiliation a " + SQL_JOIN_CONTRAT_TRAVAIL_INDIVIDU_ADH + " WHERE "
                + JPQL_CLAUSE_ADHESION_ETABLISSEMENT_MOIS_RATTACHEE_A_PERIODE + " ORDER BY ba.idBaseAssujettie";
        TypedQuery<BaseAssujettie> query = getEntityManager().createQuery(jpql, BaseAssujettie.class);
        appliqueParametragePagination(query, indexPremierResultat, taillePage);
        query.setParameter(PARAM_ID_PERIODE, idPeriode);
        return query.getResultList();
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public List<ResumeDeclarationIndividuExcel> getBasesAssujettiesRattacheesPagines(Long idPeriode, int indexPremierResultat, int taillePage) {
        String sql = "SELECT adh.DATE_FICHIER AS DATE_CONSTITUTION, adh.MOIS_RATTACHEMENT AS MOIS_RATTACHEMENT,"
                + " adh.MOIS_DECLARE AS MOIS_DECLARE, adh.SIREN_ENTREPRISE AS SIREN, adh.NIC_ETABLISSEMENT AS NIC,"

                + " i.IDENTIFIANT_REPERTOIRE AS NIR, i.NTT AS NTT, i.MATRICULE AS NUM_ADHERENT, i.SEXE AS SEXE, i.NOM_FAMILLE AS NOM_FAMILLE,"
                + " i.NOM_USAGE AS NOM_USAGE, i.PRENOM AS PRENOM, i.DATE_NAISSANCE AS DATE_NAISSANCE,"

                + " ct.DATE_DEBUT_CONTRAT AS DATE_DEBUT_CONTRAT, ct.DATE_FIN_PREVISIONNELLE AS DATE_FIN_PREVISIONNELLE,"
                + " ct.LIBELLE_EMPLOI AS LIBELLE_EMPLOI, ct.NUMERO_CONTRAT AS NUMERO_CONTRAT,"

                + " a.CODE_OPTION AS CODE_OPTION, a.CODE_POPULATION AS CODE_POPULATION, a.NOMBRE_AYDR_ADULTES AS AYANTS_DROITS_ADULTES,"
                + " a.DATE_DEBUT_AFFILIATION AS DATE_DEBUT_AFFILIATION, a.DATE_FIN_AFFILIATION AS DATE_FIN_AFFILIATION, "
                + " a.NOMBRE_AYDR AS AYANTS_DROITS, a.NOMBRE_AYDR_AUTRES AS AYANTS_DROITS_AUTRES, a.NOMBRE_AYDR_ENFANTS AS AYANTS_DROITS_ENFANTS,"

                + " ba.DATE_DEB_RATTACHEMENT AS DATE_DEB_RATTACHEMENT, ba.DATE_FIN_RATTACHEMENT AS DATE_FIN_RATTACHEMENT, ba.MONTANT_COTISATION AS MONTANT_COTISATION,"

                // Selection des montants base assujettie par type et par id_base_assujettie (group by)
                + " c.ID_BASE_ASSUJETTIE AS CBA_ID_BASE_ASSUJETTIE, "
                + " MAX(IF((c.TYPE_COMPO_BASE_ASSUJ = 10), c.MONTANT_COMPO_BASE_ASSUJ, NULL)) AS MTCBA_COL1, "
                + " MAX(IF((c.TYPE_COMPO_BASE_ASSUJ = 11), c.MONTANT_COMPO_BASE_ASSUJ, NULL)) AS MTCBA_COL2, "
                + " MAX(IF((c.TYPE_COMPO_BASE_ASSUJ = 12), c.MONTANT_COMPO_BASE_ASSUJ, NULL)) AS MTCBA_COL3, "
                + " MAX(IF((c.TYPE_COMPO_BASE_ASSUJ = 13), c.MONTANT_COMPO_BASE_ASSUJ, NULL)) AS MTCBA_COL4, "
                + " MAX(IF((c.TYPE_COMPO_BASE_ASSUJ = 14), c.MONTANT_COMPO_BASE_ASSUJ, NULL)) AS MTCBA_COL5, "
                + " MAX(IF((c.TYPE_COMPO_BASE_ASSUJ = 15), c.MONTANT_COMPO_BASE_ASSUJ, NULL)) AS MTCBA_COL6, "
                + " MAX(IF((c.TYPE_COMPO_BASE_ASSUJ = 16), c.MONTANT_COMPO_BASE_ASSUJ, NULL)) AS MTCBA_COL7, "
                + " MAX(IF((c.TYPE_COMPO_BASE_ASSUJ = 17), c.MONTANT_COMPO_BASE_ASSUJ, NULL)) AS MTCBA_COL8, "
                + " MAX(IF((c.TYPE_COMPO_BASE_ASSUJ = 18), c.MONTANT_COMPO_BASE_ASSUJ, NULL)) AS MTCBA_COL9, "
                + " MAX(IF((c.TYPE_COMPO_BASE_ASSUJ = 19), c.MONTANT_COMPO_BASE_ASSUJ, NULL)) AS MTCBA_COL10, "
                + " MAX(IF((c.TYPE_COMPO_BASE_ASSUJ = 20), c.MONTANT_COMPO_BASE_ASSUJ, NULL)) AS MTCBA_COL11, "
                + " MAX(IF((c.TYPE_COMPO_BASE_ASSUJ = 21), c.MONTANT_COMPO_BASE_ASSUJ, NULL)) AS MTCBA_COL12, "
                + " MAX(IF((c.TYPE_COMPO_BASE_ASSUJ = 24), c.MONTANT_COMPO_BASE_ASSUJ, NULL)) AS MTCBA_COL13, "

                // Selection des montants tranches par numero tranche et par id_base_assujettie (group by)

                + " tcba.ID_BASE_ASSUJETTIE AS TCBA_ID_BASE_ASSUJETTIE, "
                + " MAX(IF((tcba.NUM_TRANCHE = 1), tcba.MT_TRANCHE, NULL)) AS MONTANT_TRANCHE_1,"
                + " MAX(IF((tcba.NUM_TRANCHE = 2), tcba.MT_TRANCHE, NULL)) AS MONTANT_TRANCHE_2, "
                + " MAX(IF((tcba.NUM_TRANCHE = 3), tcba.MT_TRANCHE, NULL)) AS MONTANT_TRANCHE_3, "
                + " MAX(IF((tcba.NUM_TRANCHE = 4), tcba.MT_TRANCHE, NULL)) AS MONTANT_TRANCHE_4, "

                // estimation cotisation
                // RED-261 Le GROUP BY écrase les lignes de tcba, et somme plusieurs fois la même ligne ( demultiplication / nombre de tranche ) , d'où la SR
                + " (SELECT SUM(tcba.ESTIMATION_COTISATION) FROM TRANCHE_CATEGORIE_BASE_ASSUJETTIE tcba"
                + "     WHERE tcba.ID_BASE_ASSUJETTIE=ba.ID_BASE_ASSUJETTIE AND tcba.ID_PERIODE= :" + PARAM_ID_PERIODE + ") AS ESTIMATION_COTISATION "

                + " FROM BASE_ASSUJETTIE ba"

                + " LEFT JOIN AFFILIATION a ON ba.ID_AFFILIATION=a.ID_AFFILIATION AND ba.TMP_BATCH=a.TMP_BATCH"
                + " LEFT JOIN CONTRAT_TRAVAIL ct ON ct.ID_CONTRAT_TRAVAIL=a.ID_CONTRAT_TRAVAIL AND ct.TMP_BATCH=a.TMP_BATCH"
                + " LEFT JOIN INDIVIDU i ON i.ID_INDIVIDU=ct.ID_INDIVIDU AND i.TMP_BATCH=ct.TMP_BATCH"
                + " LEFT JOIN COMPOSANT_BASE_ASSUJETTIE c ON c.ID_BASE_ASSUJETTIE = ba.ID_BASE_ASSUJETTIE  AND c.TMP_BATCH=a.TMP_BATCH"
                + " LEFT JOIN TRANCHE_CATEGORIE_BASE_ASSUJETTIE tcba ON tcba.ID_BASE_ASSUJETTIE= ba.ID_BASE_ASSUJETTIE AND tcba.ID_PERIODE = :"
                + PARAM_ID_PERIODE
                + " LEFT JOIN ADHESION_ETABLISSEMENT_MOIS adh ON adh.ID_ADH_ETAB_MOIS=i.ID_ADH_ETAB_MOIS AND adh.TMP_BATCH=i.TMP_BATCH"

                + " WHERE " + SQL_CLAUSE_ADHESION_ETABLISSEMENT_MOIS_RATTACHEE_A_PERIODE

                + " GROUP BY c.ID_BASE_ASSUJETTIE, tcba.ID_BASE_ASSUJETTIE"
                + " ORDER BY adh.ID_ADH_ETAB_MOIS,i.NOM_FAMILLE,i.NOM_USAGE,i.PRENOM LIMIT :taillePage OFFSET :indexPremierResultat";

        Map<String, Object> params = new HashMap<>();
        params.put(PARAM_INDEX_PREMIER_RESULTAT, indexPremierResultat);
        params.put(PARAM_TAILLE_PAGE, taillePage);
        params.put(PARAM_ID_PERIODE, idPeriode);
        return getJdbcTemplate().query(sql, params, new ResumeDeclarationIndividuExcelRowMapper());

    }

    /**
     * Mapper de lignes de la declaration individu vers Excel.
     * 
     * @author rupip
     *
     */
    public static class ResumeDeclarationIndividuExcelRowMapper implements RowMapper<ResumeDeclarationIndividuExcel> {

        @Override
        public ResumeDeclarationIndividuExcel mapRow(ResultSet rs, int rowNum) throws SQLException {
            ResumeDeclarationIndividuExcel declarationIndividu = new ResumeDeclarationIndividuExcel();

            // Adhésion
            declarationIndividu.setDateConstitution(getInteger(rs, "DATE_CONSTITUTION"));
            declarationIndividu.setMoisRattachement(getInteger(rs, "MOIS_RATTACHEMENT"));
            declarationIndividu.setMoisDeclare(getInteger(rs, "MOIS_DECLARE"));
            declarationIndividu.setSiren(rs.getString("SIREN"));
            declarationIndividu.setNic(rs.getString("NIC"));

            // Individu
            declarationIndividu.setNir(rs.getString("NIR"));
            declarationIndividu.setNtt(rs.getString("NTT"));
            declarationIndividu.setNumAdherent(rs.getString("NUM_ADHERENT"));
            declarationIndividu.setSexe(rs.getString("SEXE"));
            declarationIndividu.setNomFamille(rs.getString(ROW_NOM_FAMILLE));
            declarationIndividu.setNomUsage(rs.getString("NOM_USAGE"));
            declarationIndividu.setPrenom(rs.getString("PRENOM"));
            declarationIndividu.setDateNaissance(getInteger(rs, "DATE_NAISSANCE"));

            // Contrat travail
            declarationIndividu.setDateDebutContrat(getInteger(rs, ROW_DATE_DEBUT_CONTRAT));
            declarationIndividu.setDateFinPrevisionnelle(getInteger(rs, ROW_DATE_FIN_PREVISIONNELLE));
            declarationIndividu.setLibelleEmploi(rs.getString("LIBELLE_EMPLOI"));
            declarationIndividu.setNumeroContrat(rs.getString("NUMERO_CONTRAT"));

            // Affiliation
            declarationIndividu.setCodeOption(rs.getString("CODE_OPTION"));
            declarationIndividu.setCodePopulation(rs.getString(ROW_CODE_POPULATION));
            declarationIndividu.setAyantDroitAdultes(getInteger(rs, "AYANTS_DROITS_ADULTES"));
            declarationIndividu.setAyantDroit(getInteger(rs, "AYANTS_DROITS"));
            declarationIndividu.setAyantDroitAutres(getInteger(rs, "AYANTS_DROITS_AUTRES"));
            declarationIndividu.setAyantDroitEnfants(getInteger(rs, "AYANTS_DROITS_ENFANTS"));
            declarationIndividu.setDateDebutAffiliation(getInteger(rs, ROW_DATE_DEBUT_AFFILIATION));
            declarationIndividu.setDateFinAffiliation(getInteger(rs, ROW_DATE_FIN_AFFILIATION));

            // Base assujettie
            declarationIndividu.setDateDebRattachement(getInteger(rs, "DATE_DEB_RATTACHEMENT"));
            declarationIndividu.setDateFinRattachement(getInteger(rs, "DATE_FIN_RATTACHEMENT"));
            declarationIndividu.setMontantCotisation(rs.getDouble("MONTANT_COTISATION"));
            declarationIndividu.setEstimationCotisation(rs.getDouble("ESTIMATION_COTISATION"));

            // Montant du type de composant base assujettie
            declarationIndividu.setMontantCBA10(rs.getDouble("MTCBA_COL1"));
            declarationIndividu.setMontantCBA11(rs.getDouble("MTCBA_COL2"));
            declarationIndividu.setMontantCBA12(rs.getDouble("MTCBA_COL3"));
            declarationIndividu.setMontantCBA13(rs.getDouble("MTCBA_COL4"));
            declarationIndividu.setMontantCBA14(rs.getDouble("MTCBA_COL5"));
            declarationIndividu.setMontantCBA15(rs.getDouble("MTCBA_COL6"));
            declarationIndividu.setMontantCBA16(rs.getDouble("MTCBA_COL7"));
            declarationIndividu.setMontantCBA17(rs.getDouble("MTCBA_COL8"));
            declarationIndividu.setMontantCBA18(rs.getDouble("MTCBA_COL9"));
            declarationIndividu.setMontantCBA19(rs.getDouble("MTCBA_COL10"));
            declarationIndividu.setMontantCBA20(rs.getDouble("MTCBA_COL11"));
            declarationIndividu.setMontantCBA21(rs.getDouble("MTCBA_COL12"));
            declarationIndividu.setMontantCBA24(rs.getDouble("MTCBA_COL13"));

            // Montant de la tranche correspondante
            declarationIndividu.setMontantTranche1(rs.getDouble("MONTANT_TRANCHE_1"));
            declarationIndividu.setMontantTranche2(rs.getDouble("MONTANT_TRANCHE_2"));
            declarationIndividu.setMontantTranche3(rs.getDouble("MONTANT_TRANCHE_3"));
            declarationIndividu.setMontantTranche4(rs.getDouble("MONTANT_TRANCHE_4"));

            return declarationIndividu;
        }
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public List<ComposantBaseAssujettie> getComposantsBasesAssujettiesRattachesPagines(Long idPeriode, Integer indexPremierResultat,
            Integer taillePage) {
        String jpql = "SELECT cba FROM ComposantBaseAssujettie cba LEFT JOIN cba.baseAssujettie ba LEFT JOIN ba.affiliation a "
                + SQL_JOIN_CONTRAT_TRAVAIL_INDIVIDU_ADH + " WHERE cba.montantComposantBaseAssujettie <> 0 AND ba.montantCotisation <> 0 AND "
                + JPQL_CLAUSE_ADHESION_ETABLISSEMENT_MOIS_RATTACHEE_A_PERIODE + " ORDER BY cba.idComposantBaseAssujettie";
        TypedQuery<ComposantBaseAssujettie> query = getEntityManager().createQuery(jpql, ComposantBaseAssujettie.class);
        appliqueParametragePagination(query, indexPremierResultat, taillePage);
        query.setParameter(PARAM_ID_PERIODE, idPeriode);
        return query.getResultList();
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public Set<IdentifiantsIndividu> getDistinctIdentifiantsIndividusRattachesPagines(Long idPeriode, Integer indexPremierResultat,
            Integer taillePage) {
        String jpql = "SELECT DISTINCT new fr.si2m.red.reconciliation.IdentifiantsIndividu(i.identifiantRepertoire,i.numeroTechniqueTemporaire)"
                + " FROM Individu i LEFT JOIN i.adhesionEtablissementMois adh WHERE " + JPQL_CLAUSE_ADHESION_ETABLISSEMENT_MOIS_RATTACHEE_A_PERIODE
                + " ORDER BY i.identifiantRepertoire DESC, i.numeroTechniqueTemporaire";
        TypedQuery<IdentifiantsIndividu> query = getEntityManager().createQuery(jpql, IdentifiantsIndividu.class);
        appliqueParametragePagination(query, indexPremierResultat, taillePage);
        query.setParameter(PARAM_ID_PERIODE, idPeriode);
        return new HashSet<>(query.getResultList());
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public boolean existeUneCotisationEtablissementRattachee(Long idPeriode) {
        String jpql = "SELECT COUNT(ce) FROM CotisationEtablissement ce LEFT JOIN ce.adhesionEtablissementMois adh WHERE "
                + JPQL_CLAUSE_ADHESION_ETABLISSEMENT_MOIS_RATTACHEE_A_PERIODE;
        TypedQuery<Long> query = getEntityManager().createQuery(jpql, Long.class);
        query.setParameter(PARAM_ID_PERIODE, idPeriode);
        return query.getSingleResult() > 0;
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public int compteNbEtablissementsRattaches(Long idPeriode) {
        // Pas de select-count-distinct possible en JPA / Hibernate
        String sql = "SELECT COUNT(*) FROM (SELECT DISTINCT adh.SIREN_ENTREPRISE, adh.NIC_ETABLISSEMENT FROM ADHESION_ETABLISSEMENT_MOIS adh WHERE "
                + SQL_CLAUSE_ADHESION_ETABLISSEMENT_MOIS_RATTACHEE_A_PERIODE + ") as _ETAB_DISTINCTS";
        Query query = getEntityManager().createNativeQuery(sql);
        query.setParameter(PARAM_ID_PERIODE, idPeriode);
        return Integer.parseInt(query.getSingleResult().toString());
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public int compteNbSalariesRattachesEnDebutPeriode(Long idPeriode, Integer dateDebutPeriode) {
        // Pas de select-count-distinct possible en JPA / Hibernate
        String sql = "SELECT COUNT(*) FROM (SELECT DISTINCT coalesce(i.IDENTIFIANT_REPERTOIRE, ''), coalesce(i.NTT, '') FROM INDIVIDU i "
                + "LEFT JOIN ADHESION_ETABLISSEMENT_MOIS adh ON adh.ID_ADH_ETAB_MOIS = i.ID_ADH_ETAB_MOIS AND adh.TMP_BATCH = i.TMP_BATCH "
                + "LEFT JOIN CONTRAT_TRAVAIL c ON i.ID_INDIVIDU = c.ID_INDIVIDU AND i.TMP_BATCH = c.TMP_BATCH "
                + "RIGHT JOIN AFFILIATION a ON c.ID_CONTRAT_TRAVAIL = a.ID_CONTRAT_TRAVAIL AND c.TMP_BATCH = a.TMP_BATCH "
                + "RIGHT JOIN BASE_ASSUJETTIE b ON a.ID_AFFILIATION = b.ID_AFFILIATION AND a.TMP_BATCH = b.TMP_BATCH "
                + "WHERE b.DATE_DEB_RATTACHEMENT = :dateDebutPeriode AND " + SQL_CLAUSE_ADHESION_ETABLISSEMENT_MOIS_RATTACHEE_A_PERIODE
                + SQL_PARTIE_REQUETE_INDIV_DISTINCTS;
        Query query = getEntityManager().createNativeQuery(sql);
        query.setParameter(PARAM_ID_PERIODE, idPeriode);
        query.setParameter("dateDebutPeriode", dateDebutPeriode);
        return Integer.parseInt(query.getSingleResult().toString());
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public int compteNbSalariesRattachesEnFinPeriode(Long idPeriode, Integer dateFinPeriode) {
        // Pas de select-count-distinct possible en JPA / Hibernate
        String sql = "SELECT COUNT(*) FROM (SELECT DISTINCT coalesce(i.IDENTIFIANT_REPERTOIRE, ''), coalesce(i.NTT, '') FROM INDIVIDU i "
                + "LEFT JOIN ADHESION_ETABLISSEMENT_MOIS adh ON adh.ID_ADH_ETAB_MOIS = i.ID_ADH_ETAB_MOIS AND adh.TMP_BATCH = i.TMP_BATCH "
                + "LEFT JOIN CONTRAT_TRAVAIL c ON i.ID_INDIVIDU = c.ID_INDIVIDU AND i.TMP_BATCH = c.TMP_BATCH "
                + "RIGHT JOIN AFFILIATION a ON c.ID_CONTRAT_TRAVAIL = a.ID_CONTRAT_TRAVAIL AND c.TMP_BATCH = a.TMP_BATCH "
                + "RIGHT JOIN BASE_ASSUJETTIE b ON a.ID_AFFILIATION = b.ID_AFFILIATION AND a.TMP_BATCH = b.TMP_BATCH "
                + "WHERE b.DATE_FIN_RATTACHEMENT = :dateFinPeriode AND " + SQL_CLAUSE_ADHESION_ETABLISSEMENT_MOIS_RATTACHEE_A_PERIODE
                + SQL_PARTIE_REQUETE_INDIV_DISTINCTS;
        Query query = getEntityManager().createNativeQuery(sql);
        query.setParameter(PARAM_ID_PERIODE, idPeriode);
        query.setParameter("dateFinPeriode", dateFinPeriode);
        return Integer.parseInt(query.getSingleResult().toString());
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public int compteNbSalariesRattaches(Long idPeriode) {
        // Pas de select-count-distinct possible en JPA / Hibernate
        String sql = "SELECT COUNT(*) FROM (SELECT DISTINCT (CASE WHEN coalesce(i.IDENTIFIANT_REPERTOIRE, '') <> '' THEN i.IDENTIFIANT_REPERTOIRE ELSE coalesce(i.NTT, '') END) AS ID_SALARIE FROM INDIVIDU i"
                + SQL_JOINTURE_ADHESION_ETABLISSEMENT_MOIS_POUR_INDIVIDU + SQL_CLAUSE_ADHESION_ETABLISSEMENT_MOIS_RATTACHEE_A_PERIODE
                + SQL_PARTIE_REQUETE_INDIV_DISTINCTS;
        Query query = getEntityManager().createNativeQuery(sql);
        query.setParameter(PARAM_ID_PERIODE, idPeriode);
        return Integer.parseInt(query.getSingleResult().toString());
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public boolean existeVersementRattacheAvecModePaiementParticulier(Long idPeriode) {
        String jpql = "SELECT COUNT(v) FROM Versement v LEFT JOIN v.adhesionEtablissementMois adh "
                + "WHERE (v.modePaiement = '03' OR v.modePaiement = '04' OR v.modePaiement = '05') AND "
                + JPQL_CLAUSE_ADHESION_ETABLISSEMENT_MOIS_RATTACHEE_A_PERIODE;
        TypedQuery<Long> query = getEntityManager().createQuery(jpql, Long.class);
        query.setParameter(PARAM_ID_PERIODE, idPeriode);
        return query.getSingleResult() > 0;
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public List<ComposantVersement> getComposantsVersementsRattachesControlablesPagines(Long idPeriode, Integer indexPremierResultat,
            Integer taillePage) {
        String jpql = "SELECT cv FROM ComposantVersement cv LEFT JOIN cv.versement v LEFT JOIN v.adhesionEtablissementMois adh "
                + "WHERE cv.periodeAffectation NOT LIKE '____E__' AND " + JPQL_CLAUSE_ADHESION_ETABLISSEMENT_MOIS_RATTACHEE_A_PERIODE
                + " ORDER BY cv.idComposantVersement";
        TypedQuery<ComposantVersement> query = getEntityManager().createQuery(jpql, ComposantVersement.class);
        appliqueParametragePagination(query, indexPremierResultat, taillePage);
        query.setParameter(PARAM_ID_PERIODE, idPeriode);
        return query.getResultList();
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public List<AdhesionEtablissementMois> getAdhesionsEtablissementsMoisRattaches(Long idPeriode) {
        String jpql = "SELECT adh FROM AdhesionEtablissementMois adh WHERE " + JPQL_CLAUSE_ADHESION_ETABLISSEMENT_MOIS_RATTACHEE_A_PERIODE;
        TypedQuery<AdhesionEtablissementMois> query = getEntityManager().createQuery(jpql, AdhesionEtablissementMois.class);
        query.setParameter(PARAM_ID_PERIODE, idPeriode);
        return query.getResultList();
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public AdhesionEtablissementMois getDerniereAdhesionEtablissementMoisRattachee(Long idPeriode, String sirenEntreprise, String nicEtablissement) {
        StringBuilder jpql = new StringBuilder(
                "SELECT adh FROM AdhesionEtablissementMois adh WHERE adh.sirenEntreprise = :sirenEntreprise AND adh.nicEtablissement = :nicEtablissement AND ");
        jpql.append(JPQL_CLAUSE_ADHESION_ETABLISSEMENT_MOIS_RATTACHEE_A_PERIODE).append(" ORDER BY adh.moisRattachement DESC");
        TypedQuery<AdhesionEtablissementMois> query = getEntityManager().createQuery(jpql.toString(), AdhesionEtablissementMois.class)
                .setMaxResults(1);
        query.setParameter(PARAM_ID_PERIODE, idPeriode);
        query.setParameter("sirenEntreprise", sirenEntreprise);
        query.setParameter("nicEtablissement", nicEtablissement);
        return getPremierResultatSiExiste(query.getResultList());
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public AdhesionEtablissementMois getDerniereAdhesionEtablissementMoisRattachee(Long idPeriode) {
        StringBuilder jpql = new StringBuilder("SELECT adh FROM AdhesionEtablissementMois adh WHERE ");
        jpql.append(JPQL_CLAUSE_ADHESION_ETABLISSEMENT_MOIS_RATTACHEE_A_PERIODE)
                .append(" ORDER BY adh.moisRattachement DESC, adh.idAdhEtabMois DESC");
        TypedQuery<AdhesionEtablissementMois> query = getEntityManager().createQuery(jpql.toString(), AdhesionEtablissementMois.class)
                .setMaxResults(1);
        query.setParameter(PARAM_ID_PERIODE, idPeriode);
        return getPremierResultatSiExiste(query.getResultList());
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public Integer getDateDerniereReceptionDSN(Long idPeriode) {
        StringBuilder jpql = new StringBuilder("SELECT adh.dateFichier FROM AdhesionEtablissementMois adh WHERE ");
        jpql.append(JPQL_CLAUSE_ADHESION_ETABLISSEMENT_MOIS_RATTACHEE_A_PERIODE).append(" ORDER BY adh.dateFichier DESC");
        TypedQuery<Integer> query = getEntityManager().createQuery(jpql.toString(), Integer.class).setMaxResults(1);
        query.setParameter(PARAM_ID_PERIODE, idPeriode);
        return getPremierResultatSiExiste(query.getResultList());
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public boolean existeUnChangementDateNaissanceIndividuRattache(Long idPeriode) {
        String jpql = "SELECT COUNT(ci) FROM ChangementIndividu ci LEFT JOIN ci.individu i LEFT JOIN i.adhesionEtablissementMois adh "
                + "WHERE ci.ligneEnCoursImportBatch IS FALSE AND (ci.ancienneDateNaissance IS NOT NULL AND ci.ancienneDateNaissance <> 0) AND "
                + JPQL_CLAUSE_ADHESION_ETABLISSEMENT_MOIS_RATTACHEE_A_PERIODE;
        TypedQuery<Long> query = getEntityManager().createQuery(jpql, Long.class);
        query.setParameter(PARAM_ID_PERIODE, idPeriode);
        return query.getSingleResult() > 0;
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public boolean existeUnChangementNIRIndividuRattache(Long idPeriode) {
        String jpql = "SELECT COUNT(ci) FROM ChangementIndividu ci LEFT JOIN ci.individu i LEFT JOIN i.adhesionEtablissementMois adh "
                + "WHERE ci.ligneEnCoursImportBatch IS FALSE AND (ci.ancienIdentifiant IS NOT NULL AND ci.ancienIdentifiant <> '') AND "
                + JPQL_CLAUSE_ADHESION_ETABLISSEMENT_MOIS_RATTACHEE_A_PERIODE;
        TypedQuery<Long> query = getEntityManager().createQuery(jpql, Long.class);
        query.setParameter(PARAM_ID_PERIODE, idPeriode);
        return query.getSingleResult() > 0;
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public List<Versement> getVersementsPourPeriode(Long idPeriode) {
        String jpql = JPQL_PARTIE_REQUETE_VERSEMENTS + JPQL_CLAUSE_ADHESION_ETABLISSEMENT_MOIS_RATTACHEE_A_PERIODE;
        TypedQuery<Versement> query = getEntityManager().createQuery(jpql, Versement.class);
        query.setParameter(PARAM_ID_PERIODE, idPeriode);
        return query.getResultList();
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public List<Versement> getVersementsPositifsPourPeriode(Long idPeriode) {
        String jpql = JPQL_PARTIE_REQUETE_VERSEMENTS + JPQL_CLAUSE_ADHESION_ETABLISSEMENT_MOIS_RATTACHEE_A_PERIODE + " AND v.montantVersement > 0";
        TypedQuery<Versement> query = getEntityManager().createQuery(jpql, Versement.class);
        query.setParameter(PARAM_ID_PERIODE, idPeriode);
        return query.getResultList();
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public Versement getDernierVersementPositifPourPeriode(Long idPeriode) {
        String jpql = JPQL_PARTIE_REQUETE_VERSEMENTS + JPQL_CLAUSE_ADHESION_ETABLISSEMENT_MOIS_RATTACHEE_A_PERIODE + " AND v.montantVersement > 0 "
                + " ORDER BY adh.moisRattachement DESC";
        TypedQuery<Versement> query = getEntityManager().createQuery(jpql, Versement.class).setMaxResults(1);
        query.setParameter(PARAM_ID_PERIODE, idPeriode);
        List<Versement> resultats = query.getResultList();
        return getPremierResultatSiExiste(resultats);
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public Double getSommeMontantsVersement(Long idPeriode, String modePaiement) {
        String jpql = "SELECT SUM(v.montantVersement) FROM Versement v LEFT JOIN v.adhesionEtablissementMois adh WHERE "
                + JPQL_CLAUSE_ADHESION_ETABLISSEMENT_MOIS_RATTACHEE_A_PERIODE;
        if (modePaiement != null) {
            jpql += " AND v.modePaiement = :modePaiement";
        }
        TypedQuery<Double> query = getEntityManager().createQuery(jpql, Double.class);
        query.setParameter(PARAM_ID_PERIODE, idPeriode);
        if (modePaiement != null) {
            query.setParameter("modePaiement", modePaiement);
        }
        return query.getSingleResult();
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public Double getSommeComposantsVersement(Long idPeriode) {
        String jpql = "SELECT SUM(cv.montantVerse) FROM ComposantVersement cv LEFT JOIN cv.versement v LEFT JOIN v.adhesionEtablissementMois adh WHERE "
                + JPQL_CLAUSE_ADHESION_ETABLISSEMENT_MOIS_RATTACHEE_A_PERIODE;
        TypedQuery<Double> query = getEntityManager().createQuery(jpql, Double.class);
        query.setParameter(PARAM_ID_PERIODE, idPeriode);
        return query.getSingleResult();
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public List<String> getModesPaiementsDistincts(Long idPeriode) {
        String jpql = "SELECT DISTINCT v.modePaiement FROM Versement v LEFT JOIN v.adhesionEtablissementMois adh WHERE "
                + JPQL_CLAUSE_ADHESION_ETABLISSEMENT_MOIS_RATTACHEE_A_PERIODE;
        TypedQuery<String> query = getEntityManager().createQuery(jpql, String.class);
        query.setParameter(PARAM_ID_PERIODE, idPeriode);
        return query.getResultList();
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public List<Affiliation> getAffiliationsRattachesPagines(Long idPeriode, Integer indexPremierResultat, Integer taillePage) {
        String jpql = "SELECT a FROM Affiliation a LEFT JOIN a.contratTravail ct LEFT JOIN ct.individu i LEFT JOIN i.adhesionEtablissementMois adh WHERE "
                + JPQL_CLAUSE_ADHESION_ETABLISSEMENT_MOIS_RATTACHEE_A_PERIODE + " ORDER BY i.nomUsage";
        TypedQuery<Affiliation> query = getEntityManager().createQuery(jpql, Affiliation.class);
        appliqueParametragePagination(query, indexPremierResultat, taillePage);
        query.setParameter(PARAM_ID_PERIODE, idPeriode);
        return query.getResultList();
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public List<ResumeAffiliation> getResumesAffiliationsDistinctesRattachesPagines(Long idPeriode, Integer indexPremierResultat,
            Integer taillePage) {
        String sql = "SELECT DISTINCT NOM_FAMILLE, NOM_USAGE, PRENOM,IDENTIFIANT_REPERTOIRE,NTT,MATRICULE,DATE_DEBUT_CONTRAT, DATE_FIN_PREVISIONNELLE,CODE_POPULATION"
                + " FROM (SELECT a.CODE_POPULATION AS CODE_POPULATION, i.IDENTIFIANT_REPERTOIRE AS IDENTIFIANT_REPERTOIRE, "
                + " i.NOM_USAGE AS NOM_USAGE, i.PRENOM AS PRENOM,i.NOM_FAMILLE AS NOM_FAMILLE,  i.MATRICULE AS MATRICULE, i.NTT AS NTT, "

                + "(SELECT CONCAT(@DATE_DEBUT_CONTRAT:=MIN(vctr.DATE_DEBUT_CONTRAT), @DATE_FIN_PREVISIONNELLE:=MAX(vctr.DATE_FIN_PREVISIONNELLE)) "
                + " FROM V_ctr_export_individus vctr WHERE vctr.ID_PERIODE= :" + PARAM_ID_PERIODE
                + " AND vctr.IDENTIFIANT_REPERTOIRE=i.IDENTIFIANT_REPERTOIRE AND vctr.NTT=i.NTT "
                + " GROUP BY vctr.ID_PERIODE , vctr.IDENTIFIANT_REPERTOIRE , vctr.NTT),"
                + " @DATE_DEBUT_CONTRAT AS DATE_DEBUT_CONTRAT,@DATE_FIN_PREVISIONNELLE AS DATE_FIN_PREVISIONNELLE "

                + " FROM AFFILIATION a  LEFT JOIN CONTRAT_TRAVAIL ct ON ct.ID_CONTRAT_TRAVAIL = a.ID_CONTRAT_TRAVAIL  AND ct.TMP_BATCH = a.TMP_BATCH "
                + " LEFT JOIN INDIVIDU i ON i.ID_INDIVIDU = ct.ID_INDIVIDU AND i.TMP_BATCH = ct.TMP_BATCH "
                + " LEFT JOIN ADHESION_ETABLISSEMENT_MOIS adh ON adh.ID_ADH_ETAB_MOIS = i.ID_ADH_ETAB_MOIS AND adh.TMP_BATCH = i.TMP_BATCH "
                + " WHERE adh.TMP_BATCH IS FALSE AND adh.ID_ADH_ETAB_MOIS IN "
                + " (SELECT r.ID_ADH_ETAB_MOIS FROM RATTACHEMENT_DECLARATIONS_RECUES r WHERE r.ID_PERIODE = :" + PARAM_ID_PERIODE + ")) AS _AFFILS"
                + " GROUP BY CODE_POPULATION, DATE_DEBUT_CONTRAT, DATE_FIN_PREVISIONNELLE,  NOM_USAGE, PRENOM, MATRICULE, COALESCE(IF(IDENTIFIANT_REPERTOIRE='',NULL,IDENTIFIANT_REPERTOIRE),NTT) "
                + " ORDER BY NOM_FAMILLE,NOM_USAGE, PRENOM, IDENTIFIANT_REPERTOIRE, NTT, DATE_DEBUT_CONTRAT LIMIT :" + PARAM_TAILLE_PAGE + " OFFSET :"
                + PARAM_INDEX_PREMIER_RESULTAT;
        Map<String, Object> params = new HashMap<>();
        params.put(PARAM_INDEX_PREMIER_RESULTAT, indexPremierResultat);
        params.put(PARAM_TAILLE_PAGE, taillePage);
        params.put(PARAM_ID_PERIODE, idPeriode);
        return getJdbcTemplate().query(sql, params, new ResumeAffiliationRowMapper());
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public List<ResumeAffiliationExportExcel> getResumesAffiliationsDistinctesRattachesPaginesExportExcel(Long idPeriode, Integer dateDebutPeriode,
            Integer dateFinPeriode, Integer indexPremierResultat, Integer taillePage) {
        // On SUM les informations par individu (plusieurs affiliations / indiv, ex : 3 affil / indiv sur un contrat trimestriel )
        String sql = "SELECT DISTINCT MOTIF_RUPTURE, CODE_POPULATION,  DATE_DEBUT_CONTRAT, DATE_FIN_PREVISIONNELLE, DATE_DEBUT_AFFILIATION, DATE_FIN_AFFILIATION, "
                + " IDENTIFIANT_REPERTOIRE, NOM_FAMILLE, NOM_USAGE, PRENOM, SEXE,"
                + " (CASE WHEN ((p.DATE_DEBUT_PERIODE <= coalesce(DATE_DEBUT_CONTRAT, 0) AND coalesce(DATE_DEBUT_CONTRAT, 0) <= p.DATE_FIN_PERIODE) "
                + " OR (p.DATE_DEBUT_PERIODE  <= coalesce(DATE_FIN_PREVISIONNELLE, 0)  AND coalesce(DATE_FIN_PREVISIONNELLE, 0) <= p.DATE_FIN_PERIODE)) "
                + " THEN 'OUI'  ELSE 'NON' END) AS CHGT_PERIODE, LIB_POPULATION, MATRICULE, NTT, DATE_NAISSANCE, "
                + " SUM(MT_BRANCHE_1) AS MONTANT_TRANCHE_1," + " SUM(MT_BRANCHE_2) AS MONTANT_TRANCHE_2," + " SUM(MT_BRANCHE_3) AS MONTANT_TRANCHE_3,"
                + " SUM(MT_BRANCHE_4) AS MONTANT_TRANCHE_4," + " MAX(MOIS_RATTACHEMENT) AS MAX_MOIS_RATTACHEMENT,"
                + " SUM(MTCBA_COL1) AS MTCBA_COL1, SUM(MTCBA_COL2) AS MTCBA_COL2, SUM(MTCBA_COL3) AS MTCBA_COL3, SUM(MTCBA_COL4) AS MTCBA_COL4, "
                + " SUM(MTCBA_COL5) AS MTCBA_COL5, SUM(MTCBA_COL6) AS MTCBA_COL6, SUM(MTCBA_COL7) AS MTCBA_COL7, SUM(MTCBA_COL8) AS MTCBA_COL8, "
                + " SUM(MTCBA_COL9) AS MTCBA_COL9, SUM(MTCBA_COL10) AS MTCBA_COL10, SUM(MTCBA_COL11) AS MTCBA_COL11, SUM(MTCBA_COL12) AS MTCBA_COL12,"
                + " SUM(MTCBA_COL13) AS MTCBA_COL13," + " SUM(SOMME_MONTANTS_COTISATION) AS SOMME_MONTANTS_COTISATION "

                // SR 1
                // Permet d'extraire les informations de la SR 1.2 en aplanissant (via les MAX) sur une ligne les informations de
                // l'individu (voir explications au niveau des GROUP BY) , pour une affiliation donnée
                // note 1 : une seule des lignes des MTCBA contiendra une valeur de cotisation (car sum conditionnelles ) pour un composant_base_assujetie
                // donné.

                // note 2 : la somme mt_cotisation est démultipliée sur chaque ligne ( car décomposition au niveau cba quand on somme au niveau ba dans SR 1.2),
                // le max selectionne une unique valeur ici.

                + " FROM (SELECT MOTIF_RUPTURE, CODE_POPULATION,  DATE_DEBUT_CONTRAT, DATE_FIN_PREVISIONNELLE,"
                + " DATE_DEBUT_AFFILIATION, DATE_FIN_AFFILIATION, "
                + "IDENTIFIANT_REPERTOIRE, NOM_FAMILLE, NOM_USAGE, PRENOM, SEXE, LIB_POPULATION, MATRICULE, NTT, DATE_NAISSANCE,  "
                + "MAX(MT_BRANCHE_1) AS MT_BRANCHE_1, MAX(MT_BRANCHE_2) AS MT_BRANCHE_2, MAX(MT_BRANCHE_3) AS MT_BRANCHE_3, "
                + "MAX(MT_BRANCHE_4) AS MT_BRANCHE_4, MOIS_RATTACHEMENT AS MOIS_RATTACHEMENT, "
                + "MAX(MTCBA_COL1) AS MTCBA_COL1, MAX(MTCBA_COL2) AS MTCBA_COL2, MAX(MTCBA_COL3) AS MTCBA_COL3, MAX(MTCBA_COL4) AS MTCBA_COL4,  "
                + "MAX(MTCBA_COL5) AS MTCBA_COL5, MAX(MTCBA_COL6) AS MTCBA_COL6, MAX(MTCBA_COL7) AS MTCBA_COL7, MAX(MTCBA_COL8) AS MTCBA_COL8,  "
                + "MAX(MTCBA_COL9) AS MTCBA_COL9, MAX(MTCBA_COL10) AS MTCBA_COL10, MAX(MTCBA_COL11) AS MTCBA_COL11, MAX(MTCBA_COL12) AS MTCBA_COL12, "
                + "MAX(MTCBA_COL13) AS MTCBA_COL13, " + "MAX(SOMME_MONTANTS_COTISATION) AS SOMME_MONTANTS_COTISATION "

                // SR 1.2
                // les informations de chaque individu sont reparties sur n lignes ( 1 ligne par composant_base_assujetie associé à une base assujetie,
                // elle même associé à une affiliation
                + " FROM ("

                // info contrat travail / indiv
                // on peut avoir plusieurs motif ruptures - choix du motif le plus récent + limit 1 (plusieurs lignes peuvent être match sur la valeur max )
                + "SELECT @DATE_DEBUT_CONTRAT:=null,@DATE_FIN_PREVISIONNELLE:=null,@MOTIF_RUPTURE:=null, "
                + " (SELECT CONCAT(@DATE_DEBUT_CONTRAT:=MIN(vctra.DATE_DEBUT_CONTRAT), @DATE_FIN_PREVISIONNELLE:=MAX(vctra.DATE_FIN_PREVISIONNELLE),"
                + " @MOTIF_RUPTURE:=(SELECT vctrb.MOTIF_RUPTURE FROM V_ctr_export_individus vctrb "
                + " WHERE ((vctrb.ID_PERIODE = vctra.ID_PERIODE) AND (vctrb.IDENTIFIANT_REPERTOIRE = vctra.IDENTIFIANT_REPERTOIRE) "
                + " AND (vctrb.NTT = vctra.NTT) AND (vctrb.MOTIF_RUPTURE <> ''))"
                + " ORDER BY vctrb.DATE_FIN_PREVISIONNELLE DESC , vctrb.ID_CONTRAT_TRAVAIL DESC LIMIT 1))"
                + " FROM V_ctr_export_individus vctra WHERE vctra.ID_PERIODE= :" + PARAM_ID_PERIODE
                + " AND vctra.IDENTIFIANT_REPERTOIRE= i.IDENTIFIANT_REPERTOIRE AND vctra.NTT= i.NTT "
                + " GROUP BY vctra.ID_PERIODE , vctra.IDENTIFIANT_REPERTOIRE , vctra.NTT),"

                + " @DATE_DEBUT_CONTRAT AS DATE_DEBUT_CONTRAT,@DATE_FIN_PREVISIONNELLE AS DATE_FIN_PREVISIONNELLE,@MOTIF_RUPTURE AS MOTIF_RUPTURE, "

                + " a.CODE_POPULATION AS CODE_POPULATION, a.DATE_DEBUT_AFFILIATION AS DATE_DEBUT_AFFILIATION, a.DATE_FIN_AFFILIATION AS DATE_FIN_AFFILIATION,"
                + SQL_PARTIE_REQUETE_TARIF_LICAT + PARAM_ID_PERIODE + ") AS LIB_POPULATION, "
                + " i.IDENTIFIANT_REPERTOIRE AS IDENTIFIANT_REPERTOIRE, i.NOM_FAMILLE AS NOM_FAMILLE, i.NOM_USAGE AS NOM_USAGE, i.PRENOM AS PRENOM, i.SEXE AS SEXE,"
                + " i.MATRICULE AS MATRICULE, i.NTT AS NTT, i.DATE_NAISSANCE AS DATE_NAISSANCE, adh.MOIS_RATTACHEMENT AS MOIS_RATTACHEMENT, "

                // SUM des MT_BRANCHE par tranche composant base assujetie
                + " (CASE WHEN (tcba.NUM_TRANCHE = 1) THEN SUM(tcba.MT_TRANCHE) ELSE NULL END) AS MT_BRANCHE_1,"
                + " (CASE WHEN (tcba.NUM_TRANCHE = 2) THEN SUM(tcba.MT_TRANCHE) ELSE NULL END) AS MT_BRANCHE_2,"
                + " (CASE WHEN (tcba.NUM_TRANCHE = 3) THEN SUM(tcba.MT_TRANCHE) ELSE NULL END) AS MT_BRANCHE_3,"
                + " (CASE WHEN (tcba.NUM_TRANCHE = 4) THEN SUM(tcba.MT_TRANCHE) ELSE NULL END) AS MT_BRANCHE_4,"
                + " tcba.ID_PERIODE as ID_PERIODE,tcba.ID_AFFILIATION as ID_AFFILIATION_TCBA, ba.ID_AFFILIATION as ID_AFFILIATION_BA,c.TYPE_COMPO_BASE_ASSUJ,"

                // SUM des MTCBA par base assujetie pour l'individu
                + " (CASE WHEN (c.TYPE_COMPO_BASE_ASSUJ = 10) THEN SUM(c.MONTANT_COMPO_BASE_ASSUJ) ELSE NULL END) AS MTCBA_COL1,"
                + " (CASE WHEN (c.TYPE_COMPO_BASE_ASSUJ = 11) THEN SUM(c.MONTANT_COMPO_BASE_ASSUJ) ELSE NULL END) AS MTCBA_COL2,"
                + " (CASE WHEN (c.TYPE_COMPO_BASE_ASSUJ = 12) THEN SUM(c.MONTANT_COMPO_BASE_ASSUJ) ELSE NULL END) AS MTCBA_COL3,"
                + " (CASE WHEN (c.TYPE_COMPO_BASE_ASSUJ = 13) THEN SUM(c.MONTANT_COMPO_BASE_ASSUJ) ELSE NULL END) AS MTCBA_COL4,"
                + " (CASE WHEN (c.TYPE_COMPO_BASE_ASSUJ = 14) THEN SUM(c.MONTANT_COMPO_BASE_ASSUJ) ELSE NULL END) AS MTCBA_COL5,"
                + " (CASE WHEN (c.TYPE_COMPO_BASE_ASSUJ = 15) THEN SUM(c.MONTANT_COMPO_BASE_ASSUJ) ELSE NULL END) AS MTCBA_COL6,"
                + " (CASE WHEN (c.TYPE_COMPO_BASE_ASSUJ = 16) THEN SUM(c.MONTANT_COMPO_BASE_ASSUJ) ELSE NULL END) AS MTCBA_COL7,"
                + " (CASE WHEN (c.TYPE_COMPO_BASE_ASSUJ = 17) THEN SUM(c.MONTANT_COMPO_BASE_ASSUJ) ELSE NULL END) AS MTCBA_COL8,"
                + " (CASE WHEN (c.TYPE_COMPO_BASE_ASSUJ = 18) THEN SUM(c.MONTANT_COMPO_BASE_ASSUJ) ELSE NULL END) AS MTCBA_COL9,"
                + " (CASE WHEN (c.TYPE_COMPO_BASE_ASSUJ = 19) THEN SUM(c.MONTANT_COMPO_BASE_ASSUJ) ELSE NULL END) AS MTCBA_COL10,"
                + " (CASE WHEN (c.TYPE_COMPO_BASE_ASSUJ = 20) THEN SUM(c.MONTANT_COMPO_BASE_ASSUJ) ELSE NULL END) AS MTCBA_COL11,"
                + " (CASE WHEN (c.TYPE_COMPO_BASE_ASSUJ = 21) THEN SUM(c.MONTANT_COMPO_BASE_ASSUJ) ELSE NULL END) AS MTCBA_COL12,"
                + " (CASE WHEN (c.TYPE_COMPO_BASE_ASSUJ = 24) THEN SUM(c.MONTANT_COMPO_BASE_ASSUJ) ELSE NULL END) AS MTCBA_COL13,"
                + " SUM(ba.MONTANT_COTISATION) AS SOMME_MONTANTS_COTISATION "

                + " FROM AFFILIATION a "
                + " LEFT JOIN CONTRAT_TRAVAIL ct ON ct.ID_CONTRAT_TRAVAIL = a.ID_CONTRAT_TRAVAIL AND ct.TMP_BATCH = a.TMP_BATCH "
                + " LEFT JOIN INDIVIDU i ON i.ID_INDIVIDU = ct.ID_INDIVIDU AND i.TMP_BATCH = ct.TMP_BATCH "
                + " LEFT JOIN TRANCHE_CATEGORIE_BASE_ASSUJETTIE tcba on tcba.ID_PERIODE = :" + PARAM_ID_PERIODE
                + " AND tcba.ID_AFFILIATION=a.ID_AFFILIATION LEFT JOIN BASE_ASSUJETTIE ba ON ba.ID_AFFILIATION=a.ID_AFFILIATION "
                + " INNER JOIN COMPOSANT_BASE_ASSUJETTIE c ON c.ID_BASE_ASSUJETTIE = ba.ID_BASE_ASSUJETTIE "
                + SQL_JOINTURE_ADHESION_ETABLISSEMENT_MOIS_POUR_INDIVIDU + SQL_CLAUSE_ADHESION_ETABLISSEMENT_MOIS_RATTACHEE_A_PERIODE

                // regroupement pour calcul des SUM pour MTCBA et MT_BRANCHE , pour l'affiliation courante
                + " GROUP BY tcba.ID_PERIODE , ID_AFFILIATION_TCBA,  tcba.NUM_TRANCHE, ID_AFFILIATION_BA,c.TYPE_COMPO_BASE_ASSUJ ) AS _AFFILS "

                // regroupement necessaire pour application correct des MAX sur les MTCBA et MT_BRANCHE afin d'obtenir une ligne par affiliation
                // la SR 1.2 décompose les informations de l'individu par composant_base_assujetie , on les regroupe ici sur une unique ligne.
                + " GROUP BY _AFFILS.ID_PERIODE, _AFFILS.ID_AFFILIATION_BA, _AFFILS.ID_AFFILIATION_TCBA ) AS _AGG ,"
                + " PERIODES_RECUES p  WHERE p.ID_PERIODE= :" + PARAM_ID_PERIODE

                // regroupement de la requête principale
                + " GROUP BY MOTIF_RUPTURE, CODE_POPULATION, IDENTIFIANT_REPERTOIRE, DATE_DEBUT_CONTRAT, DATE_FIN_PREVISIONNELLE, LIB_POPULATION, NOM_FAMILLE, NOM_USAGE, PRENOM, SEXE, MATRICULE, NTT, DATE_NAISSANCE"
                + " ORDER BY NOM_FAMILLE, NOM_USAGE, PRENOM, IDENTIFIANT_REPERTOIRE, NTT, DATE_DEBUT_CONTRAT LIMIT :" + PARAM_TAILLE_PAGE
                + " OFFSET :" + PARAM_INDEX_PREMIER_RESULTAT;
        Map<String, Object> params = new HashMap<>();
        params.put(PARAM_INDEX_PREMIER_RESULTAT, indexPremierResultat);
        params.put(PARAM_TAILLE_PAGE, taillePage);
        params.put(PARAM_ID_PERIODE, idPeriode);
        return getJdbcTemplate().query(sql, params, new ResumeAffiliationExportExcelRowMapper());
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public List<CotisationEtablissement> getCotisationsEtablissementsRattachees(Long idPeriode) {
        String jpql = "SELECT c FROM CotisationEtablissement c LEFT JOIN c.adhesionEtablissementMois adh WHERE "
                + JPQL_CLAUSE_ADHESION_ETABLISSEMENT_MOIS_RATTACHEE_A_PERIODE;
        TypedQuery<CotisationEtablissement> query = getEntityManager().createQuery(jpql, CotisationEtablissement.class);
        query.setParameter(PARAM_ID_PERIODE, idPeriode);
        return query.getResultList();
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public long compteAffiliationsRattachees(Long idPeriode) {
        String jpql = "SELECT COUNT(a) FROM Affiliation a LEFT JOIN a.contratTravail ct LEFT JOIN ct.individu i LEFT JOIN i.adhesionEtablissementMois adh WHERE "
                + JPQL_CLAUSE_ADHESION_ETABLISSEMENT_MOIS_RATTACHEE_A_PERIODE;
        TypedQuery<Long> query = getEntityManager().createQuery(jpql, Long.class);
        query.setParameter(PARAM_ID_PERIODE, idPeriode);
        return getPremierResultatSiExiste(query.getResultList());
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public long compteFinsDeContratsRattachees(Long idPeriode) {
        String jpql = "SELECT COUNT (c.numeroContratTravail) FROM ContratTravail c LEFT JOIN c.individu i LEFT JOIN i.adhesionEtablissementMois adh WHERE "
                + "c.dateFinContrat IS NOT NULL AND c.dateFinContrat <> 0 AND " + JPQL_CLAUSE_ADHESION_ETABLISSEMENT_MOIS_RATTACHEE_A_PERIODE;
        TypedQuery<Long> query = getEntityManager().createQuery(jpql, Long.class);
        query.setParameter(PARAM_ID_PERIODE, idPeriode);
        return getPremierResultatSiExiste(query.getResultList());
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public List<ResumeChangementIndividu> getChangementsIndividuRattache(Long idPeriode, String identifiantRepertoire,
            String numeroTechniqueTemporaire) {

        String sql = "SELECT ci.DATE_MODIFICATION, ci.ANCIEN_IDENTIFIANT, ci.ANCIEN_NOM_FAMILLE, ci.ANCIEN_PRENOM, ci.ANCIENNE_DATE_NAISSANCE "
                + " FROM CHANGEMENT_INDIVIDU ci LEFT JOIN INDIVIDU i ON ci.ID_INDIVIDU = i.ID_INDIVIDU "
                + " LEFT JOIN ADHESION_ETABLISSEMENT_MOIS adh ON i.ID_ADH_ETAB_MOIS = adh.ID_ADH_ETAB_MOIS " + " WHERE i.IDENTIFIANT_REPERTOIRE = :"
                + PARAM_IDENTIFIANT_REPERTOIRE + " AND i.NTT = :" + PARAM_NUMERO_TECHNIQUE_TEMPORAIRE + " AND adh.TMP_BATCH IS FALSE"
                + " AND adh.ID_ADH_ETAB_MOIS IN (SELECT " + "r.ID_ADH_ETAB_MOIS FROM RATTACHEMENT_DECLARATIONS_RECUES r WHERE r.ID_PERIODE = :"
                + PARAM_ID_PERIODE + ")";

        Map<String, Object> params = new HashMap<>();
        params.put(PARAM_ID_PERIODE, idPeriode);
        params.put(PARAM_IDENTIFIANT_REPERTOIRE, identifiantRepertoire);
        params.put(PARAM_NUMERO_TECHNIQUE_TEMPORAIRE, numeroTechniqueTemporaire);

        return getJdbcTemplate().query(sql, params, new ResumeChangementIndividuRowMapper());

    }

    /**
     * Mapper de lignes de résumés d'affiliations.
     * 
     * @author nortaina
     *
     */
    public static class ResumeChangementIndividuRowMapper implements RowMapper<ResumeChangementIndividu> {
        @Override
        public ResumeChangementIndividu mapRow(ResultSet rs, int rowNum) throws SQLException {
            ResumeChangementIndividu changement = new ResumeChangementIndividu();
            changement.setDateModification(rs.getInt("DATE_MODIFICATION"));
            changement.setAncienIdentifiant(rs.getString("ANCIEN_IDENTIFIANT"));
            changement.setAncienNomFamille(rs.getString("ANCIEN_NOM_FAMILLE"));
            changement.setAncienPrenom(rs.getString("ANCIEN_PRENOM"));
            changement.setAncienneDateNaissance(rs.getInt("ANCIENNE_DATE_NAISSANCE"));
            return changement;
        }
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public String getColonneChangementsIndividuRattacheString(Long idPeriode, String nomColonne, String identifiantRepertoire,
            String numeroTechniqueTemporaire) {
        StringBuilder jpql = new StringBuilder(
                "SELECT ci." + nomColonne + " FROM ChangementIndividu ci LEFT JOIN ci.individu i LEFT JOIN i.adhesionEtablissementMois adh WHERE ");
        if (!StringUtils.isBlank(identifiantRepertoire)) {
            jpql.append(JPQL_CLAUSE_IDENTIFIANT_REPERTOIRE);
        } else {
            jpql.append(JPQL_CLAUSE_NUMERO_TECHNIQUE_TEMPORAIRE);
        }
        jpql.append(JPQL_CLAUSE_ADHESION_ETABLISSEMENT_MOIS_RATTACHEE_A_PERIODE);

        // Prendre en compte seulement les colonnes String lesquelles ont des valeurs rensegnees
        jpql.append(" AND (coalesce(ci." + nomColonne + ",'') <> '')");
        jpql.append(" ORDER BY ci.dateModification ");
        TypedQuery<String> query = getEntityManager().createQuery(jpql.toString(), String.class);
        query.setParameter(PARAM_ID_PERIODE, idPeriode);
        if (!StringUtils.isBlank(identifiantRepertoire)) {
            query.setParameter(PARAM_IDENTIFIANT_REPERTOIRE, identifiantRepertoire);
        } else {
            query.setParameter(PARAM_NUMERO_TECHNIQUE_TEMPORAIRE, numeroTechniqueTemporaire);
        }
        return getPremierResultatSiExiste(query.getResultList());
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public Integer getColonneChangementsIndividuRattacheInteger(Long idPeriode, String nomColonne, String identifiantRepertoire,
            String numeroTechniqueTemporaire) {
        StringBuilder jpql = new StringBuilder(
                "SELECT ci." + nomColonne + " FROM ChangementIndividu ci LEFT JOIN ci.individu i LEFT JOIN i.adhesionEtablissementMois adh WHERE ");
        if (!StringUtils.isBlank(identifiantRepertoire)) {
            jpql.append(JPQL_CLAUSE_IDENTIFIANT_REPERTOIRE);
        } else {
            jpql.append(JPQL_CLAUSE_NUMERO_TECHNIQUE_TEMPORAIRE);
        }
        jpql.append(JPQL_CLAUSE_ADHESION_ETABLISSEMENT_MOIS_RATTACHEE_A_PERIODE);

        // Prendre en compte seulement les colonnes Integer lesquelles ont des valeurs renseignees
        jpql.append(" AND ci." + nomColonne + " > 0");
        jpql.append(" ORDER BY ci.dateModification ");
        TypedQuery<Integer> query = getEntityManager().createQuery(jpql.toString(), Integer.class);
        query.setParameter(PARAM_ID_PERIODE, idPeriode);
        if (!StringUtils.isBlank(identifiantRepertoire)) {
            query.setParameter(PARAM_IDENTIFIANT_REPERTOIRE, identifiantRepertoire);
        } else {
            query.setParameter(PARAM_NUMERO_TECHNIQUE_TEMPORAIRE, numeroTechniqueTemporaire);
        }

        return getPremierResultatSiExiste(query.getResultList());
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public String getPeriodeAffectationComposantVersementRattacheInvalide(Long idPeriode, Integer dateDebutPeriode, Integer dateFinPeriode) {
        String jpql = "SELECT c.periodeAffectation FROM ComposantVersement c LEFT JOIN c.versement v LEFT JOIN v.adhesionEtablissementMois adh "
                + "WHERE (coalesce(c.dateDebutPeriodeAffectation,'') <> :dateDebutPeriode OR coalesce(c.dateFinPeriodeAffectation,'') <> :dateFinPeriode) AND "
                + " c.periodeAffectation NOT LIKE '____E__' AND " + JPQL_CLAUSE_ADHESION_ETABLISSEMENT_MOIS_RATTACHEE_A_PERIODE;
        TypedQuery<String> query = getEntityManager().createQuery(jpql, String.class);
        query.setParameter(PARAM_ID_PERIODE, idPeriode);
        query.setParameter("dateDebutPeriode", dateDebutPeriode);
        query.setParameter("dateFinPeriode", dateFinPeriode);
        return getPremierResultatSiExiste(query.getResultList());
    }

    @Override
    public int detacheAdhesionsDernierLotRattachementPourPeriode(Long idPeriode) {
        String jpql = "DELETE FROM RattachementDeclarationsRecues r WHERE r.idPeriode = :idPeriode AND SUBSTRING(r.idAdhEtabMois, 1, 14) = (select ht.horodatage from HorodatageTraitement ht WHERE ht.auditDateCreation = (select MAX(ht2.auditDateCreation) from HorodatageTraitement ht2))";
        Query query = getEntityManager().createQuery(jpql).setParameter("idPeriode", idPeriode);
        return query.executeUpdate();

    }

    @Override
    public int detacheAdhesionsDernierLotRattachementEnMasse(Set<Long> idsPeriodes) {
        String jpql = "DELETE FROM RattachementDeclarationsRecues r WHERE r.idPeriode in (:idsPeriodes) AND SUBSTRING(r.idAdhEtabMois, 1, 14) = (select ht.horodatage from HorodatageTraitement ht WHERE ht.auditDateCreation = (select MAX(ht2.auditDateCreation) from HorodatageTraitement ht2))";
        Query query = getEntityManager().createQuery(jpql).setParameter("idsPeriodes", idsPeriodes);
        return query.executeUpdate();

    }

    /**
     * Mapper de lignes de résumés d'affiliations.
     * 
     * @author nortaina
     *
     */
    public static class ResumeAffiliationExportExcelRowMapper implements RowMapper<ResumeAffiliationExportExcel> {
        @Override
        public ResumeAffiliationExportExcel mapRow(ResultSet rs, int rowNum) throws SQLException {
            ResumeAffiliationExportExcel affiliation = new ResumeAffiliationExportExcel();
            affiliation.setMotifRupture(rs.getString("MOTIF_RUPTURE"));
            affiliation.setCodePopulation(rs.getString(ROW_CODE_POPULATION));
            affiliation.setDateDebutContrat(getInteger(rs, ROW_DATE_DEBUT_CONTRAT));
            affiliation.setDateFinPrevisionnelle(getInteger(rs, ROW_DATE_FIN_PREVISIONNELLE));
            affiliation.setIdentifiantRepertoire(rs.getString(ROW_IDENTIFIANT_REPERTOIRE));
            affiliation.setNomFamille(rs.getString(ROW_NOM_FAMILLE));
            affiliation.setNomUsage(rs.getString(ROW_NOM_USAGE));
            affiliation.setPrenom(rs.getString(ROW_PRENOM));
            affiliation.setSexe(rs.getString("SEXE"));
            affiliation.setMatricule(rs.getString(ROW_MATRICULE));
            affiliation.setNumeroTechniqueTemporaire(rs.getString("NTT"));
            affiliation.setDateNaissance(getInteger(rs, "DATE_NAISSANCE"));
            affiliation.setMontantTranche1(rs.getDouble("MONTANT_TRANCHE_1"));
            affiliation.setMontantTranche2(rs.getDouble("MONTANT_TRANCHE_2"));
            affiliation.setMontantTranche3(rs.getDouble("MONTANT_TRANCHE_3"));
            affiliation.setMontantTranche4(rs.getDouble("MONTANT_TRANCHE_4"));
            affiliation.setMontantCBA10(rs.getDouble("MTCBA_COL1"));
            affiliation.setMontantCBA11(rs.getDouble("MTCBA_COL2"));
            affiliation.setMontantCBA12(rs.getDouble("MTCBA_COL3"));
            affiliation.setMontantCBA13(rs.getDouble("MTCBA_COL4"));
            affiliation.setMontantCBA14(rs.getDouble("MTCBA_COL5"));
            affiliation.setMontantCBA15(rs.getDouble("MTCBA_COL6"));
            affiliation.setMontantCBA16(rs.getDouble("MTCBA_COL7"));
            affiliation.setMontantCBA17(rs.getDouble("MTCBA_COL8"));
            affiliation.setMontantCBA18(rs.getDouble("MTCBA_COL9"));
            affiliation.setMontantCBA19(rs.getDouble("MTCBA_COL10"));
            affiliation.setMontantCBA20(rs.getDouble("MTCBA_COL11"));
            affiliation.setMontantCBA21(rs.getDouble("MTCBA_COL12"));
            affiliation.setMontantCBA24(rs.getDouble("MTCBA_COL13"));
            affiliation.setChgtPeriode(rs.getString("CHGT_PERIODE"));
            affiliation.setLibPopulation(rs.getString("LIB_POPULATION"));
            affiliation.setMoisRattachement(getInteger(rs, "MAX_MOIS_RATTACHEMENT"));
            affiliation.setDateDebutAffiliation(getInteger(rs, ROW_DATE_DEBUT_AFFILIATION));
            affiliation.setDateFinAffiliation(getInteger(rs, ROW_DATE_FIN_AFFILIATION));
            affiliation.setSommeMontantsCotisation(rs.getDouble(ROW_SOMME_MONTANTS_COTISATION));
            return affiliation;
        }
    }

    /**
     * Mapper de lignes de résumés d'affiliations.
     * 
     * @author nortaina
     *
     */
    public static class ResumeAffiliationExportExcelAbMessage implements RowMapper<ResumeAffiliationExportExcelAllegesMessage> {
        @Override
        public ResumeAffiliationExportExcelAllegesMessage mapRow(ResultSet rs, int rowNum) throws SQLException {
            ResumeAffiliationExportExcelAllegesMessage affiliation = new ResumeAffiliationExportExcelAllegesMessage();
            affiliation.setDateDebutContrat(getInteger(rs, ROW_DATE_DEBUT_CONTRAT));
            affiliation.setDateFinPrevisionnelle(getInteger(rs, ROW_DATE_FIN_PREVISIONNELLE));
            affiliation.setIdentifiantRepertoire(rs.getString(ROW_IDENTIFIANT_REPERTOIRE));
            affiliation.setNomFamille(rs.getString(ROW_NOM_FAMILLE));
            affiliation.setNomUsage(rs.getString(ROW_NOM_USAGE));
            affiliation.setPrenom(rs.getString(ROW_PRENOM));
            affiliation.setSexe(rs.getString("SEXE"));
            affiliation.setMatricule(rs.getString(ROW_MATRICULE));
            affiliation.setNumeroTechniqueTemporaire(rs.getString("NTT"));
            affiliation.setDateNaissance(getInteger(rs, "DATE_NAISSANCE"));
            affiliation.setSignalementUtilisateur(rs.getString("Signalements"));
            affiliation.setRejetUtilisateur(rs.getString("Rejets"));
            return affiliation;
        }
    }

    /**
     * Mapper de lignes de résumés d'affiliations.
     * 
     * @author nortaina
     *
     */
    public static class ResumeAffiliationExportExcelAbMontant implements RowMapper<ResumeAffiliationExportExcelAllegesMontant> {
        @Override
        public ResumeAffiliationExportExcelAllegesMontant mapRow(ResultSet rs, int rowNum) throws SQLException {
            ResumeAffiliationExportExcelAllegesMontant affiliation = new ResumeAffiliationExportExcelAllegesMontant();
            affiliation.setDateDebutContrat(getInteger(rs, ROW_DATE_DEBUT_CONTRAT));
            affiliation.setDateFinPrevisionnelle(getInteger(rs, ROW_DATE_FIN_PREVISIONNELLE));
            affiliation.setIdentifiantRepertoire(rs.getString(ROW_IDENTIFIANT_REPERTOIRE));
            affiliation.setNomFamille(rs.getString(ROW_NOM_FAMILLE));
            affiliation.setNomUsage(rs.getString(ROW_NOM_USAGE));
            affiliation.setPrenom(rs.getString(ROW_PRENOM));
            affiliation.setSexe(rs.getString("SEXE"));
            affiliation.setMatricule(rs.getString(ROW_MATRICULE));
            affiliation.setNumeroTechniqueTemporaire(rs.getString("NTT"));
            affiliation.setDateNaissance(getInteger(rs, "DATE_NAISSANCE"));
            affiliation.setMontantCot(rs.getDouble("MT_COTISATION"));
            affiliation.setMontantCotisationExistant(!rs.wasNull());
            affiliation.setCodePopulation(rs.getString("NOCAT"));
            affiliation.setLibPopulation(rs.getString("LICAT"));
            return affiliation;
        }
    }

    /**
     * Mapper de lignes de résumés d'affiliations.
     * 
     * @author nortaina
     *
     */
    public static class ResumeAffiliationRowMapper implements RowMapper<ResumeAffiliation> {
        @Override
        public ResumeAffiliation mapRow(ResultSet rs, int rowNum) throws SQLException {
            ResumeAffiliation affiliation = new ResumeAffiliation();
            affiliation.setCodePopulation(rs.getString(ROW_CODE_POPULATION));
            affiliation.setDateDebutContrat(getInteger(rs, ROW_DATE_DEBUT_CONTRAT));
            affiliation.setDateFinPrevisionnelle(getInteger(rs, ROW_DATE_FIN_PREVISIONNELLE));
            affiliation.setIdentifiantRepertoire(rs.getString(ROW_IDENTIFIANT_REPERTOIRE));
            affiliation.setNomUsage(rs.getString(ROW_NOM_USAGE));
            affiliation.setPrenom(rs.getString(ROW_PRENOM));
            affiliation.setMatricule(rs.getString(ROW_MATRICULE));
            affiliation.setNumeroTechniqueTemporaire(rs.getString("NTT"));
            affiliation.setNomFamille(rs.getString(ROW_NOM_FAMILLE));
            return affiliation;
        }
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public boolean existeRattachementDeclarationRecue(Long idPeriode) {
        String jpql = "SELECT COUNT(*) FROM RattachementDeclarationsRecues rdr WHERE rdr.idPeriode= :idPeriode";
        TypedQuery<Long> query = getEntityManager().createQuery(jpql, Long.class);
        query.setParameter(PARAM_ID_PERIODE, idPeriode);
        return query.getSingleResult() > 0;

    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public List<ExportIndividuExcel> getDistinctIndividusTriesRattachesPagines(Long idPeriode, Integer indexPremierResultat, Integer taillePage) {
        String sql = "SELECT i.NOM_FAMILLE,i.NOM_USAGE,i.PRENOM,i.IDENTIFIANT_REPERTOIRE,i.NTT,i.MATRICULE, COALESCE(IF(IDENTIFIANT_REPERTOIRE='',NULL,IDENTIFIANT_REPERTOIRE),NTT) AS INDIVIDU, "
                + " (SELECT coalesce(CAST(GROUP_CONCAT(mc.MESSAGE_UTILISATEUR SEPARATOR ' ,\n ' ) AS CHAR),'') FROM MESSAGE_CONTROLE mc "
                + " WHERE mc.ID_PERIODE = :idPeriode AND mc.NIVEAU_ALERTE = :alertSignal AND mc.INDIVIDU = COALESCE(IF(IDENTIFIANT_REPERTOIRE='',NULL,IDENTIFIANT_REPERTOIRE),NTT)) AS CONCAT_MSG_ALERT_SIGNAL, "
                + " (SELECT coalesce(CAST(GROUP_CONCAT(mc.MESSAGE_UTILISATEUR SEPARATOR ' ,\n ' ) AS CHAR),'') FROM MESSAGE_CONTROLE mc "
                + " WHERE mc.ID_PERIODE = :idPeriode AND mc.NIVEAU_ALERTE = :alertRejet AND mc.INDIVIDU = COALESCE(IF(IDENTIFIANT_REPERTOIRE='',NULL,IDENTIFIANT_REPERTOIRE),NTT)) AS CONCAT_MSG_ALERT_REJET, "
                + " (SELECT SUM(c.MT_COTISATION) FROM CATEGORIE_QUITTANCEMENT_INDIVIDU c WHERE c.ID_PERIODE = :idPeriode "
                + " AND c.INDIVIDU = COALESCE(IF(IDENTIFIANT_REPERTOIRE='',NULL,IDENTIFIANT_REPERTOIRE),NTT)) AS SUM_COTISATION  "
                + " FROM INDIVIDU i  LEFT JOIN  ADHESION_ETABLISSEMENT_MOIS adh ON i.ID_ADH_ETAB_MOIS = adh.ID_ADH_ETAB_MOIS "
                + " WHERE adh.TMP_BATCH IS FALSE AND  adh.ID_ADH_ETAB_MOIS IN (SELECT r.ID_ADH_ETAB_MOIS FROM RATTACHEMENT_DECLARATIONS_RECUES r WHERE r.ID_PERIODE = :idPeriode)"
                + " GROUP BY COALESCE(IF(IDENTIFIANT_REPERTOIRE='',NULL,IDENTIFIANT_REPERTOIRE),NTT) "
                + " ORDER BY i.NOM_FAMILLE,i.NOM_USAGE,i.PRENOM,i.IDENTIFIANT_REPERTOIRE,i.NTT LIMIT :taillePage OFFSET :indexPremierResultat";

        Map<String, Object> params = new HashMap<>();
        params.put(PARAM_ALERT_SIGNAL, ParamControleSignalRejet.NIVEAU_ALERTE_SIGNAL);
        params.put(PARAM_ALERT_REJET, ParamControleSignalRejet.NIVEAU_ALERTE_REJET);
        params.put(PARAM_INDEX_PREMIER_RESULTAT, indexPremierResultat);
        params.put(PARAM_TAILLE_PAGE, taillePage);
        params.put(PARAM_ID_PERIODE, idPeriode);
        return getJdbcTemplate().query(sql, params, new ExportIndividuExcelRowMapper());
    }

    /**
     * Mapper de lignes de l'export d'individu vers Excel.
     * 
     * @author bussacj
     *
     */
    public static class ExportIndividuExcelRowMapper implements RowMapper<ExportIndividuExcel> {

        @Override
        public ExportIndividuExcel mapRow(ResultSet rs, int rowNum) throws SQLException {
            ExportIndividuExcel individu = new ExportIndividuExcel();
            individu.setNomUsage(rs.getString(ROW_NOM_USAGE));
            individu.setPrenom(rs.getString(ROW_PRENOM));
            individu.setIdentifiantRepertoire(rs.getString(ROW_IDENTIFIANT_REPERTOIRE));
            individu.setNumeroTechniqueTemporaire(rs.getString("NTT"));
            individu.setMatricule(rs.getString(ROW_MATRICULE));
            individu.setConcatMsgSignal(rs.getString("CONCAT_MSG_ALERT_SIGNAL"));
            individu.setConcatMsgRejet(rs.getString("CONCAT_MSG_ALERT_REJET"));
            individu.setSommeMontantsCotisation(rs.getDouble("SUM_COTISATION"));
            // si rs.getDouble("SUM_COTISATION") retourne la valeur SQL null, le mapper retourne 0
            // hors, on veut pouvoir faire la distinction entre 0 et null pour l'export excel
            individu.setSommeMontantsCotisationExistante(!rs.wasNull());
            individu.setNomFamille(rs.getString(ROW_NOM_FAMILLE));

            return individu;
        }

    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public List<Long> getPeriodesOrphelines(Set<Long> idsPeriodes) {
        String sql = "SELECT p.ID_PERIODE FROM RATTACHEMENT_DECLARATIONS_RECUES rdr RIGHT JOIN PERIODES_RECUES p ON p.ID_PERIODE = rdr.ID_PERIODE WHERE p.ID_PERIODE IN (:idsPeriodes) AND rdr.ID_PERIODE IS NULL";

        Map<String, Object> params = new HashMap<>();
        params.put("idsPeriodes", idsPeriodes);
        return getJdbcTemplate().query(sql, params, new RowMapper<Long>() {
            @Override
            public Long mapRow(ResultSet rs, int rowNum) throws SQLException {
                return rs.getLong("ID_PERIODE");
            }
        });
    }

    @Override
    public List<String> getCodePopulationAffiliation(Long idPeriode) {
        String sql = "SELECT DISTINCT a.CODE_POPULATION FROM AFFILIATION a"
                + " LEFT JOIN CONTRAT_TRAVAIL ct on a.ID_CONTRAT_TRAVAIL=ct.ID_CONTRAT_TRAVAIL LEFT JOIN INDIVIDU i on ct.ID_INDIVIDU=i.ID_INDIVIDU"
                + " LEFT JOIN ADHESION_ETABLISSEMENT_MOIS adh on i.ID_ADH_ETAB_MOIS=adh.ID_ADH_ETAB_MOIS"
                + " WHERE adh.TMP_BATCH IS FALSE AND adh.ID_ADH_ETAB_MOIS IN "
                + " (SELECT r.ID_ADH_ETAB_MOIS FROM RATTACHEMENT_DECLARATIONS_RECUES r WHERE r.ID_PERIODE = :" + PARAM_ID_PERIODE + ")";

        Map<String, Object> params = new HashMap<>();
        params.put(PARAM_ID_PERIODE, idPeriode);

        // mapper
        return getJdbcTemplate().query(sql, params, new RowMapper<String>() {
            @Override
            public String mapRow(ResultSet rs, int rowNum) throws SQLException {
                return rs.getString(1);
            }
        });
    }

    @Override
    public Integer getDernierMoisDeclareAdhesionEtablissementMois(Long idPeriode) {
        StringBuilder jpql = new StringBuilder("SELECT adh.moisDeclare FROM AdhesionEtablissementMois adh WHERE ");
        jpql.append(JPQL_CLAUSE_ADHESION_ETABLISSEMENT_MOIS_RATTACHEE_A_PERIODE).append(" ORDER BY adh.moisDeclare DESC, adh.idAdhEtabMois DESC");
        TypedQuery<Integer> query = getEntityManager().createQuery(jpql.toString(), Integer.class).setMaxResults(1);
        query.setParameter(PARAM_ID_PERIODE, idPeriode);
        return getPremierResultatSiExiste(query.getResultList());
    }

    @Override
    public int rattachementVersPeriodeCible(Long idPeriodeCible, List<Long> idsPeriodeAutres, String loginUser) {
        if (!idsPeriodeAutres.isEmpty()) {
            String jpql = "UPDATE FROM RattachementDeclarationsRecues r SET r.auditUtilisateurDerniereModification = :loginUser,r.idPeriode = :idPeriodeCible "
                    + " WHERE r.idPeriode IN (" + org.springframework.util.StringUtils.collectionToDelimitedString(idsPeriodeAutres, ",", "'", "'")
                    + ")";
            Query query = getEntityManager().createQuery(jpql);
            query.setParameter("loginUser", loginUser);
            query.setParameter("idPeriodeCible", idPeriodeCible);
            return query.executeUpdate();
        } else {
            return 0;
        }

    }

    @Override
    public List<ResumeAffiliationExportExcelAllegesMessage> getIndividusMessages(Long idPeriode) {
        String sql = "SELECT i.IDENTIFIANT_REPERTOIRE, i.NTT, i.MATRICULE, i.SEXE, i.NOM_FAMILLE, i.NOM_USAGE, i.PRENOM, i.DATE_NAISSANCE, tmp_ct1.date_entree_ct AS DATE_DEBUT_CONTRAT, tmp_ct2.date_sortie_ct AS DATE_FIN_PREVISIONNELLE, coalesce(tmp_mc.CONCAT_MSG_ALERT_SIGNAL, '') AS Signalements, coalesce(tmp_mc2.CONCAT_MSG_ALERT_REJET, '')  AS Rejets "
                + " FROM individu i" + " LEFT JOIN ("
                + " SELECT MIN(ct1.DATE_DEBUT_CONTRAT) AS date_entree_ct, COALESCE(IF(i1.IDENTIFIANT_REPERTOIRE='',NULL,i1.IDENTIFIANT_REPERTOIRE),i1.NTT) AS individu"
                + " FROM contrat_travail ct1" + " INNER JOIN individu i1 on ct1.ID_INDIVIDU = i1.ID_INDIVIDU"
                + " INNER JOIN rattachement_declarations_recues rdr1 on i1.ID_ADH_ETAB_MOIS = rdr1.ID_ADH_ETAB_MOIS"
                + " INNER JOIN adhesion_etablissement_mois adh1 on adh1.ID_ADH_ETAB_MOIS = i1.ID_ADH_ETAB_MOIS" + " WHERE rdr1.ID_PERIODE = "
                + idPeriode + " AND adh1.TMP_BATCH is false"
                + " GROUP BY COALESCE(IF(i1.IDENTIFIANT_REPERTOIRE='',NULL,i1.IDENTIFIANT_REPERTOIRE),i1.NTT)"
                + " ) tmp_ct1 on tmp_ct1.individu = COALESCE(IF(i.IDENTIFIANT_REPERTOIRE='',NULL,i.IDENTIFIANT_REPERTOIRE),i.NTT)" + " LEFT JOIN ("
                + " SELECT MAX(ct1.DATE_FIN_PREVISIONNELLE) AS date_sortie_ct, COALESCE(IF(i1.IDENTIFIANT_REPERTOIRE='',NULL,i1.IDENTIFIANT_REPERTOIRE),i1.NTT) AS individu"
                + " FROM contrat_travail ct1" + " INNER JOIN individu i1 on ct1.ID_INDIVIDU = i1.ID_INDIVIDU"
                + " INNER JOIN rattachement_declarations_recues rdr1 on i1.ID_ADH_ETAB_MOIS = rdr1.ID_ADH_ETAB_MOIS"
                + " INNER JOIN adhesion_etablissement_mois adh1 on adh1.ID_ADH_ETAB_MOIS = i1.ID_ADH_ETAB_MOIS" + " WHERE rdr1.ID_PERIODE = "
                + idPeriode + " AND adh1.TMP_BATCH is false"
                + " GROUP BY COALESCE(IF(i1.IDENTIFIANT_REPERTOIRE='',NULL,i1.IDENTIFIANT_REPERTOIRE),i1.NTT)"
                + " ) tmp_ct2 on tmp_ct2.individu = COALESCE(IF(i.IDENTIFIANT_REPERTOIRE='',NULL,i.IDENTIFIANT_REPERTOIRE),i.NTT)" + " LEFT JOIN ("
                + " SELECT mc.INDIVIDU, CAST(GROUP_CONCAT(mc.MESSAGE_UTILISATEUR SEPARATOR',\n' ) AS CHAR) CONCAT_MSG_ALERT_SIGNAL"
                + " FROM MESSAGE_CONTROLE mc" + " WHERE mc.ID_PERIODE = " + idPeriode + " AND mc.NIVEAU_ALERTE = 'SIGNAL' GROUP BY mc.INDIVIDU"
                + " ) tmp_mc on tmp_mc.INDIVIDU = COALESCE(IF(IDENTIFIANT_REPERTOIRE='',NULL,IDENTIFIANT_REPERTOIRE),NTT)" + " LEFT JOIN ("
                + " SELECT mc2.INDIVIDU, CAST(GROUP_CONCAT(mc2.MESSAGE_UTILISATEUR SEPARATOR',\n' ) AS CHAR) CONCAT_MSG_ALERT_REJET"
                + " FROM MESSAGE_CONTROLE mc2" + " WHERE mc2.ID_PERIODE = " + idPeriode + " AND mc2.NIVEAU_ALERTE = 'REJET' GROUP BY mc2.INDIVIDU"
                + " ) tmp_mc2 on tmp_mc2.INDIVIDU = COALESCE(IF(IDENTIFIANT_REPERTOIRE='',NULL,IDENTIFIANT_REPERTOIRE),NTT)"
                + " INNER JOIN rattachement_declarations_recues rdr on rdr.ID_ADH_ETAB_MOIS = i.ID_ADH_ETAB_MOIS AND rdr.ID_PERIODE = " + idPeriode
                + " INNER JOIN" + " ("
                + " SELECT MAX(adh1.ID_ADH_ETAB_MOIS) id_adh_max, COALESCE(IF(IDENTIFIANT_REPERTOIRE='',NULL,IDENTIFIANT_REPERTOIRE),NTT) AS individu"
                + " FROM rattachement_declarations_recues rdr1"
                + " INNER JOIN adhesion_etablissement_mois adh1 on rdr1.ID_ADH_ETAB_MOIS = adh1.ID_ADH_ETAB_MOIS AND adh1.TMP_BATCH is false"
                + " INNER JOIN individu i1 on adh1.ID_ADH_ETAB_MOIS = i1.ID_ADH_ETAB_MOIS" + " INNER JOIN ("
                + " SELECT MAX(adh2.MOIS_RATTACHEMENT) mois_max, COALESCE(IF(IDENTIFIANT_REPERTOIRE='',NULL,IDENTIFIANT_REPERTOIRE),NTT) AS individu"
                + " FROM rattachement_declarations_recues rdr2"
                + " INNER JOIN adhesion_etablissement_mois adh2 on rdr2.ID_ADH_ETAB_MOIS = adh2.ID_ADH_ETAB_MOIS AND adh2.TMP_BATCH is false"
                + " INNER JOIN individu i2 on i2.ID_ADH_ETAB_MOIS = adh2.ID_ADH_ETAB_MOIS" + " WHERE rdr2.ID_PERIODE = " + idPeriode
                + " GROUP BY COALESCE(IF(IDENTIFIANT_REPERTOIRE='',NULL,IDENTIFIANT_REPERTOIRE),NTT)"
                + " ) AS tmp_mois_rattac on tmp_mois_rattac.individu = COALESCE(IF(IDENTIFIANT_REPERTOIRE='',NULL,IDENTIFIANT_REPERTOIRE),NTT) AND tmp_mois_rattac.mois_max = adh1.MOIS_RATTACHEMENT"
                + " WHERE rdr1.ID_PERIODE = " + idPeriode + " GROUP BY COALESCE(IF(IDENTIFIANT_REPERTOIRE='',NULL,IDENTIFIANT_REPERTOIRE),NTT)"
                + " ) AS tmp_adh_mois on tmp_adh_mois.individu = COALESCE(IF(IDENTIFIANT_REPERTOIRE='',NULL,IDENTIFIANT_REPERTOIRE),NTT) AND i.ID_ADH_ETAB_MOIS= tmp_adh_mois.id_adh_max"
                + " GROUP BY COALESCE(IF(IDENTIFIANT_REPERTOIRE='',NULL,IDENTIFIANT_REPERTOIRE),NTT)"
                + " ORDER BY i.NOM_FAMILLE, i.NOM_USAGE, i.PRENOM";

        Map<String, Object> params = new HashMap<>();
        params.put(PARAM_ID_PERIODE, idPeriode);
        return getJdbcTemplate().query(sql, params, new ResumeAffiliationExportExcelAbMessage());
    }

    @Override
    public List<ResumeAffiliationExportExcelAllegesMontant> getIndividusMontants(Long idPeriode) {
        String sql = "SELECT i.IDENTIFIANT_REPERTOIRE, i.NTT, i.MATRICULE, i.SEXE, i.NOM_FAMILLE, i.NOM_USAGE, i.PRENOM, i.DATE_NAISSANCE, tmp_ct1.date_entree_ct  AS DATE_DEBUT_CONTRAT, tmp_ct2.date_sortie_ct AS DATE_FIN_PREVISIONNELLE, sum(cqi.MT_COTISATION) AS MT_COTISATION, cqi.NOCAT, cqi.LICAT"
                + " FROM individu i" + " LEFT JOIN ("
                + " SELECT MIN(ct1.DATE_DEBUT_CONTRAT) AS date_entree_ct, COALESCE(IF(i1.IDENTIFIANT_REPERTOIRE='',NULL,i1.IDENTIFIANT_REPERTOIRE),i1.NTT) AS individu"
                + " FROM contrat_travail ct1" + " INNER JOIN individu i1 on ct1.ID_INDIVIDU = i1.ID_INDIVIDU"
                + " INNER JOIN rattachement_declarations_recues rdr1 on i1.ID_ADH_ETAB_MOIS = rdr1.ID_ADH_ETAB_MOIS"
                + " INNER JOIN adhesion_etablissement_mois adh1 on adh1.ID_ADH_ETAB_MOIS = i1.ID_ADH_ETAB_MOIS" + " WHERE rdr1.ID_PERIODE = "
                + idPeriode + " AND adh1.TMP_BATCH is false"
                + " GROUP BY COALESCE(IF(i1.IDENTIFIANT_REPERTOIRE='',NULL,i1.IDENTIFIANT_REPERTOIRE),i1.NTT)"
                + " ) tmp_ct1 on tmp_ct1.individu = COALESCE(IF(i.IDENTIFIANT_REPERTOIRE='',NULL,i.IDENTIFIANT_REPERTOIRE),i.NTT)" + " LEFT JOIN ("
                + " SELECT MAX(ct1.DATE_FIN_PREVISIONNELLE) AS date_sortie_ct, COALESCE(IF(i1.IDENTIFIANT_REPERTOIRE='',NULL,i1.IDENTIFIANT_REPERTOIRE),i1.NTT) AS individu"
                + " FROM contrat_travail ct1" + " INNER JOIN individu i1 on ct1.ID_INDIVIDU = i1.ID_INDIVIDU"
                + " INNER JOIN rattachement_declarations_recues rdr1 on i1.ID_ADH_ETAB_MOIS = rdr1.ID_ADH_ETAB_MOIS"
                + " INNER JOIN adhesion_etablissement_mois adh1 on adh1.ID_ADH_ETAB_MOIS = i1.ID_ADH_ETAB_MOIS" + " WHERE rdr1.ID_PERIODE = "
                + idPeriode + " AND adh1.TMP_BATCH is false"
                + " GROUP BY COALESCE(IF(i1.IDENTIFIANT_REPERTOIRE='',NULL,i1.IDENTIFIANT_REPERTOIRE),i1.NTT)"
                + " ) tmp_ct2 on tmp_ct2.individu = COALESCE(IF(i.IDENTIFIANT_REPERTOIRE='',NULL,i.IDENTIFIANT_REPERTOIRE),i.NTT)"
                + " LEFT JOIN categorie_quittancement_individu cqi on cqi.ID_PERIODE = " + idPeriode
                + " AND cqi.INDIVIDU = COALESCE(IF(i.IDENTIFIANT_REPERTOIRE='',NULL,i.IDENTIFIANT_REPERTOIRE),i.NTT)"
                + " INNER JOIN rattachement_declarations_recues rdr on rdr.ID_ADH_ETAB_MOIS = i.ID_ADH_ETAB_MOIS AND rdr.ID_PERIODE = " + idPeriode
                + " INNER JOIN" + " ("
                + " SELECT MAX(adh1.ID_ADH_ETAB_MOIS) id_adh_max, COALESCE(IF(IDENTIFIANT_REPERTOIRE='',NULL,IDENTIFIANT_REPERTOIRE),NTT) AS individu"
                + " FROM rattachement_declarations_recues rdr1"
                + " INNER JOIN adhesion_etablissement_mois adh1 on rdr1.ID_ADH_ETAB_MOIS = adh1.ID_ADH_ETAB_MOIS AND adh1.TMP_BATCH is false"
                + " INNER JOIN individu i1 on adh1.ID_ADH_ETAB_MOIS = i1.ID_ADH_ETAB_MOIS" + " INNER JOIN ("
                + " SELECT MAX(adh2.MOIS_RATTACHEMENT) mois_max, COALESCE(IF(IDENTIFIANT_REPERTOIRE='',NULL,IDENTIFIANT_REPERTOIRE),NTT) AS individu"
                + " FROM rattachement_declarations_recues rdr2"
                + " INNER JOIN adhesion_etablissement_mois adh2 on rdr2.ID_ADH_ETAB_MOIS = adh2.ID_ADH_ETAB_MOIS AND adh2.TMP_BATCH is false"
                + " INNER JOIN individu i2 on i2.ID_ADH_ETAB_MOIS = adh2.ID_ADH_ETAB_MOIS" + " WHERE rdr2.ID_PERIODE = " + idPeriode
                + " GROUP BY COALESCE(IF(IDENTIFIANT_REPERTOIRE='',NULL,IDENTIFIANT_REPERTOIRE),NTT)"
                + " ) AS tmp_mois_rattac on tmp_mois_rattac.individu = COALESCE(IF(IDENTIFIANT_REPERTOIRE='',NULL,IDENTIFIANT_REPERTOIRE),NTT) AND tmp_mois_rattac.mois_max = adh1.MOIS_RATTACHEMENT"
                + " WHERE rdr1.ID_PERIODE = " + idPeriode + " GROUP BY COALESCE(IF(IDENTIFIANT_REPERTOIRE='',NULL,IDENTIFIANT_REPERTOIRE),NTT)"
                + " ) AS tmp_adh_mois on tmp_adh_mois.individu = COALESCE(IF(IDENTIFIANT_REPERTOIRE='',NULL,IDENTIFIANT_REPERTOIRE),NTT) AND i.ID_ADH_ETAB_MOIS= tmp_adh_mois.id_adh_max"
                + " GROUP BY COALESCE(IF(IDENTIFIANT_REPERTOIRE='',NULL,IDENTIFIANT_REPERTOIRE),NTT), cqi.NOCAT, cqi.LICAT"
                + " ORDER BY i.NOM_FAMILLE, i.NOM_USAGE, i.PRENOM";

        Map<String, Object> params = new HashMap<>();
        params.put(PARAM_ID_PERIODE, idPeriode);
        return getJdbcTemplate().query(sql, params, new ResumeAffiliationExportExcelAbMontant());
    }

}
