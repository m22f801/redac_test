package fr.si2m.red.internal.complement;

import java.util.List;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Root;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import fr.si2m.red.complement.ExtensionContrat;
import fr.si2m.red.complement.ExtensionContratRepository;
import fr.si2m.red.complement.ExtensionsContratsCriteresRecherche;
import fr.si2m.red.complement.ResumeExtensionContrat;
import fr.si2m.red.core.repository.jpa.JpaEntiteImportableRepository;
import fr.si2m.red.parametrage.ParamFamilleContratRepository;
import lombok.Setter;

/**
 * Base de données des entités {@link ExtensionContrat}, connectée via JPA.
 * 
 * @author nortaina
 *
 */
@Repository
public class JpaExtensionContratRepository extends JpaEntiteImportableRepository<ExtensionContrat> implements ExtensionContratRepository {

    private static final Logger LOGGER = LoggerFactory.getLogger(JpaExtensionContratRepository.class);

    private static final String PARAM_NUM_CONTRAT = "numContrat";

    private static final String PARAM_VIP_AS_TEXT = "vipAsText";

    /**
     * Le référentiel des familles de contrats connues.
     * 
     * @return le référentiel des familles de contrats connues
     */
    @Autowired
    @Setter
    private ParamFamilleContratRepository paramFamilleContratRepository;

    @Override
    @Transactional(propagation = Propagation.MANDATORY)
    public ExtensionContrat getExtensionContrat(String numContrat) {
        TypedQuery<ExtensionContrat> query = getEntityManager()
                .createQuery("SELECT e FROM ExtensionContrat e WHERE e.numContrat = :numContrat", ExtensionContrat.class).setMaxResults(1);
        query.setParameter(PARAM_NUM_CONTRAT, numContrat);
        List<ExtensionContrat> result = query.getResultList();

        ExtensionContrat extensionCorrespondante = null;
        if (!result.isEmpty()) {
            extensionCorrespondante = result.get(0);
        }
        return extensionCorrespondante;
    }

    @Override
    @Transactional(propagation = Propagation.MANDATORY)
    public List<ResumeExtensionContrat> rechercheExtensionsContrats(ExtensionsContratsCriteresRecherche criteres) {
        CriteriaBuilder criteriaBuilder = getEntityManager().getCriteriaBuilder();

        CriteriaQuery<ResumeExtensionContrat> query = criteriaBuilder.createQuery(ResumeExtensionContrat.class);
        Root<ResumeExtensionContrat> contratEntity = query.from(ResumeExtensionContrat.class);

        // Search part
        prepareRecherche(query, criteriaBuilder, contratEntity, criteres);

        // Aspects part
        prepareTri(query, criteriaBuilder, contratEntity, criteres);

        TypedQuery<ResumeExtensionContrat> typedQuery = getEntityManager().createQuery(query);
        preparePagination(typedQuery, criteres);

        LOGGER.debug("Recherche de contrats en cours avec les critères suivants : {}", criteres);
        List<ResumeExtensionContrat> contrats = typedQuery.getResultList();
        LOGGER.debug("Recherche de contrats terminées avec {} résultats", contrats.size());

        return contrats;
    }

    /**
     * Méthode privée modulable si d'autres critères de recherche viennent à s'ajoute dessus.
     * 
     * @param query
     *            la query de base des simulations
     * @param criteriaBuilder
     *            le constructeur de query JPA
     * @param resumeExtensionContratEntity
     *            la racine des entités pour la query JPA
     * @param criteres
     *            les paramètres de filtre pour la query
     */
    private void prepareRecherche(CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder, Root<ResumeExtensionContrat> resumeExtensionContratEntity,
            ExtensionsContratsCriteresRecherche criteres) {
        // Recherche uniquement activée sur les entités en fonction
        Expression<Boolean> restriction = criteriaBuilder.and();

        restriction = prepareCriteres1(restriction, criteriaBuilder, resumeExtensionContratEntity, criteres);

        restriction = prepareCriteres2(restriction, criteriaBuilder, resumeExtensionContratEntity, criteres);

        // Critère "VIP"
        Expression<Boolean> isContratVIPConsultable = resumeExtensionContratEntity.get(PARAM_NUM_CONTRAT).in(criteres.getFiltreContratsVIP());
        Expression<Boolean> isContratVIP = criteriaBuilder.equal(resumeExtensionContratEntity.get(PARAM_VIP_AS_TEXT), "O");
        Expression<Boolean> isNotContratVIP = criteriaBuilder.equal(resumeExtensionContratEntity.get(PARAM_VIP_AS_TEXT), "N");

        // Si on ne recherche qu'un seul type de contrat
        if (criteres.getVipAsList().size() == 1) {
            if (criteres.getVipAsList().get(0).toLowerCase().startsWith("o")) {
                // Si le type de contrat recherché est VIP
                restriction = criteriaBuilder.and(restriction, isContratVIP);
                if (!criteres.getFiltreContratsVIP().isEmpty()) {
                    // La liste des contrats VIP est définie => on ne peut afficher que ceux là
                    restriction = criteriaBuilder.and(restriction, isContratVIPConsultable);
                }
            } else {
                // Si le type de contrat recherché est non VIP
                restriction = criteriaBuilder.and(restriction, isNotContratVIP);
            }
        } else if (!criteres.getFiltreContratsVIP().isEmpty()) {
            // Sinon, on ne recherche pas qu'un seul type de contrat, mais les 2 types.
            // S'il y a une liste de contrat VIP définie, il faut filtrer sur cette liste UNIQUEMENT si resumeExtensionContratEntity.vipAsText est à O
            Expression<Boolean> filtreVIP = criteriaBuilder.or(isNotContratVIP, criteriaBuilder.and(isContratVIP, isContratVIPConsultable));

            restriction = criteriaBuilder.and(restriction, filtreVIP);
        }

        // Critère "Cpt Prod"
        if (StringUtils.isNotBlank(criteres.getCptProd())) {
            restriction = criteriaBuilder.and(restriction,
                    buildStartsWithExpression(criteriaBuilder, resumeExtensionContratEntity, "compteProducteur", criteres.getCptProd()));
        }

        query.where(restriction);
    }

    /**
     * Préparation des critères de la requête (partie 1).
     * 
     * @param restriction
     *            l'expression booléenne de la somme des filtres
     * @param criteriaBuilder
     *            le constructeur de query JPA
     * @param resumeExtensionContratEntity
     *            la racine des entités pour la query JPA
     * @param criteres
     *            les paramètres de filtre pour la query
     */
    private Expression<Boolean> prepareCriteres1(Expression<Boolean> restriction, CriteriaBuilder criteriaBuilder,
            Root<ResumeExtensionContrat> resumeExtensionContratEntity, ExtensionsContratsCriteresRecherche criteres) {

        Expression<Boolean> newRestriction = restriction;

        // Critère "Contrat"
        if (StringUtils.isNotBlank(criteres.getNumContrat())) {
            newRestriction = criteriaBuilder.and(newRestriction,
                    buildStartsWithExpression(criteriaBuilder, resumeExtensionContratEntity, "numContrat", criteres.getNumContrat()));
        }
        // Critère "Grpe Gestion"
        if (!criteres.getGroupesGestionAsList().isEmpty()) {
            newRestriction = criteriaBuilder.and(newRestriction,
                    resumeExtensionContratEntity.get("groupeGestion").in(criteres.getGroupesGestionAsList()));
        }
        // Critère "N° Client"
        if (StringUtils.isNotBlank(criteres.getNumClient())) {
            newRestriction = criteriaBuilder.and(newRestriction, buildStartsWithExpression(criteriaBuilder, resumeExtensionContratEntity,
                    "numSouscripteur", Long.valueOf(criteres.getNumClient().trim())));
        }
        // Critère "Eligibilité"
        if (!criteres.getEligibiliteContratDsnAsList().isEmpty()) {
            newRestriction = criteriaBuilder.and(newRestriction,
                    resumeExtensionContratEntity.get("eligibiliteContratDsn").in(criteres.getEligibiliteContratDsnAsList()));
        }
        // Critère "Famille"
        if (!criteres.getFamillesAsList().isEmpty()) {
            newRestriction = criteriaBuilder.and(newRestriction, resumeExtensionContratEntity.get("famille").in(criteres.getFamillesAsList()));
        }
        // Critère "Raison sociale"
        if (StringUtils.isNotBlank(criteres.getRaisonSociale())) {
            newRestriction = criteriaBuilder.and(newRestriction,
                    buildLikeExpression(criteriaBuilder, resumeExtensionContratEntity, "souscripteurRaisonSociale", criteres.getRaisonSociale()));
        }
        // Critère "Indicateur Exploitation"
        if (!criteres.getIndicExploitationAsList().isEmpty()) {
            newRestriction = criteriaBuilder.and(newRestriction,
                    resumeExtensionContratEntity.get("exploitationDSNAsText").in(criteres.getIndicExploitationAsList()));
        }
        // Critère "Nature"
        if (!criteres.getModesCalculCotisationAsList().isEmpty()) {
            newRestriction = criteriaBuilder.and(newRestriction,
                    resumeExtensionContratEntity.get("modeCalculCotisation").in(criteres.getModesCalculCotisationAsList()));
        }
        // Critère "SIREN"
        if (StringUtils.isNotBlank(criteres.getSiren())) {
            newRestriction = criteriaBuilder.and(newRestriction,
                    buildStartsWithExpression(criteriaBuilder, resumeExtensionContratEntity, "souscripteurSiren", criteres.getSiren()));
        }

        return newRestriction;
    }

    /**
     * Préparation des critères de la requête (partie 2).
     * 
     * @param restriction
     *            l'expression booléenne de la somme des filtres
     * @param criteriaBuilder
     *            le constructeur de query JPA
     * @param resumeExtensionContratEntity
     *            la racine des entités pour la query JPA
     * @param criteres
     *            les paramètres de filtre pour la query
     */
    private Expression<Boolean> prepareCriteres2(Expression<Boolean> restriction, CriteriaBuilder criteriaBuilder,
            Root<ResumeExtensionContrat> resumeExtensionContratEntity, ExtensionsContratsCriteresRecherche criteres) {

        Expression<Boolean> newRestriction = restriction;

        // Critère "Indicateur Edition Consignes de paiement"
        if (!criteres.getIndicConsignePaiementAsList().isEmpty()) {
            newRestriction = criteriaBuilder.and(newRestriction,
                    resumeExtensionContratEntity.get("editionConsignesPaiementAsText").in(criteres.getIndicConsignePaiementAsList()));
        }
        // Critère "Gestion directe"
        if (!criteres.getModeNatureContratAsList().isEmpty()) {
            newRestriction = criteriaBuilder.and(newRestriction,
                    resumeExtensionContratEntity.get("modeNatureContrat").in(criteres.getModeNatureContratAsList()));
        }
        // Critère "NIC"
        if (StringUtils.isNotBlank(criteres.getNic())) {
            newRestriction = criteriaBuilder.and(newRestriction,
                    buildStartsWithExpression(criteriaBuilder, resumeExtensionContratEntity, "souscripteurNic", criteres.getNic()));
        }
        // Critère "Indicateur Transfert"
        if (!criteres.getIndicTransfertAsList().isEmpty()) {
            newRestriction = criteriaBuilder.and(newRestriction,
                    resumeExtensionContratEntity.get("transfertAsText").in(criteres.getIndicTransfertAsList()));
        }
        // Critère "Cpt Enc"
        if (StringUtils.isNotBlank(criteres.getCptEnc())) {
            newRestriction = criteriaBuilder.and(newRestriction,
                    buildStartsWithExpression(criteriaBuilder, resumeExtensionContratEntity, "compteEncaissement", criteres.getCptEnc()));
        }
        // Critère "Entreprise affiliée"
        if (!criteres.getEntrepriseAffilieeAsList().isEmpty()) {
            newRestriction = criteriaBuilder.and(newRestriction,
                    resumeExtensionContratEntity.get("entrepriseAffiliee").in(criteres.getEntrepriseAffilieeAsList()));
        }

        return newRestriction;
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public long compte(ExtensionsContratsCriteresRecherche criteres) {
        CriteriaBuilder criteriaBuilder = getEntityManager().getCriteriaBuilder();

        CriteriaQuery<Long> query = criteriaBuilder.createQuery(Long.class);
        Root<ResumeExtensionContrat> contratEntity = query.from(ResumeExtensionContrat.class);

        query.select(criteriaBuilder.count(contratEntity));

        // Search part
        prepareRecherche(query, criteriaBuilder, contratEntity, criteres);

        TypedQuery<Long> typedQuery = getEntityManager().createQuery(query);
        return typedQuery.getSingleResult();
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public boolean existeExtensionContrat(String numContrat) {
        TypedQuery<Long> query = getEntityManager().createQuery("SELECT count(*) FROM ExtensionContrat e WHERE e.numContrat = :numContrat",
                Long.class);
        query.setParameter(PARAM_NUM_CONTRAT, numContrat);
        List<Long> result = query.getResultList();

        if (result.isEmpty() || result.get(0) == null) {
            return false;
        }
        return result.get(0) > 0;
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public boolean isContratVIP(String numContrat) {
        TypedQuery<ExtensionContrat> query = getEntityManager()
                .createQuery("SELECT e FROM ExtensionContrat e WHERE e.numContrat = :numContrat", ExtensionContrat.class).setMaxResults(1);
        query.setParameter(PARAM_NUM_CONTRAT, numContrat);
        List<ExtensionContrat> result = query.getResultList();
        boolean vip = false;
        if (!result.isEmpty()) {
            vip = result.get(0).isVip();
        }
        return vip;
    }
}