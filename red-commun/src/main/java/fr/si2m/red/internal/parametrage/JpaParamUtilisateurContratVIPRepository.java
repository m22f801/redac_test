package fr.si2m.red.internal.parametrage;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import fr.si2m.red.core.repository.jpa.JpaEntiteImportableRepository;
import fr.si2m.red.parametrage.ParamUtilisateurContratVIP;
import fr.si2m.red.parametrage.ParamUtilisateurContratVIPRepository;

/**
 * Référentiel des parametres utilisateur contrats vip.
 * 
 * @author delortj
 *
 */
@Repository
public class JpaParamUtilisateurContratVIPRepository extends JpaEntiteImportableRepository<ParamUtilisateurContratVIP> implements
        ParamUtilisateurContratVIPRepository {

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public List<String> getContratsVIPPourUtilisateur(String identifiantActiveDirectory) {
        TypedQuery<ParamUtilisateurContratVIP> query = getEntityManager().createQuery(
                "from ParamUtilisateurContratVIP where codeUtilisateur=:codeUtilisateur", ParamUtilisateurContratVIP.class);
        query.setParameter("codeUtilisateur", identifiantActiveDirectory);
        List<ParamUtilisateurContratVIP> contratsVIP = query.getResultList();
        List<String> numContratsVIP = new ArrayList<String>(contratsVIP.size());
        for (ParamUtilisateurContratVIP contratVIP : contratsVIP) {
            numContratsVIP.add(contratVIP.getNumContrat());
        }
        return numContratsVIP;
    }
}
