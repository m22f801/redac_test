package fr.si2m.red.internal.dsn;

import java.util.List;

import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import fr.si2m.red.core.repository.jpa.JpaEntiteImportableRepository;
import fr.si2m.red.dsn.VersementIndividu;
import fr.si2m.red.dsn.VersementIndividuRepository;

/**
 * Base de données des entités {@link VersementIndividu}, connectée via JPA.
 * 
 * @author poidij
 *
 */
@Repository
public class JpaVersementIndividuRepository extends JpaEntiteImportableRepository<VersementIndividu> implements VersementIndividuRepository {

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public boolean existeUnVersementIndividu(String idVersementIndividu) {
        TypedQuery<Long> query = getEntityManager()
                .createQuery("SELECT count(*) FROM VersementIndividu v WHERE v.idVersementIndividu = :idVersementIndividu", Long.class);
        query.setParameter("idVersementIndividu", idVersementIndividu);
        List<Long> result = query.getResultList();
        if (result.isEmpty() || result.get(0) == null) {
            return false;
        }
        return result.get(0) > 0;
    }

}
