package fr.si2m.red.internal.test.db;

import java.io.Console;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.apache.commons.lang3.StringUtils;
import org.h2.jdbcx.JdbcDataSource;
import org.h2.tools.Server;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.UrlResource;
import org.springframework.jdbc.datasource.init.ScriptUtils;

/**
 * Base de données de test, en mémoire.
 * 
 * @author nortaina
 *
 */
public final class TestDatabase {
    private static final Logger LOGGER = LoggerFactory.getLogger(TestDatabase.class);

    /**
     * Le port par défaut pour cette base de test.
     */
    public static final int PORT_PAR_DEFAUT = 9092;
    private static final int TEST_CONNEXION_TIMEOUT = 10;
    private static final int ATTENTE_CMD = 10000;

    private TestDatabase() {
    }

    /**
     * Démarrage de la base en process Java.
     * 
     * @param args
     *            RAS
     * @throws InterruptedException
     *             si une erreur survient lors de l'attente de fin d'activité
     * @throws SQLException
     *             si une erreur survient à la fermeture de la base
     */
    public static void main(String[] args) throws InterruptedException, SQLException {
        try {
            Thread dbThread = new Thread(new RunnableTestDatabase());
            dbThread.setDaemon(true);
            dbThread.start();
            dbThread.join();
        } finally {
            LOGGER.info("Fermeture de la base de données de test...");
            TestDatabase.shutdown(PORT_PAR_DEFAUT);
            LOGGER.info("Base de données de test fermée !");
        }
    }

    /**
     * Démarre la base de données de test.
     * 
     * @param port
     *            le port de démarrage
     * 
     * @throws SQLException
     *             si une erreur technique survient lors du démarrage de la base
     */
    public static synchronized void start(int port) throws SQLException {
        LOGGER.info("Démarrage de la base de test sur le port {}...", port);
        Server.createTcpServer("-tcpPort", String.valueOf(port), "-tcpAllowOthers").start();
        LOGGER.info("Base de test démarrée sur le port {} !", port);
    }

    /**
     * Eteint la base de test.
     * 
     * @param port
     *            le port ouvert pour la base
     * 
     * @throws SQLException
     *             si une erreur technique survient lors de l'extinction de la base
     */
    public static synchronized void shutdown(int port) throws SQLException {
        LOGGER.info("Fermeture de la base de test sur le port {}...", port);
        Server.shutdownTcpServer("tcp://localhost:" + port, "", true, true);
        LOGGER.info("Base de test fermée sur le port {} !", port);
    }

    /**
     * Réinitialise les données en base.
     * 
     * @param port
     *            le port ouvert pour la base
     * 
     * @throws SQLException
     *             si une erreur technique survient lors du rafraîchissement de la base
     */
    public static synchronized void refreshData(int port) throws SQLException {
        DataSource testDataSource = makeDataSource(port);
        Connection testConnection = testDataSource.getConnection();
        try {
            if (testConnection.isValid(TEST_CONNEXION_TIMEOUT)) {
                ScriptUtils.executeSqlScript(testConnection, new UrlResource(TestDatabase.class.getResource("01_schema.sql")));
                ScriptUtils.executeSqlScript(testConnection, new UrlResource(TestDatabase.class.getResource("02_data.sql")));
            }
        } finally {
            testConnection.close();
        }
    }

    /**
     * Récupère la data source pour cette base de test.
     * 
     * @param port
     *            le port ouvert pour la base
     * 
     * @return la data source pour se connecter à la base
     */
    public static DataSource getDataSource(int port) {
        return makeDataSource(port);
    }

    /**
     * Fabrique une connexion à la base de test.
     * 
     * @param port
     *            le port ouvert pour la base
     */
    private static DataSource makeDataSource(int port) {
        LOGGER.info("Fabrication de la connexion à la base de test sur le port {}...", port);
        JdbcDataSource testDataSource = new JdbcDataSource();
        testDataSource.setURL("jdbc:h2:tcp://localhost:" + port + "/~/test");
        testDataSource.setUser("sa");
        testDataSource.setPassword("");
        LOGGER.info("Connexion à la base de test sur le port {} fabriquée !", port);
        return testDataSource;
    }

    /**
     * Une version de la base de donnée de test pouvant être lancée via ligne de commande.
     * 
     * @author nortaina
     *
     */
    public static class RunnableTestDatabase implements Runnable {
        @Override
        public void run() {
            try {
                LOGGER.info("Démarrage base de données de test...");
                TestDatabase.start(PORT_PAR_DEFAUT);
                LOGGER.info("Base de données de test démarrée !");
                LOGGER.info(StringUtils.EMPTY);
            } catch (SQLException e) {
                LOGGER.error("Erreur lors du démarrage de la base de test", e);
            }
            Console console = System.console();
            if (console != null) {
                console.format("Pressez ENTREE pour éteindre...");
                console.format("");
                console.readLine();
            } else {
                try {
                    while (System.in.read() == 0) {
                        Thread.sleep(ATTENTE_CMD);
                    }
                } catch (IOException e) {
                    LOGGER.error("Erreur lors de la lecture des entrants sur le système", e);
                } catch (InterruptedException e) {
                    LOGGER.error("Erreur lors de l'attente d'action d'interruption de la base", e);
                }
            }
        }
    }
}
