package fr.si2m.red.internal.dsn;

import java.util.List;

import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import fr.si2m.red.core.repository.jpa.JpaEntiteImportableRepository;
import fr.si2m.red.dsn.Contact;
import fr.si2m.red.dsn.ContactRepository;

/**
 * Base de données des entités {@link Contact}, connectée via JPA.
 * 
 * @author poidij
 *
 */
@Repository
public class JpaContactRepository extends JpaEntiteImportableRepository<Contact> implements ContactRepository {

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public boolean existeUnContact(String idContact) {
        TypedQuery<Long> query = getEntityManager().createQuery("SELECT count(*) FROM Contact c WHERE c.idContact = :idContact", Long.class);
        query.setParameter("idContact", idContact);
        List<Long> result = query.getResultList();
        if (result.isEmpty() || result.get(0) == null) {
            return false;
        }
        return result.get(0) > 0;
    }

}
