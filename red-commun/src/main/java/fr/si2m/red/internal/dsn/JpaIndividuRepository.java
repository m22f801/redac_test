package fr.si2m.red.internal.dsn;

import java.util.List;

import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import fr.si2m.red.core.repository.jpa.JpaEntiteImportableRepository;
import fr.si2m.red.dsn.Individu;
import fr.si2m.red.dsn.IndividuRepository;

/**
 * Base de données des entités {@link Individu}, connectée via JPA.
 * 
 * @author poidij
 *
 */
@Repository
public class JpaIndividuRepository extends JpaEntiteImportableRepository<Individu> implements IndividuRepository {

    /**
     * Le nom de paramètre utilisé pour l'identifiant de période.
     */
    private static final String PARAMETRE_ID_PERIODE = "idPeriode";

    /**
     * La clause à ajouter systématiquement pour le lien entre une période et une adhésion établissement mois rattachée pour les requêtes en JPQL.
     */
    private static final String JPQL_CLAUSE_ADHESION_ETABLISSEMENT_MOIS_RATTACHEE_A_PERIODE = "adh.idAdhEtabMois IN (SELECT r.idAdhEtabMois FROM RattachementDeclarationsRecues r "
            + "WHERE r.idPeriode = :" + PARAMETRE_ID_PERIODE + ")";

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public boolean existeUnIndividu(String idIndividu) {
        TypedQuery<Long> query = getEntityManager().createQuery("SELECT count(*) FROM Individu i WHERE i.idIndividu = :idIndividu", Long.class);
        query.setParameter("idIndividu", idIndividu);
        List<Long> result = query.getResultList();
        if (result.isEmpty() || result.get(0) == null) {
            return false;
        }
        return result.get(0) > 0;
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public Individu getPourPeriode(Long idPeriode, String individu) {
        String jpql = "SELECT i FROM Individu i LEFT JOIN i.adhesionEtablissementMois adh WHERE (i.identifiantRepertoire = :individu OR i.numeroTechniqueTemporaire = :individu)"
                + " AND adh.ligneEnCoursImportBatch IS FALSE AND " + JPQL_CLAUSE_ADHESION_ETABLISSEMENT_MOIS_RATTACHEE_A_PERIODE + " ORDER BY adh.moisRattachement DESC";
        TypedQuery<Individu> query = getEntityManager().createQuery(jpql, Individu.class).setMaxResults(1);
        query.setParameter(PARAMETRE_ID_PERIODE, idPeriode);
        query.setParameter("individu", individu);
        List<Individu> resultats = query.getResultList();
        return getPremierResultatSiExiste(resultats);
    }

}
