package fr.si2m.red.internal.reconciliation;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.TypedQuery;

import org.apache.commons.collections.keyvalue.DefaultMapEntry;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import fr.si2m.red.reconciliation.EffectifCategorieMouvement;
import fr.si2m.red.reconciliation.EffectifCategorieMouvementRepository;

/**
 * Base de données des entités {@link EffectifCategorieMouvement}, connectée via JPA.
 * 
 * @author poidij
 *
 */
@Repository
public class JpaEffectifCategorieMouvementRepository extends JpaElementDeclaratifRepository<EffectifCategorieMouvement>
        implements EffectifCategorieMouvementRepository {

    /**
     * Le nom de paramètre utilisé pour l'identifiant de période.
     */
    private static final String PARAMETRE_ID_PERIODE = "idPeriode";

    /**
     * Le nom de paramètre utilisé pour le numéro de la catégorie de quittancement.
     */
    private static final String PARAMETRE_NOCAT = "numCategorieQuittancement";

    /**
     * Le nom de paramètre utilisé pour la date d'effet de mouvement.
     */
    private static final String PARAMETRE_DATE_EFF_MVT = "dateEffetMouvement";

    @Override
    public Class<EffectifCategorieMouvement> getClassePrototypeEntite() {
        return EffectifCategorieMouvement.class;
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public EffectifCategorieMouvement get(Long idPeriode, String numCategorieQuittancement, Integer dateEffetMouvement) {
        String jpql = "SELECT ct FROM EffectifCategorieMouvement ct WHERE ct.idPeriode = :idPeriode "
                + "AND ct.numCategorieQuittancement = :numCategorieQuittancement AND ct.dateEffetMouvement = :dateEffetMouvement";
        TypedQuery<EffectifCategorieMouvement> query = getEntityManager().createQuery(jpql, EffectifCategorieMouvement.class).setMaxResults(1);
        query.setParameter(PARAMETRE_ID_PERIODE, idPeriode);
        query.setParameter(PARAMETRE_NOCAT, numCategorieQuittancement);
        query.setParameter(PARAMETRE_DATE_EFF_MVT, dateEffetMouvement);
        List<EffectifCategorieMouvement> resultats = query.getResultList();
        return getPremierResultatSiExiste(resultats);
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public List<EffectifCategorieMouvement> getPourCategorie(Long idPeriode, String numCategorieQuittancement) {
        String jpql = "SELECT ct FROM EffectifCategorieMouvement ct WHERE ct.idPeriode = :idPeriode "
                + "AND ct.numCategorieQuittancement = :numCategorieQuittancement";
        TypedQuery<EffectifCategorieMouvement> query = getEntityManager().createQuery(jpql, EffectifCategorieMouvement.class);
        query.setParameter(PARAMETRE_ID_PERIODE, idPeriode);
        query.setParameter(PARAMETRE_NOCAT, numCategorieQuittancement);
        return query.getResultList();
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public List<Integer> getDatesEffetDistinctesPourPeriode(Long idPeriode) {
        String jpql = "SELECT DISTINCT ct.dateEffetMouvement FROM EffectifCategorieMouvement ct WHERE ct.idPeriode = :idPeriode ORDER BY ct.dateEffetMouvement ASC";
        TypedQuery<Integer> query = getEntityManager().createQuery(jpql, Integer.class);
        query.setParameter(PARAMETRE_ID_PERIODE, idPeriode);
        return query.getResultList();
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public Map<String, Integer> getNombresMouvementsPourDateEffetEtCategoriesQuittancement(Long idPeriode, Integer dateEffetMouvement,
            List<String> numerosCategorieQuittancement) {
        String jpql = "SELECT new org.apache.commons.collections.keyvalue.DefaultMapEntry(ct.numCategorieQuittancement, ct.nombreMouvement) FROM EffectifCategorieMouvement ct"
                + " WHERE ct.idPeriode = :idPeriode AND ct.dateEffetMouvement = :dateEffetMouvement"
                + " AND ct.numCategorieQuittancement IN (:numerosCategorieQuittancement) ORDER BY ct.numCategorieQuittancement ASC";
        TypedQuery<DefaultMapEntry> query = getEntityManager().createQuery(jpql, DefaultMapEntry.class);
        query.setParameter(PARAMETRE_ID_PERIODE, idPeriode);
        query.setParameter(PARAMETRE_DATE_EFF_MVT, dateEffetMouvement);
        query.setParameter("numerosCategorieQuittancement", numerosCategorieQuittancement);
        List<DefaultMapEntry> couples = query.getResultList();

        // Gestion des cas quand les NOCAT n'ont pas de mouvement => nous ajoutent le valeur "0" associees a ces NOCAT
        for (String numeroCategorieQuittancement : numerosCategorieQuittancement) {
            boolean present = false;
            for (DefaultMapEntry couple : couples) {
                if (couple.getKey().toString().equalsIgnoreCase(numeroCategorieQuittancement)) {
                    present = true;
                }
            }
            if (!present) {
                DefaultMapEntry emptyNocat = new DefaultMapEntry(numeroCategorieQuittancement, 0);
                couples.add(emptyNocat);
            }
        }
        Map<String, Integer> resultats = new HashMap<>();
        for (DefaultMapEntry couple : couples) {
            resultats.put(String.valueOf(couple.getKey()), (Integer) couple.getValue());
        }
        return resultats;
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public Long getNombreTotalMouvementsPourDateEffet(Long idPeriode, Integer dateEffetMouvement) {
        String jpql = "SELECT SUM(ct.nombreMouvement) FROM EffectifCategorieMouvement ct WHERE ct.idPeriode = :idPeriode AND ct.dateEffetMouvement = :dateEffetMouvement";
        TypedQuery<Long> query = getEntityManager().createQuery(jpql, Long.class);
        query.setParameter(PARAMETRE_ID_PERIODE, idPeriode);
        query.setParameter(PARAMETRE_DATE_EFF_MVT, dateEffetMouvement);
        return getPremierResultatSiExiste(query.getResultList());
    }
}
