package fr.si2m.red.internal.reconciliation;

import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import fr.si2m.red.core.repository.jpa.JpaRepository;
import fr.si2m.red.reconciliation.CompteRenduIntegration;
import fr.si2m.red.reconciliation.CompteRenduIntegrationRepository;

/**
 * Base de données des entités {@link CompteRenduIntegration}, connectée via JPA.
 * 
 * @author poidij
 *
 */
@Repository
public class JpaCompteRenduIntegrationRepository extends JpaRepository<CompteRenduIntegration> implements CompteRenduIntegrationRepository {

    private static final String ID_PERIODE = "idPeriode";

    @Override
    public Class<CompteRenduIntegration> getClassePrototypeEntite() {
        return CompteRenduIntegration.class;
    }

    @Override
    public int supprimeCompteRenduIntegrationPourPeriode(Long idPeriode) {
        String jpql = "DELETE FROM CompteRenduIntegration c WHERE c.idPeriode = :idPeriode";
        Query query = getEntityManager().createQuery(jpql);
        query.setParameter(ID_PERIODE, idPeriode);
        return query.executeUpdate();
    }

    @Override
    @Transactional(propagation = Propagation.MANDATORY)
    public CompteRenduIntegration getPourPeriode(Long idPeriode) {
        String jpql = "SELECT c FROM CompteRenduIntegration c WHERE c.idPeriode = :idPeriode ORDER BY c.dateTraitement DESC";
        TypedQuery<CompteRenduIntegration> query = getEntityManager().createQuery(jpql, CompteRenduIntegration.class).setMaxResults(1);
        query.setParameter("idPeriode", idPeriode);
        return getPremierResultatSiExiste(query.getResultList());
    }
}
