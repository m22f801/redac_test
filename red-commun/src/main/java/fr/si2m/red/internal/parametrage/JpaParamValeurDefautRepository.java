package fr.si2m.red.internal.parametrage;

import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import fr.si2m.red.core.repository.jpa.JpaEntiteImportableRepository;
import fr.si2m.red.parametrage.ParamValeurDefaut;
import fr.si2m.red.parametrage.ParamValeurDefautRepository;

/**
 * Base de données des entités ParamValeurDefaut, connectée via JPA.
 * 
 * @author nortaina
 *
 */
@Repository
public class JpaParamValeurDefautRepository extends JpaEntiteImportableRepository<ParamValeurDefaut> implements ParamValeurDefautRepository {

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public String getCodeOrganismeParDefaut() {
        TypedQuery<String> query = getEntityManager().createQuery(
                "SELECT p.codeOrganisme FROM ParamValeurDefaut p WHERE p.ligneEnCoursImportBatch IS FALSE", String.class).setMaxResults(1);
        return query.getSingleResult();

    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public String getModeNatureContratParDefaut() {
        TypedQuery<String> query = getEntityManager().createQuery(
                "SELECT p.modeNatureContrat FROM ParamValeurDefaut p WHERE p.ligneEnCoursImportBatch IS FALSE", String.class).setMaxResults(1);
        return query.getSingleResult();

    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public String getVIPParDefaut() {
        TypedQuery<String> query = getEntityManager().createQuery(
                "SELECT p.contratVIPAsText FROM ParamValeurDefaut p WHERE p.ligneEnCoursImportBatch IS FALSE", String.class).setMaxResults(1);
        return query.getSingleResult();
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public ParamValeurDefaut getParamValeurDefaut() {
        TypedQuery<ParamValeurDefaut> query = getEntityManager().createQuery(
                "SELECT p FROM ParamValeurDefaut p WHERE p.ligneEnCoursImportBatch IS FALSE", ParamValeurDefaut.class).setMaxResults(1);
        return query.getSingleResult();
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public String getModeReaffectationCategorieEffectifsParDefaut() {
        TypedQuery<String> query = getEntityManager().createQuery(
                "SELECT p.modeReaffectationCategorieEffectifs FROM ParamValeurDefaut p WHERE p.ligneEnCoursImportBatch IS FALSE", String.class)
                .setMaxResults(1);
        return query.getSingleResult();
    }
}
