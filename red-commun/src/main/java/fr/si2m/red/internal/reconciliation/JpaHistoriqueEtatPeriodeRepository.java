package fr.si2m.red.internal.reconciliation;

import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import fr.si2m.red.DateRedac;
import fr.si2m.red.core.repository.jpa.JpaRepository;
import fr.si2m.red.reconciliation.HistoriqueEtatPeriode;
import fr.si2m.red.reconciliation.HistoriqueEtatPeriodeRepository;

/**
 * Base de données des entités {@link HistoriqueEtatPeriode}, connectée via JPA.
 * 
 * @author poidij
 *
 */
@Repository
public class JpaHistoriqueEtatPeriodeRepository extends JpaRepository<HistoriqueEtatPeriode> implements HistoriqueEtatPeriodeRepository {

    private static final String PARAM_ID_PERIODE = "idPeriode";
    private static final String PARAM_LISTE_ID_PERIODES = "idsPeriodes";

    @Override
    public Class<HistoriqueEtatPeriode> getClassePrototypeEntite() {
        return HistoriqueEtatPeriode.class;
    }

    @Override
    @Transactional(propagation = Propagation.MANDATORY, rollbackFor = Throwable.class)
    public List<HistoriqueEtatPeriode> getHistoriqueEtatsPeriode(Long idPeriode) {
        String jpql = "FROM HistoriqueEtatPeriode h WHERE h.idPeriode=:idPeriode ORDER BY h.dateHeureChangement DESC";
        TypedQuery<HistoriqueEtatPeriode> query = getEntityManager().createQuery(jpql, HistoriqueEtatPeriode.class);
        query.setParameter(PARAM_ID_PERIODE, idPeriode);
        return query.getResultList();
    }

    @Override
    @Transactional(propagation = Propagation.MANDATORY, rollbackFor = Throwable.class)
    public HistoriqueEtatPeriode getHistoriqueEtatPeriodePourMessage(Long idPeriode, Long dateHeureChangement) {
        String jpql = "FROM HistoriqueEtatPeriode h WHERE h.idPeriode=:idPeriode AND h.dateHeureChangement = :dateHeureChangement AND h.etatPeriode = 'INT'";
        TypedQuery<HistoriqueEtatPeriode> query = getEntityManager().createQuery(jpql, HistoriqueEtatPeriode.class).setMaxResults(1);
        query.setParameter(PARAM_ID_PERIODE, idPeriode);
        query.setParameter("dateHeureChangement", dateHeureChangement);
        return getPremierResultatSiExiste(query.getResultList());
    }

    @Override
    @Transactional(propagation = Propagation.MANDATORY, rollbackFor = Throwable.class)
    public HistoriqueEtatPeriode getDernierHistoriquePourPeriode(Long idPeriode) {
        String jpql = "FROM HistoriqueEtatPeriode h WHERE h.idPeriode=:idPeriode ORDER BY h.dateHeureChangement DESC";
        TypedQuery<HistoriqueEtatPeriode> query = getEntityManager().createQuery(jpql, HistoriqueEtatPeriode.class).setMaxResults(1);
        query.setParameter(PARAM_ID_PERIODE, idPeriode);
        return getPremierResultatSiExiste(query.getResultList());
    }

    @Override
    public int supprimeHistoriqueEtatPeriodePourPeriode(Long idPeriode) {
        String jpql = "DELETE FROM HistoriqueEtatPeriode h WHERE h.idPeriode = :idPeriode";
        Query query = getEntityManager().createQuery(jpql);
        query.setParameter(PARAM_ID_PERIODE, idPeriode);
        return query.executeUpdate();

    }

    @Override
    @Transactional(propagation = Propagation.MANDATORY)
    public void callMysqlProcedure(String sql, Map<String, Object> params) {
        getJdbcTemplate().update(sql, params);
    }

    @Override
    public void creationEnMasseOrigineIHM(List<Long> idsPeriodes, String message, String utilisateur, String statutCible) {
        String sql = "call Creation_HistoriqueEtatPeriode_en_masse( :" + PARAM_LISTE_ID_PERIODES + ","
                + ":datehms , :etatPeriode , :origine , :utilisateur , :commentaire , :userCreation )";
        Map<String, Object> params = new HashMap<>();
        params.put(PARAM_LISTE_ID_PERIODES, StringUtils.join(idsPeriodes, ","));
        params.put("datehms", DateRedac.convertitEnDateHeureRedac(Calendar.getInstance().getTime()));
        params.put("etatPeriode", statutCible);
        params.put("origine", HistoriqueEtatPeriode.ORIGINE_CONTROLE_IHM);
        params.put("utilisateur", utilisateur);
        params.put("commentaire", message);
        params.put("userCreation", utilisateur);

        callMysqlProcedure(sql, params);

    }
}
