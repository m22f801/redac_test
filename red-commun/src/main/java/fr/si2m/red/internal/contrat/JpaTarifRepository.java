package fr.si2m.red.internal.contrat;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.MessageFormat;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.TypedQuery;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import fr.si2m.red.DateRedac;
import fr.si2m.red.contrat.NumCategorieLibelleCategorie;
import fr.si2m.red.contrat.Tarif;
import fr.si2m.red.contrat.TarifRepository;
import fr.si2m.red.core.repository.jpa.JpaEntiteImportableRepository;

/**
 * Base de données des entités tarif, connectée via JPA.
 * 
 * @author nortaina
 *
 */
@Repository
public class JpaTarifRepository extends JpaEntiteImportableRepository<Tarif> implements TarifRepository {

    private static final String PARAM_NUM_CONTRAT = "numContrat";
    private static final String PARAM_NUM_CATEGORIE = "numCategorie";
    private static final String PARAM_DATE_EFFET = "dateEffet";
    private static final String PARAM_DATE_DEBUT_PLAGE = "dateDebutPlage";
    private static final String PARAM_DATE_FIN_PLAGE = "dateFinPlage";

    private static final String JPQL_WHERE_NUMERO_CONTRAT = "WHERE t.numContrat = :";
    private static final String JPQL_CONDITION_PARAMS_DANS_PLAGE_SITUATION = " AND t.dateDebutSituationLigne <= coalesce(:{0}, 99999999) AND "
            + "(coalesce(t.dateFinSituationLigne, 99999999) >= :{1} OR (CASE WHEN (t.dateFinSituationLigne = 0) THEN 99999999 ELSE t.dateFinSituationLigne END) >= :{1})";
    private static final String JPQL_CONDITION_NUM_CATEGORIE = " AND t.numCategorie = :" + PARAM_NUM_CATEGORIE;
    private static final String JPQL_ORDER_BY_DATE_DEBUT_SITUATION_DESC = " ORDER BY t.dateDebutSituationLigne DESC";
    private static final String JPQL_ORDER_BY_NOCAT_THEN_DATE_DEBUT_SITUATION_DESC = "ORDER BY t.numCategorie, t.dateDebutSituationLigne DESC";

    /**
     * Le début de requête pour les recherches de tarifs en JPQL natif.
     */
    private static final String JPQL_PARTIE_REQUETE_TARIF = "SELECT t FROM Tarif t WHERE t.numContrat = :";

    /**
     * Le début de requête pour les recherches des MOCALCOT de tarifs en JPQL natif.
     */
    private static final String JPQL_PARTIE_REQUETE_TARIF_MOCALCOT = "SELECT t.modeCalculCotisations FROM Tarif t WHERE t.numContrat = :";

    /**
     * Le début de requête pour les recherches des NATURE_CONTRAT de tarifs en JPQL natif.
     */
    private static final String JPQL_PARTIE_REQUETE_TARIF_NATURE_CONTRAT = "SELECT t.natureContrat FROM Tarif t WHERE t.numContrat = :";

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public Integer getDateDebutMinimumTarifContratTemporaire(String numContrat) {
        String jpql = "SELECT t.dateDebutSituationLigne FROM Tarif t WHERE t.numContrat = :" + PARAM_NUM_CONTRAT
                + " AND t.ligneEnCoursImportBatch IS TRUE ORDER BY t.dateDebutSituationLigne";
        TypedQuery<Integer> query = getEntityManager().createQuery(jpql, Integer.class).setMaxResults(1);
        query.setParameter(PARAM_NUM_CONTRAT, numContrat);
        return getPremierResultatSiExiste(query.getResultList());
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public Integer getDateDebutDeLaSituationTarifTemporaireSuivante(String numContrat, Integer dateDebutSituationLigne) {
        String jpql = "SELECT t.dateDebutSituationLigne FROM Tarif t WHERE t.numContrat = :" + PARAM_NUM_CONTRAT
                + " AND t.dateDebutSituationLigne > :dateDebutSituationLigne AND t.ligneEnCoursImportBatch IS TRUE"
                + " ORDER BY t.dateDebutSituationLigne";
        TypedQuery<Integer> query = getEntityManager().createQuery(jpql, Integer.class).setMaxResults(1);
        query.setParameter(PARAM_NUM_CONTRAT, numContrat);
        query.setParameter("dateDebutSituationLigne", dateDebutSituationLigne);
        List<Integer> result = query.getResultList();
        return getPremierResultatSiExiste(result);
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public Integer getModeCalculCotisation(String numContrat) {
        TypedQuery<Integer> query = getEntityManager()
                .createQuery(JPQL_PARTIE_REQUETE_TARIF_MOCALCOT + PARAM_NUM_CONTRAT + " ORDER BY t.numCategorie ASC, t.dateDebutSituationLigne DESC",
                        Integer.class)
                .setMaxResults(1);
        query.setParameter(PARAM_NUM_CONTRAT, numContrat);
        List<Integer> resultats = query.getResultList();
        return getPremierResultatSiExiste(resultats);
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public List<Tarif> getSituationsTarifsDesPopulations(String numContrat, Integer dateDebutPlage, Integer dateFinPlage) {
        String jpql = JPQL_PARTIE_REQUETE_TARIF + PARAM_NUM_CONTRAT
                + MessageFormat.format(JPQL_CONDITION_PARAMS_DANS_PLAGE_SITUATION, PARAM_DATE_FIN_PLAGE, PARAM_DATE_DEBUT_PLAGE);
        TypedQuery<Tarif> query = getEntityManager().createQuery(jpql, Tarif.class);
        query.setParameter(PARAM_NUM_CONTRAT, numContrat);
        query.setParameter(PARAM_DATE_DEBUT_PLAGE, dateDebutPlage);
        query.setParameter(PARAM_DATE_FIN_PLAGE, DateRedac.getDateComparable(dateFinPlage));
        return query.getResultList();
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public Set<NumCategorieLibelleCategorie> getDistinctNumCategorieLibelleCategoriePourContratEtDate(String numContrat, Integer dateEffet) {
        String jpql = "SELECT DISTINCT new fr.si2m.red.contrat.NumCategorieLibelleCategorie(t.numCategorie, t.libelleCategorie, t.natureBaseCotisations) FROM Tarif t "
                + JPQL_WHERE_NUMERO_CONTRAT + PARAM_NUM_CONTRAT
                + MessageFormat.format(JPQL_CONDITION_PARAMS_DANS_PLAGE_SITUATION, PARAM_DATE_EFFET, PARAM_DATE_EFFET);
        TypedQuery<NumCategorieLibelleCategorie> query = getEntityManager().createQuery(jpql, NumCategorieLibelleCategorie.class);
        query.setParameter(PARAM_NUM_CONTRAT, numContrat);
        query.setParameter(PARAM_DATE_EFFET, DateRedac.getDateComparable(dateEffet));
        List<NumCategorieLibelleCategorie> listeCouples = query.getResultList();
        return new HashSet<NumCategorieLibelleCategorie>(listeCouples);
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public Set<NumCategorieLibelleCategorie> getDistinctNumCategorieLibelleCategoriePourContratEtPlage(String numContrat, Integer dateDebutPlage,
            Integer dateFinPlage) {
        String jpql = "SELECT DISTINCT new fr.si2m.red.contrat.NumCategorieLibelleCategorie(t.numCategorie, t.libelleCategorie, t.natureBaseCotisations) FROM Tarif t "
                + JPQL_WHERE_NUMERO_CONTRAT + PARAM_NUM_CONTRAT
                + MessageFormat.format(JPQL_CONDITION_PARAMS_DANS_PLAGE_SITUATION, PARAM_DATE_FIN_PLAGE, PARAM_DATE_DEBUT_PLAGE);
        TypedQuery<NumCategorieLibelleCategorie> query = getEntityManager().createQuery(jpql, NumCategorieLibelleCategorie.class);
        query.setParameter(PARAM_NUM_CONTRAT, numContrat);
        query.setParameter(PARAM_DATE_DEBUT_PLAGE, dateDebutPlage);
        query.setParameter(PARAM_DATE_FIN_PLAGE, DateRedac.getDateComparable(dateFinPlage));
        List<NumCategorieLibelleCategorie> listeCouples = query.getResultList();
        return new HashSet<>(listeCouples);
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public Tarif getPourNumContratEtNumCategorieEtDateEffet(String numContrat, String numCategorie, Integer dateEffet) {
        String jpql = JPQL_PARTIE_REQUETE_TARIF + PARAM_NUM_CONTRAT + JPQL_CONDITION_NUM_CATEGORIE
                + MessageFormat.format(JPQL_CONDITION_PARAMS_DANS_PLAGE_SITUATION, PARAM_DATE_EFFET, PARAM_DATE_EFFET);
        TypedQuery<Tarif> query = getEntityManager().createQuery(jpql, Tarif.class).setMaxResults(1);
        query.setParameter(PARAM_NUM_CONTRAT, numContrat);
        query.setParameter(PARAM_NUM_CATEGORIE, numCategorie);
        query.setParameter(PARAM_DATE_EFFET, DateRedac.getDateComparable(dateEffet));
        List<Tarif> result = query.getResultList();
        return getPremierResultatSiExiste(result);
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public String getDernierNumCategorieDeclare(String numContrat, Integer dateDebutPlage, Integer dateFinPlage) {
        String jpql = "SELECT t.numCategorie FROM Tarif t WHERE t.numContrat = :" + PARAM_NUM_CONTRAT
                + MessageFormat.format(JPQL_CONDITION_PARAMS_DANS_PLAGE_SITUATION, PARAM_DATE_FIN_PLAGE, PARAM_DATE_DEBUT_PLAGE)
                + JPQL_ORDER_BY_DATE_DEBUT_SITUATION_DESC;
        TypedQuery<String> query = getEntityManager().createQuery(jpql, String.class).setMaxResults(1);
        query.setParameter(PARAM_NUM_CONTRAT, numContrat);
        query.setParameter(PARAM_DATE_DEBUT_PLAGE, dateDebutPlage);
        query.setParameter(PARAM_DATE_FIN_PLAGE, DateRedac.getDateComparable(dateFinPlage));
        List<String> result = query.getResultList();
        return getPremierResultatSiExiste(result);
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public Tarif getDerniereSituationTarif(String numContrat, String numCategorie, Integer dateDebutPlage, Integer dateFinPlage) {
        String jpql = JPQL_PARTIE_REQUETE_TARIF + PARAM_NUM_CONTRAT + JPQL_CONDITION_NUM_CATEGORIE
                + MessageFormat.format(JPQL_CONDITION_PARAMS_DANS_PLAGE_SITUATION, PARAM_DATE_FIN_PLAGE, PARAM_DATE_DEBUT_PLAGE)
                + JPQL_ORDER_BY_DATE_DEBUT_SITUATION_DESC;
        TypedQuery<Tarif> query = getEntityManager().createQuery(jpql, Tarif.class).setMaxResults(1);
        query.setParameter(PARAM_NUM_CONTRAT, numContrat);
        query.setParameter(PARAM_NUM_CATEGORIE, numCategorie);
        query.setParameter(PARAM_DATE_DEBUT_PLAGE, dateDebutPlage);
        query.setParameter(PARAM_DATE_FIN_PLAGE, DateRedac.getDateComparable(dateFinPlage));
        List<Tarif> result = query.getResultList();
        return getPremierResultatSiExiste(result);
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public Integer getNatureContratPourPeriode(String numContrat, Integer dateDebutPeriode, Integer dateFinPeriode) {
        TypedQuery<Integer> query = getEntityManager().createQuery(JPQL_PARTIE_REQUETE_TARIF_NATURE_CONTRAT + PARAM_NUM_CONTRAT
                + MessageFormat.format(JPQL_CONDITION_PARAMS_DANS_PLAGE_SITUATION, PARAM_DATE_FIN_PLAGE, PARAM_DATE_DEBUT_PLAGE)
                + JPQL_ORDER_BY_DATE_DEBUT_SITUATION_DESC, Integer.class).setMaxResults(1);
        query.setParameter(PARAM_NUM_CONTRAT, numContrat);
        query.setParameter(PARAM_DATE_DEBUT_PLAGE, dateDebutPeriode);
        query.setParameter(PARAM_DATE_FIN_PLAGE, DateRedac.getDateComparable(dateFinPeriode));
        List<Integer> resultats = query.getResultList();
        return getPremierResultatSiExiste(resultats);
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public Integer getNatureContratPourPeriodeMinimalNoCat(String numContrat, Integer dateDebutPeriode, Integer dateFinPeriode) {
        TypedQuery<Integer> query = getEntityManager().createQuery(JPQL_PARTIE_REQUETE_TARIF_NATURE_CONTRAT + PARAM_NUM_CONTRAT
                + MessageFormat.format(JPQL_CONDITION_PARAMS_DANS_PLAGE_SITUATION, PARAM_DATE_FIN_PLAGE, PARAM_DATE_DEBUT_PLAGE)
                + JPQL_ORDER_BY_NOCAT_THEN_DATE_DEBUT_SITUATION_DESC, Integer.class).setMaxResults(1);
        query.setParameter(PARAM_NUM_CONTRAT, numContrat);
        query.setParameter(PARAM_DATE_DEBUT_PLAGE, dateDebutPeriode);
        query.setParameter(PARAM_DATE_FIN_PLAGE, DateRedac.getDateComparable(dateFinPeriode));
        List<Integer> resultats = query.getResultList();
        return getPremierResultatSiExiste(resultats);
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public Integer getModeCalculCotisationPourDetailsContrat(String numContrat) {
        TypedQuery<Integer> query = getEntityManager().createQuery(
                "SELECT t.modeCalculCotisations FROM Tarif t WHERE t.numContrat = :numContrat AND t.ligneEnCoursImportBatch IS FALSE AND t.dateFinSituationLigne IS NULL AND t.numCategorie = (SELECT MIN(tf.numCategorie) FROM Tarif tf WHERE tf.numContrat = :numContrat AND tf.dateFinSituationLigne IS NULL)",
                Integer.class).setMaxResults(1);
        query.setParameter(PARAM_NUM_CONTRAT, numContrat);
        List<Integer> resultats = query.getResultList();
        return getPremierResultatSiExiste(resultats);
    }

    @Override
    public List<String> getDistinctNumCategoriePourContratEtDate(String numContrat, Integer dateEffet) {
        String jpql = "SELECT DISTINCT t.numCategorie FROM Tarif t " + JPQL_WHERE_NUMERO_CONTRAT + PARAM_NUM_CONTRAT
                + MessageFormat.format(JPQL_CONDITION_PARAMS_DANS_PLAGE_SITUATION, PARAM_DATE_EFFET, PARAM_DATE_EFFET);
        TypedQuery<String> query = getEntityManager().createQuery(jpql, String.class);
        query.setParameter(PARAM_NUM_CONTRAT, numContrat);
        query.setParameter(PARAM_DATE_EFFET, DateRedac.getDateComparable(dateEffet));
        return query.getResultList();
    }

    @Override
    public String getNumCategorieDeclareValable(String numContrat, Integer dateDebutPlage) {
        String jpql = "SELECT t.numCategorie FROM Tarif t WHERE t.numContrat = :" + PARAM_NUM_CONTRAT
                + MessageFormat.format(JPQL_CONDITION_PARAMS_DANS_PLAGE_SITUATION, PARAM_DATE_DEBUT_PLAGE, PARAM_DATE_DEBUT_PLAGE)
                + " ORDER BY t.dateDebutSituationLigne";
        TypedQuery<String> query = getEntityManager().createQuery(jpql, String.class).setMaxResults(1);
        query.setParameter(PARAM_NUM_CONTRAT, numContrat);
        query.setParameter(PARAM_DATE_DEBUT_PLAGE, dateDebutPlage);
        List<String> result = query.getResultList();
        return getPremierResultatSiExiste(result);
    }

    /**
     * Mapper du nombre de situations tarifaires retournées
     * 
     * @author richardg
     *
     */
    public static class CountDatesSituationsTarif implements RowMapper<Integer> {

        @Override
        public Integer mapRow(ResultSet rs, int rowNum) throws SQLException {
            return rs.getInt(1); // On retourne la valeur de la colonne 1 de la requete (le count)
        }
    }

    @Override
    public Integer getNombreTarifPourPeriodeDonnee(Integer dateDebutPeriode, Integer dateFinPeriode, String numContrat) {
        String sql = "SELECT COUNT(*) from Tarifs t WHERE t.NOCO = :numContrat "
                + " AND t.DT_DEBUT_SIT > :dateDebutPeriode AND t.DT_DEBUT_SIT <= :dateFinPeriode ";
        Map<String, Object> params = new HashMap<>();
        params.put("dateDebutPeriode", dateDebutPeriode);
        params.put("dateFinPeriode", dateFinPeriode);
        params.put("numContrat", numContrat);

        return getPremierResultatSiExiste(getJdbcTemplate().query(sql, params, new CountDatesSituationsTarif()));

    }

    @Override
    public void updateNatureContratTarif() {
        String sql = "update TARIFS t INNER JOIN CONTRATS c ON t.NOCO = c.NOCO  AND t.DT_DEBUT_SIT <= COALESCE(c.DT_FIN_SIT,99999999) "
                + " AND COALESCE(t.DT_FIN_SIT,99999999) >= c.DT_DEBUT_SIT   INNER JOIN param_famille_contrat pfc   ON c.NOFAM = pfc.NOFAM "
                + " SET NATURE_CONTRAT = CASE WHEN (pfc.TYPE_CONSO_SALAIRE='INDIV' AND t.MOCALCOT=1 )THEN 2 ELSE t.MOCALCOT END WHERE t.TMP_BATCH IS TRUE ";

        Map<String, Object> params = new HashMap<>();
        getJdbcTemplate().update(sql, params);

    }
}
