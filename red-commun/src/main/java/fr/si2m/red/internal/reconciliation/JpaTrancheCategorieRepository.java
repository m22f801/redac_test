package fr.si2m.red.internal.reconciliation;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.TypedQuery;

import org.apache.commons.collections.keyvalue.DefaultMapEntry;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import fr.si2m.red.parametrage.Tranche;
import fr.si2m.red.reconciliation.TrancheCategorie;
import fr.si2m.red.reconciliation.TrancheCategorieRepository;

/**
 * Base de données des entités {@link TrancheCategorie}, connectée via JPA.
 * 
 * @author poidij
 *
 */
@Repository
public class JpaTrancheCategorieRepository extends JpaElementDeclaratifRepository<TrancheCategorie> implements TrancheCategorieRepository {

    private static final String PARAM_ID_PERIODE = "idPeriode";
    private static final String PARAM_NUM_TRANCHE = "numTranche";

    @Override
    public Class<TrancheCategorie> getClassePrototypeEntite() {
        return TrancheCategorie.class;
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public TrancheCategorie get(Long idPeriode, String numCategorieQuittancement, String individu, Integer numTranche) {
        String jpql = "SELECT e FROM TrancheCategorie e WHERE e.idPeriode = :" + PARAM_ID_PERIODE + " "
                + "AND e.numCategorieQuittancement = :numCategorieQuittancement AND e.individu = :individu AND e.numTranche = :" + PARAM_NUM_TRANCHE;
        TypedQuery<TrancheCategorie> query = getEntityManager().createQuery(jpql, TrancheCategorie.class).setMaxResults(1);
        query.setParameter(PARAM_ID_PERIODE, idPeriode);
        query.setParameter(PARAM_NUM_TRANCHE, numTranche);
        query.setParameter("numCategorieQuittancement", numCategorieQuittancement);
        query.setParameter("individu", individu);
        List<TrancheCategorie> resultats = query.getResultList();
        return getPremierResultatSiExiste(resultats);
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public Double getMontantSalaireTranche(Long idPeriode, String numCategorieQuittancement, String individu, Integer numTranche) {
        String jpql = "SELECT SUM(tc.montantTranche) FROM TrancheCategorie tc WHERE tc.idPeriode = :" + PARAM_ID_PERIODE
                + " AND tc.numCategorieQuittancement = :numCategorieQuittancement AND tc.individu = :individu AND tc.numTranche = :" + PARAM_NUM_TRANCHE;
        TypedQuery<Double> query = getEntityManager().createQuery(jpql, Double.class);
        query.setParameter(PARAM_ID_PERIODE, idPeriode);
        query.setParameter(PARAM_NUM_TRANCHE, numTranche);
        query.setParameter("numCategorieQuittancement", numCategorieQuittancement);
        query.setParameter("individu", individu);
        return query.getSingleResult();
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public Map<String, Double> getSommesMontantsTranchePourCategoriesQuittancement(Long idPeriode, Integer numTranche, String libelleTranche,
            List<String> numerosCategorieQuittancement) {
        String jpql = "SELECT new org.apache.commons.collections.keyvalue.DefaultMapEntry(tc.numCategorieQuittancement, SUM(tc.montantTranche)) FROM TrancheCategorie tc "
                + "WHERE tc.idPeriode = :" + PARAM_ID_PERIODE + " AND tc.numTranche = :" + PARAM_NUM_TRANCHE + " AND tc.libelleTranche = :libelleTranche "
                + "AND tc.numCategorieQuittancement IN (:numerosCategorieQuittancement) GROUP BY tc.numCategorieQuittancement ORDER BY tc.numCategorieQuittancement";
        TypedQuery<DefaultMapEntry> query = getEntityManager().createQuery(jpql, DefaultMapEntry.class);
        query.setParameter(PARAM_ID_PERIODE, idPeriode);
        query.setParameter(PARAM_NUM_TRANCHE, numTranche);
        query.setParameter("libelleTranche", libelleTranche);
        query.setParameter("numerosCategorieQuittancement", numerosCategorieQuittancement);
        List<DefaultMapEntry> couples = query.getResultList();
        Map<String, Double> resultats = new HashMap<>();
        for (DefaultMapEntry couple : couples) {
            resultats.put((String) couple.getKey(), (Double) couple.getValue());
        }
        return resultats;
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public Double getSommesMontantsTranche(Long idPeriode, Integer numTranche, String libelleTranche) {
        String jpql = "SELECT SUM(tc.montantTranche) FROM TrancheCategorie tc WHERE tc.idPeriode = :" + PARAM_ID_PERIODE + " AND tc.numTranche = :" + PARAM_NUM_TRANCHE
                + " AND tc.libelleTranche = :libelleTranche";
        TypedQuery<Double> query = getEntityManager().createQuery(jpql, Double.class);
        query.setParameter(PARAM_ID_PERIODE, idPeriode);
        query.setParameter(PARAM_NUM_TRANCHE, numTranche);
        query.setParameter("libelleTranche", libelleTranche);
        List<Double> resultats = query.getResultList();
        return getPremierResultatSiExiste(resultats);
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public List<Tranche> getTranchesDistinctesPourPeriodeTrieesParNumero(Long idPeriode) {
        String jpql = "SELECT DISTINCT new fr.si2m.red.parametrage.Tranche(tc.numTranche, tc.libelleTranche) FROM TrancheCategorie tc WHERE tc.idPeriode = :" + PARAM_ID_PERIODE
                + " ORDER BY tc.numTranche";
        TypedQuery<Tranche> query = getEntityManager().createQuery(jpql, Tranche.class);
        query.setParameter(PARAM_ID_PERIODE, idPeriode);
        return query.getResultList();
    }
}
