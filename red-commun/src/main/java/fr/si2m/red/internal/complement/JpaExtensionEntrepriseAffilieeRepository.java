package fr.si2m.red.internal.complement;

import java.util.List;

import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import fr.si2m.red.complement.ExtensionEntrepriseAffiliee;
import fr.si2m.red.complement.ExtensionEntrepriseAffilieeRepository;
import fr.si2m.red.core.repository.jpa.JpaEntiteImportableRepository;

/**
 * Base de données des entités {@link ExtensionEntrepriseAffiliee}, connectée via JPA.
 * 
 * @author nortaina
 *
 */
@Repository
public class JpaExtensionEntrepriseAffilieeRepository extends JpaEntiteImportableRepository<ExtensionEntrepriseAffiliee>
        implements ExtensionEntrepriseAffilieeRepository {

    private static final String PARAM_NUM_CONTRAT = "numContrat";
    private static final String PARAM_SIREN = "siren";
    private static final String PARAM_NIC = "nic";

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public ExtensionEntrepriseAffiliee getExtensionEntrepriseAffilieeComplementaire(String siren) {

        TypedQuery<ExtensionEntrepriseAffiliee> query = getEntityManager()
                .createQuery("SELECT e FROM ExtensionEntrepriseAffiliee e WHERE siren = :siren", ExtensionEntrepriseAffiliee.class);
        query.setParameter(PARAM_SIREN, siren);
        List<ExtensionEntrepriseAffiliee> result = query.getResultList();
        if (result.isEmpty()) {
            return null;
        }
        return result.get(0);
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public ExtensionEntrepriseAffiliee getExtensionEtablissementAffilieeComplementaire(String siren, String nic) {

        TypedQuery<ExtensionEntrepriseAffiliee> query = getEntityManager()
                .createQuery("SELECT e FROM ExtensionEntrepriseAffiliee e WHERE siren = :siren AND nic = :nic", ExtensionEntrepriseAffiliee.class);
        query.setParameter(PARAM_SIREN, siren);
        query.setParameter(PARAM_NIC, nic);
        List<ExtensionEntrepriseAffiliee> result = query.getResultList();
        if (result.isEmpty()) {
            return null;
        }
        return result.get(0);
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public List<String> getSirenEntreprisesAffiliees(String numContrat) {
        TypedQuery<String> query = getEntityManager()
                .createQuery("SELECT DISTINCT e.siren FROM ExtensionEntrepriseAffiliee e WHERE e.numContrat= :numContrat", String.class);
        query.setParameter(PARAM_NUM_CONTRAT, numContrat);
        return query.getResultList();
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public List<String> getSiretEtablissementsAffilies(String numContrat) {
        TypedQuery<String> query = getEntityManager().createQuery(
                "SELECT DISTINCT(CONCAT(e.siren, e.nic)) FROM ExtensionEntrepriseAffiliee e WHERE e.nic IS NOT NULL AND trim(e.nic) != '' AND e.numContrat= :numContrat",
                String.class);
        query.setParameter(PARAM_NUM_CONTRAT, numContrat);
        return query.getResultList();
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public List<String> getSiretEtablissementsAffiliesPourAffichage(String numContrat) {
        TypedQuery<String> query = getEntityManager().createQuery(
                "SELECT DISTINCT(CONCAT(e.siren, ' ', coalesce(e.nic, ''))) FROM ExtensionEntrepriseAffiliee e WHERE e.numContrat= :numContrat",
                String.class);
        query.setParameter(PARAM_NUM_CONTRAT, numContrat);
        return query.getResultList();
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public List<ExtensionEntrepriseAffiliee> getEntreprisesAffiliees(String numContrat) {
        TypedQuery<ExtensionEntrepriseAffiliee> query = getEntityManager()
                .createQuery("SELECT e FROM ExtensionEntrepriseAffiliee e WHERE e.numContrat= :numContrat", ExtensionEntrepriseAffiliee.class);
        query.setParameter(PARAM_NUM_CONTRAT, numContrat);
        return query.getResultList();
    }

    @Override
    @Transactional(propagation = Propagation.MANDATORY)
    public int supprimeEntrepriseAffiliee(String numContrat, String siren, String nic) {
        String jpql = "DELETE FROM ExtensionEntrepriseAffiliee e WHERE e.numContrat= :numContrat AND e.siren = :siren AND coalesce(e.nic, '') = coalesce(:nic, '')";
        Query query = getEntityManager().createQuery(jpql);
        query.setParameter(PARAM_NUM_CONTRAT, numContrat);
        query.setParameter(PARAM_SIREN, siren);
        query.setParameter(PARAM_NIC, nic);
        return query.executeUpdate();
    }

}
