package fr.si2m.red.internal.reconciliation;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.MessageFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.apache.commons.lang3.StringUtils;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import fr.si2m.red.DateRedac;
import fr.si2m.red.core.repository.jpa.JpaRepository;
import fr.si2m.red.dsn.MessageControleExportExcel;
import fr.si2m.red.parametrage.ParamControleSignalRejet;
import fr.si2m.red.reconciliation.ExportIndividuExcel;
import fr.si2m.red.reconciliation.MessageControle;
import fr.si2m.red.reconciliation.MessageControleRepository;

/**
 * Base de données des entités {@link MessageControle}, connectée via JPA.
 * 
 * @author poidij
 *
 */
@Repository
public class JpaMessageControleRepository extends JpaRepository<MessageControle> implements MessageControleRepository {

    private static final String PARAM_ID_PERIODE = "idPeriode";
    private static final String PARAM_INDIVIDU = "individu";
    private static final String PARAM_NIVEAU_ALERTE = "niveauAlerte";
    private static final String PARAM_ORIGINE = "origine";
    private static final String PARAM_ID_CONTROLE = "identifiantControle";
    private static final String PARAM_LISTE_ID_PERIODES = "idsPeriodes";

    private static final String JPA_CLAUSE_POUR_PERIODE = "m.idPeriode = :" + PARAM_ID_PERIODE;
    private static final String JPA_SELECT_POUR_PERIODE = "SELECT m FROM MessageControle m WHERE " + JPA_CLAUSE_POUR_PERIODE;
    private static final String JPA_CLAUSE_POUR_PERIODE_ET_ORIGINE_MSG = JPA_CLAUSE_POUR_PERIODE + " AND m.origineMessage = ''{0}''";
    private static final String JPA_DELETE_POUR_MESSAGE_CONTROLE = "DELETE FROM MessageControle m WHERE ";
    private static final String JDBC_DELETE_MSG_CONDITION_ID_CONTROLE = "DELETE FROM message_controle WHERE ID_CONTROLE='";
    private static final String JDBC_CONDITION_ORIGINE = "' AND ORIGINE='";
    private static final String JDBC_CONDITION_IN_PERIODE = "' AND ID_PERIODE IN (";

    @Override
    public Class<MessageControle> getClassePrototypeEntite() {
        return MessageControle.class;
    }

    @Override
    @Transactional(propagation = Propagation.MANDATORY)
    public int supprimeMessagesRedacPourPeriode(Long idPeriode) {
        String jpql = JPA_DELETE_POUR_MESSAGE_CONTROLE
                + MessageFormat.format(JPA_CLAUSE_POUR_PERIODE_ET_ORIGINE_MSG, MessageControle.ORIGINE_MESSAGE_REDAC);
        Query query = getEntityManager().createQuery(jpql);
        query.setParameter(PARAM_ID_PERIODE, idPeriode);
        return query.executeUpdate();
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public boolean existeMessageNiveauRejet(Long idPeriode) {
        TypedQuery<Long> query = getEntityManager()
                .createQuery("SELECT count(*) FROM MessageControle m WHERE " + JPA_CLAUSE_POUR_PERIODE + " AND m.niveauAlerte = 'REJET'", Long.class);
        query.setParameter(PARAM_ID_PERIODE, idPeriode);
        List<Long> result = query.getResultList();
        if (result.isEmpty() || result.get(0) == null) {
            return false;
        }
        return result.get(0) > 0;
    }

    @Override
    @Transactional(propagation = Propagation.MANDATORY)
    public int supprimeMessagesSIAvalPourPeriode(Long idPeriode) {
        String jpql = JPA_DELETE_POUR_MESSAGE_CONTROLE
                + MessageFormat.format(JPA_CLAUSE_POUR_PERIODE_ET_ORIGINE_MSG, MessageControle.ORIGINE_MESSAGE_SIAVAL);
        Query query = getEntityManager().createQuery(jpql);
        query.setParameter(PARAM_ID_PERIODE, idPeriode);
        return query.executeUpdate();
    }

    @Override
    @Transactional(propagation = Propagation.MANDATORY)
    public List<MessageControle> getPourPeriode(Long idPeriode) {
        TypedQuery<MessageControle> query = getEntityManager().createQuery(JPA_SELECT_POUR_PERIODE, MessageControle.class);
        query.setParameter(PARAM_ID_PERIODE, idPeriode);
        return query.getResultList();
    }

    @Override
    @Transactional(propagation = Propagation.MANDATORY)
    public List<MessageControle> getGenerauxPourPeriode(Long idPeriode) {
        String jpql = JPA_SELECT_POUR_PERIODE + " AND (m.individu IS NULL OR m.individu = '')";
        TypedQuery<MessageControle> query = getEntityManager().createQuery(jpql, MessageControle.class);
        query.setParameter(PARAM_ID_PERIODE, idPeriode);
        return query.getResultList();
    }

    @Override
    @Transactional(propagation = Propagation.MANDATORY)
    public MessageControle getPourPeriodeEtIdentifiantControle(Long idPeriode, String identifiantControle) {
        String jpql = JPA_SELECT_POUR_PERIODE + " AND m.identifiantControle = :identifiantControle";
        TypedQuery<MessageControle> query = getEntityManager().createQuery(jpql, MessageControle.class).setMaxResults(1);
        query.setParameter(PARAM_ID_PERIODE, idPeriode);
        query.setParameter(PARAM_ID_CONTROLE, identifiantControle);
        return getPremierResultatSiExiste(query.getResultList());
    }

    @Override
    @Transactional(propagation = Propagation.MANDATORY)
    public MessageControle getMessagesPeriodesAffectationInvalidesPourPeriode(Long idPeriode, String periodeAffectation) {
        String jpql = JPA_SELECT_POUR_PERIODE + " AND m.identifiantControle = '" + ParamControleSignalRejet.ID_PERIODE_VERSEMENT
                + "' AND m.paramMessage1 = :periodeAffectation";
        TypedQuery<MessageControle> query = getEntityManager().createQuery(jpql, MessageControle.class).setMaxResults(1);
        query.setParameter(PARAM_ID_PERIODE, idPeriode);
        query.setParameter("periodeAffectation", periodeAffectation);
        return getPremierResultatSiExiste(query.getResultList());
    }

    @Override
    @Transactional(propagation = Propagation.MANDATORY)
    public List<MessageControle> getPourPeriodeEtNiveauAlerte(Long idPeriode, String niveauAlerte) {
        String jpql = JPA_SELECT_POUR_PERIODE + " AND m.niveauAlerte = :niveauAlerte";
        TypedQuery<MessageControle> query = getEntityManager().createQuery(jpql, MessageControle.class);
        query.setParameter(PARAM_ID_PERIODE, idPeriode);
        query.setParameter(PARAM_NIVEAU_ALERTE, niveauAlerte);
        return query.getResultList();
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public Long comptePourPeriode(Long idPeriode, String niveauAlerte, String origineMessage) {
        String jpql = "SELECT count(*) FROM MessageControle m WHERE m.idPeriode = :idPeriode AND m.niveauAlerte = :niveauAlerte AND m.origineMessage = :origineMessage";
        TypedQuery<Long> query = getEntityManager().createQuery(jpql, Long.class).setMaxResults(1);
        query.setParameter(PARAM_ID_PERIODE, idPeriode);
        query.setParameter(PARAM_NIVEAU_ALERTE, niveauAlerte);
        query.setParameter("origineMessage", origineMessage);
        return getPremierResultatSiExiste(query.getResultList());
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public List<MessageControle> getPourPeriodeEtIndividu(Long idPeriode, String individu) {
        String jpql = "SELECT m FROM MessageControle m WHERE m.idPeriode = :idPeriode AND m.individu = :individu";
        TypedQuery<MessageControle> query = getEntityManager().createQuery(jpql, MessageControle.class);
        query.setParameter(PARAM_ID_PERIODE, idPeriode);
        query.setParameter(PARAM_INDIVIDU, individu);
        return query.getResultList();
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public List<MessageControle> getPourPeriodeEtIndividuEtNiveauAlerte(Long idPeriode, ExportIndividuExcel individu, String niveauAlerte) {
        String jpql = "SELECT m FROM MessageControle m WHERE m.idPeriode = :idPeriode AND m.niveauAlerte = :niveauAlerte AND m.individu = :individu";
        TypedQuery<MessageControle> query = getEntityManager().createQuery(jpql, MessageControle.class);
        query.setParameter(PARAM_ID_PERIODE, idPeriode);
        query.setParameter(PARAM_NIVEAU_ALERTE, niveauAlerte);
        String nir = individu.getIdentifiantRepertoire();
        if (StringUtils.isNotBlank(nir)) {
            query.setParameter(PARAM_INDIVIDU, nir);
        } else {
            query.setParameter(PARAM_INDIVIDU, individu.getNumeroTechniqueTemporaire());
        }
        return query.getResultList();
    }

    @Override
    public boolean existeMessageRejetPourBaseAssujettiePourPeriode(Long idPeriode) {
        TypedQuery<Long> query = getEntityManager().createQuery("SELECT count(*) FROM MessageControle m"
                + " WHERE m.idPeriode = :idPeriode AND m.niveauAlerte = 'REJET' AND m.identifiantControle='RED_TYPE_BASE_ASSUJ'", Long.class);
        query.setParameter(PARAM_ID_PERIODE, idPeriode);
        List<Long> result = query.getResultList();
        if (result.isEmpty() || result.get(0) == null) {
            return false;
        }
        return result.get(0) > 0;
    }

    @Override
    public boolean existeMessageGestPourPeriodeEtControle(Long idPeriode, String idControle) {
        TypedQuery<Long> query = getEntityManager().createQuery("SELECT count(*) FROM MessageControle m WHERE m.idPeriode=:idPeriode"
                + " AND m.identifiantControle=:identifiantControle AND m.origineMessage='GEST'", Long.class);
        query.setParameter(PARAM_ID_PERIODE, idPeriode);
        query.setParameter(PARAM_ID_CONTROLE, idControle);
        List<Long> result = query.getResultList();
        if (result.isEmpty() || result.get(0) == null) {
            return false;
        }
        return result.get(0) > 0;
    }

    @Override
    public int supprimeMessageGestPourPeriodeEtControle(Long idPeriode, String idControle) {
        String jpql = "DELETE FROM MessageControle m WHERE m.idPeriode= :idPeriode" + " AND m.identifiantControle= :identifiantControle "
                + " AND m.origineMessage='GEST'";
        Query query = getEntityManager().createQuery(jpql);
        query.setParameter(PARAM_ID_PERIODE, idPeriode);
        query.setParameter(PARAM_ID_CONTROLE, idControle);
        return query.executeUpdate();
    }

    @Override
    public int supprimeMessageControlePourPeriode(Long idPeriode) {
        String jpql = "DELETE FROM MessageControle m WHERE m.idPeriode=:idPeriode";
        Query query = getEntityManager().createQuery(jpql);
        query.setParameter(PARAM_ID_PERIODE, idPeriode);
        return query.executeUpdate();
    }

    @Override
    @Transactional(propagation = Propagation.MANDATORY)
    public MessageControle getPourPeriodeEtIdentifiantControleEtOrigine(Long idPeriode, String identifiantControle, String origine) {
        String jpql = JPA_SELECT_POUR_PERIODE + " AND m.identifiantControle = :identifiantControle AND m.origineMessage= :origine ";
        TypedQuery<MessageControle> query = getEntityManager().createQuery(jpql, MessageControle.class).setMaxResults(1);
        query.setParameter(PARAM_ID_PERIODE, idPeriode);
        query.setParameter(PARAM_ID_CONTROLE, identifiantControle);
        query.setParameter(PARAM_ORIGINE, origine);
        return getPremierResultatSiExiste(query.getResultList());
    }

    @Override
    public MessageControleExportExcel getMessagesSignalRejetPourIndividu(Long idPeriode, String individu) {
        String sql = "SELECT  CAST(GROUP_CONCAT((CASE m.NIVEAU_ALERTE WHEN 'SIGNAL' THEN m.MESSAGE_UTILISATEUR "
                + " END) SEPARATOR ', ') AS CHAR CHARSET UTF8) AS MSG_INDIV_SIGNAL, CAST(GROUP_CONCAT((CASE m.NIVEAU_ALERTE"
                + " WHEN 'REJET' THEN m.MESSAGE_UTILISATEUR END) SEPARATOR ', ') AS CHAR CHARSET UTF8) AS MSG_INDIV_REJET"
                + " FROM MESSAGE_CONTROLE m  WHERE m.ID_PERIODE= :" + PARAM_ID_PERIODE + " AND m.INDIVIDU = :" + PARAM_INDIVIDU;

        Map<String, Object> params = new HashMap<>();
        params.put(PARAM_ID_PERIODE, idPeriode);
        params.put(PARAM_INDIVIDU, individu);

        return getPremierResultatSiExiste(getJdbcTemplate().query(sql, params, new MessageControleExportExcelRowMapper()));

    }

    /**
     * Mapper de lignes de messages controles pour un individu, une période
     * 
     * @author eudesr
     *
     */
    public static class MessageControleExportExcelRowMapper implements RowMapper<MessageControleExportExcel> {
        @Override
        public MessageControleExportExcel mapRow(ResultSet rs, int rowNum) throws SQLException {
            MessageControleExportExcel messagesIndividu = new MessageControleExportExcel();
            messagesIndividu.setMessageUtilisateurRejet(rs.getString("MSG_INDIV_REJET"));
            messagesIndividu.setMessageUtilisateurSignal(rs.getString("MSG_INDIV_SIGNAL"));
            return messagesIndividu;
        }
    }

    @Override
    public int supprimeMessagesSaufOriginePourPeriode(Long idPeriode, String origine) {
        String jpql = JPA_DELETE_POUR_MESSAGE_CONTROLE + "m.idPeriode = :" + PARAM_ID_PERIODE + " AND m.origineMessage <> :" + PARAM_ORIGINE;
        Query query = getEntityManager().createQuery(jpql);
        query.setParameter(PARAM_ID_PERIODE, idPeriode);
        query.setParameter(PARAM_ORIGINE, origine);
        return query.executeUpdate();
    }

    @Override
    @Transactional(propagation = Propagation.MANDATORY)
    public void callMysqlProcedure(String sql, Map<String, Object> params) {
        getJdbcTemplate().update(sql, params);
    }

    @Override
    public void blocageEnMasse(List<Long> idsPeriodes, ParamControleSignalRejet paramControleSignalRejet, String user) {
        String sql = "call Creation_MessageControle_en_masse( :" + PARAM_LISTE_ID_PERIODES + ","
                + ":message , :idControle , :param1 , :param2 , :param3 , :param4 , :niveau , :origine , :dateJour , :userCreation )";
        Map<String, Object> params = new HashMap<>();
        params.put(PARAM_LISTE_ID_PERIODES, StringUtils.join(idsPeriodes, ","));
        params.put("message", paramControleSignalRejet != null ? paramControleSignalRejet.getMessageAlerte() : StringUtils.EMPTY);
        params.put("param1", null);
        params.put("param2", null);
        params.put("param3", null);
        params.put("param4", null);
        params.put("idControle", MessageControle.IDENTIFIANT_CONTROLE_GEST_REJET);
        params.put("niveau", paramControleSignalRejet != null ? paramControleSignalRejet.getNiveauAlerte() : StringUtils.EMPTY);
        params.put(PARAM_ORIGINE, MessageControle.ORIGINE_MESSAGE_GEST);
        params.put("dateJour", DateRedac.maintenant());
        params.put("userCreation", user);

        callMysqlProcedure(sql, params);

    }

    @Override
    public int deblocageEnMasse(List<Long> idsPeriodes) {
        String sql = JDBC_DELETE_MSG_CONDITION_ID_CONTROLE + MessageControle.IDENTIFIANT_CONTROLE_GEST_REJET + JDBC_CONDITION_ORIGINE
                + MessageControle.ORIGINE_MESSAGE_GEST + JDBC_CONDITION_IN_PERIODE
                + org.springframework.util.StringUtils.collectionToDelimitedString(idsPeriodes, ",", "'", "'") + ")";

        Map<String, Object> params = new HashMap<>();

        return getJdbcTemplate().update(sql, params);

    }

    @Override
    public void miseEnAttenteEnMasse(List<Long> idsPeriodes, ParamControleSignalRejet paramControleSignalRejet, String user) {
        String sql = "call Creation_MessageControle_en_masse( :" + PARAM_LISTE_ID_PERIODES + ","
                + ":message , :idControle , :param1 , :param2 , :param3 , :param4 , :niveau , :origine , :dateJour , :userCreation )";
        Map<String, Object> params = new HashMap<>();
        params.put(PARAM_LISTE_ID_PERIODES, StringUtils.join(idsPeriodes, ","));
        params.put("message", paramControleSignalRejet != null ? paramControleSignalRejet.getMessageAlerte() : StringUtils.EMPTY);
        params.put("param1", null);
        params.put("param2", null);
        params.put("param3", null);
        params.put("param4", null);
        params.put("idControle", MessageControle.IDENTIFIANT_CONTROLE_GEST_RETOUR_ETP);
        params.put("niveau", paramControleSignalRejet != null ? paramControleSignalRejet.getNiveauAlerte() : StringUtils.EMPTY);
        params.put(PARAM_ORIGINE, MessageControle.ORIGINE_MESSAGE_GEST);
        params.put("dateJour", DateRedac.maintenant());
        params.put("userCreation", user);

        callMysqlProcedure(sql, params);

    }

    @Override
    public int leveeMiseEnAttenteEnMasse(List<Long> idsPeriodes) {
        String sql = JDBC_DELETE_MSG_CONDITION_ID_CONTROLE + MessageControle.IDENTIFIANT_CONTROLE_GEST_RETOUR_ETP + JDBC_CONDITION_ORIGINE
                + MessageControle.ORIGINE_MESSAGE_GEST + JDBC_CONDITION_IN_PERIODE
                + org.springframework.util.StringUtils.collectionToDelimitedString(idsPeriodes, ",", "'", "'") + ")";

        Map<String, Object> params = new HashMap<>();

        return getJdbcTemplate().update(sql, params);

    }

    @Override
    public int supressionMessageControleEnMasse(List<Long> idsPeriodes, String origine, String idControle) {

        String sql = JDBC_DELETE_MSG_CONDITION_ID_CONTROLE + idControle + JDBC_CONDITION_ORIGINE + origine + JDBC_CONDITION_IN_PERIODE
                + org.springframework.util.StringUtils.collectionToDelimitedString(idsPeriodes, ",", "'", "'") + ")";

        Map<String, Object> params = new HashMap<>();

        return getJdbcTemplate().update(sql, params);

    }

}
