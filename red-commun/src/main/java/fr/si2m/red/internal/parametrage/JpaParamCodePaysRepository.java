package fr.si2m.red.internal.parametrage;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import fr.si2m.red.core.repository.jpa.JpaEntiteImportableRepository;
import fr.si2m.red.parametrage.ParamCodePays;
import fr.si2m.red.parametrage.ParamCodePaysId;
import fr.si2m.red.parametrage.ParamCodePaysRepository;

/**
 * Base de données des entités ParamCodePays, connectée via JPA.
 * 
 * @author eudesr
 *
 */
@Repository
public class JpaParamCodePaysRepository extends JpaEntiteImportableRepository<ParamCodePays> implements ParamCodePaysRepository {

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public String getCodePaysInsee(String codePays) {
        ParamCodePays paramCodePays = getEntityManager().find(ParamCodePays.class, new ParamCodePaysId(false, codePays));
        if (paramCodePays != null) {
            getEntityManager().detach(paramCodePays);
            return paramCodePays.getCodeInsee();
        } else {
            return null;
        }
    }

}
