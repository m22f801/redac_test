package fr.si2m.red.internal.reconciliation;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.TypedQuery;

import org.apache.commons.collections.keyvalue.DefaultMapEntry;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import fr.si2m.red.reconciliation.IdentifiantsIndividu;
import fr.si2m.red.reconciliation.TrancheCategorieBaseAssujettie;
import fr.si2m.red.reconciliation.TrancheCategorieBaseAssujettieRepository;

/**
 * Base de données des entités {@link TrancheCategorieBaseAssujettie}, connectée via JPA.
 * 
 * @author nortaina
 *
 */
@Repository
public class JpaTrancheCategorieBaseAssujettieRepository extends JpaElementDeclaratifRepository<TrancheCategorieBaseAssujettie>
        implements TrancheCategorieBaseAssujettieRepository {
    private static final String PARAM_ID_PERIODE = "idPeriode";
    private static final String PARAM_NUM_CATEGORIE = "numCategorie";
    private static final String PARAM_NUM_TRANCHE = "numTranche";

    /**
     * La clause sur le numéro de catégorie des tranches catégories de bases assujetties pour les requêtes en JPQL.
     */
    private static final String JPQL_CLAUSE_NUMERO_CATEGORIE_QUITTANCEMENT = " AND tcba.numCategorieQuittancement = :";

    /**
     * La clause sur le numéro de tranche pour les requêtes en JPQL.
     */
    private static final String JPQL_CLAUSE_NUMERO_TRANCHE = " AND tcba.numTranche = :";

    /**
     * Le début de requête des sommes de montants des tranches catégories de bases assujetties liées à une période donnée pour les requêtes en JPQL.
     */
    private static final String SQL_PARTIE_REQUETE_TRANCHE_CATEGORIE_BASE_ASSUJETTIE = "SELECT SUM(tcba.montantTranche) FROM TrancheCategorieBaseAssujettie tcba WHERE tcba.idPeriode = :";

    private static final String JPA_CLAUSE_EXISTS_TCBA = "ba.idBaseAssujettie IN (SELECT tcba.idBaseAssujettie FROM TrancheCategorieBaseAssujettie tcba WHERE tcba.idPeriode = :"
            + PARAM_ID_PERIODE + JPQL_CLAUSE_NUMERO_CATEGORIE_QUITTANCEMENT + PARAM_NUM_CATEGORIE + ")";

    @Override
    public Class<TrancheCategorieBaseAssujettie> getClassePrototypeEntite() {
        return TrancheCategorieBaseAssujettie.class;
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public TrancheCategorieBaseAssujettie get(Long idPeriode, Integer moisRattachement, String idAffiliation, Integer dateDebutRattachementBase,
            Integer numTranche, String numCategorie) {
        String jpql = "SELECT e FROM TrancheCategorieBaseAssujettie e WHERE e.idPeriode = :" + PARAM_ID_PERIODE
                + " AND e.moisRattachement = :moisRattachement AND e.idAffiliation = :idAffiliation " + "AND e.numCategorieQuittancement = :"
                + PARAM_NUM_CATEGORIE + " AND e.dateDebutRattachementBase = :dateDebutRattachementBase " + "AND e.numTranche = :" + PARAM_NUM_TRANCHE;
        TypedQuery<TrancheCategorieBaseAssujettie> query = getEntityManager().createQuery(jpql, TrancheCategorieBaseAssujettie.class)
                .setMaxResults(1);
        query.setParameter(PARAM_ID_PERIODE, idPeriode);
        query.setParameter(PARAM_NUM_CATEGORIE, numCategorie);
        query.setParameter(PARAM_NUM_TRANCHE, numTranche);
        query.setParameter("moisRattachement", moisRattachement);
        query.setParameter("idAffiliation", idAffiliation);
        query.setParameter("dateDebutRattachementBase", dateDebutRattachementBase);
        List<TrancheCategorieBaseAssujettie> resultats = query.getResultList();
        return getPremierResultatSiExiste(resultats);
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public double getMontantCotisationTousPourPeriodeEtNumCategorie(Long idPeriode, String numCategorie) {
        // Somme des montantCotisation des lignes de l’entité BaseAssujettie rattachées à la PeriodeRecue pour lesquelles il existe au moins une ligne dans
        // l’entité TrancheCategorieBaseAssujettie ayant le numeroCategorieQuittancement en cours
        String jpql = "SELECT SUM(ba.montantCotisation) FROM BaseAssujettie ba WHERE " + JPA_CLAUSE_EXISTS_TCBA;
        TypedQuery<Double> query = getEntityManager().createQuery(jpql, Double.class).setMaxResults(1);
        query.setParameter(PARAM_ID_PERIODE, idPeriode);
        query.setParameter(PARAM_NUM_CATEGORIE, numCategorie);
        return getResultatSomme(query);
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public double getMontantCotisationIndivPourPeriodeEtNumCategorieEtIndividu(Long idPeriode, String numCategorie, IdentifiantsIndividu individu) {
        // Somme des montantCotisation des lignes de l’entité BaseAssujettie rattachées à la PeriodeRecue et à l’individu pour lesquelles il existe une ligne
        // dans l’entité TrancheCategorieBaseAssujettie ayant le numeroCategorieQuittancement en cours
        StringBuilder jpql = new StringBuilder(
                "SELECT SUM(ba.montantCotisation) FROM BaseAssujettie ba LEFT JOIN ba.affiliation a LEFT JOIN a.contratTravail ct LEFT JOIN ct.individu i WHERE ");
        if (individu.isIsidentifiantRepertoire()) {
            jpql.append("i.identifiantRepertoire = :identifiantRepertoire ");
        } else {
            jpql.append("i.numeroTechniqueTemporaire = :numeroTechniqueTemporaire ");
        }
        jpql.append("AND ").append(JPA_CLAUSE_EXISTS_TCBA);

        TypedQuery<Double> query = getEntityManager().createQuery(jpql.toString(), Double.class);
        query.setParameter(PARAM_ID_PERIODE, idPeriode);
        query.setParameter(PARAM_NUM_CATEGORIE, numCategorie);
        if (individu.isIsidentifiantRepertoire()) {
            query.setParameter("identifiantRepertoire", individu.getIdentifiantRepertoireOuNumeroTechniqueTemporaire());
        } else {
            query.setParameter("numeroTechniqueTemporaire", individu.getIdentifiantRepertoireOuNumeroTechniqueTemporaire());
        }
        return getResultatSomme(query);
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public double getMontantTranchePourPeriodeEtNumCategorie(Integer numTranche, Long idPeriode, String numCategorie) {
        String jpql = SQL_PARTIE_REQUETE_TRANCHE_CATEGORIE_BASE_ASSUJETTIE + PARAM_ID_PERIODE + JPQL_CLAUSE_NUMERO_CATEGORIE_QUITTANCEMENT
                + PARAM_NUM_CATEGORIE + JPQL_CLAUSE_NUMERO_TRANCHE + PARAM_NUM_TRANCHE;
        TypedQuery<Double> query = getEntityManager().createQuery(jpql, Double.class).setMaxResults(1);
        query.setParameter(PARAM_ID_PERIODE, idPeriode);
        query.setParameter(PARAM_NUM_CATEGORIE, numCategorie);
        query.setParameter(PARAM_NUM_TRANCHE, numTranche);
        return getResultatSomme(query);
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public double getMontantTranchePourPeriode(Integer numTranche, Long idPeriode) {
        String jpql = SQL_PARTIE_REQUETE_TRANCHE_CATEGORIE_BASE_ASSUJETTIE + PARAM_ID_PERIODE + JPQL_CLAUSE_NUMERO_TRANCHE + PARAM_NUM_TRANCHE;
        TypedQuery<Double> query = getEntityManager().createQuery(jpql, Double.class).setMaxResults(1);
        query.setParameter(PARAM_ID_PERIODE, idPeriode);
        query.setParameter(PARAM_NUM_TRANCHE, numTranche);
        return getResultatSomme(query);
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public Double getMontantTranchePourBaseAssujettie(Long idPeriode, String idBaseAssujettie, int numTranche) {
        String jpql = "SELECT tcba.montantTranche FROM TrancheCategorieBaseAssujettie tcba WHERE tcba.idPeriode = :" + PARAM_ID_PERIODE
                + JPQL_CLAUSE_NUMERO_TRANCHE + PARAM_NUM_TRANCHE + " AND tcba.idBaseAssujettie = :idBaseAssujettie";
        TypedQuery<Double> query = getEntityManager().createQuery(jpql, Double.class).setMaxResults(1);
        query.setParameter(PARAM_ID_PERIODE, idPeriode);
        query.setParameter("idBaseAssujettie", idBaseAssujettie);
        query.setParameter(PARAM_NUM_TRANCHE, numTranche);
        return getPremierResultatSiExiste(query.getResultList());
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public double getMontantTranchePourPeriodeEtNumCategorieEtIndividu(Integer numTranche, Long idPeriode, String numCategorie, String individu) {
        String jpql = "SELECT SUM(tcba.montantTranche) FROM TrancheCategorieBaseAssujettie tcba"
                + " LEFT JOIN tcba.affiliation a LEFT JOIN a.contratTravail ct LEFT JOIN ct.individu i WHERE tcba.idPeriode = :" + PARAM_ID_PERIODE
                + JPQL_CLAUSE_NUMERO_CATEGORIE_QUITTANCEMENT + PARAM_NUM_CATEGORIE + JPQL_CLAUSE_NUMERO_TRANCHE + PARAM_NUM_TRANCHE
                + " AND (i.identifiantRepertoire = :individu OR i.numeroTechniqueTemporaire = :individu)";
        TypedQuery<Double> query = getEntityManager().createQuery(jpql, Double.class).setMaxResults(1);
        query.setParameter(PARAM_ID_PERIODE, idPeriode);
        query.setParameter(PARAM_NUM_CATEGORIE, numCategorie);
        query.setParameter(PARAM_NUM_TRANCHE, numTranche);
        query.setParameter("individu", individu);
        return getResultatSomme(query);
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public double getSommeMontantTranchePourAffiliation(Long idPeriode, String idAffiliation, Integer numTranche) {
        String jpql = SQL_PARTIE_REQUETE_TRANCHE_CATEGORIE_BASE_ASSUJETTIE + PARAM_ID_PERIODE
                + " AND tcba.idAffiliation = :idAffiliation AND tcba.numTranche = :" + PARAM_NUM_TRANCHE;
        TypedQuery<Double> query = getEntityManager().createQuery(jpql, Double.class).setMaxResults(1);
        query.setParameter(PARAM_ID_PERIODE, idPeriode);
        query.setParameter(PARAM_NUM_TRANCHE, numTranche);
        query.setParameter("idAffiliation", idAffiliation);
        return getResultatSomme(query);
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public String getLibelleTranchePourPeriodeEtNumTranche(Long idPeriode, int numTranche) {
        String jpql = "SELECT tcba.libelleTranche FROM TrancheCategorieBaseAssujettie tcba WHERE tcba.idPeriode = :" + PARAM_ID_PERIODE
                + JPQL_CLAUSE_NUMERO_TRANCHE + PARAM_NUM_TRANCHE + " ORDER BY tcba.idTechnique";
        TypedQuery<String> query = getEntityManager().createQuery(jpql, String.class).setMaxResults(1);
        query.setParameter(PARAM_ID_PERIODE, idPeriode);
        query.setParameter(PARAM_NUM_TRANCHE, numTranche);
        return getPremierResultatSiExiste(query.getResultList());
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public Map<Integer, Double> getMontantsTranchesPourBaseAssujettie(Long idPeriode, String idBaseAssujettie) {
        String jpql = "SELECT new org.apache.commons.collections.keyvalue.DefaultMapEntry(tcba.numTranche, tcba.montantTranche) FROM TrancheCategorieBaseAssujettie tcba WHERE tcba.idPeriode = :"
                + PARAM_ID_PERIODE + " AND tcba.idBaseAssujettie = :idBaseAssujettie";
        TypedQuery<DefaultMapEntry> query = getEntityManager().createQuery(jpql, DefaultMapEntry.class);
        query.setParameter(PARAM_ID_PERIODE, idPeriode);
        query.setParameter("idBaseAssujettie", idBaseAssujettie);
        List<DefaultMapEntry> couples = query.getResultList();
        Map<Integer, Double> resultats = new HashMap<>();
        for (DefaultMapEntry couple : couples) {
            resultats.put((Integer) couple.getKey(), (Double) couple.getValue());
        }
        return resultats;
    }

}
