package fr.si2m.red.internal.parametrage;

import org.springframework.stereotype.Repository;

import fr.si2m.red.core.repository.jpa.JpaEntiteImportableRepository;
import fr.si2m.red.parametrage.ParamContact;
import fr.si2m.red.parametrage.ParamContactRepository;

/**
 * Base de données des entités {@link ParamContact}, connectée via JPA.
 * 
 * @author nortaina
 *
 */
@Repository
public class JpaParamContactRepository extends JpaEntiteImportableRepository<ParamContact> implements ParamContactRepository {

}
