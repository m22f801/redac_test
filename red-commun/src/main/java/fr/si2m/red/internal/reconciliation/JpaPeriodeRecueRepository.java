package fr.si2m.red.internal.reconciliation;

import java.lang.reflect.Field;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Subquery;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.annotations.Formula;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import fr.si2m.red.DateRedac;
import fr.si2m.red.complement.ExtensionContrat;
import fr.si2m.red.contrat.Contrat;
import fr.si2m.red.core.repository.CriteresRecherche;
import fr.si2m.red.core.repository.jpa.JpaRepository;
import fr.si2m.red.dsn.AdhesionEtablissementMois;
import fr.si2m.red.dsn.EtatPeriode;
import fr.si2m.red.dsn.ParametresPartitionPeriodes;
import fr.si2m.red.dsn.Trace;
import fr.si2m.red.dsn.Versement;
import fr.si2m.red.parametrage.ParamControleSignalRejet;
import fr.si2m.red.parametrage.ParamUtilisateurGestionnaire;
import fr.si2m.red.reconciliation.ExportPeriodeExcel;
import fr.si2m.red.reconciliation.MessageControle;
import fr.si2m.red.reconciliation.PeriodeRecue;
import fr.si2m.red.reconciliation.PeriodeRecueRepository;
import fr.si2m.red.reconciliation.PeriodesRecuesCriteresRecherche;
import fr.si2m.red.reconciliation.RattachementDeclarationsRecues;
import fr.si2m.red.reconciliation.ResumePeriodeRecue;
import fr.si2m.red.reconciliation.TypeActionPopupChangementEnMasse;
import fr.si2m.red.reconciliation.TypeSelectCriteresRecherche;

/**
 * Base de données des entités {@link PeriodeRecue}, connectée via JPA.
 * 
 * @author poidij
 *
 */
@Repository
public class JpaPeriodeRecueRepository extends JpaRepository<PeriodeRecue> implements PeriodeRecueRepository {
    private static final Logger LOGGER = LoggerFactory.getLogger(JpaPeriodeRecueRepository.class);

    private static final String PARAM_ID_PERIODE = "idPeriode";
    private static final String PARAM_LISTE_ID_PERIODES = "idsPeriodes";
    private static final String PARAM_RECONSOLIDER = "reconsoliderAsText";
    private static final String PARAM_NUMERO_CONTRAT = "numeroContrat";
    private static final String PARAM_TYPE_PERIODE = "typePeriode";
    private static final String PARAM_DATE_DEBUT_PERIODE = "dateDebutPeriode";
    private static final String PARAM_DATE_FIN_PERIODE = "dateFinPeriode";
    private static final String PARAM_CONTRAT_CLIENT = "client";
    private static final String PARAM_CONTRAT_DATE_FIN_SITUATION_LIGNE = "dateFinSituationLigne";
    private static final String PARAM_MOIS_RATTACHEMENT = "moisRattachement";
    private static final String PARAM_ETAT_PERIODE = "etatPeriode";
    private static final String PARAM_A_TRAITER_PAR = "aTraiterPar";
    private static final String PARAM_TRAITE_PAR = "traitePar";
    private static final String PARAM_AFFECTE_A = "affecteA";
    private static final String PARAM_CODE = "CODE";
    private static final String SELECT_ID_PERIODE_MSG_CONTROLE = "SELECT mc.ID_PERIODE ";
    private static final String JPA_SELECT_PERIODES_IN = "SELECT p FROM PeriodeRecue p WHERE p.idPeriode IN  (";
    private static final String DEBUT_REQUETE_RANG = "(SELECT e.ID_PERIODE, e.NB_ENTITES_ECCT, @running_total := @running_total + e.NB_ENTITES_ECCT AS NB_CUMULE, @curRank := @curRank + 1 AS RANG, ";
    private static final String FIN_REQUETE_RANG = " FROM V_Effectif_categorie_contrat_travail_a_calculer e JOIN (SELECT @running_total := 0) c JOIN (SELECT @curRank := 0) r ORDER BY e.ID_PERIODE) t ";
    private static final String EXISTS = " EXISTS (";
    private static final String NOT_EXISTS = " NOT EXISTS (";
    private static final String AND = " AND ";
    private static final String ORDER_BY = "ORDER BY ";
    private static final String WHERE = " WHERE ";
    private static final String COL_AFFECTE_A = "AFFECTE_A";
    private static final String COL_A_TRAITER_PAR = "A_TRAITER_PAR";
    private static final String COL_TRAITE_PAR = "TRAITE_PAR";
    private static final String CONDITION_ID_PERIODES_IN = " WHERE p.ID_PERIODE IN ( :";
    private static final String AND_CONDITION_ORIGINE = "' AND m.ORIGINE='";
    private static final String AND_ID_PERIODE_IN = "' AND m.ID_PERIODE IN (:";

    @Override
    public Class<PeriodeRecue> getClassePrototypeEntite() {
        return PeriodeRecue.class;
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public boolean existeDeclarationRecuePremierMoisPourDebutPeriode(String numeroContrat) {
        String requeteStr = "SELECT COUNT(p) FROM PeriodeRecue p WHERE p.numeroContrat = :" + PARAM_NUMERO_CONTRAT
                + " and PREMIER_MOIS_DECL = 'O' and TYPE_PERIODE = 'DEC'";
        TypedQuery<Long> query = getEntityManager().createQuery(requeteStr, Long.class);
        query.setParameter(PARAM_NUMERO_CONTRAT, numeroContrat);
        return query.getSingleResult() > 0;
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public long compteToutesPeriodesEnCoursDeReception() {
        TypedQuery<Long> query = getEntityManager().createQuery("SELECT COUNT(p) FROM PeriodeRecue p WHERE p.etatPeriode = 'RCP'", Long.class);
        return query.getSingleResult();
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public long comptePeriodesEnCoursDeReception(List<String> groupesGestion, List<Integer> numsFamille, Integer dateDuJour) {

        StringBuilder query = new StringBuilder("SELECT COUNT(*) FROM periodes_recues p WHERE p.ETAT_PERIODE = 'RCP' ");
        Map<String, Object> params = new HashMap<String, Object>();
        prepareCriteresComptage(query, groupesGestion, numsFamille, params, dateDuJour);

        return getPremierResultatSiExiste(getJdbcTemplate().query(query.toString(), params, new RowMapper<Long>() {

            @Override
            public Long mapRow(ResultSet rs, int rowNum) throws SQLException {
                return rs.getLong(1);
            }
        }));

    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public long comptePeriodesATraiter(List<String> groupesGestion, List<Integer> numsFamille, Integer dateDuJour) {

        StringBuilder query = new StringBuilder("SELECT COUNT(*) FROM periodes_recues p WHERE p.ETAT_PERIODE IN ('INS', 'NIN') ");
        Map<String, Object> params = new HashMap<String, Object>();
        prepareCriteresComptage(query, groupesGestion, numsFamille, params, dateDuJour);

        return getPremierResultatSiExiste(getJdbcTemplate().query(query.toString(), params, new RowMapper<Long>() {

            @Override
            public Long mapRow(ResultSet rs, int rowNum) throws SQLException {
                return rs.getLong(1);
            }
        }));
    }

    /**
     * Prépare la requête de comptage en ajoutant les critères de groupe de gestion et de numéro de famille.
     * 
     * @param queryBuilder
     *            le début de requête
     * @param groupesGestion
     *            les groupes de gestion sélectionnés
     * @param numsFamille
     *            les numéros de famille sélectionnés
     * @param params
     *            la map de paramètres
     * @param dateDuJour
     *            la date du jour
     */
    private void prepareCriteresComptage(StringBuilder queryBuilder, List<String> groupesGestion, List<Integer> numsFamille,
            Map<String, Object> params, Integer dateDuJour) {
        if (!groupesGestion.isEmpty() || !numsFamille.isEmpty()) {
            queryBuilder.append(" AND p.NUMERO_CONTRAT IN (SELECT DISTINCT c.NOCO FROM contrats c "
                    + " WHERE c.DT_DEBUT_SIT <= :dateDuJour AND (:dateDuJour <= coalesce(c.DT_FIN_SIT, 99999999) "
                    + "OR :dateDuJour <= (CASE WHEN (c.DT_FIN_SIT = 0) THEN 99999999 ELSE c.DT_FIN_SIT END)) ");

            params.put("dateDuJour", dateDuJour);
        }

        if (!groupesGestion.isEmpty()) {
            queryBuilder.append(" AND c.NMGRPGES IN (:groupesGestion)");
            params.put("groupesGestion", groupesGestion);
        }

        if (!numsFamille.isEmpty()) {
            queryBuilder.append(" AND c.NOFAM IN (:numsFamilleProduit)");
            params.put("numsFamilleProduit", numsFamille);
        }

        if (!groupesGestion.isEmpty() || !numsFamille.isEmpty()) {
            queryBuilder.append(")");
        }

    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public PeriodeRecue existePeriodePourRattachement(String typePeriode, String referenceContrat, Integer moisRattachement,
            EtatPeriode etatPeriode) {
        String requeteStr = "SELECT p FROM PeriodeRecue p WHERE p.typePeriode = :" + PARAM_TYPE_PERIODE + " AND p.numeroContrat = :referenceContrat"
                + " AND p.dateDebutPeriode <= :moisRattachement AND p.dateFinPeriode >= :moisRattachement AND p.etatPeriode = :etatPeriode";
        TypedQuery<PeriodeRecue> query = getEntityManager().createQuery(requeteStr, PeriodeRecue.class);
        query.setParameter(PARAM_TYPE_PERIODE, typePeriode);
        query.setParameter("referenceContrat", referenceContrat);
        query.setParameter(PARAM_MOIS_RATTACHEMENT, moisRattachement);
        query.setParameter(PARAM_ETAT_PERIODE, etatPeriode);
        List<PeriodeRecue> result = query.getResultList();
        return getPremierResultatSiExiste(result);

    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public PeriodeRecue existePeriodePourRattachement(String typePeriode, String numeroContrat, Integer moisRattachement) {
        String requeteStr = "SELECT p FROM PeriodeRecue p WHERE p.typePeriode = :" + PARAM_TYPE_PERIODE + " AND p.numeroContrat = :"
                + PARAM_NUMERO_CONTRAT + " AND p.dateDebutPeriode <= :moisRattachement AND p.dateFinPeriode >= :moisRattachement";
        TypedQuery<PeriodeRecue> query = getEntityManager().createQuery(requeteStr, PeriodeRecue.class);
        query.setParameter(PARAM_TYPE_PERIODE, typePeriode);
        query.setParameter(PARAM_NUMERO_CONTRAT, numeroContrat);
        query.setParameter(PARAM_MOIS_RATTACHEMENT, moisRattachement);
        List<PeriodeRecue> result = query.getResultList();
        return getPremierResultatSiExiste(result);

    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public List<PeriodeRecue> getPeriodesRecuesEncoursPourApplication(String applicationConcernee) {
        String jpql = "SELECT p FROM PeriodeRecue p, Contrat c, ParamFamilleContrat pfc"
                + " WHERE p.etatPeriode = 'ENC' AND p.numeroContrat=c.numContrat AND c.numFamilleProduit = pfc.numFamille"
                + " AND c.groupeGestion = pfc.groupeGestion" + " AND (p.dateFinPeriode <= coalesce(c.dateFinSituationLigne, 99999999)"
                + " OR p.dateFinPeriode <= (CASE WHEN (c.dateFinSituationLigne = 0) THEN 99999999 ELSE c.dateFinSituationLigne END))"
                + " AND p.dateFinPeriode >= c.dateDebutSituationLigne AND pfc.applicationAvalEnCharge = :applicationConcernee";

        TypedQuery<PeriodeRecue> query = getEntityManager().createQuery(jpql, PeriodeRecue.class);
        query.setParameter("applicationConcernee", applicationConcernee);
        return query.getResultList();
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public PeriodeRecue getPeriodeRecueEncours(String numeroContrat, Integer dateDebutPeriode) {
        String jpql = "SELECT p FROM PeriodeRecue p" + " WHERE p.etatPeriode = 'ENC' AND p.numeroContrat= :" + PARAM_NUMERO_CONTRAT
                + " AND dateDebutPeriode = :" + PARAM_DATE_DEBUT_PERIODE;
        TypedQuery<PeriodeRecue> query = getEntityManager().createQuery(jpql, PeriodeRecue.class);
        query.setParameter(PARAM_NUMERO_CONTRAT, numeroContrat);
        query.setParameter(PARAM_DATE_DEBUT_PERIODE, dateDebutPeriode);
        List<PeriodeRecue> result = query.getResultList();
        return getPremierResultatSiExiste(result);
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public PeriodeRecue getPeriodeRecue(String numeroContrat, Integer dateDebutPeriode) {
        String jpql = "SELECT p FROM PeriodeRecue p" + " WHERE p.numeroContrat= :" + PARAM_NUMERO_CONTRAT + " AND dateDebutPeriode = :"
                + PARAM_DATE_DEBUT_PERIODE;
        TypedQuery<PeriodeRecue> query = getEntityManager().createQuery(jpql, PeriodeRecue.class);
        query.setParameter(PARAM_NUMERO_CONTRAT, numeroContrat);
        query.setParameter(PARAM_DATE_DEBUT_PERIODE, dateDebutPeriode);
        List<PeriodeRecue> result = query.getResultList();
        return getPremierResultatSiExiste(result);
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public PeriodeRecue getPeriodeRecueTraitee(String numeroContrat, Integer dateDebutPeriode) {
        String jpql = "SELECT p FROM PeriodeRecue p, PeriodeRecueTraitee t" + " WHERE p.idPeriode = t.idPeriode" + " AND t.numeroContrat = :"
                + PARAM_NUMERO_CONTRAT + " AND t.dateDebutPeriode = :" + PARAM_DATE_DEBUT_PERIODE;
        TypedQuery<PeriodeRecue> query = getEntityManager().createQuery(jpql, PeriodeRecue.class);
        query.setParameter(PARAM_NUMERO_CONTRAT, numeroContrat);
        query.setParameter(PARAM_DATE_DEBUT_PERIODE, dateDebutPeriode);
        List<PeriodeRecue> result = query.getResultList();
        return getPremierResultatSiExiste(result);
    }

    @Override
    @Transactional(propagation = Propagation.MANDATORY)
    public PeriodeRecue getPeriodePrecedenteDeMemeType(PeriodeRecue periode) {
        String requeteStr = "SELECT p FROM PeriodeRecue p WHERE p.numeroContrat = :" + PARAM_NUMERO_CONTRAT + " AND p.typePeriode = :"
                + PARAM_TYPE_PERIODE + " AND p.dateFinPeriode = :dateFinPeriode";
        TypedQuery<PeriodeRecue> query = getEntityManager().createQuery(requeteStr, PeriodeRecue.class).setMaxResults(1);
        query.setParameter(PARAM_NUMERO_CONTRAT, periode.getNumeroContrat());
        query.setParameter(PARAM_TYPE_PERIODE, periode.getTypePeriode());
        query.setParameter(PARAM_DATE_FIN_PERIODE,
                DateRedac.retirerALaDate(periode.getDateDebutPeriode(), 1, DateRedac.MODIFIER_JOUR, "DATE_DEBUT_PERIODE"));
        List<PeriodeRecue> result = query.getResultList();
        return getPremierResultatSiExiste(result);
    }

    @Override
    @Transactional(propagation = Propagation.MANDATORY)
    public List<PeriodeRecue> getPeriodesAReconsolider() {
        String jpql = "SELECT p FROM PeriodeRecue p WHERE p.reconsoliderAsText = 'O'";
        TypedQuery<PeriodeRecue> query = getEntityManager().createQuery(jpql, PeriodeRecue.class);
        return query.getResultList();
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public long compte(PeriodesRecuesCriteresRecherche criteres) {
        String sql = prepareRechercheNatif(criteres, TypeSelectCriteresRecherche.COUNT_PERIODES);

        return getPremierResultatSiExiste(getJdbcTemplate().query(sql, new RowMapper<Long>() {

            @Override
            public Long mapRow(ResultSet rs, int rowNum) throws SQLException {
                return rs.getLong(1);
            }
        }));

    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public List<ResumePeriodeRecue> recherchePeriodesRecues(PeriodesRecuesCriteresRecherche criteres) {
        CriteriaBuilder criteriaBuilder = getEntityManager().getCriteriaBuilder();

        CriteriaQuery<ResumePeriodeRecue> query = criteriaBuilder.createQuery(ResumePeriodeRecue.class);
        Root<ResumePeriodeRecue> rootEntity = query.from(ResumePeriodeRecue.class);

        // Search part
        prepareRecherche(query, criteriaBuilder, rootEntity, criteres);

        // Aspects part
        prepareTri(query, criteriaBuilder, rootEntity, criteres);

        TypedQuery<ResumePeriodeRecue> typedQuery = getEntityManager().createQuery(query);
        preparePagination(typedQuery, criteres);

        LOGGER.debug("Recherche de périodes avec les critères suivants : {}", criteres);
        List<ResumePeriodeRecue> periodes = typedQuery.getResultList();
        LOGGER.debug("Recherche de périodes terminées avec {} périodes", periodes.size());

        return periodes;
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public String getLibellePeriode(PeriodeRecue periode) {
        String jpql = "SELECT (CASE c.codeFractionnement WHEN 'A' THEN SUBSTRING(:dateDebutPeriode, 1, 4) "
                + "WHEN 'S' THEN (CASE WHEN (:dateDebutPeriode LIKE '____01__') THEN CONCAT(SUBSTRING(:dateDebutPeriode, 1, 4),'S1') "
                + "ELSE CONCAT(SUBSTRING(:dateDebutPeriode, 1, 4),'S2') END) "
                + "WHEN 'T' THEN (CASE WHEN (:dateDebutPeriode LIKE '____01__') THEN CONCAT(SUBSTRING(:dateDebutPeriode, 1, 4),'T1') "
                + "WHEN (:dateDebutPeriode LIKE '____04__') THEN CONCAT(SUBSTRING(:dateDebutPeriode, 1, 4),'T2') "
                + "WHEN (:dateDebutPeriode LIKE '____07__') THEN CONCAT(SUBSTRING(:dateDebutPeriode, 1, 4),'T3') "
                + "ELSE CONCAT(SUBSTRING(:dateDebutPeriode, 1, 4),'T4') END) "
                + "WHEN 'M' THEN CONCAT(SUBSTRING(:dateDebutPeriode, 1, 4),'-',SUBSTRING(:dateDebutPeriode, 5, 2)) "
                + "ELSE '' END) FROM Contrat c WHERE c.numContrat = :numeroContrat AND c.ligneEnCoursImportBatch IS FALSE "
                + "AND c.dateDebutSituationLigne <= :dateFinPeriode AND (coalesce(c.dateFinSituationLigne, 99999999) >= :dateFinPeriode "
                + "OR (CASE WHEN (c.dateFinSituationLigne = 0) THEN 99999999 ELSE c.dateFinSituationLigne END) >= :dateFinPeriode)";
        TypedQuery<String> query = getEntityManager().createQuery(jpql, String.class).setMaxResults(1);
        query.setParameter(PARAM_NUMERO_CONTRAT, periode.getNumeroContrat());
        query.setParameter(PARAM_DATE_DEBUT_PERIODE, periode.getDateDebutPeriode().toString());
        query.setParameter(PARAM_DATE_FIN_PERIODE, periode.getDateFinPeriode());
        return getPremierResultatSiExiste(query.getResultList());
    }

    @Override
    public List<PeriodeRecue> getPeriodesRecuesPourContrat(String numeroContrat) {
        String jpql = "SELECT p FROM PeriodeRecue p WHERE p.numeroContrat = :numeroContrat ORDER BY p.dateDebutPeriode ASC, p.dateCreation ASC";
        TypedQuery<PeriodeRecue> query = getEntityManager().createQuery(jpql, PeriodeRecue.class);
        query.setParameter(PARAM_NUMERO_CONTRAT, numeroContrat);
        return query.getResultList();
    }

    @Override
    protected void prepareTri(CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder, Root<?> entityRoot, CriteresRecherche criteres) {
        if (!StringUtils.equals(PeriodesRecuesCriteresRecherche.TRI_SPECIFIQUE, criteres.getTriChamp())) {
            super.prepareTri(query, criteriaBuilder, entityRoot, criteres);
            return;
        }
        // Tri spécifique... For God doth know that in the day ye eat thereof, then your eyes shall be opened, and ye shall be as gods, knowing good and evil.
        // And, behold, I come quickly; and my reward is with me, to give every man according as his work shall be. I am Alpha and Omega, the beginning and the
        // end, the first and the last.

        // Par PERIODE_RECUE.ETAT, dans cet ordre :
        // - PERIODE_RECUE.ETAT = « NIN » non intégré avec rejet (avec lien MESSAGE_CONTROLE.NIVEAU_ALERTE = « REJET », puis
        // - PERIODE_RECUE.ETAT = « NIN » non intégré avec signalement (avec lien MESSAGE_CONTROLE.NIVEAU_ALERTE = « SIGAL », puis
        // - PERIODE_RECUE.ETAT = « NIN » non intégré sans rejet ou signalement (sans lien MESSAGE_CONTROLE.NIVEAU_ALERTE = « REJET » ou
        // MESSAGE_CONTROLE.NIVEAU_ALERTE = « SIGNAL », puis
        // - PERIODE_RECUE.ETAT = « INS » intégrées avec signalements, puis
        // - PERIODE_RECUE.ETAT = « ARI » à réintégrer, puis
        // - PERIODE_RECUE.ETAT = « ENC » en cours d'intégration, puis
        // - PERIODE_RECUE.ETAT = « RCP » en réception, puis
        // - PERIODE_RECUE.ETAT = « TPG » Traité par gestion, puis
        // - PERIODE_RECUE.ETAT = « SSG » Sans suite gestion, puis
        // - PERIODE_RECUE.ETAT = « ING » Intégré avec signalements acquittés, puis
        // - PERIODE_RECUE.ETAT = « INT » Intégré.
        // Puis, au sein d'un même état, dont la date de début de période va de la plus ancienne à la plage la plus récente
        // (PERIODE_RECUE.ETAT.DATE_DEBUT_PERIODE),
        // Puis, au sein d'un même état et d'une même date de début, classé par PERIODE_RECUE.NUMERO_CONTRAT,
        // Puis, au sein d'un même état, même date de début, même contrat, classé par PERIODE_RECUE.TYPE, trié par ORDRE de l'entité PARAM_CODE_LIBELLE pour
        // lequel TBL = « PERIODERECUE » et CHAMP = « TYPE » triée sur la colonne ORDRE.

        List<Order> ordres = Arrays.asList(criteriaBuilder.asc(entityRoot.get("ordreEtatPeriode")),
                criteriaBuilder.desc(entityRoot.get("ordreEtatPeriodeNonIntegree")), criteriaBuilder.asc(entityRoot.get("dateDebutPeriode")),
                criteriaBuilder.asc(entityRoot.get("numeroContrat")), criteriaBuilder.asc(entityRoot.get("ordreLibelleType")));
        query.orderBy(ordres);
    }

    /**
     * Prépare la recherche de période correspondant à des critères donnés.
     * 
     * @param query
     *            la recherche à spécifier
     * @param cb
     *            le constructeur de critères
     * @param rootEntity
     *            la racine de l'entité période dans le schéma de données
     * @param criteres
     *            les critères de recherche à transcoder
     */
    private void prepareRecherche(CriteriaQuery<?> query, CriteriaBuilder cb, Root<ResumePeriodeRecue> rootEntity,
            PeriodesRecuesCriteresRecherche criteres) {
        Expression<Boolean> restriction = cb.and();

        restriction = prepareCriteresSansQuery(restriction, cb, rootEntity, criteres);

        // subquery CodeUsers
        Subquery<String> subqueryCodeUsers = creeSousRechercheUtilisateurGestionnaire(query);

        // Critère "Affecté à"
        if (!criteres.getAffecteAAsList().isEmpty()) {
            Expression<Boolean> affecteARestriction = cb.or();
            // hors cas particulier
            if (!criteres.filtreAffecteAHorsCasParticulier().isEmpty()) {
                affecteARestriction = cb.or(affecteARestriction, rootEntity.get(PARAM_AFFECTE_A).in(criteres.filtreAffecteAHorsCasParticulier()));
            }
            // cas non affecté
            if (criteres.filtreAffecteACasParticulier().contains(PeriodesRecuesCriteresRecherche.NON_AFFECTE_CODE)) {
                affecteARestriction = cb.or(affecteARestriction, rootEntity.get(PARAM_AFFECTE_A).isNull());
            }
            // cas autre gestionnaire
            if (criteres.filtreAffecteACasParticulier().contains(PeriodesRecuesCriteresRecherche.AUTRE_GEST_CODE)) {
                affecteARestriction = cb.or(affecteARestriction,
                        cb.and(rootEntity.get(PARAM_AFFECTE_A).in(subqueryCodeUsers).not(), rootEntity.get(PARAM_AFFECTE_A).isNotNull()));
            }

            restriction = cb.and(restriction, affecteARestriction);
        }

        // Critère "A traiter par"
        if (!criteres.getaTraiterParAsList().isEmpty()) {
            Expression<Boolean> aTraiteParRestriction = cb.or();
            // hors cas particulier
            if (!criteres.filtreATraiterParHorsCasParticulier().isEmpty()) {
                aTraiteParRestriction = cb.or(aTraiteParRestriction,
                        rootEntity.get(PARAM_A_TRAITER_PAR).in(criteres.filtreATraiterParHorsCasParticulier()));
            }
            // cas non assigné
            if (criteres.filtreATraiterParCasParticulier().contains(PeriodesRecuesCriteresRecherche.NON_ASSIGNE_CODE)) {
                aTraiteParRestriction = cb.or(aTraiteParRestriction, rootEntity.get(PARAM_A_TRAITER_PAR).isNull());
            }
            // cas autre gestionnaire
            if (criteres.filtreATraiterParCasParticulier().contains(PeriodesRecuesCriteresRecherche.AUTRE_GEST_CODE)) {
                aTraiteParRestriction = cb.or(aTraiteParRestriction, rootEntity.get(PARAM_A_TRAITER_PAR).in(subqueryCodeUsers).not());
            }

            restriction = cb.and(restriction, aTraiteParRestriction);
        }

        // Critère "Traité par"
        if (!criteres.getTraiteParAsList().isEmpty()) {
            Expression<Boolean> traiteParRestriction = cb.or();
            // hors cas particulier
            if (!criteres.filtreTraiteParHorsCasParticulier().isEmpty()) {
                traiteParRestriction = cb.or(traiteParRestriction, rootEntity.get(PARAM_TRAITE_PAR).in(criteres.filtreTraiteParHorsCasParticulier()));
            }
            // cas traité automatiquement
            if (criteres.filtreTraiteParCasParticulier().contains(PeriodesRecuesCriteresRecherche.TRAITE_AUTO_CODE)) {
                traiteParRestriction = cb.or(traiteParRestriction, rootEntity.get(PARAM_TRAITE_PAR).isNull());
            }
            // cas autre gestionnaire
            if (criteres.filtreTraiteParCasParticulier().contains(PeriodesRecuesCriteresRecherche.AUTRE_GEST_CODE)) {
                traiteParRestriction = cb.or(traiteParRestriction, rootEntity.get(PARAM_TRAITE_PAR).in(subqueryCodeUsers).not());
            }

            restriction = cb.and(restriction, traiteParRestriction);
        }

        // Critères sur le contrat lié à la période
        if (verifieSousRechercheSurNumContratNecessaire(criteres)) {
            Subquery<String> subqueryContrat = creeSousRechercheNumContratAvecCriteresSurContrat(cb, query, rootEntity, criteres);
            restriction = cb.and(restriction, cb.in(rootEntity.get(PARAM_NUMERO_CONTRAT)).value(subqueryContrat));
        }

        // Critère "Versement"
        if (criteres.getVersementsAsList().size() == 1) {
            restriction = cb.and(restriction, creeClausePresenceVersement(cb, query, rootEntity, criteres.getVersementsAsList().get(0)));
        }
        // Critère "Messages"
        List<String> messages = criteres.getMessagesAsList();
        if (!messages.isEmpty() && messages.size() != 3) {
            restriction = cb.and(restriction, creeClausePresenceAbsenceMessage(cb, query, rootEntity, messages));
        }

        // Critère "Lib.Messages"
        List<String> idControles = criteres.getIdControlesAsList();
        if (!idControles.isEmpty()) {
            restriction = cb.and(restriction, creeClausePresenceAbsenceLibMessage(cb, query, rootEntity, idControles));
        }

        // Critère "VIP" F09_RG_1_14
        if (criteres.getVipAsList().size() == 1) {
            // Standard
            restriction = cb.and(restriction, creeClauseContratVip(cb, query, rootEntity, !StringUtils.equals(criteres.getVipAsList().get(0), "N")));
        }

        // Filtre par numéro de contrat VIP
        if (verifieFiltreParContratVIPNecessaire(criteres)) {
            restriction = cb.and(restriction, rootEntity.get(PARAM_NUMERO_CONTRAT).in(criteres.getFiltreContratsVIP()));
        }

        query.where(restriction);
    }

    /**
     * Préparation des critères de la requête qui n'utilisent pas la query.
     * 
     * @param restriction
     *            l'expression booléenne de la somme des filtres
     * @param cb
     *            le constructeur de query JPA
     * @param resumeExtensionContratEntity
     *            la racine des entités pour la query JPA
     * @param criteres
     *            les paramètres de filtre pour la query
     */
    private Expression<Boolean> prepareCriteresSansQuery(Expression<Boolean> restriction, CriteriaBuilder cb, Root<ResumePeriodeRecue> rootEntity,
            PeriodesRecuesCriteresRecherche criteres) {

        Expression<Boolean> newRestriction = restriction;

        // Critère "Contrat"
        if (StringUtils.isNotBlank(criteres.getNumContrat())) {
            newRestriction = cb.and(newRestriction, buildStartsWithExpression(cb, rootEntity, PARAM_NUMERO_CONTRAT, criteres.getNumContrat()));
        }

        // Critère "Statut"
        if (!criteres.getStatutsAsList().isEmpty()) {
            newRestriction = cb.and(newRestriction, rootEntity.get(PARAM_ETAT_PERIODE).in(criteres.getStatutsAsList()));
        }
        // Critère "Nature"
        if (!criteres.getTypesPeriodeAsList().isEmpty()) {
            newRestriction = cb.and(newRestriction, rootEntity.get(PARAM_TYPE_PERIODE).in(criteres.getTypesPeriodeAsList()));
        }

        // Critère "Période"
        if (!criteres.getTrimestresAsList().isEmpty() && !criteres.getTrimestresAsList().contains("Toutes")) {
            newRestriction = cb.and(newRestriction, creeClauseChevauchementTrimestre(cb, rootEntity, criteres.getTrimestresAsList()));
        }

        return newRestriction;
    }

    /**
     * Vérifie qu'un filtre sur contrat est nécessaire pour des critères de recherche donnés.
     * 
     * @param criteres
     *            les critères de recherche
     * @return true si le filtre par contrat doit être appliqué, false sinon
     */
    private boolean verifieSousRechercheSurNumContratNecessaire(PeriodesRecuesCriteresRecherche criteres) {
        return verifieSousRecherche1(criteres) || verifieSousRecherche2(criteres) || StringUtils.isNotBlank(criteres.getNic())
                || StringUtils.isNotBlank(criteres.getRaisonSociale());
    }

    /**
     * Première partie de la vérification qu'un filtre sur contrat est nécessaire pour des critères de recherche donnés.
     * 
     * @param criteres
     *            les critères de recherche
     * @return true si la première partie de la vérification est vraie, false sinon
     */
    private boolean verifieSousRecherche1(PeriodesRecuesCriteresRecherche criteres) {
        return !criteres.getGroupesGestionAsList().isEmpty() || StringUtils.isNotBlank(criteres.getNumClient())
                || !criteres.getFamillesAsList().isEmpty();
    }

    /**
     * Deuxième partie de la vérification qu'un filtre sur contrat est nécessaire pour des critères de recherche donnés.
     * 
     * @param criteres
     *            les critères de recherche
     * @return true si la deuxième partie de la vérification est vraie, false sinon
     */
    private boolean verifieSousRecherche2(PeriodesRecuesCriteresRecherche criteres) {
        return StringUtils.isNotBlank(criteres.getCptEnc()) || StringUtils.isNotBlank(criteres.getCptProd())
                || StringUtils.isNotBlank(criteres.getSiren());
    }

    /**
     * Vérifie qu'un filtre sur numéro de contrat VIP est nécessaire pour des critères de recherche donnés.
     * 
     * @param criteres
     *            les critères de recherche
     * @return true si le filtre par numéro de contrat VIP doit être appliqué, false sinon
     */
    private boolean verifieFiltreParContratVIPNecessaire(PeriodesRecuesCriteresRecherche criteres) {
        return (criteres.getVipAsList().isEmpty() || criteres.getVipAsList().contains("O")) && !criteres.getFiltreContratsVIP().isEmpty();
    }

    /**
     * Prépare la sous-recherche sur les identifiantActiveDirectory present dans ParamUtilisateurGestionnaire
     * 
     * @param query
     *            la recherche principale
     * @return la sous-recherche sur les identifiantActiveDirectory
     */
    private Subquery<String> creeSousRechercheUtilisateurGestionnaire(CriteriaQuery<?> query) {

        Subquery<String> subqueryUtlisateurGest = query.subquery(String.class);
        Root<ParamUtilisateurGestionnaire> utlisateurGestRoot = subqueryUtlisateurGest.from(ParamUtilisateurGestionnaire.class);
        subqueryUtlisateurGest.select(utlisateurGestRoot.<String> get("identifiantActiveDirectory"));

        return subqueryUtlisateurGest;
    }

    /**
     * Prépare la sous-recherche sur les numéro de contrats correspondant à des critères de recherche sur contrat lié à une période.
     * 
     * @param cb
     *            le constructeur de critères
     * @param query
     *            la recherche principale
     * @param rootEntity
     *            la racine de l'entité période dans le schéma de données
     * @param criteres
     *            les critères de recherche
     * @return la sous-recherche sur numéros de contrat
     */
    private Subquery<String> creeSousRechercheNumContratAvecCriteresSurContrat(CriteriaBuilder cb, CriteriaQuery<?> query,
            Root<ResumePeriodeRecue> rootEntity, PeriodesRecuesCriteresRecherche criteres) {

        Subquery<String> subqueryContrat = query.subquery(String.class);
        Root<Contrat> contratRoot = subqueryContrat.from(Contrat.class);
        subqueryContrat.select(contratRoot.<String> get("numContrat"));

        Expression<Boolean> restrictionContrat = cb.isFalse(contratRoot.<Boolean> get("ligneEnCoursImportBatch"));
        restrictionContrat = cb.and(restrictionContrat,
                cb.lessThanOrEqualTo(contratRoot.<Integer> get("dateDebutSituationLigne"), rootEntity.<Integer> get(PARAM_DATE_FIN_PERIODE)));
        Expression<Boolean> restrictionDateFin = cb
                .or(cb.greaterThanOrEqualTo(
                        cb.coalesce(
                                contratRoot
                                        .<Integer> get(
                                                PARAM_CONTRAT_DATE_FIN_SITUATION_LIGNE),
                                99999999),
                        rootEntity
                                .<Integer> get(PARAM_DATE_FIN_PERIODE)),
                        cb.greaterThanOrEqualTo(
                                cb.<Integer> selectCase()
                                        .when(cb.equal(contratRoot.<Integer> get(PARAM_CONTRAT_DATE_FIN_SITUATION_LIGNE), 0), 99999999)
                                        .otherwise(contratRoot.<Integer> get(PARAM_CONTRAT_DATE_FIN_SITUATION_LIGNE)),
                                rootEntity.<Integer> get(PARAM_DATE_FIN_PERIODE)));
        restrictionContrat = cb.and(restrictionContrat, restrictionDateFin);

        // Critère "Grpe Gestion"
        if (!criteres.getGroupesGestionAsList().isEmpty()) {
            restrictionContrat = cb.and(restrictionContrat, contratRoot.get("groupeGestion").in(criteres.getGroupesGestionAsList()));
        }
        // Critère "N° Client"
        if (StringUtils.isNotBlank(criteres.getNumClient())) {
            restrictionContrat = cb.and(restrictionContrat,
                    buildStartsWithExpression(cb, contratRoot, "numSouscripteur", Long.valueOf(criteres.getNumClient())));
        }
        // Critère "Famille"
        if (!criteres.getFamillesAsList().isEmpty()) {
            restrictionContrat = cb.and(restrictionContrat, contratRoot.get("numFamilleProduit").in(criteres.getFamillesAsList()));
        }
        // Critère "Cpt Enc"
        if (StringUtils.isNotBlank(criteres.getCptEnc())) {
            restrictionContrat = cb.and(restrictionContrat, buildStartsWithExpression(cb, contratRoot, "compteEncaissement", criteres.getCptEnc()));
        }
        // Critère "Cpt Prod"
        if (StringUtils.isNotBlank(criteres.getCptProd())) {
            restrictionContrat = cb.and(restrictionContrat, buildStartsWithExpression(cb, contratRoot, "numCompteProducteur", criteres.getCptProd()));
        }

        // Critères sur le client principal du contrat
        // Critère "SIREN"
        if (StringUtils.isNotBlank(criteres.getSiren())) {
            restrictionContrat = cb.and(restrictionContrat,
                    buildStartsWithExpression(cb, contratRoot.get(PARAM_CONTRAT_CLIENT), "numSiren", criteres.getSiren()));
        }
        // Critère "NIC"
        if (StringUtils.isNotBlank(criteres.getNic())) {
            restrictionContrat = cb.and(restrictionContrat,
                    buildStartsWithExpression(cb, contratRoot.get(PARAM_CONTRAT_CLIENT), "numSiret", criteres.getNic()));
        }
        // Critère "Raison sociale"
        if (StringUtils.isNotBlank(criteres.getRaisonSociale())) {
            restrictionContrat = cb.and(restrictionContrat,
                    buildLikeExpression(cb, contratRoot.get(PARAM_CONTRAT_CLIENT), "raisonSociale", criteres.getRaisonSociale()));
        }

        subqueryContrat.where(restrictionContrat);

        return subqueryContrat;
    }

    /**
     * Prépare la clause sur le chevauchement d'une période sur des trimestres donnés.
     * 
     * @param cb
     *            le constructeur de critères
     * @param rootEntity
     *            la racine de l'entité période dans le schéma de données
     * @param trimestres
     *            les trimestres
     * @return la clause de chevauchement
     */
    private Expression<Boolean> creeClauseChevauchementTrimestre(CriteriaBuilder cb, Root<?> rootEntity, List<String> trimestres) {
        Expression<Boolean> restrictionPeriode = cb.or();
        for (String trimestreAsString : trimestres) {
            Integer trimestre = Integer.valueOf(trimestreAsString);
            Integer finTrimestre = DateRedac.getDernierJourMois(DateRedac.ajouterALaDate(trimestre, 2, DateRedac.MODIFIER_MOIS, "trimestre"));
            restrictionPeriode = cb.or(restrictionPeriode,
                    cb.and(cb.lessThanOrEqualTo(rootEntity.<Integer> get(PARAM_DATE_DEBUT_PERIODE), finTrimestre),
                            cb.greaterThanOrEqualTo(rootEntity.<Integer> get(PARAM_DATE_FIN_PERIODE), trimestre)));
        }
        return restrictionPeriode;
    }

    /**
     * Prépare la clause de présence (ou d'absence totale) de messages de certains niveaux d'alerte.
     * 
     * @param cb
     *            le constructeur de critères
     * @param query
     *            la recherche principale
     * @param rootEntity
     *            la racine de l'entité période dans le schéma de données
     * @param niveauxAlertes
     *            les niveaux d'alertes à repérer (vide si vérification sur "pas d'alertes")
     * @return la clause de présence (ou d'absence totale) de messages
     */
    private Expression<Boolean> creeClausePresenceAbsenceMessage(CriteriaBuilder cb, CriteriaQuery<?> query, Root<?> rootEntity,
            List<String> niveauxAlertes) {
        Set<String> niveauxAlertesSansAucun = new HashSet<String>(niveauxAlertes);
        niveauxAlertesSansAucun.remove(ParamControleSignalRejet.NIVEAU_ALERTE_AUCUN);

        Expression<Boolean> clausePresenceAbsence = cb.or();

        if (!niveauxAlertesSansAucun.isEmpty()) {
            Subquery<Long> subqueryMessagePresents = query.subquery(Long.class);
            Root<MessageControle> messageRoot = subqueryMessagePresents.from(MessageControle.class);
            subqueryMessagePresents.select(messageRoot.<Long> get(PARAM_ID_PERIODE));
            Expression<Boolean> restrictionMessagePresents = cb.equal(messageRoot.get(PARAM_ID_PERIODE), rootEntity.get(PARAM_ID_PERIODE));
            restrictionMessagePresents = cb.and(restrictionMessagePresents, messageRoot.get("niveauAlerte").in(niveauxAlertesSansAucun));
            subqueryMessagePresents.where(restrictionMessagePresents);
            clausePresenceAbsence = cb.exists(subqueryMessagePresents);
        }

        if (niveauxAlertes.contains(ParamControleSignalRejet.NIVEAU_ALERTE_AUCUN)) {
            Subquery<Long> subqueryMessageAbsents = query.subquery(Long.class);
            Root<MessageControle> messageRoot = subqueryMessageAbsents.from(MessageControle.class);
            subqueryMessageAbsents.select(messageRoot.<Long> get(PARAM_ID_PERIODE));
            subqueryMessageAbsents.where(cb.equal(messageRoot.get(PARAM_ID_PERIODE), rootEntity.get(PARAM_ID_PERIODE)));
            clausePresenceAbsence = cb.or(clausePresenceAbsence, cb.not(cb.exists(subqueryMessageAbsents)));
        }

        return clausePresenceAbsence;
    }

    /**
     * Prépare la clause de présence (ou d'absence totale) de libelles de messages d'alerte en utilisant les ids controle.
     * 
     * @param cb
     *            le constructeur de critères
     * @param query
     *            la recherche principale
     * @param rootEntity
     *            la racine de l'entité période dans le schéma de données
     * @param idControles
     *            les controle ids à repérer
     * @return la clause de présence (ou d'absence totale) de libelles de messages d'alerte en utilisant les ids controle.
     */
    private Expression<Boolean> creeClausePresenceAbsenceLibMessage(CriteriaBuilder cb, CriteriaQuery<?> query, Root<?> rootEntity,
            List<String> idControles) {

        Expression<Boolean> clausePresenceAbsence = cb.or();

        if (!idControles.isEmpty()) {
            Subquery<Long> subqueryIdControlesPresents = query.subquery(Long.class);
            Root<MessageControle> messageRoot = subqueryIdControlesPresents.from(MessageControle.class);
            subqueryIdControlesPresents.select(messageRoot.<Long> get(PARAM_ID_PERIODE));
            Expression<Boolean> restrictionMessagePresents = cb.equal(messageRoot.get(PARAM_ID_PERIODE), rootEntity.get(PARAM_ID_PERIODE));
            restrictionMessagePresents = cb.and(restrictionMessagePresents, messageRoot.get("identifiantControle").in(idControles));
            subqueryIdControlesPresents.where(restrictionMessagePresents);
            clausePresenceAbsence = cb.exists(subqueryIdControlesPresents);
        }

        return clausePresenceAbsence;
    }

    /**
     * Prépare la clause de présence de versements pour une période.
     * 
     * @param cb
     *            le constructeur de critères
     * @param query
     *            la recherche principale
     * @param rootEntity
     *            la racine de l'entité période dans le schéma de données
     * @param versement
     *            true si la clause doit vérifier la présence de versement, false si elle doit en vérifier l'absence
     * @return la clause de présence de versements
     */
    private Expression<Boolean> creeClausePresenceVersement(CriteriaBuilder cb, CriteriaQuery<?> query, Root<?> rootEntity, boolean versement) {
        Subquery<String> subqueryVersement = query.subquery(String.class);
        Root<Versement> versementRoot = subqueryVersement.from(Versement.class);
        Path<AdhesionEtablissementMois> adhesionEntity = versementRoot.get("adhesionEtablissementMois");
        subqueryVersement.select(versementRoot.<String> get("idVersement"));
        Expression<Boolean> restrictionVersement = cb.isFalse(adhesionEntity.<Boolean> get("ligneEnCoursImportBatch"));

        Subquery<String> subqueryRattachement = query.subquery(String.class);
        Root<RattachementDeclarationsRecues> rattachementRoot = subqueryRattachement.from(RattachementDeclarationsRecues.class);
        subqueryRattachement.select(rattachementRoot.<String> get("idAdhEtabMois"));
        subqueryRattachement.where(cb.equal(rattachementRoot.get(PARAM_ID_PERIODE), rootEntity.get(PARAM_ID_PERIODE)));
        restrictionVersement = cb.and(restrictionVersement, cb.in(versementRoot.get("idAdhEtabMois")).value(subqueryRattachement));

        subqueryVersement.where(restrictionVersement);

        if (versement) {
            return cb.exists(subqueryVersement);
        } else {
            return cb.not(cb.exists(subqueryVersement));
        }
    }

    /**
     * Prépare la clause sur le raffinement d'une recherche de période sur des contrats VIP ou non.
     * 
     * @param cb
     *            le constructeur de critères
     * @param query
     *            la recherche principale
     * @param rootEntity
     *            la racine de l'entité période dans le schéma de données
     * @param vip
     *            true si la recherche doit se faire sur des contrats VIP uniquement, false si sur des contrats non VIP uniquement
     * @return la clause sur le raffinement par statut VIP de contrat
     */
    private Expression<Boolean> creeClauseContratVip(CriteriaBuilder cb, CriteriaQuery<?> query, Root<?> rootEntity, boolean vip) {
        Subquery<Long> subqueryExtContrat = query.subquery(Long.class);
        Root<ExtensionContrat> extContratRoot = subqueryExtContrat.from(ExtensionContrat.class);
        subqueryExtContrat.select(extContratRoot.<Long> get("idTechnique"));
        Expression<Boolean> restrictionExtContrat = cb.equal(extContratRoot.get("numContrat"), rootEntity.get(PARAM_NUMERO_CONTRAT));
        restrictionExtContrat = cb.and(restrictionExtContrat, cb.equal(extContratRoot.get("vipAsText"), "O"));
        subqueryExtContrat.where(restrictionExtContrat);
        if (vip) {
            return cb.exists(subqueryExtContrat);
        } else {
            return cb.not(cb.exists(subqueryExtContrat));
        }
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public ParametresPartitionPeriodes recupereDonneesGlobalesDuPartionning() {
        String sql = "SELECT COUNT(*) as NB, MIN(s.ID_PERIODE) as MIN, MAX(s.ID_PERIODE) as MAX FROM V_Selection_periode_nature_contrat_effectifs s";

        return getJdbcTemplate().query(sql, new ParametresPartitionPeriodesRowMapper()).get(0);
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public Long recupereNombreTotalElementCalculesPourPartionning() {
        String sql = "select max(NB_CUMULE) from"
                + " (SELECT e.ID_PERIODE, e.NB_ENTITES_ECCT, @running_total := @running_total + e.NB_ENTITES_ECCT AS NB_CUMULE"
                + " FROM V_Effectif_categorie_contrat_travail_a_calculer e JOIN (SELECT @running_total := 0) c ORDER BY e.ID_PERIODE) t";

        Map<String, Object> params = new HashMap<String, Object>();

        return getJdbcTemplate().queryForList(sql, params, Long.class).get(0);
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public List<Long> recupereDonneesDuPartionning(String partitionnement) {
        String sql = "select t.ID_PERIODE from " + DEBUT_REQUETE_RANG + partitionnement + FIN_REQUETE_RANG
                + " where t.NB_CUMULE in (select MIN(NB_CUMULE) AS CUMUL from " + DEBUT_REQUETE_RANG + partitionnement + FIN_REQUETE_RANG
                + " group by NTILE) or NB_CUMULE in (select MAX(NB_CUMULE) AS CUMUL from " + DEBUT_REQUETE_RANG + partitionnement + FIN_REQUETE_RANG
                + " group by NTILE)";

        Map<String, Object> params = new HashMap<String, Object>();

        return getJdbcTemplate().queryForList(sql, params, Long.class);
    }

    /**
     * Mapper des paramètres pour le partitionning du traitement des périodes (reconsolidation sur effectifs).
     * 
     * @author poidij
     *
     */
    public static class ParametresPartitionPeriodesRowMapper implements RowMapper<ParametresPartitionPeriodes> {
        @Override
        public ParametresPartitionPeriodes mapRow(ResultSet rs, int rowNum) throws SQLException {
            ParametresPartitionPeriodes parametresPartition = new ParametresPartitionPeriodes();
            parametresPartition.setNbPeriodes(getInteger(rs, "NB"));
            parametresPartition.setMinIdPeriode(rs.getLong("MIN"));
            parametresPartition.setMaxIdPeriode(rs.getLong("MAX"));
            return parametresPartition;
        }
    }

    @Override
    @Transactional(propagation = Propagation.MANDATORY)
    public void callMysqlProcedure(String sql, Map<String, Object> params) {
        getJdbcTemplate().update(sql, params);
    }
    // FIN

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public List<Trace> recupereTracesSurSalairesBatch405() {
        String sql = "SELECT ID_PERIODE, MESSAGE FROM V_Trace_tranche_categorie_base_assujettie";
        Map<String, Object> params = new HashMap<String, Object>(0);
        return getJdbcTemplate().query(sql, params, new TraceRowMapper());
    }

    /**
     * Mapper des traces du flux 4 (batch RR405).
     * 
     * @author poidij
     *
     */
    public static class TraceRowMapper implements RowMapper<Trace> {
        @Override
        public Trace mapRow(ResultSet rs, int rowNum) throws SQLException {
            Trace trace = new Trace();
            trace.setIdPeriode(rs.getLong("ID_PERIODE"));
            trace.setMessage(rs.getString("MESSAGE"));
            return trace;
        }
    }

    @Override
    public PeriodeRecue getDernierePeriodeContrat(String numeroContrat) {
        String requeteStr = "SELECT p FROM PeriodeRecue p WHERE p.numeroContrat = :" + PARAM_NUMERO_CONTRAT + " and p.typePeriode='DEC'"
                + " ORDER BY p.dateFinPeriode DESC";
        TypedQuery<PeriodeRecue> query = getEntityManager().createQuery(requeteStr, PeriodeRecue.class).setMaxResults(1);
        query.setParameter(PARAM_NUMERO_CONTRAT, numeroContrat);

        List<PeriodeRecue> result = query.getResultList();
        return getPremierResultatSiExiste(result);
    }

    @Override
    public int supprimePeriodeRecue(Long idPeriode) {
        String jpql = "DELETE FROM PeriodeRecue p WHERE p.idPeriode= :idPeriode";
        Query query = getEntityManager().createQuery(jpql);
        query.setParameter(PARAM_ID_PERIODE, idPeriode);
        return query.executeUpdate();
    }

    @Override
    public void modifieReconsoliderPeriodeRecue(Long idPeriode, String reconsolider) {
        String jpql = "UPDATE PeriodeRecue p SET p.reconsoliderAsText= :" + PARAM_RECONSOLIDER + " WHERE p.idPeriode= :" + PARAM_ID_PERIODE;
        Query query = getEntityManager().createQuery(jpql);
        query.setParameter(PARAM_ID_PERIODE, idPeriode);
        query.setParameter(PARAM_RECONSOLIDER, reconsolider);
        query.executeUpdate();
    }

    @Override
    public List<ExportPeriodeExcel> recherchePeriodesRecuesNatifExportExcel(PeriodesRecuesCriteresRecherche criteres) {
        // Search
        String sql = prepareRechercheNatif(criteres, TypeSelectCriteresRecherche.EXPORT_PERIODES);

        // Group By
        sql = sql + " GROUP BY p.ID_PERIODE ";

        // Order
        sql = sql + prepareTriNatif(criteres);

        // Pagine
        int pageTaille = criteres.getPageTaille();
        if (pageTaille != -1) {
            sql = sql + " LIMIT " + pageTaille;

            if (criteres.getPageNumero() != -1) {
                sql = sql + " OFFSET " + (criteres.getPageNumero() - 1) * pageTaille;
            }

        }

        LOGGER.debug("Recherche de périodes avec les critères suivants : {}", criteres);

        Map<String, Object> params = new HashMap<>();
        List<ExportPeriodeExcel> periodes = getJdbcTemplate().query(sql, params, new ExportPeriodeExcelRowMapper());

        LOGGER.debug("Recherche de périodes terminées avec {} périodes", periodes.size());

        return periodes;
    }

    /**
     * Mapper de lignes de résumés d'affiliations.
     * 
     * @author nortaina
     *
     */
    public static class ExportPeriodeExcelRowMapper implements RowMapper<ExportPeriodeExcel> {
        @Override
        public ExportPeriodeExcel mapRow(ResultSet rs, int rowNum) throws SQLException {
            ExportPeriodeExcel periode = new ExportPeriodeExcel();
            periode.setIdPeriode(rs.getLong("ID_PERIODE"));
            periode.setNumeroContrat(rs.getString("NUMERO_CONTRAT"));
            periode.setDateDebutPeriode(rs.getInt("DATE_DEBUT_PERIODE"));
            periode.setDateFinPeriode(rs.getInt("DATE_FIN_PERIODE"));
            periode.setTypePeriode(rs.getString("TYPE_PERIODE"));
            periode.setEtatPeriode(rs.getString("ETAT_PERIODE"));
            periode.setGroupeGestion(rs.getString("NMGRPGES"));
            periode.setNumFamilleProduit(rs.getString("NOFAM"));
            periode.setNumCompteProducteur(rs.getString("NCPROD"));
            periode.setCompteEncaissement(rs.getString("NCENC"));
            periode.setNumSouscripteur(rs.getLong("NOCLI"));
            periode.setNumSiren(rs.getString("NOSIREN"));
            periode.setNumSiret(rs.getString("NOSIRET"));
            periode.setRaisonSociale(rs.getString("LRSO"));
            periode.setVipAsText(rs.getString("VIP"));
            periode.setNbEtablissementsRattaches(rs.getLong("NB_ETAB_RATTACH"));
            periode.setSignaltsRedac(rs.getString("CONCAT_MSG_ALERT_SIGNAL_REDAC_OU_GEST"));
            periode.setRejetsRedac(rs.getString("CONCAT_MSG_ALERT_REJET_REDAC_OU_GEST"));
            periode.setSignaltsSiAval(rs.getString("CONCAT_MSG_ALERT_SIGNAL_SIAVAL"));
            periode.setRejetsSiAval(rs.getString("CONCAT_MSG_ALERT_REJET_SIAVAL"));
            periode.setDerniereDateChangementStatut(rs.getLong("DATEHMS_CHANGEMENT"));
            periode.setDateTraitement(rs.getInt("DATE_TRAITEMENT"));
            periode.setTransfertDSN(rs.getString("TRANSFERT_DSN"));
            periode.setModeGestionDSN(rs.getString("MODE_GEST"));
            periode.setNbSalariesRattaches(rs.getInt("NB_SALARIES"));
            periode.setSommeMontantCotisation(rs.getDouble("SUM_MT_COT"));
            periode.setSommeComposantVersement(rs.getDouble("SUM_MT_COMPO_VERS"));
            periode.setModesPaiementDistincts(rs.getString("MODES_VERS"));
            periode.setAffecteA(rs.getString(COL_AFFECTE_A));
            periode.setATraiterPar(rs.getString(COL_A_TRAITER_PAR));
            periode.setTraitePar(rs.getString(COL_TRAITE_PAR));
            return periode;
        }
    }

    protected String prepareTriNatif(CriteresRecherche criteres) {

        String sql = StringUtils.EMPTY;

        if (!StringUtils.equals(PeriodesRecuesCriteresRecherche.TRI_SPECIFIQUE, criteres.getTriChamp()) && criteres.getTriChamp() != null) {

            // /!\ le nom du critère est dépendant de ce qui a été positionné dans DeclarationsRecherche.jsp au clic sur la colonne
            try {
                Field field = ResumePeriodeRecue.class.getDeclaredField(criteres.getTriChamp());
                String critere = StringUtils.EMPTY;
                Column col = field.getAnnotation(javax.persistence.Column.class);

                if (col == null) {
                    critere = field.getAnnotation(Formula.class).value();
                } else {
                    critere = col.name();
                }

                if (criteres.isTriAsc()) {
                    return ORDER_BY + critere + " ASC";
                } else {
                    return ORDER_BY + critere + " DESC";
                }

            } catch (NoSuchFieldException | SecurityException e) {
                LOGGER.error("critère de tri inconnu", e);
            }

        }

        // Par PERIODE_RECUE.ETAT, dans cet ordre :
        // - PERIODE_RECUE.ETAT = « NIN » non intégré avec rejet (avec lien MESSAGE_CONTROLE.NIVEAU_ALERTE = « REJET », puis
        // - PERIODE_RECUE.ETAT = « NIN » non intégré avec signalement (avec lien MESSAGE_CONTROLE.NIVEAU_ALERTE = « SIGAL », puis
        // - PERIODE_RECUE.ETAT = « NIN » non intégré sans rejet ou signalement (sans lien MESSAGE_CONTROLE.NIVEAU_ALERTE = « REJET » ou
        // MESSAGE_CONTROLE.NIVEAU_ALERTE = « SIGNAL », puis
        // - PERIODE_RECUE.ETAT = « INS » intégrées avec signalements, puis
        // - PERIODE_RECUE.ETAT = « ARI » à réintégrer, puis
        // - PERIODE_RECUE.ETAT = « ENC » en cours d'intégration, puis
        // - PERIODE_RECUE.ETAT = « RCP » en réception, puis
        // - PERIODE_RECUE.ETAT = « TPG » Traité par gestion, puis
        // - PERIODE_RECUE.ETAT = « SSG » Sans suite gestion, puis
        // - PERIODE_RECUE.ETAT = « ING » Intégré avec signalements acquittés, puis
        // - PERIODE_RECUE.ETAT = « INT » Intégré.
        // Puis, au sein d'un même état, dont la date de début de période va de la plus ancienne à la plage la plus récente
        // (PERIODE_RECUE.ETAT.DATE_DEBUT_PERIODE),
        // Puis, au sein d'un même état et d'une même date de début, classé par PERIODE_RECUE.NUMERO_CONTRAT,
        // Puis, au sein d'un même état, même date de début, même contrat, classé par PERIODE_RECUE.TYPE, trié par ORDRE de l'entité PARAM_CODE_LIBELLE pour
        // lequel TBL = « PERIODERECUE » et CHAMP = « TYPE » triée sur la colonne ORDRE.

        // ordreEtatPeriode
        sql = ORDER_BY + "(CASE ETAT_PERIODE WHEN 'NIN' THEN 1 WHEN 'INS' THEN 2 WHEN 'ARI' THEN 3 WHEN 'ENC' THEN 4 "
                + "WHEN 'RCP' THEN 5 WHEN 'TPG' THEN 6 WHEN 'SSG' THEN 7 WHEN 'ING' THEN 8 WHEN 'INT' THEN 9 ELSE 10 END) ASC, ";

        // ordreEtatPeriodeNonIntegree
        sql = sql
                + " (CASE WHEN ETAT_PERIODE = 'NIN' AND (SELECT EXISTS (SELECT m.ID_MSG_CTRL FROM MESSAGE_CONTROLE m WHERE m.ID_PERIODE = ID_PERIODE AND m.NIVEAU_ALERTE = 'REJET')) THEN 2 "
                + " WHEN ETAT_PERIODE = 'NIN' AND (SELECT EXISTS (SELECT m.ID_MSG_CTRL FROM MESSAGE_CONTROLE m WHERE m.ID_PERIODE = ID_PERIODE AND m.NIVEAU_ALERTE = 'SIGNAL')) THEN 1 "
                + " ELSE 0 END) DESC, ";

        // dateDebutPeriode , numeroContrat , ordreLibelleType
        sql = sql
                + " DATE_DEBUT_PERIODE ASC, NUMERO_CONTRAT ASC, (SELECT p.ORDRE FROM PARAM_CODE_LIBELLE p WHERE p.TBL = 'PERIODERECUE' AND p.CHAMP = 'TYPE' AND p.CODE = TYPE_PERIODE) ASC";

        return sql;
    }

    /**
     * Prépare la recherche de période correspondant à des critères donnés.
     * 
     * @param utilisation
     *            Boolean qui selectionne le SELECT suivant l'utilisation false - colones calculées pour l'export excel true - remonte uniquement l'idPeriode
     *            pour l'affectation en masse de gestionnaires (popup)
     * @param criteres
     *            de recherche à transcoder
     * @param utilisation
     *            le type de colonnes à remonter dans le select ( {@see TypeSelectCriteresRecherche )
     * @return la requête SQL générée sous la forme d'une string
     */
    private String prepareRechercheNatif(PeriodesRecuesCriteresRecherche criteres, TypeSelectCriteresRecherche utilisation) {

        StringBuilder query = new StringBuilder();

        // Si utilisation est false pour exportExcel
        if (TypeSelectCriteresRecherche.EXPORT_PERIODES.equals(utilisation)) {
            query.append("SELECT p.ID_PERIODE AS ID_PERIODE,p.NUMERO_CONTRAT AS NUMERO_CONTRAT,p.DATE_DEBUT_PERIODE AS DATE_DEBUT_PERIODE,"
                    + "p.AFFECTE_A AS AFFECTE_A, p.A_TRAITER_PAR AS A_TRAITER_PAR, p.TRAITE_PAR AS TRAITE_PAR,"
                    + " p.DATE_FIN_PERIODE AS DATE_FIN_PERIODE,p.TYPE_PERIODE AS TYPE_PERIODE,p.ETAT_PERIODE AS ETAT_PERIODE, "
                    + " NMGRPGES,NOFAM,NCPROD,NCENC,NOCLI,NOSIREN,NOSIRET,LRSO, MODE_GEST,TRANSFERT_DSN , "
                    + " (coalesce(CAST(GROUP_CONCAT(CASE WHEN (mc.ORIGINE = 'REDAC' OR mc.ORIGINE = 'GEST') AND mc.NIVEAU_ALERTE = '"
                    + ParamControleSignalRejet.NIVEAU_ALERTE_SIGNAL
                    + "' THEN mc.MESSAGE_UTILISATEUR END SEPARATOR '\n' ) AS CHAR),'')) AS CONCAT_MSG_ALERT_SIGNAL_REDAC_OU_GEST, "
                    + " (coalesce(CAST(GROUP_CONCAT(CASE WHEN (mc.ORIGINE = 'REDAC' OR mc.ORIGINE = 'GEST') AND mc.NIVEAU_ALERTE = '"
                    + ParamControleSignalRejet.NIVEAU_ALERTE_REJET
                    + "' THEN mc.MESSAGE_UTILISATEUR END SEPARATOR '\n' ) AS CHAR),'')) AS CONCAT_MSG_ALERT_REJET_REDAC_OU_GEST, "
                    + " (coalesce(CAST(GROUP_CONCAT(CASE WHEN (mc.ORIGINE = 'SIAVAL') AND mc.NIVEAU_ALERTE = '"
                    + ParamControleSignalRejet.NIVEAU_ALERTE_SIGNAL
                    + "' THEN mc.MESSAGE_UTILISATEUR END SEPARATOR '\n' ) AS CHAR),'')) AS CONCAT_MSG_ALERT_SIGNAL_SIAVAL, "
                    + " (coalesce(CAST(GROUP_CONCAT(CASE WHEN (mc.ORIGINE = 'SIAVAL') AND mc.NIVEAU_ALERTE = '"
                    + ParamControleSignalRejet.NIVEAU_ALERTE_REJET
                    + "' THEN mc.MESSAGE_UTILISATEUR END SEPARATOR '\n' ) AS CHAR),'')) AS CONCAT_MSG_ALERT_REJET_SIAVAL , ");

            sousRequeteExtentionContrat(query);
            sousRequeteNbEtablissementsRattachesEtNbSalarie(query);
            sousRequeteDernierChangementStatus(query);
            sousRequeteDateTraitement(query);
            sousRequeteSommeMontantCotisation(query);
            sousRequeteSommeComposantVersementEtMode(query);
        } else if (TypeSelectCriteresRecherche.ID_PERIODES.equals(utilisation)) {
            // on ne remonte que les ID PERIODES ciblés
            query.append("SELECT DISTINCT p.ID_PERIODE AS ID_PERIODE ");
        } else if (TypeSelectCriteresRecherche.ETATS.equals(utilisation)) {
            // on ne remonte que les etats distincts des périodes ciblées
            query.append("SELECT DISTINCT p.ETAT_PERIODE AS ETAT_PERIODE ");
        } else if (TypeSelectCriteresRecherche.COUNT_PERIODES.equals(utilisation)) {
            query.append("SELECT COUNT(DISTINCT p.ID_PERIODE) AS COUNT_PERIODES ");
        }

        // from
        query.append(" FROM PERIODES_RECUES p ");

        query.append(" LEFT JOIN V_Informations_Client_Export vic ON vic.NOCO=p.NUMERO_CONTRAT "
                + " AND vic.DT_DEBUT_SIT <= p.DATE_FIN_PERIODE  AND coalesce(vic.DT_FIN_SIT, 99999999) >= p.DATE_FIN_PERIODE ");

        query.append(" LEFT JOIN V_Indicateurs_Dsn_Export vid ON vid.NOCO = p.NUMERO_CONTRAT "
                + " AND vid.DEBUT_PERIODE = p.DATE_DEBUT_PERIODE AND vid.FIN_PERIODE = p.DATE_FIN_PERIODE ");

        query.append(" LEFT JOIN MESSAGE_CONTROLE mc ON mc.ID_PERIODE = p.ID_PERIODE ");

        // where
        query.append(WHERE);

        // flag utilisé pour marquer la première condition rencontrée
        boolean flagFiltre = false;

        // Critère "Contrat"
        if (StringUtils.isNotBlank(criteres.getNumContrat())) {
            query.append(" p.NUMERO_CONTRAT LIKE '" + escapeQuotes(criteres.getNumContrat()) + "%' ");
            flagFiltre = true;
        }

        // Critère "Statut"
        if (!criteres.getStatutsAsList().isEmpty()) {
            if (flagFiltre) {
                query.append(AND);
            } else {
                flagFiltre = true;
            }
            query.append(" p.ETAT_PERIODE IN ("
                    + org.springframework.util.StringUtils.collectionToDelimitedString(criteres.getStatutsAsList(), ",", "'", "'") + ") ");
        }

        // Critère "Nature"
        if (!criteres.getTypesPeriodeAsList().isEmpty()) {
            if (flagFiltre) {
                query.append(AND);
            } else {
                flagFiltre = true;
            }
            query.append(" p.TYPE_PERIODE IN ("
                    + org.springframework.util.StringUtils.collectionToDelimitedString(criteres.getTypesPeriodeAsList(), ",", "'", "'") + ") ");
        }

        // Critère "Période"
        if (!criteres.getTrimestresAsList().isEmpty() && !criteres.getTrimestresAsList().contains("Toutes")) {
            if (flagFiltre) {
                query.append(AND);
            } else {
                flagFiltre = true;
            }
            conditionCriterePeriode(query, criteres.getTrimestresAsList());
        }

        // Critères sur le contrat lié à la période

        if (verifieSousRechercheSurNumContratNecessaire(criteres)) {
            if (flagFiltre) {
                query.append(AND);
            } else {
                flagFiltre = true;
            }
            conditionsNumContrat(query, criteres);
        }

        // Critère "Versement"
        if (criteres.getVersementsAsList().size() == 1) {
            if (flagFiltre) {
                query.append(AND);
            } else {
                flagFiltre = true;
            }
            conditionVersement(query, criteres.getVersementsAsList().get(0));
        }

        // Critère "Messages"
        List<String> messages = criteres.getMessagesAsList();
        if (!messages.isEmpty() && messages.size() != 3) {
            if (flagFiltre) {
                query.append(AND);
            } else {
                flagFiltre = true;
            }
            conditionMessage(query, messages);
        }

        // Critère "Lib.Messages" F09_RG_1_28
        List<String> idControles = criteres.getIdControlesAsList();
        if (!idControles.isEmpty()) {
            if (flagFiltre) {
                query.append(AND);
            } else {
                flagFiltre = true;
            }
            conditionIdControle(query, idControles);
        }

        // Critère affecté à
        List<String> affecteA = criteres.getAffecteAAsList();
        if (!affecteA.isEmpty()) {
            if (flagFiltre) {
                query.append(AND);
            } else {
                flagFiltre = true;
            }
            generationConditionGestionnaire(query, COL_AFFECTE_A, criteres);
        }

        // Critère à traiter par
        List<String> aTraiterPar = criteres.getaTraiterParAsList();
        if (!aTraiterPar.isEmpty()) {
            if (flagFiltre) {
                query.append(AND);
            } else {
                flagFiltre = true;
            }
            generationConditionGestionnaire(query, COL_A_TRAITER_PAR, criteres);
        }

        // Critère traité par
        List<String> traitePar = criteres.getTraiteParAsList();
        if (!traitePar.isEmpty()) {
            if (flagFiltre) {
                query.append(AND);
            } else {
                flagFiltre = true;
            }
            generationConditionGestionnaire(query, COL_TRAITE_PAR, criteres);
        }

        // Critère "VIP" F09_RG_1_14
        if (criteres.getVipAsList().size() == 1) {
            if (flagFiltre) {
                query.append(AND);
            } else {
                flagFiltre = true;
            }
            conditionVipSimple(query, criteres);
        }

        // Gestion de cas quand il n'y a pas d'autres criteres de recherches mais nous avons soit selectionnee les deux criteres VIP ou aucun criteres VIP
        // Effacer le " WHERE " a la fin de la requete SQL construite ci-dessus pour ne pas le prendre en compte dans notre cas specifique
        int lastClausePosition = query.length() - 7;
        if ((criteres.getVipAsList().isEmpty() || criteres.getVipAsList().size() == 2) && (query.substring(lastClausePosition)).equals(WHERE)) {
            query.replace(lastClausePosition, query.length(), " ");
        }
        // Filtre par numéro de contrat VIP
        if (verifieFiltreParContratVIPNecessaire(criteres)) {

            query.append(" AND p.NUMERO_CONTRAT IN ("
                    + org.springframework.util.StringUtils.collectionToDelimitedString(criteres.getFiltreContratsVIP(), ",", "'", "'") + ") ");
        }

        return query.toString();
    }

    private void generationConditionGestionnaire(StringBuilder query, String colonne, PeriodesRecuesCriteresRecherche criteres) {

        List<String> utilisateursNormaux = null;
        List<String> utilisateursAnormaux = null;

        switch (colonne) {

        case COL_AFFECTE_A:
            utilisateursNormaux = criteres.filtreAffecteAHorsCasParticulier();
            utilisateursAnormaux = criteres.filtreAffecteACasParticulier();
            break;
        case COL_A_TRAITER_PAR:
            utilisateursNormaux = criteres.filtreATraiterParHorsCasParticulier();
            utilisateursAnormaux = criteres.filtreATraiterParCasParticulier();
            break;
        case COL_TRAITE_PAR:
            utilisateursNormaux = criteres.filtreTraiteParHorsCasParticulier();
            utilisateursAnormaux = criteres.filtreTraiteParCasParticulier();
            break;
        default:
            utilisateursNormaux = new ArrayList<String>();
            utilisateursAnormaux = new ArrayList<String>();
        }

        query.append(" ( ");
        if (!utilisateursNormaux.isEmpty()) {
            query.append(" p." + colonne + " IN ("
                    + org.springframework.util.StringUtils.collectionToDelimitedString(utilisateursNormaux, ",", "'", "'") + ") ");

        }

        if (!utilisateursAnormaux.isEmpty() && !utilisateursNormaux.isEmpty()) {
            query.append(" OR ");
        }

        if (!utilisateursAnormaux.isEmpty()) {
            if (utilisateursAnormaux.contains(PeriodesRecuesCriteresRecherche.AUTRE_GEST_CODE)) {
                query.append(" p." + colonne + " NOT IN ( " + " SELECT p.CODE_USER FROM PARAM_UTILISATEUR_GESTIONNAIRE p " + " ) ");
            }

            if (utilisateursAnormaux.size() > 1) {
                query.append(" OR ");
            }

            if (utilisateursAnormaux.contains(PeriodesRecuesCriteresRecherche.NON_AFFECTE_CODE)
                    || utilisateursAnormaux.contains(PeriodesRecuesCriteresRecherche.NON_ASSIGNE_CODE)
                    || utilisateursAnormaux.contains(PeriodesRecuesCriteresRecherche.TRAITE_AUTO_CODE)) {
                query.append(" p." + colonne + " IS NULL ");
            }
        }

        query.append(" ) ");
    }

    /**
     * Génère la condition sur le critère Période
     * 
     * @param query
     *            le StringBuilder contenant la requête
     * @param trimestres
     *            le critère des périodes
     */
    private void conditionCriterePeriode(StringBuilder query, List<String> trimestres) {
        List<String> filtreTrimestre = new ArrayList<String>();

        for (String trimestreAsString : trimestres) {
            Integer trimestre = Integer.valueOf(trimestreAsString);
            Integer finTrimestre = DateRedac.getDernierJourMois(DateRedac.ajouterALaDate(trimestre, 2, DateRedac.MODIFIER_MOIS, "trimestre"));

            filtreTrimestre.add(" (p.DATE_DEBUT_PERIODE <= '" + finTrimestre + "' AND p.DATE_FIN_PERIODE >= '" + trimestre + "')");
        }
        query.append(" ( " + StringUtils.join(filtreTrimestre, " OR ") + " ) ");
    }

    /**
     * Génère les conditions sur le numéro contrat
     * 
     * @param query
     *            le StringBuilder contenant la requête
     * @param criteres
     *            les critères de recherche
     */
    private void conditionsNumContrat(StringBuilder query, PeriodesRecuesCriteresRecherche criteres) {

        String subqueryContrat = " SELECT c.NOCO FROM CONTRATS c LEFT JOIN CLIENTS cl ON c.NOCLI=cl.NOCLI AND c.TMP_BATCH=cl.TMP_BATCH"
                + " WHERE c.TMP_BATCH IS FALSE " + " AND c.DT_DEBUT_SIT <= p.DATE_FIN_PERIODE"
                + " AND ( (COALESCE(c.DT_FIN_SIT,99999999) >= p.DATE_FIN_PERIODE) "
                + " OR (CASE WHEN c.DT_FIN_SIT=0 THEN 99999999 ELSE c.DT_FIN_SIT >= p.DATE_FIN_PERIODE END) ) ";

        // Critère "Grpe Gestion"
        if (!criteres.getGroupesGestionAsList().isEmpty()) {
            subqueryContrat += " AND c.NMGRPGES IN ("
                    + org.springframework.util.StringUtils.collectionToDelimitedString(criteres.getGroupesGestionAsList(), ",", "'", "'") + ") ";
        }
        // Critère "N° Client"
        if (StringUtils.isNotBlank(criteres.getNumClient())) {
            subqueryContrat += " AND c.NOCLI LIKE '" + escapeQuotes(criteres.getNumClient()) + "%' ";
        }
        // Critère "Famille"
        if (!criteres.getFamillesAsList().isEmpty()) {
            subqueryContrat += " AND c.NOFAM IN ("
                    + org.springframework.util.StringUtils.collectionToDelimitedString(criteres.getFamillesAsList(), ",", "'", "'") + ") ";
        }
        // Critère "Cpt Enc"
        if (StringUtils.isNotBlank(criteres.getCptEnc())) {
            subqueryContrat += " AND c.NCENC LIKE '" + escapeQuotes(criteres.getCptEnc()) + "%' ";
        }
        // Critère "Cpt Prod"
        if (StringUtils.isNotBlank(criteres.getCptProd())) {
            subqueryContrat += " AND c.NCPROD LIKE '" + escapeQuotes(criteres.getCptProd()) + "%' ";
        }

        // Critères sur le client principal du contrat
        // Critère "SIREN"
        if (StringUtils.isNotBlank(criteres.getSiren())) {
            subqueryContrat += " AND cl.NOSIREN LIKE '" + escapeQuotes(criteres.getSiren()) + "%' ";
        }
        // Critère "NIC"
        if (StringUtils.isNotBlank(criteres.getNic())) {
            subqueryContrat += " AND cl.NOSIRET LIKE '" + escapeQuotes(criteres.getNic()) + "%' ";
        }

        // Critère "Raison sociale"
        if (StringUtils.isNotBlank(criteres.getRaisonSociale())) {
            subqueryContrat += " AND cl.LRSO LIKE '%" + escapeQuotes(criteres.getRaisonSociale()) + "%' ";
        }

        query.append(" p.NUMERO_CONTRAT IN ( " + subqueryContrat + " ) ");
    }

    /**
     * Génère la condition sur le critère Versement
     * 
     * @param query
     *            le StringBuilder contenant la requête
     * @param flagVersement
     *            Valeur du critère Versement
     */
    private void conditionVersement(StringBuilder query, boolean flagVersement) {
        String subqueryVersement = "SELECT v.ID_VERSEMENT"
                + " FROM VERSEMENT v INNER JOIN ADHESION_ETABLISSEMENT_MOIS adh ON v.ID_ADH_ETAB_MOIS = adh.ID_ADH_ETAB_MOIS AND v.TMP_BATCH = adh.TMP_BATCH "
                + " INNER JOIN RATTACHEMENT_DECLARATIONS_RECUES rdr ON rdr.ID_ADH_ETAB_MOIS = adh.ID_ADH_ETAB_MOIS "
                + " WHERE v.TMP_BATCH IS FALSE AND adh.ID_ADH_ETAB_MOIS IN (SELECT r.ID_ADH_ETAB_MOIS FROM RATTACHEMENT_DECLARATIONS_RECUES r WHERE r.ID_PERIODE = p.ID_PERIODE ) ";

        if (flagVersement) {
            query.append(EXISTS + subqueryVersement + ")");
        } else {
            query.append(NOT_EXISTS + subqueryVersement + ")");
        }
    }

    /**
     * Génère la condition sur le critère Message
     * 
     * @param query
     *            le StringBuilder contenant la requête
     * @param messages
     *            le critère message
     */
    private void conditionMessage(StringBuilder query, List<String> messages) {
        Set<String> niveauxAlertesSansAucun = new HashSet<String>(messages);
        niveauxAlertesSansAucun.remove(ParamControleSignalRejet.NIVEAU_ALERTE_AUCUN);

        String subqueryMessages = StringUtils.EMPTY;
        String subqueryMessagesAucun = StringUtils.EMPTY;

        if (!niveauxAlertesSansAucun.isEmpty()) {
            subqueryMessages = SELECT_ID_PERIODE_MSG_CONTROLE + " FROM MESSAGE_CONTROLE mc "
                    + " WHERE mc.ID_PERIODE=p.ID_PERIODE AND mc.NIVEAU_ALERTE IN ("
                    + org.springframework.util.StringUtils.collectionToDelimitedString(niveauxAlertesSansAucun, ",", "'", "'") + ")";
            subqueryMessages = EXISTS + subqueryMessages + ")";

        }

        if (messages.contains(ParamControleSignalRejet.NIVEAU_ALERTE_AUCUN)) {
            subqueryMessagesAucun = SELECT_ID_PERIODE_MSG_CONTROLE + " FROM MESSAGE_CONTROLE mc " + " WHERE mc.ID_PERIODE=p.ID_PERIODE";
            subqueryMessagesAucun = NOT_EXISTS + subqueryMessagesAucun + ")";

        }

        if (!StringUtils.isEmpty(subqueryMessages) && !StringUtils.isEmpty(subqueryMessagesAucun)) {
            query.append(" ((" + subqueryMessages + ") OR (" + subqueryMessagesAucun + ")) ");
        } else if (!StringUtils.isEmpty(subqueryMessages)) {
            query.append(" (" + subqueryMessages + ") ");
        } else {
            query.append(" (" + subqueryMessagesAucun + ") ");
        }
    }

    /**
     * Génère la condition sur le critère id controle
     * 
     * @param query
     *            le StringBuilder contenant la requête
     * @param idControles
     *            les ids controle pour le critère lib. message
     */
    private void conditionIdControle(StringBuilder query, List<String> idControles) {

        String subqueryIdControles = StringUtils.EMPTY;

        if (!idControles.isEmpty()) {
            subqueryIdControles = SELECT_ID_PERIODE_MSG_CONTROLE + " FROM MESSAGE_CONTROLE mc INNER JOIN PARAM_CONTROLE_SIGNAL_REJET pcsr"
                    + " WHERE mc.ID_PERIODE=p.ID_PERIODE AND  mc.ID_CONTROLE=pcsr.ID_CONTROLE AND pcsr.ID_CONTROLE IN ("
                    + org.springframework.util.StringUtils.collectionToDelimitedString(idControles, ",", "'", "'") + ")";
            subqueryIdControles = EXISTS + subqueryIdControles + ")";
        }

        query.append(" ( " + subqueryIdControles + " ) ");
    }

    /**
     * Génère la condition sur le critère VIP dans le cas où la selection est unique
     * 
     * @param query
     *            le StringBuilder contenant la requête
     * @param criteres
     *            les critères de recherche
     */
    private void conditionVipSimple(StringBuilder query, PeriodesRecuesCriteresRecherche criteres) {
        // Standard
        String subqueryExtContrat = " SELECT ec.ID_TECH FROM EXTENSIONS_CONTRATS ec WHERE ec.NOCO=p.NUMERO_CONTRAT AND ec.VIP='O' ";

        if (!StringUtils.equals(criteres.getVipAsList().get(0), "N")) {
            query.append(EXISTS + subqueryExtContrat + ")");
        } else {
            query.append(NOT_EXISTS + subqueryExtContrat + ")");
        }

    }

    private void sousRequeteExtentionContrat(StringBuilder query) {
        query.append(" (SELECT e.VIP FROM EXTENSIONS_CONTRATS e WHERE e.NOCO = p.NUMERO_CONTRAT LIMIT 1) AS VIP, ");
    }

    /**
     * Génère la sous requête pour obtenir le nombre d'établissements rattachés à la période
     * 
     * @param query
     *            le StringBuilder contenant la requête
     */
    private void sousRequeteNbEtablissementsRattachesEtNbSalarie(StringBuilder query) {
        // note : syntaxe mysql non compatible avec h2
        // rappel (hypothèse): INDIVIDUS.IDENTIFIANT_REPERTOIRE et NTT ne sont jamais à null, mais peuvent être à vide ( '' )

        // version MySQL
        query.append(" (SELECT COUNT(DISTINCT adh.SIREN_ENTREPRISE,adh.NIC_ETABLISSEMENT) " + " FROM ADHESION_ETABLISSEMENT_MOIS adh"
                + " WHERE adh.TMP_BATCH = 0 AND adh.ID_ADH_ETAB_MOIS IN (SELECT r.ID_ADH_ETAB_MOIS"
                + " FROM RATTACHEMENT_DECLARATIONS_RECUES r WHERE r.ID_PERIODE = p.ID_PERIODE)) AS NB_ETAB_RATTACH,"
                + " (SELECT COUNT(DISTINCT (CASE WHEN COALESCE(i.IDENTIFIANT_REPERTOIRE, '') <> '' THEN i.IDENTIFIANT_REPERTOIRE ELSE COALESCE(i.NTT, '') END))"
                + " FROM INDIVIDU i LEFT JOIN ADHESION_ETABLISSEMENT_MOIS adh ON adh.ID_ADH_ETAB_MOIS = i.ID_ADH_ETAB_MOIS AND adh.TMP_BATCH = i.TMP_BATCH "
                + " WHERE adh.TMP_BATCH IS FALSE AND adh.ID_ADH_ETAB_MOIS IN "
                + "(SELECT r.ID_ADH_ETAB_MOIS FROM RATTACHEMENT_DECLARATIONS_RECUES r WHERE r.ID_PERIODE = p.ID_PERIODE)) AS NB_SALARIES, ");

    }

    /**
     * Génère la sous requête pour obtenir la dernière date de changement sur la période
     * 
     * @param query
     *            le StringBuilder contenant la requête
     */
    private void sousRequeteDernierChangementStatus(StringBuilder query) {
        query.append(
                "(SELECT h.DATEHMS_CHANGEMENT FROM HISTO_ETAT_PERIODE h WHERE h.ID_PERIODE = p.ID_PERIODE ORDER BY h.DATEHMS_CHANGEMENT DESC LIMIT 1) AS DATEHMS_CHANGEMENT , ");
    }

    /**
     * Génère la sous requête pour obtenir la dernière date de traitement
     * 
     * @param query
     *            le StringBuilder contenant la requête
     */
    private void sousRequeteDateTraitement(StringBuilder query) {
        query.append(
                "(SELECT c.DATE_TRAITEMENT FROM COMPTE_RENDU_INTEGRATION c WHERE c.ID_PERIODE = p.ID_PERIODE ORDER BY c.DATE_TRAITEMENT DESC LIMIT 1) AS DATE_TRAITEMENT , ");
    }

    /**
     * Génère la sous requête concernant la Somme des montants de cotisation associé à la période
     * 
     * @param query
     *            le StringBuilder contenant la requête
     */
    private void sousRequeteSommeMontantCotisation(StringBuilder query) {
        query.append(" ( SELECT SUM(c.MT_COTISATION) FROM CATEGORIE_QUITTANCEMENT_INDIVIDU c WHERE c.ID_PERIODE = p.ID_PERIODE ) AS SUM_MT_COT , ");
    }

    /**
     * Génère la sous requête concernant la somme des composants Versements et le mode de versement, associé à la période
     * 
     * @param query
     *            le StringBuilder contenant la requête
     */
    private void sousRequeteSommeComposantVersementEtMode(StringBuilder query) {
        query.append(
                "@SUM_MT_COMPO_VERS:=null,@MODES_VERS:=null, (SELECT CONCAT(@SUM_MT_COMPO_VERS:=SUM(cv.MONTANT_VERSE),@MODES_VERS:=CAST(GROUP_CONCAT( DISTINCT v.MODE_PAIEMENT SEPARATOR ',') AS CHAR)) FROM COMPOSANT_VERSEMENT cv LEFT JOIN  VERSEMENT v ON cv.ID_VERSEMENT=v.ID_VERSEMENT AND cv.TMP_BATCH=v.TMP_BATCH "
                        + " LEFT JOIN ADHESION_ETABLISSEMENT_MOIS adh ON v.ID_ADH_ETAB_MOIS = adh.ID_ADH_ETAB_MOIS AND v.TMP_BATCH=adh.TMP_BATCH  "
                        + " WHERE adh.TMP_BATCH IS FALSE AND adh.ID_ADH_ETAB_MOIS IN "
                        + " (SELECT r.ID_ADH_ETAB_MOIS FROM RATTACHEMENT_DECLARATIONS_RECUES r WHERE r.ID_PERIODE = p.ID_PERIODE ) ) AS CONCAT_VERS , @SUM_MT_COMPO_VERS AS SUM_MT_COMPO_VERS,@MODES_VERS AS MODES_VERS ");
    }

    @Override
    public boolean existePeriodeAReconsolider() {
        TypedQuery<Long> query = getEntityManager().createQuery("SELECT count(*) FROM PeriodeRecue p WHERE " + " p.reconsoliderAsText='O'",
                Long.class);

        List<Long> result = query.getResultList();
        if (result.isEmpty() || result.get(0) == null) {
            return false;
        }
        return result.get(0) > 0;
    }

    @Override
    public List<PeriodeRecue> getPeriodes(List<Long> idsPeriode) {
        List<PeriodeRecue> resultat = new ArrayList<>();
        if (!idsPeriode.isEmpty()) {
            String requeteStr = JPA_SELECT_PERIODES_IN + org.springframework.util.StringUtils.collectionToDelimitedString(idsPeriode, ",", "'", "'")
                    + ")";
            TypedQuery<PeriodeRecue> query = getEntityManager().createQuery(requeteStr, PeriodeRecue.class);

            resultat = query.getResultList();
        }

        return resultat;
    }

    @Override
    public PeriodeRecue getPlusAnciennePeriodeCree(List<Long> idsPeriode) {
        List<PeriodeRecue> resultat = new ArrayList<>();
        if (!idsPeriode.isEmpty()) {
            String requeteStr = JPA_SELECT_PERIODES_IN + org.springframework.util.StringUtils.collectionToDelimitedString(idsPeriode, ",", "'", "'")
                    + ") ORDER BY p.auditDateCreation ASC";
            TypedQuery<PeriodeRecue> query = getEntityManager().createQuery(requeteStr, PeriodeRecue.class).setMaxResults(1);

            resultat = query.getResultList();
        }

        return getPremierResultatSiExiste(resultat);
    }

    @Override
    public PeriodeRecue getPlusAnciennePeriodeCreeAvecType(String type, List<Long> idsPeriode) {
        List<PeriodeRecue> resultat = new ArrayList<>();
        if (!idsPeriode.isEmpty()) {
            String requeteStr = JPA_SELECT_PERIODES_IN + org.springframework.util.StringUtils.collectionToDelimitedString(idsPeriode, ",", "'", "'")
                    + ") AND p.typePeriode = :typePeriode ORDER BY p.auditDateCreation ASC";
            TypedQuery<PeriodeRecue> query = getEntityManager().createQuery(requeteStr, PeriodeRecue.class).setMaxResults(1);
            query.setParameter(PARAM_TYPE_PERIODE, type);
            resultat = query.getResultList();
        }

        return getPremierResultatSiExiste(resultat);
    }

    @Override
    public List<Trace> recupereTracesVerificationNocatSurSalaires() {
        String sql = "SELECT ID_PERIODE, MESSAGE FROM V_Trace_Nocat_Inconnu_salaires";
        Map<String, Object> params = new HashMap<String, Object>(0);
        return getJdbcTemplate().query(sql, params, new TraceRowMapper());
    }

    @Override
    public Set<Long> identifiantsPeriodeContratSurSalaireACalculer() {
        String sql = "SELECT vpms.ID_PERIODE FROM V_Selection_periode_nature_contrat_salaires vpms";
        return new HashSet<>(getJdbcTemplate().query(sql, new RowMapper<Long>() {

            @Override
            public Long mapRow(ResultSet rs, int rowNum) throws SQLException {
                return rs.getLong(1);
            }
        }));
    }

    @Override
    public String getAffecteADeLaPeriodeAnterieurePourMemeContrat(String numContrat) {

        String sql = "SELECT AFFECTE_A FROM PERIODES_RECUES WHERE NUMERO_CONTRAT = :" + PARAM_NUMERO_CONTRAT
                + " ORDER BY DATE_FIN_PERIODE DESC LIMIT 1";

        Map<String, Object> params = new HashMap<>();
        params.put(PARAM_NUMERO_CONTRAT, numContrat);
        return getPremierResultatSiExiste(getJdbcTemplate().query(sql, params, new RowMapper<String>() {

            @Override
            public String mapRow(ResultSet rs, int rowNum) throws SQLException {
                return rs.getString(1);
            }
        }));
    }

    @Override
    @Transactional(propagation = Propagation.MANDATORY)
    public void modifiegestionnaireAffecteA(List<Long> idsPeriodes, String affecteA) {
        String sql;

        if (PeriodesRecuesCriteresRecherche.NON_AFFECTE_CODE.equals(affecteA)) {
            sql = "UPDATE PERIODES_RECUES p SET p.AFFECTE_A=null " + CONDITION_ID_PERIODES_IN + PARAM_LISTE_ID_PERIODES
                    + " ) AND (p.AFFECTE_A IS NOT NULL) ";
        } else {
            sql = "UPDATE PERIODES_RECUES p SET p.AFFECTE_A = :" + PARAM_AFFECTE_A + CONDITION_ID_PERIODES_IN + PARAM_LISTE_ID_PERIODES
                    + " ) AND (p.AFFECTE_A IS NULL OR p.AFFECTE_A <> :" + PARAM_AFFECTE_A + " ) ";
        }

        Map<String, Object> params = new HashMap<>();

        params.put(PARAM_AFFECTE_A, affecteA);
        params.put(PARAM_LISTE_ID_PERIODES, idsPeriodes);

        getJdbcTemplate().update(sql, params);

    }

    @Override
    @Transactional(propagation = Propagation.MANDATORY)
    public void modifieGestionnaireATraiterPar(List<Long> idsPeriodes, String aTraiterPar) {
        String sql;

        if (PeriodesRecuesCriteresRecherche.NON_ASSIGNE_CODE.equals(aTraiterPar)) {
            sql = "UPDATE PERIODES_RECUES p SET p.A_TRAITER_PAR=null " + CONDITION_ID_PERIODES_IN + PARAM_LISTE_ID_PERIODES
                    + " ) AND (p.A_TRAITER_PAR IS NOT NULL) ";
        } else {
            sql = "UPDATE PERIODES_RECUES p SET p.A_TRAITER_PAR = :" + PARAM_A_TRAITER_PAR + CONDITION_ID_PERIODES_IN + PARAM_LISTE_ID_PERIODES
                    + " ) AND (p.A_TRAITER_PAR IS NULL OR p.A_TRAITER_PAR <> :" + PARAM_A_TRAITER_PAR + " ) ";
        }

        Map<String, Object> params = new HashMap<>();

        params.put(PARAM_A_TRAITER_PAR, aTraiterPar);
        params.put(PARAM_LISTE_ID_PERIODES, idsPeriodes);

        getJdbcTemplate().update(sql, params);

    }

    @Override
    public List<Long> getIdPeriodesRecherche(PeriodesRecuesCriteresRecherche criteres) {
        String sql = prepareRechercheNatif(criteres, TypeSelectCriteresRecherche.ID_PERIODES) + " ORDER BY p.ID_PERIODE ";

        return getJdbcTemplate().query(sql, new RowMapper<Long>() {

            @Override
            public Long mapRow(ResultSet rs, int rowNum) throws SQLException {
                return rs.getLong(1);
            }
        });
    }

    @Override
    public List<String> getStatutsInvalidePourBlocage(List<Long> idPeriodes) {
        String sql = "SELECT DISTINCT p.ETAT_PERIODE FROM periodes_recues p WHERE p.ETAT_PERIODE <> 'RCP' AND p.ID_PERIODE IN ( :"
                + PARAM_LISTE_ID_PERIODES + ")";

        Map<String, Object> params = new HashMap<>();
        params.put(PARAM_LISTE_ID_PERIODES, (List<Long>) idPeriodes);

        return getJdbcTemplate().query(sql, params, new RowMapper<String>() {

            @Override
            public String mapRow(ResultSet rs, int rowNum) throws SQLException {
                return rs.getString(1);
            }

        });
    }

    @Override
    public void modifieGestionnaireTraitePar(List<Long> idsPeriodes, String userIHM, String userCible, TypeActionPopupChangementEnMasse action) {
        modifieGestionnairePeriodeEnMasse(idsPeriodes, userIHM, userCible, action, "TRAITE_PAR");
    }

    @Override
    public void changementStatutPeriodeEnMasse(String code, List<Long> idPeriodes) {

        Map<String, Object> params = new HashMap<>();

        String sql = "UPDATE PERIODES_RECUES p SET p.ETAT_PERIODE =:" + PARAM_CODE + CONDITION_ID_PERIODES_IN + PARAM_LISTE_ID_PERIODES + " ) ";

        params.put(PARAM_CODE, code);
        params.put(PARAM_LISTE_ID_PERIODES, (List<Long>) idPeriodes);

        getJdbcTemplate().update(sql, params);

    }

    @Override
    public void modifieAuditUserEnMasse(List<Long> idsPeriodes, String userIHM, String userCible, TypeActionPopupChangementEnMasse action) {
        modifieGestionnairePeriodeEnMasse(idsPeriodes, userIHM, userCible, action, "USER_MISE_A_JOUR");
    }

    /**
     * Mise à jour d'une colonne de periode , conditionnée par l'action initiant la modification
     * 
     * @param idsPeriodes
     *            la liste des périodes ciblées
     * @param userIHM
     *            l'utilisateur effectuant l'action
     * @param userCible
     *            l'utilisateur ciblé par l'affectation/assignation
     * @param action
     *            l'action initiant la mise à jour
     * @param champ
     *            la colonne à mettre à jour avec l'user initiant l'action
     */
    private void modifieGestionnairePeriodeEnMasse(List<Long> idsPeriodes, String userIHM, String userCible, TypeActionPopupChangementEnMasse action,
            String champ) {
        Map<String, Object> params = new HashMap<>();

        String sql = "UPDATE PERIODES_RECUES p SET p." + champ + " = :user  WHERE p.ID_PERIODE IN ( :" + PARAM_LISTE_ID_PERIODES + " ) ";

        switch (action.getDesignation()) {
        case "BLOCAGE":
            sql += " AND p.ID_PERIODE NOT IN (SELECT m.ID_PERIODE from message_controle m WHERE m.ID_CONTROLE='"
                    + MessageControle.IDENTIFIANT_CONTROLE_GEST_REJET + AND_CONDITION_ORIGINE + MessageControle.ORIGINE_MESSAGE_GEST
                    + AND_ID_PERIODE_IN + PARAM_LISTE_ID_PERIODES + ") )";
            break;
        case "DEBLOCAGE":
            sql += " AND p.ID_PERIODE IN (SELECT m.ID_PERIODE from message_controle m WHERE m.ID_CONTROLE='"
                    + MessageControle.IDENTIFIANT_CONTROLE_GEST_REJET + AND_CONDITION_ORIGINE + MessageControle.ORIGINE_MESSAGE_GEST
                    + AND_ID_PERIODE_IN + PARAM_LISTE_ID_PERIODES + ") )";
            break;
        case "ATTENTE":
            sql += " AND p.ID_PERIODE NOT IN (SELECT m.ID_PERIODE from message_controle m WHERE m.ID_CONTROLE='"
                    + MessageControle.IDENTIFIANT_CONTROLE_GEST_RETOUR_ETP + AND_CONDITION_ORIGINE + MessageControle.ORIGINE_MESSAGE_GEST
                    + AND_ID_PERIODE_IN + PARAM_LISTE_ID_PERIODES + ") )";
            break;
        case "FIN_ATTENTE":
            sql += " AND p.ID_PERIODE IN (SELECT m.ID_PERIODE from message_controle m WHERE m.ID_CONTROLE='"
                    + MessageControle.IDENTIFIANT_CONTROLE_GEST_RETOUR_ETP + AND_CONDITION_ORIGINE + MessageControle.ORIGINE_MESSAGE_GEST
                    + AND_ID_PERIODE_IN + PARAM_LISTE_ID_PERIODES + ") )";
            break;
        case "ASSIGNER":
            sql += " AND (p.A_TRAITER_PAR IS NULL OR p.A_TRAITER_PAR <> :cible )";
            break;
        case "AFFECTER":
            sql += " AND (p.AFFECTE_A IS NULL OR p.AFFECTE_A <> :cible )";
            break;
        default:
            // par défaut, aucun filtrage suplémentaire
        }

        params.put("user", userIHM);
        params.put("cible", userCible);
        params.put(PARAM_LISTE_ID_PERIODES, idsPeriodes);

        getJdbcTemplate().update(sql, params);
    }

    @Override
    public List<String> getStatutPeriodesRecherche(PeriodesRecuesCriteresRecherche criteres) {
        String sql = prepareRechercheNatif(criteres, TypeSelectCriteresRecherche.ETATS);
        return getJdbcTemplate().query(sql, new RowMapper<String>() {

            @Override
            public String mapRow(ResultSet rs, int rowNum) throws SQLException {
                return rs.getString(1);
            }
        });
    }

    @Override
    public List<String> getStatutPeriodesRecherche(List<Long> idPeriodes) {
        String sql = "SELECT DISTINCT p.ETAT_PERIODE FROM periodes_recues p WHERE p.ID_PERIODE IN (:" + PARAM_LISTE_ID_PERIODES + ")";

        Map<String, Object> params = new HashMap<>();
        params.put(PARAM_LISTE_ID_PERIODES, (List<Long>) idPeriodes);

        return getJdbcTemplate().query(sql, params, new RowMapper<String>() {

            @Override
            public String mapRow(ResultSet rs, int rowNum) throws SQLException {
                return rs.getString(1);
            }

        });
    }

}