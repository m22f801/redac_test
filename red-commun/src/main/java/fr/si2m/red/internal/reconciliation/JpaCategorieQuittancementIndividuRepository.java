package fr.si2m.red.internal.reconciliation;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.TypedQuery;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import fr.si2m.red.reconciliation.CategorieQuittancementIndividu;
import fr.si2m.red.reconciliation.CategorieQuittancementIndividuRepository;

/**
 * Base de données des entités {@link CategorieQuittancementIndividu}, connectée via JPA.
 * 
 * @author poidij
 *
 */
@Repository
public class JpaCategorieQuittancementIndividuRepository extends JpaElementDeclaratifRepository<CategorieQuittancementIndividu>
        implements CategorieQuittancementIndividuRepository {

    /**
     * Le nom de paramètre utilisé pour l'identifiant de période.
     */
    private static final String PARAM_ID_PERIODE = "idPeriode";

    /**
     * Le nom de paramètre utilisé pour l'individu
     */
    private static final String PARAM_INDIVIDU = "individu";

    /**
     * Le nom de paramètre utilisé pour la liste de numéros de catégories de quittancement.
     */
    private static final String PARAM_NUMEROS_CAT_QUIT = "numerosCategorieQuittancement";

    /**
     * Condition sur l'individu
     */
    private static final String SQL_CONDITION_INDIVIDU = " AND c.INDIVIDU = :individu ";

    @Override
    public Class<CategorieQuittancementIndividu> getClassePrototypeEntite() {
        return CategorieQuittancementIndividu.class;
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public CategorieQuittancementIndividu get(Long idPeriode, String numCategorieQuittancement, String individu) {
        String jpql = "SELECT e FROM CategorieQuittancementIndividu e WHERE e.idPeriode = :" + PARAM_ID_PERIODE
                + " AND e.numCategorieQuittancement = :numCategorieQuittancement AND e.individu = :individu";
        TypedQuery<CategorieQuittancementIndividu> query = getEntityManager().createQuery(jpql, CategorieQuittancementIndividu.class)
                .setMaxResults(1);
        query.setParameter(PARAM_ID_PERIODE, idPeriode);
        query.setParameter("numCategorieQuittancement", numCategorieQuittancement);
        query.setParameter(PARAM_INDIVIDU, individu);
        List<CategorieQuittancementIndividu> resultats = query.getResultList();
        return getPremierResultatSiExiste(resultats);
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public CategorieQuittancementIndividu getCategorieQuittancementPourPeriode(Long idPeriode, String numCategorieQuittancement) {
        String jpql = "SELECT c FROM CategorieQuittancementIndividu c WHERE c.idPeriode = :" + PARAM_ID_PERIODE
                + " AND c.numCategorieQuittancement = :numCategorieQuittancement";
        TypedQuery<CategorieQuittancementIndividu> query = getEntityManager().createQuery(jpql, CategorieQuittancementIndividu.class)
                .setMaxResults(1);
        query.setParameter(PARAM_ID_PERIODE, idPeriode);
        query.setParameter("numCategorieQuittancement", numCategorieQuittancement);
        List<CategorieQuittancementIndividu> resultats = query.getResultList();
        return getPremierResultatSiExiste(resultats);
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public Double getSommeMontantsCotisationDeclare(Long idPeriode) {
        String jpql = "SELECT SUM(c.montantCotisation) FROM CategorieQuittancementIndividu c WHERE c.idPeriode = :" + PARAM_ID_PERIODE;
        TypedQuery<Double> query = getEntityManager().createQuery(jpql, Double.class);
        query.setParameter(PARAM_ID_PERIODE, idPeriode);
        return query.getSingleResult();
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public List<String> getNumerosCategorieDistinctsPourPeriode(Long idPeriode) {
        String jpql = "SELECT DISTINCT c.numCategorieQuittancement FROM CategorieQuittancementIndividu c WHERE c.idPeriode = :" + PARAM_ID_PERIODE
                + " ORDER BY c.numCategorieQuittancement";
        TypedQuery<String> query = getEntityManager().createQuery(jpql, String.class);
        query.setParameter(PARAM_ID_PERIODE, idPeriode);
        return query.getResultList();
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public List<Double> getSommesMontantsCotisationPourCategoriesQuittancement(Long idPeriode, List<String> numerosCategorieQuittancement) {
        String jpql = "SELECT SUM(c.montantCotisation) AS somme FROM CategorieQuittancementIndividu c WHERE c.idPeriode = :" + PARAM_ID_PERIODE
                + " AND c.numCategorieQuittancement IN (:numerosCategorieQuittancement) GROUP BY c.numCategorieQuittancement ORDER BY c.numCategorieQuittancement";
        TypedQuery<Double> query = getEntityManager().createQuery(jpql, Double.class);
        query.setParameter(PARAM_ID_PERIODE, idPeriode);
        query.setParameter(PARAM_NUMEROS_CAT_QUIT, numerosCategorieQuittancement);
        return query.getResultList();
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public List<Long> getEffectifsDebutPourPeriodeEtCategoriesQuittancement(Long idPeriode, List<String> numerosCategorieQuittancement) {
        String jpql = "SELECT c.effectifDebut AS somme FROM CategorieQuittancementIndividu c WHERE c.idPeriode = :" + PARAM_ID_PERIODE
                + " AND c.numCategorieQuittancement IN (:numerosCategorieQuittancement) ORDER BY c.numCategorieQuittancement";
        TypedQuery<Long> query = getEntityManager().createQuery(jpql, Long.class);
        query.setParameter(PARAM_ID_PERIODE, idPeriode);
        query.setParameter(PARAM_NUMEROS_CAT_QUIT, numerosCategorieQuittancement);
        return query.getResultList();
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public List<Long> getEffectifsFinPourPeriodeEtCategoriesQuittancement(Long idPeriode, List<String> numerosCategorieQuittancement) {
        String jpql = "SELECT c.effectifFin AS somme FROM CategorieQuittancementIndividu c WHERE c.idPeriode = :" + PARAM_ID_PERIODE
                + " AND c.numCategorieQuittancement IN (:numerosCategorieQuittancement) ORDER BY c.numCategorieQuittancement";
        TypedQuery<Long> query = getEntityManager().createQuery(jpql, Long.class);
        query.setParameter(PARAM_ID_PERIODE, idPeriode);
        query.setParameter(PARAM_NUMEROS_CAT_QUIT, numerosCategorieQuittancement);
        return query.getResultList();
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public Long getEffectifTotalDebutPeriode(Long idPeriode) {
        String jpql = "SELECT SUM(c.effectifDebut) AS somme FROM CategorieQuittancementIndividu c WHERE c.idPeriode = :" + PARAM_ID_PERIODE;
        TypedQuery<Long> query = getEntityManager().createQuery(jpql, Long.class);
        query.setParameter(PARAM_ID_PERIODE, idPeriode);
        return getPremierResultatSiExiste(query.getResultList());
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public Long getEffectifTotalFinPeriode(Long idPeriode) {
        String jpql = "SELECT SUM(c.effectifFin) AS somme FROM CategorieQuittancementIndividu c WHERE c.idPeriode = :" + PARAM_ID_PERIODE;
        TypedQuery<Long> query = getEntityManager().createQuery(jpql, Long.class);
        query.setParameter(PARAM_ID_PERIODE, idPeriode);
        return getPremierResultatSiExiste(query.getResultList());
    }

    @Override
    public boolean existeCategorieQuittancementIndividuPourPeriode(Long idPeriode) {
        TypedQuery<Long> query = getEntityManager()
                .createQuery("SELECT count(*) FROM CategorieQuittancementIndividu c " + "WHERE c.idPeriode = :" + PARAM_ID_PERIODE, Long.class);
        query.setParameter(PARAM_ID_PERIODE, idPeriode);
        List<Long> result = query.getResultList();
        if (result.isEmpty() || result.get(0) == null) {
            return false;
        }
        return result.get(0) > 0;
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public Double getSommeMontantsCotisationEstime(Long idPeriode) {
        String jpql = "SELECT SUM(c.estimationCotisation) FROM CategorieQuittancementIndividu c WHERE c.idPeriode = :" + PARAM_ID_PERIODE;
        TypedQuery<Double> query = getEntityManager().createQuery(jpql, Double.class);
        query.setParameter(PARAM_ID_PERIODE, idPeriode);
        return query.getSingleResult();
    }

    @Override
    public Double getSommeMontantsCotisationEstime(Long idPeriode, String individu) {
        String sql = "SELECT SUM(c.ESTIMATION_COTISATION) FROM categorie_quittancement_individu c WHERE c.ID_PERIODE = :" + PARAM_ID_PERIODE
                + SQL_CONDITION_INDIVIDU;

        Map<String, Object> params = new HashMap<>();
        params.put(PARAM_ID_PERIODE, idPeriode);
        params.put(PARAM_INDIVIDU, individu);

        return getPremierResultatSiExiste(getJdbcTemplate().query(sql, params, new RowMapper<Double>() {

            @Override
            public Double mapRow(ResultSet rs, int rowNum) throws SQLException {
                Double montant = rs.getDouble(1);
                if (rs.wasNull()) {
                    return null;
                } else {
                    return montant;
                }
            }
        }));
    }

    @Override
    public Double getSommeMontantsCotisationEstimePourPopulation(Long idPeriode, String individu, String codePopulation) {
        String sql = "SELECT SUM(CASE WHEN c.NOCAT= :codePopulation THEN c.ESTIMATION_COTISATION ELSE NULL END) FROM categorie_quittancement_individu c WHERE c.ID_PERIODE = :"
                + PARAM_ID_PERIODE + SQL_CONDITION_INDIVIDU;

        Map<String, Object> params = new HashMap<>();
        params.put(PARAM_ID_PERIODE, idPeriode);
        params.put(PARAM_INDIVIDU, individu);
        params.put("codePopulation", codePopulation);

        return getPremierResultatSiExiste(getJdbcTemplate().query(sql, params, new RowMapper<Double>() {

            @Override
            public Double mapRow(ResultSet rs, int rowNum) throws SQLException {
                Double montant = rs.getDouble(1);
                if (rs.wasNull()) {
                    return null;
                } else {
                    return montant;
                }
            }
        }));
    }

}
