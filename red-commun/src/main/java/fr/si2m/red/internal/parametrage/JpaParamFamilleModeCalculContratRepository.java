package fr.si2m.red.internal.parametrage;

import javax.persistence.TypedQuery;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import fr.si2m.red.core.repository.jpa.JpaEntiteImportableRepository;
import fr.si2m.red.parametrage.ParamFamilleContratRepository;
import fr.si2m.red.parametrage.ParamFamilleModeCalculContrat;
import fr.si2m.red.parametrage.ParamFamilleModeCalculContratRepository;
import lombok.Setter;

/**
 * Base de données des entités ParamFamilleModeCalculContrat, connectée via JPA.
 * 
 * @author nortaina
 *
 */
@Repository
public class JpaParamFamilleModeCalculContratRepository extends JpaEntiteImportableRepository<ParamFamilleModeCalculContrat>
        implements ParamFamilleModeCalculContratRepository {

    @Autowired
    @Setter
    private ParamFamilleContratRepository paramFamilleContratRepository;

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public String getModeControleTypesBasesAssujetties(Integer numFamille, String groupeGestion, Integer modeCalculCotisation) {
        String jpql = "SELECT f.modeControleTypesBasesAssujetties FROM ParamFamilleModeCalculContrat f, ParamFamilleContrat pfc "
                + "WHERE f.ligneEnCoursImportBatch IS FALSE "
                + "AND pfc.numFamille = f.numFamille AND pfc.numFamille = :numFamille AND pfc.groupeGestion = :groupeGestion "
                + "AND f.numFamille = :numFamille AND f.modeCalculCotisation = :modeCalculCotisation";
        TypedQuery<String> query = getEntityManager().createQuery(jpql, String.class).setMaxResults(1);
        query.setParameter("numFamille", numFamille);
        query.setParameter("groupeGestion", groupeGestion);
        query.setParameter("modeCalculCotisation", modeCalculCotisation);
        return getPremierResultatSiExiste(query.getResultList());
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public String getModeDeclaration(Integer numFamille, Integer modeCalculCotisation) {
        String jpql = "SELECT f.modeDeclaration FROM ParamFamilleModeCalculContrat f WHERE f.ligneEnCoursImportBatch IS FALSE "
                + "AND f.numFamille = :numFamille AND f.modeCalculCotisation = :modeCalculCotisation";
        TypedQuery<String> query = getEntityManager().createQuery(jpql, String.class).setMaxResults(1);
        query.setParameter("numFamille", numFamille);
        query.setParameter("modeCalculCotisation", modeCalculCotisation);
        return getPremierResultatSiExiste(query.getResultList());
    }

}
