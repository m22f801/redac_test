package fr.si2m.red.internal.reconciliation;

import java.util.List;

import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import fr.si2m.red.core.repository.jpa.JpaEntiteImportableRepository;
import fr.si2m.red.reconciliation.IndicateursDSNPeriode;
import fr.si2m.red.reconciliation.IndicateursDSNPeriodeRepository;

/**
 * Base de données des entités {@link IndicateursDSNPeriode}, connectée via JPA.
 * 
 * @author nortaina
 *
 */
@Repository
public class JpaIndicateursDSNPeriodeRepository extends JpaEntiteImportableRepository<IndicateursDSNPeriode>
        implements IndicateursDSNPeriodeRepository {

    private static final String PARAM_NUM_CONTRAT = "numContrat";

    private static final String PARAM_DEBUT_PERIODE = "debutPeriode";

    private static final String PARAM_FIN_PERIODE = "finPeriode";

    @Override
    @Transactional(propagation = Propagation.MANDATORY)
    public IndicateursDSNPeriode getIndicateursDernierePeriode(String numContrat) {
        String jpql = "SELECT i FROM IndicateursDSNPeriode i WHERE i.ligneEnCoursImportBatch IS FALSE AND i.numContrat = :numContrat ORDER BY i.debutPeriode DESC";
        TypedQuery<IndicateursDSNPeriode> query = getEntityManager().createQuery(jpql, IndicateursDSNPeriode.class).setMaxResults(1);
        query.setParameter(PARAM_NUM_CONTRAT, numContrat);
        return getPremierResultatSiExiste(query.getResultList());

    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public IndicateursDSNPeriode getIndicateursPourPeriode(String numContrat, Integer dateDebutPeriode, Integer dateFinPeriode) {
        TypedQuery<IndicateursDSNPeriode> query = getEntityManager().createQuery(
                "SELECT i FROM IndicateursDSNPeriode i WHERE i.numContrat = :numContrat AND i.debutPeriode = :debutPeriode AND i.finPeriode = :finPeriode",
                IndicateursDSNPeriode.class).setMaxResults(1);
        query.setParameter(PARAM_NUM_CONTRAT, numContrat);
        query.setParameter(PARAM_DEBUT_PERIODE, dateDebutPeriode);
        query.setParameter(PARAM_FIN_PERIODE, dateFinPeriode);
        List<IndicateursDSNPeriode> result = query.getResultList();
        if (result.isEmpty()) {
            return null;
        }
        return result.get(0);
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public boolean existeIndicateursPourPeriode(String numContrat, Integer dateDebutPeriode, Integer dateFinPeriode) {
        TypedQuery<IndicateursDSNPeriode> query = getEntityManager().createQuery(
                "SELECT i FROM IndicateursDSNPeriode i WHERE i.numContrat = :numContrat AND i.debutPeriode = :debutPeriode AND i.finPeriode = :finPeriode",
                IndicateursDSNPeriode.class).setMaxResults(1);
        query.setParameter(PARAM_NUM_CONTRAT, numContrat);
        query.setParameter(PARAM_DEBUT_PERIODE, dateDebutPeriode);
        query.setParameter(PARAM_FIN_PERIODE, dateFinPeriode);
        List<IndicateursDSNPeriode> result = query.getResultList();

        return result.size() > 0;
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public boolean existeIndicateursPourPeriodeActuelle(String numContrat, Integer dateDebutPeriode) {
        TypedQuery<IndicateursDSNPeriode> query = getEntityManager()
                .createQuery("SELECT i FROM IndicateursDSNPeriode i WHERE i.numContrat = :numContrat AND i.debutPeriode = :debutPeriode",
                        IndicateursDSNPeriode.class)
                .setMaxResults(1);
        query.setParameter(PARAM_NUM_CONTRAT, numContrat);
        query.setParameter(PARAM_DEBUT_PERIODE, dateDebutPeriode);
        List<IndicateursDSNPeriode> result = query.getResultList();

        return result.size() > 0;
    }

}
