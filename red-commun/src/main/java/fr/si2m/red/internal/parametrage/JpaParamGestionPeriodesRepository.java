package fr.si2m.red.internal.parametrage;

import org.springframework.stereotype.Repository;

import fr.si2m.red.core.repository.jpa.JpaEntiteImportableRepository;
import fr.si2m.red.parametrage.ParamGestionPeriodes;
import fr.si2m.red.parametrage.ParamGestionPeriodesRepository;

/**
 * Base de données des entités {@link ParamGestionPeriodes}, connectée via JPA.
 * 
 * @author nortaina
 *
 */
@Repository
public class JpaParamGestionPeriodesRepository extends JpaEntiteImportableRepository<ParamGestionPeriodes> implements ParamGestionPeriodesRepository {

}
