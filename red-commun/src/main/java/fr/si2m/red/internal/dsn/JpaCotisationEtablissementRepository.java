package fr.si2m.red.internal.dsn;

import java.util.List;

import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import fr.si2m.red.core.repository.jpa.JpaEntiteImportableRepository;
import fr.si2m.red.dsn.CotisationEtablissement;
import fr.si2m.red.dsn.CotisationEtablissementRepository;

/**
 * Base de données des entités {@link CotisationEtablissement}, connectée via JPA.
 * 
 * @author poidij
 *
 */
@Repository
public class JpaCotisationEtablissementRepository extends JpaEntiteImportableRepository<CotisationEtablissement> implements CotisationEtablissementRepository {

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public boolean existeUneCotisationEtablissement(String idCotisationEtablissement) {
        TypedQuery<Long> query = getEntityManager().createQuery("SELECT count(*) FROM CotisationEtablissement c WHERE c.idCotisationEtablissement = :idCotisationEtablissement",
                Long.class);
        query.setParameter("idCotisationEtablissement", idCotisationEtablissement);
        List<Long> result = query.getResultList();
        if (result.isEmpty() || result.get(0) == null) {
            return false;
        }
        return result.get(0) > 0;
    }

}
