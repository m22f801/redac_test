package fr.si2m.red;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Utilitaire de gestion des dates dans REDAC.
 * 
 * @author poidij
 *
 */
public final class DateRedac {
    private static final Logger LOGGER = LoggerFactory.getLogger(DateRedac.class);

    /**
     * Date maximale REDAC.
     */
    public static final int DATE_MAXIMALE = 99999999;

    /**
     * Format de stockage des dates sous forme numérique standard REDAC.
     */
    public static final String FORMAT_DATES = "yyyyMMdd";
    /**
     * Format de lecture de dates DDMMYYYY provenant par exemple de la DSN.
     */
    public static final String FORMAT_DATES_DDMMYYYY = "ddMMyyyy";
    /**
     * Format de lecture de mois provenant par exemple de la DSN.
     */
    public static final String FORMAT_DATES_MOIS = "yyyyMM";
    /**
     * Format de stockage des dates-heures sous forme numérique.
     */
    public static final String FORMAT_DATES_HEURES = "yyyyMMddHHmmss";

    /**
     * Code pour modifier la partie "JOUR" dans une date.
     */
    public static final String MODIFIER_JOUR = "DATE";
    /**
     * Code pour modifier la partie "MOIS" dans une date.
     */
    public static final String MODIFIER_MOIS = "MONTH";
    /**
     * Code pour modifier la partie "ANNEE" dans une date.
     */
    public static final String MODIFIER_ANNEE = "YEAR";

    /**
     * Format de dates standards dans les messages en français générés par REDAC.
     */
    public static final String MESSAGES_FORMAT_PAR_DEFAUT = "dd/MM/yyyy";
    /**
     * Format par défaut attendu pour les exports de données REDAC.
     */
    public static final String EXPORT_FORMAT_PAR_DEFAUT = "dd.MM.yyyy";

    /**
     * Constructeur par défaut.
     */
    private DateRedac() {
    }

    /**
     * Ajoute à la dateAModifier du champ nomChamp un decalage (absolu) sur la partieDeDate.
     * 
     * @param dateAModifier
     *            la date au format numérique REDAC à modifier
     * @param decalage
     *            la modification à appliquer
     * @param partieDeDate
     *            la partie de date à modifier parmi {@value #MODIFIER_ANNEE}, {@value #MODIFIER_MOIS} ou {@value #MODIFIER_JOUR}
     * @param nomChamp
     *            le nom du champ date modifié dans l'entité parente
     * @return la dateModifiee
     */
    public static Integer ajouterALaDate(Integer dateAModifier, int decalage, String partieDeDate, String nomChamp) {
        return modifierDate(dateAModifier, decalage, partieDeDate, nomChamp);
    }

    /**
     * Récupère une date comparable à partir d'une date REDAC.
     * 
     * @param dateRedac
     *            la date au format REDAC
     * 
     * @return la date REDAC dans un format comparable (jamais null)
     */
    public static Integer getDateComparable(Integer dateRedac) {
        return dateRedac != null && dateRedac.intValue() != 0 ? dateRedac : DATE_MAXIMALE;
    }

    /**
     * Retire à la dateAModifier du champ nomChamp un decalage (absolu) sur la partieDeDate.
     * 
     * @param dateAModifier
     *            la date au format numérique REDAC à modifier
     * @param retrait
     *            le retrait à appliquer (nombre positif)
     * @param partieDeDate
     *            la partie de date à modifier parmi {@value #MODIFIER_ANNEE}, {@value #MODIFIER_MOIS} ou {@value #MODIFIER_JOUR}
     * @param nomChamp
     *            le nom du champ date modifié dans l'entité parente
     * @return la dateModifiee
     */
    public static Integer retirerALaDate(Integer dateAModifier, int retrait, String partieDeDate, String nomChamp) {
        return modifierDate(dateAModifier, retrait * -1, partieDeDate, nomChamp);
    }

    /**
     * Modifie la dateAModifier du champ nomChamp selon un decalage (absolu) sur la partieDeDate.
     * 
     * @param dateAModifier
     *            la date au format numérique REDAC à modifier
     * @param decalage
     *            la modification à appliquer
     * @param partieDeDate
     *            la partie de date à modifier parmi {@value #MODIFIER_ANNEE}, {@value #MODIFIER_MOIS} ou {@value #MODIFIER_JOUR}
     * @param nomChamp
     *            le nom du champ date modifié dans l'entité parente
     * @return la dateModifiee
     */
    private static Integer modifierDate(Integer dateAModifier, int decalage, String partieDeDate, String nomChamp) {
        String dateEnString = dateAModifier.toString();
        Date date = null;
        SimpleDateFormat sdf = new SimpleDateFormat(FORMAT_DATES);
        sdf.setLenient(false);
        try {
            date = sdf.parse(dateEnString);
        } catch (ParseException e) {
            LOGGER.error("Erreur de parsing de la date " + nomChamp + " : (" + dateAModifier + ")", e);
            return null;
        }

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);

        if (MODIFIER_JOUR.equals(partieDeDate)) {
            calendar.add(Calendar.DATE, decalage);
        } else if (MODIFIER_MOIS.equals(partieDeDate)) {
            calendar.add(Calendar.MONTH, decalage);
        } else if (MODIFIER_ANNEE.equals(partieDeDate)) {
            calendar.add(Calendar.YEAR, decalage);
        }

        return Integer.valueOf(sdf.format(calendar.getTime()));
    }

    /**
     * Convertit une date en date numérique REDAC.
     * 
     * @param date
     *            la date à convertir
     * @return la date au format numérique REDAC
     */
    public static Integer convertitEnDateRedac(Date date) {
        SimpleDateFormat format = new SimpleDateFormat(FORMAT_DATES);
        String dateFormatee = format.format(date);
        return Integer.valueOf(dateFormatee);
    }

    /**
     * Convertit une date numérique REDAC en date-heure.
     * 
     * @param dateRedac
     *            la date numérique REDAC à convertir
     * @return la date convertie
     */
    public static Date convertitEnDate(Integer dateRedac) {
        if (dateRedac == null || dateRedac == 0) {
            return null;
        }
        SimpleDateFormat format = new SimpleDateFormat(FORMAT_DATES);
        format.setLenient(false);
        try {
            return format.parse(String.valueOf(dateRedac));
        } catch (ParseException e) {
            throw new RedacUnexpectedException("Erreur lors de la lecture d'une date REDAC", e);
        }
    }

    /**
     * Convertit une date-heure en date-heure numérique REDAC.
     * 
     * @param dateHeure
     *            la date-heure à convertir
     * @return la date-heure au format numérique REDAC
     */
    public static Long convertitEnDateHeureRedac(Date dateHeure) {
        SimpleDateFormat format = new SimpleDateFormat(FORMAT_DATES_HEURES);
        String dateHeureFormatee = format.format(dateHeure);
        return Long.valueOf(dateHeureFormatee);
    }

    /**
     * Convertit une date-heure numérique REDAC en date-heure.
     * 
     * @param dateHeureRedac
     *            la date-heure numérique REDAC à convertir
     * @return la date-heure convertie
     */
    public static Date convertitEnDateHeure(Long dateHeureRedac) {
        if (dateHeureRedac == null || dateHeureRedac == 0L) {
            return null;
        }
        SimpleDateFormat format = new SimpleDateFormat(FORMAT_DATES_HEURES);
        format.setLenient(false);
        try {
            return format.parse(String.valueOf(dateHeureRedac));
        } catch (ParseException e) {
            throw new RedacUnexpectedException("Erreur lors de la lecture d'une date REDAC", e);
        }
    }

    /**
     * Formatte une date REDAC.
     * 
     * @param format
     *            le format à utiliser
     * @param dateRedac
     *            la date REDAC à formater
     * @return la date REDAC formatée
     */
    public static String formate(String format, Integer dateRedac) {
        if (dateRedac == null) {
            return StringUtils.EMPTY;
        }
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        Date date = convertitEnDate(dateRedac);
        return sdf.format(date);
    }

    /**
     * Obtient une partie de la date numérique REDAC.
     * 
     * @param dateRedac
     *            la date numérique REDAC à convertir
     * @param partieDeDate
     *            la partie de date à récupérer
     * @return la date convertie
     */
    public static Integer selectionnePartieDate(Integer dateRedac, String partieDeDate) {
        if (dateRedac == null) {
            return null;
        }

        String dateRedacStr = dateRedac.toString();
        String resultStr = "";
        if (MODIFIER_JOUR.equals(partieDeDate)) {
            resultStr = dateRedacStr.substring(6);
        } else if (MODIFIER_MOIS.equals(partieDeDate)) {
            resultStr = dateRedacStr.substring(4, 6);
        } else if (MODIFIER_ANNEE.equals(partieDeDate)) {
            resultStr = dateRedacStr.substring(0, 4);
        }

        return Integer.parseInt(resultStr);
    }

    /**
     * Renverse la date numérique REDAC.
     * 
     * @param dateNumerique
     *            la date numérique REDAC à renverser
     * @return la date renversée
     */
    public static Integer renverse(Integer dateNumerique) {
        Integer dateRenversee = 0;
        Integer dateManipulee = dateNumerique;
        try {
            while (dateManipulee != 0) {
                dateRenversee = dateRenversee * 10 + dateManipulee % 10;
                dateManipulee = dateManipulee / 10;
            }
        } catch (Exception e) {
            LOGGER.warn("Le renversement de la date " + dateNumerique + " a échoué : ", e);
            dateRenversee = null;
        }
        return dateRenversee;
    }

    /**
     * Convertit une date française au format REDAC.
     * 
     * @param dateAsText
     *            la date au format DDMMYYYY à convertir
     * @return la date REDAC convertie si la conversion a réussi, null sinon
     */
    public static Integer convertitDateFrancaiseEnDateRedac(String dateAsText) {
        try {
            return Integer.valueOf(dateAsText.substring(4) + dateAsText.substring(2, 4) + dateAsText.substring(0, 2));
        } catch (Exception e) {
            LOGGER.warn("La conversion de la date '" + dateAsText + "' au format DDMMYYYY a échoué", e);
            return null;
        }
    }

    /**
     * Récupère la date actuelle au format REDAC.
     * 
     * @return la date actuelle au format REDAC
     */
    public static Integer maintenant() {
        return convertitEnDateRedac(Calendar.getInstance().getTime());
    }

    /**
     * Vérifie la validité d'une date REDAC.
     * 
     * @param dateRedac
     *            la date au format REDAC à valider
     * 
     * @return true si la date est valide, false sinon
     */
    public static boolean verifieDateRedac(String dateRedac) {
        if (dateRedac == null || dateRedac.length() != 8) {
            return false;
        }
        SimpleDateFormat format = new SimpleDateFormat(FORMAT_DATES);
        format.setLenient(false);
        try {
            format.parse(dateRedac);
        } catch (ParseException e) {
            return false;
        }
        return true;
    }

    /**
     * Récupère la date du dernier jour du mois d'une date REDAC donné.
     * 
     * @param dateRedac
     *            la date au format REDAC
     * 
     * @return le dernier jour du mois spécifié par la date REDAC donné
     */
    public static Integer getDernierJourMois(Integer dateRedac) {
        if (dateRedac == null) {
            return null;
        }
        SimpleDateFormat format = new SimpleDateFormat(FORMAT_DATES);
        format.setLenient(false);
        Date date;
        try {
            date = format.parse(String.valueOf(dateRedac));
        } catch (ParseException e) {
            return null;
        }
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.set(Calendar.DAY_OF_MONTH, cal.getActualMaximum(Calendar.DAY_OF_MONTH));
        return Integer.valueOf(format.format(cal.getTime()));
    }

    /**
     * Extrait la partie "année" d'une date au format REDAC.
     * 
     * @param dateRedac
     *            la date au format REDAC
     * @return la partie "année" de la date
     */
    public static Integer extraitAnnee(String dateRedac) {
        return Integer.valueOf(dateRedac.substring(0, 4));
    }

    /**
     * Extrait la partie "mois" d'une date au format REDAC.
     * 
     * @param dateRedac
     *            la date au format REDAC
     * @return la partie "mois" de la date
     */
    public static Integer extraitMois(String dateRedac) {
        return Integer.valueOf(dateRedac.substring(4, 6));
    }

    /**
     * Formate la date sans vérification de la validité calendaire de celle ci au format JJ/MM/AAAA
     * 
     * 
     * @param date
     *            entier contenant la date
     * @return une chaine de caractère représentant la date au format JJ/MM/AAAA
     */
    public static String convertionDateRedacSansValidation(Number date) {
        if (date == null || date.longValue() <= 0) {
            return StringUtils.EMPTY;
        }

        String dateAsText = date.toString();
        int sizeDate = dateAsText.length();

        // tronqué aux 8 premiers chiffres
        if (sizeDate > 8) {
            dateAsText = StringUtils.substring(dateAsText, 0, 8);
            sizeDate = 8;
        }

        if (sizeDate > 6 && sizeDate <= 8) {
            return dateAsText.substring(6, sizeDate) + "/" + dateAsText.substring(4, 6) + "/" + dateAsText.substring(0, 4);
        } else if (sizeDate > 2 && sizeDate <= 4) {
            return dateAsText.substring(2, sizeDate) + "/" + dateAsText.substring(0, 2);
        } else {
            return dateAsText;
        }
    }

    /**
     * Formate la date sans vérification de la validité calendaire de celle ci au format JJ/MM/AAAA
     * 
     * @param date
     *            entier contenant la date ( SSAAMMJJ )
     * @return une chaine de caractère représentant la date en format dd/MM
     */
    public static String convertionJourMoisDateRedacSansValidation(Integer date) {
        if (date == null || date <= 0) {
            return StringUtils.EMPTY;
        }

        String dateAsText = date.toString();
        int sizeDate = dateAsText.length();

        if (sizeDate > 6) {
            return dateAsText.substring(6, dateAsText.length()) + "/" + dateAsText.substring(4, 6);
        } else {
            return dateAsText;
        }
    }

}
