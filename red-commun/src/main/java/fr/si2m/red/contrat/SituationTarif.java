package fr.si2m.red.contrat;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;
import javax.persistence.Transient;

import fr.si2m.red.Entite;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * Identifiant de tarif à exporter.
 * 
 * @author nortaina
 *
 */
@Entity
@Table(name = "TMP_RR300_TARIFS_IDS")
@Data
@IdClass(SituationTarifId.class)
@EqualsAndHashCode(callSuper = false, of = { "numContrat", "numCategorie", "dateDebutSituationLigne", "dateDebutSituationContrat", "tauxAppel" })
@ToString(callSuper = false, of = { "numContrat", "numCategorie", "dateDebutSituationLigne", "dateDebutSituationContrat", "tauxAppel" })
public class SituationTarif extends Entite {
    /**
     * UID de version.
     */
    private static final long serialVersionUID = -6374915506905987162L;

    /**
     * Le numéro du contrat complet.
     * 
     * @param numContrat
     *            le numéro du contrat complet
     * @return le numéro du contrat complet
     * 
     */
    @Id
    @Column(name = "NOCO")
    private String numContrat;

    /**
     * Le numéro de catégorie.
     * 
     * @param numCategorie
     *            le numéro de catégorie
     * @return le numéro de catégorie
     * 
     */
    @Id
    @Column(name = "NOCAT")
    private String numCategorie;

    /**
     * La date de début de situation de la ligne; c'est aussi celle du tarif lié.
     * 
     * @param dateDebutSituationLigne
     *            la date de début de situation de la ligne
     * @return la date de début de situation de la ligne
     * 
     */
    @Id
    @Column(name = "DT_DEBUT_SIT")
    private Integer dateDebutSituationLigne;

    /**
     * La date de début de situation de la situation contrat lié.
     * 
     * @param dateDebutSituationContrat
     *            la date de début de situation du contrat lié
     * @return la date de début de situation du contrat lié
     * 
     */
    @Id
    @Column(name = "DT_DEBUT_SIT_CONTRAT")
    private Integer dateDebutSituationContrat;

    /**
     * Le taux d'appel (7 chiffres dont 2 décimales).
     * 
     * @param tauxAppel
     *            le taux d'appel (7 chiffres dont 2 décimales)
     * @return le taux d'appel (7 chiffres dont 2 décimales)
     * 
     */
    @Id
    @Column(name = "TXAPPCOT")
    private Double tauxAppel;

    /**
     * La date de fin de situation de la situation contrat lié.
     * 
     * @param dateFinSituationContrat
     *            la date de fin de situation du contrat lié
     * @return la date de fin de situation du contrat lié
     * 
     */
    @Column(name = "DT_FIN_SIT_CONTRAT")
    private Integer dateFinSituationContrat;

    /**
     * La date de fin de situation de la ligne; c'est aussi celle du tarif lié.
     * 
     * @param dateFinSituationLigne
     *            la date de fin de situation de la ligne
     * @return la date de fin de situation de la ligne
     * 
     */
    @Transient
    private Integer dateFinSituationLigne;

    /**
     * La nature de la base des cotisations du tarif lié.
     * 
     * @param natureBaseCotisations
     *            la nature de la base des cotisations du tarif lié
     * @return la nature de la base des cotisations du tarif lié
     * 
     */
    @Transient
    private Integer natureBaseCotisations;

    /**
     * Le montant des cotisations uniques du tarif lié.
     * 
     * @param montantCotisationsUniques
     *            le montant des cotisations uniques du tarif lié
     * @return le montant des cotisations uniques du tarif lié
     * 
     */
    @Transient
    private Long montantCotisationsUniques;

    /**
     * Le taux de calcul des cotisations uniques du tarif lié.
     * 
     * @param tauxCalculCotisationsUniques
     *            le taux de calcul des cotisations uniques du tarif lié
     * @return le taux de calcul des cotisations uniques du tarif lié
     * 
     */
    @Transient
    private Long tauxCalculCotisationsUniques;

    /**
     * Le taux de base 1 du tarif lié.
     * 
     * @param tauxBase1
     *            le taux de base 1 du tarif lié
     * @return le taux de base 1 du tarif lié
     * 
     */
    @Transient
    private Long tauxBase1;

    /**
     * Le taux de base 2 du tarif lié.
     * 
     * @param tauxBase1
     *            le taux de base 2 du tarif lié
     * @return le taux de base 2 du tarif lié
     * 
     */
    @Transient
    private Long tauxBase2;

    /**
     * Le taux de base 3 du tarif lié.
     * 
     * @param tauxBase3
     *            le taux de base 3 du tarif lié
     * @return le taux de base 3 du tarif lié
     * 
     */
    @Transient
    private Long tauxBase3;

    /**
     * Le taux de base 4 du tarif lié.
     * 
     * @param tauxBase4
     *            le taux de base 4 du tarif lié
     * @return le taux de base 4 du tarif lié
     * 
     */
    @Transient
    private Long tauxBase4;

    /**
     * Constructeur par défaut.
     */
    public SituationTarif() {
        super();
    }

    /**
     * Constructeur paramétré.
     * 
     * @param numContrat
     *            le numéro de contrat
     * @param numCategorie
     *            le numéro de catégorie
     * @param dateDebutSituationLigne
     *            la date de début de situation d'une ligne
     */
    public SituationTarif(String numContrat, String numCategorie, Integer dateDebutSituationLigne) {
        this();
        this.numContrat = numContrat;
        this.numCategorie = numCategorie;
        this.dateDebutSituationLigne = dateDebutSituationLigne;
    }

    /**
     * Constructeur paramétré.
     * 
     * @param numContrat
     *            le numéro de contrat
     * @param numCategorie
     *            le numéro de catégorie
     * @param dateDebutSituationLigne
     *            la date de début de situation d'une ligne
     * @param dateDebutSituationContrat
     *            la date de début de situation contrat d'une ligne
     * @param tauxAppel
     *            le taux d'appel de la situation
     */
    public SituationTarif(String numContrat, String numCategorie, Integer dateDebutSituationLigne, Integer dateDebutSituationContrat,
            Double tauxAppel) {
        this(numContrat, numCategorie, dateDebutSituationLigne);
        this.tauxAppel = tauxAppel;
        this.dateDebutSituationContrat = dateDebutSituationContrat;
    }

    @Override
    public SituationTarifId getId() {
        return new SituationTarifId(numContrat, numCategorie, dateDebutSituationLigne, dateDebutSituationContrat, tauxAppel);
    }

}
