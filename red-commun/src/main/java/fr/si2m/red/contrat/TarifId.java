package fr.si2m.red.contrat;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * Identifiant de tarif REDAC.
 * 
 * @author nortaina
 *
 */
@Data
@EqualsAndHashCode(of = { "ligneEnCoursImportBatch", "numContrat", "numCategorie", "dateDebutSituationLigne" })
@ToString(of = { "ligneEnCoursImportBatch", "numContrat", "numCategorie", "dateDebutSituationLigne" })
@NoArgsConstructor
@AllArgsConstructor
public class TarifId implements Serializable {
    /**
     * UID de version.
     */
    private static final long serialVersionUID = -6374915506905987162L;

    /**
     * Indicateur de ligne temporaire en cours d'import via Batch.
     * 
     * @param ligneEnCoursImportBatch
     *            le nom du SI d'où provient l'information
     * @return le nom du SI d'où provient l'information
     */
    private boolean ligneEnCoursImportBatch;

    /**
     * Le numéro du contrat complet.
     * 
     * @param numContrat
     *            le numéro du contrat complet
     * @return le numéro du contrat complet
     * 
     */
    private String numContrat;

    /**
     * Le numéro de catégorie.
     * 
     * @param numCategorie
     *            le numéro de catégorie
     * @return le numéro de catégorie
     * 
     */
    private String numCategorie;

    /**
     * La date de début de situation de la ligne.
     * 
     * @param dateDebutSituationLigne
     *            la date de début de situation de la ligne
     * @return la date de début de situation de la ligne
     * 
     */
    private Integer dateDebutSituationLigne;

}
