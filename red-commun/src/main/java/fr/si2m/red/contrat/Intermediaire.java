package fr.si2m.red.contrat;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import fr.si2m.red.EntiteImportableBatch;

/**
 * Modèle d'un intermédiaire de contrat DSN, pouvant être représenté notamment à l'intérieur d'un fichier DSN (au format CSV).
 * 
 * @author nortaina
 *
 */
@Entity
@Table(name = "INTERMEDIAIRES")
@IdClass(IntermediaireId.class)
@Data
@EqualsAndHashCode(callSuper = false, of = { "ligneEnCoursImportBatch", "codeIntermediaire" })
@ToString(callSuper = false, of = { "ligneEnCoursImportBatch", "codeIntermediaire" })
public class Intermediaire extends EntiteImportableBatch {

    /**
     * UID de version.
     */
    private static final long serialVersionUID = -2903436097842858571L;

    /**
     * Indicateur de ligne temporaire en cours d'import via Batch.
     * 
     * @param ligneEnCoursImportBatch
     *            le nom du SI d'où provient l'information
     * @return le nom du SI d'où provient l'information
     */
    @Id
    @Column(name = "TMP_BATCH")
    private boolean ligneEnCoursImportBatch;

    /**
     * Le code de l'intermédiaire.
     * 
     * @param codeIntermediaire
     *            le code de l'intermédiaire
     * @return le code de l'intermédiaire
     */
    @Id
    @Column(name = "NCPROD")
    private String codeIntermediaire;

    /**
     * Le type de l'intermédiaire.
     * 
     * @param typeIntermediaire
     *            le type de l'intermédiaire
     * @return le type de l'intermédiaire
     */
    @Column(name = "TYPROD")
    private Integer typeIntermediaire;

    /**
     * Le numéro de compte client.
     * 
     * @param numCompteClient
     *            le numéro de compte client
     * @return le numéro de compte client
     */
    @Column(name = "NCINTERM")
    private Integer numCompteClient;

    /**
     * Le département.
     * 
     * @param numDepartement
     *            le département
     * @return le département
     */
    @Column(name = "NODPT")
    private String departement;

    /**
     * Le code de l'état.
     * 
     * @param codeEtat
     *            le code de l'état
     * @return le code de l'état
     */
    @Column(name = "COETA")
    private Integer codeEtat;

    /**
     * La raison sociale de l'intermédiaire.
     * 
     * @param raisonSociale
     *            la raison sociale
     * @return la raison sociale
     */
    @Column(name = "NMRSO")
    private String raisonSociale;

    /**
     * Le nom de l'exploitant.
     * 
     * @param nomExploitant
     *            le nom de l'exploitant
     * @return le nom de l'exploitant
     */
    @Column(name = "NMEXP")
    private String nomExploitant;

    /**
     * La partie bâtiment pour l'adresse.
     * 
     * @param adresseBatiment
     *            la partie bâtiment pour l'adresse
     * @return la partie bâtiment pour l'adresse
     */
    @Column(name = "ADBAT")
    private String adresseBatiment;

    /**
     * La partie rue pour l'adresse.
     * 
     * @param adresseRue
     *            la partie bâtiment pour l'adresse
     * @return la partie bâtiment pour l'adresse
     */
    @Column(name = "ADRUE")
    private String adresseRue;

    /**
     * La partie commune (au sens lieu) pour l'adresse.
     * 
     * @param adresseCommune
     *            la partie commune (au sens lieu) pour l'adresse
     * @return la partie commune (au sens lieu) pour l'adresse
     */
    @Column(name = "ADCOM")
    private String adresseCommune;

    /**
     * La partie code postal pour l'adresse.
     * 
     * @param adresseCodePostal
     *            la partie code postal pour l'adresse
     * @return la partie code postal pour l'adresse
     */
    @Column(name = "ADCP")
    private String adresseCodePostal;

    /**
     * La partie ville pour l'adresse.
     * 
     * @param adresseVille
     *            la partie ville pour l'adresse
     * @return la partie ville pour l'adresse
     */
    @Column(name = "ADVIL")
    private String adresseVille;

    /**
     * Le code du pays de l'intermédiaire.
     * 
     * @param codePays
     *            le code du pays de l'intermédiaire
     * @return le code du pays de l'intermédiaire
     */
    @Column(name = "COPAYS")
    private String codePays;

    /**
     * Le numéro de SIREN de l'intermédiaire.
     * 
     * @param numSiren
     *            le numéro de SIREN de l'intermédiaire
     * @return le numéro de SIREN de l'intermédiaire
     */
    @Column(name = "NOSIREN")
    private String numSiren;

    /**
     * Le code de filiale QM.
     * 
     * @param codeFiliale
     *            le code de filiale
     * @return le code de filiale
     */
    @Column(name = "COFIR")
    private String codeFiliale;

    /**
     * Le numéro de téléphone.
     * 
     * @param telephone
     *            le numéro de téléphone
     * @return le numéro de téléphone
     */
    @Column(name = "NOTEL")
    private String telephone;

    /**
     * Le numéro de télécopie.
     * 
     * @param fax
     *            le numéro de télécopie
     * @return le numéro de télécopie
     */
    @Column(name = "NOTLC")
    private String fax;

    /**
     * Le réseau de distribution.
     * 
     * @param reseauDistribution
     *            le réseau de distribution
     * @return le réseau de distribution
     */
    @Column(name = "RESDIS")
    private String reseauDistribution;

    /**
     * Le numéro d'inspecteur QM.
     * 
     * @param reseauDistribution
     *            le numéro d'inspecteur
     * @return le numéro d'inspecteur
     */
    @Column(name = "INSPEC")
    private String numInspecteur;

    /**
     * La date de dernière modification.
     * 
     * @param dateDerniereModification
     *            la date de dernière modification
     * @return la date de dernière modification
     */
    @Column(name = "DT_DERN_MODIF")
    private Integer dateDerniereModification;

    @Override
    public IntermediaireId getId() {
        return new IntermediaireId(ligneEnCoursImportBatch, codeIntermediaire);
    }
}
