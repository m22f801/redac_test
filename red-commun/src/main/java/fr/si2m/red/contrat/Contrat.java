package fr.si2m.red.contrat;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.Formula;

import fr.si2m.red.DateRedac;
import fr.si2m.red.EntiteImportableBatch;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * Modèle de contrat DSN, pouvant être représenté notamment à l'intérieur d'un fichier contrat éligible DSN (au format CSV).
 * 
 * @author nortaina
 *
 */
@Entity
@Table(name = "CONTRATS")
@IdClass(ContratId.class)
@Data
@EqualsAndHashCode(callSuper = false, of = { "ligneEnCoursImportBatch", "numContrat", "dateDebutSituationLigne" })
@ToString(callSuper = false, of = { "ligneEnCoursImportBatch", "numContrat", "dateDebutSituationLigne" })
public class Contrat extends EntiteImportableBatch {
    /**
     * UID de version.
     */
    private static final long serialVersionUID = 8846408768775204715L;

    /**
     * Indicateur de ligne temporaire en cours d'import via Batch.
     * 
     * @param ligneEnCoursImportBatch
     *            le nom du SI d'où provient l'information
     * @return le nom du SI d'où provient l'information
     */
    @Id
    @Column(name = "TMP_BATCH")
    private boolean ligneEnCoursImportBatch;

    /**
     * Le numéro du produit dans WPDT.
     * 
     * @param numProduit
     *            le numéro du produit dans WPDT
     * @return le numéro du produit dans WPDT
     */
    @Column(name = "NMPDT")
    private String numProduit;

    /**
     * Le numéro du compte producteur.
     * 
     * @param numCompteProducteur
     *            le numéro du compte producteur
     * @return le numéro du compte producteur
     * 
     */
    @Column(name = "NCPROD")
    private String numCompteProducteur;

    /**
     * Le numéro du client (souscripteur du contrat) sur WCLI.
     * 
     * @param numSouscripteur
     *            le numéro du client (souscripteur du contrat) sur WCLI
     * @return le numéro du client (souscripteur du contrat) sur WCLI
     * 
     */
    @Column(name = "NOCLI")
    private Long numSouscripteur;

    /**
     * Le numéro du client (souscripteur du contrat) sur WCLI.
     * 
     * @param numSouscripteur
     *            le numéro du client (souscripteur du contrat) sur WCLI
     * @return le numéro du client (souscripteur du contrat) sur WCLI
     * 
     */
    /**
     * Le client principal.
     *
     * @param client
     *            Le client principal
     * @return Le client principal
     */
    @ManyToOne
    @Fetch(FetchMode.JOIN)
    @JoinColumns({ @JoinColumn(referencedColumnName = "NOCLI", name = "NOCLI", insertable = false, updatable = false),
            @JoinColumn(referencedColumnName = "TMP_BATCH", name = "TMP_BATCH", insertable = false, updatable = false) })
    private Client client;

    /**
     * Le numéro du contrat complet.
     * 
     * @param numContrat
     *            le numéro du contrat complet
     * @return le numéro du contrat complet
     * 
     */
    @Id
    @Column(name = "NOCO")
    private String numContrat;

    /**
     * La date d'effet du contrat.
     * 
     * @param dateEffetContrat
     *            la date d'effet du contrat
     * @return la date d'effet du contrat
     * 
     */
    @Column(name = "DTEFFCO")
    private Integer dateEffetContrat;

    /**
     * Le type du souscripteur au contrat.
     * 
     * @param typeSouscripteur
     *            le type du souscripteur au contrat
     * @return le type du souscripteur au contrat
     * 
     */
    @Column(name = "TYSOUS")
    private Integer typeSouscripteur;

    /**
     * Le nom du souscripteur au contrat.
     * 
     * @param nomSouscripteur
     *            le nom du souscripteur au contrat
     * @return le nom du souscripteur au contrat
     * 
     */
    @Column(name = "NMSOUS")
    private String nomSouscripteur;

    /**
     * L'état du contrat.
     * 
     * @param etatContrat
     *            l'état du contrat
     * @return l'état du contrat
     * 
     */
    @Column(name = "COETACO")
    private Integer etatContrat;

    /**
     * L'indicateur d'éligibilité à DSN.
     * 
     * @param eligibiliteContratDsn
     *            l'indicateur d'éligibilité à DSN
     * @return l'indicateur d'éligibilité à DSN
     * 
     */
    @Column(name = "ELIGDSN")
    private String eligibiliteContratDsn;

    /**
     * Le mode de quittancement (1, 2 ou 3).
     * 
     * @param modeQuittancementContrat
     *            le mode de quittancement (1, 2 ou 3)
     * @return le mode de quittancement (1, 2 ou 3)
     * 
     */
    @Column(name = "COMOQUIT")
    private String modeQuittancementContrat;

    /**
     * Le type du contrat.
     * 
     * @param typeContrat
     *            le type du contrat
     * @return le type du contrat
     * 
     */
    @Column(name = "TYCO")
    private Integer typeContrat;

    /**
     * Le mode d'émission du contrat (AVANCE / ECHU).
     * 
     * @param modeEmission
     *            le mode d'émission du contrat (AVANCE / ECHU)
     * @return le mode d'émission du contrat (AVANCE / ECHU)
     * 
     */
    @Column(name = "TYEMI")
    private String modeEmission;

    /**
     * Le code de fractionnement (M, T, A, S).
     * 
     * @param codeFractionnement
     *            le code de fractionnement (M, T, A, S)
     * @return le code de fractionnement (M, T, A, S)
     * 
     */
    @Column(name = "COFRQUIT")
    private String codeFractionnement;

    /**
     * Le taux d'appel (7 chiffres dont 2 décimales).
     * 
     * @param tauxAppel
     *            le taux d'appel (7 chiffres dont 2 décimales)
     * @return le taux d'appel (7 chiffres dont 2 décimales)
     * 
     */
    @Column(name = "TXAPPCOT")
    private Double tauxAppel;

    /**
     * Le groupe de gestion.
     * 
     * @param groupeGestion
     *            le groupe de gestion
     * @return le groupe de gestion
     * 
     */
    @Column(name = "NMGRPGES")
    private String groupeGestion;

    /**
     * Le groupe de production.
     * 
     * @param groupeProduction
     *            le groupe de production
     * @return le groupe de production
     * 
     */
    @Column(name = "NMGRPPRO")
    private String groupeProduction;

    /**
     * Le groupe de sinistre.
     * 
     * @param groupeSinistre
     *            le groupe de sinistre
     * @return le groupe de sinistre
     * 
     */
    @Column(name = "NMGRPSIN")
    private String groupeSinistre;

    /**
     * Le groupe des commissions.
     * 
     * @param groupeCommissions
     *            le groupe des commissions
     * @return le groupe des commissions
     * 
     */
    @Column(name = "NMGRPCOM")
    private String groupeCommissions;

    /**
     * Le compte d'encaissement.
     * 
     * @param compteEncaissement
     *            le compte d'encaissement
     * @return le compte d'encaissement
     * 
     */
    @Column(name = "NCENC")
    private String compteEncaissement;

    /**
     * La date de mise à jour du code d'état.
     * 
     * @param dateModifCodeEtat
     *            la date de mise à jour du code d'état
     * @return la date de mise à jour du code d'état
     * 
     */
    @Column(name = "DTCREA")
    private Integer dateModifCodeEtat;

    /**
     * La date de création du contrat.
     * 
     * @param dateMajCodeEtat
     *            la date de création du contrat
     * @return la date de création du contrat
     * 
     */
    @Column(name = "DTCREACO")
    private Integer dateCreationContrat;

    /**
     * Le numéro de la famille du produit.
     * 
     * @param numFamilleProduit
     *            le numéro de la famille du produit
     * @return le numéro de la famille du produit
     * 
     */
    @Column(name = "NOFAM")
    private Integer numFamilleProduit;

    /**
     * La date de début de situation de la ligne.
     * 
     * @param dateDebutSituationLigne
     *            la date de début de situation de la ligne
     * @return la date de début de situation de la ligne
     * 
     */
    @Id
    @Column(name = "DT_DEBUT_SIT")
    private Integer dateDebutSituationLigne;

    /**
     * La date de dernière modification de situation de la ligne.
     * 
     * @param dateDerniereModifSituationLigne
     *            la date de dernière modification de situation de la ligne
     * @return la date de dernière modification de situation de la ligne
     * 
     */
    @Column(name = "DT_DERN_MODIF")
    private Integer dateDerniereModifSituationLigne;

    /**
     * Le numéro de compte sinistres.
     * 
     * @param compteSinistres
     *            le numéro de compte sinistres
     * @return le numéro de compte sinistres
     * 
     */
    @Column(name = "NCSIN")
    private String compteSinistres;

    /**
     * Le RIB complet.
     * 
     * @param ribComplet
     *            le RIB complet
     * @return le RIB complet
     * 
     */
    @Column(name = "NORIB")
    private String ribComplet;

    /**
     * Le code ANI.
     * 
     * @param codeAni
     *            le code ANI
     * @return le code ANI
     * 
     */
    @Column(name = "COANI")
    private String codeAni;

    /**
     * La catégorie objective.
     * 
     * @param categorieObjective
     *            la catégorie objective
     * @return la catégorie objective
     * 
     */
    @Column(name = "CATOBJ")
    private String categorieObjective;

    /**
     * Le code de délégation de paiement (O/N).
     * 
     * @param delegationPaiement
     *            le code de délégation de paiement (O/N)
     * @return le code de délégation de paiement (O/N)
     * 
     */
    @Column(name = "CDLP")
    private String delegationPaiement;

    /**
     * La date de début de délégation.
     * 
     * @param debutDelegation
     *            la date de début de délégation
     * @return la date de début de délégation
     * 
     */
    @Column(name = "DTDEBDLP")
    private Integer debutDelegation;

    /**
     * La date de fin de délégation.
     * 
     * @param finDelegation
     *            la date de fin de délégation
     * @return la date de fin de délégation
     * 
     */
    @Column(name = "DTFINDLP")
    private Integer finDelegation;

    /**
     * Le numéro du client pour la délégation.
     * 
     * @param numClientDelegation
     *            le numéro du client pour la délégation
     * @return le numéro du client pour la délégation
     * 
     */
    @Column(name = "NUCLIDLP")
    private Long numClientDelegation;

    /**
     * Le nombre d'effectifs.
     * 
     * @param effectifs
     *            le nombre d'effectifs
     * @return le nombre d'effectifs
     * 
     */
    @Column(name = "NBEFF")
    private Integer effectifs;

    /**
     * La date de fin de situation de la ligne.
     * 
     * @param dateFinSituationLigne
     *            la date de fin de situation de la ligne
     * @return la date de fin de situation de la ligne
     * 
     */
    @Column(name = "DT_FIN_SIT")
    private Integer dateFinSituationLigne;

    /**
     * La désignation de la famille de produit.
     * 
     * @param designationFamille
     *            la désignation de la famille de produit
     * @return la désignation de la famille de produit
     */
    @Formula("(select p.DESIGNATION_FAMILLE from PARAM_FAMILLE_CONTRAT p where p.NOFAM = NOFAM and p.NMGRPGES = NMGRPGES and p.TMP_BATCH = TMP_BATCH)")
    private String designationFamille;

    /**
     * Récupère la date d'effet du contrat.
     * 
     * @return la date d'effet du contrat
     */
    public Date getDateEffetContratAsDate() {
        return DateRedac.convertitEnDate(getDateEffetContrat());
    }

    /**
     * Récupère le libellé du mode de gestion du contrat.
     * 
     * @return le libellé du mode de gestion du contrat
     */
    public String getLibelleGestion() {
        if (!NumberUtils.isNumber(getCompteEncaissement()) && !StringUtils.startsWith(getCompteEncaissement(), "R1")) {
            return "Déléguée";
        } else {
            return "Directe";
        }
    }

    /**
     * Récupère la date de résiliation du contrat.
     * 
     * @return la date de résiliation du contrat
     */
    public Date getDateDerniereModifSituationLigneAsDate() {
        return DateRedac.convertitEnDate(getDateDerniereModifSituationLigne());
    }

    @Override
    public ContratId getId() {
        return new ContratId(ligneEnCoursImportBatch, numContrat, dateDebutSituationLigne);
    }
}
