package fr.si2m.red.contrat;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

import fr.si2m.red.EntiteImportableBatch;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * Modèle de tarif DSN, pouvant être représenté notamment à l'intérieur d'un fichier DSN (au format CSV).
 * 
 * @author nortaina
 *
 */
@Entity
@Table(name = "TARIFS")
@IdClass(TarifId.class)
@Data
@EqualsAndHashCode(callSuper = false, of = { "numContrat", "ligneEnCoursImportBatch", "numCategorie", "dateDebutSituationLigne" })
@ToString(callSuper = false, of = { "numContrat", "ligneEnCoursImportBatch", "numCategorie", "dateDebutSituationLigne" })
public class Tarif extends EntiteImportableBatch {
    /**
     * UID de version.
     */
    private static final long serialVersionUID = 9153308838957748498L;

    /**
     * Indicateur de ligne temporaire en cours d'import via Batch.
     * 
     * @param ligneEnCoursImportBatch
     *            le nom du SI d'où provient l'information
     * @return le nom du SI d'où provient l'information
     */
    @Id
    @Column(name = "TMP_BATCH")
    private boolean ligneEnCoursImportBatch;

    /**
     * Le numéro du contrat complet.
     * 
     * @param numContrat
     *            le numéro du contrat complet
     * @return le numéro du contrat complet
     * 
     */
    @Id
    @Column(name = "NOCO")
    private String numContrat;

    /**
     * Le numéro de catégorie.
     * 
     * @param numCategorie
     *            le numéro de catégorie
     * @return le numéro de catégorie
     * 
     */
    @Id
    @Column(name = "NOCAT")
    private String numCategorie;

    /**
     * La date de début de situation de la ligne.
     * 
     * @param dateDebutSituationLigne
     *            la date de début de situation de la ligne
     * @return la date de début de situation de la ligne
     * 
     */
    @Id
    @Column(name = "DT_DEBUT_SIT")
    private Integer dateDebutSituationLigne;

    /**
     * L'état du contrat.
     * 
     * @param etatContrat
     *            l'état du contrat
     * @return l'état du contrat
     * 
     */
    @Column(name = "COETACO")
    private Integer etatContrat;

    /**
     * La date de mise à jour du quittancement.
     * 
     * @param dateModificationQuittancement
     *            la date de mise à jour du quittancement
     * @return la date de mise à jour du quittancement
     * 
     */
    @Column(name = "DTMAJQUI")
    private Integer dateModificationQuittancement;

    /**
     * Le mode de calcul des cotisations.
     * 
     * @param modeCalculCotisations
     *            le mode de calcul des cotisations
     * @return le mode de calcul des cotisations
     * 
     */
    @Column(name = "MOCALCOT")
    private Integer modeCalculCotisations;

    /**
     * La nature du contrat
     * 
     * @param natureContrat
     *            indique la nature du contrat (sur salaire ou sur effectif)
     * @return la nature du contrat (sur salaire ou sur effectif)
     */
    @Column(name = "NATURE_CONTRAT")
    private Integer natureContrat;

    /**
     * Le mode de calcul des cotisations uniques.
     * 
     * @param modeCalculCotisationsUniques
     *            le mode de calcul des cotisations uniques
     * @return le mode de calcul des cotisations uniques
     * 
     */
    @Column(name = "MOCALCCU")
    private Integer modeCalculCotisationsUniques;

    /**
     * Le code de la nature du plafond.
     * 
     * @param codeNaturePlafond
     *            le code de la nature du plafond
     * @return le code de la nature du plafond
     * 
     */
    @Column(name = "CONATPLA")
    private Integer codeNaturePlafond;

    /**
     * Le libellé de catégorie.
     * 
     * @param libelleCategorie
     *            le libellé de catégorie
     * @return le libellé de catégorie
     * 
     */
    @Column(name = "LICAT")
    private String libelleCategorie;

    /**
     * Le taux de calcul des cotisations uniques.
     * 
     * @param tauxCalculCotisationsUniques
     *            le taux de calcul des cotisations uniques
     * @return le taux de calcul des cotisations uniques
     * 
     */
    @Column(name = "TXCALCU")
    private Long tauxCalculCotisationsUniques;

    /**
     * Le montant des cotisations uniques.
     * 
     * @param montantCotisationsUniques
     *            le montant des cotisations uniques
     * @return le montant des cotisations uniques
     * 
     */
    @Column(name = "MTCUNCU")
    private Long montantCotisationsUniques;

    /**
     * La nature du taux.
     * 
     * @param natureTaux
     *            la nature du taux
     * @return la nature du taux
     * 
     */
    @Column(name = "CONATTAU")
    private Long natureTaux;

    /**
     * Le taux de base 1.
     * 
     * @param tauxBase1
     *            le taux de base 1
     * @return le taux de base 1
     * 
     */
    @Column(name = "TXBASE1")
    private Long tauxBase1;

    /**
     * Le taux de base 2.
     * 
     * @param tauxBase1
     *            le taux de base 2
     * @return le taux de base 2
     * 
     */
    @Column(name = "TXBASE2")
    private Long tauxBase2;

    /**
     * Le taux de base 3.
     * 
     * @param tauxBase3
     *            le taux de base 3
     * @return le taux de base 3
     * 
     */
    @Column(name = "TXBASE3")
    private Long tauxBase3;

    /**
     * Le taux de base 4.
     * 
     * @param tauxBase4
     *            le taux de base 4
     * @return le taux de base 4
     * 
     */
    @Column(name = "TXBASE4")
    private Long tauxBase4;

    /**
     * La nature de la base des cotisations.
     * 
     * @param natureBaseCotisations
     *            la nature de la base des cotisations
     * @return la nature de la base des cotisations
     * 
     */
    @Column(name = "CONBCOT")
    private Integer natureBaseCotisations;

    /**
     * La date de fin de situation de la ligne.
     * 
     * @param dateFinSituationLigne
     *            la date de fin de situation de la ligne
     * @return la date de fin de situation de la ligne
     * 
     */
    @Column(name = "DT_FIN_SIT")
    private Integer dateFinSituationLigne;

    @Override
    public TarifId getId() {
        return new TarifId(ligneEnCoursImportBatch, numContrat, numCategorie, dateDebutSituationLigne);
    }

}
