package fr.si2m.red.contrat;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * Identifiant d'un intermédiaire.
 * 
 * @author nortaina
 *
 */
@Data
@EqualsAndHashCode(callSuper = false, of = { "ligneEnCoursImportBatch", "codeIntermediaire" })
@ToString(callSuper = false, of = { "ligneEnCoursImportBatch", "codeIntermediaire" })
@NoArgsConstructor
@AllArgsConstructor
public class IntermediaireId implements Serializable {
    /**
     * UID de version.
     */
    private static final long serialVersionUID = 8249649406103696303L;

    /**
     * Indicateur de ligne temporaire en cours d'import via Batch.
     * 
     * @param ligneEnCoursImportBatch
     *            le nom du SI d'où provient l'information
     * @return le nom du SI d'où provient l'information
     */
    private boolean ligneEnCoursImportBatch;

    /**
     * Le code de l'intermédiaire.
     * 
     * @param codeIntermediaire
     *            le code de l'intermédiaire
     * @return le code de l'intermédiaire
     */
    private String codeIntermediaire;

}
