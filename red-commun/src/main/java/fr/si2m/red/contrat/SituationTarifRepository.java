/**
 * Référentiel des SituationsTarifs temporaires
 */
package fr.si2m.red.contrat;

import java.util.List;

/**
 * @author eudesr
 *
 */
public interface SituationTarifRepository {

    /**
     * Récupération des Situations tarifaires calculées, filtrée par noco.
     * 
     * @param numContrat
     *            le numéro de contrat
     * @return la liste des situationsTarifaires
     */
    List<SituationTarif> getSituationTarifsPourNoco(String numContrat);

}
