package fr.si2m.red.contrat;

import java.util.List;

import fr.si2m.red.core.repository.EntiteImportableRepository;

/**
 * Référentiel des contrats.
 * 
 * @author nortaina
 *
 */
public interface ContratRepository extends EntiteImportableRepository<Contrat> {

    /**
     * Récupère un contrat via sa clé fonctionnelle.
     * 
     * @param numContrat
     *            le numéro de contrat
     * @param dateDebutSituationLigne
     *            la date de la situation de la ligne
     * @return le contrat correspondant s'il existe, null sinon
     */
    Contrat getContrat(String numContrat, Integer dateDebutSituationLigne);

    /**
     * Récupère la date de début de situation de contrat minimum parmi les situations de ce contrat.
     * 
     * @param numContrat
     *            le numéro de contrat
     * @return la date de début de situation minimum du contrat, null sinon
     */
    Integer getDateDebutMinimumContratTemporaire(String numContrat);

    /**
     * Récupère la date de début de situation de contrat de la situation de contrat suivante.
     * 
     * @param numContrat
     *            le numéro de contrat
     * @param dateDebutSituationLigne
     *            la date de début de situation du contrat
     * @return la date de début de situation de contrat de la situation de contrat suivante, null sinon
     */
    Integer getDateDebutDeLaSituationContratSuivante(String numContrat, Integer dateDebutSituationLigne);

    /**
     * Retourne la situation de contrat valide pour une date donnée.
     * 
     * @param numContrat
     *            le numéro de contrat
     * @param date
     *            la date de référence
     * @return la situation du contrat valide à la date donnée
     * 
     */
    Contrat getContratValidePourDate(String numContrat, Integer date);

    /**
     * Compte le nombre de contrats éligibles DSN dans le référentiel, filtrés par numéro de groupe de gestion et numéro de famille.
     * 
     * @param numGroupeGestion
     *            le numéro de groupe de gestion (null si aucun filtre)
     * @param numFamille
     *            le numéro de famille (null si aucun filtre)
     * 
     * @return le nombre total de contrats éligibles DSN dans le référentiel
     */
    long compteContratsEligiblesDSN(String numGroupeGestion, Integer numFamille);

    /**
     * Compte le nombre de contrats éligibles DSN dans le référentiel, filtrés par groupe de gestion et numéro de famille.
     * 
     * @param groupesGestion
     *            la liste des groupe de gestion (vide si aucun filtre)
     * @param numsFamilleProduit
     *            la liste des numéros de famille (vide si aucun filtre)
     * 
     * @return le nombre total de contrats éligibles DSN dans le référentiel
     */
    long compteContratsEligiblesDSN(List<String> groupesGestion, List<Integer> numsFamilleProduit, Integer dateDuJour);

    /**
     * Récupère l'entité contrat correspondant à la dernière situation d'un numéro de contrat.
     * 
     * @param numContrat
     *            le numéro de contrat
     * 
     * @return le contrat correspondant à la dernière situation s'il existe, null sinon
     */
    Contrat getDerniereSituationContrat(String numContrat);

    /**
     * Récupère le type de consolidation sur salaire pour un contrat donné.
     * 
     * @param numContrat
     *            le numéro du contrat
     * @param dateEffet
     *            la date d'effet pour déterminer la situation de contrat
     * @return le type de consolidation sur salaire de la situation de contrat recherchée si elle existe, null sinon
     */
    String getTypeConsolidationSalaire(String numContrat, Integer dateEffet);

    /**
     * Récupère la situation de contrat non active la plus ancienne si elle existe sur une période donnée.
     * 
     * @param numContrat
     *            le numéro de contrat
     * @param dateDebutPeriode
     *            la date de début de période
     * @param dateFinPeriode
     *            la date de fin de période
     * @return la situation de contrat non active la plus ancienne si elle existe, null sinon
     */
    Contrat getPlusAncienNonActifSurPeriode(String numContrat, Integer dateDebutPeriode, Integer dateFinPeriode);

    /**
     * Teste si un contrat donné est référencé dans la table des contrats.
     * 
     * @param numContrat
     *            Le numéro du contrat à rechercher
     * @return true si le contrat est référencé au moins une fois, false sinon
     */
    boolean existeContrat(String numContrat);

    /**
     * Récupère la date de résiliation d'un contrat donné.
     * 
     * @param numContrat
     *            le numéro du contrat
     * @return la date de résiliation du contrat
     */
    Integer getDateResiliation(String numContrat);

    /**
     * Récupère la situation de contrat précédente.
     * 
     * @param numContrat
     *            le numéro de contrat
     * @param dateDebutSituationLigne
     *            la date de début de situation du contrat
     * @return la situation de contrat précédente
     */
    Contrat getSituationContratPrecedente(String numContrat, Integer dateDebutSituationLigne);

    /**
     * Récupère la situation de contrat suivante.
     * 
     * @param numContrat
     *            le numéro de contrat
     * @param dateDebutSituationLigne
     *            la date de début de situation du contrat
     * @return la situation de contrat suivante
     */
    Contrat getSituationContratSuivante(String numContrat, Integer dateDebutSituationLigne);

    /**
     * Récupère les donnees contrat pour la periode donnee.
     * 
     * @param idPeriode
     *            l'id de la periode
     * @return la date effet contrat
     */
    ResumeContrat getContratPourPeriodePublieeGERD(Long idPeriode);

    /**
     * Récupère la raison sociale (CLIENTS.LRSO) à partir du numéro de contrat avec selection du premier contrat dont DT_FIN_SIT est null ( contrat en cours )
     * 
     * @param numContrat
     *            le numéro de contrat
     * @return la raison sociale du client
     */
    String getRaisonSocialeClientContrat(String numContrat);

    /**
     * Récupère la liste des Contrats qui contiennent le début de période reçue
     * 
     * @param dateDebutPeriode
     *            la date de début période
     * @param dateFinPeriode
     *            la date de fin période
     * @param numContrat
     *            le numéro de contrat
     * @return le nombre de situation contrat actives sur la plage de temps de la période
     */
    Integer getNombreContratActifPourPeriodeDonnee(Integer dateDebutPeriode, Integer dateFinPeriode, String numContrat);

    /**
     * Retourne le taux d'appel de la situation de contrat valide pour une date donnée.
     * 
     * @param numContrat
     *            le numéro de contrat
     * @param date
     *            la date de référence
     * @return le taux d'appel de la situation du contrat valide à la date donnée
     * 
     */
    Double getTauxAppelContratValidePourDate(String numContrat, Integer date);

}
