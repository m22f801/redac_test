package fr.si2m.red.contrat;

import fr.si2m.red.core.repository.EntiteImportableRepository;

/**
 * Référentiel des clients.
 * 
 * @author nortaina
 *
 */
public interface ClientRepository extends EntiteImportableRepository<Client> {

    /**
     * Test pour savoir si un client existe dans la table Clients à partir de son numéro client.
     * 
     * @param numClient
     *            le numéro de client
     * @return true si le client existe et false sinon
     */
    boolean existeUnClientTemporaire(Long numClient);

    /**
     * Récupération d'un client à partir de son siren dans la table client dont la ligne correspond à la date de modification la plus récente.
     * 
     * @param numSiren
     *            le numéro de SIREN du client
     * @return retourne un client
     */
    Client getClientDerniereModification(String numSiren);

    /**
     * Récupération d'un établissement(client) à partir de son siren et de son nic(numSiret) dont la ligne correspond à la date de modification la plus récente.
     * 
     * @param numSiren
     *            le numéro SIREN de l'établissement
     * @param nic
     *            le NIC de l'établissement
     * @return un client qui correspond à un établissement.
     */
    Client getEtablissementDerniereModification(String numSiren, String nic);

    /**
     * Retourne le SIREN d'un client.
     * 
     * @param numClient
     *            numéro de client
     * @return le code SIREN de l'entreprise cliente si elle existe, null sinon
     */
    String getSiren(Long numClient);

    /**
     * Retourne le code SIRET complet d'un client.
     * 
     * @param numClient
     *            numéro de client
     * @return le code SIRET de l'etablissement cliente si elle existe, null sinon
     */
    String getSiret(Long numClient);
}
