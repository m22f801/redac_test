package fr.si2m.red.contrat;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * Couple "NOCAT/LICAT" généralement utilisé dans le contexte des situations tarifaires.
 * 
 * @author nortaina
 *
 */
@Data
@EqualsAndHashCode(of = { "numCategorie", "libelleCategorie" })
@ToString(of = { "numCategorie", "libelleCategorie" })
@AllArgsConstructor
public class NumCategorieLibelleCategorie implements Serializable {
    /**
     * UID de version.
     */
    private static final long serialVersionUID = 1026059747620488194L;

    /**
     * Le numéro de catégorie.
     * 
     * @param numCategorie
     *            le numéro de catégorie
     * @return le numéro de catégorie
     * 
     */
    private String numCategorie;

    /**
     * Le libellé de catégorie.
     * 
     * @param libelleCategorie
     *            le libellé de catégorie
     * @return le libellé de catégorie
     * 
     */
    private String libelleCategorie;

    /**
     * La nature de la base des cotisations.
     * 
     * @param natureBaseCotisations
     *            la nature de la base des cotisations
     * @return la nature de la base des cotisations
     * 
     */
    private Integer natureBaseCotisations;
}
