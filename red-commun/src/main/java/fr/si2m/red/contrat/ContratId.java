package fr.si2m.red.contrat;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * Identifiant de contrat REDAC.
 * 
 * @author nortaina
 *
 */
@Data
@EqualsAndHashCode(of = { "ligneEnCoursImportBatch", "numContrat", "dateDebutSituationLigne" })
@ToString(of = { "ligneEnCoursImportBatch", "numContrat", "dateDebutSituationLigne" })
@NoArgsConstructor
@AllArgsConstructor
public class ContratId implements Serializable {
    /**
     * UID de version.
     */
    private static final long serialVersionUID = -6284583294790473572L;

    /**
     * Indicateur de ligne temporaire en cours d'import via Batch.
     * 
     * @param ligneEnCoursImportBatch
     *            le nom du SI d'où provient l'information
     * @return le nom du SI d'où provient l'information
     */
    private boolean ligneEnCoursImportBatch = false;

    /**
     * Le numéro du contrat complet.
     * 
     * @param numContrat
     *            le numéro du contrat complet
     * @return le numéro du contrat complet
     * 
     */
    private String numContrat;

    /**
     * La date de début de situation de la ligne.
     * 
     * @param dateDebutSituationLigne
     *            la date de début de situation de la ligne
     * @return la date de début de situation de la ligne
     * 
     */
    private Integer dateDebutSituationLigne;

}
