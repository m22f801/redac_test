package fr.si2m.red.contrat;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import fr.si2m.red.EntiteImportableBatch;

/**
 * Modèle de client (entreprise) DSN, pouvant être représenté notamment à l'intérieur d'un fichier DSN (au format CSV).
 * 
 * @author nortaina
 *
 */
@Entity
@Table(name = "CLIENTS")
@IdClass(ClientId.class)
@Data
@EqualsAndHashCode(callSuper = false, of = { "ligneEnCoursImportBatch", "numClient" })
@ToString(callSuper = false, of = { "ligneEnCoursImportBatch", "numClient" })
public class Client extends EntiteImportableBatch {
    /**
     * UID de version.
     */
    private static final long serialVersionUID = 9153308838957748498L;

    /**
     * Indicateur de ligne temporaire en cours d'import via Batch.
     * 
     * @param ligneEnCoursImportBatch
     *            le nom du SI d'où provient l'information
     * @return le nom du SI d'où provient l'information
     */
    @Id
    @Column(name = "TMP_BATCH")
    private boolean ligneEnCoursImportBatch;

    /**
     * Le numéro de client.
     * 
     * @param numClient
     *            le numéro de client
     * @return le numéro de client
     */
    @Id
    @Column(name = "NOCLI")
    private Long numClient;

    /**
     * Le type de client.
     * 
     * @param typeClient
     *            le type de client
     * @return le type de client
     */
    @Column(name = "TYCLI")
    private Integer typeClient;

    /**
     * La raison sociale du client.
     * 
     * @param raisonSociale
     *            la raison sociale du client
     * @return la raison sociale du client
     */
    @Column(name = "LRSO")
    private String raisonSociale;

    /**
     * La partie bâtiment de l'adresse du client.
     * 
     * @param adresseBatiment
     *            la partie bâtiment de l'adresse du client
     * @return la partie bâtiment de l'adresse du client
     */
    @Column(name = "LBAT")
    private String adresseBatiment;

    /**
     * La partie rue de l'adresse du client.
     * 
     * @param adresseRue
     *            la partie rue de l'adresse du client
     * @return la partie rue de l'adresse du client
     */
    @Column(name = "LRUE")
    private String adresseRue;

    /**
     * La partie commune de l'adresse du client.
     * 
     * @param adresseCommune
     *            la partie commune de l'adresse du client
     * @return la partie commune de l'adresse du client
     */
    @Column(name = "LCMN")
    private String adresseCommune;

    /**
     * La partie code postal de l'adresse du client.
     * 
     * @param adresseCodePostal
     *            la partie code postal de l'adresse du client
     * @return la partie code postal de l'adresse du client
     */
    @Column(name = "COCP")
    private String adresseCodePostal;

    /**
     * Le bureau distributeur du client.
     * 
     * @param bureauDistributeur
     *            le bureau distributeur du client
     * @return le bureau distributeur du client
     */
    @Column(name = "CBD")
    private String bureauDistributeur;

    /**
     * Le code pays du client.
     * 
     * @param codePays
     *            le code pays du client
     * @return le code pays du client
     */
    @Column(name = "COPAY")
    private String codePays;

    /**
     * Le code NAF du client.
     * 
     * @param codeNaf
     *            le code NAF du client
     * @return le code NAF du client
     */
    @Column(name = "COPROF")
    private String codeNaf;

    /**
     * Le numéro SIREN du client.
     * 
     * @param numSiren
     *            le numéro SIREN du client
     * @return le numéro SIREN du client
     */
    @Column(name = "NOSIREN")
    private String numSiren;

    /**
     * Le complément SIRET du client.
     * 
     * @param numSiret
     *            le complément SIRET du client
     * @return le complément SIRET du client
     */
    @Column(name = "NOSIRET")
    private String numSiret;

    /**
     * Le numéro de téléphone du client.
     * 
     * @param telephone
     *            le numéro de téléphone du client
     * @return le numéro de téléphone du client
     */
    @Column(name = "NOTEL")
    private String telephone;

    /**
     * Le numéro de télécopieur du client.
     * 
     * @param fax
     *            le numéro de télécopieur du client
     * @return le numéro de télécopieur du client
     */
    @Column(name = "NOTLC")
    private String fax;

    /**
     * L'état du client.
     * 
     * @param etatClient
     *            l'état du client
     * @return l'état du client
     */
    @Column(name = "COETAT")
    private String etatClient;

    /**
     * La date de l'état du client.
     * 
     * @param dateEtat
     *            la date de l'état du client
     * @return la date de l'état du client
     */
    @Column(name = "DTETAT")
    private Integer dateEtat;

    /**
     * Le code partenaire du client.
     * 
     * @param codePartenaire
     *            le code partenaire du client
     * @return le code partenaire du client
     */
    @Column(name = "NUPART")
    private Integer codePartenaire;

    /**
     * Le numéro du client maître.
     * 
     * @param numClientMaitre
     *            le numéro du client maître
     * @return le numéro du client maître
     */
    @Column(name = "NUMAI")
    private String numClientMaitre;

    /**
     * Le code CCN (convention collective nationale) du client.
     * 
     * @param codeCcn
     *            le code CCN du client
     * @return le code CCN du client
     */
    @Column(name = "COCCN")
    private String codeCcn;

    /**
     * Le code GRP du client.
     * 
     * @param codeGrp
     *            le code GRP du client
     * @return le code GRP du client
     */
    @Column(name = "COGRP")
    private String codeGrp;

    /**
     * Le code TNS (travailleurs non salariés) du client.
     * 
     * @param codeTns
     *            le code TNS du client
     * @return le code TNS du client
     */
    @Column(name = "COTNS")
    private String codeTns;

    /**
     * Le code auto-entrepreneur du client.
     * 
     * @param codeAutoEntrepreneur
     *            le code auto-entrepreneur du client
     * @return le code auto-entrepreneur du client
     */
    @Column(name = "COAUTOENT")
    private String codeAutoEntrepreneur;

    /**
     * La date de dernière modification au format SSAAMMJJ.
     * 
     * @param dateDerniereModification
     *            la date de dernière modification au format SSAAMMJJ
     * @return la date de dernière modification au format SSAAMMJJ
     */
    @Column(name = "DT_DERN_MODIF")
    private Integer dateDerniereModification;

    @Override
    public ClientId getId() {
        return new ClientId(ligneEnCoursImportBatch, numClient);
    }
}
