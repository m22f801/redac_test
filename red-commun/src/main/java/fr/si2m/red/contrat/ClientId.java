package fr.si2m.red.contrat;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * Modèle de client (entreprise) DSN, pouvant être représenté notamment à l'intérieur d'un fichier DSN (au format CSV).
 * 
 * @author nortaina
 *
 */
@Data
@EqualsAndHashCode(callSuper = false, of = { "ligneEnCoursImportBatch", "numClient" })
@ToString(callSuper = false, of = { "ligneEnCoursImportBatch", "numClient" })
@NoArgsConstructor
@AllArgsConstructor
public class ClientId implements Serializable {
    /**
     * UID de version.
     */
    private static final long serialVersionUID = 6841818981579545966L;

    /**
     * Indicateur de ligne temporaire en cours d'import via Batch.
     * 
     * @param ligneEnCoursImportBatch
     *            le nom du SI d'où provient l'information
     * @return le nom du SI d'où provient l'information
     */
    private boolean ligneEnCoursImportBatch;

    /**
     * Le numéro de client.
     * 
     * @param numClient
     *            le numéro de client
     * @return le numéro de client
     */
    private Long numClient;

}
