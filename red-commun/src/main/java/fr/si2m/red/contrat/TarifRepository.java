package fr.si2m.red.contrat;

import java.util.List;
import java.util.Set;

import fr.si2m.red.core.repository.EntiteImportableRepository;

/**
 * Référentiel des tarifs.
 * 
 * @author nortaina
 *
 */
public interface TarifRepository extends EntiteImportableRepository<Tarif> {

    /**
     * Récupère les dates de début des situations tarifaires d'un contrat.
     * 
     * @param numContrat
     *            le numéro de contrat
     * @return la liste des dates de début des situations tarifaires du contrat, null sinon
     */
    Integer getDateDebutMinimumTarifContratTemporaire(String numContrat);

    /**
     * Récupère la date de début de situation de tarif de la situation de tarif suivante.
     * 
     * @param numContrat
     *            le numéro de contrat
     * @param dateDebutSituationLigne
     *            la date de début de situation de ligne
     * @return la date de début de situation de tarif de la situation de tarif suivante, null sinon
     */
    Integer getDateDebutDeLaSituationTarifTemporaireSuivante(String numContrat, Integer dateDebutSituationLigne);

    /**
     * Récuperation des valeurs d'indicateurs par défaut pour un contrat. Fournit le mode de calcul de cotisation pour la date de situation la plus récente et
     * la catégorie la plus petite
     * 
     * @param numContrat
     *            Le numero de contrat est le numero complet
     * @return modeCalculCotisations
     */
    Integer getModeCalculCotisation(String numContrat);

    /**
     * Récupère la liste des situations tarifaires de populations.
     * 
     * @param numContrat
     *            le numéro de contrat des tarifs recherchés
     * @param dateDebutSituation
     *            la date de début de période incluant les situations tarifaires
     * @param dateFinSituation
     *            la date de fin de période incluant les situations tarifaires
     * @return la liste des situations tarifaires sur la période donnée
     */
    List<Tarif> getSituationsTarifsDesPopulations(String numContrat, Integer dateDebutSituation, Integer dateFinSituation);

    /**
     * Récupère les couples "NOCAT/LICAT" distincts pour un numéro de contrat et relatifs à une situation tarifaire effective à une date donnée.
     * 
     * @param numContrat
     *            le numéro de contrat
     * @param dateEffet
     *            la date d'effet des situations tarifaires recherchées
     * @return les couples "NOCAT/LICAT" distincts pour les situations tarifaires du contrat effectives à la date donnée
     */
    Set<NumCategorieLibelleCategorie> getDistinctNumCategorieLibelleCategoriePourContratEtDate(String numContrat, Integer dateEffet);

    /**
     * Récupère le tarif correspondant à un contrat, un numéro de catégorie et une date d'effet donnés.
     * 
     * @param numContrat
     *            le numéro de contrat
     * @param numCategorieQuittancement
     *            le numéro de catégorie
     * @param dateEffet
     *            la date d'effet
     * @return le tarif s'il est trouvé, null sinon
     */
    Tarif getPourNumContratEtNumCategorieEtDateEffet(String numContrat, String numCategorieQuittancement, Integer dateEffet);

    /**
     * Récupère le numéro de catégorie du tarif le plus récent effectif sur une plage de temps donnée.
     * 
     * @param numContrat
     *            le numéro de contrat du tarif recherché
     * @param dateDebutPlage
     *            la date de début de plage pour la recherche
     * @param dateFinPlage
     *            la date de fin de plage pour la recherche
     * @return le numéro de catégorie du tarif le plus récent effectif sur la plage de temps donnée
     */
    String getDernierNumCategorieDeclare(String numContrat, Integer dateDebutPlage, Integer dateFinPlage);

    /**
     * Récupère la situation du tarif la plus récente au numéro de catégorie donné et effectif sur une plage de temps donnée.
     * 
     * @param numContrat
     *            le numéro de contrat du tarif recherché
     * @param numCategorie
     *            le numéro de catégorie du tarif recherché
     * @param dateDebutPlage
     *            la date de début de plage pour la recherche
     * @param dateFinPlage
     *            la date de fin de plage pour la recherche
     * @return la situation du tarif recherché
     */
    Tarif getDerniereSituationTarif(String numContrat, String numCategorie, Integer dateDebutPlage, Integer dateFinPlage);

    /**
     * Récupère les couples "NOCAT/LICAT" distincts pour un numéro de contrat et relatifs à une situation tarifaire effective sur une plage de tems donnée.
     * 
     * @param numContrat
     *            le numéro de contrat
     * @param dateDebutPlage
     *            la date de début de plage des situations tarifaires recherchées
     * @param dateFinPlage
     *            la date de fin de plage des situations tarifaires recherchées
     * @return les couples "NOCAT/LICAT" distincts pour les situations tarifaires du contrat effectives sur la plage donnée
     */
    Set<NumCategorieLibelleCategorie> getDistinctNumCategorieLibelleCategoriePourContratEtPlage(String numContrat, Integer dateDebutPlage,
            Integer dateFinPlage);

    /**
     * Récupère la nature d'un contrat sur une période donnée.
     * 
     * @param numContrat
     *            le numéro de contrat
     * @param dateDebut
     *            la date de début de la période
     * @param dateFin
     *            la date de fin de la période
     * @return la nature du contrat définie par la situation tarifaire recherchée
     */
    Integer getNatureContratPourPeriode(String numContrat, Integer dateDebut, Integer dateFin);

    /**
     * Récupère la nature d'un contrat sur une période donnée avec NOCAT minimal
     * 
     * @param numContrat
     *            le numéro de contrat
     * @param dateDebut
     *            la date de début de la période
     * @param dateFin
     *            la date de fin de la période
     * @return la nature contrat définie par la situation tarifaire recherchée
     */
    Integer getNatureContratPourPeriodeMinimalNoCat(String numContrat, Integer dateDebut, Integer dateFin);

    /**
     * Récupère le mode de calcul de cotisation d'un contrat à afficher sur l'écran de détail du contrat (Flux 2).
     * 
     * @param numContrat
     *            le numéro de contrat
     * @return le mode de calcul de cotisation recherché
     */
    Integer getModeCalculCotisationPourDetailsContrat(String numContrat);

    /**
     * Récupère les "NOCAT" distincts pour un numéro de contrat et relatifs à une situation tarifaire effective à une date donnée.
     * 
     * @param numContrat
     *            le numéro de contrat
     * @param dateEffet
     *            la date d'effet des situations tarifaires recherchées
     * @return les "NOCAT" distincts pour les situations tarifaires du contrat effectives à la date donnée
     */
    List<String> getDistinctNumCategoriePourContratEtDate(String numContrat, Integer dateEffet);

    /**
     * Récupère le numéro de catégorie du tarif valable à la date de début de plage
     * 
     * @param numContrat
     *            le numéro de contrat du tarif recherché
     * @param dateDebutPlage
     *            la date de début de plage pour la recherche
     * @return le numéro de catégorie du tarif valable en début de plage
     */
    String getNumCategorieDeclareValable(String numContrat, Integer dateDebutPlage);

    /**
     * Récupère la liste des Contrats qui contiennent le début de période reçue
     * 
     * @param dateDebutPeriode
     *            la date de début période
     * @param dateFinPeriode
     *            la date de fin période
     * @param numContrat
     *            le numéro de contrat
     * @return le nombre de situation tarifaires actives sur la plage de temps de la période
     */
    Integer getNombreTarifPourPeriodeDonnee(Integer dateDebutPeriode, Integer dateFinPeriode, String numContrat);

    /**
     * Calcul de la colonne NATURE_CONTRAT ( cf F01_RG_T03 )
     */
    void updateNatureContratTarif();
}
