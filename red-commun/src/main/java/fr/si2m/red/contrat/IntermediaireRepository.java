package fr.si2m.red.contrat;

import fr.si2m.red.core.repository.EntiteImportableRepository;

/**
 * Référentiel des intermédiaires.
 * 
 * @author nortaina
 *
 */
public interface IntermediaireRepository extends EntiteImportableRepository<Intermediaire> {
    /**
     * Indique l'existence d'un numéro Itermediaire en base.
     * 
     * @param codeIntermediaire
     *            le code intermediaire
     * @return booleen
     */
    boolean existeUnNcprod(String codeIntermediaire);

    /**
     * Retourne l' Intermediaire correspondant au code Intermediaire (ncProd).
     * 
     * @param codeIntermediaire
     *            le code de l'intermédiaire
     * @return un Intermediaire
     */
    Intermediaire getIntermediaire(String codeIntermediaire);

}
