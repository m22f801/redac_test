package fr.si2m.red.contrat;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * Identifiant de situation de contrat.
 * 
 * @author poidij
 *
 */
@Data
@EqualsAndHashCode(callSuper = false, of = { "numContrat", "numCategorie", "dateDebutSituationLigne", "dateDebutSituationContrat", "tauxAppel" })
@ToString(callSuper = false, of = { "numContrat", "numCategorie", "dateDebutSituationLigne", "dateDebutSituationContrat", "tauxAppel" })
@NoArgsConstructor
@AllArgsConstructor
public class SituationTarifId implements Serializable {
    /**
     * UID de version.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Le numéro du contrat complet.
     * 
     * @param numContrat
     *            le numéro du contrat complet
     * @return le numéro du contrat complet
     * 
     */
    private String numContrat;

    /**
     * Le numéro de catégorie.
     * 
     * @param numCategorie
     *            le numéro de catégorie
     * @return le numéro de catégorie
     * 
     */
    private String numCategorie;

    /**
     * La date de début de situation de la ligne.
     * 
     * @param dateDebutSituationLigne
     *            la date de début de situation de la ligne
     * @return la date de début de situation de la ligne
     * 
     */
    private Integer dateDebutSituationLigne;

    /**
     * La date de début de situation de la situation contrat lié.
     * 
     * @param dateDebutSituationContrat
     *            la date de début de situation du contrat lié
     * @return la date de début de situation du contrat lié
     * 
     */
    private Integer dateDebutSituationContrat;

    /**
     * Le taux d'appel (7 chiffres dont 2 décimales).
     * 
     * @param tauxAppel
     *            le taux d'appel (7 chiffres dont 2 décimales)
     * @return le taux d'appel (7 chiffres dont 2 décimales)
     * 
     */
    private Double tauxAppel;
}
