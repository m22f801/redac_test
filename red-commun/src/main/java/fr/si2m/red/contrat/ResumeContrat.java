package fr.si2m.red.contrat;

import java.io.Serializable;

import lombok.Data;

/**
 * Représentation partielle d'un contrat
 *
 */
@Data
public class ResumeContrat implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -1267116306376787295L;
    /**
     * date effectif contrat
     *
     * @param dteffco
     * 
     * @return dteffco
     */
    private Integer dteffco;

}