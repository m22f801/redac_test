package fr.si2m.red.core.utils;

/**
 * Lecteur de la version de l'application.
 * 
 */
public interface VersionReader {
    /**
     * Lit la version de l'application.
     * 
     * @return la version de l'application
     */
    String readApplicationVersion();
}
