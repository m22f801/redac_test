package fr.si2m.red.core.repository.jpa;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.springframework.beans.factory.InitializingBean;

/**
 * Bean d'initialisation des logs Hibernate par défaut, gérés par java.util.logging.
 * 
 * @author nortaina
 *
 */
public final class HibernateLoggerInitializer implements InitializingBean {
    private Logger hibernateLogger;

    @Override
    public void afterPropertiesSet() throws Exception {
        // Referencé pour éviter le passage du GC sur OpenJDK
        hibernateLogger = Logger.getLogger("org.hibernate");
        hibernateLogger.setLevel(Level.SEVERE);
    }
}
