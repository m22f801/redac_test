/**
 * Module des socles de type référentiel se connectant à des bases de données via JDBC.
 * 
 * @author nortaina
 *
 */
package fr.si2m.red.core.repository.jdbc;

