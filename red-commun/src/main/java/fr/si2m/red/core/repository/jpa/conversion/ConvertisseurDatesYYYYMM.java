package fr.si2m.red.core.repository.jpa.conversion;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.si2m.red.DateRedac;

/**
 * Convertisseur de dates au format "Année-Mois" vers des dates REDAC.
 * 
 * @author poidij
 *
 */
@Converter
public class ConvertisseurDatesYYYYMM implements AttributeConverter<String, Integer> {

    private static final Logger LOGGER = LoggerFactory.getLogger(ConvertisseurDatesYYYYMM.class);

    @Override
    public Integer convertToDatabaseColumn(String attribute) {
        Integer result = null;
        String dateString = attribute;
        if (StringUtils.isNotBlank(dateString)) {
            dateString += "01";
            try {
                result = Integer.valueOf(dateString);
            } catch (Exception e) {
                LOGGER.warn("La conversion de la valeur '" + attribute + "' en Integer a échoué : ", e);
            }
        }
        return result;
    }

    @Override
    public String convertToEntityAttribute(Integer dbData) {
        String result = "";
        if (dbData != null) {
            try {
                result = DateRedac.formate(DateRedac.FORMAT_DATES_MOIS, dbData);
            } catch (Exception e) {
                LOGGER.warn("La conversion de la valeur '" + dbData + "' en String a échoué : ", e);
                result = dbData.toString();
            }
        }
        return result;
    }
}
