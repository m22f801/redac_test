package fr.si2m.red.core.repository.jpa.conversion;

import java.math.BigDecimal;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Convertisseur d'une propriété booléenne en chaîne de caractères O/N.
 * 
 * @author nortaina
 *
 */
@Converter
public class ConvertisseurNumeriqueDecimal implements AttributeConverter<String, Double> {

    private static final Logger LOGGER = LoggerFactory.getLogger(ConvertisseurNumeriqueDecimal.class);

    @Override
    public Double convertToDatabaseColumn(String attribute) {
        Double result = null;
        if (StringUtils.isNotBlank(attribute)) {
            try {
                result = Double.valueOf(attribute.trim());
            } catch (Exception e) {
                LOGGER.warn("La conversion de la valeur '" + attribute.trim() + "' en Double a échoué : ", e);
            }
        }
        return result;
    }

    @Override
    public String convertToEntityAttribute(Double dbData) {
        String result = "";
        if (dbData != null) {
            try {
                result = BigDecimal.valueOf(dbData).toPlainString();
            } catch (Exception e) {
                LOGGER.warn("La conversion de la valeur '" + dbData + "' en String a échoué : ", e);
                result = null;
            }
        }
        return result;
    }

}
