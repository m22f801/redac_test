package fr.si2m.red.core.utils;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Lecteur de la version d'une application à partir d'un Project Object Model généré par Maven.
 * 
 */
public class PomVersionReader implements VersionReader {
    private static final Logger LOGGER = LoggerFactory.getLogger(PomVersionReader.class);

    private final String cachedVersion;

    /**
     * Constructeur par défaut.
     * 
     * @param groupId
     *            l'ID du groupe Maven de l'application
     * @param artifactId
     *            l'ID d'artifact Maven de l'application
     */
    public PomVersionReader(String groupId, String artifactId) {
        cachedVersion = readPomVersion(groupId, artifactId);
    }

    /**
     * Lit et renvoie la version de l'application contenu dans un fichier de propriétés Maven.
     * 
     * @param groupId
     *            l'ID du groupe Maven de l'application
     * @param artifactId
     *            l'ID d'artifact Maven de l'application
     * 
     * @return la version de l'application ciblée
     */
    private String readPomVersion(String groupId, String artifactId) {
        StringBuilder pomPropertiesPathBuilder = new StringBuilder("META-INF/maven/");
        pomPropertiesPathBuilder.append(groupId);
        pomPropertiesPathBuilder.append("/");
        pomPropertiesPathBuilder.append(artifactId);
        pomPropertiesPathBuilder.append("/pom.properties");
        InputStream propIs = getClass().getClassLoader().getResourceAsStream(pomPropertiesPathBuilder.toString());
        if (propIs == null) {
            LOGGER.warn("POM unavailable - please contact support if you encounter this warning");
            return "ROGUE";
        }
        try {
            Properties pomProperties = new Properties();
            pomProperties.load(propIs);
            String version = pomProperties.getProperty("version");
            LOGGER.info("POM version resolved as {} for {}/{}", version, groupId, artifactId);
            return version;
        } catch (IOException e) {
            LOGGER.error("Error while reading POM - please contact support if you encounter this warning", e);
            return "UNRESOLVED";
        } finally {
            IOUtils.closeQuietly(propIs);
        }
    }

    @Override
    public String readApplicationVersion() {
        return cachedVersion;
    }

}
