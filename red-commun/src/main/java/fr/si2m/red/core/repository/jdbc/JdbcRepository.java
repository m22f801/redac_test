package fr.si2m.red.core.repository.jdbc;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

/**
 * Base de configuration pour les référentiels de données fondées sur des connexions JDBC à la base de données REDAC.
 * 
 * @author nortaina
 *
 */
public abstract class JdbcRepository {
    private static final Logger LOGGER = LoggerFactory.getLogger(JdbcRepository.class);

    @Autowired
    private NamedParameterJdbcTemplate jdbcTemplate;

    /**
     * Met à jour le template JDBC pour créer des requêtes à la base.
     * 
     * @param jdbcTemplate
     *            le template JDBC à mettre à jour
     */
    public void setJdbcTemplate(NamedParameterJdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    /**
     * Récupère le template JDBC pour créer des requêtes à la base.
     * 
     * @return le template JDBC pour créer des requêtes à la base
     */
    protected NamedParameterJdbcTemplate getJdbcTemplate() {
        return jdbcTemplate;
    }

    /**
     * Trace l'exécution d'un query.
     * 
     * @param query
     *            la query à tracer
     * @param params
     *            les paramètres de la query
     */
    protected void logQueryExecution(String query, Object params) {
        LOGGER.debug("Executing JDBC query [{}] with parameters {}", query, params);
    }

    /**
     * Récupère un {@link Integer} sur la colonne donnée ou null si aucune valeur entière.
     * 
     * @param resultSet
     *            le set de résultats
     * @param columnName
     *            le nom de la colonne contenant la valeur
     * @return valeur entière corespondante à la colonne, null si aucune valeur entière
     * @throws SQLException
     *             levé en cas d'erreur SQL
     */
    public static Integer getInteger(ResultSet resultSet, String columnName) throws SQLException {
        if (resultSet.getObject(columnName) != null) {
            return resultSet.getInt(columnName);
        } else {
            return null;
        }
    }

}
