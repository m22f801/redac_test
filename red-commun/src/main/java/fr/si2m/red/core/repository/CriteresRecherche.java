package fr.si2m.red.core.repository;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.LinkedList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.reflect.FieldUtils;

import fr.si2m.red.RedacUnexpectedException;

/**
 * Paramètres de configuration techniques pour des requêtes dynamiquement paramétrables.
 * 
 * @author nortaina
 *
 */
public class CriteresRecherche implements Serializable {

    /**
     * UID de version.
     */
    private static final long serialVersionUID = -8642952203442601663L;

    /**
     * Le nom du champ sur lequel appliquer le tri (optionnel).
     * 
     * @param triChamp
     *            le nom du champ sur lequel appliquer le tri à mettre à jour
     * 
     * @return le nom du champ sur lequel appliquer le tri
     */
    @Getter
    @Setter
    private String triChamp = null;
    /**
     * Indique si le tri de la liste est par ordre croissant (optionnel).
     * 
     * @param triAsc
     *            <code>true</code> si le tri doit être par ordre croissant, <code>false</code> s'il doit être par ordre décroissant
     * 
     * @return <code>true</code> si le tri est par ordre croissant, <code>false</code> s'il est par ordre décroissant
     */
    @Getter
    @Setter
    private boolean triAsc = true;

    /**
     * Le numéro de page recherché.
     * 
     * @param pageNumero
     *            le numéro de page recherché
     * @return le numéro de page recherché
     */
    @Getter
    @Setter
    private int pageNumero = 1;

    /**
     * La taille d'une page recherchée.
     * 
     * @param pageTaille
     *            la taille d'une page recherchée
     * @return la taille d'une page recherchée
     */
    @Getter
    @Setter
    private int pageTaille = 50;

    /**
     * Récupère la query string de ces critères de recherche.
     * 
     * @return la query string des critères
     */
    public String toQueryString() {
        List<String> parameters = new LinkedList<String>();
        try {
            Field[] proprietesClasse = FieldUtils.getAllFields(getClass());
            for (Field propriete : proprietesClasse) {
                if (!Modifier.isStatic(propriete.getModifiers()) && !propriete.isAnnotationPresent(IgnorePourQueryString.class)) {
                    ajouteProprieteAuxParametres(propriete, parameters);
                }
            }
        } catch (Exception e) {
            throw new RedacUnexpectedException("Erreur de conversion inattendue", e);
        }
        return StringUtils.join(parameters, "&");
    }

    private void ajouteProprieteAuxParametres(Field propriete, List<String> parameters) throws IllegalAccessException {
        boolean proprieteAccessible = propriete.isAccessible();
        propriete.setAccessible(true);
        String nomPropriete = propriete.getName();
        Object valeurPropriete = propriete.get(this);
        if (valeurPropriete != null) {
            if (propriete.getType().isArray()) {
                Object[] valeurs = (Object[]) valeurPropriete;
                for (Object valeur : valeurs) {
                    parameters.add(nomPropriete + "=" + valeur);
                }
            } else {
                parameters.add(nomPropriete + "=" + valeurPropriete);
            }
        }
        propriete.setAccessible(proprieteAccessible);
    }
}
