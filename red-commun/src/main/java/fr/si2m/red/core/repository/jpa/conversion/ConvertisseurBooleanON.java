package fr.si2m.red.core.repository.jpa.conversion;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import org.apache.commons.lang3.StringUtils;

/**
 * Convertisseur d'une propriété booléenne en chaîne de caractères O/N.
 * 
 * @author nortaina
 *
 */
@Converter
public class ConvertisseurBooleanON implements AttributeConverter<Boolean, String> {

    @Override
    public String convertToDatabaseColumn(Boolean attribute) {
        return attribute == null || !attribute ? "N" : "O";
    }

    @Override
    public Boolean convertToEntityAttribute(String dbData) {
        return StringUtils.equalsIgnoreCase(dbData, "O");
    }

}
