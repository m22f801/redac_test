package fr.si2m.red.core.repository.jpa.conversion;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.ResourceBundle;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import org.springframework.util.Base64Utils;

import fr.si2m.red.RedacUnexpectedException;

/**
 * Convertisseur d'une propriété sensible à chiffrer en base.<br/>
 * <br/>
 * L'algorithme symétrique AES 256 est utilisé par REDAC pour le chiffrement de données.
 * 
 * @author nortaina
 *
 */
@Converter
public class ConvertisseurDonneeSalaire implements AttributeConverter<Double, String> {
    private static final String ALGORITHME = "AES";
    private static final String TRANSFORMATION = "AES/ECB/PKCS5Padding";

    // Chargement de la configuration à partir du classpath
    private static ResourceBundle chiffrementConfiguration = ResourceBundle.getBundle("chiffrement");
    private static final char[] CLE = chiffrementConfiguration.getString("aes.cle").toCharArray();
    private static final Boolean CONVERSION_ACTIVE = Boolean.valueOf(chiffrementConfiguration.getString("conversion.active"));

    @Override
    public String convertToDatabaseColumn(Double attribut) {
        if (!CONVERSION_ACTIVE) {
            return String.valueOf(attribut);
        }
        if (attribut == null) {
            return null;
        }
        try {
            SecretKey cleChiffrement = genereCleChiffrement();
            Cipher chiffrement = Cipher.getInstance(TRANSFORMATION);
            chiffrement.init(Cipher.ENCRYPT_MODE, cleChiffrement);
            return Base64Utils.encodeToString(chiffrement.doFinal(String.valueOf(attribut).getBytes("windows-1252")));
        } catch (Exception e) {
            throw new RedacUnexpectedException(e);
        }
    }

    @Override
    public Double convertToEntityAttribute(String attributBase) {
        if (!CONVERSION_ACTIVE) {
            return Double.valueOf(attributBase);
        }
        if (attributBase == null) {
            return null;
        }
        String attributDechiffre;
        try {
            SecretKey cleChiffrement = genereCleChiffrement();
            Cipher chiffrement = Cipher.getInstance(TRANSFORMATION);
            chiffrement.init(Cipher.DECRYPT_MODE, cleChiffrement);
            attributDechiffre = new String(chiffrement.doFinal(Base64Utils.decodeFromString(attributBase)), "windows-1252");
        } catch (Exception e) {
            throw new RedacUnexpectedException(e);
        }
        if (attributDechiffre.matches("-?\\d+(\\.\\d+)?")) {
            // La clé de déchiffrement semble correcte : on déchiffre bien un nombre
            return Double.valueOf(attributDechiffre);
        } else {
            return null;
        }
    }

    /**
     * Génère une clé de chiffrement AES non stockée en mémoire.
     * 
     * @return la clé de chiffrement AES générée
     * @throws NoSuchAlgorithmException
     *             si l'algorithme de génération n'existe pas
     * @throws InvalidKeySpecException
     *             si la spécification de clé générée n'est pas valide
     */
    private SecretKey genereCleChiffrement() throws NoSuchAlgorithmException, InvalidKeySpecException {
        MessageDigest md5Digester = MessageDigest.getInstance("MD5");
        for (int i = 0; i < CLE.length; i++) {
            md5Digester.update((byte) CLE[i]);
        }
        byte[] cleTailleAttendue = md5Digester.digest();
        return new SecretKeySpec(cleTailleAttendue, ALGORITHME);
    }
}
