package fr.si2m.red.core.repository.jpa;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.Table;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Selection;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.jpa.criteria.CriteriaBuilderImpl;
import org.hibernate.jpa.criteria.ParameterRegistry;
import org.hibernate.jpa.criteria.Renderable;
import org.hibernate.jpa.criteria.compile.RenderingContext;
import org.hibernate.jpa.criteria.expression.function.BasicFunctionExpression;
import org.hibernate.jpa.criteria.expression.function.FunctionExpression;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import fr.si2m.red.Entite;
import fr.si2m.red.core.repository.CriteresRecherche;
import fr.si2m.red.core.repository.RedacRepository;
import fr.si2m.red.core.repository.jdbc.JdbcRepository;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;

/**
 * Base des référentiels connectés à la base de données REDAC via JPA.<br/>
 * <br/>
 * Si un référentiel étend directement cette base, aucune configuration supplémentaire n'est requise.<br/>
 * En revanche, si au moins deux niveaux d'héritage sont définis, il conviendra de redéfinir la méthode {@link #getPrototypeEntite()} sur la classe finale.
 * 
 * @author nortaina
 *
 * @param <T>
 *            le type de données du référentiel
 */
public abstract class JpaRepository<T extends Entite> extends JdbcRepository implements RedacRepository<T> {

    private static final Logger LOGGER = LoggerFactory.getLogger(JpaRepository.class);

    /**
     * Le gestionnaire d'entités JPA.
     * 
     * @param entityManager
     *            le gestionnaire d'entités JPA à mettre à jour
     * 
     * @return le gestionnaire d'entités JPA
     * 
     */
    @PersistenceContext
    @Getter(AccessLevel.PROTECTED)
    @Setter
    private EntityManager entityManager;

    @Override
    @Transactional(propagation = Propagation.MANDATORY)
    public List<T> getEntiteExistantes(List<? extends T> entites) {
        TypedQuery<T> query = getEntityManager()
                .createQuery("SELECT e FROM " + getClassePrototypeEntite().getSimpleName() + " e WHERE e.id in (:ids)", getClassePrototypeEntite());
        List<Object> ids = new ArrayList<Object>(entites.size());
        for (T entite : entites) {
            if (entite.getId() != null) {
                ids.add(entite.getId());
            }
        }
        query.setParameter("ids", ids);
        List<T> existantesEnBase = query.getResultList();
        List<T> existantesDansLeContexte = new ArrayList<T>(existantesEnBase.size());
        for (T entite : entites) {
            if (existantesEnBase.contains(entite)) {
                existantesDansLeContexte.add(entite);
            }
        }
        return existantesDansLeContexte;
    }

    @Override
    @Transactional(propagation = Propagation.MANDATORY)
    public void modifieEntiteExistante(T entite) {
        LOGGER.debug("Début tentative modification en base de 1 {}...", getClassePrototypeEntite().getSimpleName());
        getEntityManager().merge(entite);
        LOGGER.debug("Modification en base de 1 {} terminée !", getClassePrototypeEntite().getSimpleName());
    }

    @Override
    @Transactional(propagation = Propagation.MANDATORY)
    public void modifieEntitesExistantes(List<? extends T> entites) {
        LOGGER.debug("Début tentative modification en base de {} {}(s)...", entites.size(), getClassePrototypeEntite().getSimpleName());
        for (T entite : entites) {
            getEntityManager().merge(entite);
        }
        reinitialiseContexteJpa();
        LOGGER.debug("Modification en base de {} {}(s) terminée !", entites.size(), getClassePrototypeEntite().getSimpleName());
    }

    @Transactional(propagation = Propagation.MANDATORY)
    @Override
    public T create(T item) {
        LOGGER.debug("Début tentative création en base de 1 {}...", getClassePrototypeEntite().getSimpleName());
        getEntityManager().persist(item);
        LOGGER.debug("Création en base de 1 {} terminée !", getClassePrototypeEntite().getSimpleName());
        return item;
    }

    @Transactional(propagation = Propagation.MANDATORY)
    @Override
    public List<T> creeEnMasse(List<T> items) {
        LOGGER.debug("Début tentative insertion en base de {} {}(s)...", items.size(), getClassePrototypeEntite().getSimpleName());
        for (T item : items) {
            getEntityManager().persist(item);
        }
        reinitialiseContexteJpa();
        LOGGER.debug("Insertion en base de {} {}(s) terminée !", items.size(), getClassePrototypeEntite().getSimpleName());
        return items;
    }

    @Transactional(propagation = Propagation.MANDATORY)
    @Override
    public T get(Object id) {
        return getEntityManager().find(getClassePrototypeEntite(), id);
    }

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    @Override
    public List<T> liste() {
        return getEntityManager().createQuery("from " + getClassePrototypeEntite().getSimpleName(), getClassePrototypeEntite()).getResultList();
    }

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    @Override
    public long compte() {
        return getEntityManager().createQuery("select count(e) from " + getClassePrototypeEntite().getSimpleName() + " e", Long.class)
                .getSingleResult();
    }

    /**
     * Resynchronise et nettoie le cache du contexte JPA local.<br/>
     * <br/>
     * Attention, toutes les données encore dans le contexte et gérées en parallèle seront détachées du contexte après cet appel !
     */
    @Transactional(propagation = Propagation.MANDATORY)
    public void reinitialiseContexteJpa() {
        getEntityManager().flush();
        getEntityManager().clear();
    }

    /**
     * Récupère la classe du prototype de l'entité gérée.
     * 
     * @return la classe du prototype de l'entité gérée
     */
    protected abstract Class<T> getClassePrototypeEntite();

    /**
     * Récupère le nom de la table constituant ce référentiel.
     * 
     * @return le nom de la table constituant ce référentiel
     */
    protected String getNomTable() {
        Table tableEntite = getClassePrototypeEntite().getAnnotation(Table.class);
        if (tableEntite != null) {
            return tableEntite.name();
        } else {
            return null;
        }
    }

    /**
     * Prépare la partie d'une query relative à l'ordonnancement.
     * 
     * @param query
     *            la query à préparer
     * @param criteriaBuilder
     *            le constructeur de critères
     * @param entityRoot
     *            la racine de l'entité demandée
     * @param criteres
     *            les paramètres de configuration de la query à prendre en compte
     */
    protected void prepareTri(CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder, Root<?> entityRoot, CriteresRecherche criteres) {
        if (criteres.getTriChamp() != null) {
            Path<Object> sortColumn = entityRoot.get(criteres.getTriChamp());
            if (criteres.isTriAsc()) {
                query.orderBy(criteriaBuilder.asc(sortColumn));
            } else {
                query.orderBy(criteriaBuilder.desc(sortColumn));
            }
        }
    }

    /**
     * Prépare la partie d'une query relative à la pagination.
     * 
     * @param typedQuery
     *            la query à préparer
     * @param criteres
     *            les paramètres de configuration de la query à prendre en compte
     */
    protected void preparePagination(TypedQuery<?> typedQuery, CriteresRecherche criteres) {
        int pageTaille = criteres.getPageTaille();
        if (pageTaille != -1) {
            if (criteres.getPageNumero() != -1) {
                typedQuery.setFirstResult((criteres.getPageNumero() - 1) * pageTaille);
            }
            typedQuery.setMaxResults(pageTaille);
        }
    }

    /**
     * Construit une expression "LIKE" pour une recherche.
     * 
     * @param criteriaBuilder
     *            le constructeur de critères de recherche
     * @param entity
     *            la racine de l'entité à tester
     * @param entityStringProperty
     *            la propriété sur l'entité à tester
     * @param likeValue
     *            la valeur à tester
     * 
     * @return l'expression construite
     */
    @SuppressWarnings("unchecked")
    protected Expression<Boolean> buildLikeExpression(CriteriaBuilder criteriaBuilder, Path<?> entity, String entityStringProperty,
            String likeValue) {
        StringBuilder valueBuilder = new StringBuilder("%").append(StringUtils.lowerCase(likeValue.trim())).append("%");
        return criteriaBuilder.like(criteriaBuilder.lower((Path<String>) (Path<?>) entity.get(entityStringProperty)), valueBuilder.toString());
    }

    /**
     * Construit une expression "LIKE" pour une recherche.
     * 
     * @param criteriaBuilder
     *            le constructeur de critères de recherche
     * @param entity
     *            la racine de l'entité à tester
     * @param entityStringProperty
     *            la propriété sur l'entité à tester
     * @param likeValue
     *            la valeur à tester
     * 
     * @return l'expression construite
     */
    @SuppressWarnings("unchecked")
    protected Expression<Boolean> buildLikeExpression(CriteriaBuilder criteriaBuilder, Path<?> entity, String entityStringProperty, Long likeValue) {
        StringBuilder valueBuilder = new StringBuilder("%").append(escapeQuotes(StringUtils.lowerCase(likeValue.toString()))).append("%");
        return criteriaBuilder.like(new StrFunction<Long>(criteriaBuilder, (Path<Long>) (Path<?>) entity.get(entityStringProperty)),
                valueBuilder.toString());
    }

    /**
     * Permet d'échapper les quotes afin de sécuriser les requêtes SQL
     * 
     * @param stingValue
     *            la chaine de caractères à échapper
     * 
     * @return la chaine de caractères avec les éventuelles quotes échappées.
     */
    @SuppressWarnings("unchecked")
    protected String escapeQuotes(String stringValue) {
        return stringValue.replace("'", "\\'");
    }

    /**
     * Construit une expression "LIKE" de type "commence par" pour une recherche libre sur champ texte.
     * 
     * @param criteriaBuilder
     *            le constructeur de critères de recherche
     * @param entity
     *            la racine de l'entité à tester
     * @param entityStringProperty
     *            la propriété sur l'entité à tester
     * @param likeValue
     *            la valeur à tester
     * 
     * @return l'expression construite
     */
    @SuppressWarnings("unchecked")
    protected Expression<Boolean> buildStartsWithExpression(CriteriaBuilder criteriaBuilder, Path<?> entity, String entityStringProperty,
            String likeValue) {
        StringBuilder valueBuilder = new StringBuilder(escapeQuotes(StringUtils.lowerCase(likeValue))).append("%");
        return criteriaBuilder.like(criteriaBuilder.lower((Path<String>) (Path<?>) entity.get(entityStringProperty)), valueBuilder.toString());
    }

    /**
     * Construit une expression "LIKE" de type "commence par" pour une recherche libre sur champ numérique.
     * 
     * @param criteriaBuilder
     *            le constructeur de critères de recherche
     * @param entity
     *            la racine de l'entité à tester
     * @param entityStringProperty
     *            la propriété sur l'entité à tester
     * @param likeValue
     *            la valeur à tester
     * 
     * @return l'expression construite
     */
    @SuppressWarnings("unchecked")
    protected Expression<Boolean> buildStartsWithExpression(CriteriaBuilder criteriaBuilder, Path<?> entity, String entityStringProperty,
            Long likeValue) {
        StringBuilder valueBuilder = new StringBuilder(escapeQuotes(StringUtils.lowerCase(likeValue.toString()))).append("%");
        return criteriaBuilder.like(new StrFunction<Long>(criteriaBuilder, (Path<Long>) (Path<?>) entity.get(entityStringProperty)),
                valueBuilder.toString());
    }

    /**
     * Récupère le premier résultat d'une liste s'il existe.
     * 
     * @param resultats
     *            la liste des résultats
     * @param <X>
     *            le type de résultat
     * 
     * @return le premier résultat s'il existe, null sinon
     * 
     */
    protected <X> X getPremierResultatSiExiste(List<X> resultats) {
        if (resultats.isEmpty()) {
            return null;
        }
        return resultats.get(0);
    }

    /**
     * Récupère le résultat d'une somme et 0 si la requête ne renvoie pas de résultats.
     * 
     * @param query
     *            la requête de somme
     * @return le résultat de la somme
     */
    protected double getResultatSomme(TypedQuery<Double> query) {
        Double somme = getPremierResultatSiExiste(query.getResultList());
        if (somme == null) {
            return 0d;
        } else {
            return somme.doubleValue();
        }
    }

    /**
     * Applique un paramétrage de pagination à une requête JPA.
     * 
     * @param query
     *            la requête
     * @param indexPremierResultat
     *            l'index du premier résultat (commençant à 0), 0 si null
     * @param taillePage
     *            la taille maximale d'une page, tous les résultats à partir du premier si null
     */
    protected void appliqueParametragePagination(Query query, Integer indexPremierResultat, Integer taillePage) {
        if (indexPremierResultat != null) {
            query.setFirstResult(indexPremierResultat);
        }
        if (taillePage != null) {
            query.setMaxResults(taillePage);
        }
    }

    /**
     * Fonction pour transformer des numériques en chaînes de caractères notamment pour les critères de recherche LIKE.<br/>
     * <br/>
     * Code repris de org.hibernate.ejb.criteria.expression.function.CastFunction.
     * 
     * @author nortaina
     *
     * @param <Y>
     *            le type numérique à caster
     */
    private static final class StrFunction<Y extends Number> extends BasicFunctionExpression<String> implements FunctionExpression<String> {
        /**
         * UID de version.
         */
        private static final long serialVersionUID = -6501922465791721921L;

        public static final String FCT_NAME = "str";

        private final Serializable selection;

        /**
         * Constructeur.
         * 
         * @param criteriaBuilder
         *            le constructeur de critères
         * @param selection
         *            la sélection de propriété sur laquelle appliquer la fonction
         */
        public StrFunction(CriteriaBuilder criteriaBuilder, Selection<Y> selection) {
            super((CriteriaBuilderImpl) criteriaBuilder, String.class, FCT_NAME);
            if (!(selection instanceof Serializable)) {
                throw new IllegalArgumentException("Selection must be serializable");
            }
            this.selection = (Serializable) selection;
        }

        @Override
        @SuppressWarnings("unchecked")
        public void registerParameters(ParameterRegistry registry) {
            Helper.possibleParameter((Selection<Y>) selection, registry);
        }

        @Override
        public String render(RenderingContext renderingContext) {
            return FCT_NAME + '(' + ((Renderable) selection).render(renderingContext) + ')';
        }
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void nettoieToutesEntites() {
        String scriptSuppression = new StringBuilder("delete from ").append(getNomTable()).toString();
        Query querySuppression = getEntityManager().createNativeQuery(scriptSuppression);
        querySuppression.executeUpdate();
    }
}
