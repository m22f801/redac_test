package fr.si2m.red.core.repository;

import java.util.List;

import fr.si2m.red.EntiteImportableBatch;

/**
 * Fondation pour les référentiels de l'application REDAC pour les entités importables.
 * 
 * Tout référentiel d'entité importable possède un "espace temporaire" pour les données non validées en cours d'import via batch en plus de l'espace normal des
 * données actives.
 * 
 * @author nortaina
 *
 * @param <T>
 *            le type de données stockées dans le référentiel
 */
public interface EntiteImportableRepository<T extends EntiteImportableBatch> extends RedacRepository<T> {

    /**
     * Crée un ensemble d'éléments dans le référentiel en tant que données temporaires en attente de promotion.
     * 
     * @param items
     *            les éléments à créer dans l'espace temporaire du référentiel
     */
    void importeEnMasseEntitesTemporaires(List<? extends T> items);

    /**
     * Promeut les entités temporaires au statut de nouveau référentiel valide. En pratique : crée des entités actives par copie de l'espace temporaire.
     * 
     * @return le nombre d'entités promues
     */
    int promeutEntitesTemporaires();

    /**
     * Nettoie le référentiel de ses éléments temporaires.
     * 
     * @return le nombre d'éléments temporaires supprimés
     */
    int nettoieEntitesTemporaires();

    /**
     * Nettoie le référentiel de ses éléments actifs.
     * 
     * @return le nombre d'éléments actifs supprimés
     */
    int nettoieEntitesActives();

    /**
     * Supprime un ensemble d'éléments dans le référentiel parmi une liste donnée.
     * 
     * @param entites
     *            les entités à supprimer
     */
    void supprimeEntitesEnMasse(List<? extends T> entites);

    /**
     * Supprime un élément donné dans le référentiel.
     * 
     * @param entite
     *            l'entité à supprimer
     */
    void supprimeEntiteExistante(T entite);

}
