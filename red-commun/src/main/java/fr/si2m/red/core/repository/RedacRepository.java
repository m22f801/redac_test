package fr.si2m.red.core.repository;

import java.util.List;

import fr.si2m.red.Entite;

/**
 * Fondation pour les référentiels de l'application REDAC.
 * 
 * @author nortaina
 *
 * @param <T>
 *            le type de données stockées dans le référentiel
 */
public interface RedacRepository<T extends Entite> {
    /**
     * Permet de rechercher une entité existante dans le référentiel.
     * 
     * @param primaryKey
     *            la clé primaire de l'entité recherchée
     * 
     * @return l'entité si trouvée, null sinon
     */
    T get(Object primaryKey);

    /**
     * Modifie une entité existante dans le référentiel.
     * 
     * @param entite
     *            l'entité à modifier
     */
    void modifieEntiteExistante(T entite);

    /**
     * Récupère les entités existantes dans le référentiel parmi une liste donnée.
     * 
     * @param entites
     *            les entités à tester
     * @return les entités existantes déjà dans le référentiel parmi celles-ci
     */
    List<T> getEntiteExistantes(List<? extends T> entites);

    /**
     * Met à jour une liste d'entités existantes dans le référentiel.
     * 
     * @param entites
     *            les entités à mettre à jour
     */
    void modifieEntitesExistantes(List<? extends T> entites);

    /**
     * Crée une entité dans le référentiel.
     * 
     * @param item
     *            l'entité à créer
     * 
     * @return l'entité créée
     */
    T create(T item);

    /**
     * Crée une liste d'entités dans le référentiel.
     * 
     * @param items
     *            la liste d'entités à créer
     * @return les entités créées
     */
    List<T> creeEnMasse(List<T> items);

    /**
     * Liste la totalité des éléments présents dans le référentiel (à utiliser avec intelligence sur des petits volumes uniquement).
     * 
     * @return la totalité des éléments présents dans le référentiel
     */
    List<T> liste();

    /**
     * Compte la totalité des éléments présents dans le référentiel.
     * 
     * @return le nombre total d'éléments présents dans le référentiel
     */
    long compte();

    /**
     * Nettoie la totalité des éléments présents dans le référentiel.
     * 
     */
    void nettoieToutesEntites();
}
