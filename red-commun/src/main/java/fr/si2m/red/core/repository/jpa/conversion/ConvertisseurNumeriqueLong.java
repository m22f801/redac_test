package fr.si2m.red.core.repository.jpa.conversion;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Convertisseur d'une propriété booléenne en chaîne de caractères O/N.
 * 
 * @author nortaina
 *
 */
@Converter
public class ConvertisseurNumeriqueLong implements AttributeConverter<String, Long> {

    private static final Logger LOGGER = LoggerFactory.getLogger(ConvertisseurNumeriqueLong.class);

    @Override
    public Long convertToDatabaseColumn(String attribute) {
        Long result = null;
        if (StringUtils.isNotBlank(attribute)) {
            try {
                result = Long.valueOf(attribute.trim());
            } catch (Exception e) {
                LOGGER.warn("La conversion de la valeur '" + attribute.trim() + "' en Long a échoué : ", e);
            }
        }
        return result;
    }

    @Override
    public String convertToEntityAttribute(Long dbData) {
        String result = "";
        if (dbData != null) {
            result = dbData.toString();
        }
        return result;
    }

}
