package fr.si2m.red.core.repository.jpa.conversion;

import javax.persistence.AttributeConverter;

import org.apache.commons.lang3.StringUtils;

/**
 * Convertisseur d'une propriété booléenne en chaîne de caractères OUI/NON.
 * 
 * @author nortaina
 *
 */
public class ConvertisseurBooleanOuiNon implements AttributeConverter<Boolean, String> {

    @Override
    public String convertToDatabaseColumn(Boolean attribute) {
        return attribute == null || !attribute ? "NON" : "OUI";
    }

    @Override
    public Boolean convertToEntityAttribute(String dbData) {
        return StringUtils.equalsIgnoreCase(dbData, "OUI");
    }

}
