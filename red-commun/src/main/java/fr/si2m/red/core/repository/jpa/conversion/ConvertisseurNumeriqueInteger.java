package fr.si2m.red.core.repository.jpa.conversion;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Convertisseur d'une propriété booléenne en chaîne de caractères O/N.
 * 
 * @author nortaina
 *
 */
@Converter
public class ConvertisseurNumeriqueInteger implements AttributeConverter<String, Integer> {

    private static final Logger LOGGER = LoggerFactory.getLogger(ConvertisseurNumeriqueInteger.class);

    @Override
    public Integer convertToDatabaseColumn(String attribute) {
        Integer result = null;
        if (StringUtils.isNotBlank(attribute)) {
            try {
                result = Integer.valueOf(attribute.trim());
            } catch (Exception e) {
                LOGGER.warn("La conversion de la valeur '" + attribute.trim() + "' en Integer a échoué : ", e);
            }
        }
        return result;
    }

    @Override
    public String convertToEntityAttribute(Integer dbData) {
        String result = "";
        if (dbData != null) {
            result = dbData.toString();
        }
        return result;
    }

}
