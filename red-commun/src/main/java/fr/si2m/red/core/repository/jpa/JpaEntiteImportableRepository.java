package fr.si2m.red.core.repository.jpa;

import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Query;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.reflect.FieldUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import fr.si2m.red.EntiteImportableBatch;
import fr.si2m.red.RedacUnexpectedException;
import fr.si2m.red.core.repository.EntiteImportableRepository;

/**
 * Base des référentiels d'entités importables via batch, connectés à la base de données REDAC via JPA.<br/>
 * <br/>
 * Si un référentiel étend directement cette base, aucune configuration supplémentaire n'est requise.<br/>
 * En revanche, si au moins deux niveaux d'héritage sont définis, il conviendra de redéfinir la méthode {@link #getPrototypeEntite()} sur la classe finale.
 * 
 * @author nortaina
 * 
 * @param <T>
 *            le type des entités gérées par le référentiel
 *
 */
public abstract class JpaEntiteImportableRepository<T extends EntiteImportableBatch> extends JpaRepository<T>
        implements EntiteImportableRepository<T> {
    private static final Logger LOGGER = LoggerFactory.getLogger(JpaEntiteImportableRepository.class);

    /**
     * Mise en cache du prototype d'entité gérée.
     */
    private T prototypeEntite;

    @Override
    @Transactional(propagation = Propagation.MANDATORY)
    public void modifieEntiteExistante(T entite) {
        LOGGER.debug("Début tentative modification en base de 1 {}...", getClassePrototypeEntite().getSimpleName());
        getEntityManager().merge(entite);
        LOGGER.debug("Modification en base de 1 {} terminée !", getClassePrototypeEntite().getSimpleName());
    }

    @Override
    @Transactional(propagation = Propagation.MANDATORY)
    public void modifieEntitesExistantes(List<? extends T> items) {
        LOGGER.debug("Début tentative modification en base de {} {}(s)...", items.size(), getClassePrototypeEntite().getSimpleName());
        for (T item : items) {
            getEntityManager().merge(item);
        }
        reinitialiseContexteJpa();
        LOGGER.debug("Modification en base de {} {}(s) terminée !", items.size(), getClassePrototypeEntite().getSimpleName());
    }

    @Override
    @Transactional(propagation = Propagation.MANDATORY)
    public int promeutEntitesTemporaires() {
        Map<String, String> champsEntite = getMappingProprieteEntiteChampTable();

        // On ne veut pas copier les colonnes d'aspects techniques
        champsEntite.remove("idTechnique");
        champsEntite.remove("ligneEnCoursImportBatch");
        champsEntite.remove("auditDateCreation");
        champsEntite.remove("auditDateDerniereModification");
        champsEntite.remove("auditUtilisateurDerniereModification");

        List<String> listeColonnes = new ArrayList<String>(champsEntite.values());
        String colonnes = StringUtils.join(listeColonnes.toArray(), ",");

        // INSERT SELECT pour copie de l'entité vers l'espace actif
        String scriptPromotion = new StringBuilder("insert into ").append(getNomTable()).append("(TMP_BATCH,").append(colonnes).append(") ")
                .append("select FALSE,").append(colonnes).append(" from ").append(getNomTable()).append(" where TMP_BATCH IS TRUE").toString();

        Query queryPromotion = getEntityManager().createNativeQuery(scriptPromotion);
        return queryPromotion.executeUpdate();
    }

    @Override
    @Transactional(propagation = Propagation.MANDATORY)
    public int nettoieEntitesTemporaires() {
        String scriptSuppression = new StringBuilder("delete from ").append(getNomTable()).append(" where TMP_BATCH IS TRUE").toString();
        Query querySuppression = getEntityManager().createNativeQuery(scriptSuppression);
        return querySuppression.executeUpdate();
    }

    @Override
    @Transactional(propagation = Propagation.MANDATORY)
    public int nettoieEntitesActives() {
        String scriptSuppression = new StringBuilder("delete from ").append(getNomTable()).append(" where TMP_BATCH IS FALSE").toString();
        Query querySuppression = getEntityManager().createNativeQuery(scriptSuppression);
        return querySuppression.executeUpdate();
    }

    @Override
    @Transactional(propagation = Propagation.MANDATORY)
    public void importeEnMasseEntitesTemporaires(List<? extends T> items) {
        LOGGER.debug("Début tentative insertion en base de {} {}(s)...", items.size(), getClassePrototypeEntite().getSimpleName());
        for (T item : items) {
            if (item.isImportValide()) {
                // On ne prend en compte que des entités valides
                // Insertion dans le domaine temporaire par défaut
                item.setLigneEnCoursImportBatch(true);
                getEntityManager().persist(item);
            }
        }
        reinitialiseContexteJpa();
        LOGGER.debug("Insertion en base de {} {}(s) terminée !", items.size(), getClassePrototypeEntite().getSimpleName());
    }

    /**
     * Récupère la classe du prototype de l'entité gérée.
     * 
     * @return la classe du prototype de l'entité gérée
     */
    @Override
    @SuppressWarnings("unchecked")
    protected Class<T> getClassePrototypeEntite() {
        return (Class<T>) getPrototypeEntite().getClass();
    }

    /**
     * Récupère le prototype de l'entité gérée.
     * 
     * @SuppressWarnings("unchecked")
     * 
     * @return le prototype de l'entité gérée
     */
    private T getPrototypeEntite() {
        if (this.prototypeEntite == null) {
            ParameterizedType typeReferentiel = (ParameterizedType) getClass().getGenericSuperclass();
            @SuppressWarnings("unchecked")
            Class<T> typePrototype = (Class<T>) typeReferentiel.getActualTypeArguments()[0];
            try {
                this.prototypeEntite = typePrototype.newInstance();
            } catch (InstantiationException | IllegalAccessException e) {
                throw new RedacUnexpectedException(e);
            }
        }
        return this.prototypeEntite;
    }

    /**
     * Récupère le mapping propriété entité / champ table de l'entité ciblée.
     * 
     * @return le mapping propriété entité / champ table de l'entité ciblée
     */
    private Map<String, String> getMappingProprieteEntiteChampTable() {
        Class<T> classeEntite = getClassePrototypeEntite();
        Field[] proprietesClasse = FieldUtils.getAllFields(classeEntite);
        Map<String, String> mapping = new HashMap<String, String>();
        for (Field propriete : proprietesClasse) {
            Column column = propriete.getAnnotation(Column.class);
            if (column != null) {
                mapping.put(propriete.getName(), column.name());
            }
        }
        return mapping;
    }

    @Override
    @Transactional(propagation = Propagation.MANDATORY)
    public void supprimeEntitesEnMasse(List<? extends T> items) {
        LOGGER.debug("Début tentative suppression en base de {} {}(s)...", items.size(), getClassePrototypeEntite().getSimpleName());
        for (T item : items) {
            getEntityManager().remove(getEntityManager().merge(item));
        }
        reinitialiseContexteJpa();
        LOGGER.debug("Suppression en base de {} {}(s) terminée !", items.size(), getClassePrototypeEntite().getSimpleName());
    }

    @Override
    @Transactional(propagation = Propagation.MANDATORY)
    public void supprimeEntiteExistante(T entite) {
        LOGGER.debug("Début tentative suppression en base de 1 {}...", getClassePrototypeEntite().getSimpleName());
        getEntityManager().remove(getEntityManager().merge(entite));
        reinitialiseContexteJpa();
        LOGGER.debug("Suppression en base de 1 {} terminée !", getClassePrototypeEntite().getSimpleName());
    }

}
