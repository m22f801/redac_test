-- INITIALISATION DES SCHEMAS DE REFERENCE

INSERT INTO PARAM_VALEUR_DEFAUT (
	CODE_ORGANISME,
	EXPLOIT,
	EDIT_CONS_PAIMT,
	TRANSFERT,
	VAR_ETAB_NB,
	VAR_ETAB_PC,
	VAR_SAL_NB,
	VAR_SAL_PC,
	VIP,
	MODE_NAT_CONT,
	MODE_REAFF_CAT_EFFECTIF,
	SEUIL_DIFF_COT_MT,
	SEUIL_DIFF_COT_PC,
	USER_CREATION) 
VALUES (
	'A4M001',
	'OUI',
	'OUI',
	'OUI',
	5,
	10,
	5,
	10,
	'N',
	'C',
	'REACA',
	10,
	5,
	'REDAC_INIT'
);

INSERT INTO PARAM_CONTROLE_SIGNAL_REJET(ID_CONTROLE,NOFAM,NIVEAU_ALERTE,MSG_ALERTE,USER_CREATION)
VALUES
('RED_RESILIE', 'ALL', 'SIGNAL', 'Contrat résilié au $1','RR000'),
('RED_COTIS_ETAB', 'ALL', 'SIGNAL', 'Contrat avec cotisation établissement','RR000'),
('RED_VAR_NB_ETB', 'ALL', 'REJET', 'Variation significative du nombre d''établissements','RR000'),
('RED_VAR_NB_SAL', 'ALL', 'REJET', 'Variation significative du nombre de salariés','RR000'),
('RED_MODE_PAIEMT', 'ALL', 'REJET', 'Mode de paiement particulier','RR000'),
('RED_PERIOD_VRST', 'ALL', 'REJET', 'Période de versement non conforme à la périodicité du contrat','RR000'),
('RED_MOIS_ABSENT', 'ALL', 'REJET', 'Mois absent dans la période','RR000'),
('RED_TYPE_BASE_ASSUJ', 'ALL', 'SIGNAL', 'Type composant base assujettie déclaré non conforme : $1','RR000'),
('RED_CHGT_DATE_NAIS', 'ALL', 'SIGNAL', 'Changement de date de naissance d''un assuré','RR000'),
('RED_CHGT_NIR', 'ALL', 'SIGNAL', 'Changement de NIR d''un assuré','RR000'),
('GEST_REJET', 'ALL', 'REJET', 'REJET INVALIDE','RR000'),
('GEST_RETOUR_ETP', 'ALL', 'REJET', 'Mise en attente','RR000'),
('RED_FUSION','ALL','SIGNAL','Période en cours de fusion','RR000'),
('RED_ASSIETTES_VIDES','ALL','SIGNAL','Assiette vide - Somme montant cotisations KO : $1','RR000'),
('RED_CHGT_TARIF','ALL','SIGNAL','Des changements de tarification : $1','RR000'),
('RED_DIFF_COTIS','ALL','SIGNAL','Estimé : $1, Declaré : $2, seuil (en %) : $3, seuil ( en €) : $4','RR000');