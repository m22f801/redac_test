package fr.si2m.red.internal.parametrage;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTransactionalTestNGSpringContextTests;
import org.testng.Assert;
import org.testng.annotations.Test;

import fr.si2m.red.parametrage.ParamUtilisateurGestionnaire;
import fr.si2m.red.parametrage.ParamUtilisateurGestionnaireId;

@Test
@DirtiesContext
@ActiveProfiles("test")
@ContextConfiguration(locations = { "classpath:fr/si2m/red/config.xml" })
public class JpaParamUtilisateurGestionnaireRepositoryTests extends AbstractTransactionalTestNGSpringContextTests {

    @Autowired
    private JpaParamUtilisateurGestionnaireRepository paramUtilisateurGestionnaireRepository;

    @Test
    public void testInsertAndGet() {
        List<ParamUtilisateurGestionnaire> initialement = paramUtilisateurGestionnaireRepository.liste();
        Assert.assertEquals(0, initialement.size(), "Il ne devrait y avoir aucune gestion en base à l'origine");

        ParamUtilisateurGestionnaire utilisateurGestionnaire = new ParamUtilisateurGestionnaire();
        utilisateurGestionnaire.setAuditUtilisateurCreation("TESTEUR");
        utilisateurGestionnaire.setIdentifiantActiveDirectory("testeurt");
        utilisateurGestionnaire.setDroitIndividuAsText("O");

        paramUtilisateurGestionnaireRepository.importeEnMasseEntitesTemporaires(Arrays.asList(utilisateurGestionnaire));
        paramUtilisateurGestionnaireRepository.promeutEntitesTemporaires();

        ParamUtilisateurGestionnaireId id = new ParamUtilisateurGestionnaireId();
        id.setIdentifiantActiveDirectory(utilisateurGestionnaire.getIdentifiantActiveDirectory());

        ParamUtilisateurGestionnaire cree = paramUtilisateurGestionnaireRepository.get(id);

        Assert.assertNotNull(cree);
        Assert.assertEquals(cree.getIdentifiantActiveDirectory(), utilisateurGestionnaire.getIdentifiantActiveDirectory());
        Assert.assertEquals(cree.isDroitIndividu(), utilisateurGestionnaire.isDroitIndividu());
    }
}
