package fr.si2m.red.internal.dsn;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTransactionalTestNGSpringContextTests;
import org.testng.Assert;
import org.testng.annotations.Test;

import fr.si2m.red.dsn.Versement;
import fr.si2m.red.dsn.VersementId;

@Test
@ActiveProfiles("test")
@ContextConfiguration(locations = { "classpath:fr/si2m/red/config.xml" })
public class JpaVersementRepositoryTests extends AbstractTransactionalTestNGSpringContextTests {

    @Autowired
    private JpaVersementRepository versementRepository;

    @Test
    public void testInsertAndGet() {
        List<Versement> initiaux = versementRepository.liste();
        Assert.assertEquals(0, initiaux.size(), "Il ne devrait y avoir aucun versement en base à l'origine");

        Versement entite = new Versement();
        entite.setIdVersement("cleVersement");
        entite.setAuditUtilisateurCreation("TESTEUR");

        versementRepository.importeEnMasseEntitesTemporaires(Arrays.asList(entite));

        VersementId id = new VersementId();
        id.setIdVersement(entite.getIdVersement());

        Versement cree = versementRepository.get(id);
        Assert.assertNull(cree, "Le versement devrait être créé dans l'espace temporaire pour le moment");

        versementRepository.promeutEntitesTemporaires();

        cree = versementRepository.get(id);
        Assert.assertNotNull(cree, "Le versement devrait être promu");

        Assert.assertNotNull(cree);
        Assert.assertEquals(cree.getIdVersement(), entite.getIdVersement());
        Assert.assertFalse(cree.isLigneEnCoursImportBatch(), "Le versement ne devrait plus être dans l'espace temporaire");

    }

}
