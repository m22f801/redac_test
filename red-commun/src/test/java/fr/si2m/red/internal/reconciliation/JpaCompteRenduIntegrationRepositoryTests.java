package fr.si2m.red.internal.reconciliation;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTransactionalTestNGSpringContextTests;
import org.testng.Assert;
import org.testng.annotations.Test;

import fr.si2m.red.reconciliation.CompteRenduIntegration;

@Test
@ActiveProfiles("test")
@ContextConfiguration(locations = { "classpath:fr/si2m/red/config.xml" })
public class JpaCompteRenduIntegrationRepositoryTests extends AbstractTransactionalTestNGSpringContextTests {

    @Autowired
    private JpaCompteRenduIntegrationRepository compteRenduIntegrationRepository;

    @Test
    public void testInsertAndGet() {
        List<CompteRenduIntegration> initiaux = compteRenduIntegrationRepository.liste();
        Assert.assertEquals(0, initiaux.size(), "Il ne devrait y avoir aucun compteRenduIntegration en base à l'origine");

        CompteRenduIntegration entite = new CompteRenduIntegration();
        entite.setIdPeriode(123L);
        entite.setDateTraitement(20151102);
        entite.setAuditUtilisateurCreation("TEST");

        compteRenduIntegrationRepository.create(entite);

        CompteRenduIntegration cree = compteRenduIntegrationRepository.get(1L);
        Assert.assertNotNull(cree);

        Assert.assertEquals(cree.getIdPeriode(), entite.getIdPeriode());
        Assert.assertEquals(cree.getDateTraitement(), entite.getDateTraitement());

    }

}
