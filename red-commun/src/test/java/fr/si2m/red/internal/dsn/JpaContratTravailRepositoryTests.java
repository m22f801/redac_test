package fr.si2m.red.internal.dsn;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTransactionalTestNGSpringContextTests;
import org.testng.Assert;
import org.testng.annotations.Test;

import fr.si2m.red.dsn.ContratTravail;
import fr.si2m.red.dsn.ContratTravailId;

@Test
@ActiveProfiles("test")
@ContextConfiguration(locations = { "classpath:fr/si2m/red/config.xml" })
public class JpaContratTravailRepositoryTests extends AbstractTransactionalTestNGSpringContextTests {

    @Autowired
    private JpaContratTravailRepository contratTravailRepository;

    @Test
    public void testInsertAndGet() {
        List<ContratTravail> initiaux = contratTravailRepository.liste();
        Assert.assertEquals(0, initiaux.size(), "Il ne devrait y avoir aucun contratTravail en base à l'origine");

        ContratTravail entite = new ContratTravail();
        entite.setIdContratTravail("cleContratTravail");
        entite.setAuditUtilisateurCreation("TESTEUR");

        contratTravailRepository.importeEnMasseEntitesTemporaires(Arrays.asList(entite));

        ContratTravailId id = new ContratTravailId();
        id.setIdContratTravail(entite.getIdContratTravail());

        ContratTravail cree = contratTravailRepository.get(id);
        Assert.assertNull(cree, "Le contratTravail devrait être créé dans l'espace temporaire pour le moment");

        contratTravailRepository.promeutEntitesTemporaires();

        cree = contratTravailRepository.get(id);
        Assert.assertNotNull(cree, "Le contratTravail devrait être promu");

        Assert.assertNotNull(cree);
        Assert.assertEquals(cree.getIdContratTravail(), entite.getIdContratTravail());
        Assert.assertFalse(cree.isLigneEnCoursImportBatch(), "Le contratTravail ne devrait plus être dans l'espace temporaire");

    }

}
