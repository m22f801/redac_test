package fr.si2m.red.internal.parametrage;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTransactionalTestNGSpringContextTests;
import org.testng.Assert;
import org.testng.annotations.Test;

import fr.si2m.red.parametrage.ParamCodeLibelle;
import fr.si2m.red.parametrage.ParamCodeLibelleId;

@Test
@DirtiesContext
@ActiveProfiles("test")
@ContextConfiguration(locations = { "classpath:fr/si2m/red/config.xml" })
public class JpaParamCodeLibelleRepositoryTests extends AbstractTransactionalTestNGSpringContextTests {

    @Autowired
    private JpaParamCodeLibelleRepository paramCodeLibelleRepository;

    @Test
    public void testInsertAndGetUnLibelle() {
        List<ParamCodeLibelle> libellesInitiaux = paramCodeLibelleRepository.liste();
        Assert.assertEquals(0, libellesInitiaux.size(), "Il ne devrait y avoir aucun libellé en base à l'origine");

        ParamCodeLibelle libelle1 = new ParamCodeLibelle();
        libelle1.setAuditUtilisateurCreation("TESTEUR");
        libelle1.setTable("CONTRAT");
        libelle1.setChamp("COETACO");
        libelle1.setCode("1");
        libelle1.setOrdre(Long.valueOf("0"));
        libelle1.setLibelleCourt("Un libellé court");
        libelle1.setLibelleLong("Un libellé super super super super long");
        libelle1.setLibelleEdition("Un libellé qui a la classe");

        paramCodeLibelleRepository.importeEnMasseEntitesTemporaires(Arrays.asList(libelle1));
        paramCodeLibelleRepository.promeutEntitesTemporaires();

        ParamCodeLibelleId libelleId1 = new ParamCodeLibelleId();
        libelleId1.setTable(libelle1.getTable());
        libelleId1.setChamp(libelle1.getChamp());
        libelleId1.setCode(libelle1.getCode());
        ParamCodeLibelle libelleCree = paramCodeLibelleRepository.get(libelleId1);

        Assert.assertNotNull(libelleCree, "Le libellé n'a pas été correctement inséré ou n'est pas requêtable");
        Assert.assertEquals(libelle1.getOrdre(), libelleCree.getOrdre(), "L'ordre du libellé inséré n'est pas le bon");
        Assert.assertEquals(libelle1.getLibelleCourt(), libelleCree.getLibelleCourt(), "Le libellé court inséré n'est pas le bon");
        Assert.assertEquals(libelle1.getLibelleLong(), libelleCree.getLibelleLong(), "Le libellé long inséré n'est pas le bon");
        Assert.assertEquals(libelle1.getLibelleEdition(), libelleCree.getLibelleEdition(), "Le libellé d'édition inséré n'est pas le bon");
    }

    @Test
    public void testInsertAndGetMultipleLibelles() {
        List<ParamCodeLibelle> libellesInitiaux = paramCodeLibelleRepository.liste();
        Assert.assertEquals(0, libellesInitiaux.size(), "Il ne devrait y avoir aucun libellé en base à l'origine");

        ParamCodeLibelle libelle1 = new ParamCodeLibelle();
        libelle1.setAuditUtilisateurCreation("TESTEUR");
        libelle1.setTable("CONTRAT");
        libelle1.setChamp("COETACO");
        libelle1.setCode("1");
        libelle1.setOrdre(Long.valueOf("0"));
        libelle1.setLibelleCourt("Un libellé court");
        libelle1.setLibelleLong("Un libellé super super super super long");
        libelle1.setLibelleEdition("Un libellé qui a la classe");

        ParamCodeLibelle libelle2 = new ParamCodeLibelle();
        libelle2.setAuditUtilisateurCreation("TESTEUR");
        libelle2.setTable("CONTRAT");
        libelle2.setChamp("COETACO");
        libelle2.setCode("2");
        libelle2.setLibelleCourt("Un libellé court 2");
        libelle2.setLibelleLong("Un libellé super super super super long 2");
        libelle2.setLibelleEdition("Un libellé qui a la classe 2");

        paramCodeLibelleRepository.importeEnMasseEntitesTemporaires(Arrays.asList(libelle1, libelle2));
        paramCodeLibelleRepository.promeutEntitesTemporaires();
        paramCodeLibelleRepository.nettoieEntitesTemporaires();

        List<ParamCodeLibelle> libellesCrees = paramCodeLibelleRepository.liste();

        Assert.assertEquals(2, libellesCrees.size(), "Deux libellés devraient avoir été créés en base");

        ParamCodeLibelle libelleCree1 = libellesCrees.get(0);
        Assert.assertEquals(libelle1.getOrdre(), libelleCree1.getOrdre(), "L'ordre du libellé 1 inséré n'est pas le bon");
        Assert.assertEquals(libelle1.getLibelleCourt(), libelleCree1.getLibelleCourt(), "Le libellé 1 court inséré n'est pas le bon");
        Assert.assertEquals(libelle1.getLibelleLong(), libelleCree1.getLibelleLong(), "Le libellé 1 long inséré n'est pas le bon");
        Assert.assertEquals(libelle1.getLibelleEdition(), libelleCree1.getLibelleEdition(), "Le libellé 1 d'édition inséré n'est pas le bon");

        ParamCodeLibelle libelleCree2 = libellesCrees.get(1);
        Assert.assertEquals(libelle2.getOrdre(), libelleCree2.getOrdre(), "L'ordre du libellé 2 inséré n'est pas le bon");
        Assert.assertEquals(libelle2.getLibelleCourt(), libelleCree2.getLibelleCourt(), "Le libellé 2 court inséré n'est pas le bon");
        Assert.assertEquals(libelle2.getLibelleLong(), libelleCree2.getLibelleLong(), "Le libellé 2 long inséré n'est pas le bon");
        Assert.assertEquals(libelle2.getLibelleEdition(), libelleCree2.getLibelleEdition(), "Le libellé 2 d'édition inséré n'est pas le bon");

    }
}
