package fr.si2m.red.internal.reconciliation;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTransactionalTestNGSpringContextTests;
import org.testng.Assert;
import org.testng.annotations.Test;

import fr.si2m.red.reconciliation.PeriodeRecue;
import fr.si2m.red.reconciliation.RattachementDeclarationsRecues;

@Test
@ActiveProfiles("test")
@ContextConfiguration(locations = { "classpath:fr/si2m/red/config.xml" })
public class JpaRattachementDeclarationsRecuesRepositoryTests extends AbstractTransactionalTestNGSpringContextTests {

    @Autowired
    private JpaRattachementDeclarationsRecuesRepository rattachementDeclarationsRecuesRepository;

    @Test
    public void testInsertAndGet() {
        List<RattachementDeclarationsRecues> initiaux = rattachementDeclarationsRecuesRepository.liste();
        Assert.assertEquals(0, initiaux.size(), "Il ne devrait y avoir aucun rattachementDeclarationsRecues en base à l'origine");

        RattachementDeclarationsRecues entite = new RattachementDeclarationsRecues();
        entite.setIdAdhEtabMois("cleRattachement");
        entite.setIdPeriode(123L);
        entite.setAuditUtilisateurCreation("TEST");

        rattachementDeclarationsRecuesRepository.create(entite);

        RattachementDeclarationsRecues cree = rattachementDeclarationsRecuesRepository.get(1L);
        Assert.assertNotNull(cree);

        Assert.assertEquals(cree.getIdAdhEtabMois(), entite.getIdAdhEtabMois());
        Assert.assertEquals(cree.getIdPeriode(), entite.getIdPeriode());

    }

    @Test
    public void testCompteNbSalariesRattaches() {
        PeriodeRecue periode = new PeriodeRecue();
        periode.setIdPeriode(1L);
        periode.setDateDebutPeriode(20150501);
        int nbSalariesRattaches = rattachementDeclarationsRecuesRepository.compteNbSalariesRattachesEnDebutPeriode(periode.getIdPeriode(), periode.getDateDebutPeriode());
        Assert.assertEquals(nbSalariesRattaches, 0);
    }

}
