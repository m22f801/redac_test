package fr.si2m.red.internal.parametrage;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTransactionalTestNGSpringContextTests;
import org.testng.Assert;
import org.testng.annotations.Test;

import fr.si2m.red.parametrage.ParamControleSignalRejet;
import fr.si2m.red.parametrage.ParamControleSignalRejetId;

@Test
@DirtiesContext
@ActiveProfiles("test")
@ContextConfiguration(locations = { "classpath:fr/si2m/red/config.xml" })
public class JpaParamControleSignalRejetRepositoryTests extends AbstractTransactionalTestNGSpringContextTests {

    @Autowired
    private JpaParamControleSignalRejetRepository paramControleSignalRejetRepository;

    @Test
    public void testInsertAndGet() {
        List<ParamControleSignalRejet> initialement = paramControleSignalRejetRepository.liste();
        Assert.assertEquals(16, initialement.size(), "Il devrait y avoir 16 libellés en base à l'origine");

        ParamControleSignalRejet param = new ParamControleSignalRejet();
        param.setAuditUtilisateurCreation("TESTEUR");
        param.setIdentifiantControle("GERD_REJET");
        param.setNofam("ALL");
        param.setNiveauAlerte("C12");
        param.setMessageAlerte("Alerte rouge!");

        paramControleSignalRejetRepository.importeEnMasseEntitesTemporaires(Arrays.asList(param));
        paramControleSignalRejetRepository.promeutEntitesTemporaires();

        ParamControleSignalRejetId id = new ParamControleSignalRejetId();
        id.setIdentifiantControle(param.getIdentifiantControle());
        id.setNofam(param.getNofam());
        ParamControleSignalRejet cree = paramControleSignalRejetRepository.get(id);

        Assert.assertNotNull(cree);
        Assert.assertEquals(cree.getIdentifiantControle(), param.getIdentifiantControle());
        Assert.assertEquals(cree.getNiveauAlerte(), param.getNiveauAlerte());
        Assert.assertEquals(cree.getMessageAlerte(), param.getMessageAlerte());
        Assert.assertEquals(cree.getNofam(), param.getNofam());
    }
}
