package fr.si2m.red.internal.parametrage;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTransactionalTestNGSpringContextTests;
import org.testng.Assert;
import org.testng.annotations.Test;

import fr.si2m.red.parametrage.ParamNatureBase;
import fr.si2m.red.parametrage.ParamNatureBaseId;

@Test
@DirtiesContext
@ActiveProfiles("test")
@ContextConfiguration(locations = { "classpath:fr/si2m/red/config.xml" })
public class JpaParamNatureBaseRepositoryTests extends AbstractTransactionalTestNGSpringContextTests {

    @Autowired
    private JpaParamNatureBaseRepository paramNatureBaseRepository;

    @Test
    public void testInsertAndGet() {
        List<ParamNatureBase> initialement = paramNatureBaseRepository.liste();
        Assert.assertEquals(0, initialement.size(), "Il ne devrait y avoir aucune gestion en base à l'origine");

        ParamNatureBase param = new ParamNatureBase();
        param.setAuditUtilisateurCreation("TESTEUR");
        param.setCodeNatureBaseCotisations(12);
        param.setTauxCalculRempli(true);
        param.setNumTranche(4);
        param.setLibelleTranche("Check");
        param.setTauxUtilise(null);
        param.setMontantUtilise("MTCUNCU");
        param.setComplementDesignation("Shaka");

        paramNatureBaseRepository.importeEnMasseEntitesTemporaires(Arrays.asList(param));
        paramNatureBaseRepository.promeutEntitesTemporaires();

        ParamNatureBaseId id = new ParamNatureBaseId();
        id.setCodeNatureBaseCotisations(param.getCodeNatureBaseCotisations());
        id.setTauxCalculRempliAsText(param.getTauxCalculRempliAsText());
        id.setNumTranche(param.getNumTranche());

        ParamNatureBase cree = paramNatureBaseRepository.get(id);

        Assert.assertNotNull(cree);
        Assert.assertEquals(cree.getCodeNatureBaseCotisations(), param.getCodeNatureBaseCotisations());
        Assert.assertEquals(cree.getTauxCalculRempliAsText(), param.getTauxCalculRempliAsText());
        Assert.assertEquals(cree.isTauxCalculRempli(), param.isTauxCalculRempli());
        Assert.assertEquals(cree.getNumTranche(), param.getNumTranche());
        Assert.assertEquals(cree.getLibelleTranche(), param.getLibelleTranche());
        Assert.assertEquals(cree.getTauxUtilise(), param.getTauxUtilise());
        Assert.assertEquals(cree.getMontantUtilise(), param.getMontantUtilise());
        Assert.assertEquals(cree.getComplementDesignation(), param.getComplementDesignation());
    }

}
