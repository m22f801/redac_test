package fr.si2m.red.internal.dsn;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTransactionalTestNGSpringContextTests;
import org.testng.Assert;
import org.testng.annotations.Test;

import fr.si2m.red.dsn.AdhesionEtablissementMois;
import fr.si2m.red.dsn.AdhesionEtablissementMoisId;

@Test
@ActiveProfiles("test")
@ContextConfiguration(locations = { "classpath:fr/si2m/red/config.xml" })
public class JpaAdhesionEtablissementMoisRepositoryTests extends AbstractTransactionalTestNGSpringContextTests {

    @Autowired
    private JpaAdhesionEtablissementMoisRepository adhesionEtablissementMoisRepository;

    @Test
    public void testInsertAndGet() {
        List<AdhesionEtablissementMois> initiaux = adhesionEtablissementMoisRepository.liste();
        Assert.assertEquals(0, initiaux.size(), "Il ne devrait y avoir aucun adhesionEtablissementMois en base à l'origine");

        AdhesionEtablissementMois entite = new AdhesionEtablissementMois();
        entite.setIdAdhEtabMois("cleAdhesionEtablissementMois");
        entite.setAuditUtilisateurCreation("TESTEUR");

        adhesionEtablissementMoisRepository.importeEnMasseEntitesTemporaires(Arrays.asList(entite));

        AdhesionEtablissementMoisId id = new AdhesionEtablissementMoisId();
        id.setIdAdhEtabMois(entite.getIdAdhEtabMois());

        AdhesionEtablissementMois cree = adhesionEtablissementMoisRepository.get(id);
        Assert.assertNull(cree, "Le adhesionEtablissementMois devrait être créé dans l'espace temporaire pour le moment");

        adhesionEtablissementMoisRepository.promeutEntitesTemporaires();

        cree = adhesionEtablissementMoisRepository.get(id);
        Assert.assertNotNull(cree, "Le adhesionEtablissementMois devrait être promu");

        Assert.assertNotNull(cree);
        Assert.assertEquals(cree.getIdAdhEtabMois(), entite.getIdAdhEtabMois());
        Assert.assertFalse(cree.isLigneEnCoursImportBatch(), "Le adhesionEtablissementMois ne devrait plus être dans l'espace temporaire");

    }

}
