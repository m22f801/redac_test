package fr.si2m.red.internal.parametrage;

import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTransactionalTestNGSpringContextTests;
import org.testng.Assert;
import org.testng.annotations.Test;

import fr.si2m.red.parametrage.ParamValeurDefaut;
import fr.si2m.red.parametrage.ParamValeurDefautId;

@Test
@DirtiesContext
@ActiveProfiles("test")
@ContextConfiguration(locations = { "classpath:fr/si2m/red/config.xml" })
public class JpaParamValeurDefautRepositoryTests extends AbstractTransactionalTestNGSpringContextTests {

    @Autowired
    private JpaParamValeurDefautRepository paramValeurDefautRepository;

    @Test
    public void testGetParametresParDefautInitiaux() {
        ParamValeurDefaut valeurDefaut = new ParamValeurDefaut();
        valeurDefaut.setAuditUtilisateurCreation("TESTEUR");
        valeurDefaut.setCodeOrganisme("A4M001");
        valeurDefaut.setExploitationDSNAsText("OUI");
        valeurDefaut.setEditionConsignePaiementModeDSNAsText("OUI");
        valeurDefaut.setTransfertDeclarationsVersQuatremAutoriseAsText("OUI");
        valeurDefaut.setSeuilVariationAlertesEnNbEtablissements(5);
        valeurDefaut.setSeuilVariationAlertesEnPourcentageNbEtablissements(10);
        valeurDefaut.setSeuilVariationAlertesEnNbSalaries(5);
        valeurDefaut.setSeuilVariationAlertesEnPourcentageNbSalaries(10);
        valeurDefaut.setContratVIPAsText("N");
        valeurDefaut.setModeNatureContrat("C");
        valeurDefaut.setModeReaffectationCategorieEffectifs("REACA");

        paramValeurDefautRepository.importeEnMasseEntitesTemporaires(Arrays.asList(valeurDefaut));
        paramValeurDefautRepository.nettoieEntitesActives();
        paramValeurDefautRepository.promeutEntitesTemporaires();
        paramValeurDefautRepository.nettoieEntitesTemporaires();

        ParamValeurDefautId id = new ParamValeurDefautId();

        ParamValeurDefaut paramInitiaux = paramValeurDefautRepository.get(id);
        Assert.assertNotNull(paramInitiaux);
        Assert.assertEquals(paramInitiaux.getCodeOrganisme(), "A4M001");
        Assert.assertTrue(paramInitiaux.isExploitationDSN());
        Assert.assertTrue(paramInitiaux.isEditionConsignePaiementModeDSN());
        Assert.assertTrue(paramInitiaux.isTransfertDeclarationsVersQuatremAutorise());
        Assert.assertEquals(paramInitiaux.getSeuilVariationAlertesEnNbEtablissements(), Integer.valueOf(5));
        Assert.assertEquals(paramInitiaux.getSeuilVariationAlertesEnPourcentageNbEtablissements(), Integer.valueOf(10));
        Assert.assertEquals(paramInitiaux.getSeuilVariationAlertesEnNbSalaries(), Integer.valueOf(5));
        Assert.assertEquals(paramInitiaux.getSeuilVariationAlertesEnPourcentageNbSalaries(), Integer.valueOf(10));
        Assert.assertFalse(paramInitiaux.isContratVIP());
        Assert.assertEquals(paramInitiaux.getModeNatureContrat(), "C");
        Assert.assertEquals(paramInitiaux.getModeReaffectationCategorieEffectifs(), "REACA");
    }

}
