package fr.si2m.red.internal.parametrage;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTransactionalTestNGSpringContextTests;
import org.testng.Assert;
import org.testng.annotations.Test;

import fr.si2m.red.parametrage.ParamGroupeGestion;
import fr.si2m.red.parametrage.ParamGroupeGestionId;

@Test
@DirtiesContext
@ActiveProfiles("test")
@ContextConfiguration(locations = { "classpath:fr/si2m/red/config.xml" })
public class JpaParamGroupeGestionRepositoryTests extends AbstractTransactionalTestNGSpringContextTests {

    @Autowired
    private JpaParamGroupeGestionRepository paramGroupeGestionRepository;

    @Test
    public void testInsertAndGetUneGestion() {
        List<ParamGroupeGestion> gestionsInitiales = paramGroupeGestionRepository.liste();
        Assert.assertEquals(0, gestionsInitiales.size(), "Il ne devrait y avoir aucun mode de calcul en base à l'origine");

        ParamGroupeGestion groupeGestion = new ParamGroupeGestion();
        groupeGestion.setAuditUtilisateurCreation("TESTEUR");
        groupeGestion.setNumGroupeGestion("T1");
        groupeGestion.setMailAlerte("yyy@xxx.fr");

        paramGroupeGestionRepository.importeEnMasseEntitesTemporaires(Arrays.asList(groupeGestion));
        paramGroupeGestionRepository.promeutEntitesTemporaires();

        ParamGroupeGestionId id = new ParamGroupeGestionId();
        id.setNumGroupeGestion(groupeGestion.getNumGroupeGestion());
        ParamGroupeGestion groupeGestionCree = paramGroupeGestionRepository.get(id);

        Assert.assertNotNull(groupeGestionCree, "Le mode n'a pas été correctement inséré ou n'est pas requêtable");
        Assert.assertEquals(groupeGestionCree.getNumGroupeGestion(), groupeGestion.getNumGroupeGestion(), "Le numéro de groupe n'est pas le bon");
        Assert.assertEquals(groupeGestionCree.getMailAlerte(), groupeGestion.getMailAlerte(), "Le mail n'est pas le bon");
    }

}
