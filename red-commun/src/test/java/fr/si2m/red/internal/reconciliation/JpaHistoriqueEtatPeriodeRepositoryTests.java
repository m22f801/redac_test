package fr.si2m.red.internal.reconciliation;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTransactionalTestNGSpringContextTests;
import org.testng.Assert;
import org.testng.annotations.Test;

import fr.si2m.red.reconciliation.HistoriqueEtatPeriode;

@Test
@ActiveProfiles("test")
@ContextConfiguration(locations = { "classpath:fr/si2m/red/config.xml" })
public class JpaHistoriqueEtatPeriodeRepositoryTests extends AbstractTransactionalTestNGSpringContextTests {

    @Autowired
    private JpaHistoriqueEtatPeriodeRepository historiqueEtatPeriodeRepository;

    @Test
    public void testInsertAndGet() {
        List<HistoriqueEtatPeriode> initiaux = historiqueEtatPeriodeRepository.liste();
        Assert.assertEquals(0, initiaux.size(), "Il ne devrait y avoir aucun historiqueEtatPeriode en base à l'origine");

        HistoriqueEtatPeriode entite = new HistoriqueEtatPeriode();
        entite.setIdPeriode(123L);
        entite.setDateHeureChangement(20151102133053L);
        entite.setAuditUtilisateurCreation("TEST");

        historiqueEtatPeriodeRepository.create(entite);

        HistoriqueEtatPeriode cree = historiqueEtatPeriodeRepository.get(1L);
        Assert.assertNotNull(cree);

        Assert.assertEquals(cree.getIdPeriode(), entite.getIdPeriode());
        Assert.assertEquals(cree.getDateHeureChangement(), entite.getDateHeureChangement());

    }

}
