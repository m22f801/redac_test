package fr.si2m.red.internal.parametrage;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTransactionalTestNGSpringContextTests;
import org.testng.Assert;
import org.testng.annotations.Test;

import fr.si2m.red.parametrage.ParamFamilleModeCalculContrat;
import fr.si2m.red.parametrage.ParamFamilleModeCalculContratId;

@Test
@DirtiesContext
@ActiveProfiles("test")
@ContextConfiguration(locations = { "classpath:fr/si2m/red/config.xml" })
public class JpaParamFamilleModeCalculContratRepositoryTests extends AbstractTransactionalTestNGSpringContextTests {

    @Autowired
    private JpaParamFamilleModeCalculContratRepository paramFamilleModeCalculContratRepository;

    @Test
    public void testInsertAndGetUneFamille() {
        List<ParamFamilleModeCalculContrat> modesInitiaux = paramFamilleModeCalculContratRepository.liste();
        Assert.assertEquals(0, modesInitiaux.size(), "Il ne devrait y avoir aucun mode de calcul en base à l'origine");

        ParamFamilleModeCalculContrat modeCalcul = new ParamFamilleModeCalculContrat();
        modeCalcul.setAuditUtilisateurCreation("TESTEUR");
        modeCalcul.setNumFamille(53);
        modeCalcul.setModeCalculCotisation(2);
        modeCalcul.setModeControleTypesBasesAssujetties("N");
        modeCalcul.setModeDeclaration("A");

        paramFamilleModeCalculContratRepository.importeEnMasseEntitesTemporaires(Arrays.asList(modeCalcul));
        paramFamilleModeCalculContratRepository.promeutEntitesTemporaires();

        ParamFamilleModeCalculContratId id = new ParamFamilleModeCalculContratId();
        id.setNumFamille(modeCalcul.getNumFamille());
        id.setModeCalculCotisation(modeCalcul.getModeCalculCotisation());
        id.setModeDeclaration(modeCalcul.getModeDeclaration());
        ParamFamilleModeCalculContrat modeCalculCree = paramFamilleModeCalculContratRepository.get(id);

        Assert.assertNotNull(modeCalculCree, "Le mode n'a pas été correctement inséré ou n'est pas requêtable");
        Assert.assertEquals(modeCalcul.getNumFamille(), modeCalculCree.getNumFamille(), "Le numéro de famille n'est pas le bon");
        Assert.assertEquals(modeCalcul.getModeCalculCotisation(), modeCalculCree.getModeCalculCotisation(),
                "Le mode de calcul de cotisation n'est pas le bon");
        Assert.assertEquals(modeCalcul.getModeControleTypesBasesAssujetties(), modeCalculCree.getModeControleTypesBasesAssujetties(),
                "Le mode de contrôle n'est pas le bon");
    }

}
