package fr.si2m.red.internal.dsn;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTransactionalTestNGSpringContextTests;
import org.testng.Assert;
import org.testng.annotations.Test;

import fr.si2m.red.dsn.Contact;
import fr.si2m.red.dsn.ContactId;

@Test
@ActiveProfiles("test")
@ContextConfiguration(locations = { "classpath:fr/si2m/red/config.xml" })
public class JpaContactRepositoryTests extends AbstractTransactionalTestNGSpringContextTests {

    @Autowired
    private JpaContactRepository contactRepository;

    @Test
    public void testInsertAndGet() {
        List<Contact> initiaux = contactRepository.liste();
        Assert.assertEquals(0, initiaux.size(), "Il ne devrait y avoir aucun contact en base à l'origine");

        Contact entite = new Contact();
        entite.setIdContact("cleContact");
        entite.setAuditUtilisateurCreation("TESTEUR");

        contactRepository.importeEnMasseEntitesTemporaires(Arrays.asList(entite));

        ContactId id = new ContactId();
        id.setIdContact(entite.getIdContact());

        Contact cree = contactRepository.get(id);
        Assert.assertNull(cree, "Le contact devrait être créé dans l'espace temporaire pour le moment");

        contactRepository.promeutEntitesTemporaires();

        cree = contactRepository.get(id);
        Assert.assertNotNull(cree, "Le contact devrait être promu");

        Assert.assertNotNull(cree);
        Assert.assertEquals(cree.getIdContact(), entite.getIdContact());
        Assert.assertFalse(cree.isLigneEnCoursImportBatch(), "Le contact ne devrait plus être dans l'espace temporaire");

    }

}
