package fr.si2m.red.internal.dsn;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTransactionalTestNGSpringContextTests;
import org.testng.Assert;
import org.testng.annotations.Test;

import fr.si2m.red.dsn.ComposantVersement;
import fr.si2m.red.dsn.ComposantVersementId;

@Test
@ActiveProfiles("test")
@ContextConfiguration(locations = { "classpath:fr/si2m/red/config.xml" })
public class JpaComposantVersementRepositoryTests extends AbstractTransactionalTestNGSpringContextTests {

    @Autowired
    private JpaComposantVersementRepository composantVersementRepository;

    @Test
    public void testInsertAndGet() {
        List<ComposantVersement> initiaux = composantVersementRepository.liste();
        Assert.assertEquals(0, initiaux.size(), "Il ne devrait y avoir aucun composantVersement en base à l'origine");

        ComposantVersement entite = new ComposantVersement();
        entite.setIdComposantVersement("cleComposantVersement");
        entite.setAuditUtilisateurCreation("TESTEUR");

        composantVersementRepository.importeEnMasseEntitesTemporaires(Arrays.asList(entite));

        ComposantVersementId id = new ComposantVersementId();
        id.setIdComposantVersement(entite.getIdComposantVersement());

        ComposantVersement cree = composantVersementRepository.get(id);
        Assert.assertNull(cree, "Le composantVersement devrait être créé dans l'espace temporaire pour le moment");

        composantVersementRepository.promeutEntitesTemporaires();

        cree = composantVersementRepository.get(id);
        Assert.assertNotNull(cree, "Le composantVersement devrait être promu");

        Assert.assertNotNull(cree);
        Assert.assertEquals(cree.getIdComposantVersement(), entite.getIdComposantVersement());
        Assert.assertFalse(cree.isLigneEnCoursImportBatch(), "Le composantVersement ne devrait plus être dans l'espace temporaire");

    }

}
