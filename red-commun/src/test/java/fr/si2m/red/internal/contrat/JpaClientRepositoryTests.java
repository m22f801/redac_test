package fr.si2m.red.internal.contrat;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTransactionalTestNGSpringContextTests;
import org.testng.Assert;
import org.testng.annotations.Test;

import fr.si2m.red.contrat.Client;
import fr.si2m.red.contrat.ClientId;

@Test
@DirtiesContext
@ActiveProfiles("test")
@ContextConfiguration(locations = { "classpath:fr/si2m/red/config.xml" })
public class JpaClientRepositoryTests extends AbstractTransactionalTestNGSpringContextTests {

    @Autowired
    private JpaClientRepository clientRepository;

    @Test
    public void testInsertAndGet() {
        List<Client> initialement = clientRepository.liste();
        Assert.assertEquals(0, initialement.size(), "Il ne devrait y avoir aucun intermédiaire en base à l'origine");

        Client entite = new Client();
        entite.setAuditUtilisateurCreation("TESTEUR");
        entite.setNumClient(123456L);
        entite.setTypeClient(1);
        entite.setRaisonSociale("CGI");
        entite.setAdresseBatiment("1A");
        entite.setAdresseRue("rue des Nanettes");
        entite.setAdresseCommune("Paris");
        entite.setAdresseCodePostal("75011");
        entite.setBureauDistributeur("CA");
        entite.setCodePays("FRA");
        entite.setCodeNaf("AE");
        entite.setNumSiren("123456789");
        entite.setNumSiret("12345");
        entite.setTelephone("0755223344");
        entite.setFax("0455223344");
        entite.setEtatClient("7");
        entite.setDateEtat(20150101);
        entite.setCodePartenaire(3);
        entite.setNumClientMaitre(null);
        entite.setCodeCcn("2356");
        entite.setCodeGrp("33");
        entite.setCodeTns("2");
        entite.setCodeAutoEntrepreneur("2");
        entite.setDateDerniereModification(20140306);

        clientRepository.importeEnMasseEntitesTemporaires(Arrays.asList(entite));

        ClientId id = new ClientId();
        id.setNumClient(entite.getNumClient());

        Client cree = clientRepository.get(id);
        Assert.assertNull(cree, "Le client devrait être créé dans l'espace temporaire pour le moment");

        clientRepository.promeutEntitesTemporaires();

        cree = clientRepository.get(id);
        Assert.assertNotNull(cree, "Le client devrait être promu");

        Assert.assertEquals(entite.getNumClient(), cree.getNumClient());
        Assert.assertEquals(entite.getTypeClient(), cree.getTypeClient());
        Assert.assertEquals(entite.getRaisonSociale(), cree.getRaisonSociale());
        Assert.assertEquals(entite.getAdresseBatiment(), cree.getAdresseBatiment());
        Assert.assertEquals(entite.getAdresseRue(), cree.getAdresseRue());
        Assert.assertEquals(entite.getAdresseCommune(), cree.getAdresseCommune());
        Assert.assertEquals(entite.getAdresseCodePostal(), cree.getAdresseCodePostal());
        Assert.assertEquals(entite.getBureauDistributeur(), cree.getBureauDistributeur());
        Assert.assertEquals(entite.getCodePays(), cree.getCodePays());
        Assert.assertEquals(entite.getCodeNaf(), cree.getCodeNaf());
        Assert.assertEquals(entite.getNumSiren(), cree.getNumSiren());
        Assert.assertEquals(entite.getNumSiret(), cree.getNumSiret());
        Assert.assertEquals(entite.getTelephone(), cree.getTelephone());
        Assert.assertEquals(entite.getFax(), cree.getFax());
        Assert.assertEquals(entite.getEtatClient(), cree.getEtatClient());
        Assert.assertEquals(entite.getDateEtat(), cree.getDateEtat());
        Assert.assertEquals(entite.getCodePartenaire(), cree.getCodePartenaire());
        Assert.assertEquals(entite.getNumClientMaitre(), cree.getNumClientMaitre());
        Assert.assertEquals(entite.getCodeCcn(), cree.getCodeCcn());
        Assert.assertEquals(entite.getCodeGrp(), cree.getCodeGrp());
        Assert.assertEquals(entite.getCodeTns(), cree.getCodeTns());
        Assert.assertEquals(entite.getCodeAutoEntrepreneur(), cree.getCodeAutoEntrepreneur());
        Assert.assertEquals(entite.getDateDerniereModification(), cree.getDateDerniereModification());

    }

}
