package fr.si2m.red.internal.parametrage;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTransactionalTestNGSpringContextTests;
import org.testng.Assert;
import org.testng.annotations.Test;

import fr.si2m.red.parametrage.ParamFamilleContrat;
import fr.si2m.red.parametrage.ParamFamilleContratId;

@Test
@DirtiesContext
@ActiveProfiles("test")
@ContextConfiguration(locations = { "classpath:fr/si2m/red/config.xml" })
public class JpaParamFamilleContratRepositoryTests extends AbstractTransactionalTestNGSpringContextTests {

    @Autowired
    private JpaParamFamilleContratRepository paramFamilleContratRepository;

    @Test
    public void testInsertAndGetUneFamille() {
        List<ParamFamilleContrat> famillesInitiales = paramFamilleContratRepository.liste();
        Assert.assertEquals(0, famillesInitiales.size(), "Il ne devrait y avoir aucune famille en base à l'origine");

        ParamFamilleContrat famille = new ParamFamilleContrat();
        famille.setAuditUtilisateurCreation("TESTEUR");
        famille.setNumFamille(53);
        famille.setApplicationAvalEnCharge("WQUI");
        famille.setDesignationFamille("Maladie");
        famille.setTypeConsolidationSalaire("INDIV");
        famille.setTypeContratCodeDSN("Y22");

        paramFamilleContratRepository.importeEnMasseEntitesTemporaires(Arrays.asList(famille));
        paramFamilleContratRepository.promeutEntitesTemporaires();

        ParamFamilleContratId id = new ParamFamilleContratId();
        id.setNumFamille(famille.getNumFamille());
        ParamFamilleContrat familleCreee = paramFamilleContratRepository.get(id);

        Assert.assertNotNull(familleCreee, "La famille n'a pas été correctement insérée ou n'est pas requêtable");
        Assert.assertEquals(famille.getNumFamille(), familleCreee.getNumFamille(), "Le numéro de famille n'est pas le bon");
        Assert.assertEquals(famille.getApplicationAvalEnCharge(), familleCreee.getApplicationAvalEnCharge(),
                "L'application en charge de la famille n'est pas la bonne");
        Assert.assertEquals(famille.getDesignationFamille(), familleCreee.getDesignationFamille(), "La désignation de famille n'est pas bonne");
        Assert.assertEquals(famille.getTypeConsolidationSalaire(), familleCreee.getTypeConsolidationSalaire(),
                "Le type de consolidation de salaire n'est pas le bon");
    }

}
