package fr.si2m.red.internal.dsn;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTransactionalTestNGSpringContextTests;
import org.testng.Assert;
import org.testng.annotations.Test;

import fr.si2m.red.dsn.BaseAssujettie;
import fr.si2m.red.dsn.BaseAssujettieId;

@Test
@ActiveProfiles("test")
@ContextConfiguration(locations = { "classpath:fr/si2m/red/config.xml" })
public class JpaBaseAssujettieRepositoryTests extends AbstractTransactionalTestNGSpringContextTests {

    @Autowired
    private JpaBaseAssujettieRepository baseAssujettieRepository;

    @Test
    public void testInsertAndGet() {
        List<BaseAssujettie> initiaux = baseAssujettieRepository.liste();
        Assert.assertEquals(0, initiaux.size(), "Il ne devrait y avoir aucun baseAssujettie en base à l'origine");

        BaseAssujettie entite = new BaseAssujettie();
        entite.setIdBaseAssujettie("cleBaseAssujettie");
        entite.setAuditUtilisateurCreation("TESTEUR");

        baseAssujettieRepository.importeEnMasseEntitesTemporaires(Arrays.asList(entite));

        BaseAssujettieId id = new BaseAssujettieId();
        id.setIdBaseAssujettie(entite.getIdBaseAssujettie());

        BaseAssujettie cree = baseAssujettieRepository.get(id);
        Assert.assertNull(cree, "Le baseAssujettie devrait être créé dans l'espace temporaire pour le moment");

        baseAssujettieRepository.promeutEntitesTemporaires();

        cree = baseAssujettieRepository.get(id);
        Assert.assertNotNull(cree, "Le baseAssujettie devrait être promu");

        Assert.assertNotNull(cree);
        Assert.assertEquals(cree.getIdBaseAssujettie(), entite.getIdBaseAssujettie());
        Assert.assertFalse(cree.isLigneEnCoursImportBatch(), "Le baseAssujettie ne devrait plus être dans l'espace temporaire");

    }

}
