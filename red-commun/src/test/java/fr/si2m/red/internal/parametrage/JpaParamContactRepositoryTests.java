package fr.si2m.red.internal.parametrage;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTransactionalTestNGSpringContextTests;
import org.testng.Assert;
import org.testng.annotations.Test;

import fr.si2m.red.parametrage.ParamContact;
import fr.si2m.red.parametrage.ParamContactId;

@Test
@DirtiesContext
@ActiveProfiles("test")
@ContextConfiguration(locations = { "classpath:fr/si2m/red/config.xml" })
public class JpaParamContactRepositoryTests extends AbstractTransactionalTestNGSpringContextTests {

    @Autowired
    private JpaParamContactRepository paramContactRepository;

    @Test
    public void testInsertAndGet() {
        List<ParamContact> contactsInitiaux = paramContactRepository.liste();
        Assert.assertEquals(0, contactsInitiaux.size(), "Il ne devrait y avoir aucun contact en base à l'origine");

        ParamContact contact = new ParamContact();
        contact.setAuditUtilisateurCreation("TESTEUR");
        contact.setIdentifiantContact("1234");
        contact.setDebut(20140507);
        contact.setEmail("st@rk.com");

        paramContactRepository.importeEnMasseEntitesTemporaires(Arrays.asList(contact));
        paramContactRepository.promeutEntitesTemporaires();

        ParamContactId id = new ParamContactId();
        id.setIdentifiantContact(contact.getIdentifiantContact());
        id.setDebut(contact.getDebut());

        ParamContact contactCree = paramContactRepository.get(id);

        Assert.assertNotNull(contactCree, "Le contact n'a pas été correctement inséré ou n'est pas requêtable");
        Assert.assertEquals(contactCree.getIdentifiantContact(), contact.getIdentifiantContact());
        Assert.assertEquals(contactCree.getDebut(), contact.getDebut());
        Assert.assertEquals(contactCree.getEmail(), contact.getEmail());
    }

}
