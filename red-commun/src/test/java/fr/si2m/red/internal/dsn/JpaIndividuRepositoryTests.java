package fr.si2m.red.internal.dsn;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTransactionalTestNGSpringContextTests;
import org.testng.Assert;
import org.testng.annotations.Test;

import fr.si2m.red.dsn.Individu;
import fr.si2m.red.dsn.IndividuId;

@Test
@ActiveProfiles("test")
@ContextConfiguration(locations = { "classpath:fr/si2m/red/config.xml" })
public class JpaIndividuRepositoryTests extends AbstractTransactionalTestNGSpringContextTests {

    @Autowired
    private JpaIndividuRepository individuRepository;

    @Test
    public void testInsertAndGet() {
        List<Individu> initiaux = individuRepository.liste();
        Assert.assertEquals(0, initiaux.size(), "Il ne devrait y avoir aucun individu en base à l'origine");

        Individu entite = new Individu();
        entite.setIdIndividu("cleIndividu");
        entite.setAuditUtilisateurCreation("TESTEUR");

        individuRepository.importeEnMasseEntitesTemporaires(Arrays.asList(entite));

        IndividuId id = new IndividuId();
        id.setIdIndividu(entite.getIdIndividu());

        Individu cree = individuRepository.get(id);
        Assert.assertNull(cree, "Le individu devrait être créé dans l'espace temporaire pour le moment");

        individuRepository.promeutEntitesTemporaires();

        cree = individuRepository.get(id);
        Assert.assertNotNull(cree, "Le individu devrait être promu");

        Assert.assertNotNull(cree);
        Assert.assertEquals(cree.getIdIndividu(), entite.getIdIndividu());
        Assert.assertFalse(cree.isLigneEnCoursImportBatch(), "Le individu ne devrait plus être dans l'espace temporaire");

    }

}
