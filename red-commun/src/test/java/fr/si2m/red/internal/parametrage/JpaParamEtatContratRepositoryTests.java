package fr.si2m.red.internal.parametrage;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTransactionalTestNGSpringContextTests;
import org.testng.Assert;
import org.testng.annotations.Test;

import fr.si2m.red.parametrage.ParamEtatContrat;
import fr.si2m.red.parametrage.ParamEtatContratId;

@Test
@DirtiesContext
@ActiveProfiles("test")
@ContextConfiguration(locations = { "classpath:fr/si2m/red/config.xml" })
public class JpaParamEtatContratRepositoryTests extends AbstractTransactionalTestNGSpringContextTests {

    @Autowired
    private JpaParamEtatContratRepository paramEtatContratRepository;

    @Test
    public void testInsertAndGetUnEtat() {
        List<ParamEtatContrat> etatsContrats = paramEtatContratRepository.liste();
        Assert.assertEquals(0, etatsContrats.size(), "Il ne devrait y avoir aucun état contrat en base à l'origine");

        ParamEtatContrat etatContrat = new ParamEtatContrat();
        etatContrat.setAuditUtilisateurCreation("TESTEUR");
        etatContrat.setEtatContrat(1);
        etatContrat.setActif(true);

        paramEtatContratRepository.importeEnMasseEntitesTemporaires(Arrays.asList(etatContrat));
        paramEtatContratRepository.promeutEntitesTemporaires();

        ParamEtatContratId id = new ParamEtatContratId();
        id.setEtatContrat(etatContrat.getEtatContrat());
        ParamEtatContrat etatContratCree = paramEtatContratRepository.get(id);

        Assert.assertNotNull(etatContratCree, "L'état n'a pas été correctement inséré ou n'est pas requêtable");
        Assert.assertEquals(etatContrat.getActifAsText(), etatContratCree.getActifAsText(), "L'état actif n'est pas le bon");
    }

    @Test
    public void testInsertAndGetMultipleLibelles() {
        List<ParamEtatContrat> etatsContrats = paramEtatContratRepository.liste();
        Assert.assertEquals(0, etatsContrats.size(), "Il ne devrait y avoir aucun état contrat en base à l'origine");

        ParamEtatContrat etatContrat1 = new ParamEtatContrat();
        etatContrat1.setAuditUtilisateurCreation("TESTEUR");
        etatContrat1.setEtatContrat(1);
        etatContrat1.setActif(true);

        ParamEtatContrat etatContrat2 = new ParamEtatContrat();
        etatContrat2.setAuditUtilisateurCreation("TESTEUR");
        etatContrat2.setEtatContrat(2);
        etatContrat2.setActif(false);

        paramEtatContratRepository.importeEnMasseEntitesTemporaires(Arrays.asList(etatContrat1, etatContrat2));
        paramEtatContratRepository.promeutEntitesTemporaires();
        paramEtatContratRepository.nettoieEntitesTemporaires();

        List<ParamEtatContrat> listeEtatsContrats = paramEtatContratRepository.liste();

        Assert.assertEquals(listeEtatsContrats.size(), 2, "Les états n'ont pas été persistés en base");

        Assert.assertEquals(listeEtatsContrats.get(0).getActifAsText(), etatContrat1.getActifAsText(), "L'état actif n'est pas le bon");
        Assert.assertEquals(listeEtatsContrats.get(1).getActifAsText(), etatContrat2.getActifAsText(), "L'état actif n'est pas le bon");

    }
}
