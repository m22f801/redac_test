package fr.si2m.red.internal.parametrage;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTransactionalTestNGSpringContextTests;
import org.testng.Assert;
import org.testng.annotations.Test;

import fr.si2m.red.parametrage.ParamGestionPeriodes;
import fr.si2m.red.parametrage.ParamGestionPeriodesId;

@Test
@DirtiesContext
@ActiveProfiles("test")
@ContextConfiguration(locations = { "classpath:fr/si2m/red/config.xml" })
public class JpaParamGestionPeriodesRepositoryTests extends AbstractTransactionalTestNGSpringContextTests {

    @Autowired
    private JpaParamGestionPeriodesRepository paramGestionPeriodesRepository;

    @Test
    public void testInsertAndGet() {
        List<ParamGestionPeriodes> gestionsInitiales = paramGestionPeriodesRepository.liste();
        Assert.assertEquals(0, gestionsInitiales.size(), "Il ne devrait y avoir aucune gestion en base à l'origine");

        ParamGestionPeriodes periodes = new ParamGestionPeriodes();
        periodes.setAuditUtilisateurCreation("TESTEUR");
        periodes.setDelaiPublicationFinDeclaration5(66);
        periodes.setDelaiPublicationFinDeclaration15(272);
        periodes.setDelaiPublicationCreationDeclaration(11);
        periodes.setDelaiPublicationCreationComplement(12);
        periodes.setDelaiPublicationCreationRegulation(25);

        paramGestionPeriodesRepository.importeEnMasseEntitesTemporaires(Arrays.asList(periodes));
        paramGestionPeriodesRepository.promeutEntitesTemporaires();

        ParamGestionPeriodesId id = new ParamGestionPeriodesId();
        id.setDelaiPublicationFinDeclaration5(periodes.getDelaiPublicationFinDeclaration5());
        id.setDelaiPublicationFinDeclaration15(periodes.getDelaiPublicationFinDeclaration15());
        id.setDelaiPublicationCreationDeclaration(periodes.getDelaiPublicationCreationDeclaration());
        id.setDelaiPublicationCreationComplement(periodes.getDelaiPublicationCreationComplement());
        id.setDelaiPublicationCreationRegulation(periodes.getDelaiPublicationCreationRegulation());

        ParamGestionPeriodes gestionPeriodesCreee = paramGestionPeriodesRepository.get(id);

        Assert.assertNotNull(gestionPeriodesCreee);
        Assert.assertEquals(gestionPeriodesCreee.getDelaiPublicationFinDeclaration5(), periodes.getDelaiPublicationFinDeclaration5());
        Assert.assertEquals(gestionPeriodesCreee.getDelaiPublicationFinDeclaration15(), periodes.getDelaiPublicationFinDeclaration15());
        Assert.assertEquals(gestionPeriodesCreee.getDelaiPublicationCreationDeclaration(), periodes.getDelaiPublicationCreationDeclaration());
        Assert.assertEquals(gestionPeriodesCreee.getDelaiPublicationCreationComplement(), periodes.getDelaiPublicationCreationComplement());
        Assert.assertEquals(gestionPeriodesCreee.getDelaiPublicationCreationRegulation(), periodes.getDelaiPublicationCreationRegulation());
    }

}
