package fr.si2m.red.internal.dsn;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTransactionalTestNGSpringContextTests;
import org.testng.Assert;
import org.testng.annotations.Test;

import fr.si2m.red.dsn.Affiliation;
import fr.si2m.red.dsn.AffiliationId;

@Test
@ActiveProfiles("test")
@ContextConfiguration(locations = { "classpath:fr/si2m/red/config.xml" })
public class JpaAffiliationRepositoryTests extends AbstractTransactionalTestNGSpringContextTests {

    @Autowired
    private JpaAffiliationRepository affiliationRepository;

    @Test
    public void testInsertAndGet() {
        List<Affiliation> initiaux = affiliationRepository.liste();
        Assert.assertEquals(0, initiaux.size(), "Il ne devrait y avoir aucun affiliation en base à l'origine");

        Affiliation entite = new Affiliation();
        entite.setIdAffiliation("cleAffiliation");
        entite.setAuditUtilisateurCreation("TESTEUR");

        affiliationRepository.importeEnMasseEntitesTemporaires(Arrays.asList(entite));

        AffiliationId id = new AffiliationId();
        id.setIdAffiliation(entite.getIdAffiliation());

        Affiliation cree = affiliationRepository.get(id);
        Assert.assertNull(cree, "Le affiliation devrait être créé dans l'espace temporaire pour le moment");

        affiliationRepository.promeutEntitesTemporaires();

        cree = affiliationRepository.get(id);
        Assert.assertNotNull(cree, "Le affiliation devrait être promu");

        Assert.assertNotNull(cree);
        Assert.assertEquals(cree.getIdAffiliation(), entite.getIdAffiliation());
        Assert.assertFalse(cree.isLigneEnCoursImportBatch(), "Le affiliation ne devrait plus être dans l'espace temporaire");

    }

}
