package fr.si2m.red.internal.complement;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTransactionalTestNGSpringContextTests;
import org.testng.Assert;
import org.testng.annotations.Test;

import fr.si2m.red.complement.ExtensionEntrepriseAffiliee;

@Test
@DirtiesContext
@ActiveProfiles("test")
@ContextConfiguration(locations = { "classpath:fr/si2m/red/config.xml" })
public class JpaExtensionEntrepriseAffilieeRepositoryTests extends AbstractTransactionalTestNGSpringContextTests {

    @Autowired
    private JpaExtensionEntrepriseAffilieeRepository extensionEntrepriseAffilieeRepository;

    @Test
    public void testInsertAndGet() {
        List<ExtensionEntrepriseAffiliee> initiaux = extensionEntrepriseAffilieeRepository.liste();
        Assert.assertEquals(0, initiaux.size());

        ExtensionEntrepriseAffiliee entite = new ExtensionEntrepriseAffiliee();
        entite.setAuditUtilisateurCreation("TESTEUR");
        entite.setNumContrat("007");
        entite.setSiren("12548487");
        entite.setNic("0215");

        extensionEntrepriseAffilieeRepository.importeEnMasseEntitesTemporaires(Arrays.asList(entite));

        ExtensionEntrepriseAffiliee cree = extensionEntrepriseAffilieeRepository.get(1L);
        Assert.assertNotNull(cree);

        Assert.assertEquals(cree.getNumContrat(), entite.getNumContrat());
        Assert.assertEquals(cree.getSiren(), entite.getSiren());
        Assert.assertEquals(cree.getNic(), entite.getNic());

    }

}
