package fr.si2m.red.internal.dsn;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTransactionalTestNGSpringContextTests;
import org.testng.Assert;
import org.testng.annotations.Test;

import fr.si2m.red.dsn.ComposantBaseAssujettie;
import fr.si2m.red.dsn.ComposantBaseAssujettieId;

@Test
@ActiveProfiles("test")
@ContextConfiguration(locations = { "classpath:fr/si2m/red/config.xml" })
public class JpaComposantBaseAssujettieRepositoryTests extends AbstractTransactionalTestNGSpringContextTests {

    @Autowired
    private JpaComposantBaseAssujettieRepository composantBaseAssujettieRepository;

    @Test
    public void testInsertAndGet() {
        List<ComposantBaseAssujettie> initiaux = composantBaseAssujettieRepository.liste();
        Assert.assertEquals(0, initiaux.size(), "Il ne devrait y avoir aucun composantBaseAssujettie en base à l'origine");

        ComposantBaseAssujettie entite = new ComposantBaseAssujettie();
        entite.setIdComposantBaseAssujettie("cleComposantBaseAssujettie");
        entite.setAuditUtilisateurCreation("TESTEUR");

        composantBaseAssujettieRepository.importeEnMasseEntitesTemporaires(Arrays.asList(entite));

        ComposantBaseAssujettieId id = new ComposantBaseAssujettieId();
        id.setIdComposantBaseAssujettie(entite.getIdComposantBaseAssujettie());

        ComposantBaseAssujettie cree = composantBaseAssujettieRepository.get(id);
        Assert.assertNull(cree, "Le composantBaseAssujettie devrait être créé dans l'espace temporaire pour le moment");

        composantBaseAssujettieRepository.promeutEntitesTemporaires();

        cree = composantBaseAssujettieRepository.get(id);
        Assert.assertNotNull(cree, "Le composantBaseAssujettie devrait être promu");

        Assert.assertNotNull(cree);
        Assert.assertEquals(cree.getIdComposantBaseAssujettie(), entite.getIdComposantBaseAssujettie());
        Assert.assertFalse(cree.isLigneEnCoursImportBatch(), "Le composantBaseAssujettie ne devrait plus être dans l'espace temporaire");

    }

}
