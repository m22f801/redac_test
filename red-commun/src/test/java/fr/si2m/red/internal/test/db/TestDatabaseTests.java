package fr.si2m.red.internal.test.db;

import java.sql.DatabaseMetaData;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * Teste si la base de données de test peut bien être utilisée.
 * 
 * @author nortaina
 *
 */
public class TestDatabaseTests {

    /**
     * Teste l'utilisation de la BDD de test.
     * 
     * @throws SQLException
     *             si une erreur SQL survient
     */
    @Test
    public void testUtilisation() throws SQLException {
        TestDatabase.start(9093);

        DataSource dataSource = TestDatabase.getDataSource(9093);
        Assert.assertTrue(dataSource.getConnection().isValid(10), "On devrait pouvoir se connecter à la BDD de test");

        DatabaseMetaData metadata = dataSource.getConnection().getMetaData();
        Assert.assertEquals(metadata.getDatabaseProductName(), "H2", "On devrait se connecter à une base H2 et pas une autre");

        TestDatabase.shutdown(9093);
        boolean exceptionJetee = false;
        try {
            dataSource.getConnection();
        } catch (Exception e) {
            exceptionJetee = true;
        }
        Assert.assertTrue(exceptionJetee, "On ne devrait plus pouvoir se connecter à la BDD de test");
    }
}
