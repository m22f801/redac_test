package fr.si2m.red.internal.reconciliation;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTransactionalTestNGSpringContextTests;
import org.testng.Assert;
import org.testng.annotations.Test;

import fr.si2m.red.reconciliation.CategorieQuittancementIndividu;

@Test
@ActiveProfiles("test")
@ContextConfiguration(locations = { "classpath:fr/si2m/red/config.xml" })
public class JpaCategorieQuittancementIndividuRepositoryTests extends AbstractTransactionalTestNGSpringContextTests {

    @Autowired
    private JpaCategorieQuittancementIndividuRepository categorieQuittancementIndividuRepository;

    @Test
    public void testInsertAndGet() {
        List<CategorieQuittancementIndividu> initiaux = categorieQuittancementIndividuRepository.liste();
        Assert.assertEquals(0, initiaux.size(), "Il ne devrait y avoir aucun categorieQuittancementIndividu en base à l'origine");

        CategorieQuittancementIndividu entite = new CategorieQuittancementIndividu();
        entite.setIdPeriode(123L);
        entite.setNumCategorieQuittancement("xyz");
        entite.setIndividu("cleIndividu");
        entite.setAuditUtilisateurCreation("TEST");

        categorieQuittancementIndividuRepository.create(entite);

        CategorieQuittancementIndividu cree = categorieQuittancementIndividuRepository.get(1L);
        Assert.assertNotNull(cree);

        Assert.assertEquals(cree.getIdPeriode(), entite.getIdPeriode());
        Assert.assertEquals(cree.getNumCategorieQuittancement(), entite.getNumCategorieQuittancement());
        Assert.assertEquals(cree.getIndividu(), entite.getIndividu());

    }

}
