package fr.si2m.red.internal.contrat;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTransactionalTestNGSpringContextTests;
import org.testng.Assert;
import org.testng.annotations.Test;

import fr.si2m.red.contrat.Contrat;

@Test
@DirtiesContext
@ActiveProfiles("test")
@ContextConfiguration(locations = { "classpath:fr/si2m/red/config.xml" })
public class JpaContratRepositoryTests extends AbstractTransactionalTestNGSpringContextTests {

    @Autowired
    private JpaContratRepository contratRepository;

    @Test
    public void testInsertAndGetUnContrat() {
        List<Contrat> contratsInitiaux = contratRepository.liste();
        Assert.assertEquals(0, contratsInitiaux.size(), "Il ne devrait y avoir aucun contrat en base à l'origine");

        Contrat contrat1 = new Contrat();
        contrat1.setAuditUtilisateurCreation("TESTEUR");
        contrat1.setNumContrat("007");
        contrat1.setNumProduit("1522");
        contrat1.setDateDebutSituationLigne(20141205);

        contratRepository.importeEnMasseEntitesTemporaires(Arrays.asList(contrat1));

        Contrat contratCree = contratRepository.getContrat("007", 20141205);
        Assert.assertNull(contratCree, "Le contrat devrait être créé dans l'espace temporaire pour le moment");

        contratRepository.promeutEntitesTemporaires();

        contratCree = contratRepository.getContrat("007", 20141205);
        Assert.assertNotNull(contratCree, "Le contrat devrait être promu");

        Assert.assertEquals(contrat1.getNumContrat(), contratCree.getNumContrat(), "Le numéro du contrat 1 inséré n'est pas le bon");
        Assert.assertEquals(contrat1.getNumProduit(), contratCree.getNumProduit(), "Le numéro du produit 1 inséré n'est pas le bon");

    }

    @Test
    public void testInsertAndGetMultipleContrats() {
        List<Contrat> contratsInitiaux = contratRepository.liste();
        Assert.assertEquals(0, contratsInitiaux.size(), "Il ne devrait y avoir aucun contrat en base à l'origine");

        Contrat contrat1 = new Contrat();
        contrat1.setAuditUtilisateurCreation("TESTEUR");
        contrat1.setNumContrat("007");
        contrat1.setNumProduit("1522");
        contrat1.setDateDebutSituationLigne(20141205);
        Contrat contrat2 = new Contrat();
        contrat2.setAuditUtilisateurCreation("TESTEUR");
        contrat2.setNumContrat("42");
        contrat2.setNumProduit("6812");
        contrat2.setDateDebutSituationLigne(20141122);

        contratRepository.importeEnMasseEntitesTemporaires(Arrays.asList(contrat1, contrat2));
        contratRepository.promeutEntitesTemporaires();
        contratRepository.nettoieEntitesTemporaires();

        List<Contrat> contratsCrees = contratRepository.liste();
        Assert.assertEquals(2, contratsCrees.size(), "Deux contrats devraient avoir été créés en base");

        Contrat contratCree1 = contratsCrees.get(0);
        Assert.assertEquals(contrat1.getNumContrat(), contratCree1.getNumContrat(), "Le numéro du contrat 1 inséré n'est pas le bon");
        Assert.assertEquals(contrat1.getNumProduit(), contratCree1.getNumProduit(), "Le numéro du produit 1 inséré n'est pas le bon");
        Contrat contratCree2 = contratsCrees.get(1);
        Assert.assertEquals(contrat2.getNumContrat(), contratCree2.getNumContrat(), "Le numéro du contrat 2 inséré n'est pas le bon");
        Assert.assertEquals(contrat2.getNumProduit(), contratCree2.getNumProduit(), "Le numéro du produit 1 inséré n'est pas le bon");

    }
}
