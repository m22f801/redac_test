package fr.si2m.red.internal.reconciliation;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTransactionalTestNGSpringContextTests;
import org.testng.Assert;
import org.testng.annotations.Test;

import fr.si2m.red.reconciliation.IndicateursDSNPeriode;

@Test
@ActiveProfiles("test")
@ContextConfiguration(locations = { "classpath:fr/si2m/red/config.xml" })
public class JpaIndicateursDSNPeriodeRepositoryTests extends AbstractTransactionalTestNGSpringContextTests {

    @Autowired
    private JpaIndicateursDSNPeriodeRepository indicateursDSNPeriodeRepository;

    @Test
    public void testInsertAndGet() {
        List<IndicateursDSNPeriode> initiaux = indicateursDSNPeriodeRepository.liste();
        Assert.assertEquals(0, initiaux.size());

        IndicateursDSNPeriode entite = new IndicateursDSNPeriode();
        entite.setAuditUtilisateurCreation("TESTEUR");
        entite.setNumContrat("007");
        entite.setDebutPeriode(20150103);
        entite.setFinPeriode(20150515);
        entite.setDateDebutReceptionDSN(20141028);
        entite.setDateModifIndicateurs(20150505);
        entite.setGestionnaireModifIndicateurs("Tony Stark");
        entite.setMotifChangementIndicateurs("Pour les tests");
        entite.setExploitationDSNAsText("NON");
        entite.setDateDebutExploitationDSN(20150202);
        entite.setEditionConsignesPaiementDSNAsText("OUI");
        entite.setDateDebutEditionConsignesPaiement(20150305);
        entite.setModeGestionDSN("C");
        entite.setDateDebutModeGestionDSN(20140909);
        entite.setTransfertDSNAsText("NON");
        entite.setDateDebutTransfertDSN(20150815);
        entite.setSeuilVariationAlertesEnNbEtablissements(10);
        entite.setSeuilVariationAlertesEnPourcentageNbEtablissements(22);
        entite.setSeuilVariationAlertesEnNbSalaries(100);
        entite.setSeuilVariationAlertesEnPourcentageNbSalaries(30);
        entite.setControleTypeBaseAssujettie("RED");
        entite.setModeNatureContrat("L");
        entite.setCodeVIPAsText("O");
        entite.setModeReaffectationCategorieEffectif("REACA");

        indicateursDSNPeriodeRepository.importeEnMasseEntitesTemporaires(Arrays.asList(entite));

        IndicateursDSNPeriode cree = indicateursDSNPeriodeRepository.get(1L);
        Assert.assertNotNull(cree);

        Assert.assertEquals(cree.getNumContrat(), entite.getNumContrat());
        Assert.assertEquals(cree.getDebutPeriode(), entite.getDebutPeriode());
        Assert.assertEquals(cree.getFinPeriode(), entite.getFinPeriode());
        Assert.assertEquals(cree.getDateDebutReceptionDSN(), entite.getDateDebutReceptionDSN());
        Assert.assertEquals(cree.getDateModifIndicateurs(), entite.getDateModifIndicateurs());
        Assert.assertEquals(cree.getGestionnaireModifIndicateurs(), entite.getGestionnaireModifIndicateurs());
        Assert.assertEquals(cree.getMotifChangementIndicateurs(), entite.getMotifChangementIndicateurs());
        Assert.assertEquals(cree.getExploitationDSNAsText(), entite.getExploitationDSNAsText());
        Assert.assertEquals(cree.isExploitationDSN(), entite.isExploitationDSN());
        Assert.assertEquals(cree.getDateDebutExploitationDSN(), entite.getDateDebutExploitationDSN());
        Assert.assertEquals(cree.getEditionConsignesPaiementDSNAsText(), entite.getEditionConsignesPaiementDSNAsText());
        Assert.assertEquals(cree.isEditionConsignesPaiementDSN(), entite.isEditionConsignesPaiementDSN());
        Assert.assertEquals(cree.getDateDebutEditionConsignesPaiement(), entite.getDateDebutEditionConsignesPaiement());
        Assert.assertEquals(cree.getTransfertDSNAsText(), entite.getTransfertDSNAsText());
        Assert.assertEquals(cree.isTransfertDSN(), entite.isTransfertDSN());
        Assert.assertEquals(cree.getDateDebutTransfertDSN(), entite.getDateDebutTransfertDSN());
        Assert.assertEquals(cree.getModeGestionDSN(), entite.getModeGestionDSN());
        Assert.assertEquals(cree.getDateDebutModeGestionDSN(), entite.getDateDebutModeGestionDSN());
        Assert.assertEquals(cree.getSeuilVariationAlertesEnNbEtablissements(), entite.getSeuilVariationAlertesEnNbEtablissements());
        Assert.assertEquals(cree.getSeuilVariationAlertesEnPourcentageNbEtablissements(), cree.getSeuilVariationAlertesEnPourcentageNbEtablissements());
        Assert.assertEquals(cree.getSeuilVariationAlertesEnNbSalaries(), entite.getSeuilVariationAlertesEnNbSalaries());
        Assert.assertEquals(cree.getSeuilVariationAlertesEnPourcentageNbSalaries(), entite.getSeuilVariationAlertesEnPourcentageNbSalaries());
        Assert.assertEquals(cree.getControleTypeBaseAssujettie(), entite.getControleTypeBaseAssujettie());
        Assert.assertEquals(cree.getModeNatureContrat(), entite.getModeNatureContrat());
        Assert.assertEquals(cree.getCodeVIPAsText(), entite.getCodeVIPAsText());
        Assert.assertEquals(cree.isCodeVIP(), entite.isCodeVIP());
        Assert.assertEquals(cree.getModeReaffectationCategorieEffectif(), entite.getModeReaffectationCategorieEffectif());

        Assert.assertTrue(cree.isLigneEnCoursImportBatch());

    }

}
