package fr.si2m.red.internal.reconciliation;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTransactionalTestNGSpringContextTests;
import org.testng.Assert;
import org.testng.annotations.Test;

import fr.si2m.red.reconciliation.MessageControle;

@Test
@ActiveProfiles("test")
@ContextConfiguration(locations = { "classpath:fr/si2m/red/config.xml" })
public class JpaMessageControleRepositoryTests extends AbstractTransactionalTestNGSpringContextTests {

    @Autowired
    private JpaMessageControleRepository messageControleRepository;

    @Test
    public void testInsertAndGet() {
        List<MessageControle> initiaux = messageControleRepository.liste();
        Assert.assertEquals(0, initiaux.size(), "Il ne devrait y avoir aucun messageControle en base à l'origine");

        MessageControle entite = new MessageControle();
        entite.setIdPeriode(2L);
        entite.setAuditUtilisateurCreation("TEST");

        messageControleRepository.create(entite);

        MessageControle cree = messageControleRepository.get(1L);
        Assert.assertNotNull(cree);

        Assert.assertEquals(cree.getIdMessageControle(), entite.getIdMessageControle());
        Assert.assertEquals(cree.getIdPeriode(), entite.getIdPeriode());

    }

}
