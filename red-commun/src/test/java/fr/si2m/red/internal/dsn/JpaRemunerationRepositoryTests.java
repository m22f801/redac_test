package fr.si2m.red.internal.dsn;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTransactionalTestNGSpringContextTests;
import org.testng.Assert;
import org.testng.annotations.Test;

import fr.si2m.red.dsn.Remuneration;
import fr.si2m.red.dsn.RemunerationId;

@Test
@ActiveProfiles("test")
@ContextConfiguration(locations = { "classpath:fr/si2m/red/config.xml" })
public class JpaRemunerationRepositoryTests extends AbstractTransactionalTestNGSpringContextTests {

    @Autowired
    private JpaRemunerationRepository remunerationRepository;

    @Test
    public void testInsertAndGet() {
        List<Remuneration> initiaux = remunerationRepository.liste();
        Assert.assertEquals(0, initiaux.size(), "Il ne devrait y avoir aucun remuneration en base à l'origine");

        Remuneration entite = new Remuneration();
        entite.setIdRemuneration("cleRemuneration");
        entite.setMontantPaie(48659.12);
        entite.setAuditUtilisateurCreation("TESTEUR");

        remunerationRepository.importeEnMasseEntitesTemporaires(Arrays.asList(entite));

        RemunerationId id = new RemunerationId();
        id.setIdRemuneration(entite.getIdRemuneration());

        Remuneration cree = remunerationRepository.get(id);
        Assert.assertNull(cree, "Le remuneration devrait être créé dans l'espace temporaire pour le moment");

        remunerationRepository.promeutEntitesTemporaires();

        cree = remunerationRepository.get(id);
        Assert.assertNotNull(cree, "Le remuneration devrait être promu");

        Assert.assertNotNull(cree);
        Assert.assertEquals(cree.getIdRemuneration(), entite.getIdRemuneration());
        Assert.assertEquals(cree.getMontantPaie(), entite.getMontantPaie());
        Assert.assertFalse(cree.isLigneEnCoursImportBatch(), "Le remuneration ne devrait plus être dans l'espace temporaire");

    }

}
