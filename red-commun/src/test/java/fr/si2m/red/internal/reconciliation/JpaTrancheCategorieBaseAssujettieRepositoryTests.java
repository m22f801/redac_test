package fr.si2m.red.internal.reconciliation;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTransactionalTestNGSpringContextTests;
import org.testng.Assert;
import org.testng.annotations.Test;

import fr.si2m.red.reconciliation.TrancheCategorieBaseAssujettie;

@Test
@ActiveProfiles("test")
@ContextConfiguration(locations = { "classpath:fr/si2m/red/config.xml" })
public class JpaTrancheCategorieBaseAssujettieRepositoryTests extends AbstractTransactionalTestNGSpringContextTests {

    @Autowired
    private JpaTrancheCategorieBaseAssujettieRepository trancheCategorieBaseAssujettieRepository;

    @Test
    public void testInsertAndGet() {
        List<TrancheCategorieBaseAssujettie> initiaux = trancheCategorieBaseAssujettieRepository.liste();
        Assert.assertEquals(0, initiaux.size(), "Il ne devrait y avoir aucun trancheCategorieBaseAssujettie en base à l'origine");

        TrancheCategorieBaseAssujettie entite = new TrancheCategorieBaseAssujettie();
        entite.setIdPeriode(123L);
        entite.setIdBaseAssujettie("1234");
        entite.setMoisRattachement(20151101);
        entite.setIdAffiliation("cleAffiliation");
        entite.setDateDebutRattachementBase(20151102);
        entite.setNumTranche(3);
        entite.setNumCategorieQuittancement("xyz");
        entite.setAuditUtilisateurCreation("TEST");

        trancheCategorieBaseAssujettieRepository.create(entite);

        TrancheCategorieBaseAssujettie cree = trancheCategorieBaseAssujettieRepository.get(1L);
        Assert.assertNotNull(cree);

        Assert.assertEquals(cree.getIdPeriode(), entite.getIdPeriode());
        Assert.assertEquals(cree.getMoisRattachement(), entite.getMoisRattachement());
        Assert.assertEquals(cree.getIdAffiliation(), entite.getIdAffiliation());
        Assert.assertEquals(cree.getDateDebutRattachementBase(), entite.getDateDebutRattachementBase());
        Assert.assertEquals(cree.getNumTranche(), entite.getNumTranche());
        Assert.assertEquals(cree.getNumCategorieQuittancement(), entite.getNumCategorieQuittancement());

    }

}
