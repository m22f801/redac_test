package fr.si2m.red.internal.dsn;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTransactionalTestNGSpringContextTests;
import org.testng.Assert;
import org.testng.annotations.Test;

import fr.si2m.red.dsn.CotisationEtablissement;
import fr.si2m.red.dsn.CotisationEtablissementId;

@Test
@ActiveProfiles("test")
@ContextConfiguration(locations = { "classpath:fr/si2m/red/config.xml" })
public class JpaCotisationEtablissementRepositoryTests extends AbstractTransactionalTestNGSpringContextTests {

    @Autowired
    private JpaCotisationEtablissementRepository cotisationEtablissementRepository;

    @Test
    public void testInsertAndGet() {
        List<CotisationEtablissement> initiaux = cotisationEtablissementRepository.liste();
        Assert.assertEquals(0, initiaux.size(), "Il ne devrait y avoir aucun cotisationEtablissement en base à l'origine");

        CotisationEtablissement entite = new CotisationEtablissement();
        entite.setIdCotisationEtablissement("cleCotisationEtablissement");
        entite.setAuditUtilisateurCreation("TESTEUR");

        cotisationEtablissementRepository.importeEnMasseEntitesTemporaires(Arrays.asList(entite));

        CotisationEtablissementId id = new CotisationEtablissementId();
        id.setIdCotisationEtablissement(entite.getIdCotisationEtablissement());

        CotisationEtablissement cree = cotisationEtablissementRepository.get(id);
        Assert.assertNull(cree, "Le cotisationEtablissement devrait être créé dans l'espace temporaire pour le moment");

        cotisationEtablissementRepository.promeutEntitesTemporaires();

        cree = cotisationEtablissementRepository.get(id);
        Assert.assertNotNull(cree, "Le cotisationEtablissement devrait être promu");

        Assert.assertNotNull(cree);
        Assert.assertEquals(cree.getIdCotisationEtablissement(), entite.getIdCotisationEtablissement());
        Assert.assertFalse(cree.isLigneEnCoursImportBatch(), "Le cotisationEtablissement ne devrait plus être dans l'espace temporaire");

    }

}
