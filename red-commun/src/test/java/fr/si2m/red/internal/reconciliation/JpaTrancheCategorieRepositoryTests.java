package fr.si2m.red.internal.reconciliation;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTransactionalTestNGSpringContextTests;
import org.testng.Assert;
import org.testng.annotations.Test;

import fr.si2m.red.reconciliation.TrancheCategorie;

@Test
@ActiveProfiles("test")
@ContextConfiguration(locations = { "classpath:fr/si2m/red/config.xml" })
public class JpaTrancheCategorieRepositoryTests extends AbstractTransactionalTestNGSpringContextTests {

    @Autowired
    private JpaTrancheCategorieRepository trancheCategorieRepository;

    @Test
    public void testInsertAndGet() {
        List<TrancheCategorie> initiaux = trancheCategorieRepository.liste();
        Assert.assertEquals(0, initiaux.size(), "Il ne devrait y avoir aucun trancheCategorie en base à l'origine");

        TrancheCategorie entite = new TrancheCategorie();
        entite.setIdPeriode(123L);
        entite.setNumCategorieQuittancement("xyz");
        entite.setIndividu("cleIndividu");
        entite.setNumTranche(3);
        entite.setAuditUtilisateurCreation("TEST");

        trancheCategorieRepository.create(entite);

        TrancheCategorie cree = trancheCategorieRepository.get(1L);
        Assert.assertNotNull(cree);

        Assert.assertEquals(cree.getIdPeriode(), entite.getIdPeriode());
        Assert.assertEquals(cree.getNumCategorieQuittancement(), entite.getNumCategorieQuittancement());
        Assert.assertEquals(cree.getIndividu(), entite.getIndividu());
        Assert.assertEquals(cree.getNumTranche(), entite.getNumTranche());

    }

}
