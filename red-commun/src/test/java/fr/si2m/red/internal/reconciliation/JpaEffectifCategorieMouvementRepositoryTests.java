package fr.si2m.red.internal.reconciliation;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTransactionalTestNGSpringContextTests;
import org.testng.Assert;
import org.testng.annotations.Test;

import fr.si2m.red.reconciliation.EffectifCategorieMouvement;

@Test
@ActiveProfiles("test")
@ContextConfiguration(locations = { "classpath:fr/si2m/red/config.xml" })
public class JpaEffectifCategorieMouvementRepositoryTests extends AbstractTransactionalTestNGSpringContextTests {

    @Autowired
    private JpaEffectifCategorieMouvementRepository effectifCategorieMouvementRepository;

    @Test
    public void testInsertAndGet() {
        List<EffectifCategorieMouvement> initiaux = effectifCategorieMouvementRepository.liste();
        Assert.assertEquals(0, initiaux.size(), "Il ne devrait y avoir aucun effectifCategorieMouvement en base à l'origine");

        EffectifCategorieMouvement entite = new EffectifCategorieMouvement();
        entite.setIdPeriode(123L);
        entite.setNumCategorieQuittancement("abc");
        entite.setDateEffetMouvement(20151102);
        entite.setAuditUtilisateurCreation("TEST");

        effectifCategorieMouvementRepository.create(entite);

        EffectifCategorieMouvement cree = effectifCategorieMouvementRepository.get(1L);
        Assert.assertNotNull(cree);

        Assert.assertEquals(cree.getIdPeriode(), entite.getIdPeriode());
        Assert.assertEquals(cree.getNumCategorieQuittancement(), entite.getNumCategorieQuittancement());
        Assert.assertEquals(cree.getDateEffetMouvement(), entite.getDateEffetMouvement());

    }

}
