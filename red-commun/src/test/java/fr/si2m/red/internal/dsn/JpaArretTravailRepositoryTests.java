package fr.si2m.red.internal.dsn;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTransactionalTestNGSpringContextTests;
import org.testng.Assert;
import org.testng.annotations.Test;

import fr.si2m.red.dsn.ArretTravail;
import fr.si2m.red.dsn.ArretTravailId;

@Test
@ActiveProfiles("test")
@ContextConfiguration(locations = { "classpath:fr/si2m/red/config.xml" })
public class JpaArretTravailRepositoryTests extends AbstractTransactionalTestNGSpringContextTests {

    @Autowired
    private JpaArretTravailRepository arretTravailRepository;

    @Test
    public void testInsertAndGet() {
        List<ArretTravail> initiaux = arretTravailRepository.liste();
        Assert.assertEquals(0, initiaux.size(), "Il ne devrait y avoir aucun arretTravail en base à l'origine");

        ArretTravail entite = new ArretTravail();
        entite.setIdArretTravail("cleArretTravail");
        entite.setAuditUtilisateurCreation("TESTEUR");

        arretTravailRepository.importeEnMasseEntitesTemporaires(Arrays.asList(entite));

        ArretTravailId id = new ArretTravailId();
        id.setIdArretTravail(entite.getIdArretTravail());

        ArretTravail cree = arretTravailRepository.get(id);
        Assert.assertNull(cree, "Le arretTravail devrait être créé dans l'espace temporaire pour le moment");

        arretTravailRepository.promeutEntitesTemporaires();

        cree = arretTravailRepository.get(id);
        Assert.assertNotNull(cree, "Le arretTravail devrait être promu");

        Assert.assertNotNull(cree);
        Assert.assertEquals(cree.getIdArretTravail(), entite.getIdArretTravail());
        Assert.assertFalse(cree.isLigneEnCoursImportBatch(), "Le arretTravail ne devrait plus être dans l'espace temporaire");

    }

}
