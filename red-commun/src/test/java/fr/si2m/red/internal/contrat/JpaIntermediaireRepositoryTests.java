package fr.si2m.red.internal.contrat;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTransactionalTestNGSpringContextTests;
import org.testng.Assert;
import org.testng.annotations.Test;

import fr.si2m.red.contrat.Client;
import fr.si2m.red.contrat.Intermediaire;
import fr.si2m.red.contrat.IntermediaireId;
import fr.si2m.red.internal.parametrage.JpaParamCategorieEffectifsRepository;
import fr.si2m.red.internal.parametrage.JpaParamEtatContratRepository;
import fr.si2m.red.internal.parametrage.JpaParamNatureBaseRepository;
import fr.si2m.red.parametrage.ParamCategorieEffectifs;
import fr.si2m.red.parametrage.ParamEtatContrat;
import fr.si2m.red.parametrage.ParamNatureBase;

@Test
@DirtiesContext(classMode = ClassMode.AFTER_EACH_TEST_METHOD)
@ActiveProfiles("test")
@ContextConfiguration(locations = { "classpath:fr/si2m/red/config.xml" })
public class JpaIntermediaireRepositoryTests extends AbstractTransactionalTestNGSpringContextTests {

    @Autowired
    private JpaIntermediaireRepository intermediaireRepository;
    @Autowired
    private JpaParamEtatContratRepository paramEtatContratRepository;
    @Autowired
    private JpaParamNatureBaseRepository paramNatureBaseRepository;
    @Autowired
    private JpaParamCategorieEffectifsRepository paramCategoriesEffectifsRepository;
    @Autowired
    private JpaClientRepository clientRepository;

    @Test
    public void testInsertAndGet() {
        List<Intermediaire> initialement = intermediaireRepository.liste();
        Assert.assertEquals(0, initialement.size(), "Il ne devrait y avoir aucun intermédiaire en base à l'origine");

        Intermediaire entite = new Intermediaire();
        entite.setAuditUtilisateurCreation("TESTEUR");
        entite.setCodeIntermediaire("123A");
        entite.setTypeIntermediaire(2);
        entite.setNumCompteClient(27);
        entite.setDepartement("AB");
        entite.setCodeEtat(2);
        entite.setRaisonSociale("Les joyeux testeurs");
        entite.setNomExploitant("CGI");
        entite.setAdresseBatiment("A");
        entite.setAdresseRue("rue de la Tombissoire");
        entite.setAdresseCommune("Saint-Ser");
        entite.setAdresseCodePostal("69007");
        entite.setAdresseVille("Saint-Ser en Fouille-Tourte");
        entite.setCodePays("FRA");
        entite.setNumSiren("123456789");
        entite.setCodeFiliale("25");
        entite.setTelephone("0699887766");
        entite.setFax("0499887766");
        entite.setReseauDistribution("CL");
        entite.setNumInspecteur("34");
        entite.setDateDerniereModification(20150801);

        intermediaireRepository.importeEnMasseEntitesTemporaires(Arrays.asList(entite));

        IntermediaireId id = new IntermediaireId();
        id.setCodeIntermediaire(entite.getCodeIntermediaire());

        Intermediaire cree = intermediaireRepository.get(id);
        Assert.assertNull(cree, "L'intermédiaire devrait être créé dans l'espace temporaire pour le moment");

        intermediaireRepository.promeutEntitesTemporaires();
        intermediaireRepository.nettoieEntitesTemporaires();

        cree = intermediaireRepository.get(id);
        Assert.assertNotNull(cree, "L'intermédiaire devrait être promu");

        Assert.assertEquals(entite.getCodeIntermediaire(), cree.getCodeIntermediaire());
        Assert.assertEquals(entite.getTypeIntermediaire(), cree.getTypeIntermediaire());
        Assert.assertEquals(entite.getNumCompteClient(), cree.getNumCompteClient());
        Assert.assertEquals(entite.getDepartement(), cree.getDepartement());
        Assert.assertEquals(entite.getCodeEtat(), cree.getCodeEtat());
        Assert.assertEquals(entite.getRaisonSociale(), cree.getRaisonSociale());
        Assert.assertEquals(entite.getNomExploitant(), cree.getNomExploitant());
        Assert.assertEquals(entite.getAdresseBatiment(), cree.getAdresseBatiment());
        Assert.assertEquals(entite.getAdresseRue(), cree.getAdresseRue());
        Assert.assertEquals(entite.getAdresseCommune(), cree.getAdresseCommune());
        Assert.assertEquals(entite.getAdresseCodePostal(), cree.getAdresseCodePostal());
        Assert.assertEquals(entite.getAdresseVille(), cree.getAdresseVille());
        Assert.assertEquals(entite.getCodePays(), cree.getCodePays());
        Assert.assertEquals(entite.getNumSiren(), cree.getNumSiren());
        Assert.assertEquals(entite.getCodeFiliale(), cree.getCodeFiliale());
        Assert.assertEquals(entite.getTelephone(), cree.getTelephone());
        Assert.assertEquals(entite.getFax(), cree.getFax());
        Assert.assertEquals(entite.getReseauDistribution(), cree.getReseauDistribution());
        Assert.assertEquals(entite.getNumInspecteur(), cree.getNumInspecteur());
        Assert.assertEquals(entite.getDateDerniereModification(), cree.getDateDerniereModification());

    }

    @Test
    public void testExisteUnCoetaco() {
        // Insertion
        List<ParamEtatContrat> etatsContrats = paramEtatContratRepository.liste();
        Assert.assertEquals(0, etatsContrats.size(), "Il ne devrait y avoir aucun état contrat en base à l'origine");

        ParamEtatContrat etatContrat1 = new ParamEtatContrat();
        etatContrat1.setAuditUtilisateurCreation("TESTEUR");
        etatContrat1.setEtatContrat(12);
        etatContrat1.setActif(true);

        ParamEtatContrat etatContrat2 = new ParamEtatContrat();
        etatContrat2.setAuditUtilisateurCreation("TESTEUR");
        etatContrat2.setEtatContrat(2);
        etatContrat2.setActif(false);

        paramEtatContratRepository.importeEnMasseEntitesTemporaires(Arrays.asList(etatContrat1, etatContrat2));
        paramEtatContratRepository.promeutEntitesTemporaires();
        paramEtatContratRepository.nettoieEntitesTemporaires();

        // vérification de l'insertion
        List<ParamEtatContrat> listeEtatsContrats = paramEtatContratRepository.liste();
        Assert.assertEquals(listeEtatsContrats.size(), 2, "Les états n'ont pas été persistés en base");

        // test de la méthode de recherche
        Assert.assertEquals(paramEtatContratRepository.existeParamEtatContrat(12), true,
                "Il n'y a pas de ParamEtatContrat avec le champ COETACO à 12");
    }

    @Test
    public void testExisteUnConbcot() {
        // Insertion
        List<ParamNatureBase> initialement = paramNatureBaseRepository.liste();
        Assert.assertEquals(0, initialement.size(), "Il ne devrait y avoir aucune gestion en base à l'origine");

        ParamNatureBase param = new ParamNatureBase();
        param.setAuditUtilisateurCreation("TESTEUR");
        param.setCodeNatureBaseCotisations(12);
        param.setTauxCalculRempli(true);
        param.setNumTranche(4);
        param.setLibelleTranche("Check");
        param.setTauxUtilise(null);
        param.setMontantUtilise("MTCUNCU");
        param.setComplementDesignation("Shaka");

        ParamNatureBase param2 = new ParamNatureBase();
        param2.setAuditUtilisateurCreation("TESTEUR");
        param2.setCodeNatureBaseCotisations(25);
        param2.setTauxCalculRempli(true);
        param2.setNumTranche(5);
        param2.setLibelleTranche("Vérif");
        param2.setTauxUtilise(null);
        param2.setMontantUtilise("MTCUNCU");
        param2.setComplementDesignation("Zulu");

        paramNatureBaseRepository.importeEnMasseEntitesTemporaires(Arrays.asList(param, param2));
        paramNatureBaseRepository.promeutEntitesTemporaires();
        paramNatureBaseRepository.nettoieEntitesTemporaires();

        // vérification de l'insertion
        List<ParamNatureBase> listeNatureBases = paramNatureBaseRepository.liste();
        Assert.assertEquals(listeNatureBases.size(), 2, "Les Natures de base n'ont pas été persistées en base");

        // test de la méthode de recherche
        Assert.assertEquals(paramNatureBaseRepository.existeUnParametragePourNatureBaseCotisations(25), true,
                "Il n'y a pas de ParamEtatContrat avec le champ Conbcot à 25");
    }

    @Test
    public void testExisteUnNocatLicat() {
        List<ParamCategorieEffectifs> initialement = paramCategoriesEffectifsRepository.liste();
        Assert.assertEquals(0, initialement.size(), "Il ne devrait y avoir aucun libellé en base à l'origine");

        // Insertion
        ParamCategorieEffectifs param1 = new ParamCategorieEffectifs();
        param1.setAuditUtilisateurCreation("TESTEUR");
        param1.setNumCategorie("34");
        param1.setLibelleCategorie("ABC");
        param1.setPrioriteAffilies(9);
        param1.setPrioriteAyantsDroitAdultes(8);
        param1.setPrioriteAyantsDroitEnfants(5);
        param1.setPrioriteAyantsDroitAutres(2);
        param1.setSeuilAyantsDroitAdultes(12);
        param1.setSeuilAyantsDroitEnfants(45);
        param1.setSeuilAyantsDroitAutres(21);

        ParamCategorieEffectifs param2 = new ParamCategorieEffectifs();
        param2.setAuditUtilisateurCreation("TESTEUR");
        param2.setNumCategorie("35");
        param2.setLibelleCategorie("DEF");
        param2.setPrioriteAffilies(9);
        param2.setPrioriteAyantsDroitAdultes(8);
        param2.setPrioriteAyantsDroitEnfants(5);
        param2.setPrioriteAyantsDroitAutres(2);
        param2.setSeuilAyantsDroitAdultes(12);
        param2.setSeuilAyantsDroitEnfants(45);
        param2.setSeuilAyantsDroitAutres(21);

        paramCategoriesEffectifsRepository.importeEnMasseEntitesTemporaires(Arrays.asList(param1, param2));
        paramCategoriesEffectifsRepository.promeutEntitesTemporaires();
        paramCategoriesEffectifsRepository.nettoieEntitesTemporaires();

        // vérification de l'insertion
        List<ParamCategorieEffectifs> listeCategoriesEffectifs = paramCategoriesEffectifsRepository.liste();
        Assert.assertEquals(listeCategoriesEffectifs.size(), 2, "Les Categories Effectifs n'ont pas été persistées en base");

        // test de la méthode de recherche
        Assert.assertEquals(paramCategoriesEffectifsRepository.existeUnParamCategorieEffectifs("34", "ABC"), true,
                "Il n'y a pas de ParamCategoriesEffectifs avec les champs Nocat à 34 et Licat à ABC");
    }

    @Test
    public void testExisteUnNcprod() {
        // Insertion
        List<Intermediaire> initialement = intermediaireRepository.liste();
        Assert.assertEquals(0, initialement.size(), "Il ne devrait y avoir aucun intermédiaire en base à l'origine");

        Intermediaire entite = new Intermediaire();
        entite.setAuditUtilisateurCreation("TESTEUR");
        entite.setCodeIntermediaire("123A");
        entite.setTypeIntermediaire(2);
        entite.setNumCompteClient(27);
        entite.setDepartement("AB");
        entite.setCodeEtat(2);
        entite.setRaisonSociale("Les joyeux testeurs");
        entite.setNomExploitant("CGI");
        entite.setAdresseBatiment("A");
        entite.setAdresseRue("rue de la Tombissoire");
        entite.setAdresseCommune("Saint-Ser");
        entite.setAdresseCodePostal("69007");
        entite.setAdresseVille("Saint-Ser en Fouille-Tourte");
        entite.setCodePays("FRA");
        entite.setNumSiren("123456789");
        entite.setCodeFiliale("25");
        entite.setTelephone("0699887766");
        entite.setFax("0499887766");
        entite.setReseauDistribution("CL");
        entite.setNumInspecteur("34");
        entite.setDateDerniereModification(20150801);

        Intermediaire entite2 = new Intermediaire();
        entite2.setAuditUtilisateurCreation("TESTEUR");
        entite2.setCodeIntermediaire("456B");
        entite2.setTypeIntermediaire(2);
        entite2.setNumCompteClient(27);
        entite2.setDepartement("AB");
        entite2.setCodeEtat(2);
        entite2.setRaisonSociale("Les joyeux testeurs");
        entite2.setNomExploitant("CGI");
        entite2.setAdresseBatiment("A");
        entite2.setAdresseRue("rue de la Tombissoire");
        entite2.setAdresseCommune("Saint-Ser");
        entite2.setAdresseCodePostal("69007");
        entite2.setAdresseVille("Saint-Ser en Fouille-Tourte");
        entite2.setCodePays("FRA");
        entite2.setNumSiren("123456789");
        entite2.setCodeFiliale("25");
        entite2.setTelephone("0699887766");
        entite2.setFax("0499887766");
        entite2.setReseauDistribution("CL");
        entite2.setNumInspecteur("34");
        entite2.setDateDerniereModification(20150801);

        intermediaireRepository.importeEnMasseEntitesTemporaires(Arrays.asList(entite, entite2));
        intermediaireRepository.promeutEntitesTemporaires();
        intermediaireRepository.nettoieEntitesTemporaires();

        // vérification de l'insertion
        List<Intermediaire> listeIntermediaires = intermediaireRepository.liste();
        Assert.assertEquals(listeIntermediaires.size(), 2, "Les Natures de base n'ont pas été persistées en base");

        // test de la méthode de recherche
        Assert.assertEquals(intermediaireRepository.existeUnNcprod("456B"), true, "Il n'y a pas d'Intermediaire avec le champ Ncprod à 456B");
    }

    @Test
    public void testExisteUnNocli() {
        // Insertion
        List<Client> initialement = clientRepository.liste();
        Assert.assertEquals(0, initialement.size(), "Il ne devrait y avoir aucun intermédiaire en base à l'origine");

        Client entite = new Client();
        entite.setAuditUtilisateurCreation("TESTEUR");
        entite.setNumClient(123456L);
        entite.setTypeClient(1);
        entite.setRaisonSociale("CGI");
        entite.setAdresseBatiment("1A");
        entite.setAdresseRue("rue des Nanettes");
        entite.setAdresseCommune("Paris");
        entite.setAdresseCodePostal("75011");
        entite.setBureauDistributeur("CA");
        entite.setCodePays("FRA");
        entite.setCodeNaf("AE");
        entite.setNumSiren("123456789");
        entite.setNumSiret("12345");
        entite.setTelephone("0755223344");
        entite.setFax("0455223344");
        entite.setEtatClient("7");
        entite.setDateEtat(20150101);
        entite.setCodePartenaire(3);
        entite.setNumClientMaitre(null);
        entite.setCodeCcn("2356");
        entite.setCodeGrp("33");
        entite.setCodeTns("2");
        entite.setCodeAutoEntrepreneur("2");
        entite.setDateDerniereModification(20140306);

        Client entite2 = new Client();
        entite2.setAuditUtilisateurCreation("TESTEUR");
        entite2.setNumClient(7890L);
        entite2.setTypeClient(2);
        entite2.setRaisonSociale("CGI");
        entite2.setAdresseBatiment("1A");
        entite2.setAdresseRue("rue des Nanettes");
        entite2.setAdresseCommune("Paris");
        entite2.setAdresseCodePostal("75011");
        entite2.setBureauDistributeur("CA");
        entite2.setCodePays("FRA");
        entite2.setCodeNaf("AE");
        entite2.setNumSiren("123456789");
        entite2.setNumSiret("12345");
        entite2.setTelephone("0755223344");
        entite2.setFax("0455223344");
        entite2.setEtatClient("7");
        entite2.setDateEtat(20150101);
        entite2.setCodePartenaire(3);
        entite2.setNumClientMaitre(null);
        entite2.setCodeCcn("2356");
        entite2.setCodeGrp("33");
        entite2.setCodeTns("2");
        entite2.setCodeAutoEntrepreneur("2");
        entite2.setDateDerniereModification(20140306);

        clientRepository.importeEnMasseEntitesTemporaires(Arrays.asList(entite, entite2));

        Assert.assertEquals(clientRepository.existeUnClientTemporaire(7890L), true, "Il n'y a pas de client avec le champ NOCLI à 7890");

        clientRepository.promeutEntitesTemporaires();
        clientRepository.nettoieEntitesTemporaires();

        // vérification de l'insertion
        List<Client> listeClients = clientRepository.liste();
        Assert.assertEquals(listeClients.size(), 2, "Les Natures de base n'ont pas été persistées en base");

    }

}
