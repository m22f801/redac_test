package fr.si2m.red.internal.complement;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTransactionalTestNGSpringContextTests;
import org.testng.Assert;
import org.testng.annotations.Test;

import fr.si2m.red.complement.ExtensionContrat;

@Test
@DirtiesContext
@ActiveProfiles("test")
@ContextConfiguration(locations = { "classpath:fr/si2m/red/config.xml" })
public class JpaExtensionContratRepositoryTests extends AbstractTransactionalTestNGSpringContextTests {

    @Autowired
    private JpaExtensionContratRepository extensionContratRepository;

    @Test
    public void testInsertAndGet() {
        List<ExtensionContrat> initiaux = extensionContratRepository.liste();
        Assert.assertEquals(0, initiaux.size());

        ExtensionContrat entite = new ExtensionContrat();
        entite.setAuditUtilisateurCreation("TESTEUR");
        entite.setNumContrat("007");
        entite.setDateDerniereModificationIndicateurs(20150505);
        entite.setGestionnaireDerniereModificationIndicateurs("Tony Stark");
        entite.setDateEffetIndicateurs(20150602);
        entite.setMotifChangementIndicateurs("Pour les tests");
        entite.setExploitationDSNAsText("NON");
        entite.setEditionConsignesPaiementAsText("OUI");
        entite.setTransfertAsText("NON");
        entite.setSeuilVariationAlertesEnNbEtablissements(10);
        entite.setSeuilVariationAlertesEnPourcentageNbEtablissements(22);
        entite.setSeuilVariationAlertesEnNbSalaries(100);
        entite.setSeuilVariationAlertesEnPourcentageNbSalaries(30);
        entite.setControleTypeBaseAssujettie("RED");
        entite.setModeNatureContrat("L");
        entite.setVipAsText("O");
        entite.setModeReaffectationCategorieEffectifs("REACA");

        extensionContratRepository.importeEnMasseEntitesTemporaires(Arrays.asList(entite));

        ExtensionContrat cree = extensionContratRepository.get(1L);
        Assert.assertNotNull(cree);

        Assert.assertEquals(cree.getNumContrat(), entite.getNumContrat());
        Assert.assertEquals(cree.getDateDerniereModificationIndicateurs(), entite.getDateDerniereModificationIndicateurs());
        Assert.assertEquals(cree.getGestionnaireDerniereModificationIndicateurs(), entite.getGestionnaireDerniereModificationIndicateurs());
        Assert.assertEquals(cree.getDateEffetIndicateurs(), entite.getDateEffetIndicateurs());
        Assert.assertEquals(cree.getMotifChangementIndicateurs(), entite.getMotifChangementIndicateurs());
        Assert.assertEquals(cree.getExploitationDSNAsText(), entite.getExploitationDSNAsText());
        Assert.assertEquals(cree.isExploitationDSN(), entite.isExploitationDSN());
        Assert.assertEquals(cree.getEditionConsignesPaiementAsText(), entite.getEditionConsignesPaiementAsText());
        Assert.assertEquals(cree.isEditionConsignesPaiement(), entite.isEditionConsignesPaiement());
        Assert.assertEquals(cree.getTransfertAsText(), entite.getTransfertAsText());
        Assert.assertEquals(cree.isTransfert(), entite.isTransfert());
        Assert.assertEquals(cree.getSeuilVariationAlertesEnNbEtablissements(), entite.getSeuilVariationAlertesEnNbEtablissements());
        Assert.assertEquals(cree.getSeuilVariationAlertesEnPourcentageNbEtablissements(),
                cree.getSeuilVariationAlertesEnPourcentageNbEtablissements());
        Assert.assertEquals(cree.getSeuilVariationAlertesEnNbSalaries(), entite.getSeuilVariationAlertesEnNbSalaries());
        Assert.assertEquals(cree.getSeuilVariationAlertesEnPourcentageNbSalaries(), entite.getSeuilVariationAlertesEnPourcentageNbSalaries());
        Assert.assertEquals(cree.getControleTypeBaseAssujettie(), entite.getControleTypeBaseAssujettie());
        Assert.assertEquals(cree.getModeNatureContrat(), entite.getModeNatureContrat());
        Assert.assertEquals(cree.getVipAsText(), entite.getVipAsText());
        Assert.assertEquals(cree.isVip(), entite.isVip());
        Assert.assertEquals(cree.getModeReaffectationCategorieEffectifs(), entite.getModeReaffectationCategorieEffectifs());

    }

}
