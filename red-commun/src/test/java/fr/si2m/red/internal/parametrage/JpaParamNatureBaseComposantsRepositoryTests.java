package fr.si2m.red.internal.parametrage;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTransactionalTestNGSpringContextTests;
import org.testng.Assert;
import org.testng.annotations.Test;

import fr.si2m.red.parametrage.ParamNatureBaseComposants;
import fr.si2m.red.parametrage.ParamNatureBaseComposantsId;

@Test
@DirtiesContext
@ActiveProfiles("test")
@ContextConfiguration(locations = { "classpath:fr/si2m/red/config.xml" })
public class JpaParamNatureBaseComposantsRepositoryTests extends AbstractTransactionalTestNGSpringContextTests {

    @Autowired
    private JpaParamNatureBaseComposantsRepository paramNatureBaseComposantsRepository;

    @Test
    public void testInsertAndGet() {
        List<ParamNatureBaseComposants> initialement = paramNatureBaseComposantsRepository.liste();
        Assert.assertEquals(0, initialement.size(), "Il ne devrait y avoir aucune gestion en base à l'origine");

        ParamNatureBaseComposants param = new ParamNatureBaseComposants();
        param.setAuditUtilisateurCreation("TESTEUR");
        param.setCodeNatureBaseCotisations(12);
        param.setNumTranche(4);
        param.setTauxCalculRempli(true);
        param.setNumTypeComposantBase(2);
        param.setModeConsolidation("S");

        paramNatureBaseComposantsRepository.importeEnMasseEntitesTemporaires(Arrays.asList(param));
        paramNatureBaseComposantsRepository.promeutEntitesTemporaires();

        ParamNatureBaseComposantsId id = new ParamNatureBaseComposantsId();
        id.setCodeNatureBaseCotisations(param.getCodeNatureBaseCotisations());
        id.setNumTranche(param.getNumTranche());
        id.setTauxCalculRempliAsText(param.getTauxCalculRempliAsText());
        id.setNumTypeComposantBase(param.getNumTypeComposantBase());

        ParamNatureBaseComposants cree = paramNatureBaseComposantsRepository.get(id);

        Assert.assertNotNull(cree);
        Assert.assertEquals(cree.getCodeNatureBaseCotisations(), param.getCodeNatureBaseCotisations());
        Assert.assertEquals(cree.getNumTranche(), param.getNumTranche());
        Assert.assertEquals(cree.getTauxCalculRempliAsText(), param.getTauxCalculRempliAsText());
        Assert.assertEquals(cree.isTauxCalculRempli(), param.isTauxCalculRempli());
        Assert.assertEquals(cree.getNumTypeComposantBase(), param.getNumTypeComposantBase());
        Assert.assertEquals(cree.getModeConsolidation(), param.getModeConsolidation());
    }

}
