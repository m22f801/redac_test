package fr.si2m.red.internal.parametrage;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTransactionalTestNGSpringContextTests;
import org.testng.Assert;
import org.testng.annotations.Test;

import fr.si2m.red.parametrage.ParamUtilisateurContratVIP;
import fr.si2m.red.parametrage.ParamUtilisateurContratVIPId;

@Test
@DirtiesContext
@ActiveProfiles("test")
@ContextConfiguration(locations = { "classpath:fr/si2m/red/config.xml" })
public class JpaParamUtilisateurContratsVIPTests extends AbstractTransactionalTestNGSpringContextTests {

    @Autowired
    private JpaParamUtilisateurContratVIPRepository paramUtilisateurContratVIPRepository;

    @Test
    public void testInsertAndGet() {
        List<ParamUtilisateurContratVIP> initialement = paramUtilisateurContratVIPRepository.liste();
        Assert.assertEquals(0, initialement.size(), "Il ne devrait y avoir aucune gestion en base à l'origine");

        ParamUtilisateurContratVIP utilisateurVIP = new ParamUtilisateurContratVIP();
        utilisateurVIP.setAuditUtilisateurCreation("user1");
        utilisateurVIP.setCodeUtilisateur("codetest");
        utilisateurVIP.setNumContrat("000025369878942");

        paramUtilisateurContratVIPRepository.importeEnMasseEntitesTemporaires(Arrays.asList(utilisateurVIP));
        paramUtilisateurContratVIPRepository.promeutEntitesTemporaires();

        ParamUtilisateurContratVIPId id = new ParamUtilisateurContratVIPId();
        id.setCodeUtilisateur(utilisateurVIP.getCodeUtilisateur());
        id.setNumContrat(utilisateurVIP.getNumContrat());

        ParamUtilisateurContratVIP cree = paramUtilisateurContratVIPRepository.get(id);

        Assert.assertNotNull(cree);
        Assert.assertEquals(cree.getCodeUtilisateur(), utilisateurVIP.getCodeUtilisateur());
        Assert.assertEquals(cree.getNumContrat(), utilisateurVIP.getNumContrat());
    }
}
