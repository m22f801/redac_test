package fr.si2m.red.internal.reconciliation;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTransactionalTestNGSpringContextTests;
import org.testng.Assert;
import org.testng.annotations.Test;

import fr.si2m.red.reconciliation.PeriodeRecue;

@Test
@ActiveProfiles("test")
@ContextConfiguration(locations = { "classpath:fr/si2m/red/config.xml" })
public class JpaPeriodeRecueRepositoryTests extends AbstractTransactionalTestNGSpringContextTests {

    @Autowired
    private JpaPeriodeRecueRepository periodeRecueRepository;

    @Test
    public void testInsertAndGet() {
        List<PeriodeRecue> initiaux = periodeRecueRepository.liste();
        Assert.assertEquals(0, initiaux.size(), "Il ne devrait y avoir aucun periodeRecue en base à l'origine");

        PeriodeRecue entite = new PeriodeRecue();
        entite.setAuditUtilisateurCreation("TEST");

        periodeRecueRepository.create(entite);

        PeriodeRecue cree = periodeRecueRepository.get(1L);
        Assert.assertNotNull(cree);

        Assert.assertEquals(cree.getIdPeriode(), entite.getIdPeriode());

    }

}
