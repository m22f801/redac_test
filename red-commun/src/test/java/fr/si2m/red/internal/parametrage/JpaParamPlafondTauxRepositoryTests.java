package fr.si2m.red.internal.parametrage;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTransactionalTestNGSpringContextTests;
import org.testng.Assert;
import org.testng.annotations.Test;

import fr.si2m.red.parametrage.ParamPlafondTaux;
import fr.si2m.red.parametrage.ParamPlafondTauxId;

@Test
@DirtiesContext
@ActiveProfiles("test")
@ContextConfiguration(locations = { "classpath:fr/si2m/red/config.xml" })
public class JpaParamPlafondTauxRepositoryTests extends AbstractTransactionalTestNGSpringContextTests {

    @Autowired
    private JpaParamPlafondTauxRepository paramPlafondTauxRepository;

    @Test
    public void testInsertAndGet() {
        List<ParamPlafondTaux> initialement = paramPlafondTauxRepository.liste();
        Assert.assertEquals(0, initialement.size(), "Il ne devrait y avoir aucune gestion en base à l'origine");

        ParamPlafondTaux plafonds = new ParamPlafondTaux();
        plafonds.setAuditUtilisateurCreation("TESTEUR");
        plafonds.setDebut(20150101);
        plafonds.setFin(20150503);
        plafonds.setPlafondMensuelSecuriteSociale(15.58);

        paramPlafondTauxRepository.importeEnMasseEntitesTemporaires(Arrays.asList(plafonds));
        paramPlafondTauxRepository.promeutEntitesTemporaires();

        ParamPlafondTauxId id = new ParamPlafondTauxId();
        id.setDebut(plafonds.getDebut());

        ParamPlafondTaux cree = paramPlafondTauxRepository.get(id);

        Assert.assertNotNull(cree);
        Assert.assertEquals(cree.getDebut(), plafonds.getDebut());
        Assert.assertEquals(cree.getFin(), plafonds.getFin());
        Assert.assertEquals(cree.getPlafondMensuelSecuriteSociale(), plafonds.getPlafondMensuelSecuriteSociale());
    }

}
