package fr.si2m.red.internal.parametrage;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTransactionalTestNGSpringContextTests;
import org.testng.Assert;
import org.testng.annotations.Test;

import fr.si2m.red.parametrage.ParamCategorieEffectifs;
import fr.si2m.red.parametrage.ParamCategorieEffectifsId;

@Test
@DirtiesContext
@ActiveProfiles("test")
@ContextConfiguration(locations = { "classpath:fr/si2m/red/config.xml" })
public class JpaParamCategorieEffectifsRepositoryTests extends AbstractTransactionalTestNGSpringContextTests {

    @Autowired
    private JpaParamCategorieEffectifsRepository paramCategoriesEffectifsRepository;

    @Test
    public void testInsertAndGetUnLibelle() {
        List<ParamCategorieEffectifs> initialement = paramCategoriesEffectifsRepository.liste();
        Assert.assertEquals(0, initialement.size(), "Il ne devrait y avoir aucun libellé en base à l'origine");

        ParamCategorieEffectifs param = new ParamCategorieEffectifs();
        param.setAuditUtilisateurCreation("TESTEUR");
        param.setNumCategorie("34");
        param.setLibelleCategorie("ABC");
        param.setPrioriteAffilies(9);
        param.setPrioriteAyantsDroitAdultes(8);
        param.setPrioriteAyantsDroitEnfants(5);
        param.setPrioriteAyantsDroitAutres(2);
        param.setSeuilAyantsDroitAdultes(12);
        param.setSeuilAyantsDroitEnfants(45);
        param.setSeuilAyantsDroitAutres(21);

        paramCategoriesEffectifsRepository.importeEnMasseEntitesTemporaires(Arrays.asList(param));
        paramCategoriesEffectifsRepository.promeutEntitesTemporaires();

        ParamCategorieEffectifsId id = new ParamCategorieEffectifsId();
        id.setNumCategorie(param.getNumCategorie());
        id.setLibelleCategorie(param.getLibelleCategorie());
        ParamCategorieEffectifs cree = paramCategoriesEffectifsRepository.get(id);

        Assert.assertNotNull(cree);
        Assert.assertEquals(cree.getNumCategorie(), param.getNumCategorie());
        Assert.assertEquals(cree.getLibelleCategorie(), param.getLibelleCategorie());
        Assert.assertEquals(cree.getPrioriteAffilies(), param.getPrioriteAffilies());
        Assert.assertEquals(cree.getPrioriteAyantsDroitAdultes(), param.getPrioriteAyantsDroitAdultes());
        Assert.assertEquals(cree.getPrioriteAyantsDroitEnfants(), param.getPrioriteAyantsDroitEnfants());
        Assert.assertEquals(cree.getPrioriteAyantsDroitAutres(), param.getPrioriteAyantsDroitAutres());
        Assert.assertEquals(cree.getSeuilAyantsDroitAdultes(), param.getSeuilAyantsDroitAdultes());
        Assert.assertEquals(cree.getSeuilAyantsDroitEnfants(), param.getSeuilAyantsDroitEnfants());
        Assert.assertEquals(cree.getSeuilAyantsDroitAutres(), param.getSeuilAyantsDroitAutres());
    }
}
