package fr.si2m.red.internal.dsn;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTransactionalTestNGSpringContextTests;
import org.testng.Assert;
import org.testng.annotations.Test;

import fr.si2m.red.dsn.VersementIndividu;
import fr.si2m.red.dsn.VersementIndividuId;

@Test
@ActiveProfiles("test")
@ContextConfiguration(locations = { "classpath:fr/si2m/red/config.xml" })
public class JpaVersementIndividuRepositoryTests extends AbstractTransactionalTestNGSpringContextTests {

    @Autowired
    private JpaVersementIndividuRepository versementIndividuRepository;

    @Test
    public void testInsertAndGet() {
        List<VersementIndividu> initiaux = versementIndividuRepository.liste();
        Assert.assertEquals(0, initiaux.size(), "Il ne devrait y avoir aucun versementIndividu en base à l'origine");

        VersementIndividu entite = new VersementIndividu();
        entite.setIdVersementIndividu("cleVersementIndividu");
        entite.setRemunerationNette(36500.25d);
        entite.setAuditUtilisateurCreation("TESTEUR");

        versementIndividuRepository.importeEnMasseEntitesTemporaires(Arrays.asList(entite));

        VersementIndividuId id = new VersementIndividuId();
        id.setIdVersementIndividu(entite.getIdVersementIndividu());

        VersementIndividu cree = versementIndividuRepository.get(id);
        Assert.assertNull(cree, "Le versementIndividu devrait être créé dans l'espace temporaire pour le moment");

        versementIndividuRepository.promeutEntitesTemporaires();

        cree = versementIndividuRepository.get(id);
        Assert.assertNotNull(cree, "Le versementIndividu devrait être promu");

        Assert.assertNotNull(cree);
        Assert.assertEquals(cree.getIdVersementIndividu(), entite.getIdVersementIndividu());
        Assert.assertEquals(cree.getRemunerationNette(), entite.getRemunerationNette());
        Assert.assertFalse(cree.isLigneEnCoursImportBatch(), "Le versementIndividu ne devrait plus être dans l'espace temporaire");

    }

}
