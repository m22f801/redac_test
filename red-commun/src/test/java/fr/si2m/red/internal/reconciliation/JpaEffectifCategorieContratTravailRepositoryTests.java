package fr.si2m.red.internal.reconciliation;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTransactionalTestNGSpringContextTests;
import org.testng.Assert;
import org.testng.annotations.Test;

import fr.si2m.red.reconciliation.EffectifCategorieContratTravail;

@Test
@ActiveProfiles("test")
@ContextConfiguration(locations = { "classpath:fr/si2m/red/config.xml" })
public class JpaEffectifCategorieContratTravailRepositoryTests extends AbstractTransactionalTestNGSpringContextTests {

    @Autowired
    private JpaEffectifCategorieContratTravailRepository effectifCategorieContratTravailRepository;

    @Test
    public void testInsertAndGet() {
        List<EffectifCategorieContratTravail> initiaux = effectifCategorieContratTravailRepository.liste();
        Assert.assertEquals(0, initiaux.size(), "Il ne devrait y avoir aucun effectifCategorieContratTravail en base à l'origine");

        EffectifCategorieContratTravail entite = new EffectifCategorieContratTravail();
        entite.setIdPeriode(123L);
        entite.setMoisRattachement(2);
        entite.setIdContratTravail("cleContratTravail");
        entite.setNumCategorieQuittancement("abc");
        entite.setDateDebutBase(20151102);
        entite.setAuditUtilisateurCreation("TEST");

        effectifCategorieContratTravailRepository.create(entite);

        EffectifCategorieContratTravail cree = effectifCategorieContratTravailRepository.get(1L);
        Assert.assertNotNull(cree);

        Assert.assertEquals(cree.getIdPeriode(), entite.getIdPeriode());
        Assert.assertEquals(cree.getMoisRattachement(), entite.getMoisRattachement());
        Assert.assertEquals(cree.getIdContratTravail(), entite.getIdContratTravail());
        Assert.assertEquals(cree.getNumCategorieQuittancement(), entite.getNumCategorieQuittancement());
        Assert.assertEquals(cree.getDateDebutBase(), entite.getDateDebutBase());

    }

}
