package fr.si2m.red.internal.dsn;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTransactionalTestNGSpringContextTests;
import org.testng.Assert;
import org.testng.annotations.Test;

import fr.si2m.red.dsn.ChangementIndividu;
import fr.si2m.red.dsn.ChangementIndividuId;

@Test
@ActiveProfiles("test")
@ContextConfiguration(locations = { "classpath:fr/si2m/red/config.xml" })
public class JpaChangementIndividuRepositoryTests extends AbstractTransactionalTestNGSpringContextTests {

    @Autowired
    private JpaChangementIndividuRepository changementIndividuRepository;

    @Test
    public void testInsertAndGet() {
        List<ChangementIndividu> initiaux = changementIndividuRepository.liste();
        Assert.assertEquals(0, initiaux.size(), "Il ne devrait y avoir aucun changementIndividu en base à l'origine");

        ChangementIndividu entite = new ChangementIndividu();
        entite.setIdChangementIndividu("cleChangementIndividu");
        entite.setAuditUtilisateurCreation("TESTEUR");

        changementIndividuRepository.importeEnMasseEntitesTemporaires(Arrays.asList(entite));

        ChangementIndividuId id = new ChangementIndividuId();
        id.setIdChangementIndividu(entite.getIdChangementIndividu());

        ChangementIndividu cree = changementIndividuRepository.get(id);
        Assert.assertNull(cree, "Le changementIndividu devrait être créé dans l'espace temporaire pour le moment");

        changementIndividuRepository.promeutEntitesTemporaires();

        cree = changementIndividuRepository.get(id);
        Assert.assertNotNull(cree, "Le changementIndividu devrait être promu");

        Assert.assertNotNull(cree);
        Assert.assertEquals(cree.getIdChangementIndividu(), entite.getIdChangementIndividu());
        Assert.assertFalse(cree.isLigneEnCoursImportBatch(), "Le changementIndividu ne devrait plus être dans l'espace temporaire");

    }

}
