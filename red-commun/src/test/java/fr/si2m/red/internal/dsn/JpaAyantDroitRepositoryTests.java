package fr.si2m.red.internal.dsn;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTransactionalTestNGSpringContextTests;
import org.testng.Assert;
import org.testng.annotations.Test;

import fr.si2m.red.dsn.AyantDroit;
import fr.si2m.red.dsn.AyantDroitId;

@Test
@ActiveProfiles("test")
@ContextConfiguration(locations = { "classpath:fr/si2m/red/config.xml" })
public class JpaAyantDroitRepositoryTests extends AbstractTransactionalTestNGSpringContextTests {

    @Autowired
    private JpaAyantDroitRepository ayantDroitRepository;

    @Test
    public void testInsertAndGet() {
        List<AyantDroit> initiaux = ayantDroitRepository.liste();
        Assert.assertEquals(0, initiaux.size(), "Il ne devrait y avoir aucun ayantDroit en base à l'origine");

        AyantDroit entite = new AyantDroit();
        entite.setIdAyantDroit("cleAyantDroit");
        entite.setAuditUtilisateurCreation("TESTEUR");

        ayantDroitRepository.importeEnMasseEntitesTemporaires(Arrays.asList(entite));

        AyantDroitId id = new AyantDroitId();
        id.setIdAyantDroit(entite.getIdAyantDroit());

        AyantDroit cree = ayantDroitRepository.get(id);
        Assert.assertNull(cree, "Le ayantDroit devrait être créé dans l'espace temporaire pour le moment");

        ayantDroitRepository.promeutEntitesTemporaires();

        cree = ayantDroitRepository.get(id);
        Assert.assertNotNull(cree, "Le ayantDroit devrait être promu");

        Assert.assertNotNull(cree);
        Assert.assertEquals(cree.getIdAyantDroit(), entite.getIdAyantDroit());
        Assert.assertFalse(cree.isLigneEnCoursImportBatch(), "Le ayantDroit ne devrait plus être dans l'espace temporaire");

    }

}
