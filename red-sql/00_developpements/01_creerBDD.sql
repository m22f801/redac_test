-- ATTENTION : Ce script n'est pas un livrable et est simplement utilisé par CGI pour monter des BDD sur des postes de développement.

-- Création de la base de données
-- pour être conforme aux env client, on en précise pas l'encodage de la BDD (param par defaut).

-- CREATE DATABASE red DEFAULT CHARACTER SET latin1 COLLATE latin1_general_ci;

CREATE DATABASE red;

