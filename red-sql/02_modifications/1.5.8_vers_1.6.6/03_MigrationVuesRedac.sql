-- TM2749

-- Alimentation CategorieQuittancementIndividu - salaires
DROP VIEW IF EXISTS V_Categorie_quittancement_salaires_a_inserer;
CREATE VIEW V_Categorie_quittancement_salaires_a_inserer AS SELECT 
        `p`.`ID_PERIODE` AS `ID_PERIODE`,
        `t`.`NOCAT` AS `NOCAT`,
        (CASE
            WHEN (`i`.`IDENTIFIANT_REPERTOIRE` = '') THEN `i`.`NTT`
            ELSE `i`.`IDENTIFIANT_REPERTOIRE`
        END) AS `INDIVIDU`,
        `t`.`LICAT` AS `LICAT`,
        `t`.`CONBCOT` AS `CONBCOT`,
        COALESCE(CALCULMONTANTSINDIVIDUS(`t`.`NOCAT`,
                        `p`.`ID_PERIODE`,
                        `i`.`IDENTIFIANT_REPERTOIRE`,
                        `i`.`NTT`),
                0) AS `MONTANT_COTISATION`,
        NULL AS `CHAMP_NULL_1`,
        NULL AS `CHAMP_NULL_2`
    FROM
        (((((`TARIFS` `t`
        JOIN `PERIODES_RECUES` `p` ON ((`t`.`NOCO` = `p`.`NUMERO_CONTRAT`)))
        JOIN `RATTACHEMENT_DECLARATIONS_RECUES` `r` ON ((`r`.`ID_PERIODE` = `p`.`ID_PERIODE`)))
        JOIN `INDIVIDU` `i` ON ((`i`.`ID_ADH_ETAB_MOIS` = `r`.`ID_ADH_ETAB_MOIS`)))
        JOIN `V_Selection_periode_mocalcot_salaires` `s` ON (((`s`.`ID_PERIODE` = `p`.`ID_PERIODE`)
            AND (`s`.`TYPE_CONSO` = 'INDIV'))))
        LEFT JOIN `V_Categorie_quittancement_salaires_montants` `montant` ON (((CASE
            WHEN (`i`.`IDENTIFIANT_REPERTOIRE` = '') THEN `i`.`NTT`
            ELSE `i`.`IDENTIFIANT_REPERTOIRE`
        END) = `montant`.`identifiant`)))
    WHERE
        ((`t`.`NOCO` = `p`.`NUMERO_CONTRAT`)
            AND (`t`.`DT_DEBUT_SIT` <= `p`.`DATE_DEBUT_PERIODE`)
            AND ((COALESCE(`t`.`DT_FIN_SIT`, 99999999) >= `p`.`DATE_DEBUT_PERIODE`)
            OR ((CASE
            WHEN (`t`.`DT_FIN_SIT` = 0) THEN 99999999
            ELSE `t`.`DT_FIN_SIT`
        END) >= `p`.`DATE_DEBUT_PERIODE`))
            AND CONCAT(`t`.`NOCAT`, `p`.`ID_PERIODE`) IN (SELECT DISTINCT
                CONCAT(`t`.`NOCAT`, `p`.`ID_PERIODE`)
            FROM
                (`TARIFS` `t`
                JOIN `PERIODES_RECUES` `p` ON ((`t`.`NOCO` = `p`.`NUMERO_CONTRAT`)))
            WHERE
                ((`t`.`NOCO` = `p`.`NUMERO_CONTRAT`)
                    AND (`t`.`DT_DEBUT_SIT` <= `p`.`DATE_DEBUT_PERIODE`)
                    AND ((COALESCE(`t`.`DT_FIN_SIT`, 99999999) >= `p`.`DATE_DEBUT_PERIODE`)
                    OR ((CASE
                    WHEN (`t`.`DT_FIN_SIT` = 0) THEN 99999999
                    ELSE `t`.`DT_FIN_SIT`
                END) >= `p`.`DATE_DEBUT_PERIODE`))))
            AND CONCAT(`i`.`IDENTIFIANT_REPERTOIRE`, `i`.`NTT`) IN (SELECT DISTINCT
                CONCAT(`i`.`IDENTIFIANT_REPERTOIRE`, `i`.`NTT`)
            FROM
                (`INDIVIDU` `i`
                LEFT JOIN `ADHESION_ETABLISSEMENT_MOIS` `adh` ON ((`adh`.`ID_ADH_ETAB_MOIS` = `i`.`ID_ADH_ETAB_MOIS`)))
            WHERE
                ((`adh`.`TMP_BATCH` IS FALSE)
                    AND `adh`.`ID_ADH_ETAB_MOIS` IN (SELECT 
                        `r`.`ID_ADH_ETAB_MOIS`
                    FROM
                        `RATTACHEMENT_DECLARATIONS_RECUES` `r`
                    WHERE
                        (`r`.`ID_PERIODE` = `p`.`ID_PERIODE`)))))
    UNION SELECT 
        p.ID_PERIODE AS ID_PERIODE,
        t.NOCAT AS NOCAT,
        'TOUS' AS INDIVIDU,
        t.LICAT AS LICAT,
        t.CONBCOT AS CONBCOT,
        COALESCE((SELECT 
                        SUM(ba.MONTANT_COTISATION)
                    FROM
                        BASE_ASSUJETTIE ba
                    WHERE
                        ba.ID_BASE_ASSUJETTIE IN (SELECT 
                                tcba.ID_BASE_ASSUJETTIE
                            FROM
                                TRANCHE_CATEGORIE_BASE_ASSUJETTIE tcba
                            WHERE
                                ((tcba.ID_PERIODE = p.ID_PERIODE)
                                    AND (tcba.NOCAT = t.NOCAT)))),
                0) AS MONTANT_COTISATION,
        NULL AS CHAMP_NULL_1,
        NULL AS CHAMP_NULL_2
    FROM
        ((TARIFS t
        JOIN PERIODES_RECUES p ON ((t.NOCO = p.NUMERO_CONTRAT)))
        JOIN V_Selection_periode_mocalcot_salaires s ON (((s.ID_PERIODE = p.ID_PERIODE)
            AND (s.TYPE_CONSO = 'TOUS'))))
    WHERE
    ((`t`.`NOCO` = `p`.`NUMERO_CONTRAT`)
                    AND (`t`.`DT_DEBUT_SIT` <= `p`.`DATE_DEBUT_PERIODE`)
                    AND ((COALESCE(`t`.`DT_FIN_SIT`, 99999999) >= `p`.`DATE_DEBUT_PERIODE`)
                    OR ((CASE
                    WHEN (`t`.`DT_FIN_SIT` = 0) THEN 99999999
                    ELSE `t`.`DT_FIN_SIT`
                END) >= `p`.`DATE_DEBUT_PERIODE`))       
        AND CONCAT(t.NOCAT,p.ID_PERIODE) IN (SELECT DISTINCT
                CONCAT(t.NOCAT, p.ID_PERIODE)
            FROM
                (TARIFS t
                JOIN PERIODES_RECUES p ON ((t.NOCO = p.NUMERO_CONTRAT)))
            WHERE
                ((`t`.`NOCO` = `p`.`NUMERO_CONTRAT`)
                    AND (`t`.`DT_DEBUT_SIT` <= `p`.`DATE_DEBUT_PERIODE`)
                    AND ((COALESCE(`t`.`DT_FIN_SIT`, 99999999) >= `p`.`DATE_DEBUT_PERIODE`)
                    OR ((CASE
                    WHEN (`t`.`DT_FIN_SIT` = 0) THEN 99999999
                    ELSE `t`.`DT_FIN_SIT`
                END) >= `p`.`DATE_DEBUT_PERIODE`))
                    AND (p.ID_PERIODE = s.ID_PERIODE))));
                    
-- TM2749 - Alimentation TrancheCategorieBaseAssujetie nocatUnique - salaires
DROP VIEW IF EXISTS V_Selection_base_assujettie;
CREATE VIEW V_Selection_base_assujettie AS SELECT 
    `ba`.`ID_BASE_ASSUJETTIE` AS `ID_BASE_ASSUJETTIE`,
    `ba`.`ID_AFFILIATION` AS `ID_AFFILIATION`,
    `ba`.`DATE_DEB_RATTACHEMENT` AS `DATE_DEB_RATTACHEMENT`,
    `ba`.`DATE_FIN_RATTACHEMENT` AS `DATE_FIN_RATTACHEMENT`,
    `ba`.`TMP_BATCH` AS `TMP_BATCH`,
    `s`.`ID_PERIODE` AS `id_periode`,
    `s`.`NUMERO_CONTRAT` AS `numero_contrat`,
    `s`.`TYPE_PERIODE` AS `TYPE_PERIODE`,
    `s`.`DATE_CREATION` AS `DATE_CREATION`,
    `s`.`DATE_DEBUT_PERIODE` AS `DATE_DEBUT_PERIODE`,
    `s`.`DATE_FIN_PERIODE` AS `DATE_FIN_PERIODE`,
    `adh`.`MOIS_RATTACHEMENT` AS `mois_rattachement`,
    COALESCE(IF((`a`.`CODE_POPULATION` = ''),
                NULL,
                `a`.`CODE_POPULATION`),
            (SELECT 
                    `t`.`NOCAT`
                FROM
                    `TARIFS` `t`
                WHERE
                    ((`t`.`NOCO` = `s`.`NUMERO_CONTRAT`)
                        AND (`t`.`DT_DEBUT_SIT` <= `s`.`DATE_DEBUT_PERIODE`)
                        AND ((COALESCE(`t`.`DT_FIN_SIT`, 99999999) >= `s`.`DATE_DEBUT_PERIODE`)
                        OR ((CASE
                        WHEN (`t`.`DT_FIN_SIT` = 0) THEN 99999999
                        ELSE `t`.`DT_FIN_SIT`
                    END) >= `s`.`DATE_DEBUT_PERIODE`)))
                ORDER BY `t`.`DT_DEBUT_SIT`
                LIMIT 1)) AS `NOCAT`
FROM
    ((((((`BASE_ASSUJETTIE` `ba`
    LEFT JOIN `AFFILIATION` `a` ON ((`ba`.`ID_AFFILIATION` = `a`.`ID_AFFILIATION`)))
    LEFT JOIN `CONTRAT_TRAVAIL` `ct` ON ((`a`.`ID_CONTRAT_TRAVAIL` = `ct`.`ID_CONTRAT_TRAVAIL`)))
    LEFT JOIN `INDIVIDU` `i` ON ((`ct`.`ID_INDIVIDU` = `i`.`ID_INDIVIDU`)))
    LEFT JOIN `ADHESION_ETABLISSEMENT_MOIS` `adh` ON ((`i`.`ID_ADH_ETAB_MOIS` = `adh`.`ID_ADH_ETAB_MOIS`)))
    JOIN `RATTACHEMENT_DECLARATIONS_RECUES` `r` ON ((`adh`.`ID_ADH_ETAB_MOIS` = `r`.`ID_ADH_ETAB_MOIS`)))
    JOIN `V_Selection_periode_mocalcot_salaires` `s` ON ((`s`.`ID_PERIODE` = `r`.`ID_PERIODE`)))
WHERE
    ((`ba`.`MONTANT_COTISATION` <> 0)
        AND (`adh`.`TMP_BATCH` IS FALSE));
        
    
-- TM2752
DROP VIEW IF EXISTS V_Trace_tranche_categorie_base_assujettie;
CREATE VIEW V_Trace_tranche_categorie_base_assujettie AS SELECT DISTINCT
        `sel`.`id_periode` AS `ID_PERIODE`,
        `sel`.`numero_contrat` AS `NUMERO_CONTRAT`,
        `sel`.`DATE_DEB_RATTACHEMENT` AS `DATE_DEB_RATTACHEMENT`,
        `sel`.`DATE_FIN_RATTACHEMENT` AS `DATE_FIN_RATTACHEMENT`,
        `sel`.`NOCAT` AS `NOCAT`,
        CONCAT('La situation tarifaire [numContrat=',
                `sel`.`numero_contrat`,
                ', numCategorie=',
                `sel`.`NOCAT`,
                ', date=',
                `sel`.`DATE_FIN_RATTACHEMENT`,
                '] n\'existe pas. Periode traitee : [PeriodeRecue(idPeriode=',
                `sel`.`id_periode`,
                ', typePeriode=',
                `sel`.`TYPE_PERIODE`,
                ', dateDebutPeriode=',
                `sel`.`DATE_DEBUT_PERIODE`,
                ', dateFinPeriode=',
                `sel`.`DATE_FIN_PERIODE`,
                ', dateCreation=',
                `sel`.`DATE_CREATION`,
                ')]') AS `MESSAGE`
    FROM
        (`V_Selection_base_assujettie` `sel`
        LEFT JOIN `TARIFS` `t` ON (((`sel`.`numero_contrat` = `t`.`NOCO`)
            AND (`sel`.`NOCAT` = `t`.`NOCAT`)
            AND (`t`.`DT_DEBUT_SIT` <= `sel`.`DATE_DEB_RATTACHEMENT`)
            AND ((COALESCE(`t`.`DT_FIN_SIT`, 99999999) >= `sel`.`DATE_DEB_RATTACHEMENT`)
            OR ((CASE
            WHEN (`t`.`DT_FIN_SIT` = 0) THEN 99999999
            ELSE `t`.`DT_FIN_SIT`
        END) >= `sel`.`DATE_DEB_RATTACHEMENT`)))))
    WHERE
        (ISNULL(`t`.`NOCO`)
            OR ISNULL(`t`.`NOCAT`)) 
    UNION SELECT DISTINCT
        `sel`.`id_periode` AS `ID_PERIODE`,
        `sel`.`numero_contrat` AS `NUMERO_CONTRAT`,
        `sel`.`DATE_DEB_RATTACHEMENT` AS `DATE_DEB_RATTACHEMENT`,
        `sel`.`DATE_FIN_RATTACHEMENT` AS `DATE_FIN_RATTACHEMENT`,
        `sel`.`NOCAT` AS `NOCAT`,
        (CONCAT(CONVERT( CONCAT('La nature base [CONBCOT=',
                        (CASE
                            WHEN ISNULL(`t`.`CONBCOT`) THEN 'NULL'
                            ELSE `t`.`CONBCOT`
                        END),
                        ', TXCALCU_REMPLI=',
                        (CASE
                            WHEN (COALESCE(`t`.`TXCALCU`, 0) > 0) THEN 'O'
                            ELSE 'N'
                        END),
                        '] n\'existe pas. Situation tarifaire : numContrat=') USING LATIN1),
                `sel`.`numero_contrat`,
                ', numCategorie=',
                `sel`.`NOCAT`,
                ', date=',
                `sel`.`DATE_FIN_RATTACHEMENT`,
                '. Periode traitee : [PeriodeRecue(idPeriode=',
                `sel`.`id_periode`,
                ', typePeriode=',
                `sel`.`TYPE_PERIODE`,
                ', dateDebutPeriode=',
                `sel`.`DATE_DEBUT_PERIODE`,
                ', dateFinPeriode=',
                `sel`.`DATE_FIN_PERIODE`,
                ', dateCreation=',
                `sel`.`DATE_CREATION`,
                ')]') COLLATE latin1_general_ci) AS `trace`
    FROM
        ((`V_Selection_base_assujettie` `sel`
        JOIN `TARIFS` `t` ON (((`sel`.`numero_contrat` = `t`.`NOCO`)
            AND (`sel`.`NOCAT` = `t`.`NOCAT`)
            AND (`t`.`DT_DEBUT_SIT` <= `sel`.`DATE_DEB_RATTACHEMENT`)
            AND ((COALESCE(`t`.`DT_FIN_SIT`, 99999999) >= `sel`.`DATE_DEB_RATTACHEMENT`)
            OR ((CASE
            WHEN (`t`.`DT_FIN_SIT` = 0) THEN 99999999
            ELSE `t`.`DT_FIN_SIT`
        END) >= `sel`.`DATE_DEB_RATTACHEMENT`)))))
        LEFT JOIN `PARAM_NATURE_BASE` `p` ON (((`t`.`CONBCOT` = `p`.`CONBCOT`)
            AND (CONVERT( (CASE
            WHEN (COALESCE(`t`.`TXCALCU`, 0) > 0) THEN 'O'
            ELSE 'N'
        END) USING LATIN1) = (`p`.`TXCALCU_REMPLI` COLLATE latin1_general_ci)))))
    WHERE
        ISNULL(`p`.`NUM_TRANCHE`);
        
        
-- TM 2749 Alimentation CQI effectif
DROP VIEW IF EXISTS V_Distinct_noco_nocat_pour_contrat_et_plage;
CREATE VIEW `V_Distinct_noco_nocat_pour_contrat_et_plage` AS
    SELECT DISTINCT
        `s`.`ID_PERIODE` AS `ID_PERIODE`,
        `t`.`NOCO` AS `NOCO`,
        `t`.`NOCAT` AS `NOCAT`
    FROM
        (`TARIFS` `t`
        JOIN `V_Selection_periode_mocalcot_effectifs` `s` ON ((`t`.`NOCO` = `s`.`NUMERO_CONTRAT`)))
    WHERE
        ((`t`.`DT_DEBUT_SIT` <= `s`.`DATE_DEBUT_PERIODE`)
            AND ((COALESCE(`t`.`DT_FIN_SIT`, 99999999) >= `s`.`DATE_DEBUT_PERIODE`)
            OR ((CASE
            WHEN (`t`.`DT_FIN_SIT` = 0) THEN 99999999
            ELSE `t`.`DT_FIN_SIT`
        END) >= `s`.`DATE_DEBUT_PERIODE`)));

-- TM2750
DROP VIEW IF EXISTS V_Selection_categorie_quittancement_effectif_data;
CREATE VIEW `V_Selection_categorie_quittancement_effectif_data` AS
   SELECT 
        `d`.`ID_PERIODE` AS `ID_PERIODE`,
        `d`.`NOCAT` AS `NOCAT`,
        `e`.`DEBUT_BASE` AS `DEBUT_BASE`,
        `e`.`FIN_BASE` AS `FIN_BASE`,
        (CASE WHEN `e`.`PRORATA_COTISATION` IS NULL THEN 0 ELSE `e`.`PRORATA_COTISATION` END)  AS `PRORATA_COTISATION`,
        `e`.`TOTAL_EFFECTIF` AS `TOTAL_EFFECTIF`,
        (CASE
            WHEN (`e`.`DEBUT_BASE` = `s`.`DATE_DEBUT_PERIODE`) THEN `e`.`TOTAL_EFFECTIF`
            ELSE 0
        END) AS `effectif_debut`,
        (CASE
            WHEN (`e`.`FIN_BASE` = `s`.`DATE_FIN_PERIODE`) THEN `e`.`TOTAL_EFFECTIF`
            ELSE 0
        END) AS `effectif_fin`,
        `s`.`DATE_DEBUT_PERIODE` AS `DATE_DEBUT_PERIODE`,
        `s`.`DATE_FIN_PERIODE` AS `DATE_FIN_PERIODE`, 
        `t`.`CONBCOT` AS `CONBCOT`,
        `t`.`LICAT` AS `LICAT`
    FROM
        (((`EFFECTIF_CATEGORIE_CONTRAT_TRAVAIL` `e`
        RIGHT JOIN `V_Distinct_noco_nocat_pour_contrat_et_plage` `d` ON (((`d`.`ID_PERIODE` = `e`.`ID_PERIODE`)
            AND (`d`.`NOCAT` = `e`.`NOCAT`))))
        JOIN `V_Selection_periode_mocalcot_effectifs` `s` ON ((`s`.`ID_PERIODE` = `d`.`ID_PERIODE`)))
        JOIN `TARIFS` `t` ON (((`t`.`NOCAT` = `d`.`NOCAT`)
            AND (`t`.`NOCO` = `d`.`NOCO`)
            AND (`t`.`DT_DEBUT_SIT` <= `s`.`DATE_DEBUT_PERIODE`)
            AND ((COALESCE(`t`.`DT_FIN_SIT`, 99999999) >= `s`.`DATE_DEBUT_PERIODE`)
            OR ((CASE
            WHEN (`t`.`DT_FIN_SIT` = 0) THEN 99999999
            ELSE `t`.`DT_FIN_SIT`
        END) >= `s`.`DATE_DEBUT_PERIODE`)))));


DROP VIEW IF EXISTS V_Selection_categorie_quittancement_effectif;
CREATE VIEW `V_Selection_categorie_quittancement_effectif` AS
    SELECT 
        `ecct`.`ID_PERIODE` AS `ID_PERIODE`,
        `ecct`.`NOCAT` AS `NOCAT`,
        'TOUS' AS `INDIVIDU`,
        `ecct`.`LICAT` AS `LICAT`,
        `ecct`.`CONBCOT` AS `CONBCOT`,
        SUM(`ecct`.`PRORATA_COTISATION`) AS `COTISATION`,
        SUM(`ecct`.`effectif_debut`) AS `EFFECTIF_DEBUT`,
        SUM(`ecct`.`effectif_fin`) AS `EFFECTIF_FIN`
    FROM
        `V_Selection_categorie_quittancement_effectif_data` `ecct`
    GROUP BY `ecct`.`ID_PERIODE` , `ecct`.`NOCAT`;
    
        
-- TM 2749-2752 mise en coherence 405

DROP VIEW IF EXISTS V_Selection_periode_mocalcot_effectifs;
CREATE VIEW V_Selection_periode_mocalcot_effectifs AS
    SELECT 
        p.ID_PERIODE AS ID_PERIODE,
        p.TYPE_PERIODE AS TYPE_PERIODE,
        p.DATE_CREATION AS DATE_CREATION,
        p.NUMERO_CONTRAT AS NUMERO_CONTRAT,
        p.DATE_DEBUT_PERIODE AS DATE_DEBUT_PERIODE,
        p.DATE_FIN_PERIODE AS DATE_FIN_PERIODE,
        p.PREMIER_MOIS_DECL AS PREMIER_MOIS_DECL,
        p.ETAT_PERIODE AS ETAT_PERIODE,
        p.RECONSOLIDER AS RECONSOLIDER,
        p.DATE_ECHEANCE AS DATE_ECHEANCE
    FROM
        (PERIODES_RECUES p
        JOIN TARIFS t)
    WHERE
        ((p.RECONSOLIDER = 'O')
            AND (t.NOCO = p.NUMERO_CONTRAT)
            AND (t.TMP_BATCH IS FALSE)
            AND ((t.DT_DEBUT_SIT , t.NOCAT) = (SELECT 
                tf.DT_DEBUT_SIT, tf.NOCAT
            FROM
                TARIFS tf
            WHERE
                ((tf.NOCO = p.NUMERO_CONTRAT)
                    AND (tf.DT_DEBUT_SIT <= p.DATE_DEBUT_PERIODE)
                    AND ((COALESCE(tf.DT_FIN_SIT, 99999999) >= p.DATE_DEBUT_PERIODE)
                    OR ((CASE
                    WHEN (tf.DT_FIN_SIT = 0) THEN 99999999
                    ELSE tf.DT_FIN_SIT
                END) >= p.DATE_DEBUT_PERIODE)))
            LIMIT 1))
            AND (t.MOCALCOT = 1));

DROP VIEW IF EXISTS V_Selection_periode_mocalcot_salaires;
CREATE VIEW V_Selection_periode_mocalcot_salaires AS
    SELECT 
        p.ID_PERIODE AS ID_PERIODE,
        p.TYPE_PERIODE AS TYPE_PERIODE,
        p.DATE_CREATION AS DATE_CREATION,
        p.NUMERO_CONTRAT AS NUMERO_CONTRAT,
        p.DATE_DEBUT_PERIODE AS DATE_DEBUT_PERIODE,
        p.DATE_FIN_PERIODE AS DATE_FIN_PERIODE,
        p.PREMIER_MOIS_DECL AS PREMIER_MOIS_DECL,
        p.ETAT_PERIODE AS ETAT_PERIODE,
        p.RECONSOLIDER AS RECONSOLIDER,
        p.DATE_ECHEANCE AS DATE_ECHEANCE,
        s.TYPE_CONSO_SALAIRE AS TYPE_CONSO
    FROM
        ((PERIODES_RECUES p
        JOIN TARIFS t)
        JOIN V_Selection_type_consolidation s)
    WHERE
        ((p.RECONSOLIDER = 'O')
            AND (t.NOCO = p.NUMERO_CONTRAT)
            AND (t.TMP_BATCH IS FALSE)
            AND ((t.DT_DEBUT_SIT , t.NOCAT) = (SELECT 
                tf.DT_DEBUT_SIT, tf.NOCAT
            FROM
                TARIFS tf
            WHERE
                ((tf.NOCO = p.NUMERO_CONTRAT)
                    AND (tf.DT_DEBUT_SIT <= p.DATE_DEBUT_PERIODE)
                    AND ((COALESCE(tf.DT_FIN_SIT, 99999999) >= p.DATE_DEBUT_PERIODE)
                    OR ((CASE
                    WHEN (tf.DT_FIN_SIT = 0) THEN 99999999
                    ELSE tf.DT_FIN_SIT
                END) >= p.DATE_DEBUT_PERIODE)))
            LIMIT 1))
            AND (t.MOCALCOT = 2)
            AND (s.ID_PERIODE = p.ID_PERIODE));
            
-- Creation view - F04_RG_P5_03 pour contrats sur salaires
-- RED-88 / RED-89
DROP VIEW IF EXISTS V_Trace_Nocat_Inconnu_salaires;
CREATE VIEW V_Trace_Nocat_Inconnu_salaires AS 
SELECT DISTINCT
    vpms.ID_PERIODE,CONCAT('La situation tarifaire [numContrat=',
                vpms.NUMERO_CONTRAT,
                ', numCategorieQuittancement=',
                a.CODE_POPULATION,
                ', dateDebutPeriode=',
                vpms.DATE_DEBUT_PERIODE,
                '] n\'existe pas. Periode traitee : [PeriodeRecue(idPeriode=',
                vpms.ID_PERIODE,
                ', typePeriode=',
                vpms.TYPE_PERIODE,
                ', dateDebutPeriode=',
                vpms.DATE_DEBUT_PERIODE,
                ', dateFinPeriode=',
                vpms.DATE_FIN_PERIODE,
                ', dateCreation=',
                vpms.DATE_CREATION,
                ')]') AS MESSAGE
FROM
	V_Selection_periode_mocalcot_salaires vpms,
    AFFILIATION a
        LEFT JOIN
    CONTRAT_TRAVAIL ct ON a.ID_CONTRAT_TRAVAIL = ct.ID_CONTRAT_TRAVAIL
        LEFT JOIN
    INDIVIDU i ON ct.ID_INDIVIDU = i.ID_INDIVIDU
        LEFT JOIN
    ADHESION_ETABLISSEMENT_MOIS adh ON i.ID_ADH_ETAB_MOIS = adh.ID_ADH_ETAB_MOIS
WHERE
    adh.TMP_BATCH IS FALSE
		AND a.CODE_POPULATION <> ''
        AND adh.ID_ADH_ETAB_MOIS IN (SELECT 
            r.ID_ADH_ETAB_MOIS
        FROM
            RATTACHEMENT_DECLARATIONS_RECUES r
        WHERE
            r.ID_PERIODE = vpms.ID_PERIODE)
        AND a.CODE_POPULATION NOT IN (SELECT DISTINCT
            t.NOCAT
        FROM
            TARIFS t
        WHERE
            (t.NOCO = vpms.NUMERO_CONTRAT)
            AND (t.NOCAT <> '')
                AND (t.DT_DEBUT_SIT <= vpms.DATE_DEBUT_PERIODE)
                AND ((COALESCE(t.DT_FIN_SIT, 99999999) >= vpms.DATE_DEBUT_PERIODE)
                OR ((CASE
                WHEN (t.DT_FIN_SIT = 0) THEN 99999999
                ELSE t.DT_FIN_SIT
            END) >= vpms.DATE_DEBUT_PERIODE)));