DROP PROCEDURE IF EXISTS Recherche_gestionnaire_siren;
DELIMITER $$
CREATE PROCEDURE Recherche_gestionnaire_siren()
BEGIN
-- ####################################################################
-- ####################################################################
-- CAS 1.1 + Cas 2.1 : 1 groupe et 1 gestionnaire existant
-- ####################################################################
-- ####################################################################
select distinct tmp_siren.NOSIREN, pug.code_user, pug.NOM, pug.PRENOM, pug2.NIVEAU_1, pug2.NIVEAU_2, pug2.NIVEAU_3
from  (
		select cl.nosiren, group_concat( distinct co.nmgrpges) gg, group_concat( distinct pr.a_traiter_par) gest
		from 
			(	select distinct contrats.noco as noco, clients.nosiren as nosiren from clients inner join contrats on clients.nocli = contrats.nocli
			union 
				select distinct ext.noco as noco, ext.siren as nosiren from extensions_entreprises_affiliees ext
			) as cl
		inner join contrats co on cl.noco = co.noco and co.DT_FIN_SIT is null and co.tmp_batch IS FALSE AND (co.eligdsn = 'SRN' OR co.eligdsn = 'SRT')
		inner join param_etat_contrat p on p.COETACO = co.coetaco and co.tmp_batch = p.tmp_batch AND p.ACTIF = 'O'
		left join (
					select distinct a_traiter_par, p.numero_contrat from periodes_recues p
					inner join contrats ct on p.NUMERO_CONTRAT = ct.NOCO
					inner join (
						select p1.numero_contrat, tmp1.dt_rec, max(p1.dt_creation) dt_crea 
						from periodes_recues p1
						inner join (
							select numero_contrat, max(date_debut_periode) dt_rec
							from periodes_recues
							group by numero_contrat
						) as tmp1 on tmp1.numero_contrat = p1.numero_contrat and tmp1.dt_rec = p1.DATE_DEBUT_PERIODE
						group by numero_contrat, tmp1.dt_rec
					) as tmp2 on tmp2.numero_contrat = p.numero_contrat and tmp2.dt_rec = p.DATE_DEBUT_PERIODE and tmp2.dt_crea = p.DT_CREATION
				) as pr on pr.numero_contrat = co.noco
		where cl.nosiren not in (select siren from param_siren_faux)
		group by cl.nosiren
		having count( distinct co.nmgrpges) = 1 and count( distinct pr.a_traiter_par) = 1
		) as tmp_siren
inner join param_utilisateur_gestionnaire pug on pug.code_user = tmp_siren.gest and pug.GRP_GEST = tmp_siren.gg
inner join param_utilisateur_gestionnaire pug2 on pug2.TYPE = 'GEST_GU' and pug2.GG_EGAL = tmp_siren.gg
where exists (select 1 from param_utilisateur_gestionnaire pug 
		WHERE pug.TYPE = 'GEST_AUTRE'
		AND pug.GG_INCLUS_1 = '' and pug.GG_INCLUS_2 = '' and pug.GG_EXCLUS <> ''
		AND pug.GG_EXCLUS like concat('%', tmp_siren.gg, '%')
	)

union

select distinct tmp_siren.NOSIREN, concat(pug2.GG_EGAL, pug.code_user), pug.NOM, pug.PRENOM, pug2.NIVEAU_1, pug2.NIVEAU_2, pug2.NIVEAU_3
from  (
		select cl.nosiren, group_concat( distinct co.nmgrpges) gg, group_concat( distinct pr.a_traiter_par) gest
		from 
			(	select distinct contrats.noco as noco, clients.nosiren as nosiren from clients inner join contrats on clients.nocli = contrats.nocli
			union 
				select distinct ext.noco as noco, ext.siren as nosiren from extensions_entreprises_affiliees ext
			) as cl
		inner join contrats co on cl.noco = co.noco and co.DT_FIN_SIT is null and co.tmp_batch IS FALSE AND (co.eligdsn = 'SRN' OR co.eligdsn = 'SRT')
		inner join param_etat_contrat p on p.COETACO = co.coetaco and co.tmp_batch = p.tmp_batch AND p.ACTIF = 'O'
		left join (
					select distinct a_traiter_par, p.numero_contrat from periodes_recues p
					inner join contrats ct on p.NUMERO_CONTRAT = ct.NOCO
					inner join (
						select p1.numero_contrat, tmp1.dt_rec, max(p1.dt_creation) dt_crea 
						from periodes_recues p1
						inner join (
							select numero_contrat, max(date_debut_periode) dt_rec
							from periodes_recues
							group by numero_contrat
						) as tmp1 on tmp1.numero_contrat = p1.numero_contrat and tmp1.dt_rec = p1.DATE_DEBUT_PERIODE
						group by numero_contrat, tmp1.dt_rec
					) as tmp2 on tmp2.numero_contrat = p.numero_contrat and tmp2.dt_rec = p.DATE_DEBUT_PERIODE and tmp2.dt_crea = p.DT_CREATION
				) as pr on pr.numero_contrat = co.noco
		where cl.nosiren not in (select siren from param_siren_faux)
		group by cl.nosiren
		having count( distinct co.nmgrpges) = 1 and count( distinct pr.a_traiter_par) = 1
		) as tmp_siren
inner join param_utilisateur_gestionnaire pug on pug.code_user = tmp_siren.gest and pug.GRP_GEST <> tmp_siren.gg
inner join param_utilisateur_gestionnaire pug2 on pug2.TYPE = 'GEST_GU' and pug2.GG_EGAL = tmp_siren.gg
where exists (select 1 from param_utilisateur_gestionnaire pug 
		WHERE pug.TYPE = 'GEST_AUTRE'
		AND pug.GG_INCLUS_1 = '' and pug.GG_INCLUS_2 = '' and pug.GG_EXCLUS <> ''
		AND pug.GG_EXCLUS like concat('%', tmp_siren.gg, '%')
	)


-- ####################################################################
-- ####################################################################
-- CAS 1.2 + Cas 2.2 : 1 groupe et 1 gestionnaire non existant
-- ####################################################################
-- ####################################################################
union

select distinct tmp_siren.NOSIREN, pug2.code_user, pug2.NOM, pug2.PRENOM, pug2.NIVEAU_1, pug2.NIVEAU_2, pug2.NIVEAU_3
from  (
		select cl.nosiren, group_concat( distinct co.nmgrpges) gg, group_concat( distinct pr.a_traiter_par) gest
		from 
			(	select distinct contrats.noco as noco, clients.nosiren as nosiren from clients inner join contrats on clients.nocli = contrats.nocli
			union 
				select distinct ext.noco as noco, ext.siren as nosiren from extensions_entreprises_affiliees ext
			) as cl
		inner join contrats co on cl.noco = co.noco and co.DT_FIN_SIT is null and co.tmp_batch IS FALSE AND (co.eligdsn = 'SRN' OR co.eligdsn = 'SRT')
		inner join param_etat_contrat p on p.COETACO = co.coetaco and co.tmp_batch = p.tmp_batch AND p.ACTIF = 'O'
		left join (
					select distinct a_traiter_par, p.numero_contrat from periodes_recues p
					inner join contrats ct on p.NUMERO_CONTRAT = ct.NOCO
					inner join (
						select p1.numero_contrat, tmp1.dt_rec, max(p1.dt_creation) dt_crea 
						from periodes_recues p1
						inner join (
							select numero_contrat, max(date_debut_periode) dt_rec
							from periodes_recues
							group by numero_contrat
						) as tmp1 on tmp1.numero_contrat = p1.numero_contrat and tmp1.dt_rec = p1.DATE_DEBUT_PERIODE
						group by numero_contrat, tmp1.dt_rec
					) as tmp2 on tmp2.numero_contrat = p.numero_contrat and tmp2.dt_rec = p.DATE_DEBUT_PERIODE and tmp2.dt_crea = p.DT_CREATION
				) as pr on pr.numero_contrat = co.noco
		where cl.nosiren not in (select siren from param_siren_faux)
		group by cl.nosiren
		having count( distinct co.nmgrpges) = 1 and count( distinct pr.a_traiter_par) = 1
		) as tmp_siren
left join param_utilisateur_gestionnaire pug on pug.code_user = tmp_siren.gest
inner join param_utilisateur_gestionnaire pug2 on pug2.GG_EGAL =  tmp_siren.gg and pug2.`TYPE` = 'AUTRE_GU'
where pug.code_user is null and tmp_siren.gest is not null



-- ####################################################################
-- ####################################################################
-- CAS 1.3 + Cas 2.3 : 1 groupe et 0 gestionnaire assigné
-- ####################################################################
-- ####################################################################
union

select distinct tmp_siren.NOSIREN, pug2.code_user, pug2.NOM, pug2.PRENOM, pug2.NIVEAU_1, pug2.NIVEAU_2, pug2.NIVEAU_3
from  (
		select cl.nosiren, group_concat( distinct co.nmgrpges) gg, group_concat( distinct pr.a_traiter_par) gest
		from 
			(	select distinct contrats.noco as noco, clients.nosiren as nosiren from clients inner join contrats on clients.nocli = contrats.nocli
			union 
				select distinct ext.noco as noco, ext.siren as nosiren from extensions_entreprises_affiliees ext
			) as cl
		inner join contrats co on cl.noco = co.noco and co.DT_FIN_SIT is null and co.tmp_batch IS FALSE AND (co.eligdsn = 'SRN' OR co.eligdsn = 'SRT')
		inner join param_etat_contrat p on p.COETACO = co.coetaco and co.tmp_batch = p.tmp_batch AND p.ACTIF = 'O'
		left join (
					select distinct a_traiter_par, p.numero_contrat from periodes_recues p
					inner join contrats ct on p.NUMERO_CONTRAT = ct.NOCO
					inner join (
						select p1.numero_contrat, tmp1.dt_rec, max(p1.dt_creation) dt_crea 
						from periodes_recues p1
						inner join (
							select numero_contrat, max(date_debut_periode) dt_rec
							from periodes_recues
							group by numero_contrat
						) as tmp1 on tmp1.numero_contrat = p1.numero_contrat and tmp1.dt_rec = p1.DATE_DEBUT_PERIODE
						group by numero_contrat, tmp1.dt_rec
					) as tmp2 on tmp2.numero_contrat = p.numero_contrat and tmp2.dt_rec = p.DATE_DEBUT_PERIODE and tmp2.dt_crea = p.DT_CREATION
				) as pr on pr.numero_contrat = co.noco
		where cl.nosiren not in (select siren from param_siren_faux)
		group by cl.nosiren
		having count( distinct co.nmgrpges) = 1 and count( distinct pr.a_traiter_par) = 0
		) as tmp_siren
inner join param_utilisateur_gestionnaire pug2 on pug2.GG_EGAL =  tmp_siren.gg and pug2.`TYPE` = 'NON_ASS_GU'



-- ####################################################################
-- ####################################################################
-- CAS 2.4 : 1 groupe et plusieurs gestionnaires
-- ####################################################################
-- ####################################################################
union

select distinct tmp_siren.NOSIREN, pug2.code_user, pug2.NOM, pug2.PRENOM, pug2.NIVEAU_1, pug2.NIVEAU_2, pug2.NIVEAU_3
from  (
		select cl.nosiren, group_concat( distinct co.nmgrpges) gg, group_concat( distinct pr.a_traiter_par) gest
		from 
			(	select distinct contrats.noco as noco, clients.nosiren as nosiren from clients inner join contrats on clients.nocli = contrats.nocli
			union 
				select distinct ext.noco as noco, ext.siren as nosiren from extensions_entreprises_affiliees ext
			) as cl
		inner join contrats co on cl.noco = co.noco and co.DT_FIN_SIT is null and co.tmp_batch IS FALSE AND (co.eligdsn = 'SRN' OR co.eligdsn = 'SRT')
		inner join param_etat_contrat p on p.COETACO = co.coetaco and co.tmp_batch = p.tmp_batch AND p.ACTIF = 'O'
		left join (
					select distinct a_traiter_par, p.numero_contrat from periodes_recues p
					inner join contrats ct on p.NUMERO_CONTRAT = ct.NOCO
					inner join (
						select p1.numero_contrat, tmp1.dt_rec, max(p1.dt_creation) dt_crea 
						from periodes_recues p1
						inner join (
							select numero_contrat, max(date_debut_periode) dt_rec
							from periodes_recues
							group by numero_contrat
						) as tmp1 on tmp1.numero_contrat = p1.numero_contrat and tmp1.dt_rec = p1.DATE_DEBUT_PERIODE
						group by numero_contrat, tmp1.dt_rec
					) as tmp2 on tmp2.numero_contrat = p.numero_contrat and tmp2.dt_rec = p.DATE_DEBUT_PERIODE and tmp2.dt_crea = p.DT_CREATION
				) as pr on pr.numero_contrat = co.noco
		where cl.nosiren not in (select siren from param_siren_faux)
		group by cl.nosiren
		having count( distinct co.nmgrpges) = 1 and count( distinct pr.a_traiter_par) > 1
		) as tmp_siren
inner join param_utilisateur_gestionnaire pug2 on pug2.GG_EGAL =  tmp_siren.gg and pug2.`TYPE` = 'MULTI_GU'



-- ####################################################################
-- ####################################################################
-- CAS 3.1 : multi-groupes / mono domaine et 1 gestionnaire existant
-- ####################################################################
-- ####################################################################
union

select distinct tmp_siren.NOSIREN, concat('1',pug.code_user), pug.NOM, pug.PRENOM, pug2.NIVEAU_1, pug2.NIVEAU_2, pug2.NIVEAU_3
from  (
		select cl.nosiren, group_concat(distinct co.nmgrpges order by co.nmgrpges separator '%') gg, group_concat( distinct pr.a_traiter_par) gest
		from 
			(	select distinct contrats.noco as noco, clients.nosiren as nosiren from clients inner join contrats on clients.nocli = contrats.nocli
			union 
				select distinct ext.noco as noco, ext.siren as nosiren from extensions_entreprises_affiliees ext
			) as cl
		inner join contrats co on cl.noco = co.noco and co.DT_FIN_SIT is null and co.tmp_batch IS FALSE AND (co.eligdsn = 'SRN' OR co.eligdsn = 'SRT')
		inner join param_etat_contrat p on p.COETACO = co.coetaco and co.tmp_batch = p.tmp_batch AND p.ACTIF = 'O'
		left join (
					select distinct a_traiter_par, p.numero_contrat from periodes_recues p
					inner join contrats ct on p.NUMERO_CONTRAT = ct.NOCO
					inner join (
						select p1.numero_contrat, tmp1.dt_rec, max(p1.dt_creation) dt_crea 
						from periodes_recues p1
						inner join (
							select numero_contrat, max(date_debut_periode) dt_rec
							from periodes_recues
							group by numero_contrat
						) as tmp1 on tmp1.numero_contrat = p1.numero_contrat and tmp1.dt_rec = p1.DATE_DEBUT_PERIODE
						group by numero_contrat, tmp1.dt_rec
					) as tmp2 on tmp2.numero_contrat = p.numero_contrat and tmp2.dt_rec = p.DATE_DEBUT_PERIODE and tmp2.dt_crea = p.DT_CREATION
				) as pr on pr.numero_contrat = co.noco
		where cl.nosiren not in (select siren from param_siren_faux)
		group by cl.nosiren
		having count( distinct co.nmgrpges) > 1 and count( distinct pr.a_traiter_par) = 1
		) as tmp_siren
inner join param_utilisateur_gestionnaire pug2 on pug2.GG_inclus_1 like concat('%',tmp_siren.gg,'%') and pug2.`TYPE` = 'GEST_MULTI'
inner join param_utilisateur_gestionnaire pug on pug.code_user = tmp_siren.gest 
where pug2.GG_INCLUS_2 = '' and pug2.GG_EXCLUS = ''
group by tmp_siren.NOSIREN



-- ####################################################################
-- ####################################################################
-- CAS 3.2 : multi-groupes / mono domaine et 1 gestionnaire non existant
-- ####################################################################
-- ####################################################################
union

select distinct tmp_siren.NOSIREN, pug2.code_user, pug2.NOM, pug2.PRENOM, pug2.NIVEAU_1, pug2.NIVEAU_2, pug2.NIVEAU_3
from  (
		select cl.nosiren, group_concat(distinct co.nmgrpges order by co.nmgrpges separator '%') gg, group_concat( distinct pr.a_traiter_par) gest
		from 
			(	select distinct contrats.noco as noco, clients.nosiren as nosiren from clients inner join contrats on clients.nocli = contrats.nocli
			union 
				select distinct ext.noco as noco, ext.siren as nosiren from extensions_entreprises_affiliees ext
			) as cl
		inner join contrats co on cl.noco = co.noco and co.DT_FIN_SIT is null and co.tmp_batch IS FALSE AND (co.eligdsn = 'SRN' OR co.eligdsn = 'SRT')
		inner join param_etat_contrat p on p.COETACO = co.coetaco and co.tmp_batch = p.tmp_batch AND p.ACTIF = 'O'
		left join (
					select distinct a_traiter_par, p.numero_contrat from periodes_recues p
					inner join contrats ct on p.NUMERO_CONTRAT = ct.NOCO
					inner join (
						select p1.numero_contrat, tmp1.dt_rec, max(p1.dt_creation) dt_crea 
						from periodes_recues p1
						inner join (
							select numero_contrat, max(date_debut_periode) dt_rec
							from periodes_recues
							group by numero_contrat
						) as tmp1 on tmp1.numero_contrat = p1.numero_contrat and tmp1.dt_rec = p1.DATE_DEBUT_PERIODE
						group by numero_contrat, tmp1.dt_rec
					) as tmp2 on tmp2.numero_contrat = p.numero_contrat and tmp2.dt_rec = p.DATE_DEBUT_PERIODE and tmp2.dt_crea = p.DT_CREATION
				) as pr on pr.numero_contrat = co.noco
		where cl.nosiren not in (select siren from param_siren_faux)
		group by cl.nosiren
		having count( distinct co.nmgrpges) > 1 and count( distinct pr.a_traiter_par) = 1
		) as tmp_siren
inner join param_utilisateur_gestionnaire pug2 on pug2.GG_inclus_1 like concat('%',tmp_siren.gg,'%') and pug2.`TYPE` = 'AUTRE_MULTI'
left join param_utilisateur_gestionnaire pug on pug.code_user = tmp_siren.gest 
where pug2.GG_INCLUS_2 = '' and pug2.GG_EXCLUS = '' 
and pug.code_user is null and tmp_siren.gest is not null
group by tmp_siren.NOSIREN



-- ####################################################################
-- ####################################################################
-- CAS 3.3 : multi-groupes / mono domaine et 0 gestionnaire assigné
-- ####################################################################
-- ####################################################################
union

select distinct tmp_siren.NOSIREN, pug2.code_user, pug2.NOM, pug2.PRENOM, pug2.NIVEAU_1, pug2.NIVEAU_2, pug2.NIVEAU_3
from  (
		select cl.nosiren, group_concat(distinct co.nmgrpges order by co.nmgrpges separator '%') gg, group_concat( distinct pr.a_traiter_par) gest
		from 
			(	select distinct contrats.noco as noco, clients.nosiren as nosiren from clients inner join contrats on clients.nocli = contrats.nocli
			union 
				select distinct ext.noco as noco, ext.siren as nosiren from extensions_entreprises_affiliees ext
			) as cl
		inner join contrats co on cl.noco = co.noco and co.DT_FIN_SIT is null and co.tmp_batch IS FALSE AND (co.eligdsn = 'SRN' OR co.eligdsn = 'SRT')
		inner join param_etat_contrat p on p.COETACO = co.coetaco and co.tmp_batch = p.tmp_batch AND p.ACTIF = 'O'
		left join (
					select distinct a_traiter_par, p.numero_contrat from periodes_recues p
					inner join contrats ct on p.NUMERO_CONTRAT = ct.NOCO
					inner join (
						select p1.numero_contrat, tmp1.dt_rec, max(p1.dt_creation) dt_crea 
						from periodes_recues p1
						inner join (
							select numero_contrat, max(date_debut_periode) dt_rec
							from periodes_recues
							group by numero_contrat
						) as tmp1 on tmp1.numero_contrat = p1.numero_contrat and tmp1.dt_rec = p1.DATE_DEBUT_PERIODE
						group by numero_contrat, tmp1.dt_rec
					) as tmp2 on tmp2.numero_contrat = p.numero_contrat and tmp2.dt_rec = p.DATE_DEBUT_PERIODE and tmp2.dt_crea = p.DT_CREATION
				) as pr on pr.numero_contrat = co.noco
		where cl.nosiren not in (select siren from param_siren_faux)
		group by cl.nosiren
		having count( distinct co.nmgrpges) > 1 and count( distinct pr.a_traiter_par) = 0
		) as tmp_siren
 inner join param_utilisateur_gestionnaire pug2 on pug2.GG_inclus_1 like concat('%',tmp_siren.gg,'%') and pug2.`TYPE` = 'NON_ASS_MULTI'
 where pug2.GG_INCLUS_2 = '' and pug2.GG_EXCLUS = ''
group by tmp_siren.NOSIREN



-- ####################################################################
-- ####################################################################
-- CAS 3.4 : multi-groupes / mono domaine et plusieurs gestionnaires 
-- ####################################################################
-- ####################################################################
union

select distinct tmp_siren.NOSIREN, pug2.code_user, pug2.NOM, pug2.PRENOM, pug2.NIVEAU_1, pug2.NIVEAU_2, pug2.NIVEAU_3
from  (
		select cl.nosiren, group_concat(distinct co.nmgrpges order by co.nmgrpges separator '%') gg, group_concat( distinct pr.a_traiter_par) gest
		from 
			(	select distinct contrats.noco as noco, clients.nosiren as nosiren from clients inner join contrats on clients.nocli = contrats.nocli
			union 
				select distinct ext.noco as noco, ext.siren as nosiren from extensions_entreprises_affiliees ext
			) as cl
		inner join contrats co on cl.noco = co.noco and co.DT_FIN_SIT is null and co.tmp_batch IS FALSE AND (co.eligdsn = 'SRN' OR co.eligdsn = 'SRT')
		inner join param_etat_contrat p on p.COETACO = co.coetaco and co.tmp_batch = p.tmp_batch AND p.ACTIF = 'O'
		left join (
					select distinct a_traiter_par, p.numero_contrat from periodes_recues p
					inner join contrats ct on p.NUMERO_CONTRAT = ct.NOCO
					inner join (
						select p1.numero_contrat, tmp1.dt_rec, max(p1.dt_creation) dt_crea 
						from periodes_recues p1
						inner join (
							select numero_contrat, max(date_debut_periode) dt_rec
							from periodes_recues
							group by numero_contrat
						) as tmp1 on tmp1.numero_contrat = p1.numero_contrat and tmp1.dt_rec = p1.DATE_DEBUT_PERIODE
						group by numero_contrat, tmp1.dt_rec
					) as tmp2 on tmp2.numero_contrat = p.numero_contrat and tmp2.dt_rec = p.DATE_DEBUT_PERIODE and tmp2.dt_crea = p.DT_CREATION
				) as pr on pr.numero_contrat = co.noco
		where cl.nosiren not in (select siren from param_siren_faux)
		group by cl.nosiren
		having count( distinct co.nmgrpges) > 1 and count( distinct pr.a_traiter_par) > 1
		) as tmp_siren
inner join param_utilisateur_gestionnaire pug2 on pug2.GG_inclus_1 like concat('%',tmp_siren.gg,'%') and pug2.`TYPE` = 'MULTI_MULTI'
where pug2.GG_INCLUS_2 = '' and pug2.GG_EXCLUS = ''



-- ####################################################################
-- ####################################################################
-- CAS 4.1 : multi-groupes / 2 domaines et 1 gestionnaire existant
-- ####################################################################
-- ####################################################################
union

select distinct tmp_siren.NOSIREN, concat('2',pug.code_user), pug.NOM, pug.PRENOM, pug2.NIVEAU_1, pug2.NIVEAU_2, pug2.NIVEAU_3
from  (
		select cl.nosiren, group_concat(distinct co.nmgrpges order by co.nmgrpges separator '%') gg, group_concat( distinct pr.a_traiter_par) gest
		from 
			(	select distinct contrats.noco as noco, clients.nosiren as nosiren from clients inner join contrats on clients.nocli = contrats.nocli
			union 
				select distinct ext.noco as noco, ext.siren as nosiren from extensions_entreprises_affiliees ext
			) as cl
		inner join contrats co on cl.noco = co.noco and co.DT_FIN_SIT is null and co.tmp_batch IS FALSE AND (co.eligdsn = 'SRN' OR co.eligdsn = 'SRT')
		inner join param_etat_contrat p on p.COETACO = co.coetaco and co.tmp_batch = p.tmp_batch AND p.ACTIF = 'O'
		left join (
					select distinct a_traiter_par, p.numero_contrat from periodes_recues p
					inner join contrats ct on p.NUMERO_CONTRAT = ct.NOCO
					inner join (
						select p1.numero_contrat, tmp1.dt_rec, max(p1.dt_creation) dt_crea 
						from periodes_recues p1
						inner join (
							select numero_contrat, max(date_debut_periode) dt_rec
							from periodes_recues
							group by numero_contrat
						) as tmp1 on tmp1.numero_contrat = p1.numero_contrat and tmp1.dt_rec = p1.DATE_DEBUT_PERIODE
						group by numero_contrat, tmp1.dt_rec
					) as tmp2 on tmp2.numero_contrat = p.numero_contrat and tmp2.dt_rec = p.DATE_DEBUT_PERIODE and tmp2.dt_crea = p.DT_CREATION
				) as pr on pr.numero_contrat = co.noco
		where cl.nosiren not in (select siren from param_siren_faux)
		group by cl.nosiren
		having count( distinct co.nmgrpges) > 1 and count( distinct pr.a_traiter_par) = 1
		) as tmp_siren
inner join param_utilisateur_gestionnaire pug2 
		on concat(pug2.GG_inclus_1, ',', pug2.GG_inclus_2) like concat('%',tmp_siren.gg,'%') 
		and pug2.GG_inclus_1 not like concat('%',tmp_siren.gg,'%')
		and pug2.GG_inclus_2 not like concat('%',tmp_siren.gg,'%')
		and pug2.`TYPE` = 'GEST_MULTI'
inner join param_utilisateur_gestionnaire pug on pug.code_user = tmp_siren.gest 
where pug2.GG_INCLUS_1 <> '' and pug2.GG_INCLUS_2 <> '' and pug2.GG_EXCLUS = ''
group by tmp_siren.NOSIREN



-- ####################################################################
-- ####################################################################
-- CAS 4.2 : multi-groupes / 2 domaines et 1 gestionnaire non existant
-- ####################################################################
-- ####################################################################
union

select distinct tmp_siren.NOSIREN, pug2.code_user, pug2.NOM, pug2.PRENOM, pug2.NIVEAU_1, pug2.NIVEAU_2, pug2.NIVEAU_3
from  (
		select cl.nosiren, group_concat(distinct co.nmgrpges order by co.nmgrpges separator '%') gg, group_concat( distinct pr.a_traiter_par) gest
		from 
			(	select distinct contrats.noco as noco, clients.nosiren as nosiren from clients inner join contrats on clients.nocli = contrats.nocli
			union 
				select distinct ext.noco as noco, ext.siren as nosiren from extensions_entreprises_affiliees ext
			) as cl
		inner join contrats co on cl.noco = co.noco and co.DT_FIN_SIT is null and co.tmp_batch IS FALSE AND (co.eligdsn = 'SRN' OR co.eligdsn = 'SRT')
		inner join param_etat_contrat p on p.COETACO = co.coetaco and co.tmp_batch = p.tmp_batch AND p.ACTIF = 'O'
		left join (
					select distinct a_traiter_par, p.numero_contrat from periodes_recues p
					inner join contrats ct on p.NUMERO_CONTRAT = ct.NOCO
					inner join (
						select p1.numero_contrat, tmp1.dt_rec, max(p1.dt_creation) dt_crea 
						from periodes_recues p1
						inner join (
							select numero_contrat, max(date_debut_periode) dt_rec
							from periodes_recues
							group by numero_contrat
						) as tmp1 on tmp1.numero_contrat = p1.numero_contrat and tmp1.dt_rec = p1.DATE_DEBUT_PERIODE
						group by numero_contrat, tmp1.dt_rec
					) as tmp2 on tmp2.numero_contrat = p.numero_contrat and tmp2.dt_rec = p.DATE_DEBUT_PERIODE and tmp2.dt_crea = p.DT_CREATION
				) as pr on pr.numero_contrat = co.noco
		where cl.nosiren not in (select siren from param_siren_faux)
		group by cl.nosiren
		having count( distinct co.nmgrpges) > 1 and count( distinct pr.a_traiter_par) = 1
		) as tmp_siren
inner join param_utilisateur_gestionnaire pug2 
		on concat(pug2.GG_inclus_1, ',', pug2.GG_inclus_2) like concat('%',tmp_siren.gg,'%') 
		and pug2.GG_inclus_1 not like concat('%',tmp_siren.gg,'%')
		and pug2.GG_inclus_2 not like concat('%',tmp_siren.gg,'%')
		and pug2.`TYPE` = 'AUTRE_MULTI'
left join param_utilisateur_gestionnaire pug on pug.code_user = tmp_siren.gest 
where pug2.GG_INCLUS_1 <> '' and pug2.GG_INCLUS_2 <> '' and pug2.GG_EXCLUS = ''
and pug.code_user is null and tmp_siren.gest is not null
group by tmp_siren.NOSIREN



-- ####################################################################
-- ####################################################################
-- CAS 4.3 : multi-groupes / 2 domaines et 0 gestionnaire assigné
-- ####################################################################
-- ####################################################################
union

select distinct tmp_siren.NOSIREN, pug2.code_user, pug2.NOM, pug2.PRENOM, pug2.NIVEAU_1, pug2.NIVEAU_2, pug2.NIVEAU_3
from  (
		select cl.nosiren, group_concat(distinct co.nmgrpges order by co.nmgrpges separator '%') gg, group_concat( distinct pr.a_traiter_par) gest
		from 
			(	select distinct contrats.noco as noco, clients.nosiren as nosiren from clients inner join contrats on clients.nocli = contrats.nocli
			union 
				select distinct ext.noco as noco, ext.siren as nosiren from extensions_entreprises_affiliees ext
			) as cl
		inner join contrats co on cl.noco = co.noco and co.DT_FIN_SIT is null and co.tmp_batch IS FALSE AND (co.eligdsn = 'SRN' OR co.eligdsn = 'SRT')
		inner join param_etat_contrat p on p.COETACO = co.coetaco and co.tmp_batch = p.tmp_batch AND p.ACTIF = 'O'
		left join (
					select distinct a_traiter_par, p.numero_contrat from periodes_recues p
					inner join contrats ct on p.NUMERO_CONTRAT = ct.NOCO
					inner join (
						select p1.numero_contrat, tmp1.dt_rec, max(p1.dt_creation) dt_crea 
						from periodes_recues p1
						inner join (
							select numero_contrat, max(date_debut_periode) dt_rec
							from periodes_recues
							group by numero_contrat
						) as tmp1 on tmp1.numero_contrat = p1.numero_contrat and tmp1.dt_rec = p1.DATE_DEBUT_PERIODE
						group by numero_contrat, tmp1.dt_rec
					) as tmp2 on tmp2.numero_contrat = p.numero_contrat and tmp2.dt_rec = p.DATE_DEBUT_PERIODE and tmp2.dt_crea = p.DT_CREATION
				) as pr on pr.numero_contrat = co.noco
		where cl.nosiren not in (select siren from param_siren_faux)
		group by cl.nosiren
		having count( distinct co.nmgrpges) > 1 and count( distinct pr.a_traiter_par) = 0
		) as tmp_siren
inner join param_utilisateur_gestionnaire pug2 
		on concat(pug2.GG_inclus_1, ',', pug2.GG_inclus_2) like concat('%',tmp_siren.gg,'%') 
		and pug2.GG_inclus_1 not like concat('%',tmp_siren.gg,'%')
		and pug2.GG_inclus_2 not like concat('%',tmp_siren.gg,'%')
		and pug2.`TYPE` = 'NON_ASS_MULTI'
where pug2.GG_INCLUS_1 <> '' and pug2.GG_INCLUS_2 <> '' and pug2.GG_EXCLUS = ''
group by tmp_siren.NOSIREN



-- ####################################################################
-- ####################################################################
-- CAS 4.4 : multi-groupes / 2 domaines et plusieurs gestionnaires 
-- ####################################################################
-- ####################################################################
union

select distinct tmp_siren.NOSIREN, pug2.code_user, pug2.NOM, pug2.PRENOM, pug2.NIVEAU_1, pug2.NIVEAU_2, pug2.NIVEAU_3
from  (
		select cl.nosiren, group_concat(distinct co.nmgrpges order by co.nmgrpges separator '%') gg, group_concat( distinct pr.a_traiter_par) gest
		from 
			(	select distinct contrats.noco as noco, clients.nosiren as nosiren from clients inner join contrats on clients.nocli = contrats.nocli
			union 
				select distinct ext.noco as noco, ext.siren as nosiren from extensions_entreprises_affiliees ext
			) as cl
		inner join contrats co on cl.noco = co.noco and co.DT_FIN_SIT is null and co.tmp_batch IS FALSE AND (co.eligdsn = 'SRN' OR co.eligdsn = 'SRT')
		inner join param_etat_contrat p on p.COETACO = co.coetaco and co.tmp_batch = p.tmp_batch AND p.ACTIF = 'O'
		left join (
					select distinct a_traiter_par, p.numero_contrat from periodes_recues p
					inner join contrats ct on p.NUMERO_CONTRAT = ct.NOCO
					inner join (
						select p1.numero_contrat, tmp1.dt_rec, max(p1.dt_creation) dt_crea 
						from periodes_recues p1
						inner join (
							select numero_contrat, max(date_debut_periode) dt_rec
							from periodes_recues
							group by numero_contrat
						) as tmp1 on tmp1.numero_contrat = p1.numero_contrat and tmp1.dt_rec = p1.DATE_DEBUT_PERIODE
						group by numero_contrat, tmp1.dt_rec
					) as tmp2 on tmp2.numero_contrat = p.numero_contrat and tmp2.dt_rec = p.DATE_DEBUT_PERIODE and tmp2.dt_crea = p.DT_CREATION
				) as pr on pr.numero_contrat = co.noco
		where cl.nosiren not in (select siren from param_siren_faux)
		group by cl.nosiren
		having count( distinct co.nmgrpges) > 1 and count( distinct pr.a_traiter_par) > 1
		) as tmp_siren
inner join param_utilisateur_gestionnaire pug2 
		on concat(pug2.GG_inclus_1, ',', pug2.GG_inclus_2) like concat('%',tmp_siren.gg,'%') 
		and pug2.GG_inclus_1 not like concat('%',tmp_siren.gg,'%')
		and pug2.GG_inclus_2 not like concat('%',tmp_siren.gg,'%')
		and pug2.`TYPE` = 'MULTI_MULTI'
where pug2.GG_INCLUS_1 <> '' and pug2.GG_INCLUS_2 <> '' and pug2.GG_EXCLUS = ''
group by tmp_siren.NOSIREN



-- ####################################################################
-- ####################################################################
-- CAS 5.1 : multi-groupes / 1 domaine + autre et 1 gestionnaire existant
-- ####################################################################
-- ####################################################################
union

select tmp_siren_liste_gg.NOSIREN, concat('3',pug.code_user), pug.NOM, pug.PRENOM, pug2.NIVEAU_1, pug2.NIVEAU_2, pug2.NIVEAU_3
from (		
		select distinct cl1.NOSIREN, co1.noco, co1.nmgrpges
		from  (
				select cl.nosiren, group_concat(distinct co.nmgrpges order by co.nmgrpges separator '%') gg, count( distinct pr.a_traiter_par)
				from 
					(	select distinct contrats.noco as noco, clients.nosiren as nosiren from clients inner join contrats on clients.nocli = contrats.nocli
					union 
						select distinct ext.noco as noco, ext.siren as nosiren from extensions_entreprises_affiliees ext
					) as cl
				inner join contrats co on cl.noco = co.noco and co.DT_FIN_SIT is null and co.tmp_batch IS FALSE AND (co.eligdsn = 'SRN' OR co.eligdsn = 'SRT')
				inner join param_etat_contrat p on p.COETACO = co.coetaco and co.tmp_batch = p.tmp_batch AND p.ACTIF = 'O'
				left join (
					select distinct a_traiter_par, p.numero_contrat from periodes_recues p
					inner join contrats ct on p.NUMERO_CONTRAT = ct.NOCO
					inner join (
						select p1.numero_contrat, tmp1.dt_rec, max(p1.dt_creation) dt_crea 
						from periodes_recues p1
						inner join (
							select numero_contrat, max(date_debut_periode) dt_rec
							from periodes_recues
							group by numero_contrat
						) as tmp1 on tmp1.numero_contrat = p1.numero_contrat and tmp1.dt_rec = p1.DATE_DEBUT_PERIODE
						group by numero_contrat, tmp1.dt_rec
					) as tmp2 on tmp2.numero_contrat = p.numero_contrat and tmp2.dt_rec = p.DATE_DEBUT_PERIODE and tmp2.dt_crea = p.DT_CREATION
				) as pr on pr.numero_contrat = co.noco
				where cl.nosiren not in (select siren from param_siren_faux)
				group by cl.nosiren
				having count( distinct co.nmgrpges) > 1 and count( distinct pr.a_traiter_par) = 1
				) as tmp_siren
		inner join 
			(	select distinct contrats.noco as noco, clients.nosiren as nosiren from clients inner join contrats on clients.nocli = contrats.nocli
			union 
				select distinct ext.noco as noco, ext.siren as nosiren from extensions_entreprises_affiliees ext
			) as cl1 on cl1.NOSIREN = tmp_siren.nosiren
		inner join contrats co1 on cl1.noco = co1.noco and co1.DT_FIN_SIT is null and co1.tmp_batch IS FALSE AND (co1.eligdsn = 'SRN' OR co1.eligdsn = 'SRT')
		inner join param_etat_contrat p1 on p1.COETACO = co1.coetaco and co1.tmp_batch = p1.tmp_batch AND p1.ACTIF = 'O'
	) as tmp_siren_liste_gg
inner join (select cl.nosiren, group_concat(distinct co.nmgrpges order by co.nmgrpges separator '%') gg, group_concat( distinct pr.a_traiter_par) gest
				from 
					(	select distinct contrats.noco as noco, clients.nosiren as nosiren from clients inner join contrats on clients.nocli = contrats.nocli
					union 
						select distinct ext.noco as noco, ext.siren as nosiren from extensions_entreprises_affiliees ext
					) as cl
				inner join contrats co on cl.noco = co.noco and co.DT_FIN_SIT is null and co.tmp_batch IS FALSE AND (co.eligdsn = 'SRN' OR co.eligdsn = 'SRT')
				inner join param_etat_contrat p on p.COETACO = co.coetaco and co.tmp_batch = p.tmp_batch AND p.ACTIF = 'O'
				left join (
					select distinct a_traiter_par, p.numero_contrat from periodes_recues p
					inner join contrats ct on p.NUMERO_CONTRAT = ct.NOCO
					inner join (
						select p1.numero_contrat, tmp1.dt_rec, max(p1.dt_creation) dt_crea 
						from periodes_recues p1
						inner join (
							select numero_contrat, max(date_debut_periode) dt_rec
							from periodes_recues
							group by numero_contrat
						) as tmp1 on tmp1.numero_contrat = p1.numero_contrat and tmp1.dt_rec = p1.DATE_DEBUT_PERIODE
						group by numero_contrat, tmp1.dt_rec
					) as tmp2 on tmp2.numero_contrat = p.numero_contrat and tmp2.dt_rec = p.DATE_DEBUT_PERIODE and tmp2.dt_crea = p.DT_CREATION
				) as pr on pr.numero_contrat = co.noco
				where cl.nosiren not in (select siren from param_siren_faux)
				group by cl.nosiren
				having count( distinct co.nmgrpges) > 1 and count( distinct pr.a_traiter_par) = 1
				) as tmp_siren2 on tmp_siren2.nosiren = tmp_siren_liste_gg.nosiren
inner join param_utilisateur_gestionnaire pug2 
		on pug2.GG_inclus_1 like concat('%',tmp_siren_liste_gg.nmgrpges,'%') 
		and pug2.GG_exclus not like concat('%',tmp_siren_liste_gg.nmgrpges,'%')
 	  	and concat(pug2.GG_inclus_1, ',', pug2.GG_exclus) not like concat('%',tmp_siren2.gg,'%') 
 	  	and concat(pug2.GG_exclus, ',', pug2.GG_inclus_1) not like concat('%',tmp_siren2.gg,'%') 
		and pug2.`TYPE` = 'GEST_MULTI'
inner join param_utilisateur_gestionnaire pug on pug.code_user = tmp_siren2.gest 
where pug2.GG_INCLUS_1 <> '' and pug2.GG_INCLUS_2 = '' and pug2.GG_EXCLUS <> ''
and tmp_siren2.nosiren not in (
	select distinct cl.NOSIREN
	from
		(	select distinct contrats.noco as noco, clients.nosiren as nosiren from clients inner join contrats on clients.nocli = contrats.nocli
		union 
			select distinct ext.noco as noco, ext.siren as nosiren from extensions_entreprises_affiliees ext
		) as cl
	inner join (
		select distinct nosiren, nmgrpges 
		from 
			(	select distinct contrats.noco as noco, clients.nosiren as nosiren from clients inner join contrats on clients.nocli = contrats.nocli
			union 
				select distinct ext.noco as noco, ext.siren as nosiren from extensions_entreprises_affiliees ext
			) as cl1
		inner join contrats co1 on cl1.noco = co1.noco and co1.DT_FIN_SIT is null and co1.tmp_batch IS FALSE AND (co1.eligdsn = 'SRN' OR co1.eligdsn = 'SRT')
		inner join param_etat_contrat p1 on p1.COETACO = co1.coetaco and co1.tmp_batch = p1.tmp_batch AND p1.ACTIF = 'O'
		) tmp1 on cl.NOSIREN = tmp1.nosiren
	inner join (
		select distinct nosiren, nmgrpges 
		from 
			(	select distinct contrats.noco as noco, clients.nosiren as nosiren from clients inner join contrats on clients.nocli = contrats.nocli
			union 
				select distinct ext.noco as noco, ext.siren as nosiren from extensions_entreprises_affiliees ext
			) as cl1
		inner join contrats co1 on cl1.noco = co1.noco and co1.DT_FIN_SIT is null and co1.tmp_batch IS FALSE AND (co1.eligdsn = 'SRN' OR co1.eligdsn = 'SRT')
		inner join param_etat_contrat p1 on p1.COETACO = co1.coetaco and co1.tmp_batch = p1.tmp_batch AND p1.ACTIF = 'O'
		) tmp2 on cl.NOSIREN = tmp2.nosiren
	inner join (
		select distinct nosiren, nmgrpges 
		from 
			(	select distinct contrats.noco as noco, clients.nosiren as nosiren from clients inner join contrats on clients.nocli = contrats.nocli
			union 
				select distinct ext.noco as noco, ext.siren as nosiren from extensions_entreprises_affiliees ext
			) as cl1
		inner join contrats co1 on cl1.noco = co1.noco and co1.DT_FIN_SIT is null and co1.tmp_batch IS FALSE AND (co1.eligdsn = 'SRN' OR co1.eligdsn = 'SRT')
		inner join param_etat_contrat p1 on p1.COETACO = co1.coetaco and co1.tmp_batch = p1.tmp_batch AND p1.ACTIF = 'O'
		) tmp3 on cl.NOSIREN = tmp3.nosiren
	inner join param_utilisateur_gestionnaire pug on pug.`TYPE` = 'GEST_MULTI'
		and pug.GG_INCLUS_1 <> '' and pug.GG_INCLUS_2 = '' and pug.GG_EXCLUS <> ''
	where exists (
		select 1 from param_utilisateur_gestionnaire pug2 
		where 1 = 1 
		and pug2.GG_inclus_1 not like concat('%',tmp1.nmgrpges,'%') 
		and pug2.GG_exclus not like concat('%',tmp1.nmgrpges,'%')
		and pug2.`TYPE` = 'GEST_MULTI'
		and pug2.GG_INCLUS_1 <> '' and pug2.GG_INCLUS_2 = '' and pug2.GG_EXCLUS <> ''
		and pug2.code_user = pug.code_user
		)
	and exists (
		select 1 from param_utilisateur_gestionnaire pug2 
		where 1 = 1 
		and pug2.GG_inclus_1 like concat('%',tmp2.nmgrpges,'%')
		and pug2.`TYPE` = 'GEST_MULTI'
		and pug2.GG_INCLUS_1 <> '' and pug2.GG_INCLUS_2 = '' and pug2.GG_EXCLUS <> ''
		and pug2.code_user = pug.code_user
		)
	and exists (
		select 1 from param_utilisateur_gestionnaire pug2 
		where 1 = 1 
		and pug2.GG_exclus like concat('%',tmp3.nmgrpges,'%')
		and pug2.`TYPE` = 'GEST_MULTI'
		and pug2.GG_INCLUS_1 <> '' and pug2.GG_INCLUS_2 = '' and pug2.GG_EXCLUS <> ''
		and pug2.code_user = pug.code_user
		)
	)
group by tmp_siren_liste_gg.NOSIREN



-- ####################################################################
-- ####################################################################
-- CAS 5.2 : multi-groupes / 1 domaine + autre et 1 gestionnaire non existant
-- ####################################################################
-- ####################################################################
union

select tmp_siren_liste_gg.NOSIREN, pug2.code_user, pug2.NOM, pug2.PRENOM, pug2.NIVEAU_1, pug2.NIVEAU_2, pug2.NIVEAU_3
from (		
		select distinct cl1.NOSIREN, co1.noco, co1.nmgrpges
		from  (
				select cl.nosiren, group_concat(distinct co.nmgrpges order by co.nmgrpges separator '%') gg, count( distinct pr.a_traiter_par)
				from 
					(	select distinct contrats.noco as noco, clients.nosiren as nosiren from clients inner join contrats on clients.nocli = contrats.nocli
					union 
						select distinct ext.noco as noco, ext.siren as nosiren from extensions_entreprises_affiliees ext
					) as cl
				inner join contrats co on cl.noco = co.noco and co.DT_FIN_SIT is null and co.tmp_batch IS FALSE AND (co.eligdsn = 'SRN' OR co.eligdsn = 'SRT')
				inner join param_etat_contrat p on p.COETACO = co.coetaco and co.tmp_batch = p.tmp_batch AND p.ACTIF = 'O'
				left join (
					select distinct a_traiter_par, p.numero_contrat from periodes_recues p
					inner join contrats ct on p.NUMERO_CONTRAT = ct.NOCO
					inner join (
						select p1.numero_contrat, tmp1.dt_rec, max(p1.dt_creation) dt_crea 
						from periodes_recues p1
						inner join (
							select numero_contrat, max(date_debut_periode) dt_rec
							from periodes_recues
							group by numero_contrat
						) as tmp1 on tmp1.numero_contrat = p1.numero_contrat and tmp1.dt_rec = p1.DATE_DEBUT_PERIODE
						group by numero_contrat, tmp1.dt_rec
					) as tmp2 on tmp2.numero_contrat = p.numero_contrat and tmp2.dt_rec = p.DATE_DEBUT_PERIODE and tmp2.dt_crea = p.DT_CREATION
				) as pr on pr.numero_contrat = co.noco
				where cl.nosiren not in (select siren from param_siren_faux)
				group by cl.nosiren
				having count( distinct co.nmgrpges) > 1 and count( distinct pr.a_traiter_par) = 1
				) as tmp_siren
		inner join 
			(	select distinct contrats.noco as noco, clients.nosiren as nosiren from clients inner join contrats on clients.nocli = contrats.nocli
			union 
				select distinct ext.noco as noco, ext.siren as nosiren from extensions_entreprises_affiliees ext
			) as cl1 on cl1.NOSIREN = tmp_siren.nosiren
		inner join contrats co1 on cl1.noco = co1.noco and co1.DT_FIN_SIT is null and co1.tmp_batch IS FALSE AND (co1.eligdsn = 'SRN' OR co1.eligdsn = 'SRT')
		inner join param_etat_contrat p1 on p1.COETACO = co1.coetaco and co1.tmp_batch = p1.tmp_batch AND p1.ACTIF = 'O'
	) as tmp_siren_liste_gg
inner join (select cl.nosiren, group_concat(distinct co.nmgrpges order by co.nmgrpges separator '%') gg, group_concat( distinct pr.a_traiter_par) gest
				from 
					(	select distinct contrats.noco as noco, clients.nosiren as nosiren from clients inner join contrats on clients.nocli = contrats.nocli
					union 
						select distinct ext.noco as noco, ext.siren as nosiren from extensions_entreprises_affiliees ext
					) as cl
				inner join contrats co on cl.noco = co.noco and co.DT_FIN_SIT is null and co.tmp_batch IS FALSE AND (co.eligdsn = 'SRN' OR co.eligdsn = 'SRT')
				inner join param_etat_contrat p on p.COETACO = co.coetaco and co.tmp_batch = p.tmp_batch AND p.ACTIF = 'O'
				left join (
					select distinct a_traiter_par, p.numero_contrat from periodes_recues p
					inner join contrats ct on p.NUMERO_CONTRAT = ct.NOCO
					inner join (
						select p1.numero_contrat, tmp1.dt_rec, max(p1.dt_creation) dt_crea 
						from periodes_recues p1
						inner join (
							select numero_contrat, max(date_debut_periode) dt_rec
							from periodes_recues
							group by numero_contrat
						) as tmp1 on tmp1.numero_contrat = p1.numero_contrat and tmp1.dt_rec = p1.DATE_DEBUT_PERIODE
						group by numero_contrat, tmp1.dt_rec
					) as tmp2 on tmp2.numero_contrat = p.numero_contrat and tmp2.dt_rec = p.DATE_DEBUT_PERIODE and tmp2.dt_crea = p.DT_CREATION
				) as pr on pr.numero_contrat = co.noco
				where cl.nosiren not in (select siren from param_siren_faux)
				group by cl.nosiren
				having count( distinct co.nmgrpges) > 1 and count( distinct pr.a_traiter_par) = 1
				) as tmp_siren2 on tmp_siren2.nosiren = tmp_siren_liste_gg.nosiren
inner join param_utilisateur_gestionnaire pug2 
		on pug2.GG_inclus_1 like concat('%',tmp_siren_liste_gg.nmgrpges,'%') 
		and pug2.GG_exclus not like concat('%',tmp_siren_liste_gg.nmgrpges,'%')
 	  	and concat(pug2.GG_inclus_1, ',', pug2.GG_exclus) not like concat('%',tmp_siren2.gg,'%') 
 	  	and concat(pug2.GG_exclus, ',', pug2.GG_inclus_1) not like concat('%',tmp_siren2.gg,'%') 
		and pug2.`TYPE` = 'AUTRE_MULTI'
left join param_utilisateur_gestionnaire pug on pug.code_user = tmp_siren2.gest
where pug2.GG_INCLUS_1 <> '' and pug2.GG_INCLUS_2 = '' and pug2.GG_EXCLUS <> ''
and pug.code_user is null and tmp_siren2.gest is not null
and tmp_siren2.nosiren not in (
	select distinct cl.NOSIREN
	from
		(	select distinct contrats.noco as noco, clients.nosiren as nosiren from clients inner join contrats on clients.nocli = contrats.nocli
		union 
			select distinct ext.noco as noco, ext.siren as nosiren from extensions_entreprises_affiliees ext
		) as cl
	inner join (
		select distinct nosiren, nmgrpges 
		from 
			(	select distinct contrats.noco as noco, clients.nosiren as nosiren from clients inner join contrats on clients.nocli = contrats.nocli
			union 
				select distinct ext.noco as noco, ext.siren as nosiren from extensions_entreprises_affiliees ext
			) as cl1
		inner join contrats co1 on cl1.noco = co1.noco and co1.DT_FIN_SIT is null and co1.tmp_batch IS FALSE AND (co1.eligdsn = 'SRN' OR co1.eligdsn = 'SRT')
		inner join param_etat_contrat p1 on p1.COETACO = co1.coetaco and co1.tmp_batch = p1.tmp_batch AND p1.ACTIF = 'O'
		) tmp1 on cl.NOSIREN = tmp1.nosiren
	inner join (
		select distinct nosiren, nmgrpges 
		from 
			(	select distinct contrats.noco as noco, clients.nosiren as nosiren from clients inner join contrats on clients.nocli = contrats.nocli
			union 
				select distinct ext.noco as noco, ext.siren as nosiren from extensions_entreprises_affiliees ext
			) as cl1
		inner join contrats co1 on cl1.noco = co1.noco and co1.DT_FIN_SIT is null and co1.tmp_batch IS FALSE AND (co1.eligdsn = 'SRN' OR co1.eligdsn = 'SRT')
		inner join param_etat_contrat p1 on p1.COETACO = co1.coetaco and co1.tmp_batch = p1.tmp_batch AND p1.ACTIF = 'O'
		) tmp2 on cl.NOSIREN = tmp2.nosiren
	inner join (
		select distinct nosiren, nmgrpges 
		from 
			(	select distinct contrats.noco as noco, clients.nosiren as nosiren from clients inner join contrats on clients.nocli = contrats.nocli
			union 
				select distinct ext.noco as noco, ext.siren as nosiren from extensions_entreprises_affiliees ext
			) as cl1
		inner join contrats co1 on cl1.noco = co1.noco and co1.DT_FIN_SIT is null and co1.tmp_batch IS FALSE AND (co1.eligdsn = 'SRN' OR co1.eligdsn = 'SRT')
		inner join param_etat_contrat p1 on p1.COETACO = co1.coetaco and co1.tmp_batch = p1.tmp_batch AND p1.ACTIF = 'O'
		) tmp3 on cl.NOSIREN = tmp3.nosiren
	inner join param_utilisateur_gestionnaire pug on pug.`TYPE` = 'AUTRE_MULTI'
		and pug.GG_INCLUS_1 <> '' and pug.GG_INCLUS_2 = '' and pug.GG_EXCLUS <> ''
	where exists (
		select 1 from param_utilisateur_gestionnaire pug2 
		where 1 = 1 
		and pug2.GG_inclus_1 not like concat('%',tmp1.nmgrpges,'%') 
		and pug2.GG_exclus not like concat('%',tmp1.nmgrpges,'%')
		and pug2.`TYPE` = 'AUTRE_MULTI'
		and pug2.GG_INCLUS_1 <> '' and pug2.GG_INCLUS_2 = '' and pug2.GG_EXCLUS <> ''
		and pug2.code_user = pug.code_user
		)
	and exists (
		select 1 from param_utilisateur_gestionnaire pug2 
		where 1 = 1 
		and pug2.GG_inclus_1 like concat('%',tmp2.nmgrpges,'%')
		and pug2.`TYPE` = 'AUTRE_MULTI'
		and pug2.GG_INCLUS_1 <> '' and pug2.GG_INCLUS_2 = '' and pug2.GG_EXCLUS <> ''
		and pug2.code_user = pug.code_user
		)
	and exists (
		select 1 from param_utilisateur_gestionnaire pug2 
		where 1 = 1 
		and pug2.GG_exclus like concat('%',tmp3.nmgrpges,'%')
		and pug2.`TYPE` = 'AUTRE_MULTI'
		and pug2.GG_INCLUS_1 <> '' and pug2.GG_INCLUS_2 = '' and pug2.GG_EXCLUS <> ''
		and pug2.code_user = pug.code_user
		)
	)
group by tmp_siren_liste_gg.NOSIREN





-- ####################################################################
-- ####################################################################
-- CAS 5.3 : multi-groupes / 1 domaine + autre et 0 gestionnaire 
-- ####################################################################
-- ####################################################################
union

select tmp_siren_liste_gg.NOSIREN, pug2.code_user, pug2.NOM, pug2.PRENOM, pug2.NIVEAU_1, pug2.NIVEAU_2, pug2.NIVEAU_3
from (		
		select distinct cl1.NOSIREN, co1.noco, co1.nmgrpges
		from  (
				select cl.nosiren, group_concat(distinct co.nmgrpges order by co.nmgrpges separator '%') gg, count( distinct pr.a_traiter_par)
				from 
					(	select distinct contrats.noco as noco, clients.nosiren as nosiren from clients inner join contrats on clients.nocli = contrats.nocli
					union 
						select distinct ext.noco as noco, ext.siren as nosiren from extensions_entreprises_affiliees ext
					) as cl
				inner join contrats co on cl.noco = co.noco and co.DT_FIN_SIT is null and co.tmp_batch IS FALSE AND (co.eligdsn = 'SRN' OR co.eligdsn = 'SRT')
				inner join param_etat_contrat p on p.COETACO = co.coetaco and co.tmp_batch = p.tmp_batch AND p.ACTIF = 'O'
				left join (
					select distinct a_traiter_par, p.numero_contrat from periodes_recues p
					inner join contrats ct on p.NUMERO_CONTRAT = ct.NOCO
					inner join (
						select p1.numero_contrat, tmp1.dt_rec, max(p1.dt_creation) dt_crea 
						from periodes_recues p1
						inner join (
							select numero_contrat, max(date_debut_periode) dt_rec
							from periodes_recues
							group by numero_contrat
						) as tmp1 on tmp1.numero_contrat = p1.numero_contrat and tmp1.dt_rec = p1.DATE_DEBUT_PERIODE
						group by numero_contrat, tmp1.dt_rec
					) as tmp2 on tmp2.numero_contrat = p.numero_contrat and tmp2.dt_rec = p.DATE_DEBUT_PERIODE and tmp2.dt_crea = p.DT_CREATION
				) as pr on pr.numero_contrat = co.noco
				where cl.nosiren not in (select siren from param_siren_faux)
				group by cl.nosiren
				having count( distinct co.nmgrpges) > 1 and count( distinct pr.a_traiter_par) = 0
				) as tmp_siren
		inner join 
			(	select distinct contrats.noco as noco, clients.nosiren as nosiren from clients inner join contrats on clients.nocli = contrats.nocli
			union 
				select distinct ext.noco as noco, ext.siren as nosiren from extensions_entreprises_affiliees ext
			) as cl1 on cl1.NOSIREN = tmp_siren.nosiren
		inner join contrats co1 on cl1.noco = co1.noco and co1.DT_FIN_SIT is null and co1.tmp_batch IS FALSE AND (co1.eligdsn = 'SRN' OR co1.eligdsn = 'SRT')
		inner join param_etat_contrat p1 on p1.COETACO = co1.coetaco and co1.tmp_batch = p1.tmp_batch AND p1.ACTIF = 'O'
	) as tmp_siren_liste_gg
inner join (select cl.nosiren, group_concat(distinct co.nmgrpges order by co.nmgrpges separator '%') gg, count( distinct pr.a_traiter_par)
				from 
					(	select distinct contrats.noco as noco, clients.nosiren as nosiren from clients inner join contrats on clients.nocli = contrats.nocli
					union 
						select distinct ext.noco as noco, ext.siren as nosiren from extensions_entreprises_affiliees ext
					) as cl
				inner join contrats co on cl.noco = co.noco and co.DT_FIN_SIT is null and co.tmp_batch IS FALSE AND (co.eligdsn = 'SRN' OR co.eligdsn = 'SRT')
				inner join param_etat_contrat p on p.COETACO = co.coetaco and co.tmp_batch = p.tmp_batch AND p.ACTIF = 'O'
				left join (
					select distinct a_traiter_par, p.numero_contrat from periodes_recues p
					inner join contrats ct on p.NUMERO_CONTRAT = ct.NOCO
					inner join (
						select p1.numero_contrat, tmp1.dt_rec, max(p1.dt_creation) dt_crea 
						from periodes_recues p1
						inner join (
							select numero_contrat, max(date_debut_periode) dt_rec
							from periodes_recues
							group by numero_contrat
						) as tmp1 on tmp1.numero_contrat = p1.numero_contrat and tmp1.dt_rec = p1.DATE_DEBUT_PERIODE
						group by numero_contrat, tmp1.dt_rec
					) as tmp2 on tmp2.numero_contrat = p.numero_contrat and tmp2.dt_rec = p.DATE_DEBUT_PERIODE and tmp2.dt_crea = p.DT_CREATION
				) as pr on pr.numero_contrat = co.noco
				where cl.nosiren not in (select siren from param_siren_faux)
				group by cl.nosiren
				having count( distinct co.nmgrpges) > 1 and count( distinct pr.a_traiter_par) = 0
				) as tmp_siren2 on tmp_siren2.nosiren = tmp_siren_liste_gg.nosiren
inner join param_utilisateur_gestionnaire pug2 
		on pug2.GG_inclus_1 like concat('%',tmp_siren_liste_gg.nmgrpges,'%') 
		and pug2.GG_exclus not like concat('%',tmp_siren_liste_gg.nmgrpges,'%')
 	  	and concat(pug2.GG_inclus_1, ',', pug2.GG_exclus) not like concat('%',tmp_siren2.gg,'%') 
 	  	and concat(pug2.GG_exclus, ',', pug2.GG_inclus_1) not like concat('%',tmp_siren2.gg,'%') 
		and pug2.`TYPE` = 'NON_ASS_MULTI'
where pug2.GG_INCLUS_1 <> '' and pug2.GG_INCLUS_2 = '' and pug2.GG_EXCLUS <> ''
and tmp_siren2.nosiren not in (
	select distinct cl.NOSIREN
	from
		(	select distinct contrats.noco as noco, clients.nosiren as nosiren from clients inner join contrats on clients.nocli = contrats.nocli
		union 
			select distinct ext.noco as noco, ext.siren as nosiren from extensions_entreprises_affiliees ext
		) as cl
	inner join (
		select distinct nosiren, nmgrpges 
		from 
			(	select distinct contrats.noco as noco, clients.nosiren as nosiren from clients inner join contrats on clients.nocli = contrats.nocli
			union 
				select distinct ext.noco as noco, ext.siren as nosiren from extensions_entreprises_affiliees ext
			) as cl1
		inner join contrats co1 on cl1.noco = co1.noco and co1.DT_FIN_SIT is null and co1.tmp_batch IS FALSE AND (co1.eligdsn = 'SRN' OR co1.eligdsn = 'SRT')
		inner join param_etat_contrat p1 on p1.COETACO = co1.coetaco and co1.tmp_batch = p1.tmp_batch AND p1.ACTIF = 'O'
		) tmp1 on cl.NOSIREN = tmp1.nosiren
	inner join (
		select distinct nosiren, nmgrpges 
		from 
			(	select distinct contrats.noco as noco, clients.nosiren as nosiren from clients inner join contrats on clients.nocli = contrats.nocli
			union 
				select distinct ext.noco as noco, ext.siren as nosiren from extensions_entreprises_affiliees ext
			) as cl1
		inner join contrats co1 on cl1.noco = co1.noco and co1.DT_FIN_SIT is null and co1.tmp_batch IS FALSE AND (co1.eligdsn = 'SRN' OR co1.eligdsn = 'SRT')
		inner join param_etat_contrat p1 on p1.COETACO = co1.coetaco and co1.tmp_batch = p1.tmp_batch AND p1.ACTIF = 'O'
		) tmp2 on cl.NOSIREN = tmp2.nosiren
	inner join (
		select distinct nosiren, nmgrpges 
		from 
			(	select distinct contrats.noco as noco, clients.nosiren as nosiren from clients inner join contrats on clients.nocli = contrats.nocli
			union 
				select distinct ext.noco as noco, ext.siren as nosiren from extensions_entreprises_affiliees ext
			) as cl1
		inner join contrats co1 on cl1.noco = co1.noco and co1.DT_FIN_SIT is null and co1.tmp_batch IS FALSE AND (co1.eligdsn = 'SRN' OR co1.eligdsn = 'SRT')
		inner join param_etat_contrat p1 on p1.COETACO = co1.coetaco and co1.tmp_batch = p1.tmp_batch AND p1.ACTIF = 'O'
		) tmp3 on cl.NOSIREN = tmp3.nosiren
	inner join param_utilisateur_gestionnaire pug on pug.`TYPE` = 'NON_ASS_MULTI'
		and pug.GG_INCLUS_1 <> '' and pug.GG_INCLUS_2 = '' and pug.GG_EXCLUS <> ''
	where exists (
		select 1 from param_utilisateur_gestionnaire pug2 
		where 1 = 1 
		and pug2.GG_inclus_1 not like concat('%',tmp1.nmgrpges,'%') 
		and pug2.GG_exclus not like concat('%',tmp1.nmgrpges,'%')
		and pug2.`TYPE` = 'NON_ASS_MULTI'
		and pug2.GG_INCLUS_1 <> '' and pug2.GG_INCLUS_2 = '' and pug2.GG_EXCLUS <> ''
		and pug2.code_user = pug.code_user
		)
	and exists (
		select 1 from param_utilisateur_gestionnaire pug2 
		where 1 = 1 
		and pug2.GG_inclus_1 like concat('%',tmp2.nmgrpges,'%')
		and pug2.`TYPE` = 'NON_ASS_MULTI'
		and pug2.GG_INCLUS_1 <> '' and pug2.GG_INCLUS_2 = '' and pug2.GG_EXCLUS <> ''
		and pug2.code_user = pug.code_user
		)
	and exists (
		select 1 from param_utilisateur_gestionnaire pug2 
		where 1 = 1 
		and pug2.GG_exclus like concat('%',tmp3.nmgrpges,'%')
		and pug2.`TYPE` = 'NON_ASS_MULTI'
		and pug2.GG_INCLUS_1 <> '' and pug2.GG_INCLUS_2 = '' and pug2.GG_EXCLUS <> ''
		and pug2.code_user = pug.code_user
		)
	)
group by tmp_siren_liste_gg.NOSIREN



-- ####################################################################
-- ####################################################################
-- CAS 5.4 : multi-groupes / 1 domaine + autre et plusieurs gestionnaires 
-- ####################################################################
-- ####################################################################
union

select tmp_siren_liste_gg.NOSIREN, pug2.code_user, pug2.NOM, pug2.PRENOM, pug2.NIVEAU_1, pug2.NIVEAU_2, pug2.NIVEAU_3
from (		
		select distinct cl1.NOSIREN, co1.noco, co1.nmgrpges
		from  (
				select cl.nosiren, group_concat(distinct co.nmgrpges order by co.nmgrpges separator '%') gg, count( distinct pr.a_traiter_par)
				from 
					(	select distinct contrats.noco as noco, clients.nosiren as nosiren from clients inner join contrats on clients.nocli = contrats.nocli
					union 
						select distinct ext.noco as noco, ext.siren as nosiren from extensions_entreprises_affiliees ext
					) as cl
				inner join contrats co on cl.noco = co.noco and co.DT_FIN_SIT is null and co.tmp_batch IS FALSE AND (co.eligdsn = 'SRN' OR co.eligdsn = 'SRT')
				inner join param_etat_contrat p on p.COETACO = co.coetaco and co.tmp_batch = p.tmp_batch AND p.ACTIF = 'O'
				left join (
							select distinct a_traiter_par, p.numero_contrat from periodes_recues p
							inner join contrats ct on p.NUMERO_CONTRAT = ct.NOCO
							inner join (select numero_contrat, max(date_debut_periode) dt_rec
								from periodes_recues where type_periode = 'DEC'
								group by numero_contrat
							) as tmp2 on tmp2.numero_contrat = p.numero_contrat and tmp2.dt_rec = p.DATE_DEBUT_PERIODE
						) as pr on pr.numero_contrat = co.noco
				where cl.nosiren not in (select siren from param_siren_faux)
				group by cl.nosiren
				having count( distinct co.nmgrpges) > 1 and count( distinct pr.a_traiter_par) > 1
				) as tmp_siren
		inner join  
			(	select distinct contrats.noco as noco, clients.nosiren as nosiren from clients inner join contrats on clients.nocli = contrats.nocli
			union 
				select distinct ext.noco as noco, ext.siren as nosiren from extensions_entreprises_affiliees ext
			) as cl1 on cl1.NOSIREN = tmp_siren.nosiren
		inner join contrats co1 on cl1.noco = co1.noco and co1.DT_FIN_SIT is null and co1.tmp_batch IS FALSE AND (co1.eligdsn = 'SRN' OR co1.eligdsn = 'SRT')
		inner join param_etat_contrat p1 on p1.COETACO = co1.coetaco and co1.tmp_batch = p1.tmp_batch AND p1.ACTIF = 'O'
	) as tmp_siren_liste_gg
inner join (select cl.nosiren, group_concat(distinct co.nmgrpges order by co.nmgrpges separator '%') gg, count( distinct pr.a_traiter_par)
				from 
					(	select distinct contrats.noco as noco, clients.nosiren as nosiren from clients inner join contrats on clients.nocli = contrats.nocli
					union 
						select distinct ext.noco as noco, ext.siren as nosiren from extensions_entreprises_affiliees ext
					) as cl
				inner join contrats co on cl.noco = co.noco and co.DT_FIN_SIT is null and co.tmp_batch IS FALSE AND (co.eligdsn = 'SRN' OR co.eligdsn = 'SRT')
				inner join param_etat_contrat p on p.COETACO = co.coetaco and co.tmp_batch = p.tmp_batch AND p.ACTIF = 'O'
				left join (
					select distinct a_traiter_par, p.numero_contrat from periodes_recues p
					inner join contrats ct on p.NUMERO_CONTRAT = ct.NOCO
					inner join (
						select p1.numero_contrat, tmp1.dt_rec, max(p1.dt_creation) dt_crea 
						from periodes_recues p1
						inner join (
							select numero_contrat, max(date_debut_periode) dt_rec
							from periodes_recues
							group by numero_contrat
						) as tmp1 on tmp1.numero_contrat = p1.numero_contrat and tmp1.dt_rec = p1.DATE_DEBUT_PERIODE
						group by numero_contrat, tmp1.dt_rec
					) as tmp2 on tmp2.numero_contrat = p.numero_contrat and tmp2.dt_rec = p.DATE_DEBUT_PERIODE and tmp2.dt_crea = p.DT_CREATION
				) as pr on pr.numero_contrat = co.noco
				where cl.nosiren not in (select siren from param_siren_faux)
				group by cl.nosiren
				having count( distinct co.nmgrpges) > 1 and count( distinct pr.a_traiter_par) > 1
				) as tmp_siren2 on tmp_siren2.nosiren = tmp_siren_liste_gg.nosiren
inner join param_utilisateur_gestionnaire pug2 
		on pug2.GG_inclus_1 like concat('%',tmp_siren_liste_gg.nmgrpges,'%') 
		and pug2.GG_exclus not like concat('%',tmp_siren_liste_gg.nmgrpges,'%')
		and concat(pug2.GG_inclus_1, ',', pug2.GG_exclus) not like concat('%',tmp_siren2.gg,'%') 
		and concat(pug2.GG_exclus, ',', pug2.GG_inclus_1) not like concat('%',tmp_siren2.gg,'%') 
		and pug2.`TYPE` = 'MULTI_MULTI'
where pug2.GG_INCLUS_1 <> '' and pug2.GG_INCLUS_2 = '' and pug2.GG_EXCLUS <> ''
and tmp_siren2.nosiren not in (
	select distinct cl.NOSIREN
	from
		(	select distinct contrats.noco as noco, clients.nosiren as nosiren from clients inner join contrats on clients.nocli = contrats.nocli
		union 
			select distinct ext.noco as noco, ext.siren as nosiren from extensions_entreprises_affiliees ext
		) as cl
	inner join (
		select distinct nosiren, nmgrpges 
		from 
			(	select distinct contrats.noco as noco, clients.nosiren as nosiren from clients inner join contrats on clients.nocli = contrats.nocli
			union 
				select distinct ext.noco as noco, ext.siren as nosiren from extensions_entreprises_affiliees ext
			) as cl1
		inner join contrats co1 on cl1.noco = co1.noco and co1.DT_FIN_SIT is null and co1.tmp_batch IS FALSE AND (co1.eligdsn = 'SRN' OR co1.eligdsn = 'SRT')
		inner join param_etat_contrat p1 on p1.COETACO = co1.coetaco and co1.tmp_batch = p1.tmp_batch AND p1.ACTIF = 'O'
		) tmp1 on cl.NOSIREN = tmp1.nosiren
	inner join (
		select distinct nosiren, nmgrpges 
		from 
			(	select distinct contrats.noco as noco, clients.nosiren as nosiren from clients inner join contrats on clients.nocli = contrats.nocli
			union 
				select distinct ext.noco as noco, ext.siren as nosiren from extensions_entreprises_affiliees ext
			) as cl1
		inner join contrats co1 on cl1.noco = co1.noco and co1.DT_FIN_SIT is null and co1.tmp_batch IS FALSE AND (co1.eligdsn = 'SRN' OR co1.eligdsn = 'SRT')
		inner join param_etat_contrat p1 on p1.COETACO = co1.coetaco and co1.tmp_batch = p1.tmp_batch AND p1.ACTIF = 'O'
		) tmp2 on cl.NOSIREN = tmp2.nosiren
	inner join (
		select distinct nosiren, nmgrpges 
		from 
			(	select distinct contrats.noco as noco, clients.nosiren as nosiren from clients inner join contrats on clients.nocli = contrats.nocli
			union 
				select distinct ext.noco as noco, ext.siren as nosiren from extensions_entreprises_affiliees ext
			) as cl1
		inner join contrats co1 on cl1.noco = co1.noco and co1.DT_FIN_SIT is null and co1.tmp_batch IS FALSE AND (co1.eligdsn = 'SRN' OR co1.eligdsn = 'SRT')
		inner join param_etat_contrat p1 on p1.COETACO = co1.coetaco and co1.tmp_batch = p1.tmp_batch AND p1.ACTIF = 'O'
		) tmp3 on cl.NOSIREN = tmp3.nosiren
	inner join param_utilisateur_gestionnaire pug on pug.`TYPE` = 'MULTI_MULTI'
		and pug.GG_INCLUS_1 <> '' and pug.GG_INCLUS_2 = '' and pug.GG_EXCLUS <> ''
	where exists (
		select 1 from param_utilisateur_gestionnaire pug2 
		where 1 = 1 
		and pug2.GG_inclus_1 not like concat('%',tmp1.nmgrpges,'%') 
		and pug2.GG_exclus not like concat('%',tmp1.nmgrpges,'%')
		and pug2.`TYPE` = 'MULTI_MULTI'
		and pug2.GG_INCLUS_1 <> '' and pug2.GG_INCLUS_2 = '' and pug2.GG_EXCLUS <> ''
		and pug2.code_user = pug.code_user
		)
	and exists (
		select 1 from param_utilisateur_gestionnaire pug2 
		where 1 = 1 
		and pug2.GG_inclus_1 like concat('%',tmp2.nmgrpges,'%')
		and pug2.`TYPE` = 'MULTI_MULTI'
		and pug2.GG_INCLUS_1 <> '' and pug2.GG_INCLUS_2 = '' and pug2.GG_EXCLUS <> ''
		and pug2.code_user = pug.code_user
		)
	and exists (
		select 1 from param_utilisateur_gestionnaire pug2 
		where 1 = 1 
		and pug2.GG_exclus like concat('%',tmp3.nmgrpges,'%')
		and pug2.`TYPE` = 'MULTI_MULTI'
		and pug2.GG_INCLUS_1 <> '' and pug2.GG_INCLUS_2 = '' and pug2.GG_EXCLUS <> ''
		and pug2.code_user = pug.code_user
		)
	)
group by tmp_siren_liste_gg.NOSIREN






-- ####################################################################
-- ####################################################################
-- CAS 6.1 : 1 ou plusieurs groupes hors 2 domaines (quelques soit le nombre de gestionnaires)
-- ####################################################################
-- ####################################################################
union

select tmp_siren2.NOSIREN, pug2.code_user, pug2.NOM, pug2.PRENOM, pug2.NIVEAU_1, pug2.NIVEAU_2, pug2.NIVEAU_3
from 
	(select cl.nosiren, group_concat(distinct co.nmgrpges order by co.nmgrpges separator '%') gg
				from 
					(	select distinct contrats.noco as noco, clients.nosiren as nosiren from clients inner join contrats on clients.nocli = contrats.nocli
					union 
						select distinct ext.noco as noco, ext.siren as nosiren from extensions_entreprises_affiliees ext
					) as cl
				inner join contrats co on cl.noco = co.noco and co.DT_FIN_SIT is null and co.tmp_batch IS FALSE AND (co.eligdsn = 'SRN' OR co.eligdsn = 'SRT')
				inner join param_etat_contrat p on p.COETACO = co.coetaco and co.tmp_batch = p.tmp_batch AND p.ACTIF = 'O'
				left join (
					select distinct a_traiter_par, p.numero_contrat from periodes_recues p
					inner join contrats ct on p.NUMERO_CONTRAT = ct.NOCO
					inner join (
						select p1.numero_contrat, tmp1.dt_rec, max(p1.dt_creation) dt_crea 
						from periodes_recues p1
						inner join (
							select numero_contrat, max(date_debut_periode) dt_rec
							from periodes_recues
							group by numero_contrat
						) as tmp1 on tmp1.numero_contrat = p1.numero_contrat and tmp1.dt_rec = p1.DATE_DEBUT_PERIODE
						group by numero_contrat, tmp1.dt_rec
					) as tmp2 on tmp2.numero_contrat = p.numero_contrat and tmp2.dt_rec = p.DATE_DEBUT_PERIODE and tmp2.dt_crea = p.DT_CREATION
				) as pr on pr.numero_contrat = co.noco
				where cl.nosiren not in (select siren from param_siren_faux)
				group by cl.nosiren
				) as tmp_siren2
inner join param_utilisateur_gestionnaire pug2 on pug2.`TYPE` = 'GEST_AUTRE'
where pug2.GG_INCLUS_1 = '' and pug2.GG_INCLUS_2 = '' and pug2.GG_EXCLUS <> ''
and tmp_siren2.nosiren not in (
	select distinct cl.NOSIREN
	from
		(	select distinct contrats.noco as noco, clients.nosiren as nosiren from clients inner join contrats on clients.nocli = contrats.nocli
		union 
			select distinct ext.noco as noco, ext.siren as nosiren from extensions_entreprises_affiliees ext
		) as cl
	inner join (
		select distinct nosiren, nmgrpges 
		from 
			(	select distinct contrats.noco as noco, clients.nosiren as nosiren from clients inner join contrats on clients.nocli = contrats.nocli
			union 
				select distinct ext.noco as noco, ext.siren as nosiren from extensions_entreprises_affiliees ext
			) as cl1
		inner join contrats co1 on cl1.noco = co1.noco and co1.DT_FIN_SIT is null and co1.tmp_batch IS FALSE AND (co1.eligdsn = 'SRN' OR co1.eligdsn = 'SRT')
		inner join param_etat_contrat p1 on p1.COETACO = co1.coetaco and co1.tmp_batch = p1.tmp_batch AND p1.ACTIF = 'O'
		) tmp3 on cl.NOSIREN = tmp3.nosiren
	where	exists (
		select 1 from param_utilisateur_gestionnaire pug2 
		where pug2.GG_exclus like concat('%',tmp3.nmgrpges,'%')
		and pug2.`TYPE` = 'GEST_AUTRE'
		and pug2.GG_INCLUS_1 = '' and pug2.GG_INCLUS_2 = '' and pug2.GG_EXCLUS <> ''
		) 
	)
group by tmp_siren2.NOSIREN



-- ####################################################################
-- ####################################################################
-- CAS 7.1 : OUBLIE DANS LA SFD : plusieurs groupes dans les 2 domaines + autre (quelque soit le nb de gestionnaires)
-- ####################################################################
-- ####################################################################
union

select tmp_siren2.NOSIREN, pug2.code_user, pug2.NOM, pug2.PRENOM, pug2.NIVEAU_1, pug2.NIVEAU_2, pug2.NIVEAU_3
from 
	(select cl.nosiren, group_concat(distinct co.nmgrpges order by co.nmgrpges separator '%') gg
				from 
					(	select distinct contrats.noco as noco, clients.nosiren as nosiren from clients inner join contrats on clients.nocli = contrats.nocli
					union 
						select distinct ext.noco as noco, ext.siren as nosiren from extensions_entreprises_affiliees ext
					) as cl
				inner join contrats co on cl.noco = co.noco and co.DT_FIN_SIT is null and co.tmp_batch IS FALSE AND (co.eligdsn = 'SRN' OR co.eligdsn = 'SRT')
				inner join param_etat_contrat p on p.COETACO = co.coetaco and co.tmp_batch = p.tmp_batch AND p.ACTIF = 'O'
				left join (
					select distinct a_traiter_par, p.numero_contrat from periodes_recues p
					inner join contrats ct on p.NUMERO_CONTRAT = ct.NOCO
					inner join (
						select p1.numero_contrat, tmp1.dt_rec, max(p1.dt_creation) dt_crea 
						from periodes_recues p1
						inner join (
							select numero_contrat, max(date_debut_periode) dt_rec
							from periodes_recues
							group by numero_contrat
						) as tmp1 on tmp1.numero_contrat = p1.numero_contrat and tmp1.dt_rec = p1.DATE_DEBUT_PERIODE
						group by numero_contrat, tmp1.dt_rec
					) as tmp2 on tmp2.numero_contrat = p.numero_contrat and tmp2.dt_rec = p.DATE_DEBUT_PERIODE and tmp2.dt_crea = p.DT_CREATION
				) as pr on pr.numero_contrat = co.noco
				where cl.nosiren not in (select siren from param_siren_faux)
				group by cl.nosiren
				) as tmp_siren2
inner join param_utilisateur_gestionnaire pug2 on pug2.`TYPE` = 'MULTI_2DA'
where pug2.GG_INCLUS_1 <> '' and pug2.GG_INCLUS_2 <> '' and pug2.GG_EXCLUS = ''
and tmp_siren2.nosiren in (
	select distinct cl.NOSIREN
	from 
		(	select distinct contrats.noco as noco, clients.nosiren as nosiren from clients inner join contrats on clients.nocli = contrats.nocli
		union 
			select distinct ext.noco as noco, ext.siren as nosiren from extensions_entreprises_affiliees ext
		) as cl
	inner join (
		select distinct nosiren, nmgrpges 
		from 
			(	select distinct contrats.noco as noco, clients.nosiren as nosiren from clients inner join contrats on clients.nocli = contrats.nocli
			union 
				select distinct ext.noco as noco, ext.siren as nosiren from extensions_entreprises_affiliees ext
			) as cl1
		inner join contrats co1 on cl1.noco = co1.noco and co1.DT_FIN_SIT is null and co1.tmp_batch IS FALSE AND (co1.eligdsn = 'SRN' OR co1.eligdsn = 'SRT')
		inner join param_etat_contrat p1 on p1.COETACO = co1.coetaco and co1.tmp_batch = p1.tmp_batch AND p1.ACTIF = 'O'
		) tmp1 on cl.NOSIREN = tmp1.nosiren
	inner join (
		select distinct nosiren, nmgrpges 
		from 
			(	select distinct contrats.noco as noco, clients.nosiren as nosiren from clients inner join contrats on clients.nocli = contrats.nocli
			union 
				select distinct ext.noco as noco, ext.siren as nosiren from extensions_entreprises_affiliees ext
			) as cl1
		inner join contrats co1 on cl1.noco = co1.noco and co1.DT_FIN_SIT is null and co1.tmp_batch IS FALSE AND (co1.eligdsn = 'SRN' OR co1.eligdsn = 'SRT')
		inner join param_etat_contrat p1 on p1.COETACO = co1.coetaco and co1.tmp_batch = p1.tmp_batch AND p1.ACTIF = 'O'
		) tmp2 on cl.NOSIREN = tmp2.nosiren
	inner join (
		select distinct nosiren, nmgrpges 
		from 
			(	select distinct contrats.noco as noco, clients.nosiren as nosiren from clients inner join contrats on clients.nocli = contrats.nocli
			union 
				select distinct ext.noco as noco, ext.siren as nosiren from extensions_entreprises_affiliees ext
			) as cl1
		inner join contrats co1 on cl1.noco = co1.noco and co1.DT_FIN_SIT is null and co1.tmp_batch IS FALSE AND (co1.eligdsn = 'SRN' OR co1.eligdsn = 'SRT')
		inner join param_etat_contrat p1 on p1.COETACO = co1.coetaco and co1.tmp_batch = p1.tmp_batch AND p1.ACTIF = 'O'
		) tmp3 on cl.NOSIREN = tmp3.nosiren
	inner join param_utilisateur_gestionnaire pug on pug.`TYPE` = 'MULTI_MULTI'
		and pug.GG_INCLUS_1 <> '' and pug.GG_INCLUS_2 <> '' and pug.GG_EXCLUS = ''
	where exists (
		select 1 from param_utilisateur_gestionnaire pug2 
		where 1 = 1 
		and pug2.GG_inclus_1 not like concat('%',tmp1.nmgrpges,'%') 
		and pug2.GG_INCLUS_2 not like concat('%',tmp1.nmgrpges,'%')
		and pug2.`TYPE` = 'MULTI_MULTI'
		and pug2.GG_INCLUS_1 <> '' and pug2.GG_INCLUS_2 <> '' and pug2.GG_EXCLUS = ''
		and pug2.code_user = pug.code_user
		)
	and exists (
		select 1 from param_utilisateur_gestionnaire pug2 
		where 1 = 1 
		and pug2.GG_inclus_1 like concat('%',tmp2.nmgrpges,'%')
		and pug2.`TYPE` = 'MULTI_MULTI'
		and pug2.GG_INCLUS_1 <> '' and pug2.GG_INCLUS_2 <> '' and pug2.GG_EXCLUS = ''
		and pug2.code_user = pug.code_user
		)
	and exists (
		select 1 from param_utilisateur_gestionnaire pug2 
		where 1 = 1 
		and pug2.GG_INCLUS_2 like concat('%',tmp3.nmgrpges,'%')
		and pug2.`TYPE` = 'MULTI_MULTI'
		and pug2.GG_INCLUS_1 <> '' and pug2.GG_INCLUS_2 <> '' and pug2.GG_EXCLUS = ''
		and pug2.code_user = pug.code_user
		)
	)
group by tmp_siren2.NOSIREN

order by NOSIREN;
END$$
DELIMITER ;


