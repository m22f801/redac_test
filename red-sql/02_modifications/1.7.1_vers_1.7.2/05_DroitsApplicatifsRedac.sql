
-- Octroiement des droits d'exécution au user batch (à préciser)

GRANT EXECUTE ON PROCEDURE Inserer_CATEGORIE_QUITTANCEMENT_INDIVIDU_EFFECTIF TO 'user_batch';
GRANT EXECUTE ON PROCEDURE Inserer_CATEGORIE_QUITTANCEMENT_INDIVIDU_SALAIRE TO 'user_batch';
GRANT EXECUTE ON PROCEDURE Inserer_CATEGORIE_QUITTANCEMENT_INDIVIDU_SALAIRE_pour_periodes TO 'user_batch';
GRANT EXECUTE ON PROCEDURE Inserer_EFFECTIF_CATEGORIE_MVT TO 'user_batch';
GRANT EXECUTE ON PROCEDURE Inserer_TRANCHE_CATEGORIE TO 'user_batch';
GRANT EXECUTE ON PROCEDURE Inserer_TRANCHE_CATEGORIE_BASE_ASSUJETTIE TO 'user_batch';
GRANT EXECUTE ON PROCEDURE Inserer_TRANCHE_CATEGORIE_BASE_ASSUJETTIE_pour_periodes TO 'user_batch';
GRANT EXECUTE ON PROCEDURE Inserer_TRANCHE_CATEGORIE_pour_periodes TO 'user_batch';
GRANT EXECUTE ON PROCEDURE Purger_elements_declaratifs_en_masse TO 'user_batch';

-- Octroiement des droits d'exécution au user TP (à préciser)
GRANT EXECUTE ON PROCEDURE Purger_elements_declaratifs_en_masse TO 'user_TP';
