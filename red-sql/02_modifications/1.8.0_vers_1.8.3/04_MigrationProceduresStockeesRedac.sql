-- RED-257
DROP PROCEDURE IF EXISTS Creation_HistoriqueCommentairePeriode_en_masse;
DELIMITER $$
CREATE PROCEDURE Creation_HistoriqueCommentairePeriode_en_masse(IN p_liste_ids_periodes TEXT,IN datehms NUMERIC(14), IN utilisateur CHAR(30), IN commentaire VARCHAR(1000), IN userCreation VARCHAR(20))
BEGIN
	
INSERT INTO HISTO_COMMENTAIRE_PERIODE(ID_PERIODE, UTILISATEUR, DATEHMS_SAISIE, COMMENTAIRE, USER_CREATION)
SELECT p.ID_PERIODE, utilisateur, datehms, commentaire, userCreation
FROM periodes_recues p WHERE FIND_IN_SET(ID_PERIODE, p_liste_ids_periodes) > 0;

END$$
DELIMITER ;

-- RED-258
DROP PROCEDURE IF EXISTS Creation_HistoriqueAssignationPeriode_en_masse;
DELIMITER $$
CREATE PROCEDURE Creation_HistoriqueAssignationPeriode_en_masse(IN p_liste_ids_periodes TEXT,IN datehms NUMERIC(14), IN aTraiterPar CHAR(30), IN userCreation VARCHAR(20))
BEGIN
	
INSERT INTO HISTO_ASSIGNATION_PERIODE(ID_PERIODE, A_TRAITER_PAR, DATEHMS_ASSIGNE, USER_CREATION)
SELECT p.ID_PERIODE, aTraiterPar, datehms, userCreation
FROM periodes_recues p WHERE FIND_IN_SET(ID_PERIODE, p_liste_ids_periodes) > 0;

END$$
DELIMITER ;

-- RED-259
DROP PROCEDURE IF EXISTS Creation_HistoriqueAttenteRetourEtpPeriode_en_masse;
DELIMITER $$
CREATE PROCEDURE Creation_HistoriqueAttenteRetourEtpPeriode_en_masse(IN p_liste_ids_periodes TEXT,IN datehms NUMERIC(14), IN utilisateur CHAR(30), IN action VARCHAR(20), IN userCreation VARCHAR(20))
BEGIN
	
INSERT INTO HISTO_ATTENTE_RETOUR_ETP_PERIODE(ID_PERIODE, ACTION, UTILISATEUR, DATEHMS_RETOUR, USER_CREATION)
SELECT p.ID_PERIODE, action, utilisateur, datehms, userCreation
FROM periodes_recues p WHERE FIND_IN_SET(ID_PERIODE, p_liste_ids_periodes) > 0;
AND action collate latin1_general_ci != coalesce((SELECT h.action FROM histo_attente_retour_etp_periode h WHERE h.ID_PERIODE=p.ID_PERIODE ORDER BY h.DT_CREATION DESC LIMIT 1), 'NA');

END$$
DELIMITER ;

-- RED-244
DROP PROCEDURE IF EXISTS Calcul_effectif_indiv_salaire;
DELIMITER $$
CREATE PROCEDURE Calcul_effectif_indiv_salaire(IN p_audit_nom_batch VARCHAR(20))
BEGIN

DECLARE done INT DEFAULT FALSE;
DECLARE C_NO_CAT char(30);

DECLARE C_ID_PERIODE bigint(20);
DECLARE ID_PER_MAJ bigint(20);
DECLARE EFF_DEB_MAJ decimal(10,0);
DECLARE EFF_FIN_MAJ decimal(10,0);

DECLARE liste_nocat_unique CURSOR FOR
(SELECT cqi.ID_PERIODE,cqi.NOCAT
FROM categorie_quittancement_individu cqi INNER JOIN V_selection_periode_nature_contrat_salaires vpns ON (cqi.ID_PERIODE=vpns.ID_PERIODE)
WHERE cqi.individu='TOUS'
GROUP BY cqi.ID_PERIODE
HAVING COUNT(DISTINCT cqi.NOCAT)<=1);

DECLARE liste_nocat_non_unique CURSOR FOR
(SELECT cqi.ID_PERIODE,cqi.NOCAT
FROM categorie_quittancement_individu cqi INNER JOIN V_selection_periode_nature_contrat_salaires vpns ON (cqi.ID_PERIODE=vpns.ID_PERIODE)
WHERE cqi.individu='TOUS'
 AND (SELECT COUNT(DISTINCT cqi2.NOCAT) FROM categorie_quittancement_individu cqi2 WHERE cqi2.ID_PERIODE=cqi.ID_PERIODE)>1
GROUP BY cqi.ID_PERIODE, cqi.NOCAT);

DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

OPEN liste_nocat_unique;
IF (Select FOUND_ROWS()) > 0 THEN
	nocat_uni: LOOP 
	FETCH liste_nocat_unique INTO C_ID_PERIODE, C_NO_CAT;
		IF done THEN 
			LEAVE nocat_uni;
		END IF;
		-- Calcul effectifs
		SELECT 
    COUNT(DISTINCT (CASE
            WHEN (i.IDENTIFIANT_REPERTOIRE = '') THEN i.NTT
            ELSE i.IDENTIFIANT_REPERTOIRE
        END)), p.ID_PERIODE INTO EFF_DEB_MAJ, ID_PER_MAJ
FROM
    periodes_recues p
        INNER JOIN
    rattachement_declarations_recues rda ON (p.ID_PERIODE = rda.ID_PERIODE)
        INNER JOIN
    adhesion_etablissement_mois adh ON (adh.ID_ADH_ETAB_MOIS = rda.ID_ADH_ETAB_MOIS)
        INNER JOIN
    individu i ON (i.ID_ADH_ETAB_MOIS = adh.ID_ADH_ETAB_MOIS)
		INNER JOIN
	contrat_travail ct ON (ct.ID_INDIVIDU = i.ID_INDIVIDU)
		INNER JOIN
	affiliation a ON (a.ID_CONTRAT_TRAVAIL = ct.ID_CONTRAT_TRAVAIL)
		INNER JOIN
	base_assujettie ba ON (ba.ID_AFFILIATION = a.ID_AFFILIATION AND ba.DATE_DEB_RATTACHEMENT = p.DATE_DEBUT_PERIODE)
WHERE
    p.ID_PERIODE = C_ID_PERIODE;
					
		SELECT 
    COUNT(DISTINCT (CASE
            WHEN (i.IDENTIFIANT_REPERTOIRE = '') THEN i.NTT
            ELSE i.IDENTIFIANT_REPERTOIRE
        END)) INTO EFF_FIN_MAJ
FROM
    periodes_recues p
        INNER JOIN
    rattachement_declarations_recues rda ON (p.ID_PERIODE = rda.ID_PERIODE)
        INNER JOIN
    adhesion_etablissement_mois adh ON (adh.ID_ADH_ETAB_MOIS = rda.ID_ADH_ETAB_MOIS)
        INNER JOIN
    individu i ON (i.ID_ADH_ETAB_MOIS = adh.ID_ADH_ETAB_MOIS)
		INNER JOIN
	contrat_travail ct ON (ct.ID_INDIVIDU = i.ID_INDIVIDU)
		INNER JOIN
	affiliation a ON (a.ID_CONTRAT_TRAVAIL = ct.ID_CONTRAT_TRAVAIL)
		INNER JOIN
	base_assujettie ba ON (ba.ID_AFFILIATION = a.ID_AFFILIATION AND ba.DATE_FIN_RATTACHEMENT = p.DATE_FIN_PERIODE)
WHERE
    p.ID_PERIODE = C_ID_PERIODE;
	
UPDATE categorie_quittancement_individu cqi3
SET 
	cqi3.EFFECTIF_DEBUT = EFF_DEB_MAJ,
	cqi3.EFFECTIF_FIN = EFF_FIN_MAJ
WHERE
	cqi3.ID_PERIODE = ID_PER_MAJ;
END LOOP;
END IF;
CLOSE liste_nocat_unique;

BEGIN
DECLARE done2 INT DEFAULT FALSE;
DECLARE CONTINUE HANDLER FOR NOT FOUND SET done2 = TRUE;

OPEN liste_nocat_non_unique;
IF (SELECT FOUND_ROWS()) > 0 THEN
	nocat_non_uni : LOOP 
	FETCH liste_nocat_non_unique INTO C_ID_PERIODE, C_NO_CAT;
        
		IF done2 THEN 
			LEAVE nocat_non_uni;
		END IF;

		-- Calcul effectifs
		SELECT 
    COUNT(DISTINCT (CASE
            WHEN (i.IDENTIFIANT_REPERTOIRE = '') THEN i.NTT
            ELSE i.IDENTIFIANT_REPERTOIRE
        END)), p.ID_PERIODE INTO EFF_DEB_MAJ, ID_PER_MAJ
FROM
    periodes_recues p
        INNER JOIN
    rattachement_declarations_recues rda ON (p.ID_PERIODE = rda.ID_PERIODE)
        INNER JOIN
    adhesion_etablissement_mois adh ON (adh.ID_ADH_ETAB_MOIS = rda.ID_ADH_ETAB_MOIS)
        INNER JOIN
    individu i ON (i.ID_ADH_ETAB_MOIS = adh.ID_ADH_ETAB_MOIS)
		INNER JOIN
	contrat_travail ct ON (ct.ID_INDIVIDU = i.ID_INDIVIDU)
		INNER JOIN
	affiliation a ON (a.ID_CONTRAT_TRAVAIL = ct.ID_CONTRAT_TRAVAIL)
		INNER JOIN
	base_assujettie ba ON (ba.ID_AFFILIATION = a.ID_AFFILIATION AND ba.DATE_DEB_RATTACHEMENT = p.DATE_DEBUT_PERIODE)
WHERE
    p.ID_PERIODE = C_ID_PERIODE
	AND (C_NO_CAT collate latin1_general_ci) = (COALESCE(IF((a.CODE_POPULATION = ''),
                    NULL,
                    a.CODE_POPULATION),
                (SELECT 
                        t.NOCAT
                    FROM
                        tarifs t
                    WHERE
                        ((t.NOCO = p.NUMERO_CONTRAT)
                            AND (t.DT_DEBUT_SIT <= p.DATE_DEBUT_PERIODE)
                            AND ((COALESCE(t.DT_FIN_SIT, 99999999) >= p.DATE_DEBUT_PERIODE)
                            OR ((CASE
                            WHEN (t.DT_FIN_SIT = 0) THEN 99999999
                            ELSE t.DT_FIN_SIT
                        END) >= p.DATE_DEBUT_PERIODE)))
                    ORDER BY t.DT_DEBUT_SIT
                    LIMIT 1)));

SELECT 
    COUNT(DISTINCT (CASE
            WHEN (i.IDENTIFIANT_REPERTOIRE = '') THEN i.NTT
            ELSE i.IDENTIFIANT_REPERTOIRE
        END)) INTO EFF_FIN_MAJ
FROM
    periodes_recues p
        INNER JOIN
    rattachement_declarations_recues rda ON (p.ID_PERIODE = rda.ID_PERIODE)
        INNER JOIN
    adhesion_etablissement_mois adh ON (adh.ID_ADH_ETAB_MOIS = rda.ID_ADH_ETAB_MOIS)
        INNER JOIN
    individu i ON (i.ID_ADH_ETAB_MOIS = adh.ID_ADH_ETAB_MOIS)
		INNER JOIN
	contrat_travail ct ON (ct.ID_INDIVIDU = i.ID_INDIVIDU)
		INNER JOIN
	affiliation a ON (a.ID_CONTRAT_TRAVAIL = ct.ID_CONTRAT_TRAVAIL)
		INNER JOIN
	base_assujettie ba ON (ba.ID_AFFILIATION = a.ID_AFFILIATION AND ba.DATE_FIN_RATTACHEMENT = p.DATE_FIN_PERIODE)
WHERE
    p.ID_PERIODE = C_ID_PERIODE
	AND (C_NO_CAT collate latin1_general_ci) = (COALESCE(IF((a.CODE_POPULATION = ''),
                    NULL,
                    a.CODE_POPULATION),
                (SELECT 
                        t.NOCAT
                    FROM
                        tarifs t
                    WHERE
                        ((t.NOCO = p.NUMERO_CONTRAT)
                            AND (t.DT_DEBUT_SIT <= p.DATE_DEBUT_PERIODE)
                            AND ((COALESCE(t.DT_FIN_SIT, 99999999) >= p.DATE_DEBUT_PERIODE)
                            OR ((CASE
                            WHEN (t.DT_FIN_SIT = 0) THEN 99999999
                            ELSE t.DT_FIN_SIT
                        END) >= p.DATE_DEBUT_PERIODE)))
                    ORDER BY t.DT_DEBUT_SIT
                    LIMIT 1)));
	    
		UPDATE categorie_quittancement_individu cqi3 
SET 
    cqi3.EFFECTIF_DEBUT = EFF_DEB_MAJ,
    cqi3.EFFECTIF_FIN = EFF_FIN_MAJ
WHERE
    cqi3.ID_PERIODE = ID_PER_MAJ
AND cqi3.NOCAT = (C_NO_CAT collate latin1_general_ci);
	END LOOP;
END IF;
CLOSE liste_nocat_non_unique;
END;
END$$
DELIMITER ;

DROP PROCEDURE IF EXISTS Calcul_effectif_indiv_salaire_pour_periodes;
DELIMITER $$
CREATE PROCEDURE Calcul_effectif_indiv_salaire_pour_periodes(IN p_audit_nom_batch VARCHAR(20), in p_liste_ids_periodes TEXT)
BEGIN

DECLARE done INT DEFAULT FALSE;
DECLARE C_NO_CAT char(30);

DECLARE C_ID_PERIODE bigint(20);
DECLARE ID_PER_MAJ bigint(20);
DECLARE EFF_DEB_MAJ decimal(10,0);
DECLARE EFF_FIN_MAJ decimal(10,0);

DECLARE liste_nocat_unique CURSOR FOR
(SELECT cqi.ID_PERIODE,cqi.NOCAT
FROM categorie_quittancement_individu cqi INNER JOIN V_selection_periode_nature_contrat_salaires vpns ON (cqi.ID_PERIODE=vpns.ID_PERIODE)
WHERE cqi.individu='TOUS'
AND FIND_IN_SET(cqi.ID_PERIODE, p_liste_ids_periodes)
GROUP BY cqi.ID_PERIODE
HAVING COUNT(DISTINCT cqi.NOCAT)<=1);

DECLARE liste_nocat_non_unique CURSOR FOR
(SELECT cqi.ID_PERIODE,cqi.NOCAT
FROM categorie_quittancement_individu cqi INNER JOIN V_selection_periode_nature_contrat_salaires vpns ON (cqi.ID_PERIODE=vpns.ID_PERIODE)
WHERE cqi.individu='TOUS'
 AND FIND_IN_SET(cqi.ID_PERIODE, p_liste_ids_periodes)
 AND (SELECT COUNT(DISTINCT cqi2.NOCAT) FROM categorie_quittancement_individu cqi2 WHERE cqi2.ID_PERIODE=cqi.ID_PERIODE)>1
GROUP BY cqi.ID_PERIODE, cqi.NOCAT);

DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

OPEN liste_nocat_unique;
IF (Select FOUND_ROWS()) > 0 THEN
	nocat_uni: LOOP 
	FETCH liste_nocat_unique INTO C_ID_PERIODE, C_NO_CAT;
		IF done THEN 
			LEAVE nocat_uni;
		END IF;
		-- Calcul effectifs
		SELECT 
    COUNT(DISTINCT (CASE
            WHEN (i.IDENTIFIANT_REPERTOIRE = '') THEN i.NTT
            ELSE i.IDENTIFIANT_REPERTOIRE
        END)), p.ID_PERIODE INTO EFF_DEB_MAJ, ID_PER_MAJ
FROM
    periodes_recues p
        INNER JOIN
    rattachement_declarations_recues rda ON (p.ID_PERIODE = rda.ID_PERIODE)
        INNER JOIN
    adhesion_etablissement_mois adh ON (adh.ID_ADH_ETAB_MOIS = rda.ID_ADH_ETAB_MOIS)
        INNER JOIN
    individu i ON (i.ID_ADH_ETAB_MOIS = adh.ID_ADH_ETAB_MOIS)
		INNER JOIN
	contrat_travail ct ON (ct.ID_INDIVIDU = i.ID_INDIVIDU)
		INNER JOIN
	affiliation a ON (a.ID_CONTRAT_TRAVAIL = ct.ID_CONTRAT_TRAVAIL)
		INNER JOIN
	base_assujettie ba ON (ba.ID_AFFILIATION = a.ID_AFFILIATION AND ba.DATE_DEB_RATTACHEMENT = p.DATE_DEBUT_PERIODE)
WHERE
    p.ID_PERIODE = C_ID_PERIODE;
					
		SELECT 
    COUNT(DISTINCT (CASE
            WHEN (i.IDENTIFIANT_REPERTOIRE = '') THEN i.NTT
            ELSE i.IDENTIFIANT_REPERTOIRE
        END)) INTO EFF_FIN_MAJ
FROM
    periodes_recues p
        INNER JOIN
    rattachement_declarations_recues rda ON (p.ID_PERIODE = rda.ID_PERIODE)
        INNER JOIN
    adhesion_etablissement_mois adh ON (adh.ID_ADH_ETAB_MOIS = rda.ID_ADH_ETAB_MOIS)
        INNER JOIN
    individu i ON (i.ID_ADH_ETAB_MOIS = adh.ID_ADH_ETAB_MOIS)
		INNER JOIN
	contrat_travail ct ON (ct.ID_INDIVIDU = i.ID_INDIVIDU)
		INNER JOIN
	affiliation a ON (a.ID_CONTRAT_TRAVAIL = ct.ID_CONTRAT_TRAVAIL)
		INNER JOIN
	base_assujettie ba ON (ba.ID_AFFILIATION = a.ID_AFFILIATION AND ba.DATE_FIN_RATTACHEMENT = p.DATE_FIN_PERIODE)
WHERE
    p.ID_PERIODE = C_ID_PERIODE;
        
UPDATE categorie_quittancement_individu cqi3
SET 
	cqi3.EFFECTIF_DEBUT = EFF_DEB_MAJ,
	cqi3.EFFECTIF_FIN = EFF_FIN_MAJ
WHERE
	cqi3.ID_PERIODE = ID_PER_MAJ;
END LOOP;
END IF;
CLOSE liste_nocat_unique;


BEGIN
DECLARE done2 INT DEFAULT FALSE;
DECLARE CONTINUE HANDLER FOR NOT FOUND SET done2 = TRUE;

OPEN liste_nocat_non_unique;
IF (SELECT FOUND_ROWS()) > 0 THEN
	nocat_non_uni : LOOP 
	FETCH liste_nocat_non_unique INTO C_ID_PERIODE, C_NO_CAT;
		IF done2 THEN 
			LEAVE nocat_non_uni;
		END IF;
		-- Calcul effectifs 
		SELECT 
    COUNT(DISTINCT (CASE
            WHEN (i.IDENTIFIANT_REPERTOIRE = '') THEN i.NTT
            ELSE i.IDENTIFIANT_REPERTOIRE
        END)), p.ID_PERIODE INTO EFF_DEB_MAJ, ID_PER_MAJ
FROM
    periodes_recues p
        INNER JOIN
    rattachement_declarations_recues rda ON (p.ID_PERIODE = rda.ID_PERIODE)
        INNER JOIN
    adhesion_etablissement_mois adh ON (adh.ID_ADH_ETAB_MOIS = rda.ID_ADH_ETAB_MOIS)
        INNER JOIN
    individu i ON (i.ID_ADH_ETAB_MOIS = adh.ID_ADH_ETAB_MOIS)
		INNER JOIN
	contrat_travail ct ON (ct.ID_INDIVIDU = i.ID_INDIVIDU)
		INNER JOIN
	affiliation a ON (a.ID_CONTRAT_TRAVAIL = ct.ID_CONTRAT_TRAVAIL)
		INNER JOIN
	base_assujettie ba ON (ba.ID_AFFILIATION = a.ID_AFFILIATION AND ba.DATE_DEB_RATTACHEMENT = p.DATE_DEBUT_PERIODE)
WHERE
    p.ID_PERIODE = C_ID_PERIODE
	AND (C_NO_CAT collate latin1_general_ci) = (COALESCE(IF((a.CODE_POPULATION = ''),
                    NULL,
                    a.CODE_POPULATION),
                (SELECT 
                        t.NOCAT
                    FROM
                        tarifs t
                    WHERE
                        ((t.NOCO = p.NUMERO_CONTRAT)
                            AND (t.DT_DEBUT_SIT <= p.DATE_DEBUT_PERIODE)
                            AND ((COALESCE(t.DT_FIN_SIT, 99999999) >= p.DATE_DEBUT_PERIODE)
                            OR ((CASE
                            WHEN (t.DT_FIN_SIT = 0) THEN 99999999
                            ELSE t.DT_FIN_SIT
                        END) >= p.DATE_DEBUT_PERIODE)))
                    ORDER BY t.DT_DEBUT_SIT
                    LIMIT 1)));

SELECT 
    COUNT(DISTINCT (CASE
            WHEN (i.IDENTIFIANT_REPERTOIRE = '') THEN i.NTT
            ELSE i.IDENTIFIANT_REPERTOIRE
        END)) INTO EFF_FIN_MAJ
FROM
periodes_recues p
        INNER JOIN
    rattachement_declarations_recues rda ON (p.ID_PERIODE = rda.ID_PERIODE)
        INNER JOIN
    adhesion_etablissement_mois adh ON (adh.ID_ADH_ETAB_MOIS = rda.ID_ADH_ETAB_MOIS)
        INNER JOIN
    individu i ON (i.ID_ADH_ETAB_MOIS = adh.ID_ADH_ETAB_MOIS)
		INNER JOIN
	contrat_travail ct ON (ct.ID_INDIVIDU = i.ID_INDIVIDU)
		INNER JOIN
	affiliation a ON (a.ID_CONTRAT_TRAVAIL = ct.ID_CONTRAT_TRAVAIL)
		INNER JOIN
	base_assujettie ba ON (ba.ID_AFFILIATION = a.ID_AFFILIATION AND ba.DATE_FIN_RATTACHEMENT = p.DATE_FIN_PERIODE)
WHERE
    p.ID_PERIODE = C_ID_PERIODE
	AND (C_NO_CAT collate latin1_general_ci) = (COALESCE(IF((a.CODE_POPULATION = ''),
                    NULL,
                    a.CODE_POPULATION),
                (SELECT 
                        t.NOCAT
                    FROM
                        tarifs t
                    WHERE
                        ((t.NOCO = p.NUMERO_CONTRAT)
                            AND (t.DT_DEBUT_SIT <= p.DATE_DEBUT_PERIODE)
                            AND ((COALESCE(t.DT_FIN_SIT, 99999999) >= p.DATE_DEBUT_PERIODE)
                            OR ((CASE
                            WHEN (t.DT_FIN_SIT = 0) THEN 99999999
                            ELSE t.DT_FIN_SIT
                        END) >= p.DATE_DEBUT_PERIODE)))
                    ORDER BY t.DT_DEBUT_SIT
                    LIMIT 1)));

		UPDATE categorie_quittancement_individu cqi3 
SET 
    cqi3.EFFECTIF_DEBUT = EFF_DEB_MAJ,
    cqi3.EFFECTIF_FIN = EFF_FIN_MAJ
WHERE
    cqi3.ID_PERIODE = ID_PER_MAJ
AND cqi3.NOCAT = (C_NO_CAT collate latin1_general_ci);
	END LOOP;
END IF;
CLOSE liste_nocat_non_unique;
END;
END$$
DELIMITER ;