DROP PROCEDURE IF EXISTS Recherche_gestionnaire_siren;
DELIMITER $$
CREATE PROCEDURE Recherche_gestionnaire_siren(IN n_siren TEXT)
BEGIN

DECLARE VAR_NB_GG bigint(10);		-- variable contenant le nb de groupe de gestion des contrats du SIREN
DECLARE VAR_GG char(100); 			-- variable contenant la liste des groupes de gestion des contrats du SIREN

DECLARE VAR_NB_GEST bigint(10);		-- variable contenant le nb de gestionnaires assignés aux périodes du SIREN
DECLARE VAR_GEST char(200); 		-- variable contenant la liste des gestionnaires assignés aux périodes du SIREN

DECLARE VAR_GG_AUTO char(100);		-- variable contenant le résultat de la recherche pour savoir si le groupe de gestion est "autorisé"
DECLARE VAR_GEST_EXISTE char(100);	-- variable contenant le résultat de la recherche de l'existance du gestionnaire
DECLARE VAR_RECHERCHE char(100);	-- variable contenant le résultat des recherches des groupes de gestion dans les domaines
DECLARE VAR_NB_FICTIF bigint(10);	-- variable contenant le nb de gestionnaire fictif répondant aux critères
DECLARE VAR_INCLUS_1 char(100);		-- variable contenant le domaine inclus du gestionnaire fictif
DECLARE VAR_EXCLUS char (100);		-- variable contenant le domaine exclus du gestionnaire fictif

DECLARE OUT_CODE_USER TEXT;			-- variable contenant le code de l'utilisateurs
DECLARE OUT_NOM_USER TEXT;			-- variable contenant le nom du gestionnaire
DECLARE OUT_PRENOM_USER TEXT;		-- variable contenant le prenom du gestionnaire 
DECLARE OUT_NIVEAU_1 TEXT;			-- variable contenant le niveau 1 d'habilitation du gestionnaire
DECLARE OUT_NIVEAU_2 TEXT;			-- variable contenant le niveau 2 d'habilitation du gestionnaire
DECLARE OUT_NIVEAU_3 TEXT;			-- variable contenant le niveau 3 d'habilitation du gestionnaire

-- Pour le SIREN en entrée : recherche de ses groupes de gestion et gestionnaires assignés (count et group_concat)
		-- requete de recherche des contrats valides liés au SIREN (via clients ou extensions_entreprise_affilié)
		
		SELECT count(distinct c.nmgrpges) as nbgg, group_concat(distinct c.nmgrpges order by c.nmgrpges separator '%') as gg
				INTO VAR_NB_GG, VAR_GG
		FROM contrats c
		inner join param_etat_contrat p on p.COETACO = c.coetaco and c.tmp_batch = p.tmp_batch
		left join clients cl on cl.nocli = c.nocli and c.tmp_batch = cl.tmp_batch and cl.nosiren = (n_siren collate latin1_general_ci)
		left join extensions_entreprises_affiliees ext on ext.noco=c.NOCO and ext.siren = (n_siren collate latin1_general_ci)
		WHERE c.tmp_batch IS FALSE
		AND p.ACTIF = 'O'
		AND (c.eligdsn = 'SRN' OR c.eligdsn = 'SRT')
		AND (c.dt_fin_sit IS NULL)
		AND (NOT EXISTS (SELECT 1 FROM param_siren_faux s WHERE s.SIREN=(n_siren collate latin1_general_ci)))
		and ( (cl.nosiren is null and ext.siren is not null) or (cl.nosiren is not null and ext.siren is null) );
		
-- si le nombre de groupe = 0
IF (VAR_NB_GG = 0) THEN
	-- !!!!!! cas non prévu mais existant
	-- => A qui associer ce SIREN ??? => A enlever du fichier 
	SELECT null, null, null, null, null, null
	INTO OUT_CODE_USER, OUT_PRENOM_USER, OUT_NOM_USER, OUT_NIVEAU_1, OUT_NIVEAU_2, OUT_NIVEAU_3;
	
-- sinon, si le nombre de groupe = 1
ELSEIF (VAR_NB_GG = 1) THEN
	
	-- recherche si le groupe est "autorisé"
		-- recherche si retour pour le gestionnaire fictif de type GEST_AUTRE avec GG_INCLUS_1 = '' and GG_INCLUS_2 = '' and GG_EXCLUS <> '' et GG_exclus like groupe du gestion du SIREN
		-- si retour => groupe "autorisé"
		-- si pas de retour => groupe non "autorisé"
		SELECT pug.code_user INTO VAR_GG_AUTO 
		FROM param_utilisateur_gestionnaire pug 
		WHERE pug.TYPE = 'GEST_AUTRE'
		AND pug.GG_INCLUS_1 = '' and pug.GG_INCLUS_2 = '' and pug.GG_EXCLUS <> ''
		AND pug.GG_EXCLUS like concat('%', (VAR_GG collate latin1_general_ci), '%');
	
	-- si le groupe n'est pas "autorisé"
	IF (VAR_GG_AUTO is null) THEN
		-- Cas 6.1 : 
		-- => le SIREN est associé au gestionnaire fictif de type : GEST_AUTRE avec GG_INCLUS_1 = '' and GG_INCLUS_2 = '' and GG_EXCLUS <> '' 
		SELECT pug.code_user, pug.PRENOM, pug.NOM, pug.NIVEAU_1, pug.NIVEAU_2, pug.NIVEAU_3 
		INTO OUT_CODE_USER, OUT_PRENOM_USER, OUT_NOM_USER, OUT_NIVEAU_1, OUT_NIVEAU_2, OUT_NIVEAU_3
		FROM param_utilisateur_gestionnaire pug 
		WHERE pug.TYPE = 'GEST_AUTRE'
		AND pug.GG_INCLUS_1 = '' and pug.GG_INCLUS_2 = '' and pug.GG_EXCLUS <> '';
		
	-- sinon (le groupe est "autorisé")
	ELSE 
	
		-- recherche du nombre de gestionnaires assignés aux dernières périodes des contrats du SIREN
		SELECT COUNT(DISTINCT(LIGNES)), group_concat(distinct lignes) 
		INTO VAR_NB_GEST, VAR_GEST
		FROM
		(SELECT _periode.A_TRAITER_PAR AS LIGNES FROM
			(SELECT pr.A_TRAITER_PAR, pr.NUMERO_CONTRAT, pr.DATE_FIN_PERIODE FROM periodes_recues pr where numero_contrat in (
			SELECT distinct c.noco
			FROM contrats c
			inner join param_etat_contrat p on p.COETACO = c.coetaco and c.tmp_batch = p.tmp_batch
			left join clients cl on cl.nocli = c.nocli and c.tmp_batch = cl.tmp_batch and cl.nosiren = (n_siren collate latin1_general_ci)
			left join extensions_entreprises_affiliees ext on ext.noco=c.NOCO and ext.siren = (n_siren collate latin1_general_ci)
			WHERE c.tmp_batch IS FALSE
			AND p.ACTIF = 'O'
			AND (c.eligdsn = 'SRN' OR c.eligdsn = 'SRT')
			AND (c.dt_fin_sit IS NULL)
			AND (NOT EXISTS (SELECT 1 FROM param_siren_faux s WHERE s.SIREN=(n_siren collate latin1_general_ci)))
			and ( (cl.nosiren is null and ext.siren is not null) or (cl.nosiren is not null and ext.siren is null) ))
			order by date_fin_periode DESC, date_creation desc) AS _periode
			GROUP BY _periode.NUMERO_CONTRAT) as _pug1ionnaire;
	
	
		-- si le nombre de gestionnaire assigné = 0
		IF (VAR_NB_GEST = 0) THEN
			-- Cas 1.3 + 2.3 : mono-groupe / aucun gestionnaire
			-- => Le SIREN est associé au gestionnaire « Non assigné » en groupe unitaire pour le groupe de gestion de ce contrat (infos des niveaux de ce gestionnaire)
				-- recherche du gestionnaire fictif de type : NON_ASS_GU et GG_EGAL = groupe de gestion du SIREN
			SELECT pug.code_user, pug.PRENOM, pug.NOM, pug.NIVEAU_1, pug.NIVEAU_2, pug.NIVEAU_3 
			INTO OUT_CODE_USER, OUT_PRENOM_USER, OUT_NOM_USER, OUT_NIVEAU_1, OUT_NIVEAU_2, OUT_NIVEAU_3
			FROM param_utilisateur_gestionnaire pug 
			WHERE pug.TYPE = 'NON_ASS_GU'
			AND pug.GG_EGAL = (VAR_GG collate latin1_general_ci);
			
		-- sinon, si le nombre de gestionnaire assigné = 1
		ELSEIF (VAR_NB_GEST = 1) THEN
		
			-- recherche si le gestionnaire existe
			SELECT pug.code_user
			INTO VAR_GEST_EXISTE
			FROM param_utilisateur_gestionnaire pug 
			WHERE pug.TYPE = 'GEST'
			AND pug.CODE_USER = (VAR_GEST collate latin1_general_ci);
			
			-- si le gestionnaire existe 
			IF (VAR_GEST_EXISTE is not null) THEN
				-- Cas 1.1 + Cas 2.1 : mono-groupe / mono-gestionnaire existant 
				-- => le SIREN est associé au gestionnaire existant (infos des niveaux de ce gestionnaire)
				SELECT pug.code_user, pug.PRENOM, pug.NOM, pug.NIVEAU_1, pug.NIVEAU_2, pug.NIVEAU_3 
				INTO OUT_CODE_USER, OUT_PRENOM_USER, OUT_NOM_USER, OUT_NIVEAU_1, OUT_NIVEAU_2, OUT_NIVEAU_3
				FROM param_utilisateur_gestionnaire pug 
				WHERE pug.TYPE = 'GEST'
				AND pug.CODE_USER = (VAR_GEST collate latin1_general_ci);
				
			-- sinon (le gestionnaire n'existe pas)
			ELSE
				-- Cas 1.2 + Cas 2.2 : mono-groupe / mono-gestionnaire non existant
				-- => Le SIREN est associé au gestionnaire « Autre » en groupe unitaire pour le groupe de gestion de ce contrat (infos des niveaux de ce gestionnaire)
					-- recherche du gestionnaire fictif de type : AUTRE_GU et GG_EGAL = groupe de gestion du SIREN
				SELECT pug.code_user, pug.PRENOM, pug.NOM, pug.NIVEAU_1, pug.NIVEAU_2, pug.NIVEAU_3 
				INTO OUT_CODE_USER, OUT_PRENOM_USER, OUT_NOM_USER, OUT_NIVEAU_1, OUT_NIVEAU_2, OUT_NIVEAU_3
				FROM param_utilisateur_gestionnaire pug 
				WHERE pug.TYPE = 'AUTRE_GU'
				AND pug.GG_EGAL = (VAR_GG collate latin1_general_ci);
			
			-- end (gestionnaire existant ou non)
			END IF;
			
		-- sinon (le nombre de gestionnaire assigné > 1)
		ELSE
			-- Cas 2.4 : mono-groupe / multi-gestionnaire
			-- => Le SIREN est associé au gestionnaire « Multi » en groupe unitaire pour le groupe de gestion de ces contrats (infos des niveaux de ce gestionnaire)
				-- recherche du gestionnaire fictif de type : MULTI_GU et GG_EGAL = groupe de gestion du SIREN
			SELECT pug.code_user, pug.PRENOM, pug.NOM, pug.NIVEAU_1, pug.NIVEAU_2, pug.NIVEAU_3 
			INTO OUT_CODE_USER, OUT_PRENOM_USER, OUT_NOM_USER, OUT_NIVEAU_1, OUT_NIVEAU_2, OUT_NIVEAU_3
			FROM param_utilisateur_gestionnaire pug 
			WHERE pug.TYPE = 'MULTI_GU'
			AND pug.GG_EGAL = (VAR_GG collate latin1_general_ci);
		
		-- end (nombre de gestionnaire assigné)
		END IF;
		
	-- end (group "autorisé" ou non)
	END IF;
	
-- sinon (nombre de groupe > 1)
ELSE
	
	-- recherche du nombre de gestionnaires assignés aux dernières périodes des contrats du SIREN
	SELECT COUNT(DISTINCT(LIGNES)), group_concat(distinct lignes) 
	INTO VAR_NB_GEST, VAR_GEST
	FROM
	(SELECT _periode.A_TRAITER_PAR AS LIGNES FROM
		(SELECT pr.A_TRAITER_PAR, pr.NUMERO_CONTRAT, pr.DATE_FIN_PERIODE FROM periodes_recues pr where numero_contrat in (
		SELECT distinct c.noco
		FROM contrats c
		inner join param_etat_contrat p on p.COETACO = c.coetaco and c.tmp_batch = p.tmp_batch
		left join clients cl on cl.nocli = c.nocli and c.tmp_batch = cl.tmp_batch and cl.nosiren = (n_siren collate latin1_general_ci)
		left join extensions_entreprises_affiliees ext on ext.noco=c.NOCO and ext.siren = (n_siren collate latin1_general_ci)
		WHERE c.tmp_batch IS FALSE
		AND p.ACTIF = 'O'
		AND (c.eligdsn = 'SRN' OR c.eligdsn = 'SRT')
		AND (c.dt_fin_sit IS NULL)
		AND (NOT EXISTS (SELECT 1 FROM param_siren_faux s WHERE s.SIREN=(n_siren collate latin1_general_ci)))
		and ( (cl.nosiren is null and ext.siren is not null) or (cl.nosiren is not null and ext.siren is null) ))
		order by date_fin_periode DESC, date_creation desc) AS _periode
		GROUP BY _periode.NUMERO_CONTRAT) as _pug1ionnaire;
	
	
	-- recherche si les groupes du SIREN sont mono-domaine
		-- recherche du gestionnaire fictif de type GEST_MULTI avec GG_INCLUS_1 <> '' and GG_INCLUS_2 = '' and GG_EXCLUS = '' et GG_INCLUS_1 like groupes du gestion du SIREN
		-- si retour => groupes mono-domaine
		-- si pas de retour => groupes non mono-domaine
	SELECT pug.code_user
	INTO VAR_RECHERCHE
	FROM param_utilisateur_gestionnaire pug 
	WHERE pug.TYPE = 'GEST_MULTI'
	AND pug.GG_INCLUS_1 <> '' and pug.GG_INCLUS_2 = '' and pug.GG_EXCLUS = ''
	AND pug.GG_INCLUS_1 like (concat('%', (VAR_GG collate latin1_general_ci), '%'));
		
		
	-- si groupes mono-domaine
	IF (VAR_RECHERCHE is not null) THEN
	
		-- si le nombre de gestionnaire assigné = 0
		IF (VAR_NB_GEST = 0) THEN
			-- Cas 3.3 : multi-groupe / mono-domaine / aucun gestionnaire
			-- => Le SIREN est associé au gestionnaire « Non assigné » en multi-groupes de ce domaine (infos des niveaux de ce gestionnaire)
				-- recherche du gestionnaire fictif de type : NON_ASS_MULTI avec GG_INCLUS_1 <> '' and GG_INCLUS_2 = '' and GG_EXCLUS = '' et GG_INCLUS_1 like groupes du gestion du SIREN
			SELECT pug.code_user, pug.PRENOM, pug.NOM, pug.NIVEAU_1, pug.NIVEAU_2, pug.NIVEAU_3 
			INTO OUT_CODE_USER, OUT_PRENOM_USER, OUT_NOM_USER, OUT_NIVEAU_1, OUT_NIVEAU_2, OUT_NIVEAU_3
			FROM param_utilisateur_gestionnaire pug 
			WHERE pug.TYPE = 'NON_ASS_MULTI'
			AND pug.GG_INCLUS_1 <> '' and pug.GG_INCLUS_2 = '' and pug.GG_EXCLUS = ''
			AND pug.GG_INCLUS_1 like (concat('%', (VAR_GG collate latin1_general_ci), '%'));
		
		-- sinon, si le nombre de gestionnaire assigné = 1
		ELSEIF (VAR_NB_GEST = 1) THEN
		
			-- recherche si le gestionnaire existe
			SELECT pug.code_user
			INTO VAR_GEST_EXISTE
			FROM param_utilisateur_gestionnaire pug 
			WHERE pug.TYPE = 'GEST'
			AND pug.CODE_USER = (VAR_GEST collate latin1_general_ci);
			
			-- si le gestionnaire existe 
			IF (VAR_GEST_EXISTE is not null) THEN
				-- Cas 3.1 : multi-groupe / mono-domaine / mono-gestionnaire existant 
				-- => Le SIREN est associé à ce gestionnaire (infos des niveaux du gestionnaire en multi-groupe du domaine)
						-- Pour les infos de niveaux : recherche du gestionnaire fictif de type : GEST_MULTI avec GG_INCLUS_1 <> '' and GG_INCLUS_2 = '' and GG_EXCLUS = '' et GG_INCLUS_1 like groupes du gestion du SIREN
				SELECT pug.code_user, pug.PRENOM, pug.NOM
				INTO OUT_CODE_USER, OUT_PRENOM_USER, OUT_NOM_USER
				FROM param_utilisateur_gestionnaire pug 
				WHERE pug.TYPE = 'GEST'
				AND pug.CODE_USER = (VAR_GEST collate latin1_general_ci);
				
				SELECT pug.NIVEAU_1, pug.NIVEAU_2, pug.NIVEAU_3 
				INTO OUT_NIVEAU_1, OUT_NIVEAU_2, OUT_NIVEAU_3
				FROM param_utilisateur_gestionnaire pug 
				WHERE pug.TYPE = 'GEST_MULTI'
				AND pug.GG_INCLUS_1 <> '' and pug.GG_INCLUS_2 = '' and pug.GG_EXCLUS = ''
				AND pug.GG_INCLUS_1 like (concat('%', (VAR_GG collate latin1_general_ci), '%'));
					
			-- sinon (le gestionnaire n'existe pas)
			ELSE
				-- Cas 3.2 : multi-groupe / mono-domaine / mono-gestionnaire non existant
				-- => Le SIREN est associé au gestionnaire « Autre » en multi-groupes de ce domaine (infos des niveaux de ce gestionnaire)
					-- recherche du gestionnaire fictif de type : AUTRE_MULTI avec GG_INCLUS_1 <> '' and GG_INCLUS_2 = '' and GG_EXCLUS = '' et GG_INCLUS_1 like groupes du gestion du SIREN
				SELECT pug.code_user, pug.PRENOM, pug.NOM, pug.NIVEAU_1, pug.NIVEAU_2, pug.NIVEAU_3 
				INTO OUT_CODE_USER, OUT_PRENOM_USER, OUT_NOM_USER, OUT_NIVEAU_1, OUT_NIVEAU_2, OUT_NIVEAU_3
				FROM param_utilisateur_gestionnaire pug 
				WHERE pug.TYPE = 'AUTRE_MULTI'
				AND pug.GG_INCLUS_1 <> '' and pug.GG_INCLUS_2 = '' and pug.GG_EXCLUS = ''
				AND pug.GG_INCLUS_1 like (concat('%', (VAR_GG collate latin1_general_ci), '%'));
			
			-- end (gestionnaire existant ou non)
			END IF;
			
		-- sinon (le nombre de gestionnaire assigné > 1)
		ELSE
			-- Cas 3.4 : multi-groupe / mono-domaine / multi-gestionnaire
			-- => Le SIREN est associé au gestionnaire « Multi » en multi-groupes de ce domaine (infos des niveaux de ce gestionnaire)
				-- recherche du gestionnaire fictif de type : MULTI_MULTI avec GG_INCLUS_1 <> '' and GG_INCLUS_2 = '' and GG_EXCLUS = '' et GG_INCLUS_1 like groupes du gestion du SIREN
			SELECT pug.code_user, pug.PRENOM, pug.NOM, pug.NIVEAU_1, pug.NIVEAU_2, pug.NIVEAU_3 
			INTO OUT_CODE_USER, OUT_PRENOM_USER, OUT_NOM_USER, OUT_NIVEAU_1, OUT_NIVEAU_2, OUT_NIVEAU_3
			FROM param_utilisateur_gestionnaire pug 
			WHERE pug.TYPE = 'MULTI_MULTI'
			AND pug.GG_INCLUS_1 <> '' and pug.GG_INCLUS_2 = '' and pug.GG_EXCLUS = ''
			AND pug.GG_INCLUS_1 like (concat('%', (VAR_GG collate latin1_general_ci), '%'));
		
		-- end (nombre de gestionnaire assigné)
		END IF;
		
	-- sinon (groupes non mono_domaine)
	ELSE
		
		-- recherche si les groupes du SIREN sont uniquement sur les 2 domaines
			-- recherche du gestionnaire fictif de type GEST_MULTI avec GG_INCLUS_1 <> '' and GG_INCLUS_2 <> '' and GG_EXCLUS = '' 
				-- et concat(GG_INCLUS_1, GG_INCLUS_2) like groupes du gestion du SIREN
			-- si retour => groupes uniquement sur les 2 domaines
			-- si pas de retour => groupes non uniquement sur les 2 domaines
		SELECT pug.code_user
		INTO VAR_RECHERCHE
		FROM param_utilisateur_gestionnaire pug 
		WHERE pug.TYPE = 'GEST_MULTI'
		AND pug.GG_INCLUS_1 <> '' and pug.GG_INCLUS_2 <> '' and pug.GG_EXCLUS = ''
		AND concat(pug.GG_INCLUS_1, pug.GG_INCLUS_2) like (concat('%', (VAR_GG collate latin1_general_ci), '%'));
		
		-- si groupes uniquement sur les 2 domaines
		IF (VAR_RECHERCHE is not null) THEN
			
			-- si le nombre de gestionnaire assigné = 0
			IF (VAR_NB_GEST = 0) THEN
				-- Cas 4.3 : multi-groupe / 2 domaines / aucun gestionnaire
				-- => Le SIREN est associé au gestionnaire « Non assigné » en multi-groupes des 2 domaines (infos des niveaux de ce gestionnaire)
					-- recherche du gestionnaire fictif de type : NON_ASS_MULTI avec GG_INCLUS_1 <> '' and GG_INCLUS_2 <> '' and GG_EXCLUS = '' (pas sur que utile : et concat(GG_INCLUS_1, GG_INCLUS_2) like groupes du gestion du SIREN)
				SELECT pug.code_user, pug.PRENOM, pug.NOM, pug.NIVEAU_1, pug.NIVEAU_2, pug.NIVEAU_3 
				INTO OUT_CODE_USER, OUT_PRENOM_USER, OUT_NOM_USER, OUT_NIVEAU_1, OUT_NIVEAU_2, OUT_NIVEAU_3
				FROM param_utilisateur_gestionnaire pug 
				WHERE pug.TYPE = 'NON_ASS_MULTI'
				AND pug.GG_INCLUS_1 <> '' and pug.GG_INCLUS_2 <> '' and pug.GG_EXCLUS = '';
			
			-- sinon, si le nombre de gestionnaire assigné = 1
			ELSEIF (VAR_NB_GEST = 1) THEN
			
				-- recherche si le gestionnaire existe
				SELECT pug.code_user
				INTO VAR_GEST_EXISTE
				FROM param_utilisateur_gestionnaire pug 
				WHERE pug.TYPE = 'GEST'
				AND pug.CODE_USER = (VAR_GEST collate latin1_general_ci);
				
				-- si le gestionnaire existe 
				IF (VAR_GEST_EXISTE is not null) THEN
					-- Cas 4.1 : multi-groupe / 2 domaines / mono-gestionnaire existant 
					-- => Le SIREN est associé à ce gestionnaire (infos des niveaux du gestionnaire en multi-groupes des 2 domaines)
							-- Pour les infos de niveaux : recherche du gestionnaire fictif de type : GEST_MULTI avec GG_INCLUS_1 <> '' and GG_INCLUS_2 <> '' and GG_EXCLUS = '' (pas sur que utile : et concat(GG_INCLUS_1, GG_INCLUS_2) like groupes du gestion du SIREN)
					SELECT pug.code_user, pug.PRENOM, pug.NOM
					INTO OUT_CODE_USER, OUT_PRENOM_USER, OUT_NOM_USER
					FROM param_utilisateur_gestionnaire pug 
					WHERE pug.TYPE = 'GEST'
					AND pug.CODE_USER = (VAR_GEST collate latin1_general_ci);
					
					SELECT pug.NIVEAU_1, pug.NIVEAU_2, pug.NIVEAU_3 
					INTO OUT_NIVEAU_1, OUT_NIVEAU_2, OUT_NIVEAU_3
					FROM param_utilisateur_gestionnaire pug 
					WHERE pug.TYPE = 'GEST_MULTI'
					AND pug.GG_INCLUS_1 <> '' and pug.GG_INCLUS_2 <> '' and pug.GG_EXCLUS = '';
					
				-- sinon (le gestionnaire n'existe pas)
				ELSE
					-- Cas 4.2 : multi-groupe / 2 domaines / mono-gestionnaire non existant
					-- => Le SIREN est associé au gestionnaire « Autre » en multi-groupes des 2 domaines (infos des niveaux de ce gestionnaire)
						-- recherche du gestionnaire fictif de type : AUTRE_MULTI avec GG_INCLUS_1 <> '' and GG_INCLUS_2 <>'' and GG_EXCLUS = '' (pas sur que utile : et concat(GG_INCLUS_1, GG_INCLUS_2) like groupes du gestion du SIREN)
					SELECT pug.code_user, pug.PRENOM, pug.NOM, pug.NIVEAU_1, pug.NIVEAU_2, pug.NIVEAU_3 
					INTO OUT_CODE_USER, OUT_PRENOM_USER, OUT_NOM_USER, OUT_NIVEAU_1, OUT_NIVEAU_2, OUT_NIVEAU_3
					FROM param_utilisateur_gestionnaire pug 
					WHERE pug.TYPE = 'AUTRE_MULTI'
					AND pug.GG_INCLUS_1 <> '' and pug.GG_INCLUS_2 <> '' and pug.GG_EXCLUS = '';
				
				-- end (gestionnaire existant ou non)
				END IF;
				
			-- sinon (le nombre de gestionnaire assigné > 1)
			ELSE
				-- Cas 4.4 : multi-groupe / 2 domaines / multi-gestionnaire
				-- => Le SIREN est associé au gestionnaire « Multi » en multi-groupes des 2 domaines (infos des niveaux de ce gestionnaire)
					-- recherche du gestionnaire fictif de type : MULTI_MULTI avec GG_INCLUS_1 <> '' and GG_INCLUS_2 <> '' and GG_EXCLUS = '' (pas sur que utile : et concat(GG_INCLUS_1, GG_INCLUS_2) like groupes du gestion du SIREN)
				SELECT pug.code_user, pug.PRENOM, pug.NOM, pug.NIVEAU_1, pug.NIVEAU_2, pug.NIVEAU_3 
				INTO OUT_CODE_USER, OUT_PRENOM_USER, OUT_NOM_USER, OUT_NIVEAU_1, OUT_NIVEAU_2, OUT_NIVEAU_3
				FROM param_utilisateur_gestionnaire pug 
				WHERE pug.TYPE = 'MULTI_MULTI'
				AND pug.GG_INCLUS_1 <> '' and pug.GG_INCLUS_2 <> '' and pug.GG_EXCLUS = '';
			
			-- end (nombre de gestionnaire assigné)
			END IF;
		
		-- sinon (groupes non uniquement sur les 2 domaines)
		ELSE
			
			-- recherche si les groupes du SIREN sont sur 1 seul domaine + autre
				-- recherche du gestionnaire fictif de type GEST_MULTI avec GG_INCLUS_1 <> '' and GG_INCLUS_2 = '' and GG_EXCLUS <> '' 
					-- on a au moins 1 groupe dans le inclus et aucun groupe dans le exclus
				-- si retour = 0 => groupes sur aucun domaine
				-- si retour = 1 => groupes sur 1 seul domaine + autre
				-- si retour = 2 => groupes sur 2 domaine + autre
			SELECT count(distinct pug2.code_user), pug2.GG_INCLUS_1, pug2.GG_EXCLUS INTO VAR_NB_FICTIF, VAR_INCLUS_1, VAR_EXCLUS
			FROM (
				SELECT distinct c.nmgrpges
				FROM contrats c
				inner join param_etat_contrat p on p.COETACO = c.coetaco and c.tmp_batch = p.tmp_batch
				left join clients cl on cl.nocli = c.nocli and c.tmp_batch = cl.tmp_batch and cl.nosiren = (n_siren collate latin1_general_ci)
				left join extensions_entreprises_affiliees ext on ext.noco=c.NOCO and ext.siren = (n_siren collate latin1_general_ci)
				WHERE c.tmp_batch IS FALSE
				AND p.ACTIF = 'O'
				AND (c.eligdsn = 'SRN' OR c.eligdsn = 'SRT')
				AND (c.dt_fin_sit IS NULL)
				AND (NOT EXISTS (SELECT 1 FROM param_siren_faux s WHERE s.SIREN=(n_siren collate latin1_general_ci)))
				and ( (cl.nosiren is null and ext.siren is not null) or (cl.nosiren is not null and ext.siren is null) )
			) as tmp_siren_liste_gg
			inner join param_utilisateur_gestionnaire pug2 
				on pug2.GG_inclus_1 like concat('%',tmp_siren_liste_gg.nmgrpges,'%') 
				and pug2.GG_exclus not like concat('%',tmp_siren_liste_gg.nmgrpges,'%')
				and concat(pug2.GG_inclus_1, ',', pug2.GG_exclus) not like concat('%', (VAR_GG collate latin1_general_ci), '%') 
				and concat(pug2.GG_exclus, ',', pug2.GG_inclus_1) not like concat('%', (VAR_GG collate latin1_general_ci), '%') 
				and pug2.`TYPE` = 'GEST_MULTI'
				and pug2.GG_inclus_1 <> '' and pug2.GG_INCLUS_2 = '' and pug2.GG_EXCLUS <> '';
				
			-- si groupes sur aucun domaine
			IF (VAR_NB_FICTIF = 0) THEN
				-- Cas 6.1 : 
				-- => le SIREN est associé au gestionnaire fictif de type : GEST_AUTRE avec GG_INCLUS_1 = '' and GG_INCLUS_2 = '' and GG_EXCLUS <> '' 
				SELECT pug.code_user, pug.PRENOM, pug.NOM, pug.NIVEAU_1, pug.NIVEAU_2, pug.NIVEAU_3 
				INTO OUT_CODE_USER, OUT_PRENOM_USER, OUT_NOM_USER, OUT_NIVEAU_1, OUT_NIVEAU_2, OUT_NIVEAU_3
				FROM param_utilisateur_gestionnaire pug 
				WHERE pug.TYPE = 'GEST_AUTRE'
				AND pug.GG_INCLUS_1 = '' and pug.GG_INCLUS_2 = '' and pug.GG_EXCLUS <> '';
				
			-- sinon, si groupes uniquement sur 1 seul domaine + autre
			ELSEIF (VAR_NB_FICTIF = 1) THEN
			
				-- si le nombre de gestionnaire assigné = 0
				IF (VAR_NB_GEST = 0) THEN
					-- Cas 5.3 : multi-groupe / 1 seul domaine + autre / aucun gestionnaire
					-- => Le SIREN est associé au gestionnaire « Non assigné » pour le domaine et autre (infos des niveaux de ce gestionnaire)
						-- recherche du gestionnaire fictif de type : NON_ASS_MULTI avec GG_INCLUS_1 <> '' and GG_INCLUS_2 = '' and GG_EXCLUS <> ''
					SELECT pug.code_user, pug.PRENOM, pug.NOM, pug.NIVEAU_1, pug.NIVEAU_2, pug.NIVEAU_3 
					INTO OUT_CODE_USER, OUT_PRENOM_USER, OUT_NOM_USER, OUT_NIVEAU_1, OUT_NIVEAU_2, OUT_NIVEAU_3
					FROM param_utilisateur_gestionnaire pug 
					WHERE pug.TYPE = 'NON_ASS_MULTI'
					AND pug.GG_INCLUS_1 = (VAR_INCLUS_1 collate latin1_general_ci) and pug.GG_INCLUS_2 = '' and pug.GG_EXCLUS = (VAR_EXCLUS collate latin1_general_ci);
				
				-- sinon, si le nombre de gestionnaire assigné = 1
				ELSEIF (VAR_NB_GEST = 1) THEN
				
					-- recherche si le gestionnaire existe
					SELECT pug.code_user
					INTO VAR_GEST_EXISTE
					FROM param_utilisateur_gestionnaire pug 
					WHERE pug.TYPE = 'GEST'
					AND pug.CODE_USER = (VAR_GEST collate latin1_general_ci);
					
					-- si le gestionnaire existe 
					IF (VAR_GEST_EXISTE is not null) THEN
						-- Cas 5.1 : multi-groupe / 1 seul domaine + autre / mono-gestionnaire existant 
						-- => Le SIREN est associé à ce gestionnaire (infos des niveaux du gestionnaire en multi-groupes des 2 domaines)
								-- Pour les infos de niveaux : recherche du gestionnaire fictif de type : GEST_MULTI avec GG_INCLUS_1 <> '' and GG_INCLUS_2 = '' and GG_EXCLUS <> ''
						SELECT pug.code_user, pug.PRENOM, pug.NOM
						INTO OUT_CODE_USER, OUT_PRENOM_USER, OUT_NOM_USER
						FROM param_utilisateur_gestionnaire pug 
						WHERE pug.TYPE = 'GEST'
						AND pug.CODE_USER = (VAR_GEST collate latin1_general_ci);
						
						SELECT pug.NIVEAU_1, pug.NIVEAU_2, pug.NIVEAU_3 
						INTO OUT_NIVEAU_1, OUT_NIVEAU_2, OUT_NIVEAU_3
						FROM param_utilisateur_gestionnaire pug 
						WHERE pug.TYPE = 'GEST_MULTI'
						AND pug.GG_INCLUS_1 = (VAR_INCLUS_1 collate latin1_general_ci) and pug.GG_INCLUS_2 = '' and pug.GG_EXCLUS = (VAR_EXCLUS collate latin1_general_ci);
						
					-- sinon (le gestionnaire n'existe pas)
					ELSE
						-- Cas 5.2 : multi-groupe / 1 seul domaine + autre / mono-gestionnaire non existant
						-- => Le SIREN est associé au gestionnaire « Autre » en multi-groupes des 2 domaines (infos des niveaux de ce gestionnaire)
							-- recherche du gestionnaire fictif de type : AUTRE_MULTI avec GG_INCLUS_1 <> '' and GG_INCLUS_2 ='' and GG_EXCLUS <> ''
						SELECT pug.code_user, pug.PRENOM, pug.NOM, pug.NIVEAU_1, pug.NIVEAU_2, pug.NIVEAU_3 
						INTO OUT_CODE_USER, OUT_PRENOM_USER, OUT_NOM_USER, OUT_NIVEAU_1, OUT_NIVEAU_2, OUT_NIVEAU_3
						FROM param_utilisateur_gestionnaire pug 
						WHERE pug.TYPE = 'AUTRE_MULTI'
						AND pug.GG_INCLUS_1 = (VAR_INCLUS_1 collate latin1_general_ci) and pug.GG_INCLUS_2 = '' and pug.GG_EXCLUS = (VAR_EXCLUS collate latin1_general_ci);
						
					-- end (gestionnaire existant ou non)
					END IF;
					
				-- sinon (le nombre de gestionnaire assigné > 1)
				ELSE
					-- Cas 5.4 : multi-groupe / 1 seul domaine + autre / multi-gestionnaire
					-- => Le SIREN est associé au gestionnaire « Multi » en multi-groupes des 2 domaines (infos des niveaux de ce gestionnaire)
						-- recherche du gestionnaire fictif de type : MULTI_MULTI avec GG_INCLUS_1 <> '' and GG_INCLUS_2 = '' and GG_EXCLUS <> ''
					SELECT pug.code_user, pug.PRENOM, pug.NOM, pug.NIVEAU_1, pug.NIVEAU_2, pug.NIVEAU_3 
					INTO OUT_CODE_USER, OUT_PRENOM_USER, OUT_NOM_USER, OUT_NIVEAU_1, OUT_NIVEAU_2, OUT_NIVEAU_3
					FROM param_utilisateur_gestionnaire pug 
					WHERE pug.TYPE = 'MULTI_MULTI'
					AND pug.GG_INCLUS_1 = (VAR_INCLUS_1 collate latin1_general_ci) and pug.GG_INCLUS_2 = '' and pug.GG_EXCLUS = (VAR_EXCLUS collate latin1_general_ci);
					
				-- end (nombre de gestionnaire assigné)
				END IF;
			
			-- sinon (groupes sur 2 + autre)
			ELSE
				-- Cas 7.1 : reste les cas de groupes sur 2 domaines + autres
				-- !!!!!! cas non prévu mais existant
					-- => A qui associer ce SIREN ???
				SELECT concat(pug.code_user, '_2DA'), pug.PRENOM, pug.NOM, pug.NIVEAU_1, pug.NIVEAU_2, pug.NIVEAU_3 
				INTO OUT_CODE_USER, OUT_PRENOM_USER, OUT_NOM_USER, OUT_NIVEAU_1, OUT_NIVEAU_2, OUT_NIVEAU_3
				FROM param_utilisateur_gestionnaire pug 
				WHERE pug.TYPE = 'MULTI_MULTI'
				AND pug.GG_INCLUS_1 <> '' and pug.GG_INCLUS_2 <> '' and pug.GG_EXCLUS = '';
				-- !!!!!!!!!!!! A voir si on garde ce gest qui est le gest multi 2 domaines ou si on fait un autre gest fictif !!!!!!!!!!!
				
			
			-- end (groupe sur aucun domaine ou 1 seul domaine + autre ou 2 domaines + autre)
			END IF;
			
		-- end (groupes uniquement sur les 2 domaines ou non)
		END IF;
		
	-- end (groupes mono-domaine ou non)
	END IF;
	
-- end (nb de groupes de gestion)
END IF;
-- Select pour renvoyer la ligne avec les données au traitements java
SELECT OUT_CODE_USER, OUT_NOM_USER, OUT_PRENOM_USER, OUT_NIVEAU_1, OUT_NIVEAU_2, OUT_NIVEAU_3;
END$$
DELIMITER ;

