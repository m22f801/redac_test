
DROP PROCEDURE IF EXISTS Inserer_TRANCHE_CATEGORIE_BASE_ASSUJETTIE;

DELIMITER $$
CREATE PROCEDURE Inserer_TRANCHE_CATEGORIE_BASE_ASSUJETTIE(IN p_audit_nom_batch VARCHAR(20))
BEGIN
INSERT INTO TRANCHE_CATEGORIE_BASE_ASSUJETTIE 
(
ID_PERIODE
,ID_BASE_ASSUJETTIE
,MOIS_RATTACHEMENT
,ID_AFFILIATION
,DATE_DEB_RATTACHEMENT
,NUM_TRANCHE
,NOCAT
,LIB_TRANCHE
,MT_TRANCHE
,MT_COTISATION
,TMP_BATCH
,USER_CREATION
)
SELECT 
sel.ID_PERIODE
,sel.ID_BASE_ASSUJETTIE
,sel.MOIS_RATTACHEMENT
,sel.ID_AFFILIATION
,sel.DATE_DEB_RATTACHEMENT
,p.NUM_TRANCHE
,t.NOCAT
,p.LIB_TRANCHE
,
coalesce(
(SELECT SUM(c.MONTANT_COMPO_BASE_ASSUJ) 
FROM COMPOSANT_BASE_ASSUJETTIE c 
WHERE c.ID_BASE_ASSUJETTIE = sel.ID_BASE_ASSUJETTIE
AND c.TYPE_COMPO_BASE_ASSUJ IN (select pc.NUM_TCBA
                                                                             from PARAM_NATURE_BASE_COMPOSANTS pc 
                                                                             where p.CONBCOT=pc.CONBCOT
                                                                            and p.NUM_TRANCHE=pc.NUM_TRANCHE
                                                                             and p.TXCALCU_REMPLI=pc.TXCALCU_REMPLI
                                                                              and pc.mode_conso='S' and pc.TMP_BATCH IS FALSE)
AND c.TMP_BATCH IS FALSE
), 0)   MT_TRANCHE
, null MT_COTISATION
, sel.tmp_batch
, p_audit_nom_batch
from
V_Selection_base_assujettie sel
left join TARIFS t ON    sel.NUMERO_CONTRAT = t.NOCO 
                                  AND sel.NOCAT = t.nocat
                                  AND     t.DT_DEBUT_SIT <= sel.DATE_FIN_RATTACHEMENT 
                                  AND (coalesce(t.DT_FIN_SIT, 99999999) >= sel.DATE_FIN_RATTACHEMENT OR (CASE WHEN (t.DT_FIN_SIT = 0) THEN 99999999 ELSE t.DT_FIN_SIT END) >= sel.DATE_FIN_RATTACHEMENT)
left join PARAM_NATURE_BASE p on t.CONBCOT=p.CONBCOT
                                                                   and (case when coalesce(t.txcalcu,0)>0 then 'O' else 'N' end) =p.TXCALCU_REMPLI
where not isnull(t.NOCO) and not isnull(t.NOCAT) and not isnull(p.NUM_TRANCHE) 
order by ID_PERIODE, ID_BASE_ASSUJETTIE;
END$$
DELIMITER ;

DROP PROCEDURE IF EXISTS Inserer_TRANCHE_CATEGORIE_BASE_ASSUJETTIE_Pour_Periodes;

DELIMITER $$
CREATE PROCEDURE Inserer_TRANCHE_CATEGORIE_BASE_ASSUJETTIE_Pour_Periodes(IN p_audit_nom_batch VARCHAR(20), IN p_liste_ids_periodes TEXT)
BEGIN
INSERT INTO TRANCHE_CATEGORIE_BASE_ASSUJETTIE 
(
ID_PERIODE
,ID_BASE_ASSUJETTIE
,MOIS_RATTACHEMENT
,ID_AFFILIATION
,DATE_DEB_RATTACHEMENT
,NUM_TRANCHE
,NOCAT
,LIB_TRANCHE
,MT_TRANCHE
,MT_COTISATION
,TMP_BATCH
,USER_CREATION
)
SELECT 
    sel.ID_PERIODE,
    sel.ID_BASE_ASSUJETTIE,
    sel.MOIS_RATTACHEMENT,
    sel.ID_AFFILIATION,
    sel.DATE_DEB_RATTACHEMENT,
    p.NUM_TRANCHE,
    t.NOCAT,
    p.LIB_TRANCHE,
    (SELECT 
            SUM(c.MONTANT_COMPO_BASE_ASSUJ)
        FROM
            COMPOSANT_BASE_ASSUJETTIE c
        WHERE
            c.ID_BASE_ASSUJETTIE = sel.ID_BASE_ASSUJETTIE
                AND c.TYPE_COMPO_BASE_ASSUJ IN (SELECT 
                    pc.NUM_TCBA
                FROM
                    PARAM_NATURE_BASE_COMPOSANTS pc
                WHERE
                    p.CONBCOT = pc.CONBCOT
                        AND p.NUM_TRANCHE = pc.NUM_TRANCHE
                        AND p.TXCALCU_REMPLI = pc.TXCALCU_REMPLI
                        AND pc.mode_conso = 'S'
                        AND pc.TMP_BATCH IS FALSE)
                AND c.TMP_BATCH IS FALSE) MT_TRANCHE,
    NULL MT_COTISATION,
    sel.tmp_batch,
    p_audit_nom_batch
FROM
    V_Selection_base_assujettie sel
        LEFT JOIN
    TARIFS t ON sel.NUMERO_CONTRAT = t.NOCO
        AND sel.NOCAT = t.nocat
        AND t.DT_DEBUT_SIT <= sel.DATE_FIN_RATTACHEMENT
        AND (COALESCE(t.DT_FIN_SIT, 99999999) >= sel.DATE_FIN_RATTACHEMENT
        OR (CASE
        WHEN (t.DT_FIN_SIT = 0) THEN 99999999
        ELSE t.DT_FIN_SIT
    END) >= sel.DATE_FIN_RATTACHEMENT)

LEFT JOIN PARAM_NATURE_BASE p ON t.CONBCOT = p.CONBCOT
        AND (CASE
        WHEN COALESCE(t.txcalcu, 0) > 0 THEN 'O'
        ELSE 'N'
    END) = p.TXCALCU_REMPLI
WHERE
    NOT ISNULL(t.NOCO)
        AND NOT ISNULL(t.NOCAT)
        AND NOT ISNULL(p.NUM_TRANCHE)
        AND FIND_IN_SET(sel.ID_PERIODE, p_liste_ids_periodes) > 0
ORDER BY ID_PERIODE , ID_BASE_ASSUJETTIE;
END$$
DELIMITER ;