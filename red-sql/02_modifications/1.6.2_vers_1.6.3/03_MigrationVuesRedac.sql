-- TM2752
DROP VIEW IF EXISTS V_Trace_tranche_categorie_base_assujettie;
CREATE VIEW V_Trace_tranche_categorie_base_assujettie AS SELECT DISTINCT
        `sel`.`id_periode` AS `ID_PERIODE`,
        `sel`.`numero_contrat` AS `NUMERO_CONTRAT`,
        `sel`.`DATE_DEB_RATTACHEMENT` AS `DATE_DEB_RATTACHEMENT`,
        `sel`.`DATE_FIN_RATTACHEMENT` AS `DATE_FIN_RATTACHEMENT`,
        `sel`.`NOCAT` AS `NOCAT`,
        CONCAT('La situation tarifaire [numContrat=',
                `sel`.`numero_contrat`,
                ', numCategorie=',
                `sel`.`NOCAT`,
                ', date=',
                `sel`.`DATE_FIN_RATTACHEMENT`,
                '] n\'existe pas. Periode traitee : [PeriodeRecue(idPeriode=',
                `sel`.`id_periode`,
                ', typePeriode=',
                `sel`.`TYPE_PERIODE`,
                ', dateDebutPeriode=',
                `sel`.`DATE_DEBUT_PERIODE`,
                ', dateFinPeriode=',
                `sel`.`DATE_FIN_PERIODE`,
                ', dateCreation=',
                `sel`.`DATE_CREATION`,
                ')]') AS `MESSAGE`
    FROM
        (`V_Selection_base_assujettie` `sel`
        LEFT JOIN `TARIFS` `t` ON (((`sel`.`numero_contrat` = `t`.`NOCO`)
            AND (`sel`.`NOCAT` = `t`.`NOCAT`)
            AND (`t`.`DT_DEBUT_SIT` <= `sel`.`DATE_DEB_RATTACHEMENT`)
            AND ((COALESCE(`t`.`DT_FIN_SIT`, 99999999) >= `sel`.`DATE_DEB_RATTACHEMENT`)
            OR ((CASE
            WHEN (`t`.`DT_FIN_SIT` = 0) THEN 99999999
            ELSE `t`.`DT_FIN_SIT`
        END) >= `sel`.`DATE_DEB_RATTACHEMENT`)))))
    WHERE
        (ISNULL(`t`.`NOCO`)
            OR ISNULL(`t`.`NOCAT`)) 
    UNION SELECT DISTINCT
        `sel`.`id_periode` AS `ID_PERIODE`,
        `sel`.`numero_contrat` AS `NUMERO_CONTRAT`,
        `sel`.`DATE_DEB_RATTACHEMENT` AS `DATE_DEB_RATTACHEMENT`,
        `sel`.`DATE_FIN_RATTACHEMENT` AS `DATE_FIN_RATTACHEMENT`,
        `sel`.`NOCAT` AS `NOCAT`,
        (CONCAT(CONVERT( CONCAT('La nature base [CONBCOT=',
                        (CASE
                            WHEN ISNULL(`t`.`CONBCOT`) THEN 'NULL'
                            ELSE `t`.`CONBCOT`
                        END),
                        ', TXCALCU_REMPLI=',
                        (CASE
                            WHEN (COALESCE(`t`.`TXCALCU`, 0) > 0) THEN 'O'
                            ELSE 'N'
                        END),
                        '] n\'existe pas. Situation tarifaire : numContrat=') USING LATIN1),
                `sel`.`numero_contrat`,
                ', numCategorie=',
                `sel`.`NOCAT`,
                ', date=',
                `sel`.`DATE_FIN_RATTACHEMENT`,
                '. Periode traitee : [PeriodeRecue(idPeriode=',
                `sel`.`id_periode`,
                ', typePeriode=',
                `sel`.`TYPE_PERIODE`,
                ', dateDebutPeriode=',
                `sel`.`DATE_DEBUT_PERIODE`,
                ', dateFinPeriode=',
                `sel`.`DATE_FIN_PERIODE`,
                ', dateCreation=',
                `sel`.`DATE_CREATION`,
                ')]') COLLATE latin1_general_ci) AS `trace`
    FROM
        ((`V_Selection_base_assujettie` `sel`
        JOIN `TARIFS` `t` ON (((`sel`.`numero_contrat` = `t`.`NOCO`)
            AND (`sel`.`NOCAT` = `t`.`NOCAT`)
            AND (`t`.`DT_DEBUT_SIT` <= `sel`.`DATE_DEB_RATTACHEMENT`)
            AND ((COALESCE(`t`.`DT_FIN_SIT`, 99999999) >= `sel`.`DATE_DEB_RATTACHEMENT`)
            OR ((CASE
            WHEN (`t`.`DT_FIN_SIT` = 0) THEN 99999999
            ELSE `t`.`DT_FIN_SIT`
        END) >= `sel`.`DATE_DEB_RATTACHEMENT`)))))
        LEFT JOIN `PARAM_NATURE_BASE` `p` ON (((`t`.`CONBCOT` = `p`.`CONBCOT`)
            AND (CONVERT( (CASE
            WHEN (COALESCE(`t`.`TXCALCU`, 0) > 0) THEN 'O'
            ELSE 'N'
        END) USING LATIN1) = (`p`.`TXCALCU_REMPLI` COLLATE latin1_general_ci)))))
    WHERE
        ISNULL(`p`.`NUM_TRANCHE`);
        
-- TM 2749-2752 mise en coherence 405

DROP VIEW IF EXISTS V_Selection_periode_mocalcot_effectifs;
CREATE VIEW V_Selection_periode_mocalcot_effectifs AS
    SELECT 
        p.ID_PERIODE AS ID_PERIODE,
        p.TYPE_PERIODE AS TYPE_PERIODE,
        p.DATE_CREATION AS DATE_CREATION,
        p.NUMERO_CONTRAT AS NUMERO_CONTRAT,
        p.DATE_DEBUT_PERIODE AS DATE_DEBUT_PERIODE,
        p.DATE_FIN_PERIODE AS DATE_FIN_PERIODE,
        p.PREMIER_MOIS_DECL AS PREMIER_MOIS_DECL,
        p.ETAT_PERIODE AS ETAT_PERIODE,
        p.RECONSOLIDER AS RECONSOLIDER,
        p.DATE_ECHEANCE AS DATE_ECHEANCE
    FROM
        (PERIODES_RECUES p
        JOIN TARIFS t)
    WHERE
        ((p.RECONSOLIDER = 'O')
            AND (t.NOCO = p.NUMERO_CONTRAT)
            AND (t.TMP_BATCH IS FALSE)
            AND ((t.DT_DEBUT_SIT , t.NOCAT) = (SELECT 
                tf.DT_DEBUT_SIT, tf.NOCAT
            FROM
                TARIFS tf
            WHERE
                ((tf.NOCO = p.NUMERO_CONTRAT)
                    AND (tf.DT_DEBUT_SIT <= p.DATE_DEBUT_PERIODE)
                    AND ((COALESCE(tf.DT_FIN_SIT, 99999999) >= p.DATE_DEBUT_PERIODE)
                    OR ((CASE
                    WHEN (tf.DT_FIN_SIT = 0) THEN 99999999
                    ELSE tf.DT_FIN_SIT
                END) >= p.DATE_DEBUT_PERIODE)))
            LIMIT 1))
            AND (t.MOCALCOT = 1));

DROP VIEW IF EXISTS V_Selection_periode_mocalcot_salaires;
CREATE VIEW V_Selection_periode_mocalcot_salaires AS
    SELECT 
        p.ID_PERIODE AS ID_PERIODE,
        p.TYPE_PERIODE AS TYPE_PERIODE,
        p.DATE_CREATION AS DATE_CREATION,
        p.NUMERO_CONTRAT AS NUMERO_CONTRAT,
        p.DATE_DEBUT_PERIODE AS DATE_DEBUT_PERIODE,
        p.DATE_FIN_PERIODE AS DATE_FIN_PERIODE,
        p.PREMIER_MOIS_DECL AS PREMIER_MOIS_DECL,
        p.ETAT_PERIODE AS ETAT_PERIODE,
        p.RECONSOLIDER AS RECONSOLIDER,
        p.DATE_ECHEANCE AS DATE_ECHEANCE,
        s.TYPE_CONSO_SALAIRE AS TYPE_CONSO
    FROM
        ((PERIODES_RECUES p
        JOIN TARIFS t)
        JOIN V_Selection_type_consolidation s)
    WHERE
        ((p.RECONSOLIDER = 'O')
            AND (t.NOCO = p.NUMERO_CONTRAT)
            AND (t.TMP_BATCH IS FALSE)
            AND ((t.DT_DEBUT_SIT , t.NOCAT) = (SELECT 
                tf.DT_DEBUT_SIT, tf.NOCAT
            FROM
                TARIFS tf
            WHERE
                ((tf.NOCO = p.NUMERO_CONTRAT)
                    AND (tf.DT_DEBUT_SIT <= p.DATE_DEBUT_PERIODE)
                    AND ((COALESCE(tf.DT_FIN_SIT, 99999999) >= p.DATE_DEBUT_PERIODE)
                    OR ((CASE
                    WHEN (tf.DT_FIN_SIT = 0) THEN 99999999
                    ELSE tf.DT_FIN_SIT
                END) >= p.DATE_DEBUT_PERIODE)))
            LIMIT 1))
            AND (t.MOCALCOT = 2)
            AND (s.ID_PERIODE = p.ID_PERIODE));
            
-- Creation view - F04_RG_P5_03 pour contrats sur salaires
DROP VIEW IF EXISTS V_Trace_Nocat_Inconnu_salaires;
CREATE VIEW V_Trace_Nocat_Inconnu_salaires AS 
SELECT DISTINCT
    vpms.ID_PERIODE,CONCAT('La situation tarifaire [numContrat=',
                vpms.NUMERO_CONTRAT,
                ', numCategorieQuittancement=',
                a.CODE_POPULATION,
                ', dateDebutPeriode=',
                vpms.DATE_DEBUT_PERIODE,
                '] n\'existe pas. Periode traitee : [PeriodeRecue(idPeriode=',
                vpms.ID_PERIODE,
                ', typePeriode=',
                vpms.TYPE_PERIODE,
                ', dateDebutPeriode=',
                vpms.DATE_DEBUT_PERIODE,
                ', dateFinPeriode=',
                vpms.DATE_FIN_PERIODE,
                ', dateCreation=',
                vpms.DATE_CREATION,
                ')]') AS MESSAGE
FROM
	V_Selection_periode_mocalcot_salaires vpms,
    AFFILIATION a
        LEFT JOIN
    CONTRAT_TRAVAIL ct ON a.ID_CONTRAT_TRAVAIL = ct.ID_CONTRAT_TRAVAIL
        LEFT JOIN
    INDIVIDU i ON ct.ID_INDIVIDU = i.ID_INDIVIDU
        LEFT JOIN
    ADHESION_ETABLISSEMENT_MOIS adh ON i.ID_ADH_ETAB_MOIS = adh.ID_ADH_ETAB_MOIS
WHERE
    adh.TMP_BATCH IS FALSE
        AND adh.ID_ADH_ETAB_MOIS IN (SELECT 
            r.ID_ADH_ETAB_MOIS
        FROM
            RATTACHEMENT_DECLARATIONS_RECUES r
        WHERE
            r.ID_PERIODE = vpms.ID_PERIODE)
        AND a.CODE_POPULATION NOT IN (SELECT DISTINCT
            t.NOCAT
        FROM
            TARIFS t
        WHERE
            (t.NOCO = vpms.NUMERO_CONTRAT)
                AND (t.DT_DEBUT_SIT <= vpms.DATE_DEBUT_PERIODE)
                AND ((COALESCE(t.DT_FIN_SIT, 99999999) >= vpms.DATE_DEBUT_PERIODE)
                OR ((CASE
                WHEN (t.DT_FIN_SIT = 0) THEN 99999999
                ELSE t.DT_FIN_SIT
            END) >= vpms.DATE_DEBUT_PERIODE)));
