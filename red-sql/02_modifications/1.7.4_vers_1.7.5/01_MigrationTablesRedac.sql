-- Version du schéma REDAC actuel
INSERT INTO VERSION_BASE (VERSION, DESCRIPTION) VALUES ('1.7.5', 'Version supportant les flux 10, 1, 2, 3, 4, 5/6, 7/8 et 9');

-- RED 229
ALTER TABLE param_famille_contrat ADD COLUMN AVEC_POPULATION VARCHAR(3) AFTER TYPE_CONSO_SALAIRE;
