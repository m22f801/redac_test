-- RED-63
DROP VIEW IF EXISTS V_Selection_categorie_quittancement_effectif_data;
CREATE VIEW V_Selection_categorie_quittancement_effectif_data AS
   SELECT 
        d.ID_PERIODE AS ID_PERIODE,
        d.NOCAT AS NOCAT,
        e.DEBUT_BASE AS DEBUT_BASE,
        e.FIN_BASE AS FIN_BASE,
        (CASE WHEN e.PRORATA_COTISATION IS NULL THEN 0 ELSE e.PRORATA_COTISATION END)  AS PRORATA_COTISATION,
        (CASE WHEN e.ESTIMATION_COTISATION IS NULL THEN 0 ELSE e.ESTIMATION_COTISATION END)  AS ESTIM_COTISATION,
        e.TOTAL_EFFECTIF AS TOTAL_EFFECTIF,
        (CASE
            WHEN (e.DEBUT_BASE = s.DATE_DEBUT_PERIODE) THEN e.TOTAL_EFFECTIF
            ELSE 0
        END) AS effectif_debut,
        (CASE
            WHEN (e.FIN_BASE = s.DATE_FIN_PERIODE) THEN e.TOTAL_EFFECTIF
            ELSE 0
        END) AS effectif_fin,
        s.DATE_DEBUT_PERIODE AS DATE_DEBUT_PERIODE,
        s.DATE_FIN_PERIODE AS DATE_FIN_PERIODE, 
        t.CONBCOT AS CONBCOT,
        t.LICAT AS LICAT
    FROM
        (((EFFECTIF_CATEGORIE_CONTRAT_TRAVAIL e
        RIGHT JOIN V_Distinct_noco_nocat_pour_contrat_et_plage d ON (((d.ID_PERIODE = e.ID_PERIODE)
            AND (d.NOCAT = e.NOCAT))))
        JOIN V_Selection_periode_mocalcot_effectifs s ON ((s.ID_PERIODE = d.ID_PERIODE)))
        JOIN TARIFS t ON (((t.NOCAT = d.NOCAT)
            AND (t.NOCO = d.NOCO)
            AND (t.DT_DEBUT_SIT <= s.DATE_DEBUT_PERIODE)
            AND ((COALESCE(t.DT_FIN_SIT, 99999999) >= s.DATE_DEBUT_PERIODE)
            OR ((CASE
            WHEN (t.DT_FIN_SIT = 0) THEN 99999999
            ELSE t.DT_FIN_SIT
        END) >= s.DATE_DEBUT_PERIODE)))));
        
-- RED-63
DROP VIEW IF EXISTS V_Selection_categorie_quittancement_effectif;
CREATE VIEW V_Selection_categorie_quittancement_effectif AS
    SELECT 
        ecct.ID_PERIODE AS ID_PERIODE,
        ecct.NOCAT AS NOCAT,
        'TOUS' AS INDIVIDU,
        ecct.LICAT AS LICAT,
        ecct.CONBCOT AS CONBCOT,
        SUM(ecct.PRORATA_COTISATION) AS COTISATION,
        SUM(ecct.effectif_debut) AS EFFECTIF_DEBUT,
        SUM(ecct.effectif_fin) AS EFFECTIF_FIN,
        SUM(ecct.ESTIM_COTISATION) AS ESTIMATION_COTISATION
    FROM
        V_Selection_categorie_quittancement_effectif_data ecct
    GROUP BY ecct.ID_PERIODE , ecct.NOCAT;
           
-- RED-63
 
DROP VIEW IF EXISTS V_Categorie_quittancement_salaires_a_inserer;
CREATE VIEW V_Categorie_quittancement_salaires_a_inserer AS
 SELECT 
        p.ID_PERIODE AS ID_PERIODE,
        t.NOCAT AS NOCAT,
        (CASE
            WHEN (i.IDENTIFIANT_REPERTOIRE = '') THEN i.NTT
            ELSE i.IDENTIFIANT_REPERTOIRE
        END) AS INDIVIDU,
        t.LICAT AS LICAT,
        t.CONBCOT AS CONBCOT,
        COALESCE((SELECT 
                        SUM(ba.MONTANT_COTISATION)
                    FROM
                        INDIVIDU ind
                            INNER JOIN
                        CONTRAT_TRAVAIL ct ON ind.ID_INDIVIDU = ct.ID_INDIVIDU
                            INNER JOIN
                        AFFILIATION a ON ct.ID_CONTRAT_TRAVAIL = a.ID_CONTRAT_TRAVAIL
                            INNER JOIN
                        BASE_ASSUJETTIE ba ON a.ID_AFFILIATION = ba.ID_AFFILIATION
                    WHERE
                        (CASE
                            WHEN i.IDENTIFIANT_REPERTOIRE <> '' THEN ind.IDENTIFIANT_REPERTOIRE = i.IDENTIFIANT_REPERTOIRE
                            ELSE ind.NTT = i.NTT
                        END)
                            AND ba.ID_BASE_ASSUJETTIE IN (SELECT 
                                tcba.ID_BASE_ASSUJETTIE
                            FROM
                                TRANCHE_CATEGORIE_BASE_ASSUJETTIE tcba
                            WHERE
                                tcba.NOCAT = t.NOCAT
                                    AND tcba.ID_PERIODE = p.ID_PERIODE)),
                0) AS MONTANT_COTISATION,
                (SELECT 
                        SUM(tcba.ESTIMATION_COTISATION)
                    FROM
                        INDIVIDU ind
                            INNER JOIN
                        CONTRAT_TRAVAIL ct ON ind.ID_INDIVIDU = ct.ID_INDIVIDU
                            INNER JOIN
                        AFFILIATION a ON ct.ID_CONTRAT_TRAVAIL = a.ID_CONTRAT_TRAVAIL
                            INNER JOIN
                        BASE_ASSUJETTIE ba ON a.ID_AFFILIATION = ba.ID_AFFILIATION
							INNER JOIN tranche_categorie_base_assujettie tcba ON ba.ID_BASE_ASSUJETTIE = tcba.ID_BASE_ASSUJETTIE
                    WHERE
                        (CASE
                            WHEN i.IDENTIFIANT_REPERTOIRE <> '' THEN ind.IDENTIFIANT_REPERTOIRE = i.IDENTIFIANT_REPERTOIRE
                            ELSE ind.NTT = i.NTT
                        END)
                            AND ba.ID_BASE_ASSUJETTIE IN (SELECT 
                                tcba.ID_BASE_ASSUJETTIE
                            FROM
                                TRANCHE_CATEGORIE_BASE_ASSUJETTIE tcba
                            WHERE
                                tcba.NOCAT = t.NOCAT
                                    AND tcba.ID_PERIODE = p.ID_PERIODE)) AS ESTIMATION_COTISATION,
        NULL AS CHAMP_NULL_1,
        NULL AS CHAMP_NULL_2
    FROM
        TARIFS t
            INNER JOIN
        PERIODES_RECUES p ON t.NOCO = p.NUMERO_CONTRAT
            INNER JOIN
        RATTACHEMENT_DECLARATIONS_RECUES r ON r.ID_PERIODE = p.ID_PERIODE
            INNER JOIN
        INDIVIDU i ON i.ID_ADH_ETAB_MOIS = r.ID_ADH_ETAB_MOIS
            AND i.TMP_BATCH IS FALSE
            INNER JOIN
        CONTRATS c ON c.NOCO = t.NOCO
            INNER JOIN
        PARAM_FAMILLE_CONTRAT par ON par.NOFAM = c.NOFAM
    WHERE
        p.RECONSOLIDER = 'O' AND t.MOCALCOT = 2
            AND (t.TMP_BATCH IS FALSE)
            AND par.TYPE_CONSO_SALAIRE = 'INDIV'
            AND (par.TMP_BATCH IS FALSE)
            AND (c.TMP_BATCH IS FALSE)
            AND (c.DT_DEBUT_SIT <= p.DATE_DEBUT_PERIODE)
            AND ((COALESCE(c.DT_FIN_SIT, 99999999) >= p.DATE_DEBUT_PERIODE)
            OR ((CASE
            WHEN (c.DT_FIN_SIT = 0) THEN 99999999
            ELSE c.DT_FIN_SIT
        END) >= p.DATE_DEBUT_PERIODE))
            AND ((t.NOCO = p.NUMERO_CONTRAT)
            AND (t.DT_DEBUT_SIT <= p.DATE_DEBUT_PERIODE)
            AND ((COALESCE(t.DT_FIN_SIT, 99999999) >= p.DATE_DEBUT_PERIODE)
            OR ((CASE
            WHEN (t.DT_FIN_SIT = 0) THEN 99999999
            ELSE t.DT_FIN_SIT
        END) >= p.DATE_DEBUT_PERIODE))
            AND r.ID_ADH_ETAB_MOIS IN (SELECT 
                adh.ID_ADH_ETAB_MOIS
            FROM
                ADHESION_ETABLISSEMENT_MOIS adh
            WHERE
                adh.ID_ADH_ETAB_MOIS = r.ID_ADH_ETAB_MOIS
                    AND adh.TMP_BATCH IS FALSE)) 
    
    UNION SELECT 
        p.ID_PERIODE AS ID_PERIODE,
        t.NOCAT AS NOCAT,
        'TOUS' AS INDIVIDU,
        t.LICAT AS LICAT,
        t.CONBCOT AS CONBCOT,
        COALESCE((SELECT 
                        SUM(ba.MONTANT_COTISATION)
                    FROM
                        BASE_ASSUJETTIE ba
                    WHERE
                        ba.ID_BASE_ASSUJETTIE IN (SELECT 
                                tcba.ID_BASE_ASSUJETTIE
                            FROM
                                TRANCHE_CATEGORIE_BASE_ASSUJETTIE tcba
                            WHERE
                                ((tcba.ID_PERIODE = p.ID_PERIODE)
                                    AND (tcba.NOCAT = t.NOCAT)))),
                0) AS MONTANT_COTISATION,
                (SELECT 
                        SUM(tcba.ESTIMATION_COTISATION)
                    FROM 
						TRANCHE_CATEGORIE_BASE_ASSUJETTIE tcba
                            WHERE
                                ((tcba.ID_PERIODE = p.ID_PERIODE)
                                    AND (tcba.NOCAT = t.NOCAT))) AS ESTIMATION_COTISATION,
        NULL AS CHAMP_NULL_1,
        NULL AS CHAMP_NULL_2
    FROM
        (TARIFS t
        JOIN PERIODES_RECUES p ON ((t.NOCO = p.NUMERO_CONTRAT)))
            INNER JOIN
        CONTRATS c ON c.NOCO = t.NOCO
            INNER JOIN
        PARAM_FAMILLE_CONTRAT par ON (par.NOFAM = c.NOFAM)
    WHERE
        (t.NOCO = p.NUMERO_CONTRAT
            AND t.DT_DEBUT_SIT <= p.DATE_DEBUT_PERIODE
            AND (COALESCE(t.DT_FIN_SIT, 99999999) >= p.DATE_DEBUT_PERIODE
            OR ((CASE
            WHEN (t.DT_FIN_SIT = 0) THEN 99999999
            ELSE t.DT_FIN_SIT
        END) >= p.DATE_DEBUT_PERIODE))
            AND p.RECONSOLIDER = 'O'
            AND t.MOCALCOT = 2
            AND (t.TMP_BATCH IS FALSE)
            AND par.TYPE_CONSO_SALAIRE = 'TOUS'
            AND (par.TMP_BATCH IS FALSE)
            AND (c.TMP_BATCH IS FALSE)
            AND (c.DT_DEBUT_SIT <= p.DATE_DEBUT_PERIODE)
            AND ((COALESCE(c.DT_FIN_SIT, 99999999) >= p.DATE_DEBUT_PERIODE)
            OR ((CASE
            WHEN (c.DT_FIN_SIT = 0) THEN 99999999
            ELSE c.DT_FIN_SIT
        END) >= p.DATE_DEBUT_PERIODE)));
                
    
    
    
    
-- RED-63
DROP VIEW IF EXISTS V_Trace_tranche_categorie_base_assujettie;
CREATE VIEW V_Trace_tranche_categorie_base_assujettie AS SELECT DISTINCT
        sel.id_periode AS ID_PERIODE,
        sel.numero_contrat AS NUMERO_CONTRAT,
        sel.DATE_DEB_RATTACHEMENT AS DATE_DEB_RATTACHEMENT,
        sel.DATE_FIN_RATTACHEMENT AS DATE_FIN_RATTACHEMENT,
        sel.NOCAT AS NOCAT,
        CONCAT('La situation tarifaire [numContrat=',
                sel.numero_contrat,
                ', numCategorie=',
                sel.NOCAT,
                ', date=',
                sel.DATE_FIN_RATTACHEMENT,
                '] n\'existe pas. Periode traitee : [PeriodeRecue(idPeriode=',
                sel.id_periode,
                ', typePeriode=',
                sel.TYPE_PERIODE,
                ', dateDebutPeriode=',
                sel.DATE_DEBUT_PERIODE,
                ', dateFinPeriode=',
                sel.DATE_FIN_PERIODE,
                ', dateCreation=',
                sel.DATE_CREATION,
                ')]') AS MESSAGE
    FROM
        (V_Selection_base_assujettie sel
        LEFT JOIN TARIFS t ON (((sel.numero_contrat = t.NOCO)
            AND (sel.NOCAT = t.NOCAT)
            AND (t.DT_DEBUT_SIT <= sel.DATE_FIN_RATTACHEMENT)
            AND ((COALESCE(t.DT_FIN_SIT, 99999999) >= sel.DATE_FIN_RATTACHEMENT)
            OR ((CASE
            WHEN (t.DT_FIN_SIT = 0) THEN 99999999
            ELSE t.DT_FIN_SIT
        END) >= sel.DATE_FIN_RATTACHEMENT)))))
    WHERE
        (ISNULL(t.NOCO)
            OR ISNULL(t.NOCAT)) 
    UNION SELECT DISTINCT
        sel.id_periode AS ID_PERIODE,
        sel.numero_contrat AS NUMERO_CONTRAT,
        sel.DATE_DEB_RATTACHEMENT AS DATE_DEB_RATTACHEMENT,
        sel.DATE_FIN_RATTACHEMENT AS DATE_FIN_RATTACHEMENT,
        sel.NOCAT AS NOCAT,
        (CONCAT(CONVERT( CONCAT('La nature base [CONBCOT=',
                        (CASE
                            WHEN ISNULL(t.CONBCOT) THEN 'NULL'
                            ELSE t.CONBCOT
                        END),
                        ', TXCALCU_REMPLI=',
                        (CASE
                            WHEN (COALESCE(t.TXCALCU, 0) > 0) THEN 'O'
                            ELSE 'N'
                        END),
                        '] n\'existe pas. Situation tarifaire : numContrat=') USING LATIN1),
                sel.numero_contrat,
                ', numCategorie=',
                sel.NOCAT,
                ', date=',
                sel.DATE_FIN_RATTACHEMENT,
                '. Periode traitee : [PeriodeRecue(idPeriode=',
                sel.id_periode,
                ', typePeriode=',
                sel.TYPE_PERIODE,
                ', dateDebutPeriode=',
                sel.DATE_DEBUT_PERIODE,
                ', dateFinPeriode=',
                sel.DATE_FIN_PERIODE,
                ', dateCreation=',
                sel.DATE_CREATION,
                ')]') COLLATE latin1_general_ci) AS trace
    FROM
        ((V_Selection_base_assujettie sel
        JOIN TARIFS t ON (((sel.numero_contrat = t.NOCO)
            AND (sel.NOCAT = t.NOCAT)
            AND (t.DT_DEBUT_SIT <= sel.DATE_FIN_RATTACHEMENT)
            AND ((COALESCE(t.DT_FIN_SIT, 99999999) >= sel.DATE_FIN_RATTACHEMENT)
            OR ((CASE
            WHEN (t.DT_FIN_SIT = 0) THEN 99999999
            ELSE t.DT_FIN_SIT
        END) >= sel.DATE_FIN_RATTACHEMENT)))))
        LEFT JOIN PARAM_NATURE_BASE p ON (((t.CONBCOT = p.CONBCOT)
            AND (CONVERT( (CASE
            WHEN (COALESCE(t.TXCALCU, 0) > 0) THEN 'O'
            ELSE 'N'
        END) USING LATIN1) = (p.TXCALCU_REMPLI COLLATE latin1_general_ci)))))
    WHERE
        ISNULL(p.NUM_TRANCHE)
        UNION SELECT DISTINCT
        sel.id_periode AS ID_PERIODE,
        sel.numero_contrat AS NUMERO_CONTRAT,
        sel.DATE_DEB_RATTACHEMENT AS DATE_DEB_RATTACHEMENT,
        sel.DATE_FIN_RATTACHEMENT AS DATE_FIN_RATTACHEMENT,
        sel.NOCAT AS NOCAT,
        (CONCAT(CONVERT( CONCAT('La situation contrat [numContrat=') USING LATIN1),
                sel.numero_contrat,
                ', date=(',
                sel.DATE_DEBUT_PERIODE,
                ')] n\'existe pas. Periode traitee : [(idPeriode=',
                sel.id_periode,
                ', typePeriode=',
                sel.TYPE_PERIODE,
                ', dateDebutPeriode=',
                sel.DATE_DEBUT_PERIODE,
                ', dateCreation=',
                sel.DATE_CREATION,
                ')]') COLLATE latin1_general_ci) AS trace
    FROM
        V_Selection_base_assujettie sel
        JOIN contrats ON (contrats.NOCO = sel.NUMERO_CONTRAT
        AND contrats.DT_DEBUT_SIT <= sel.DATE_DEBUT_PERIODE
        AND COALESCE(contrats.DT_FIN_SIT, 99999999) >= sel.DATE_DEBUT_PERIODE)
    WHERE
        ISNULL(contrats.NOCO);
                
          