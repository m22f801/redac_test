-- Version du schéma REDAC actuel
INSERT INTO VERSION_BASE (VERSION, DESCRIPTION) VALUES ('1.7.1', 'Version supportant les flux 10, 1, 2, 3, 4, 5/6, 7/8 et 9');

-- RED-114
ALTER TABLE AFFILIATION ADD COLUMN DATE_DEBUT_AFFILIATION NUMERIC(8) AFTER NOMBRE_AYDR_ENFANTS;
ALTER TABLE AFFILIATION ADD COLUMN DATE_FIN_AFFILIATION NUMERIC(8) AFTER DT_MISE_A_JOUR;

-- RED-63 
ALTER TABLE PARAM_VALEUR_DEFAUT ADD COLUMN SEUIL_DIFF_COT_MT NUMERIC(8) AFTER CENTRE_GESTION_GEST;
ALTER TABLE PARAM_VALEUR_DEFAUT ADD COLUMN SEUIL_DIFF_COT_PC NUMERIC(3) AFTER SEUIL_DIFF_COT_MT;
ALTER TABLE CATEGORIE_QUITTANCEMENT_INDIVIDU ADD COLUMN ESTIMATION_COTISATION NUMERIC(15,2) AFTER EFFECTIF_FIN;
ALTER TABLE TRANCHE_CATEGORIE_BASE_ASSUJETTIE CHANGE MT_COTISATION ESTIMATION_COTISATION NUMERIC(15,2);
