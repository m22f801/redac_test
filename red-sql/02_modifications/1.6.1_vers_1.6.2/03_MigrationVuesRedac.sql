-- TM2750
DROP VIEW IF EXISTS V_Selection_categorie_quittancement_effectif_data;
CREATE VIEW `V_Selection_categorie_quittancement_effectif_data` AS
   SELECT 
        `d`.`ID_PERIODE` AS `ID_PERIODE`,
        `d`.`NOCAT` AS `NOCAT`,
        `e`.`DEBUT_BASE` AS `DEBUT_BASE`,
        `e`.`FIN_BASE` AS `FIN_BASE`,
        (CASE WHEN `e`.`PRORATA_COTISATION` IS NULL THEN 0 ELSE `e`.`PRORATA_COTISATION` END)  AS `PRORATA_COTISATION`,
        `e`.`TOTAL_EFFECTIF` AS `TOTAL_EFFECTIF`,
        (CASE
            WHEN (`e`.`DEBUT_BASE` = `s`.`DATE_DEBUT_PERIODE`) THEN `e`.`TOTAL_EFFECTIF`
            ELSE 0
        END) AS `effectif_debut`,
        (CASE
            WHEN (`e`.`FIN_BASE` = `s`.`DATE_FIN_PERIODE`) THEN `e`.`TOTAL_EFFECTIF`
            ELSE 0
        END) AS `effectif_fin`,
        `s`.`DATE_DEBUT_PERIODE` AS `DATE_DEBUT_PERIODE`,
        `s`.`DATE_FIN_PERIODE` AS `DATE_FIN_PERIODE`, 
        `t`.`CONBCOT` AS `CONBCOT`,
        `t`.`LICAT` AS `LICAT`
    FROM
        (((`EFFECTIF_CATEGORIE_CONTRAT_TRAVAIL` `e`
        RIGHT JOIN `V_Distinct_noco_nocat_pour_contrat_et_plage` `d` ON (((`d`.`ID_PERIODE` = `e`.`ID_PERIODE`)
            AND (`d`.`NOCAT` = `e`.`NOCAT`))))
        JOIN `V_Selection_periode_mocalcot_effectifs` `s` ON ((`s`.`ID_PERIODE` = `d`.`ID_PERIODE`)))
        JOIN `TARIFS` `t` ON (((`t`.`NOCAT` = `d`.`NOCAT`)
            AND (`t`.`NOCO` = `d`.`NOCO`)
            AND (`t`.`DT_DEBUT_SIT` <= `s`.`DATE_DEBUT_PERIODE`)
            AND ((COALESCE(`t`.`DT_FIN_SIT`, 99999999) >= `s`.`DATE_DEBUT_PERIODE`)
            OR ((CASE
            WHEN (`t`.`DT_FIN_SIT` = 0) THEN 99999999
            ELSE `t`.`DT_FIN_SIT`
        END) >= `s`.`DATE_DEBUT_PERIODE`)))));