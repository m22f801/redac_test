-- RED-249 
DROP PROCEDURE IF EXISTS Inserer_CATEGORIE_QUITTANCEMENT_INDIVIDU_SALAIRE_Pour_Periodes;

DELIMITER $$
CREATE PROCEDURE Inserer_CATEGORIE_QUITTANCEMENT_INDIVIDU_SALAIRE_Pour_Periodes(IN p_audit_nom_batch VARCHAR(20), IN p_liste_ids_periodes TEXT)
BEGIN
INSERT INTO CATEGORIE_QUITTANCEMENT_INDIVIDU (
ID_PERIODE
,NOCAT
,INDIVIDU
,LICAT
,CONBCOT
,MT_COTISATION
,ESTIMATION_COTISATION
,EFFECTIF_DEBUT
,EFFECTIF_FIN
,USER_CREATION
)
SELECT 
        p.ID_PERIODE AS ID_PERIODE,
        t.NOCAT AS NOCAT,
        (CASE
            WHEN (i.IDENTIFIANT_REPERTOIRE = '') THEN i.NTT
            ELSE i.IDENTIFIANT_REPERTOIRE
        END) AS INDIVIDU,
        t.LICAT AS LICAT,
        t.CONBCOT AS CONBCOT,
        COALESCE((SELECT 
                        SUM(ba.MONTANT_COTISATION)
                    FROM
                        INDIVIDU ind
                            INNER JOIN
                        CONTRAT_TRAVAIL ct ON ind.ID_INDIVIDU = ct.ID_INDIVIDU
                            INNER JOIN
                        AFFILIATION a ON ct.ID_CONTRAT_TRAVAIL = a.ID_CONTRAT_TRAVAIL
                            INNER JOIN
                        BASE_ASSUJETTIE ba ON a.ID_AFFILIATION = ba.ID_AFFILIATION
                    WHERE
                        (CASE
                            WHEN i.IDENTIFIANT_REPERTOIRE <> '' THEN ind.IDENTIFIANT_REPERTOIRE = i.IDENTIFIANT_REPERTOIRE
                            ELSE ind.NTT = i.NTT
                        END)
                        
                        AND ba.ID_BASE_ASSUJETTIE IN (SELECT 
                                tcba.ID_BASE_ASSUJETTIE
                            FROM
                                TRANCHE_CATEGORIE_BASE_ASSUJETTIE tcba
                            WHERE
                                tcba.NOCAT = t.NOCAT
                                    AND tcba.ID_PERIODE = p.ID_PERIODE)), 0) AS MONTANT_COTISATION,
        (SELECT 
                SUM(tcba.ESTIMATION_COTISATION)
            FROM
                TRANCHE_CATEGORIE_BASE_ASSUJETTIE tcba
            WHERE
                tcba.NOCAT = t.NOCAT
                    AND tcba.ID_PERIODE = p.ID_PERIODE
                    AND tcba.ID_BASE_ASSUJETTIE = ba.ID_BASE_ASSUJETTIE) AS ESTIMATION_COTISATION,
        NULL AS CHAMP_NULL_1,
        NULL AS CHAMP_NULL_2,
        p_audit_nom_batch
    FROM
        TARIFS t
            INNER JOIN
        PERIODES_RECUES p ON t.NOCO = p.NUMERO_CONTRAT
            INNER JOIN
        RATTACHEMENT_DECLARATIONS_RECUES r ON r.ID_PERIODE = p.ID_PERIODE
            INNER JOIN
        INDIVIDU i ON i.ID_ADH_ETAB_MOIS = r.ID_ADH_ETAB_MOIS
            AND i.TMP_BATCH IS FALSE
            INNER JOIN
        CONTRATS c ON c.NOCO = t.NOCO
            INNER JOIN
        PARAM_FAMILLE_CONTRAT par ON par.NOFAM = c.NOFAM
            INNER JOIN
        CONTRAT_TRAVAIL ct ON ct.ID_INDIVIDU = i.ID_INDIVIDU
            INNER JOIN
        AFFILIATION a ON ct.ID_CONTRAT_TRAVAIL = a.ID_CONTRAT_TRAVAIL
            INNER JOIN
        BASE_ASSUJETTIE ba ON a.ID_AFFILIATION = ba.ID_AFFILIATION
        	LEFT JOIN tranche_categorie_base_assujettie tcba ON (tcba.NOCAT = t.NOCAT
                    AND tcba.ID_PERIODE = p.ID_PERIODE
                    AND tcba.ID_BASE_ASSUJETTIE = ba.ID_BASE_ASSUJETTIE)
    WHERE
        p.RECONSOLIDER = 'O' AND t.NATURE_CONTRAT = 2
            AND (t.TMP_BATCH IS FALSE)
            AND par.TYPE_CONSO_SALAIRE = 'INDIV'
            AND (par.TMP_BATCH IS FALSE)
            AND (c.TMP_BATCH IS FALSE)
            AND (c.DT_DEBUT_SIT <= p.DATE_DEBUT_PERIODE)
            AND ((COALESCE(c.DT_FIN_SIT, 99999999) >= p.DATE_DEBUT_PERIODE)
            OR ((CASE
            WHEN (c.DT_FIN_SIT = 0) THEN 99999999
            ELSE c.DT_FIN_SIT
        END) >= p.DATE_DEBUT_PERIODE))
            AND ((t.NOCO = p.NUMERO_CONTRAT)
            AND (t.DT_DEBUT_SIT <= p.DATE_DEBUT_PERIODE)
            AND ((COALESCE(t.DT_FIN_SIT, 99999999) >= p.DATE_DEBUT_PERIODE)
            OR ((CASE
            WHEN (t.DT_FIN_SIT = 0) THEN 99999999
            ELSE t.DT_FIN_SIT
        END) >= p.DATE_DEBUT_PERIODE))
            AND r.ID_ADH_ETAB_MOIS IN (SELECT 
                adh.ID_ADH_ETAB_MOIS
            FROM
                ADHESION_ETABLISSEMENT_MOIS adh
            WHERE
                adh.ID_ADH_ETAB_MOIS = r.ID_ADH_ETAB_MOIS
                    AND adh.TMP_BATCH IS FALSE))
			AND FIND_IN_SET(p.ID_PERIODE, p_liste_ids_periodes) > 0
    GROUP BY p.ID_PERIODE, INDIVIDU, COALESCE(
        IF( (a.CODE_POPULATION = ''),NULL,a.CODE_POPULATION),
            (SELECT t.NOCAT
             FROM tarifs t
             WHERE((t.NOCO = p.NUMERO_CONTRAT)
             AND (t.DT_DEBUT_SIT <= p.DATE_DEBUT_PERIODE)
             AND ((COALESCE(t.DT_FIN_SIT, 99999999) >= p.DATE_DEBUT_PERIODE)
             OR ((CASE
                WHEN (t.DT_FIN_SIT = 0) THEN 99999999
                ELSE t.DT_FIN_SIT
                END) >= p.DATE_DEBUT_PERIODE)))
             ORDER BY t.DT_DEBUT_SIT
             LIMIT 1))
    UNION SELECT 
        p.ID_PERIODE AS ID_PERIODE,
        t.NOCAT AS NOCAT,
        'TOUS' AS INDIVIDU,
        t.LICAT AS LICAT,
        t.CONBCOT AS CONBCOT,
        COALESCE((SELECT 
                        SUM(ba.MONTANT_COTISATION)
                    FROM
                        BASE_ASSUJETTIE ba
                    WHERE
                        ba.ID_BASE_ASSUJETTIE IN (SELECT 
                                tcba.ID_BASE_ASSUJETTIE
                            FROM
                                TRANCHE_CATEGORIE_BASE_ASSUJETTIE tcba
                            WHERE
                                ((tcba.ID_PERIODE = p.ID_PERIODE)
                                    AND (tcba.NOCAT = t.NOCAT)))),
                0) AS MONTANT_COTISATION,
        (SELECT 
                SUM(tcba.ESTIMATION_COTISATION)
            FROM
                TRANCHE_CATEGORIE_BASE_ASSUJETTIE tcba
            WHERE
                ((tcba.ID_PERIODE = p.ID_PERIODE)
                    AND (tcba.NOCAT = t.NOCAT))) AS ESTIMATION_COTISATION,
        NULL AS CHAMP_NULL_1,
        NULL AS CHAMP_NULL_2,
        p_audit_nom_batch
    FROM
        (TARIFS t
        JOIN PERIODES_RECUES p ON ((t.NOCO = p.NUMERO_CONTRAT)))
            INNER JOIN
        CONTRATS c ON c.NOCO = t.NOCO
            INNER JOIN
        PARAM_FAMILLE_CONTRAT par ON (par.NOFAM = c.NOFAM)
    WHERE
        (t.NOCO = p.NUMERO_CONTRAT
            AND t.DT_DEBUT_SIT <= p.DATE_DEBUT_PERIODE
            AND (COALESCE(t.DT_FIN_SIT, 99999999) >= p.DATE_DEBUT_PERIODE
            OR ((CASE
            WHEN (t.DT_FIN_SIT = 0) THEN 99999999
            ELSE t.DT_FIN_SIT
        END) >= p.DATE_DEBUT_PERIODE))
            AND p.RECONSOLIDER = 'O'
            AND t.NATURE_CONTRAT = 2
            AND (t.TMP_BATCH IS FALSE)
            AND par.TYPE_CONSO_SALAIRE = 'TOUS'
            AND (par.TMP_BATCH IS FALSE)
            AND (c.TMP_BATCH IS FALSE)
            AND (c.DT_DEBUT_SIT <= p.DATE_DEBUT_PERIODE)
            AND ((COALESCE(c.DT_FIN_SIT, 99999999) >= p.DATE_DEBUT_PERIODE)
            OR ((CASE
            WHEN (c.DT_FIN_SIT = 0) THEN 99999999
            ELSE c.DT_FIN_SIT
        END) >= p.DATE_DEBUT_PERIODE)))
        AND FIND_IN_SET(p.ID_PERIODE, p_liste_ids_periodes) > 0;

END$$

DELIMITER ;

DROP PROCEDURE IF EXISTS Creation_HistoriqueCommentairePeriode_en_masse;
DELIMITER $$
CREATE PROCEDURE Creation_HistoriqueCommentairePeriode_en_masse(IN p_liste_ids_periodes TEXT,IN datehms NUMERIC(14), IN utilisateur CHAR(30), IN commentaire VARCHAR(1000), IN userCreation VARCHAR(20))
BEGIN
	
INSERT INTO HISTO_COMMENTAIRE_PERIODE(ID_PERIODE, UTILISATEUR, DATEHMS_SAISIE, COMMENTAIRE, USER_CREATION)
SELECT p.ID_PERIODE, utilisateur, datehms, commentaire, userCreation
FROM periodes_recues p WHERE FIND_IN_SET(ID_PERIODE, p_liste_ids_periodes) > 0;

END$$
DELIMITER ;

DROP PROCEDURE IF EXISTS Calcul_effectif_indiv_salaire;
DELIMITER $$
CREATE PROCEDURE Calcul_effectif_indiv_salaire(IN p_audit_nom_batch VARCHAR(20))
BEGIN

DECLARE done INT DEFAULT FALSE;
DECLARE C_NO_CAT char(30);
DECLARE C_ID_PERIODE bigint(20);
DECLARE ID_PER_MAJ bigint(20);
DECLARE EFF_DEB_MAJ decimal(10,0);
DECLARE EFF_FIN_MAJ decimal(10,0);

DECLARE liste_nocat_unique CURSOR FOR
(SELECT cqi.ID_PERIODE,cqi.NOCAT
FROM categorie_quittancement_individu cqi INNER JOIN V_selection_periode_nature_contrat_salaires vpns ON (cqi.ID_PERIODE=vpns.ID_PERIODE)
WHERE cqi.individu='TOUS'
GROUP BY cqi.ID_PERIODE
HAVING COUNT(DISTINCT cqi.NOCAT)<=1);

DECLARE liste_nocat_non_unique CURSOR FOR
(SELECT cqi.ID_PERIODE,cqi.NOCAT
FROM categorie_quittancement_individu cqi INNER JOIN V_selection_periode_nature_contrat_salaires vpns ON (cqi.ID_PERIODE=vpns.ID_PERIODE)
WHERE cqi.individu='TOUS'
 AND (SELECT COUNT(DISTINCT cqi2.NOCAT) FROM categorie_quittancement_individu cqi2 WHERE cqi2.ID_PERIODE=cqi.ID_PERIODE)>1
GROUP BY cqi.ID_PERIODE, cqi.NOCAT);

DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

OPEN liste_nocat_unique;
IF (Select FOUND_ROWS()) > 0 THEN
	nocat_uni: LOOP 
	FETCH liste_nocat_unique INTO C_ID_PERIODE, C_NO_CAT;
		IF done THEN 
			LEAVE nocat_uni;
		END IF;
		-- Calcul effectifs debut
		SELECT 
    COUNT(DISTINCT (CASE
            WHEN (i.IDENTIFIANT_REPERTOIRE = '') THEN i.NTT
            ELSE i.IDENTIFIANT_REPERTOIRE
        END)), p.ID_PERIODE INTO EFF_DEB_MAJ, ID_PER_MAJ
FROM
    periodes_recues p
        INNER JOIN
    rattachement_declarations_recues rda ON (p.ID_PERIODE = rda.ID_PERIODE)
        INNER JOIN
    adhesion_etablissement_mois adh ON (adh.ID_ADH_ETAB_MOIS = rda.ID_ADH_ETAB_MOIS)
        INNER JOIN
    individu i ON (i.ID_ADH_ETAB_MOIS = adh.ID_ADH_ETAB_MOIS)
		INNER JOIN
	contrat_travail ct ON (ct.ID_INDIVIDU = i.ID_INDIVIDU)
		INNER JOIN
	affiliation a ON (a.ID_CONTRAT_TRAVAIL = ct.ID_CONTRAT_TRAVAIL)
		INNER JOIN
	base_assujettie ba ON (ba.ID_AFFILIATION = a.ID_AFFILIATION AND ba.DATE_DEB_RATTACHEMENT = p.DATE_DEBUT_PERIODE)
WHERE
    p.ID_PERIODE = C_ID_PERIODE;
					
		SELECT 
    COUNT(DISTINCT (CASE
            WHEN (i.IDENTIFIANT_REPERTOIRE = '') THEN i.NTT
            ELSE i.IDENTIFIANT_REPERTOIRE
        END)) INTO EFF_FIN_MAJ
FROM
    periodes_recues p
        INNER JOIN
    rattachement_declarations_recues rda ON (p.ID_PERIODE = rda.ID_PERIODE)
        INNER JOIN
    adhesion_etablissement_mois adh ON (adh.ID_ADH_ETAB_MOIS = rda.ID_ADH_ETAB_MOIS)
        INNER JOIN
    individu i ON (i.ID_ADH_ETAB_MOIS = adh.ID_ADH_ETAB_MOIS)
		INNER JOIN
	contrat_travail ct ON (ct.ID_INDIVIDU = i.ID_INDIVIDU)
		INNER JOIN
	affiliation a ON (a.ID_CONTRAT_TRAVAIL = ct.ID_CONTRAT_TRAVAIL)
		INNER JOIN
	base_assujettie ba ON (ba.ID_AFFILIATION = a.ID_AFFILIATION AND ba.DATE_FIN_RATTACHEMENT = p.DATE_FIN_PERIODE)
WHERE
    p.ID_PERIODE = C_ID_PERIODE;
	
UPDATE categorie_quittancement_individu cqi3
SET 
	cqi3.EFFECTIF_DEBUT = EFF_DEB_MAJ,
	cqi3.EFFECTIF_FIN = EFF_FIN_MAJ
WHERE
	cqi3.ID_PERIODE = ID_PER_MAJ;
END LOOP;
END IF;
CLOSE liste_nocat_unique;

OPEN liste_nocat_non_unique;
IF (SELECT FOUND_ROWS()) > 0 THEN
	nocat_non_uni : LOOP 
	FETCH liste_nocat_non_unique INTO C_ID_PERIODE, C_NO_CAT;
		IF done THEN 
			LEAVE nocat_non_uni;
		END IF;
		-- Calcul effectifs fin
		SELECT 
    COUNT(DISTINCT (CASE
            WHEN (i.IDENTIFIANT_REPERTOIRE = '') THEN i.NTT
            ELSE i.IDENTIFIANT_REPERTOIRE
        END)), p.ID_PERIODE INTO EFF_DEB_MAJ, ID_PER_MAJ
FROM
    periodes_recues p
        INNER JOIN
    rattachement_declarations_recues rda ON (p.ID_PERIODE = rda.ID_PERIODE)
        INNER JOIN
    adhesion_etablissement_mois adh ON (adh.ID_ADH_ETAB_MOIS = rda.ID_ADH_ETAB_MOIS)
        INNER JOIN
    individu i ON (i.ID_ADH_ETAB_MOIS = adh.ID_ADH_ETAB_MOIS)
		INNER JOIN
	contrat_travail ct ON (ct.ID_INDIVIDU = i.ID_INDIVIDU)
		INNER JOIN
	affiliation a ON (a.ID_CONTRAT_TRAVAIL = ct.ID_CONTRAT_TRAVAIL AND a.CODE_POPULATION = C_NO_CAT)
		INNER JOIN
	base_assujettie ba ON (ba.ID_AFFILIATION = a.ID_AFFILIATION AND ba.DATE_DEB_RATTACHEMENT = p.DATE_DEBUT_PERIODE)
WHERE
    p.ID_PERIODE = C_ID_PERIODE;

SELECT 
    COUNT(DISTINCT (CASE
            WHEN (i.IDENTIFIANT_REPERTOIRE = '') THEN i.NTT
            ELSE i.IDENTIFIANT_REPERTOIRE
        END)) INTO EFF_FIN_MAJ
FROM
    periodes_recues p
        INNER JOIN
    rattachement_declarations_recues rda ON (p.ID_PERIODE = rda.ID_PERIODE)
        INNER JOIN
    adhesion_etablissement_mois adh ON (adh.ID_ADH_ETAB_MOIS = rda.ID_ADH_ETAB_MOIS)
        INNER JOIN
    individu i ON (i.ID_ADH_ETAB_MOIS = adh.ID_ADH_ETAB_MOIS)
		INNER JOIN
	contrat_travail ct ON (ct.ID_INDIVIDU = i.ID_INDIVIDU)
		INNER JOIN
	affiliation a ON (a.ID_CONTRAT_TRAVAIL = ct.ID_CONTRAT_TRAVAIL AND a.CODE_POPULATION = C_NO_CAT)
		INNER JOIN
	base_assujettie ba ON (ba.ID_AFFILIATION = a.ID_AFFILIATION AND ba.DATE_FIN_RATTACHEMENT = p.DATE_FIN_PERIODE)
WHERE
    p.ID_PERIODE = C_ID_PERIODE;
	
		UPDATE categorie_quittancement_individu cqi3 
SET 
    cqi3.EFFECTIF_DEBUT = EFF_DEB_MAJ,
    cqi3.EFFECTIF_FIN = EFF_FIN_MAJ
WHERE
    cqi3.ID_PERIODE = ID_PER_MAJ
AND cqi3.NOCAT = C_NO_CAT;
    
	END LOOP;
END IF;
CLOSE liste_nocat_non_unique;

END$$
DELIMITER ;

DROP PROCEDURE IF EXISTS Calcul_effectif_indiv_salaire_pour_periodes;
DELIMITER $$
CREATE PROCEDURE Calcul_effectif_indiv_salaire_pour_periodes(IN p_audit_nom_batch VARCHAR(20), in p_liste_ids_periodes TEXT)
BEGIN

DECLARE done INT DEFAULT FALSE;
DECLARE C_NO_CAT char(30);
DECLARE C_ID_PERIODE bigint(20);
DECLARE ID_PER_MAJ bigint(20);
DECLARE EFF_DEB_MAJ decimal(10,0);
DECLARE EFF_FIN_MAJ decimal(10,0);

DECLARE liste_nocat_unique CURSOR FOR
(SELECT cqi.ID_PERIODE,cqi.NOCAT
FROM categorie_quittancement_individu cqi INNER JOIN V_selection_periode_nature_contrat_salaires vpns ON (cqi.ID_PERIODE=vpns.ID_PERIODE)
WHERE cqi.individu='TOUS'
AND FIND_IN_SET(cqi.ID_PERIODE, p_liste_ids_periodes)
GROUP BY cqi.ID_PERIODE
HAVING COUNT(DISTINCT cqi.NOCAT)<=1);

DECLARE liste_nocat_non_unique CURSOR FOR
(SELECT cqi.ID_PERIODE,cqi.NOCAT
FROM categorie_quittancement_individu cqi INNER JOIN V_selection_periode_nature_contrat_salaires vpns ON (cqi.ID_PERIODE=vpns.ID_PERIODE)
WHERE cqi.individu='TOUS'
 AND FIND_IN_SET(cqi.ID_PERIODE, p_liste_ids_periodes)
 AND (SELECT COUNT(DISTINCT cqi2.NOCAT) FROM categorie_quittancement_individu cqi2 WHERE cqi2.ID_PERIODE=cqi.ID_PERIODE)>1
GROUP BY cqi.ID_PERIODE, cqi.NOCAT);

DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

OPEN liste_nocat_unique;
IF (Select FOUND_ROWS()) > 0 THEN
	nocat_uni: LOOP 
	FETCH liste_nocat_unique INTO C_ID_PERIODE, C_NO_CAT;
		IF done THEN 
			LEAVE nocat_uni;
		END IF;
		-- Calcul effectifs debut
		SELECT 
    COUNT(DISTINCT (CASE
            WHEN (i.IDENTIFIANT_REPERTOIRE = '') THEN i.NTT
            ELSE i.IDENTIFIANT_REPERTOIRE
        END)), p.ID_PERIODE INTO EFF_DEB_MAJ, ID_PER_MAJ
FROM
    periodes_recues p
        INNER JOIN
    rattachement_declarations_recues rda ON (p.ID_PERIODE = rda.ID_PERIODE)
        INNER JOIN
    adhesion_etablissement_mois adh ON (adh.ID_ADH_ETAB_MOIS = rda.ID_ADH_ETAB_MOIS)
        INNER JOIN
    individu i ON (i.ID_ADH_ETAB_MOIS = adh.ID_ADH_ETAB_MOIS)
		INNER JOIN
	contrat_travail ct ON (ct.ID_INDIVIDU = i.ID_INDIVIDU)
		INNER JOIN
	affiliation a ON (a.ID_CONTRAT_TRAVAIL = ct.ID_CONTRAT_TRAVAIL)
		INNER JOIN
	base_assujettie ba ON (ba.ID_AFFILIATION = a.ID_AFFILIATION AND ba.DATE_DEB_RATTACHEMENT = p.DATE_DEBUT_PERIODE)
WHERE
    p.ID_PERIODE = C_ID_PERIODE;
					
		SELECT 
    COUNT(DISTINCT (CASE
            WHEN (i.IDENTIFIANT_REPERTOIRE = '') THEN i.NTT
            ELSE i.IDENTIFIANT_REPERTOIRE
        END)) INTO EFF_FIN_MAJ
FROM
    periodes_recues p
        INNER JOIN
    rattachement_declarations_recues rda ON (p.ID_PERIODE = rda.ID_PERIODE)
        INNER JOIN
    adhesion_etablissement_mois adh ON (adh.ID_ADH_ETAB_MOIS = rda.ID_ADH_ETAB_MOIS)
        INNER JOIN
    individu i ON (i.ID_ADH_ETAB_MOIS = adh.ID_ADH_ETAB_MOIS)
		INNER JOIN
	contrat_travail ct ON (ct.ID_INDIVIDU = i.ID_INDIVIDU)
		INNER JOIN
	affiliation a ON (a.ID_CONTRAT_TRAVAIL = ct.ID_CONTRAT_TRAVAIL)
		INNER JOIN
	base_assujettie ba ON (ba.ID_AFFILIATION = a.ID_AFFILIATION AND ba.DATE_FIN_RATTACHEMENT = p.DATE_FIN_PERIODE)
WHERE
    p.ID_PERIODE = C_ID_PERIODE;
        
UPDATE categorie_quittancement_individu cqi3
SET 
	cqi3.EFFECTIF_DEBUT = EFF_DEB_MAJ,
	cqi3.EFFECTIF_FIN = EFF_FIN_MAJ
WHERE
	cqi3.ID_PERIODE = ID_PER_MAJ;
END LOOP;
END IF;
CLOSE liste_nocat_unique;

OPEN liste_nocat_non_unique;
IF (SELECT FOUND_ROWS()) > 0 THEN
	nocat_non_uni : LOOP 
	FETCH liste_nocat_non_unique INTO C_ID_PERIODE, C_NO_CAT;
		IF done THEN 
			LEAVE nocat_non_uni;
		END IF;
		-- Calcul effectifs fin
		SELECT 
    COUNT(DISTINCT (CASE
            WHEN (i.IDENTIFIANT_REPERTOIRE = '') THEN i.NTT
            ELSE i.IDENTIFIANT_REPERTOIRE
        END)), p.ID_PERIODE INTO EFF_DEB_MAJ, ID_PER_MAJ
FROM
    periodes_recues p
        INNER JOIN
    rattachement_declarations_recues rda ON (p.ID_PERIODE = rda.ID_PERIODE)
        INNER JOIN
    adhesion_etablissement_mois adh ON (adh.ID_ADH_ETAB_MOIS = rda.ID_ADH_ETAB_MOIS)
        INNER JOIN
    individu i ON (i.ID_ADH_ETAB_MOIS = adh.ID_ADH_ETAB_MOIS)
		INNER JOIN
	contrat_travail ct ON (ct.ID_INDIVIDU = i.ID_INDIVIDU)
		INNER JOIN
	affiliation a ON (a.ID_CONTRAT_TRAVAIL = ct.ID_CONTRAT_TRAVAIL AND a.CODE_POPULATION = C_NO_CAT)
		INNER JOIN
	base_assujettie ba ON (ba.ID_AFFILIATION = a.ID_AFFILIATION AND ba.DATE_DEB_RATTACHEMENT = p.DATE_DEBUT_PERIODE)
WHERE
    p.ID_PERIODE = C_ID_PERIODE;

SELECT 
    COUNT(DISTINCT (CASE
            WHEN (i.IDENTIFIANT_REPERTOIRE = '') THEN i.NTT
            ELSE i.IDENTIFIANT_REPERTOIRE
        END)) INTO EFF_FIN_MAJ
FROM
periodes_recues p
        INNER JOIN
    rattachement_declarations_recues rda ON (p.ID_PERIODE = rda.ID_PERIODE)
        INNER JOIN
    adhesion_etablissement_mois adh ON (adh.ID_ADH_ETAB_MOIS = rda.ID_ADH_ETAB_MOIS)
        INNER JOIN
    individu i ON (i.ID_ADH_ETAB_MOIS = adh.ID_ADH_ETAB_MOIS)
		INNER JOIN
	contrat_travail ct ON (ct.ID_INDIVIDU = i.ID_INDIVIDU)
		INNER JOIN
	affiliation a ON (a.ID_CONTRAT_TRAVAIL = ct.ID_CONTRAT_TRAVAIL AND a.CODE_POPULATION = C_NO_CAT)
		INNER JOIN
	base_assujettie ba ON (ba.ID_AFFILIATION = a.ID_AFFILIATION AND ba.DATE_FIN_RATTACHEMENT = p.DATE_FIN_PERIODE)
WHERE
    p.ID_PERIODE = C_ID_PERIODE;

		UPDATE categorie_quittancement_individu cqi3 
SET 
    cqi3.EFFECTIF_DEBUT = EFF_DEB_MAJ,
    cqi3.EFFECTIF_FIN = EFF_FIN_MAJ
WHERE
    cqi3.ID_PERIODE = ID_PER_MAJ
AND cqi3.NOCAT = C_NO_CAT;
    
	END LOOP;
END IF;
CLOSE liste_nocat_non_unique;

END$$
DELIMITER ;
