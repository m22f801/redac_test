-- Version du schéma REDAC actuel
INSERT INTO VERSION_BASE (VERSION, DESCRIPTION) VALUES ('1.4.3', 'Version supportant les flux 10, 1, 2, 3, 4, 5/6, 7/8 et 9');

-- TM 2669
ALTER TABLE PARAM_FAMILLE_MODE_CALCUL_CONTRAT ADD DONNEES_A_INTEGRER CHAR(1);

ALTER TABLE EXTENSIONS_CONTRATS ADD DONNEES_A_INTEGRER CHAR(1);
