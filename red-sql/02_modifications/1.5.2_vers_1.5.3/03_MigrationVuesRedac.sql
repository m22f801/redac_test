
-- Suppresions View TM 2191 - Export individus 
DROP VIEW IF EXISTS V_Messages_Controle_IndividuPeriode;
DROP VIEW IF EXISTS V_cumul_cqi;

-- Suppresions View TM 2191 - Export Synthese 
DROP VIEW IF EXISTS V_Selection_ba_montantCBA;
DROP VIEW IF EXISTS V_Selection_periode_ba_montantTCBA;

-- Suppression View TM2191 - Export périodes
DROP VIEW IF EXISTS V_count_messages_controle;