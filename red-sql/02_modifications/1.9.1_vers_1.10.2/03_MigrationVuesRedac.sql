DROP VIEW IF EXISTS V_Selection_type_consolidation;
CREATE VIEW V_Selection_type_consolidation AS select par.TYPE_CONSO_SALAIRE AS TYPE_CONSO_SALAIRE,per.ID_PERIODE AS ID_PERIODE from ((PARAM_FAMILLE_CONTRAT par join CONTRATS c on((par.NOFAM = c.NOFAM AND par.NMGRPGES = c.NMGRPGES))) join PERIODES_RECUES per on((per.NUMERO_CONTRAT = c.NOCO))) where ((par.TMP_BATCH is false) and (c.TMP_BATCH is false) and (c.DT_DEBUT_SIT <= per.DATE_FIN_PERIODE) and ((coalesce(c.DT_FIN_SIT,99999999) >= per.DATE_FIN_PERIODE) or ((case when (c.DT_FIN_SIT = 0) then 99999999 else c.DT_FIN_SIT end) >= per.DATE_FIN_PERIODE)));

DROP VIEW IF EXISTS V_Selection_periode_nature_contrat_effectifs;
CREATE VIEW V_Selection_periode_nature_contrat_effectifs AS
    SELECT 
        p.ID_PERIODE AS ID_PERIODE,
        p.TYPE_PERIODE AS TYPE_PERIODE,
        p.DATE_CREATION AS DATE_CREATION,
        p.NUMERO_CONTRAT AS NUMERO_CONTRAT,
        p.DATE_DEBUT_PERIODE AS DATE_DEBUT_PERIODE,
        p.DATE_FIN_PERIODE AS DATE_FIN_PERIODE,
        p.PREMIER_MOIS_DECL AS PREMIER_MOIS_DECL,
        p.ETAT_PERIODE AS ETAT_PERIODE,
        p.RECONSOLIDER AS RECONSOLIDER,
        p.DATE_ECHEANCE AS DATE_ECHEANCE
    FROM
        (PERIODES_RECUES p
        JOIN TARIFS t)
    WHERE
        ((p.RECONSOLIDER = 'O')
            AND (t.NOCO = p.NUMERO_CONTRAT)
            AND (t.TMP_BATCH IS FALSE)
            AND ((t.DT_DEBUT_SIT , t.NOCAT) = (SELECT 
                tf.DT_DEBUT_SIT, tf.NOCAT
            FROM
                TARIFS tf
            WHERE
                ((tf.NOCO = p.NUMERO_CONTRAT)
                    AND (tf.DT_DEBUT_SIT <= p.DATE_DEBUT_PERIODE)
                    AND ((COALESCE(tf.DT_FIN_SIT, 99999999) >= p.DATE_DEBUT_PERIODE)
                    OR ((CASE
                    WHEN (tf.DT_FIN_SIT = 0) THEN 99999999
                    ELSE tf.DT_FIN_SIT
                END) >= p.DATE_DEBUT_PERIODE)))
            LIMIT 1))
            AND (t.NATURE_CONTRAT = 1));