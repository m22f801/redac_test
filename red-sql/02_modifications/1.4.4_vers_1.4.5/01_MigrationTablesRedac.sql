-- Version du schéma REDAC actuel
INSERT INTO VERSION_BASE (VERSION, DESCRIPTION) VALUES ('1.4.5', 'Version supportant les flux 10, 1, 2, 3, 4, 5/6, 7/8 et 9');

-- TM 2693
-- note : La ligne suivante ne peut être exécutée uniquement si la table est vide
-- A appliquer uniquement dans le cas où on est sur une BDD créé avant la v1.2.0, sur laquelle les scripts de delta ont été appliqués.
ALTER TABLE TRANCHE_CATEGORIE_BASE_ASSUJETTIE ADD CONSTRAINT TRANCHE_CATEGORIE_BASE_ASSUJETTIE_FK_BASE_ASSUJETTIE FOREIGN KEY(ID_BASE_ASSUJETTIE, TMP_BATCH) REFERENCES BASE_ASSUJETTIE(ID_BASE_ASSUJETTIE, TMP_BATCH);
