-- RED-191 Passer les contrats GERD forfaitaires en contrats sur base assujettie

DROP VIEW IF EXISTS V_Selection_periode_mocalcot_effectifs;
DROP VIEW IF EXISTS V_Selection_periode_nature_contrat_effectifs;
CREATE VIEW V_Selection_periode_nature_contrat_effectifs AS
    SELECT 
        p.ID_PERIODE AS ID_PERIODE,
        p.TYPE_PERIODE AS TYPE_PERIODE,
        p.DATE_CREATION AS DATE_CREATION,
        p.NUMERO_CONTRAT AS NUMERO_CONTRAT,
        p.DATE_DEBUT_PERIODE AS DATE_DEBUT_PERIODE,
        p.DATE_FIN_PERIODE AS DATE_FIN_PERIODE,
        p.PREMIER_MOIS_DECL AS PREMIER_MOIS_DECL,
        p.ETAT_PERIODE AS ETAT_PERIODE,
        p.RECONSOLIDER AS RECONSOLIDER,
        p.DATE_ECHEANCE AS DATE_ECHEANCE
    FROM
        (PERIODES_RECUES p
        JOIN TARIFS t)
    WHERE
        ((p.RECONSOLIDER = 'O')
            AND (t.NOCO = p.NUMERO_CONTRAT)
            AND (t.TMP_BATCH IS FALSE)
            AND ((t.DT_DEBUT_SIT , t.NOCAT) = (SELECT 
                tf.DT_DEBUT_SIT, tf.NOCAT
            FROM
                TARIFS tf
            WHERE
                ((tf.NOCO = p.NUMERO_CONTRAT)
                    AND (tf.DT_DEBUT_SIT <= p.DATE_DEBUT_PERIODE)
                    AND ((COALESCE(tf.DT_FIN_SIT, 99999999) >= p.DATE_DEBUT_PERIODE)
                    OR ((CASE
                    WHEN (tf.DT_FIN_SIT = 0) THEN 99999999
                    ELSE tf.DT_FIN_SIT
                END) >= p.DATE_DEBUT_PERIODE)))
            LIMIT 1))
            AND (t.NATURE_CONTRAT = 1));



DROP VIEW IF EXISTS V_Selection_periode_mocalcot_salaires;
DROP VIEW IF EXISTS V_Selection_periode_nature_contrat_salaires;
CREATE VIEW V_Selection_periode_nature_contrat_salaires AS
    SELECT 
        p.ID_PERIODE AS ID_PERIODE,
        p.TYPE_PERIODE AS TYPE_PERIODE,
        p.DATE_CREATION AS DATE_CREATION,
        p.NUMERO_CONTRAT AS NUMERO_CONTRAT,
        p.DATE_DEBUT_PERIODE AS DATE_DEBUT_PERIODE,
        p.DATE_FIN_PERIODE AS DATE_FIN_PERIODE,
        p.PREMIER_MOIS_DECL AS PREMIER_MOIS_DECL,
        p.ETAT_PERIODE AS ETAT_PERIODE,
        p.RECONSOLIDER AS RECONSOLIDER,
        p.DATE_ECHEANCE AS DATE_ECHEANCE,
        s.TYPE_CONSO_SALAIRE AS TYPE_CONSO
    FROM
        ((PERIODES_RECUES p
        JOIN TARIFS t)
        JOIN V_Selection_type_consolidation s)
    WHERE
        ((p.RECONSOLIDER = 'O')
            AND (t.NOCO = p.NUMERO_CONTRAT)
            AND (t.TMP_BATCH IS FALSE)
            AND ((t.DT_DEBUT_SIT , t.NOCAT) = (SELECT 
                tf.DT_DEBUT_SIT, tf.NOCAT
            FROM
                TARIFS tf
            WHERE
                ((tf.NOCO = p.NUMERO_CONTRAT)
                    AND (tf.DT_DEBUT_SIT <= p.DATE_DEBUT_PERIODE)
                    AND ((COALESCE(tf.DT_FIN_SIT, 99999999) >= p.DATE_DEBUT_PERIODE)
                    OR ((CASE
                    WHEN (tf.DT_FIN_SIT = 0) THEN 99999999
                    ELSE tf.DT_FIN_SIT
                END) >= p.DATE_DEBUT_PERIODE)))
            LIMIT 1))
            AND (t.NATURE_CONTRAT = 2)
            AND (s.ID_PERIODE = p.ID_PERIODE));
        
-- RED-216 & RED-217 
DROP VIEW IF EXISTS V_Categorie_quittancement_salaires_a_inserer;
CREATE VIEW V_Categorie_quittancement_salaires_a_inserer AS
SELECT 
        p.ID_PERIODE AS ID_PERIODE,
        t.NOCAT AS NOCAT,
        (CASE
            WHEN (i.IDENTIFIANT_REPERTOIRE = '') THEN i.NTT
            ELSE i.IDENTIFIANT_REPERTOIRE
        END) AS INDIVIDU,
        t.LICAT AS LICAT,
        t.CONBCOT AS CONBCOT,
        COALESCE(SUM(CASE
                    WHEN
                        ba.ID_BASE_ASSUJETTIE IN (SELECT 
                                tcba.ID_BASE_ASSUJETTIE
                            FROM
                                TRANCHE_CATEGORIE_BASE_ASSUJETTIE tcba
                            WHERE
                                tcba.NOCAT = t.NOCAT
                                    AND tcba.ID_PERIODE = p.ID_PERIODE)
                    THEN
                        ba.MONTANT_COTISATION
                    ELSE NULL
                END),
                0) AS MONTANT_COTISATION,
		SUM(tcba.ESTIMATION_COTISATION) AS ESTIMATION_COTISATION,
        NULL AS CHAMP_NULL_1,
        NULL AS CHAMP_NULL_2
    FROM
        TARIFS t
            INNER JOIN
        PERIODES_RECUES p ON t.NOCO = p.NUMERO_CONTRAT
            INNER JOIN
        RATTACHEMENT_DECLARATIONS_RECUES r ON r.ID_PERIODE = p.ID_PERIODE
            INNER JOIN
        INDIVIDU i ON i.ID_ADH_ETAB_MOIS = r.ID_ADH_ETAB_MOIS
            AND i.TMP_BATCH IS FALSE
            INNER JOIN
        CONTRATS c ON c.NOCO = t.NOCO
            INNER JOIN
        PARAM_FAMILLE_CONTRAT par ON par.NOFAM = c.NOFAM
            LEFT JOIN
        CONTRAT_TRAVAIL ct ON ct.ID_INDIVIDU = i.ID_INDIVIDU
            LEFT JOIN
        AFFILIATION a ON ct.ID_CONTRAT_TRAVAIL = a.ID_CONTRAT_TRAVAIL
            LEFT JOIN
        BASE_ASSUJETTIE ba ON a.ID_AFFILIATION = ba.ID_AFFILIATION
			LEFT JOIN tranche_categorie_base_assujettie tcba ON (tcba.NOCAT = t.NOCAT
                    AND tcba.ID_PERIODE = p.ID_PERIODE
                    AND tcba.ID_BASE_ASSUJETTIE = ba.ID_BASE_ASSUJETTIE)
    WHERE
        p.RECONSOLIDER = 'O' AND t.NATURE_CONTRAT = 2
            AND (t.TMP_BATCH IS FALSE)
            AND par.TYPE_CONSO_SALAIRE = 'INDIV'
            AND (par.TMP_BATCH IS FALSE)
            AND (c.TMP_BATCH IS FALSE)
            AND (c.DT_DEBUT_SIT <= p.DATE_DEBUT_PERIODE)
            AND ((COALESCE(c.DT_FIN_SIT, 99999999) >= p.DATE_DEBUT_PERIODE)
            OR ((CASE
            WHEN (c.DT_FIN_SIT = 0) THEN 99999999
            ELSE c.DT_FIN_SIT
        END) >= p.DATE_DEBUT_PERIODE))
            AND ((t.NOCO = p.NUMERO_CONTRAT)
            AND (t.DT_DEBUT_SIT <= p.DATE_DEBUT_PERIODE)
            AND ((COALESCE(t.DT_FIN_SIT, 99999999) >= p.DATE_DEBUT_PERIODE)
            OR ((CASE
            WHEN (t.DT_FIN_SIT = 0) THEN 99999999
            ELSE t.DT_FIN_SIT
        END) >= p.DATE_DEBUT_PERIODE))
            AND r.ID_ADH_ETAB_MOIS IN (SELECT 
                adh.ID_ADH_ETAB_MOIS
            FROM
                ADHESION_ETABLISSEMENT_MOIS adh
            WHERE
                adh.ID_ADH_ETAB_MOIS = r.ID_ADH_ETAB_MOIS
                    AND adh.TMP_BATCH IS FALSE))	
    GROUP BY ID_PERIODE , NOCAT , LICAT , INDIVIDU , CONBCOT
    UNION SELECT 
        p.ID_PERIODE AS ID_PERIODE,
        t.NOCAT AS NOCAT,
        'TOUS' AS INDIVIDU,
        t.LICAT AS LICAT,
        t.CONBCOT AS CONBCOT,
        COALESCE((SELECT 
                        SUM(ba.MONTANT_COTISATION)
                    FROM
                        BASE_ASSUJETTIE ba
                    WHERE
                        ba.ID_BASE_ASSUJETTIE IN (SELECT 
                                tcba.ID_BASE_ASSUJETTIE
                            FROM
                                TRANCHE_CATEGORIE_BASE_ASSUJETTIE tcba
                            WHERE
                                ((tcba.ID_PERIODE = p.ID_PERIODE)
                                    AND (tcba.NOCAT = t.NOCAT)))),
                0) AS MONTANT_COTISATION,
        (SELECT 
                SUM(tcba.ESTIMATION_COTISATION)
            FROM
                TRANCHE_CATEGORIE_BASE_ASSUJETTIE tcba
            WHERE
                ((tcba.ID_PERIODE = p.ID_PERIODE)
                    AND (tcba.NOCAT = t.NOCAT))) AS ESTIMATION_COTISATION,
        NULL AS CHAMP_NULL_1,
        NULL AS CHAMP_NULL_2
    FROM
        (TARIFS t
        JOIN PERIODES_RECUES p ON ((t.NOCO = p.NUMERO_CONTRAT)))
            INNER JOIN
        CONTRATS c ON c.NOCO = t.NOCO
            INNER JOIN
        PARAM_FAMILLE_CONTRAT par ON (par.NOFAM = c.NOFAM)
    WHERE
        (t.NOCO = p.NUMERO_CONTRAT
            AND t.DT_DEBUT_SIT <= p.DATE_DEBUT_PERIODE
            AND (COALESCE(t.DT_FIN_SIT, 99999999) >= p.DATE_DEBUT_PERIODE
            OR ((CASE
            WHEN (t.DT_FIN_SIT = 0) THEN 99999999
            ELSE t.DT_FIN_SIT
        END) >= p.DATE_DEBUT_PERIODE))
            AND p.RECONSOLIDER = 'O'
            AND t.NATURE_CONTRAT = 2
            AND (t.TMP_BATCH IS FALSE)
            AND par.TYPE_CONSO_SALAIRE = 'TOUS'
            AND (par.TMP_BATCH IS FALSE)
            AND (c.TMP_BATCH IS FALSE)
            AND (c.DT_DEBUT_SIT <= p.DATE_DEBUT_PERIODE)
            AND ((COALESCE(c.DT_FIN_SIT, 99999999) >= p.DATE_DEBUT_PERIODE)
            OR ((CASE
            WHEN (c.DT_FIN_SIT = 0) THEN 99999999
            ELSE c.DT_FIN_SIT
        END) >= p.DATE_DEBUT_PERIODE)));
        
        
-- ECM --        
DROP VIEW IF EXISTS V_Liste_effectif_categorie_contrat_travail;
CREATE VIEW V_Liste_effectif_categorie_contrat_travail AS select ct.ID_PERIODE AS ID_PERIODE,ct.NOCAT AS NOCAT,ct.DEBUT_BASE AS DEBUT_BASE,ct.FIN_BASE AS FIN_BASE,ct.TOTAL_EFFECTIF AS TOTAL_EFFECTIF,s.DATE_DEBUT_PERIODE AS DATE_DEBUT_PERIODE,s.DATE_FIN_PERIODE AS DATE_FIN_PERIODE from ((EFFECTIF_CATEGORIE_CONTRAT_TRAVAIL ct join V_Selection_periode_nature_contrat_effectifs s on((s.ID_PERIODE = ct.ID_PERIODE))) join CATEGORIE_QUITTANCEMENT_INDIVIDU cq on(((cq.ID_PERIODE = s.ID_PERIODE) and (cq.NOCAT = ct.NOCAT)))) where (ct.TOTAL_EFFECTIF <> 0);


-- CQI EFF -- 
DROP VIEW IF EXISTS V_Distinct_noco_nocat_licat_pour_contrat_et_plage;
CREATE VIEW V_Distinct_noco_nocat_licat_pour_contrat_et_plage AS select distinct s.ID_PERIODE AS ID_PERIODE,t.NOCO AS NOCO,t.NOCAT AS NOCAT,t.LICAT AS LICAT,t.CONBCOT AS CONBCOT from (TARIFS t join V_Selection_periode_nature_contrat_effectifs s on((t.NOCO = s.NUMERO_CONTRAT))) where ((t.DT_DEBUT_SIT <= s.DATE_FIN_PERIODE) and ((coalesce(t.DT_FIN_SIT,99999999) >= s.DATE_DEBUT_PERIODE) or ((case when (t.DT_FIN_SIT = 0) then 99999999 else t.DT_FIN_SIT end) >= s.DATE_DEBUT_PERIODE)));


DROP VIEW IF EXISTS V_Distinct_noco_nocat_pour_contrat_et_plage;
CREATE VIEW `V_Distinct_noco_nocat_pour_contrat_et_plage` AS
    SELECT DISTINCT
        `s`.`ID_PERIODE` AS `ID_PERIODE`,
        `t`.`NOCO` AS `NOCO`,
        `t`.`NOCAT` AS `NOCAT`
    FROM
        (`TARIFS` `t`
        JOIN `V_Selection_periode_nature_contrat_effectifs` `s` ON ((`t`.`NOCO` = `s`.`NUMERO_CONTRAT`)))
    WHERE
        ((`t`.`DT_DEBUT_SIT` <= `s`.`DATE_DEBUT_PERIODE`)
            AND ((COALESCE(`t`.`DT_FIN_SIT`, 99999999) >= `s`.`DATE_DEBUT_PERIODE`)
            OR ((CASE
            WHEN (`t`.`DT_FIN_SIT` = 0) THEN 99999999
            ELSE `t`.`DT_FIN_SIT`
        END) >= `s`.`DATE_DEBUT_PERIODE`)));

        
DROP VIEW IF EXISTS V_Selection_categorie_quittancement_effectif_data;
CREATE VIEW V_Selection_categorie_quittancement_effectif_data AS
   SELECT 
        d.ID_PERIODE AS ID_PERIODE,
        d.NOCAT AS NOCAT,
        e.DEBUT_BASE AS DEBUT_BASE,
        e.FIN_BASE AS FIN_BASE,
        (CASE WHEN e.PRORATA_COTISATION IS NULL THEN 0 ELSE e.PRORATA_COTISATION END)  AS PRORATA_COTISATION,
        (CASE WHEN e.ESTIMATION_COTISATION IS NULL THEN 0 ELSE e.ESTIMATION_COTISATION END)  AS ESTIM_COTISATION,
        e.TOTAL_EFFECTIF AS TOTAL_EFFECTIF,
        (CASE
            WHEN (e.DEBUT_BASE = s.DATE_DEBUT_PERIODE) THEN e.TOTAL_EFFECTIF
            ELSE 0
        END) AS effectif_debut,
        (CASE
            WHEN (e.FIN_BASE = s.DATE_FIN_PERIODE) THEN e.TOTAL_EFFECTIF
            ELSE 0
        END) AS effectif_fin,
        s.DATE_DEBUT_PERIODE AS DATE_DEBUT_PERIODE,
        s.DATE_FIN_PERIODE AS DATE_FIN_PERIODE, 
        t.CONBCOT AS CONBCOT,
        t.LICAT AS LICAT
    FROM
        (((EFFECTIF_CATEGORIE_CONTRAT_TRAVAIL e
        RIGHT JOIN V_Distinct_noco_nocat_pour_contrat_et_plage d ON (((d.ID_PERIODE = e.ID_PERIODE)
            AND (d.NOCAT = e.NOCAT))))
        JOIN V_Selection_periode_nature_contrat_effectifs s ON ((s.ID_PERIODE = d.ID_PERIODE)))
        JOIN TARIFS t ON (((t.NOCAT = d.NOCAT)
            AND (t.NOCO = d.NOCO)
            AND (t.DT_DEBUT_SIT <= s.DATE_DEBUT_PERIODE)
            AND ((COALESCE(t.DT_FIN_SIT, 99999999) >= s.DATE_DEBUT_PERIODE)
            OR ((CASE
            WHEN (t.DT_FIN_SIT = 0) THEN 99999999
            ELSE t.DT_FIN_SIT
        END) >= s.DATE_DEBUT_PERIODE)))));
        

-- TCBA -- 
DROP VIEW IF EXISTS V_Selection_base_assujettie;
CREATE VIEW V_Selection_base_assujettie AS SELECT 
    `ba`.`ID_BASE_ASSUJETTIE` AS `ID_BASE_ASSUJETTIE`,
    `ba`.`ID_AFFILIATION` AS `ID_AFFILIATION`,
    `ba`.`DATE_DEB_RATTACHEMENT` AS `DATE_DEB_RATTACHEMENT`,
    `ba`.`DATE_FIN_RATTACHEMENT` AS `DATE_FIN_RATTACHEMENT`,
    `ba`.`TMP_BATCH` AS `TMP_BATCH`,
    `s`.`ID_PERIODE` AS `id_periode`,
    `s`.`NUMERO_CONTRAT` AS `numero_contrat`,
    `s`.`TYPE_PERIODE` AS `TYPE_PERIODE`,
    `s`.`DATE_CREATION` AS `DATE_CREATION`,
    `s`.`DATE_DEBUT_PERIODE` AS `DATE_DEBUT_PERIODE`,
    `s`.`DATE_FIN_PERIODE` AS `DATE_FIN_PERIODE`,
    `adh`.`MOIS_RATTACHEMENT` AS `mois_rattachement`,
    COALESCE(IF((`a`.`CODE_POPULATION` = ''),
                NULL,
                `a`.`CODE_POPULATION`),
            (SELECT 
                    `t`.`NOCAT`
                FROM
                    `TARIFS` `t`
                WHERE
                    ((`t`.`NOCO` = `s`.`NUMERO_CONTRAT`)
                        AND (`t`.`DT_DEBUT_SIT` <= `s`.`DATE_DEBUT_PERIODE`)
                        AND ((COALESCE(`t`.`DT_FIN_SIT`, 99999999) >= `s`.`DATE_DEBUT_PERIODE`)
                        OR ((CASE
                        WHEN (`t`.`DT_FIN_SIT` = 0) THEN 99999999
                        ELSE `t`.`DT_FIN_SIT`
                    END) >= `s`.`DATE_DEBUT_PERIODE`)))
                ORDER BY `t`.`DT_DEBUT_SIT`
                LIMIT 1)) AS `NOCAT`
FROM
    ((((((`BASE_ASSUJETTIE` `ba`
    LEFT JOIN `AFFILIATION` `a` ON ((`ba`.`ID_AFFILIATION` = `a`.`ID_AFFILIATION`)))
    LEFT JOIN `CONTRAT_TRAVAIL` `ct` ON ((`a`.`ID_CONTRAT_TRAVAIL` = `ct`.`ID_CONTRAT_TRAVAIL`)))
    LEFT JOIN `INDIVIDU` `i` ON ((`ct`.`ID_INDIVIDU` = `i`.`ID_INDIVIDU`)))
    LEFT JOIN `ADHESION_ETABLISSEMENT_MOIS` `adh` ON ((`i`.`ID_ADH_ETAB_MOIS` = `adh`.`ID_ADH_ETAB_MOIS`)))
    JOIN `RATTACHEMENT_DECLARATIONS_RECUES` `r` ON ((`adh`.`ID_ADH_ETAB_MOIS` = `r`.`ID_ADH_ETAB_MOIS`)))
    JOIN `V_Selection_periode_nature_contrat_salaires` `s` ON ((`s`.`ID_PERIODE` = `r`.`ID_PERIODE`)))
WHERE
    ((`ba`.`MONTANT_COTISATION` <> 0)
        AND (`adh`.`TMP_BATCH` IS FALSE));
   
        
-- Vue pour le partitionning -- 
DROP VIEW IF EXISTS V_Effectif_categorie_contrat_travail_a_calculer;
CREATE VIEW V_Effectif_categorie_contrat_travail_a_calculer AS select s.ID_PERIODE AS ID_PERIODE,count(0) AS NB_ENTITES_ECCT from (((((V_Selection_periode_nature_contrat_effectifs s join V_Distinct_noco_nocat_licat_pour_contrat_et_plage d on((d.ID_PERIODE = s.ID_PERIODE))) join RATTACHEMENT_DECLARATIONS_RECUES r on((r.ID_PERIODE = d.ID_PERIODE))) join ADHESION_ETABLISSEMENT_MOIS a on((a.ID_ADH_ETAB_MOIS = r.ID_ADH_ETAB_MOIS))) join INDIVIDU i on((i.ID_ADH_ETAB_MOIS = a.ID_ADH_ETAB_MOIS))) join CONTRAT_TRAVAIL ct on((ct.ID_INDIVIDU = i.ID_INDIVIDU))) group by s.ID_PERIODE;


-- Vue F04_RG_P5_03 pour contrats sur salaires
DROP VIEW IF EXISTS V_Trace_Nocat_Inconnu_salaires;
CREATE VIEW V_Trace_Nocat_Inconnu_salaires AS 
SELECT DISTINCT
    vpms.ID_PERIODE,CONCAT('La situation tarifaire [numContrat=',
                vpms.NUMERO_CONTRAT,
                ', numCategorieQuittancement=',
                a.CODE_POPULATION,
                ', dateDebutPeriode=',
                vpms.DATE_DEBUT_PERIODE,
                '] n\'existe pas. Periode traitee : [PeriodeRecue(idPeriode=',
                vpms.ID_PERIODE,
                ', typePeriode=',
                vpms.TYPE_PERIODE,
                ', dateDebutPeriode=',
                vpms.DATE_DEBUT_PERIODE,
                ', dateFinPeriode=',
                vpms.DATE_FIN_PERIODE,
                ', dateCreation=',
                vpms.DATE_CREATION,
                ')]') AS MESSAGE
FROM
	V_Selection_periode_nature_contrat_salaires vpms,
    AFFILIATION a
        LEFT JOIN
    CONTRAT_TRAVAIL ct ON a.ID_CONTRAT_TRAVAIL = ct.ID_CONTRAT_TRAVAIL
        LEFT JOIN
    INDIVIDU i ON ct.ID_INDIVIDU = i.ID_INDIVIDU
        LEFT JOIN
    ADHESION_ETABLISSEMENT_MOIS adh ON i.ID_ADH_ETAB_MOIS = adh.ID_ADH_ETAB_MOIS
WHERE
    adh.TMP_BATCH IS FALSE
		AND a.CODE_POPULATION <> ''
        AND adh.ID_ADH_ETAB_MOIS IN (SELECT 
            r.ID_ADH_ETAB_MOIS
        FROM
            RATTACHEMENT_DECLARATIONS_RECUES r
        WHERE
            r.ID_PERIODE = vpms.ID_PERIODE)
        AND a.CODE_POPULATION NOT IN (SELECT DISTINCT
            t.NOCAT
        FROM
            TARIFS t
        WHERE
            (t.NOCO = vpms.NUMERO_CONTRAT)
            AND (t.NOCAT <> '')
                AND (t.DT_DEBUT_SIT <= vpms.DATE_DEBUT_PERIODE)
                AND ((COALESCE(t.DT_FIN_SIT, 99999999) >= vpms.DATE_DEBUT_PERIODE)
                OR ((CASE
                WHEN (t.DT_FIN_SIT = 0) THEN 99999999
                ELSE t.DT_FIN_SIT
            END) >= vpms.DATE_DEBUT_PERIODE)));