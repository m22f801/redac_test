-- Version du schéma REDAC actuel
INSERT INTO VERSION_BASE (VERSION, DESCRIPTION) VALUES ('1.7.4', 'Version supportant les flux 10, 1, 2, 3, 4, 5/6, 7/8 et 9');

-- RED-80 Ajout des colonnes dans l'entite periodes reçues
alter table periodes_recues add column AFFECTE_A VARCHAR(50); 
alter table periodes_recues add column A_TRAITER_PAR VARCHAR(50); 
alter table periodes_recues add column TRAITE_PAR VARCHAR(50); 

-- RED-191 Ajout colonne dans l'entité tarif
alter table tarifs add column NATURE_CONTRAT NUMERIC(1) AFTER MOCALCOT;

-- RED-208 Suppression de la colonne PK_ENTITEE_BRIQUE de la table ADHESION_ETABLISSEMENT_MOIS
ALTER TABLE ADHESION_ETABLISSEMENT_MOIS DROP COLUMN PK_ENTITEE_BRIQUE;
