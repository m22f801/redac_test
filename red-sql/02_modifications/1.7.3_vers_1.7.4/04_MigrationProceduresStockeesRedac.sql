-- RED-80 Blocage periodes
DROP PROCEDURE IF EXISTS Creation_MessageControle_en_masse;
DELIMITER $$
CREATE PROCEDURE Creation_MessageControle_en_masse(IN p_liste_ids_periodes TEXT,IN message CHAR(100) ,IN idControle VARCHAR(30), IN param1 CHAR(50),IN param2 CHAR(50),IN param3 CHAR(50),IN param4 CHAR(50), IN niveau CHAR(10) , IN origine CHAR(6) , IN dateJour DECIMAL(8,0) , IN userCreation VARCHAR(100))
BEGIN
	
INSERT INTO message_controle(ID_PERIODE,ID_CONTROLE,ORIGINE,DATE_TRAITEMENT,NIVEAU_ALERTE,MESSAGE_UTILISATEUR,PARAM1,PARAM2,PARAM3,PARAM4,USER_CREATION)
SELECT p.ID_PERIODE,idControle,origine,dateJour,niveau,message,param1,param2,param3,param4,userCreation
FROM periodes_recues p WHERE FIND_IN_SET(p.ID_PERIODE, p_liste_ids_periodes) > 0 AND p.ID_PERIODE NOT IN(SELECT m.ID_PERIODE from message_controle m WHERE m.ID_CONTROLE=(idControle COLLATE latin1_general_ci)  AND m.ORIGINE=(origine COLLATE latin1_general_ci) AND FIND_IN_SET(m.ID_PERIODE, p_liste_ids_periodes) > 0 );

END$$
DELIMITER ;



-- RED-80 Changer Statut périodes en masse
DROP PROCEDURE IF EXISTS Creation_HistoriqueEtatPeriode_en_masse;
DELIMITER $$
CREATE PROCEDURE Creation_HistoriqueEtatPeriode_en_masse(IN p_liste_ids_periodes TEXT,IN datehms NUMERIC(14), IN etatPeriode CHAR(3), IN origine CHAR(4),
IN utilisateur CHAR(30), IN commentaire VARCHAR(1000), IN userCreation VARCHAR(20))
BEGIN
	
INSERT INTO HISTO_ETAT_PERIODE(ID_PERIODE, DATEHMS_CHANGEMENT, ETAT_PERIODE, ORIGINE, UTILISATEUR, COMMENTAIRE, USER_CREATION)
SELECT p.ID_PERIODE,datehms,etatPeriode,origine,utilisateur,commentaire,userCreation
FROM periodes_recues p WHERE FIND_IN_SET(ID_PERIODE, p_liste_ids_periodes) > 0;

END$$
DELIMITER ;


-- RED-191

DROP PROCEDURE IF EXISTS Inserer_CATEGORIE_QUITTANCEMENT_INDIVIDU_SALAIRE_Pour_Periodes;

DELIMITER $$
CREATE PROCEDURE Inserer_CATEGORIE_QUITTANCEMENT_INDIVIDU_SALAIRE_Pour_Periodes(IN p_audit_nom_batch VARCHAR(20), IN p_liste_ids_periodes TEXT)
BEGIN
INSERT INTO CATEGORIE_QUITTANCEMENT_INDIVIDU (
ID_PERIODE
,NOCAT
,INDIVIDU
,LICAT
,CONBCOT
,MT_COTISATION
,ESTIMATION_COTISATION
,EFFECTIF_DEBUT
,EFFECTIF_FIN
,USER_CREATION
)
SELECT 
        p.ID_PERIODE AS ID_PERIODE,
        t.NOCAT AS NOCAT,
        (CASE
            WHEN (i.IDENTIFIANT_REPERTOIRE = '') THEN i.NTT
            ELSE i.IDENTIFIANT_REPERTOIRE
        END) AS INDIVIDU,
        t.LICAT AS LICAT,
        t.CONBCOT AS CONBCOT,
        COALESCE(SUM(CASE
                    WHEN
                        ba.ID_BASE_ASSUJETTIE IN (SELECT 
                                tcba.ID_BASE_ASSUJETTIE
                            FROM
                                TRANCHE_CATEGORIE_BASE_ASSUJETTIE tcba
                            WHERE
                                tcba.NOCAT = t.NOCAT
                                    AND tcba.ID_PERIODE = p.ID_PERIODE)
                    THEN
                        ba.MONTANT_COTISATION
                    ELSE NULL
                END),
                0) AS MONTANT_COTISATION,
        (SELECT 
                SUM(tcba.ESTIMATION_COTISATION)
            FROM
                TRANCHE_CATEGORIE_BASE_ASSUJETTIE tcba
            WHERE
                tcba.NOCAT = t.NOCAT
                    AND tcba.ID_PERIODE = p.ID_PERIODE
                    AND tcba.ID_BASE_ASSUJETTIE = ba.ID_BASE_ASSUJETTIE) AS ESTIMATION_COTISATION,
        NULL AS CHAMP_NULL_1,
        NULL AS CHAMP_NULL_2,
        p_audit_nom_batch
    FROM
        TARIFS t
            INNER JOIN
        PERIODES_RECUES p ON t.NOCO = p.NUMERO_CONTRAT
            INNER JOIN
        RATTACHEMENT_DECLARATIONS_RECUES r ON r.ID_PERIODE = p.ID_PERIODE
            INNER JOIN
        INDIVIDU i ON i.ID_ADH_ETAB_MOIS = r.ID_ADH_ETAB_MOIS
            AND i.TMP_BATCH IS FALSE
            INNER JOIN
        CONTRATS c ON c.NOCO = t.NOCO
            INNER JOIN
        PARAM_FAMILLE_CONTRAT par ON par.NOFAM = c.NOFAM
            INNER JOIN
        CONTRAT_TRAVAIL ct ON ct.ID_INDIVIDU = i.ID_INDIVIDU
            INNER JOIN
        AFFILIATION a ON ct.ID_CONTRAT_TRAVAIL = a.ID_CONTRAT_TRAVAIL
            INNER JOIN
        BASE_ASSUJETTIE ba ON a.ID_AFFILIATION = ba.ID_AFFILIATION
    WHERE
        p.RECONSOLIDER = 'O' AND t.NATURE_CONTRAT = 2
            AND (t.TMP_BATCH IS FALSE)
            AND par.TYPE_CONSO_SALAIRE = 'INDIV'
            AND (par.TMP_BATCH IS FALSE)
            AND (c.TMP_BATCH IS FALSE)
            AND (c.DT_DEBUT_SIT <= p.DATE_DEBUT_PERIODE)
            AND ((COALESCE(c.DT_FIN_SIT, 99999999) >= p.DATE_DEBUT_PERIODE)
            OR ((CASE
            WHEN (c.DT_FIN_SIT = 0) THEN 99999999
            ELSE c.DT_FIN_SIT
        END) >= p.DATE_DEBUT_PERIODE))
            AND ((t.NOCO = p.NUMERO_CONTRAT)
            AND (t.DT_DEBUT_SIT <= p.DATE_DEBUT_PERIODE)
            AND ((COALESCE(t.DT_FIN_SIT, 99999999) >= p.DATE_DEBUT_PERIODE)
            OR ((CASE
            WHEN (t.DT_FIN_SIT = 0) THEN 99999999
            ELSE t.DT_FIN_SIT
        END) >= p.DATE_DEBUT_PERIODE))
            AND r.ID_ADH_ETAB_MOIS IN (SELECT 
                adh.ID_ADH_ETAB_MOIS
            FROM
                ADHESION_ETABLISSEMENT_MOIS adh
            WHERE
                adh.ID_ADH_ETAB_MOIS = r.ID_ADH_ETAB_MOIS
                    AND adh.TMP_BATCH IS FALSE))
			AND FIND_IN_SET(p.ID_PERIODE, p_liste_ids_periodes) > 0
    GROUP BY ID_PERIODE , NOCAT , LICAT , INDIVIDU , CONBCOT 
    UNION SELECT 
        p.ID_PERIODE AS ID_PERIODE,
        t.NOCAT AS NOCAT,
        'TOUS' AS INDIVIDU,
        t.LICAT AS LICAT,
        t.CONBCOT AS CONBCOT,
        COALESCE((SELECT 
                        SUM(ba.MONTANT_COTISATION)
                    FROM
                        BASE_ASSUJETTIE ba
                    WHERE
                        ba.ID_BASE_ASSUJETTIE IN (SELECT 
                                tcba.ID_BASE_ASSUJETTIE
                            FROM
                                TRANCHE_CATEGORIE_BASE_ASSUJETTIE tcba
                            WHERE
                                ((tcba.ID_PERIODE = p.ID_PERIODE)
                                    AND (tcba.NOCAT = t.NOCAT)))),
                0) AS MONTANT_COTISATION,
        (SELECT 
                SUM(tcba.ESTIMATION_COTISATION)
            FROM
                TRANCHE_CATEGORIE_BASE_ASSUJETTIE tcba
            WHERE
                ((tcba.ID_PERIODE = p.ID_PERIODE)
                    AND (tcba.NOCAT = t.NOCAT))) AS ESTIMATION_COTISATION,
        NULL AS CHAMP_NULL_1,
        NULL AS CHAMP_NULL_2,
        p_audit_nom_batch
    FROM
        (TARIFS t
        JOIN PERIODES_RECUES p ON ((t.NOCO = p.NUMERO_CONTRAT)))
            INNER JOIN
        CONTRATS c ON c.NOCO = t.NOCO
            INNER JOIN
        PARAM_FAMILLE_CONTRAT par ON (par.NOFAM = c.NOFAM)
    WHERE
        (t.NOCO = p.NUMERO_CONTRAT
            AND t.DT_DEBUT_SIT <= p.DATE_DEBUT_PERIODE
            AND (COALESCE(t.DT_FIN_SIT, 99999999) >= p.DATE_DEBUT_PERIODE
            OR ((CASE
            WHEN (t.DT_FIN_SIT = 0) THEN 99999999
            ELSE t.DT_FIN_SIT
        END) >= p.DATE_DEBUT_PERIODE))
            AND p.RECONSOLIDER = 'O'
            AND t.NATURE_CONTRAT = 2
            AND (t.TMP_BATCH IS FALSE)
            AND par.TYPE_CONSO_SALAIRE = 'TOUS'
            AND (par.TMP_BATCH IS FALSE)
            AND (c.TMP_BATCH IS FALSE)
            AND (c.DT_DEBUT_SIT <= p.DATE_DEBUT_PERIODE)
            AND ((COALESCE(c.DT_FIN_SIT, 99999999) >= p.DATE_DEBUT_PERIODE)
            OR ((CASE
            WHEN (c.DT_FIN_SIT = 0) THEN 99999999
            ELSE c.DT_FIN_SIT
        END) >= p.DATE_DEBUT_PERIODE)))
        AND FIND_IN_SET(p.ID_PERIODE, p_liste_ids_periodes) > 0;

END$$
DELIMITER ;


DROP PROCEDURE IF EXISTS Inserer_TRANCHE_CATEGORIE;


DELIMITER $$
CREATE PROCEDURE Inserer_TRANCHE_CATEGORIE(IN p_audit_nom_batch VARCHAR(20))
BEGIN
-- Calcul et insersion des nouveaux éléments des périodes à traiter
INSERT INTO TRANCHE_CATEGORIE (ID_PERIODE, NOCAT, INDIVIDU, NUM_TRANCHE, LIB_TRANCHE, MT_TRANCHE, USER_CREATION)
select c.ID_PERIODE, c.NOCAT, c.INDIVIDU, p.NUM_TRANCHE, p.LIB_TRANCHE, 
coalesce(case WHEN (c.INDIVIDU = 'TOUS') THEN (SELECT SUM(tcba.MT_TRANCHE) 
FROM TRANCHE_CATEGORIE_BASE_ASSUJETTIE tcba 
WHERE tcba.ID_PERIODE = c.ID_PERIODE
AND tcba.NOCAT = c.NOCAT
AND tcba.NUM_TRANCHE = p.NUM_TRANCHE) 
ELSE (SELECT SUM(tcba.MT_TRANCHE) 
FROM TRANCHE_CATEGORIE_BASE_ASSUJETTIE tcba 
LEFT JOIN AFFILIATION a ON tcba.ID_AFFILIATION = a.ID_AFFILIATION
LEFT JOIN CONTRAT_TRAVAIL ct ON a.ID_CONTRAT_TRAVAIL = ct.ID_CONTRAT_TRAVAIL
LEFT JOIN INDIVIDU i ON ct.ID_INDIVIDU = i.ID_INDIVIDU 
WHERE tcba.ID_PERIODE = c.ID_PERIODE
AND tcba.NOCAT = c.NOCAT
AND tcba.NUM_TRANCHE = p.NUM_TRANCHE
AND (i.IDENTIFIANT_REPERTOIRE = c.INDIVIDU OR i.ntt = c.INDIVIDU)) END, 0), 
p_audit_nom_batch
from CATEGORIE_QUITTANCEMENT_INDIVIDU c
INNER JOIN (select distinct pnb.NUM_TRANCHE, pnb.LIB_TRANCHE, pnb.CONBCOT from PARAM_NATURE_BASE pnb) p ON p.CONBCOT = c.CONBCOT
INNER JOIN V_Selection_periode_nature_contrat_salaires s on s.ID_PERIODE = c.ID_PERIODE;

END$$
DELIMITER ;



DROP PROCEDURE IF EXISTS Inserer_TRANCHE_CATEGORIE_Pour_Periodes;

DELIMITER $$
CREATE PROCEDURE Inserer_TRANCHE_CATEGORIE_Pour_Periodes(IN p_audit_nom_batch VARCHAR(20), IN p_liste_ids_periodes TEXT)
BEGIN
-- Calcul et insersion des nouveaux éléments des périodes à traiter
INSERT INTO TRANCHE_CATEGORIE (ID_PERIODE, NOCAT, INDIVIDU, NUM_TRANCHE, LIB_TRANCHE, MT_TRANCHE, USER_CREATION)
select c.ID_PERIODE, c.NOCAT, c.INDIVIDU, p.NUM_TRANCHE, p.LIB_TRANCHE, 
coalesce(case WHEN (c.INDIVIDU = 'TOUS') THEN (SELECT SUM(tcba.MT_TRANCHE) 
FROM TRANCHE_CATEGORIE_BASE_ASSUJETTIE tcba 
WHERE tcba.ID_PERIODE = c.ID_PERIODE
AND tcba.NOCAT = c.NOCAT
AND tcba.NUM_TRANCHE = p.NUM_TRANCHE) 
ELSE (SELECT SUM(tcba.MT_TRANCHE) 
FROM TRANCHE_CATEGORIE_BASE_ASSUJETTIE tcba 
LEFT JOIN AFFILIATION a ON tcba.ID_AFFILIATION = a.ID_AFFILIATION
LEFT JOIN CONTRAT_TRAVAIL ct ON a.ID_CONTRAT_TRAVAIL = ct.ID_CONTRAT_TRAVAIL
LEFT JOIN INDIVIDU i ON ct.ID_INDIVIDU = i.ID_INDIVIDU 
WHERE tcba.ID_PERIODE = c.ID_PERIODE
AND tcba.NOCAT = c.NOCAT
AND tcba.NUM_TRANCHE = p.NUM_TRANCHE
AND (i.IDENTIFIANT_REPERTOIRE = c.INDIVIDU OR i.ntt = c.INDIVIDU)) END, 0), 
p_audit_nom_batch
from CATEGORIE_QUITTANCEMENT_INDIVIDU c
INNER JOIN (select distinct pnb.NUM_TRANCHE, pnb.LIB_TRANCHE, pnb.CONBCOT from PARAM_NATURE_BASE pnb) p ON p.CONBCOT = c.CONBCOT
INNER JOIN V_Selection_periode_nature_contrat_salaires s on s.ID_PERIODE = c.ID_PERIODE
WHERE FIND_IN_SET(c.ID_PERIODE, p_liste_ids_periodes) > 0;
END$$
DELIMITER ;


DROP PROCEDURE IF EXISTS Purger_elements_declaratifs_en_masse;

DELIMITER $$
CREATE PROCEDURE Purger_elements_declaratifs_en_masse(IN p_liste_ids_periodes TEXT)
BEGIN
	IF p_liste_ids_periodes = 'ALL' THEN BEGIN
		DELETE ecm FROM EFFECTIF_CATEGORIE_MVT ecm 
		INNER JOIN V_Selection_periode_nature_contrat_effectifs s ON s.ID_PERIODE = ecm.ID_PERIODE; 

		DELETE cqi FROM CATEGORIE_QUITTANCEMENT_INDIVIDU cqi
		INNER JOIN V_Selection_periode_nature_contrat_effectifs s ON s.ID_PERIODE = cqi.ID_PERIODE; 

		DELETE ecct FROM EFFECTIF_CATEGORIE_CONTRAT_TRAVAIL ecct
		INNER JOIN V_Selection_periode_nature_contrat_effectifs s ON s.ID_PERIODE = ecct.ID_PERIODE; 


		DELETE tc FROM TRANCHE_CATEGORIE tc
		INNER JOIN V_Selection_periode_nature_contrat_salaires s ON s.ID_PERIODE = tc.ID_PERIODE; 

		DELETE cqi FROM CATEGORIE_QUITTANCEMENT_INDIVIDU cqi
		INNER JOIN V_Selection_periode_nature_contrat_salaires s ON s.ID_PERIODE = cqi.ID_PERIODE; 

		DELETE tcba FROM TRANCHE_CATEGORIE_BASE_ASSUJETTIE tcba
		INNER JOIN V_Selection_periode_nature_contrat_salaires s ON s.ID_PERIODE = tcba.ID_PERIODE; 

		END;
	ELSE BEGIN
		DELETE FROM CATEGORIE_QUITTANCEMENT_INDIVIDU
		WHERE FIND_IN_SET(ID_PERIODE, p_liste_ids_periodes) > 0;

		DELETE FROM EFFECTIF_CATEGORIE_MVT
		WHERE FIND_IN_SET(ID_PERIODE, p_liste_ids_periodes) > 0;

		DELETE FROM TRANCHE_CATEGORIE
		WHERE FIND_IN_SET(ID_PERIODE, p_liste_ids_periodes) > 0;

		DELETE FROM EFFECTIF_CATEGORIE_CONTRAT_TRAVAIL
		WHERE FIND_IN_SET(ID_PERIODE, p_liste_ids_periodes) > 0;

		DELETE FROM TRANCHE_CATEGORIE_BASE_ASSUJETTIE
		WHERE FIND_IN_SET(ID_PERIODE, p_liste_ids_periodes) > 0;		
		END;
	END IF;
END$$
DELIMITER ;
