-- RED-251 Calcul EstimationCotisation

DROP PROCEDURE IF EXISTS Inserer_TRANCHE_CATEGORIE_BASE_ASSUJETTIE;

DELIMITER $$
CREATE PROCEDURE Inserer_TRANCHE_CATEGORIE_BASE_ASSUJETTIE(IN p_audit_nom_batch VARCHAR(20))
BEGIN
INSERT INTO TRANCHE_CATEGORIE_BASE_ASSUJETTIE 
(
ID_PERIODE
,ID_BASE_ASSUJETTIE
,MOIS_RATTACHEMENT
,ID_AFFILIATION
,DATE_DEB_RATTACHEMENT
,NUM_TRANCHE
,NOCAT
,LIB_TRANCHE
,MT_TRANCHE
,ESTIMATION_COTISATION
,TMP_BATCH
,USER_CREATION
)
SELECT 
    sel.ID_PERIODE,
    sel.ID_BASE_ASSUJETTIE,
    sel.MOIS_RATTACHEMENT,
    sel.ID_AFFILIATION,
    sel.DATE_DEB_RATTACHEMENT,
    p.NUM_TRANCHE,
    t.NOCAT,
    p.LIB_TRANCHE,
    @MT_TRANCHE_SAVE:=COALESCE((SELECT 
                    SUM(c.MONTANT_COMPO_BASE_ASSUJ)
                FROM
                    COMPOSANT_BASE_ASSUJETTIE c
                WHERE
                    c.ID_BASE_ASSUJETTIE = sel.ID_BASE_ASSUJETTIE
                        AND c.TYPE_COMPO_BASE_ASSUJ IN (SELECT 
                            pc.NUM_TCBA
                        FROM
                            PARAM_NATURE_BASE_COMPOSANTS pc
                        WHERE
                            p.CONBCOT = pc.CONBCOT
                                AND p.NUM_TRANCHE = pc.NUM_TRANCHE
                                AND p.TXCALCU_REMPLI = pc.TXCALCU_REMPLI
                                AND pc.mode_conso = 'S'
                                AND pc.TMP_BATCH IS FALSE)
                        AND c.TMP_BATCH IS FALSE),
            0) AS MT_TRANCHE,
    CAST((@MT_TRANCHE_SAVE *
    ((CASE 
    WHEN p.NUM_TRANCHE = 1 THEN t.TXBASE1
    WHEN p.NUM_TRANCHE = 2 THEN t.TXBASE2
    WHEN p.NUM_TRANCHE = 3 THEN t.TXBASE3
    WHEN p.NUM_TRANCHE = 4 THEN t.TXBASE4
    END) / 1000000 )
    * (c.TXAPPCOT / 10000)) AS DECIMAL(15,2)) AS ESTIMATION_COTISATION,
    sel.tmp_batch,
    p_audit_nom_batch
FROM
    V_Selection_base_assujettie sel
        LEFT JOIN
    TARIFS t ON sel.NUMERO_CONTRAT = t.NOCO
        AND sel.NOCAT = t.nocat
        AND t.DT_DEBUT_SIT <= sel.DATE_FIN_RATTACHEMENT
        AND (COALESCE(t.DT_FIN_SIT, 99999999) >= sel.DATE_FIN_RATTACHEMENT
        OR (CASE
        WHEN (t.DT_FIN_SIT = 0) THEN 99999999
        ELSE t.DT_FIN_SIT
    END) >= sel.DATE_FIN_RATTACHEMENT)
        LEFT JOIN
    PARAM_NATURE_BASE p ON t.CONBCOT = p.CONBCOT
        AND (CASE
        WHEN COALESCE(t.txcalcu, 0) > 0 THEN 'O'
        ELSE 'N'
    END) = p.TXCALCU_REMPLI
        INNER JOIN
        CONTRATS c ON (c.TMP_BATCH IS FALSE
	AND c.NOCO = sel.NUMERO_CONTRAT
    AND c.DT_DEBUT_SIT <= sel.DATE_DEBUT_PERIODE
        AND COALESCE(c.DT_FIN_SIT, 99999999) >= sel.DATE_DEBUT_PERIODE)
WHERE
    NOT ISNULL(t.NOCO)
        AND NOT ISNULL(t.NOCAT)
        AND NOT ISNULL(p.NUM_TRANCHE)
ORDER BY ID_PERIODE , ID_BASE_ASSUJETTIE;
END$$
DELIMITER ;

DROP PROCEDURE IF EXISTS Inserer_TRANCHE_CATEGORIE_BASE_ASSUJETTIE_Pour_Periodes;

DELIMITER $$
CREATE PROCEDURE Inserer_TRANCHE_CATEGORIE_BASE_ASSUJETTIE_Pour_Periodes(IN p_audit_nom_batch VARCHAR(20), IN p_liste_ids_periodes TEXT)
BEGIN
INSERT INTO TRANCHE_CATEGORIE_BASE_ASSUJETTIE 
(
ID_PERIODE
,ID_BASE_ASSUJETTIE
,MOIS_RATTACHEMENT
,ID_AFFILIATION
,DATE_DEB_RATTACHEMENT
,NUM_TRANCHE
,NOCAT
,LIB_TRANCHE
,MT_TRANCHE
,ESTIMATION_COTISATION
,TMP_BATCH
,USER_CREATION
)
SELECT 
    sel.ID_PERIODE,
    sel.ID_BASE_ASSUJETTIE,
    sel.MOIS_RATTACHEMENT,
    sel.ID_AFFILIATION,
    sel.DATE_DEB_RATTACHEMENT,
    p.NUM_TRANCHE,
    t.NOCAT,
    p.LIB_TRANCHE,
    @MT_TRANCHE_SAVE:=COALESCE((SELECT 
                    SUM(c.MONTANT_COMPO_BASE_ASSUJ)
                FROM
                    COMPOSANT_BASE_ASSUJETTIE c
                WHERE
                    c.ID_BASE_ASSUJETTIE = sel.ID_BASE_ASSUJETTIE
                        AND c.TYPE_COMPO_BASE_ASSUJ IN (SELECT 
                            pc.NUM_TCBA
                        FROM
                            PARAM_NATURE_BASE_COMPOSANTS pc
                        WHERE
                            p.CONBCOT = pc.CONBCOT
                                AND p.NUM_TRANCHE = pc.NUM_TRANCHE
                                AND p.TXCALCU_REMPLI = pc.TXCALCU_REMPLI
                                AND pc.mode_conso = 'S'
                                AND pc.TMP_BATCH IS FALSE)
                        AND c.TMP_BATCH IS FALSE),
            0) AS MT_TRANCHE,
    CAST((@MT_TRANCHE_SAVE * 
    ((CASE 
    WHEN p.NUM_TRANCHE = 1 THEN t.TXBASE1
    WHEN p.NUM_TRANCHE = 2 THEN t.TXBASE2
    WHEN p.NUM_TRANCHE = 3 THEN t.TXBASE3
    WHEN p.NUM_TRANCHE = 4 THEN t.TXBASE4
    END) / 1000000 )
    * (c.TXAPPCOT / 10000)) AS DECIMAL(15,2)) AS ESTIMATION_COTISATION,
    sel.tmp_batch,
    p_audit_nom_batch
FROM
    V_Selection_base_assujettie sel
        LEFT JOIN
    TARIFS t ON sel.NUMERO_CONTRAT = t.NOCO
        AND sel.NOCAT = t.nocat
        AND t.DT_DEBUT_SIT <= sel.DATE_FIN_RATTACHEMENT
        AND (COALESCE(t.DT_FIN_SIT, 99999999) >= sel.DATE_FIN_RATTACHEMENT
        OR (CASE
        WHEN (t.DT_FIN_SIT = 0) THEN 99999999
        ELSE t.DT_FIN_SIT
    END) >= sel.DATE_FIN_RATTACHEMENT)
        LEFT JOIN
    PARAM_NATURE_BASE p ON t.CONBCOT = p.CONBCOT
        AND (CASE
        WHEN COALESCE(t.txcalcu, 0) > 0 THEN 'O'
        ELSE 'N'
    END) = p.TXCALCU_REMPLI
        INNER JOIN
        CONTRATS c ON (c.TMP_BATCH IS FALSE
	AND c.NOCO = sel.NUMERO_CONTRAT
    AND c.DT_DEBUT_SIT <= sel.DATE_DEBUT_PERIODE
        AND COALESCE(c.DT_FIN_SIT, 99999999) >= sel.DATE_DEBUT_PERIODE)
WHERE
    NOT ISNULL(t.NOCO)
        AND NOT ISNULL(t.NOCAT)
        AND NOT ISNULL(p.NUM_TRANCHE)
        AND FIND_IN_SET(sel.ID_PERIODE, p_liste_ids_periodes) > 0
ORDER BY ID_PERIODE , ID_BASE_ASSUJETTIE;
END$$
DELIMITER ;


