--  RED-148
DROP VIEW IF EXISTS V_Trace_tranche_categorie_base_assujettie;
CREATE VIEW V_Trace_tranche_categorie_base_assujettie AS SELECT DISTINCT
        `sel`.`id_periode` AS `ID_PERIODE`,
        `sel`.`numero_contrat` AS `NUMERO_CONTRAT`,
        `sel`.`DATE_DEB_RATTACHEMENT` AS `DATE_DEB_RATTACHEMENT`,
        `sel`.`DATE_FIN_RATTACHEMENT` AS `DATE_FIN_RATTACHEMENT`,
        `sel`.`NOCAT` AS `NOCAT`,
        CONCAT('La situation tarifaire [numContrat=',
                `sel`.`numero_contrat`,
                ', numCategorie=',
                `sel`.`NOCAT`,
                ', date=',
                `sel`.`DATE_FIN_RATTACHEMENT`,
                '] n\'existe pas. Periode traitee : [PeriodeRecue(idPeriode=',
                `sel`.`id_periode`,
                ', typePeriode=',
                `sel`.`TYPE_PERIODE`,
                ', dateDebutPeriode=',
                `sel`.`DATE_DEBUT_PERIODE`,
                ', dateFinPeriode=',
                `sel`.`DATE_FIN_PERIODE`,
                ', dateCreation=',
                `sel`.`DATE_CREATION`,
                ')]') AS `MESSAGE`
    FROM
        (`V_Selection_base_assujettie` `sel`
        LEFT JOIN `TARIFS` `t` ON (((`sel`.`numero_contrat` = `t`.`NOCO`)
            AND (`sel`.`NOCAT` = `t`.`NOCAT`)
            AND (`t`.`DT_DEBUT_SIT` <= `sel`.`DATE_FIN_RATTACHEMENT`)
            AND ((COALESCE(`t`.`DT_FIN_SIT`, 99999999) >= `sel`.`DATE_FIN_RATTACHEMENT`)
            OR ((CASE
            WHEN (`t`.`DT_FIN_SIT` = 0) THEN 99999999
            ELSE `t`.`DT_FIN_SIT`
        END) >= `sel`.`DATE_FIN_RATTACHEMENT`)))))
    WHERE
        (ISNULL(`t`.`NOCO`)
            OR ISNULL(`t`.`NOCAT`)) 
    UNION SELECT DISTINCT
        `sel`.`id_periode` AS `ID_PERIODE`,
        `sel`.`numero_contrat` AS `NUMERO_CONTRAT`,
        `sel`.`DATE_DEB_RATTACHEMENT` AS `DATE_DEB_RATTACHEMENT`,
        `sel`.`DATE_FIN_RATTACHEMENT` AS `DATE_FIN_RATTACHEMENT`,
        `sel`.`NOCAT` AS `NOCAT`,
        (CONCAT(CONVERT( CONCAT('La nature base [CONBCOT=',
                        (CASE
                            WHEN ISNULL(`t`.`CONBCOT`) THEN 'NULL'
                            ELSE `t`.`CONBCOT`
                        END),
                        ', TXCALCU_REMPLI=',
                        (CASE
                            WHEN (COALESCE(`t`.`TXCALCU`, 0) > 0) THEN 'O'
                            ELSE 'N'
                        END),
                        '] n\'existe pas. Situation tarifaire : numContrat=') USING LATIN1),
                `sel`.`numero_contrat`,
                ', numCategorie=',
                `sel`.`NOCAT`,
                ', date=',
                `sel`.`DATE_FIN_RATTACHEMENT`,
                '. Periode traitee : [PeriodeRecue(idPeriode=',
                `sel`.`id_periode`,
                ', typePeriode=',
                `sel`.`TYPE_PERIODE`,
                ', dateDebutPeriode=',
                `sel`.`DATE_DEBUT_PERIODE`,
                ', dateFinPeriode=',
                `sel`.`DATE_FIN_PERIODE`,
                ', dateCreation=',
                `sel`.`DATE_CREATION`,
                ')]') COLLATE latin1_general_ci) AS `trace`
    FROM
        ((`V_Selection_base_assujettie` `sel`
        JOIN `TARIFS` `t` ON (((`sel`.`numero_contrat` = `t`.`NOCO`)
            AND (`sel`.`NOCAT` = `t`.`NOCAT`)
            AND (`t`.`DT_DEBUT_SIT` <= `sel`.`DATE_FIN_RATTACHEMENT`)
            AND ((COALESCE(`t`.`DT_FIN_SIT`, 99999999) >= `sel`.`DATE_FIN_RATTACHEMENT`)
            OR ((CASE
            WHEN (`t`.`DT_FIN_SIT` = 0) THEN 99999999
            ELSE `t`.`DT_FIN_SIT`
        END) >= `sel`.`DATE_FIN_RATTACHEMENT`)))))
        LEFT JOIN `PARAM_NATURE_BASE` `p` ON (((`t`.`CONBCOT` = `p`.`CONBCOT`)
            AND (CONVERT( (CASE
            WHEN (COALESCE(`t`.`TXCALCU`, 0) > 0) THEN 'O'
            ELSE 'N'
        END) USING LATIN1) = (`p`.`TXCALCU_REMPLI` COLLATE latin1_general_ci)))))
    WHERE
        ISNULL(`p`.`NUM_TRANCHE`);