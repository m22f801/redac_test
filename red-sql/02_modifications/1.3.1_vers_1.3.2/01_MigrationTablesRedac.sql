-- Version du schéma REDAC actuel
INSERT INTO VERSION_BASE (VERSION, DESCRIPTION) VALUES ('1.3.2', 'Version supportant les flux 10, 1, 2, 3, 4, 5/6, 7/8 et 9');

-- Suppression de la table existante
DROP TABLE IF EXISTS PARAM_SIREN_FAUX;

-- Création de la table Paramétrage 16 ParamSirenFaux
CREATE TABLE PARAM_SIREN_FAUX (
SIREN CHAR(9) NOT NULL,
TMP_BATCH BOOLEAN NOT NULL DEFAULT FALSE,
USER_CREATION VARCHAR(20) NOT NULL,
DT_CREATION TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
USER_MISE_A_JOUR VARCHAR(20),
DT_MISE_A_JOUR TIMESTAMP NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB CHARACTER SET latin1 COLLATE latin1_general_ci;
ALTER TABLE PARAM_SIREN_FAUX ADD PRIMARY KEY(SIREN,TMP_BATCH);

