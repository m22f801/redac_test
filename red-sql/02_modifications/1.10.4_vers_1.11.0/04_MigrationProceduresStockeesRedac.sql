DROP PROCEDURE IF EXISTS Inserer_CATEGORIE_QUITTANCEMENT_INDIVIDU_SALAIRE_Pour_Periodes;

DELIMITER $$
CREATE PROCEDURE Inserer_CATEGORIE_QUITTANCEMENT_INDIVIDU_SALAIRE_Pour_Periodes(IN p_audit_nom_batch VARCHAR(20), IN p_liste_ids_periodes TEXT)
BEGIN
																	   
INSERT INTO CATEGORIE_QUITTANCEMENT_INDIVIDU (
ID_PERIODE
,NOCAT
,INDIVIDU
,LICAT
,CONBCOT
,MT_COTISATION
,ESTIMATION_COTISATION
,EFFECTIF_DEBUT
,EFFECTIF_FIN
,USER_CREATION
)
SELECT ID_PERIODE, NOCAT, INDIVIDU, LICAT, CONBCOT, sum(MONTANT_COTISATION), SUM(ESTIMATION_COTISATION) AS ESTIMATION_COTISATION, CHAMP_NULL_1, CHAMP_NULL_2, 'RR405' -- p_audit_nom_batch
FROM (

	SELECT 
        p.ID_PERIODE AS ID_PERIODE,
        t.NOCAT AS NOCAT,
        (CASE
            WHEN (i.IDENTIFIANT_REPERTOIRE = '') THEN i.NTT
            ELSE i.IDENTIFIANT_REPERTOIRE
        END) AS INDIVIDU,
        t.LICAT AS LICAT,
        t.CONBCOT AS CONBCOT,
		COALESCE(s.montant_cot, 0) AS MONTANT_COTISATION,
		SUM(tcba.ESTIMATION_COTISATION) AS ESTIMATION_COTISATION,
		NULL AS CHAMP_NULL_1,
        NULL AS CHAMP_NULL_2,
        p_audit_nom_batch
    FROM
        TARIFS t
            INNER JOIN
        PERIODES_RECUES p ON t.NOCO = p.NUMERO_CONTRAT
            INNER JOIN
        RATTACHEMENT_DECLARATIONS_RECUES r ON r.ID_PERIODE = p.ID_PERIODE
            INNER JOIN
        INDIVIDU i ON i.ID_ADH_ETAB_MOIS = r.ID_ADH_ETAB_MOIS
            AND i.TMP_BATCH IS FALSE
            INNER JOIN
        CONTRATS c ON c.NOCO = t.NOCO
            INNER JOIN
        PARAM_FAMILLE_CONTRAT par ON (par.NOFAM = c.NOFAM AND par.NMGRPGES = c.NMGRPGES)
            LEFT JOIN
        CONTRAT_TRAVAIL ct ON ct.ID_INDIVIDU = i.ID_INDIVIDU
            INNER JOIN
        AFFILIATION a ON  (`ct`.`ID_CONTRAT_TRAVAIL` = `a`.`ID_CONTRAT_TRAVAIL`)
        AND ISNULL(IF(`a`.`CODE_POPULATION` = '', NULL, a.CODE_POPULATION)) = 1
			LEFT JOIN
        BASE_ASSUJETTIE ba ON a.ID_AFFILIATION = ba.ID_AFFILIATION
			LEFT JOIN tranche_categorie_base_assujettie tcba ON (tcba.NOCAT = t.NOCAT
                    AND tcba.ID_PERIODE = p.ID_PERIODE
                    AND tcba.ID_BASE_ASSUJETTIE = ba.ID_BASE_ASSUJETTIE)
         LEFT JOIN
         	(SELECT 
                        SUM(ba.MONTANT_COTISATION) as montant_cot,  
								(CASE
            					WHEN (ind.IDENTIFIANT_REPERTOIRE = '') THEN ind.NTT
            					ELSE ind.IDENTIFIANT_REPERTOIRE
        						END) as id_indiv,
        						af.CODE_POPULATION,
        						rdr.id_periode
                    FROM
                        RATTACHEMENT_DECLARATIONS_RECUES rdr
									INNER JOIN 
								INDIVIDU ind ON rdr.ID_ADH_ETAB_MOIS = ind.ID_ADH_ETAB_MOIS
                            INNER JOIN
                        CONTRAT_TRAVAIL ct ON ind.ID_INDIVIDU = ct.ID_INDIVIDU
                            INNER JOIN
                        AFFILIATION af ON ct.ID_CONTRAT_TRAVAIL = af.ID_CONTRAT_TRAVAIL
                            INNER JOIN
                        BASE_ASSUJETTIE ba ON af.ID_AFFILIATION = ba.ID_AFFILIATION
                    WHERE
                    		FIND_IN_SET(rdr.ID_PERIODE, p_liste_ids_periodes) > 0
                     GROUP BY 
								(CASE
            					WHEN (ind.IDENTIFIANT_REPERTOIRE = '') THEN ind.NTT
            					ELSE ind.IDENTIFIANT_REPERTOIRE
        						END),
        						af.CODE_POPULATION, rdr.id_periode
				) as s on s.id_indiv = (CASE
            					WHEN (i.IDENTIFIANT_REPERTOIRE = '') THEN i.NTT
            					ELSE i.IDENTIFIANT_REPERTOIRE
        						END) and s.CODE_POPULATION = a.code_population and s.id_periode = p.id_periode
    WHERE
		  
        p.RECONSOLIDER = 'O' AND t.NATURE_CONTRAT = 2
            AND (t.TMP_BATCH IS FALSE)
            AND par.TYPE_CONSO_SALAIRE = 'INDIV'
            AND (par.TMP_BATCH IS FALSE)
            AND (c.TMP_BATCH IS FALSE)
            AND (c.DT_DEBUT_SIT <= p.DATE_DEBUT_PERIODE)
            AND (COALESCE(c.DT_FIN_SIT, 99999999) >= p.DATE_DEBUT_PERIODE)
            AND ((t.NOCO = p.NUMERO_CONTRAT)
            AND (t.DT_DEBUT_SIT <= p.DATE_DEBUT_PERIODE)
            AND (COALESCE(t.DT_FIN_SIT, 99999999) >= p.DATE_DEBUT_PERIODE)
            AND r.ID_ADH_ETAB_MOIS IN (SELECT 
                adh.ID_ADH_ETAB_MOIS
            FROM
                ADHESION_ETABLISSEMENT_MOIS adh
            WHERE
                adh.ID_ADH_ETAB_MOIS = r.ID_ADH_ETAB_MOIS
                    AND adh.TMP_BATCH IS FALSE))
					AND FIND_IN_SET(p.ID_PERIODE, p_liste_ids_periodes) > 0
    GROUP BY p.ID_PERIODE, INDIVIDU, COALESCE(
        IF( (a.CODE_POPULATION = ''),NULL,a.CODE_POPULATION),
            (SELECT t.NOCAT
             FROM tarifs t
             WHERE((t.NOCO = p.NUMERO_CONTRAT)
             AND (t.DT_DEBUT_SIT <= p.DATE_DEBUT_PERIODE)
             AND ((COALESCE(t.DT_FIN_SIT, 99999999) >= p.DATE_DEBUT_PERIODE)
             ))
             ORDER BY t.DT_DEBUT_SIT
             LIMIT 1))
	UNION ALL
	SELECT 
        p.ID_PERIODE AS ID_PERIODE,
        a.CODE_POPULATION AS NOCAT,
        (CASE
            WHEN (i.IDENTIFIANT_REPERTOIRE = '') THEN i.NTT
            ELSE i.IDENTIFIANT_REPERTOIRE
        END) AS INDIVIDU,
        t.LICAT AS LICAT,
        t.CONBCOT AS CONBCOT,
		COALESCE(s.montant_cot, 0) AS MONTANT_COTISATION,
		SUM(tcba.ESTIMATION_COTISATION) AS ESTIMATION_COTISATION,
        NULL AS CHAMP_NULL_1,
        NULL AS CHAMP_NULL_2,
        p_audit_nom_batch
    FROM
        TARIFS t
            INNER JOIN
        PERIODES_RECUES p ON t.NOCO = p.NUMERO_CONTRAT
            INNER JOIN
        RATTACHEMENT_DECLARATIONS_RECUES r ON r.ID_PERIODE = p.ID_PERIODE
            INNER JOIN
        INDIVIDU i ON i.ID_ADH_ETAB_MOIS = r.ID_ADH_ETAB_MOIS
            AND i.TMP_BATCH IS FALSE
            INNER JOIN
        CONTRATS c ON c.NOCO = t.NOCO
            INNER JOIN
        PARAM_FAMILLE_CONTRAT par ON (par.NOFAM = c.NOFAM AND par.NMGRPGES = c.NMGRPGES)
            LEFT JOIN
        CONTRAT_TRAVAIL ct ON ct.ID_INDIVIDU = i.ID_INDIVIDU
            INNER JOIN
        AFFILIATION a ON  ((`ct`.`ID_CONTRAT_TRAVAIL` = `a`.`ID_CONTRAT_TRAVAIL`)
                AND (`t`.`NOCAT` = `a`.`CODE_POPULATION`))
				AND ISNULL(IF(`a`.`CODE_POPULATION` = '', NULL, a.CODE_POPULATION)) = 0
			LEFT JOIN
        BASE_ASSUJETTIE ba ON a.ID_AFFILIATION = ba.ID_AFFILIATION
			LEFT JOIN tranche_categorie_base_assujettie tcba ON (tcba.NOCAT = t.NOCAT
                    AND tcba.ID_PERIODE = p.ID_PERIODE
                    AND tcba.ID_BASE_ASSUJETTIE = ba.ID_BASE_ASSUJETTIE)
         LEFT JOIN
         	(SELECT 
                        SUM(ba.MONTANT_COTISATION) as montant_cot,  
								(CASE
            					WHEN (ind.IDENTIFIANT_REPERTOIRE = '') THEN ind.NTT
            					ELSE ind.IDENTIFIANT_REPERTOIRE
        						END) as id_indiv,
        						af.CODE_POPULATION,
        						rdr.id_periode
                    FROM
                        RATTACHEMENT_DECLARATIONS_RECUES rdr
									INNER JOIN 
								INDIVIDU ind ON rdr.ID_ADH_ETAB_MOIS = ind.ID_ADH_ETAB_MOIS
                            INNER JOIN
                        CONTRAT_TRAVAIL ct ON ind.ID_INDIVIDU = ct.ID_INDIVIDU
                            INNER JOIN
                        AFFILIATION af ON ct.ID_CONTRAT_TRAVAIL = af.ID_CONTRAT_TRAVAIL
                            INNER JOIN
                        BASE_ASSUJETTIE ba ON af.ID_AFFILIATION = ba.ID_AFFILIATION
                    WHERE
                    		FIND_IN_SET(rdr.ID_PERIODE, p_liste_ids_periodes) > 0
                     GROUP BY 
								(CASE
            					WHEN (ind.IDENTIFIANT_REPERTOIRE = '') THEN ind.NTT
            					ELSE ind.IDENTIFIANT_REPERTOIRE
        						END),
        						af.CODE_POPULATION, rdr.id_periode
				) as s on s.id_indiv = (CASE
            					WHEN (i.IDENTIFIANT_REPERTOIRE = '') THEN i.NTT
            					ELSE i.IDENTIFIANT_REPERTOIRE
        						END) and s.CODE_POPULATION = a.code_population and s.id_periode = p.id_periode
    WHERE
        p.RECONSOLIDER = 'O' AND t.NATURE_CONTRAT = 2
            AND (t.TMP_BATCH IS FALSE)
            AND par.TYPE_CONSO_SALAIRE = 'INDIV'
            AND (par.TMP_BATCH IS FALSE)
            AND (c.TMP_BATCH IS FALSE)
            AND (c.DT_DEBUT_SIT <= p.DATE_DEBUT_PERIODE)
            AND (COALESCE(c.DT_FIN_SIT, 99999999) >= p.DATE_DEBUT_PERIODE)
            AND ((t.NOCO = p.NUMERO_CONTRAT)
            AND (t.DT_DEBUT_SIT <= p.DATE_DEBUT_PERIODE)
            AND (COALESCE(t.DT_FIN_SIT, 99999999) >= p.DATE_DEBUT_PERIODE)
            AND r.ID_ADH_ETAB_MOIS IN (SELECT 
                adh.ID_ADH_ETAB_MOIS
            FROM
                ADHESION_ETABLISSEMENT_MOIS adh
            WHERE
                adh.ID_ADH_ETAB_MOIS = r.ID_ADH_ETAB_MOIS
                    AND adh.TMP_BATCH IS FALSE))
			AND FIND_IN_SET(p.ID_PERIODE, p_liste_ids_periodes) > 0
    GROUP BY p.ID_PERIODE, INDIVIDU, COALESCE(
        IF( (a.CODE_POPULATION = ''),NULL,a.CODE_POPULATION),
            (SELECT t.NOCAT
             FROM tarifs t
             WHERE (t.NOCO = p.NUMERO_CONTRAT
             AND t.DT_DEBUT_SIT <= p.DATE_DEBUT_PERIODE
             AND COALESCE(t.DT_FIN_SIT, 99999999) >= p.DATE_DEBUT_PERIODE)
             ORDER BY t.DT_DEBUT_SIT
             LIMIT 1))
									

	) AS U GROUP BY ID_PERIODE, INDIVIDU, NOCAT    

    UNION SELECT 
        p.ID_PERIODE AS ID_PERIODE,
        t.NOCAT AS NOCAT,
        'TOUS' AS INDIVIDU,
        t.LICAT AS LICAT,
        t.CONBCOT AS CONBCOT,
        COALESCE(s.montant_cot, 0) AS MONTANT_COTISATION,
        (SELECT 
                SUM(tcba.ESTIMATION_COTISATION)
            FROM
                TRANCHE_CATEGORIE_BASE_ASSUJETTIE tcba
            WHERE
                ((tcba.ID_PERIODE = p.ID_PERIODE)
                    AND (tcba.NOCAT = t.NOCAT))) AS ESTIMATION_COTISATION,
        NULL AS CHAMP_NULL_1,
        NULL AS CHAMP_NULL_2,
		p_audit_nom_batch
    FROM
        (TARIFS t
        JOIN PERIODES_RECUES p ON ((t.NOCO = p.NUMERO_CONTRAT)))
            INNER JOIN
        CONTRATS c ON c.NOCO = t.NOCO
            INNER JOIN
        PARAM_FAMILLE_CONTRAT par ON (par.NOFAM = c.NOFAM AND par.NMGRPGES = c.NMGRPGES)
                 LEFT JOIN
         	(SELECT 
				SUM(ba.MONTANT_COTISATION) as montant_cot,  
						COALESCE(IF( (af.CODE_POPULATION = ''),NULL,af.CODE_POPULATION),
					   (SELECT t.NOCAT
						FROM tarifs t
						WHERE t.NOCO = per.NUMERO_CONTRAT
						AND t.DT_DEBUT_SIT <= per.DATE_DEBUT_PERIODE
						AND COALESCE(t.DT_FIN_SIT, 99999999) >= per.DATE_DEBUT_PERIODE
						ORDER BY t.DT_DEBUT_SIT
						LIMIT 1)) as CODE_POPULATION, rdr.id_periode
				FROM
					RATTACHEMENT_DECLARATIONS_RECUES rdr
					INNER JOIN 
						INDIVIDU ind ON rdr.ID_ADH_ETAB_MOIS = ind.ID_ADH_ETAB_MOIS
					INNER JOIN
						CONTRAT_TRAVAIL ct ON ind.ID_INDIVIDU = ct.ID_INDIVIDU
					INNER JOIN
						AFFILIATION af ON ct.ID_CONTRAT_TRAVAIL = af.ID_CONTRAT_TRAVAIL
					INNER JOIN
						BASE_ASSUJETTIE ba ON af.ID_AFFILIATION = ba.ID_AFFILIATION
					INNER JOIN
					periodes_recues per ON per.ID_PERIODE = rdr.ID_PERIODE
				WHERE
					FIND_IN_SET(rdr.ID_PERIODE, p_liste_ids_periodes) > 0
				GROUP BY COALESCE(IF( (af.CODE_POPULATION = ''),NULL,af.CODE_POPULATION),
									   (SELECT t.NOCAT
										FROM tarifs t
										WHERE t.NOCO = per.NUMERO_CONTRAT
										AND t.DT_DEBUT_SIT <= per.DATE_DEBUT_PERIODE
										AND COALESCE(t.DT_FIN_SIT, 99999999) >= per.DATE_DEBUT_PERIODE
										ORDER BY t.DT_DEBUT_SIT
										LIMIT 1)), rdr.id_periode
				)as s on 
						COALESCE(IF( (s.CODE_POPULATION = ''),NULL,s.CODE_POPULATION),
			            (SELECT t.NOCAT
			             FROM tarifs t
			             WHERE t.NOCO = p.NUMERO_CONTRAT
			             AND t.DT_DEBUT_SIT <= p.DATE_DEBUT_PERIODE
			             AND COALESCE(t.DT_FIN_SIT, 99999999) >= p.DATE_DEBUT_PERIODE
			             ORDER BY t.DT_DEBUT_SIT
			             LIMIT 1)) = t.NOCAT 
						AND s.id_periode = p.id_periode
        
    WHERE
        (t.NOCO = p.NUMERO_CONTRAT
            AND t.DT_DEBUT_SIT <= p.DATE_DEBUT_PERIODE
            AND COALESCE(t.DT_FIN_SIT, 99999999) >= p.DATE_DEBUT_PERIODE
            AND p.RECONSOLIDER = 'O'
            AND t.NATURE_CONTRAT = 2
            AND t.TMP_BATCH IS FALSE
            AND par.TYPE_CONSO_SALAIRE = 'TOUS'
            AND par.TMP_BATCH IS FALSE
            AND c.TMP_BATCH IS FALSE
            AND c.DT_DEBUT_SIT <= p.DATE_DEBUT_PERIODE
            AND COALESCE(c.DT_FIN_SIT, 99999999) >= p.DATE_DEBUT_PERIODE)
        AND FIND_IN_SET(p.ID_PERIODE, p_liste_ids_periodes) > 0
		  ;
END$$
DELIMITER ;