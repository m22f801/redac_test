DROP VIEW IF EXISTS V_Effectifs_mouvements;
CREATE VIEW V_Effectifs_mouvements AS 
SELECT 
        `dmd`.`ID_PERIODE` AS `ID_PERIODE`,
        `dmd`.`NOCAT` AS `NOCAT`,
        `dmd`.`DATE_MOUVEMENT` AS `DATE_MOUVEMENT`,
        SUM(`li`.`TOTAL_EFFECTIF`) AS `EFF_DEB`,
        0 AS `EFF_FIN`,
        'DEB' AS `TYPE_DATE`
    FROM
        (`v_liste_effectif_categorie_contrat_travail` `li`
        JOIN `v_dates_mouvement` `dmd` ON (((`dmd`.`ID_PERIODE` = `li`.`ID_PERIODE`)
            AND (`dmd`.`NOCAT` = `li`.`NOCAT`)
            AND (`dmd`.`DATE_MOUVEMENT` = `li`.`DEBUT_BASE`)
            AND (`dmd`.`DATE_MOUVEMENT` <> (SELECT 
                `p`.`DATE_DEBUT_PERIODE`
            FROM
                `periodes_recues` `p`
            WHERE
                (`p`.`ID_PERIODE` = `dmd`.`ID_PERIODE`))))))
    GROUP BY `dmd`.`ID_PERIODE` , `dmd`.`NOCAT` , `dmd`.`DATE_MOUVEMENT` 
    UNION SELECT 
        `dmf`.`ID_PERIODE` AS `ID_PERIODE`,
        `dmf`.`NOCAT` AS `NOCAT`,
        `dmf`.`DATE_MOUVEMENT` AS `DATE_MOUVEMENT`,
        0 AS `EFF_DEB`,
        SUM(`li`.`TOTAL_EFFECTIF`) AS `EFF_FIN`,
        'FIN' AS `TYPE_DATE`
    FROM
        (`v_liste_effectif_categorie_contrat_travail` `li`
        JOIN `v_dates_mouvement` `dmf` ON (((`dmf`.`ID_PERIODE` = `li`.`ID_PERIODE`)
            AND (`dmf`.`NOCAT` = `li`.`NOCAT`)
            AND (`dmf`.`DATE_MOUVEMENT` = `li`.`FIN_BASE`)
            AND (`dmf`.`DATE_MOUVEMENT` <> (SELECT 
                `p`.`DATE_FIN_PERIODE`
            FROM
                `periodes_recues` `p`
            WHERE
                (`p`.`ID_PERIODE` = `dmf`.`ID_PERIODE`))))))
    GROUP BY `dmf`.`ID_PERIODE` , `dmf`.`NOCAT` , `dmf`.`DATE_MOUVEMENT`;