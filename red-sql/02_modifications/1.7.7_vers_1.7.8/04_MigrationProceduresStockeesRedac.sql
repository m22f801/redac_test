-- RED-256 mise à de la PS pour être cohérent avec la vue V_Categorie_quittancement_salaires_a_inserer appelée par 
-- Inserer_CATEGORIE_QUITTANCEMENT_INDIVIDU_SALAIRE

DROP PROCEDURE IF EXISTS Inserer_CATEGORIE_QUITTANCEMENT_INDIVIDU_SALAIRE_Pour_Periodes;

DELIMITER $$
CREATE PROCEDURE Inserer_CATEGORIE_QUITTANCEMENT_INDIVIDU_SALAIRE_Pour_Periodes(IN p_audit_nom_batch VARCHAR(20), IN p_liste_ids_periodes TEXT)
BEGIN
INSERT INTO CATEGORIE_QUITTANCEMENT_INDIVIDU (
ID_PERIODE
,NOCAT
,INDIVIDU
,LICAT
,CONBCOT
,MT_COTISATION
,ESTIMATION_COTISATION
,EFFECTIF_DEBUT
,EFFECTIF_FIN
,USER_CREATION
)
SELECT 
        p.ID_PERIODE AS ID_PERIODE,
        t.NOCAT AS NOCAT,
        (CASE
            WHEN (i.IDENTIFIANT_REPERTOIRE = '') THEN i.NTT
            ELSE i.IDENTIFIANT_REPERTOIRE
        END) AS INDIVIDU,
        t.LICAT AS LICAT,
        t.CONBCOT AS CONBCOT,
        COALESCE((SELECT 
                        SUM(ba.MONTANT_COTISATION)
                    FROM
                        INDIVIDU ind
                            INNER JOIN
                        CONTRAT_TRAVAIL ct ON ind.ID_INDIVIDU = ct.ID_INDIVIDU
                            INNER JOIN
                        AFFILIATION a ON ct.ID_CONTRAT_TRAVAIL = a.ID_CONTRAT_TRAVAIL
                            INNER JOIN
                        BASE_ASSUJETTIE ba ON a.ID_AFFILIATION = ba.ID_AFFILIATION
                    WHERE
                        (CASE
                            WHEN i.IDENTIFIANT_REPERTOIRE <> '' THEN ind.IDENTIFIANT_REPERTOIRE = i.IDENTIFIANT_REPERTOIRE
                            ELSE ind.NTT = i.NTT
                        END)
                        
                        AND ba.ID_BASE_ASSUJETTIE IN (SELECT 
                                tcba.ID_BASE_ASSUJETTIE
                            FROM
                                TRANCHE_CATEGORIE_BASE_ASSUJETTIE tcba
                            WHERE
                                tcba.NOCAT = t.NOCAT
                                    AND tcba.ID_PERIODE = p.ID_PERIODE)), 0) AS MONTANT_COTISATION,
        (SELECT 
                SUM(tcba.ESTIMATION_COTISATION)
            FROM
                TRANCHE_CATEGORIE_BASE_ASSUJETTIE tcba
            WHERE
                tcba.NOCAT = t.NOCAT
                    AND tcba.ID_PERIODE = p.ID_PERIODE
                    AND tcba.ID_BASE_ASSUJETTIE = ba.ID_BASE_ASSUJETTIE) AS ESTIMATION_COTISATION,
        NULL AS CHAMP_NULL_1,
        NULL AS CHAMP_NULL_2,
        p_audit_nom_batch
    FROM
        TARIFS t
            INNER JOIN
        PERIODES_RECUES p ON t.NOCO = p.NUMERO_CONTRAT
            INNER JOIN
        RATTACHEMENT_DECLARATIONS_RECUES r ON r.ID_PERIODE = p.ID_PERIODE
            INNER JOIN
        INDIVIDU i ON i.ID_ADH_ETAB_MOIS = r.ID_ADH_ETAB_MOIS
            AND i.TMP_BATCH IS FALSE
            INNER JOIN
        CONTRATS c ON c.NOCO = t.NOCO
            INNER JOIN
        PARAM_FAMILLE_CONTRAT par ON par.NOFAM = c.NOFAM
            INNER JOIN
        CONTRAT_TRAVAIL ct ON ct.ID_INDIVIDU = i.ID_INDIVIDU
            INNER JOIN
        AFFILIATION a ON ct.ID_CONTRAT_TRAVAIL = a.ID_CONTRAT_TRAVAIL
            INNER JOIN
        BASE_ASSUJETTIE ba ON a.ID_AFFILIATION = ba.ID_AFFILIATION
        	LEFT JOIN tranche_categorie_base_assujettie tcba ON (tcba.NOCAT = t.NOCAT
                    AND tcba.ID_PERIODE = p.ID_PERIODE
                    AND tcba.ID_BASE_ASSUJETTIE = ba.ID_BASE_ASSUJETTIE)
    WHERE
        p.RECONSOLIDER = 'O' AND t.NATURE_CONTRAT = 2
            AND (t.TMP_BATCH IS FALSE)
            AND par.TYPE_CONSO_SALAIRE = 'INDIV'
            AND (par.TMP_BATCH IS FALSE)
            AND (c.TMP_BATCH IS FALSE)
            AND (c.DT_DEBUT_SIT <= p.DATE_DEBUT_PERIODE)
            AND ((COALESCE(c.DT_FIN_SIT, 99999999) >= p.DATE_DEBUT_PERIODE)
            OR ((CASE
            WHEN (c.DT_FIN_SIT = 0) THEN 99999999
            ELSE c.DT_FIN_SIT
        END) >= p.DATE_DEBUT_PERIODE))
            AND ((t.NOCO = p.NUMERO_CONTRAT)
            AND (t.DT_DEBUT_SIT <= p.DATE_DEBUT_PERIODE)
            AND ((COALESCE(t.DT_FIN_SIT, 99999999) >= p.DATE_DEBUT_PERIODE)
            OR ((CASE
            WHEN (t.DT_FIN_SIT = 0) THEN 99999999
            ELSE t.DT_FIN_SIT
        END) >= p.DATE_DEBUT_PERIODE))
            AND r.ID_ADH_ETAB_MOIS IN (SELECT 
                adh.ID_ADH_ETAB_MOIS
            FROM
                ADHESION_ETABLISSEMENT_MOIS adh
            WHERE
                adh.ID_ADH_ETAB_MOIS = r.ID_ADH_ETAB_MOIS
                    AND adh.TMP_BATCH IS FALSE))
			AND FIND_IN_SET(p.ID_PERIODE, p_liste_ids_periodes) > 0
    GROUP BY ID_PERIODE , NOCAT , LICAT , INDIVIDU , CONBCOT 
    UNION SELECT 
        p.ID_PERIODE AS ID_PERIODE,
        t.NOCAT AS NOCAT,
        'TOUS' AS INDIVIDU,
        t.LICAT AS LICAT,
        t.CONBCOT AS CONBCOT,
        COALESCE((SELECT 
                        SUM(ba.MONTANT_COTISATION)
                    FROM
                        BASE_ASSUJETTIE ba
                    WHERE
                        ba.ID_BASE_ASSUJETTIE IN (SELECT 
                                tcba.ID_BASE_ASSUJETTIE
                            FROM
                                TRANCHE_CATEGORIE_BASE_ASSUJETTIE tcba
                            WHERE
                                ((tcba.ID_PERIODE = p.ID_PERIODE)
                                    AND (tcba.NOCAT = t.NOCAT)))),
                0) AS MONTANT_COTISATION,
        (SELECT 
                SUM(tcba.ESTIMATION_COTISATION)
            FROM
                TRANCHE_CATEGORIE_BASE_ASSUJETTIE tcba
            WHERE
                ((tcba.ID_PERIODE = p.ID_PERIODE)
                    AND (tcba.NOCAT = t.NOCAT))) AS ESTIMATION_COTISATION,
        NULL AS CHAMP_NULL_1,
        NULL AS CHAMP_NULL_2,
        p_audit_nom_batch
    FROM
        (TARIFS t
        JOIN PERIODES_RECUES p ON ((t.NOCO = p.NUMERO_CONTRAT)))
            INNER JOIN
        CONTRATS c ON c.NOCO = t.NOCO
            INNER JOIN
        PARAM_FAMILLE_CONTRAT par ON (par.NOFAM = c.NOFAM)
    WHERE
        (t.NOCO = p.NUMERO_CONTRAT
            AND t.DT_DEBUT_SIT <= p.DATE_DEBUT_PERIODE
            AND (COALESCE(t.DT_FIN_SIT, 99999999) >= p.DATE_DEBUT_PERIODE
            OR ((CASE
            WHEN (t.DT_FIN_SIT = 0) THEN 99999999
            ELSE t.DT_FIN_SIT
        END) >= p.DATE_DEBUT_PERIODE))
            AND p.RECONSOLIDER = 'O'
            AND t.NATURE_CONTRAT = 2
            AND (t.TMP_BATCH IS FALSE)
            AND par.TYPE_CONSO_SALAIRE = 'TOUS'
            AND (par.TMP_BATCH IS FALSE)
            AND (c.TMP_BATCH IS FALSE)
            AND (c.DT_DEBUT_SIT <= p.DATE_DEBUT_PERIODE)
            AND ((COALESCE(c.DT_FIN_SIT, 99999999) >= p.DATE_DEBUT_PERIODE)
            OR ((CASE
            WHEN (c.DT_FIN_SIT = 0) THEN 99999999
            ELSE c.DT_FIN_SIT
        END) >= p.DATE_DEBUT_PERIODE)))
        AND FIND_IN_SET(p.ID_PERIODE, p_liste_ids_periodes) > 0;

END$$
DELIMITER ;

