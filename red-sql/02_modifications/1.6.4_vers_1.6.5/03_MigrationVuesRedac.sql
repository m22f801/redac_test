-- RED-88 / RED-89
DROP VIEW IF EXISTS V_Trace_Nocat_Inconnu_salaires;
CREATE VIEW V_Trace_Nocat_Inconnu_salaires AS 
SELECT DISTINCT
    vpms.ID_PERIODE,CONCAT('La situation tarifaire [numContrat=',
                vpms.NUMERO_CONTRAT,
                ', numCategorieQuittancement=',
                a.CODE_POPULATION,
                ', dateDebutPeriode=',
                vpms.DATE_DEBUT_PERIODE,
                '] n\'existe pas. Periode traitee : [PeriodeRecue(idPeriode=',
                vpms.ID_PERIODE,
                ', typePeriode=',
                vpms.TYPE_PERIODE,
                ', dateDebutPeriode=',
                vpms.DATE_DEBUT_PERIODE,
                ', dateFinPeriode=',
                vpms.DATE_FIN_PERIODE,
                ', dateCreation=',
                vpms.DATE_CREATION,
                ')]') AS MESSAGE
FROM
	V_Selection_periode_mocalcot_salaires vpms,
    AFFILIATION a
        LEFT JOIN
    CONTRAT_TRAVAIL ct ON a.ID_CONTRAT_TRAVAIL = ct.ID_CONTRAT_TRAVAIL
        LEFT JOIN
    INDIVIDU i ON ct.ID_INDIVIDU = i.ID_INDIVIDU
        LEFT JOIN
    ADHESION_ETABLISSEMENT_MOIS adh ON i.ID_ADH_ETAB_MOIS = adh.ID_ADH_ETAB_MOIS
WHERE
    adh.TMP_BATCH IS FALSE
		AND a.CODE_POPULATION <> ''
        AND adh.ID_ADH_ETAB_MOIS IN (SELECT 
            r.ID_ADH_ETAB_MOIS
        FROM
            RATTACHEMENT_DECLARATIONS_RECUES r
        WHERE
            r.ID_PERIODE = vpms.ID_PERIODE)
        AND a.CODE_POPULATION NOT IN (SELECT DISTINCT
            t.NOCAT
        FROM
            TARIFS t
        WHERE
            (t.NOCO = vpms.NUMERO_CONTRAT)
            AND (t.NOCAT <> '')
                AND (t.DT_DEBUT_SIT <= vpms.DATE_DEBUT_PERIODE)
                AND ((COALESCE(t.DT_FIN_SIT, 99999999) >= vpms.DATE_DEBUT_PERIODE)
                OR ((CASE
                WHEN (t.DT_FIN_SIT = 0) THEN 99999999
                ELSE t.DT_FIN_SIT
            END) >= vpms.DATE_DEBUT_PERIODE)));