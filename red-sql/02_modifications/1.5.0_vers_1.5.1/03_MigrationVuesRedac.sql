
-- Ajout View TM 2191 - Export individus
DROP VIEW IF EXISTS V_Messages_Controle_IndividuPeriode;
CREATE VIEW V_Messages_Controle_IndividuPeriode AS SELECT ID_PERIODE,INDIVIDU,CAST(group_concat(case m.NIVEAU_ALERTE when 'SIGNAL' then m.MESSAGE_UTILISATEUR end SEPARATOR ',\n' ) AS CHAR) AS MSG_INDIV_SIGNAL, CAST(group_concat(case m.NIVEAU_ALERTE when 'REJET' then m.MESSAGE_UTILISATEUR end SEPARATOR ',\n' ) AS CHAR) AS MSG_INDIV_REJET FROM MESSAGE_CONTROLE m GROUP BY ID_PERIODE,INDIVIDU;

-- suppression de la view TM 2191 ( pb full scan )
DROP VIEW IF EXISTS V_ctr_aggr_export_individu;