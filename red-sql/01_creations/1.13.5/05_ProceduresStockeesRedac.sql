-- Création des procédures stockées REDAC
DROP PROCEDURE IF EXISTS Inserer_CATEGORIE_QUITTANCEMENT_INDIVIDU_EFFECTIF;

DELIMITER $$
CREATE PROCEDURE Inserer_CATEGORIE_QUITTANCEMENT_INDIVIDU_EFFECTIF(IN p_audit_nom_batch VARCHAR(20))
BEGIN
-- Calcul et insersion des nouveaux éléments des périodes à traiter
INSERT INTO CATEGORIE_QUITTANCEMENT_INDIVIDU
(
ID_PERIODE
,NOCAT
,INDIVIDU
,LICAT
,CONBCOT
,MT_COTISATION
,EFFECTIF_DEBUT
,EFFECTIF_FIN
,ESTIMATION_COTISATION
,USER_CREATION
)
SELECT 
ID_PERIODE
,NOCAT
,INDIVIDU
,LICAT
,CONBCOT
,COTISATION
,EFFECTIF_DEBUT
,EFFECTIF_FIN
,ESTIMATION_COTISATION
,p_audit_nom_batch
FROM V_Selection_categorie_quittancement_effectif;
END$$
DELIMITER ;

DROP PROCEDURE IF EXISTS Inserer_CATEGORIE_QUITTANCEMENT_INDIVIDU_SALAIRE_Pour_Periodes;

DELIMITER $$
CREATE PROCEDURE Inserer_CATEGORIE_QUITTANCEMENT_INDIVIDU_SALAIRE_Pour_Periodes(IN p_audit_nom_batch VARCHAR(20), IN p_liste_ids_periodes MEDIUMTEXT)
BEGIN
																	   
INSERT INTO CATEGORIE_QUITTANCEMENT_INDIVIDU (
ID_PERIODE
,NOCAT
,INDIVIDU
,LICAT
,CONBCOT
,MT_COTISATION
,ESTIMATION_COTISATION
,EFFECTIF_DEBUT
,EFFECTIF_FIN
,USER_CREATION
)
SELECT ID_PERIODE, NOCAT, INDIVIDU, LICAT, CONBCOT, sum(MONTANT_COTISATION), SUM(ESTIMATION_COTISATION) AS ESTIMATION_COTISATION, CHAMP_NULL_1, CHAMP_NULL_2, 'RR405' -- p_audit_nom_batch
FROM (

	SELECT 
        p.ID_PERIODE AS ID_PERIODE,
        t.NOCAT AS NOCAT,
        (CASE
            WHEN (i.IDENTIFIANT_REPERTOIRE = '') THEN i.NTT
            ELSE i.IDENTIFIANT_REPERTOIRE
        END) AS INDIVIDU,
        t.LICAT AS LICAT,
        t.CONBCOT AS CONBCOT,
		COALESCE(s.montant_cot, 0) AS MONTANT_COTISATION,
		SUM(tcba.ESTIMATION_COTISATION) AS ESTIMATION_COTISATION,
		NULL AS CHAMP_NULL_1,
        NULL AS CHAMP_NULL_2,
        p_audit_nom_batch
    FROM
        TARIFS t
            INNER JOIN
        PERIODES_RECUES p ON t.NOCO = p.NUMERO_CONTRAT
            INNER JOIN
        RATTACHEMENT_DECLARATIONS_RECUES r ON r.ID_PERIODE = p.ID_PERIODE
            INNER JOIN
        INDIVIDU i ON i.ID_ADH_ETAB_MOIS = r.ID_ADH_ETAB_MOIS
            AND i.TMP_BATCH IS FALSE
            INNER JOIN
        CONTRATS c ON c.NOCO = t.NOCO
            INNER JOIN
        PARAM_FAMILLE_CONTRAT par ON (par.NOFAM = c.NOFAM AND par.NMGRPGES = c.NMGRPGES)
            LEFT JOIN
        CONTRAT_TRAVAIL ct ON ct.ID_INDIVIDU = i.ID_INDIVIDU
            INNER JOIN
        AFFILIATION a ON  (`ct`.`ID_CONTRAT_TRAVAIL` = `a`.`ID_CONTRAT_TRAVAIL`)
        AND ISNULL(IF(`a`.`CODE_POPULATION` = '', NULL, a.CODE_POPULATION)) = 1
			LEFT JOIN
        BASE_ASSUJETTIE ba ON a.ID_AFFILIATION = ba.ID_AFFILIATION
			LEFT JOIN tranche_categorie_base_assujettie tcba ON (tcba.NOCAT = t.NOCAT
                    AND tcba.ID_PERIODE = p.ID_PERIODE
                    AND tcba.ID_BASE_ASSUJETTIE = ba.ID_BASE_ASSUJETTIE)
         LEFT JOIN
         	(SELECT 
                        SUM(ba.MONTANT_COTISATION) as montant_cot,  
								(CASE
            					WHEN (ind.IDENTIFIANT_REPERTOIRE = '') THEN ind.NTT
            					ELSE ind.IDENTIFIANT_REPERTOIRE
        						END) as id_indiv,
        						af.CODE_POPULATION,
        						rdr.id_periode
                    FROM
                        RATTACHEMENT_DECLARATIONS_RECUES rdr
									INNER JOIN 
								INDIVIDU ind ON rdr.ID_ADH_ETAB_MOIS = ind.ID_ADH_ETAB_MOIS
                            INNER JOIN
                        CONTRAT_TRAVAIL ct ON ind.ID_INDIVIDU = ct.ID_INDIVIDU
                            INNER JOIN
                        AFFILIATION af ON ct.ID_CONTRAT_TRAVAIL = af.ID_CONTRAT_TRAVAIL
                            INNER JOIN
                        BASE_ASSUJETTIE ba ON af.ID_AFFILIATION = ba.ID_AFFILIATION
                    WHERE
                    		FIND_IN_SET(rdr.ID_PERIODE, p_liste_ids_periodes) > 0
                     GROUP BY 
								(CASE
            					WHEN (ind.IDENTIFIANT_REPERTOIRE = '') THEN ind.NTT
            					ELSE ind.IDENTIFIANT_REPERTOIRE
        						END),
        						af.CODE_POPULATION, rdr.id_periode
				) as s on s.id_indiv = (CASE
            					WHEN (i.IDENTIFIANT_REPERTOIRE = '') THEN i.NTT
            					ELSE i.IDENTIFIANT_REPERTOIRE
        						END) and s.CODE_POPULATION = a.code_population and s.id_periode = p.id_periode
    WHERE
		  
        p.RECONSOLIDER = 'O' AND t.NATURE_CONTRAT = 2
            AND (t.TMP_BATCH IS FALSE)
            AND par.TYPE_CONSO_SALAIRE = 'INDIV'
            AND (par.TMP_BATCH IS FALSE)
            AND (c.TMP_BATCH IS FALSE)
            AND (c.DT_DEBUT_SIT <= p.DATE_DEBUT_PERIODE)
            AND (COALESCE(c.DT_FIN_SIT, 99999999) >= p.DATE_DEBUT_PERIODE)
            AND ((t.NOCO = p.NUMERO_CONTRAT)
            AND (t.DT_DEBUT_SIT <= p.DATE_DEBUT_PERIODE)
            AND (COALESCE(t.DT_FIN_SIT, 99999999) >= p.DATE_DEBUT_PERIODE)
            AND r.ID_ADH_ETAB_MOIS IN (SELECT 
                adh.ID_ADH_ETAB_MOIS
            FROM
                ADHESION_ETABLISSEMENT_MOIS adh
            WHERE
                adh.ID_ADH_ETAB_MOIS = r.ID_ADH_ETAB_MOIS
                    AND adh.TMP_BATCH IS FALSE))
					AND FIND_IN_SET(p.ID_PERIODE, p_liste_ids_periodes) > 0
    GROUP BY p.ID_PERIODE, INDIVIDU, COALESCE(
        IF( (a.CODE_POPULATION = ''),NULL,a.CODE_POPULATION),
            (SELECT t.NOCAT
             FROM tarifs t
             WHERE((t.NOCO = p.NUMERO_CONTRAT)
             AND (t.DT_DEBUT_SIT <= p.DATE_DEBUT_PERIODE)
             AND ((COALESCE(t.DT_FIN_SIT, 99999999) >= p.DATE_DEBUT_PERIODE)
             ))
             ORDER BY t.DT_DEBUT_SIT
             LIMIT 1))
	UNION ALL
	SELECT 
        p.ID_PERIODE AS ID_PERIODE,
        a.CODE_POPULATION AS NOCAT,
        (CASE
            WHEN (i.IDENTIFIANT_REPERTOIRE = '') THEN i.NTT
            ELSE i.IDENTIFIANT_REPERTOIRE
        END) AS INDIVIDU,
        t.LICAT AS LICAT,
        t.CONBCOT AS CONBCOT,
		COALESCE(s.montant_cot, 0) AS MONTANT_COTISATION,
		SUM(tcba.ESTIMATION_COTISATION) AS ESTIMATION_COTISATION,
        NULL AS CHAMP_NULL_1,
        NULL AS CHAMP_NULL_2,
        p_audit_nom_batch
    FROM
        TARIFS t
            INNER JOIN
        PERIODES_RECUES p ON t.NOCO = p.NUMERO_CONTRAT
            INNER JOIN
        RATTACHEMENT_DECLARATIONS_RECUES r ON r.ID_PERIODE = p.ID_PERIODE
            INNER JOIN
        INDIVIDU i ON i.ID_ADH_ETAB_MOIS = r.ID_ADH_ETAB_MOIS
            AND i.TMP_BATCH IS FALSE
            INNER JOIN
        CONTRATS c ON c.NOCO = t.NOCO
            INNER JOIN
        PARAM_FAMILLE_CONTRAT par ON (par.NOFAM = c.NOFAM AND par.NMGRPGES = c.NMGRPGES)
            LEFT JOIN
        CONTRAT_TRAVAIL ct ON ct.ID_INDIVIDU = i.ID_INDIVIDU
            INNER JOIN
        AFFILIATION a ON  ((`ct`.`ID_CONTRAT_TRAVAIL` = `a`.`ID_CONTRAT_TRAVAIL`)
                AND (`t`.`NOCAT` = `a`.`CODE_POPULATION`))
				AND ISNULL(IF(`a`.`CODE_POPULATION` = '', NULL, a.CODE_POPULATION)) = 0
			LEFT JOIN
        BASE_ASSUJETTIE ba ON a.ID_AFFILIATION = ba.ID_AFFILIATION
			LEFT JOIN tranche_categorie_base_assujettie tcba ON (tcba.NOCAT = t.NOCAT
                    AND tcba.ID_PERIODE = p.ID_PERIODE
                    AND tcba.ID_BASE_ASSUJETTIE = ba.ID_BASE_ASSUJETTIE)
         LEFT JOIN
         	(SELECT 
                        SUM(ba.MONTANT_COTISATION) as montant_cot,  
								(CASE
            					WHEN (ind.IDENTIFIANT_REPERTOIRE = '') THEN ind.NTT
            					ELSE ind.IDENTIFIANT_REPERTOIRE
        						END) as id_indiv,
        						af.CODE_POPULATION,
        						rdr.id_periode
                    FROM
                        RATTACHEMENT_DECLARATIONS_RECUES rdr
									INNER JOIN 
								INDIVIDU ind ON rdr.ID_ADH_ETAB_MOIS = ind.ID_ADH_ETAB_MOIS
                            INNER JOIN
                        CONTRAT_TRAVAIL ct ON ind.ID_INDIVIDU = ct.ID_INDIVIDU
                            INNER JOIN
                        AFFILIATION af ON ct.ID_CONTRAT_TRAVAIL = af.ID_CONTRAT_TRAVAIL
                            INNER JOIN
                        BASE_ASSUJETTIE ba ON af.ID_AFFILIATION = ba.ID_AFFILIATION
                    WHERE
                    		FIND_IN_SET(rdr.ID_PERIODE, p_liste_ids_periodes) > 0
                     GROUP BY 
								(CASE
            					WHEN (ind.IDENTIFIANT_REPERTOIRE = '') THEN ind.NTT
            					ELSE ind.IDENTIFIANT_REPERTOIRE
        						END),
        						af.CODE_POPULATION, rdr.id_periode
				) as s on s.id_indiv = (CASE
            					WHEN (i.IDENTIFIANT_REPERTOIRE = '') THEN i.NTT
            					ELSE i.IDENTIFIANT_REPERTOIRE
        						END) and s.CODE_POPULATION = a.code_population and s.id_periode = p.id_periode
    WHERE
        p.RECONSOLIDER = 'O' AND t.NATURE_CONTRAT = 2
            AND (t.TMP_BATCH IS FALSE)
            AND par.TYPE_CONSO_SALAIRE = 'INDIV'
            AND (par.TMP_BATCH IS FALSE)
            AND (c.TMP_BATCH IS FALSE)
            AND (c.DT_DEBUT_SIT <= p.DATE_DEBUT_PERIODE)
            AND (COALESCE(c.DT_FIN_SIT, 99999999) >= p.DATE_DEBUT_PERIODE)
            AND ((t.NOCO = p.NUMERO_CONTRAT)
            AND (t.DT_DEBUT_SIT <= p.DATE_DEBUT_PERIODE)
            AND (COALESCE(t.DT_FIN_SIT, 99999999) >= p.DATE_DEBUT_PERIODE)
            AND r.ID_ADH_ETAB_MOIS IN (SELECT 
                adh.ID_ADH_ETAB_MOIS
            FROM
                ADHESION_ETABLISSEMENT_MOIS adh
            WHERE
                adh.ID_ADH_ETAB_MOIS = r.ID_ADH_ETAB_MOIS
                    AND adh.TMP_BATCH IS FALSE))
			AND FIND_IN_SET(p.ID_PERIODE, p_liste_ids_periodes) > 0
    GROUP BY p.ID_PERIODE, INDIVIDU, COALESCE(
        IF( (a.CODE_POPULATION = ''),NULL,a.CODE_POPULATION),
            (SELECT t.NOCAT
             FROM tarifs t
             WHERE (t.NOCO = p.NUMERO_CONTRAT
             AND t.DT_DEBUT_SIT <= p.DATE_DEBUT_PERIODE
             AND COALESCE(t.DT_FIN_SIT, 99999999) >= p.DATE_DEBUT_PERIODE)
             ORDER BY t.DT_DEBUT_SIT
             LIMIT 1))
									

	) AS U GROUP BY ID_PERIODE, INDIVIDU, NOCAT    

    UNION SELECT 
        p.ID_PERIODE AS ID_PERIODE,
        t.NOCAT AS NOCAT,
        'TOUS' AS INDIVIDU,
        t.LICAT AS LICAT,
        t.CONBCOT AS CONBCOT,
        COALESCE(s.montant_cot, 0) AS MONTANT_COTISATION,
        (SELECT 
                SUM(tcba.ESTIMATION_COTISATION)
            FROM
                TRANCHE_CATEGORIE_BASE_ASSUJETTIE tcba
            WHERE
                ((tcba.ID_PERIODE = p.ID_PERIODE)
                    AND (tcba.NOCAT = t.NOCAT))) AS ESTIMATION_COTISATION,
        NULL AS CHAMP_NULL_1,
        NULL AS CHAMP_NULL_2,
		p_audit_nom_batch
    FROM
        (TARIFS t
        JOIN PERIODES_RECUES p ON ((t.NOCO = p.NUMERO_CONTRAT)))
            INNER JOIN
        CONTRATS c ON c.NOCO = t.NOCO
            INNER JOIN
        PARAM_FAMILLE_CONTRAT par ON (par.NOFAM = c.NOFAM AND par.NMGRPGES = c.NMGRPGES)
                 LEFT JOIN
         	(SELECT 
				SUM(ba.MONTANT_COTISATION) as montant_cot,  
						COALESCE(IF( (af.CODE_POPULATION = ''),NULL,af.CODE_POPULATION),
					   (SELECT t.NOCAT
						FROM tarifs t
						WHERE t.NOCO = per.NUMERO_CONTRAT
						AND t.DT_DEBUT_SIT <= per.DATE_DEBUT_PERIODE
						AND COALESCE(t.DT_FIN_SIT, 99999999) >= per.DATE_DEBUT_PERIODE
						ORDER BY t.DT_DEBUT_SIT
						LIMIT 1)) as CODE_POPULATION, rdr.id_periode
				FROM
					RATTACHEMENT_DECLARATIONS_RECUES rdr
					INNER JOIN 
						INDIVIDU ind ON rdr.ID_ADH_ETAB_MOIS = ind.ID_ADH_ETAB_MOIS
					INNER JOIN
						CONTRAT_TRAVAIL ct ON ind.ID_INDIVIDU = ct.ID_INDIVIDU
					INNER JOIN
						AFFILIATION af ON ct.ID_CONTRAT_TRAVAIL = af.ID_CONTRAT_TRAVAIL
					INNER JOIN
						BASE_ASSUJETTIE ba ON af.ID_AFFILIATION = ba.ID_AFFILIATION
					INNER JOIN
					periodes_recues per ON per.ID_PERIODE = rdr.ID_PERIODE
				WHERE
					FIND_IN_SET(rdr.ID_PERIODE, p_liste_ids_periodes) > 0
				GROUP BY COALESCE(IF( (af.CODE_POPULATION = ''),NULL,af.CODE_POPULATION),
									   (SELECT t.NOCAT
										FROM tarifs t
										WHERE t.NOCO = per.NUMERO_CONTRAT
										AND t.DT_DEBUT_SIT <= per.DATE_DEBUT_PERIODE
										AND COALESCE(t.DT_FIN_SIT, 99999999) >= per.DATE_DEBUT_PERIODE
										ORDER BY t.DT_DEBUT_SIT
										LIMIT 1)), rdr.id_periode
				) as s on 
						COALESCE(IF( (s.CODE_POPULATION = ''),NULL,s.CODE_POPULATION),
			            (SELECT t.NOCAT
			             FROM tarifs t
			             WHERE t.NOCO = p.NUMERO_CONTRAT
			             AND t.DT_DEBUT_SIT <= p.DATE_DEBUT_PERIODE
			             AND COALESCE(t.DT_FIN_SIT, 99999999) >= p.DATE_DEBUT_PERIODE
			             ORDER BY t.DT_DEBUT_SIT
			             LIMIT 1)) = t.NOCAT 
				 and s.id_periode = p.id_periode
        
    WHERE
        (t.NOCO = p.NUMERO_CONTRAT
            AND t.DT_DEBUT_SIT <= p.DATE_DEBUT_PERIODE
            AND COALESCE(t.DT_FIN_SIT, 99999999) >= p.DATE_DEBUT_PERIODE
            AND p.RECONSOLIDER = 'O'
            AND t.NATURE_CONTRAT = 2
            AND t.TMP_BATCH IS FALSE
            AND par.TYPE_CONSO_SALAIRE = 'TOUS'
            AND par.TMP_BATCH IS FALSE
            AND c.TMP_BATCH IS FALSE
            AND c.DT_DEBUT_SIT <= p.DATE_DEBUT_PERIODE
            AND COALESCE(c.DT_FIN_SIT, 99999999) >= p.DATE_DEBUT_PERIODE)
        AND FIND_IN_SET(p.ID_PERIODE, p_liste_ids_periodes) > 0
		  ;
END$$
DELIMITER ;

DROP PROCEDURE IF EXISTS Inserer_EFFECTIF_CATEGORIE_MVT;

DELIMITER $$
CREATE PROCEDURE Inserer_EFFECTIF_CATEGORIE_MVT(IN p_audit_nom_batch VARCHAR(20))
BEGIN
-- Calcul et insersion des nouveaux éléments des périodes à traiter
INSERT INTO EFFECTIF_CATEGORIE_MVT (ID_PERIODE, NOCAT, DATE_EFFET_MVT, NB_MOUVEMENT, USER_CREATION)
SELECT ID_PERIODE
, NOCAT
, DATE_CONSERVEE
, NB_MVT_FINAL
, p_audit_nom_batch
FROM V_Effectif_categorie_mouvement_data
WHERE concat(ID_PERIODE, NOCAT, DATE_CONSERVEE) NOT IN (SELECT concat(ID_PERIODE, NOCAT, DATE_SUPPR) FROM V_Effectif_categorie_mouvement_suppression1)
AND concat(ID_PERIODE, NOCAT, DATE_CONSERVEE, NB_MVT_FINAL) NOT IN (SELECT concat(ID_PERIODE, NOCAT, DATE_SUPPR, NB_MVT_INITIAL) FROM V_Effectif_categorie_mouvement_suppression2)
ORDER BY ID_PERIODE, NOCAT, DATE_CONSERVEE;

END$$
DELIMITER ;

DROP PROCEDURE IF EXISTS Inserer_TRANCHE_CATEGORIE_BASE_ASSUJETTIE_Pour_Periodes;

DELIMITER $$
CREATE PROCEDURE Inserer_TRANCHE_CATEGORIE_BASE_ASSUJETTIE_Pour_Periodes(IN p_audit_nom_batch VARCHAR(20), IN p_liste_ids_periodes MEDIUMTEXT)
BEGIN
INSERT INTO TRANCHE_CATEGORIE_BASE_ASSUJETTIE 
(
ID_PERIODE
,ID_BASE_ASSUJETTIE
,MOIS_RATTACHEMENT
,ID_AFFILIATION
,DATE_DEB_RATTACHEMENT
,NUM_TRANCHE
,NOCAT
,LIB_TRANCHE
,MT_TRANCHE
,ESTIMATION_COTISATION
,TMP_BATCH
,USER_CREATION
)
SELECT 
    sel.ID_PERIODE,
    sel.ID_BASE_ASSUJETTIE,
    sel.MOIS_RATTACHEMENT,
    sel.ID_AFFILIATION,
    sel.DATE_DEB_RATTACHEMENT,
    p.NUM_TRANCHE,
    t.NOCAT,
    p.LIB_TRANCHE,
    @MT_TRANCHE_SAVE:=COALESCE((SELECT 
                    SUM(c.MONTANT_COMPO_BASE_ASSUJ)
                FROM
                    COMPOSANT_BASE_ASSUJETTIE c
                WHERE
                    c.ID_BASE_ASSUJETTIE = sel.ID_BASE_ASSUJETTIE
                        AND c.TYPE_COMPO_BASE_ASSUJ IN (SELECT 
                            pc.NUM_TCBA
                        FROM
                            PARAM_NATURE_BASE_COMPOSANTS pc
                        WHERE
                            p.CONBCOT = pc.CONBCOT
                                AND p.NUM_TRANCHE = pc.NUM_TRANCHE
                                AND p.TXCALCU_REMPLI = pc.TXCALCU_REMPLI
                                AND pc.mode_conso = 'S'
                                AND pc.TMP_BATCH IS FALSE)
                        AND c.TMP_BATCH IS FALSE),
            0) AS MT_TRANCHE,
    CAST((@MT_TRANCHE_SAVE * 
    ((CASE 
    WHEN p.NUM_TRANCHE = 1 THEN t.TXBASE1
    WHEN p.NUM_TRANCHE = 2 THEN t.TXBASE2
    WHEN p.NUM_TRANCHE = 3 THEN t.TXBASE3
    WHEN p.NUM_TRANCHE = 4 THEN t.TXBASE4
    END) / 1000000 )
    * (c.TXAPPCOT / 10000)) AS DECIMAL(15,2)) AS ESTIMATION_COTISATION,
    sel.tmp_batch,
    p_audit_nom_batch
FROM
    V_Selection_base_assujettie sel
        LEFT JOIN
    TARIFS t ON sel.NUMERO_CONTRAT = t.NOCO
        AND sel.NOCAT = t.nocat
        AND t.DT_DEBUT_SIT <= sel.DATE_FIN_RATTACHEMENT
        AND (COALESCE(t.DT_FIN_SIT, 99999999) >= sel.DATE_FIN_RATTACHEMENT
        OR (CASE
        WHEN (t.DT_FIN_SIT = 0) THEN 99999999
        ELSE t.DT_FIN_SIT
    END) >= sel.DATE_FIN_RATTACHEMENT)
        LEFT JOIN
    PARAM_NATURE_BASE p ON t.CONBCOT = p.CONBCOT
        AND (CASE
        WHEN COALESCE(t.txcalcu, 0) > 0 THEN 'O'
        ELSE 'N'
    END) = p.TXCALCU_REMPLI
        INNER JOIN
        CONTRATS c ON (c.TMP_BATCH IS FALSE
	AND c.NOCO = sel.NUMERO_CONTRAT
    AND c.DT_DEBUT_SIT <= sel.DATE_DEBUT_PERIODE
        AND COALESCE(c.DT_FIN_SIT, 99999999) >= sel.DATE_DEBUT_PERIODE)
WHERE
    NOT ISNULL(t.NOCO)
        AND NOT ISNULL(t.NOCAT)
        AND NOT ISNULL(p.NUM_TRANCHE)
        AND FIND_IN_SET(sel.ID_PERIODE, p_liste_ids_periodes) > 0
ORDER BY ID_PERIODE , ID_BASE_ASSUJETTIE;
END$$
DELIMITER ;

DROP PROCEDURE IF EXISTS Inserer_TRANCHE_CATEGORIE_Pour_Periodes;

DELIMITER $$
CREATE PROCEDURE Inserer_TRANCHE_CATEGORIE_Pour_Periodes(IN p_audit_nom_batch VARCHAR(20), IN p_liste_ids_periodes MEDIUMTEXT)
BEGIN
-- Calcul et insersion des nouveaux éléments des périodes à traiter
INSERT INTO TRANCHE_CATEGORIE (ID_PERIODE, NOCAT, INDIVIDU, NUM_TRANCHE, LIB_TRANCHE, MT_TRANCHE, USER_CREATION)
select c.ID_PERIODE, c.NOCAT, c.INDIVIDU, p.NUM_TRANCHE, p.LIB_TRANCHE, 
coalesce(case WHEN (c.INDIVIDU = 'TOUS') THEN (SELECT SUM(tcba.MT_TRANCHE) 
FROM TRANCHE_CATEGORIE_BASE_ASSUJETTIE tcba 
WHERE tcba.ID_PERIODE = c.ID_PERIODE
AND tcba.NOCAT = c.NOCAT
AND tcba.NUM_TRANCHE = p.NUM_TRANCHE) 
ELSE (SELECT SUM(tcba.MT_TRANCHE) 
FROM TRANCHE_CATEGORIE_BASE_ASSUJETTIE tcba 
LEFT JOIN AFFILIATION a ON tcba.ID_AFFILIATION = a.ID_AFFILIATION
LEFT JOIN CONTRAT_TRAVAIL ct ON a.ID_CONTRAT_TRAVAIL = ct.ID_CONTRAT_TRAVAIL
LEFT JOIN INDIVIDU i ON ct.ID_INDIVIDU = i.ID_INDIVIDU 
WHERE tcba.ID_PERIODE = c.ID_PERIODE
AND tcba.NOCAT = c.NOCAT
AND tcba.NUM_TRANCHE = p.NUM_TRANCHE
AND (i.NTT  = c.INDIVIDU)) END, 0) +
coalesce(case WHEN (c.INDIVIDU = 'TOUS') THEN (null) 
ELSE (SELECT SUM(tcba.MT_TRANCHE) 
FROM TRANCHE_CATEGORIE_BASE_ASSUJETTIE tcba 
LEFT JOIN AFFILIATION a ON tcba.ID_AFFILIATION = a.ID_AFFILIATION
LEFT JOIN CONTRAT_TRAVAIL ct ON a.ID_CONTRAT_TRAVAIL = ct.ID_CONTRAT_TRAVAIL
LEFT JOIN INDIVIDU i ON ct.ID_INDIVIDU = i.ID_INDIVIDU 
WHERE tcba.ID_PERIODE = c.ID_PERIODE
AND tcba.NOCAT = c.NOCAT
AND tcba.NUM_TRANCHE = p.NUM_TRANCHE
AND (i.IDENTIFIANT_REPERTOIRE  = c.INDIVIDU)) END, 0) as MT_TRANCHE,
p_audit_nom_batch
from CATEGORIE_QUITTANCEMENT_INDIVIDU c
INNER JOIN (select distinct pnb.NUM_TRANCHE, pnb.LIB_TRANCHE, pnb.CONBCOT from PARAM_NATURE_BASE pnb) p ON p.CONBCOT = c.CONBCOT
INNER JOIN V_Selection_periode_nature_contrat_salaires s on s.ID_PERIODE = c.ID_PERIODE
WHERE FIND_IN_SET(c.ID_PERIODE, p_liste_ids_periodes) > 0;
END$$
DELIMITER ;

DROP PROCEDURE IF EXISTS Purger_elements_declaratifs_en_masse;

DELIMITER $$
CREATE PROCEDURE Purger_elements_declaratifs_en_masse(IN p_liste_ids_periodes MEDIUMTEXT)
BEGIN
	IF p_liste_ids_periodes = 'ALL' THEN BEGIN
		DELETE ecm FROM EFFECTIF_CATEGORIE_MVT ecm 
		INNER JOIN V_Selection_periode_nature_contrat_effectifs s ON s.ID_PERIODE = ecm.ID_PERIODE; 

		DELETE cqi FROM CATEGORIE_QUITTANCEMENT_INDIVIDU cqi
		INNER JOIN V_Selection_periode_nature_contrat_effectifs s ON s.ID_PERIODE = cqi.ID_PERIODE; 

		DELETE ecct FROM EFFECTIF_CATEGORIE_CONTRAT_TRAVAIL ecct
		INNER JOIN V_Selection_periode_nature_contrat_effectifs s ON s.ID_PERIODE = ecct.ID_PERIODE; 


		DELETE tc FROM TRANCHE_CATEGORIE tc
		INNER JOIN V_Selection_periode_nature_contrat_salaires s ON s.ID_PERIODE = tc.ID_PERIODE; 

		DELETE cqi FROM CATEGORIE_QUITTANCEMENT_INDIVIDU cqi
		INNER JOIN V_Selection_periode_nature_contrat_salaires s ON s.ID_PERIODE = cqi.ID_PERIODE; 

		DELETE tcba FROM TRANCHE_CATEGORIE_BASE_ASSUJETTIE tcba
		INNER JOIN V_Selection_periode_nature_contrat_salaires s ON s.ID_PERIODE = tcba.ID_PERIODE; 

		END;
	ELSE BEGIN
		DELETE FROM CATEGORIE_QUITTANCEMENT_INDIVIDU
		WHERE FIND_IN_SET(ID_PERIODE, p_liste_ids_periodes) > 0;

		DELETE FROM EFFECTIF_CATEGORIE_MVT
		WHERE FIND_IN_SET(ID_PERIODE, p_liste_ids_periodes) > 0;

		DELETE FROM TRANCHE_CATEGORIE
		WHERE FIND_IN_SET(ID_PERIODE, p_liste_ids_periodes) > 0;

		DELETE FROM EFFECTIF_CATEGORIE_CONTRAT_TRAVAIL
		WHERE FIND_IN_SET(ID_PERIODE, p_liste_ids_periodes) > 0;

		DELETE FROM TRANCHE_CATEGORIE_BASE_ASSUJETTIE
		WHERE FIND_IN_SET(ID_PERIODE, p_liste_ids_periodes) > 0;		
		END;
	END IF;
END$$
DELIMITER ;

DROP PROCEDURE IF EXISTS Creation_MessageControle_en_masse;
DELIMITER $$
CREATE PROCEDURE Creation_MessageControle_en_masse(IN p_liste_ids_periodes MEDIUMTEXT,IN message CHAR(100) ,IN idControle VARCHAR(30), IN param1 CHAR(50),IN param2 CHAR(50),IN param3 CHAR(50),IN param4 CHAR(50), IN niveau CHAR(10) , IN origine CHAR(6) , IN dateJour DECIMAL(8,0) , IN userCreation VARCHAR(100))
BEGIN
	
INSERT INTO message_controle(ID_PERIODE,ID_CONTROLE,ORIGINE,DATE_TRAITEMENT,NIVEAU_ALERTE,MESSAGE_UTILISATEUR,PARAM1,PARAM2,PARAM3,PARAM4,USER_CREATION)
SELECT p.ID_PERIODE,idControle,origine,dateJour,niveau,message,param1,param2,param3,param4,userCreation
FROM periodes_recues p WHERE FIND_IN_SET(p.ID_PERIODE, p_liste_ids_periodes) > 0 AND p.ID_PERIODE NOT IN(SELECT m.ID_PERIODE from message_controle m WHERE m.ID_CONTROLE=(idControle COLLATE latin1_general_ci)  AND m.ORIGINE=(origine COLLATE latin1_general_ci) AND FIND_IN_SET(m.ID_PERIODE, p_liste_ids_periodes) > 0 );

END$$
DELIMITER ;


DROP PROCEDURE IF EXISTS Creation_HistoriqueEtatPeriode_en_masse;
DELIMITER $$
CREATE PROCEDURE Creation_HistoriqueEtatPeriode_en_masse(IN p_liste_ids_periodes MEDIUMTEXT,IN datehms NUMERIC(14), IN etatPeriode CHAR(3), IN origine CHAR(4),
IN utilisateur CHAR(30), IN commentaire VARCHAR(1000), IN userCreation VARCHAR(20))
BEGIN
	
INSERT INTO HISTO_ETAT_PERIODE(ID_PERIODE, DATEHMS_CHANGEMENT, ETAT_PERIODE, ORIGINE, UTILISATEUR, COMMENTAIRE, USER_CREATION)
SELECT p.ID_PERIODE,datehms,etatPeriode,origine,utilisateur,commentaire,userCreation
FROM periodes_recues p WHERE FIND_IN_SET(ID_PERIODE, p_liste_ids_periodes) > 0;

END$$
DELIMITER ;


DROP PROCEDURE IF EXISTS Creation_HistoriqueCommentairePeriode_en_masse;
DELIMITER $$
CREATE PROCEDURE Creation_HistoriqueCommentairePeriode_en_masse(IN p_liste_ids_periodes MEDIUMTEXT,IN datehms NUMERIC(14), IN utilisateur CHAR(30), IN commentaire VARCHAR(1000), IN userCreation VARCHAR(20))
BEGIN
	
INSERT INTO HISTO_COMMENTAIRE_PERIODE(ID_PERIODE, UTILISATEUR, DATEHMS_SAISIE, COMMENTAIRE, USER_CREATION)
SELECT p.ID_PERIODE, utilisateur, datehms, commentaire, userCreation
FROM periodes_recues p WHERE FIND_IN_SET(ID_PERIODE, p_liste_ids_periodes) > 0;

END$$
DELIMITER ;


DROP PROCEDURE IF EXISTS Creation_HistoriqueAssignationPeriode_en_masse;
DELIMITER $$
CREATE PROCEDURE Creation_HistoriqueAssignationPeriode_en_masse(IN p_liste_ids_periodes MEDIUMTEXT,IN datehms NUMERIC(14), IN aTraiterPar CHAR(30), IN userCreation VARCHAR(20))
BEGIN
	
INSERT INTO HISTO_ASSIGNATION_PERIODE(ID_PERIODE, A_TRAITER_PAR, DATEHMS_ASSIGNE, USER_CREATION)
SELECT p.ID_PERIODE, aTraiterPar, datehms, userCreation
FROM periodes_recues p WHERE FIND_IN_SET(ID_PERIODE, p_liste_ids_periodes) > 0;

END$$
DELIMITER ;


DROP PROCEDURE IF EXISTS Creation_HistoriqueAttenteRetourEtpPeriode_en_masse;
DELIMITER $$
CREATE PROCEDURE Creation_HistoriqueAttenteRetourEtpPeriode_en_masse(IN p_liste_ids_periodes MEDIUMTEXT,IN datehms NUMERIC(14), IN utilisateur CHAR(30), IN action VARCHAR(20), IN userCreation VARCHAR(20))
BEGIN
	
INSERT INTO HISTO_ATTENTE_RETOUR_ETP_PERIODE(ID_PERIODE, ACTION, UTILISATEUR, DATEHMS_RETOUR, USER_CREATION)
SELECT p.ID_PERIODE, action, utilisateur, datehms, userCreation
FROM periodes_recues p WHERE FIND_IN_SET(ID_PERIODE, p_liste_ids_periodes) > 0
AND action collate latin1_general_ci != coalesce((SELECT h.action FROM histo_attente_retour_etp_periode h WHERE h.ID_PERIODE=p.ID_PERIODE ORDER BY h.DT_CREATION DESC LIMIT 1), 'NA');

END$$
DELIMITER ;


DROP PROCEDURE IF EXISTS Calcul_effectif_indiv_salaire_pour_periodes;
DELIMITER $$
CREATE PROCEDURE Calcul_effectif_indiv_salaire_pour_periodes(IN p_audit_nom_batch VARCHAR(20), in p_liste_ids_periodes MEDIUMTEXT)
BEGIN

DECLARE done INT DEFAULT FALSE;
DECLARE C_NO_CAT char(30);

DECLARE C_ID_PERIODE bigint(20);
DECLARE ID_PER_MAJ bigint(20);
DECLARE EFF_DEB_MAJ decimal(10,0);
DECLARE EFF_FIN_MAJ decimal(10,0);

DECLARE liste_nocat_unique CURSOR FOR
(SELECT cqi.ID_PERIODE,cqi.NOCAT
FROM categorie_quittancement_individu cqi INNER JOIN V_selection_periode_nature_contrat_salaires vpns ON (cqi.ID_PERIODE=vpns.ID_PERIODE)
WHERE cqi.individu='TOUS'
AND FIND_IN_SET(cqi.ID_PERIODE, p_liste_ids_periodes)
GROUP BY cqi.ID_PERIODE
HAVING COUNT(DISTINCT cqi.NOCAT)<=1);

DECLARE liste_nocat_non_unique CURSOR FOR
(SELECT cqi.ID_PERIODE,cqi.NOCAT
FROM categorie_quittancement_individu cqi INNER JOIN V_selection_periode_nature_contrat_salaires vpns ON (cqi.ID_PERIODE=vpns.ID_PERIODE)
WHERE cqi.individu='TOUS'
 AND FIND_IN_SET(cqi.ID_PERIODE, p_liste_ids_periodes)
 AND (SELECT COUNT(DISTINCT cqi2.NOCAT) FROM categorie_quittancement_individu cqi2 WHERE cqi2.ID_PERIODE=cqi.ID_PERIODE)>1
GROUP BY cqi.ID_PERIODE, cqi.NOCAT);

DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

OPEN liste_nocat_unique;
IF (Select FOUND_ROWS()) > 0 THEN
	nocat_uni: LOOP 
	FETCH liste_nocat_unique INTO C_ID_PERIODE, C_NO_CAT;
		IF done THEN 
			LEAVE nocat_uni;
		END IF;
		-- Calcul effectifs
		SELECT 
    COUNT(DISTINCT (CASE
            WHEN (i.IDENTIFIANT_REPERTOIRE = '') THEN i.NTT
            ELSE i.IDENTIFIANT_REPERTOIRE
        END)), p.ID_PERIODE INTO EFF_DEB_MAJ, ID_PER_MAJ
FROM
    periodes_recues p
        INNER JOIN
    rattachement_declarations_recues rda ON (p.ID_PERIODE = rda.ID_PERIODE)
        INNER JOIN
    adhesion_etablissement_mois adh ON (adh.ID_ADH_ETAB_MOIS = rda.ID_ADH_ETAB_MOIS)
        INNER JOIN
    individu i ON (i.ID_ADH_ETAB_MOIS = adh.ID_ADH_ETAB_MOIS)
		INNER JOIN
	contrat_travail ct ON (ct.ID_INDIVIDU = i.ID_INDIVIDU)
		INNER JOIN
	affiliation a ON (a.ID_CONTRAT_TRAVAIL = ct.ID_CONTRAT_TRAVAIL)
		INNER JOIN
	base_assujettie ba ON (ba.ID_AFFILIATION = a.ID_AFFILIATION AND ba.DATE_DEB_RATTACHEMENT = p.DATE_DEBUT_PERIODE)
WHERE
    p.ID_PERIODE = C_ID_PERIODE;
					
		SELECT 
    COUNT(DISTINCT (CASE
            WHEN (i.IDENTIFIANT_REPERTOIRE = '') THEN i.NTT
            ELSE i.IDENTIFIANT_REPERTOIRE
        END)) INTO EFF_FIN_MAJ
FROM
    periodes_recues p
        INNER JOIN
    rattachement_declarations_recues rda ON (p.ID_PERIODE = rda.ID_PERIODE)
        INNER JOIN
    adhesion_etablissement_mois adh ON (adh.ID_ADH_ETAB_MOIS = rda.ID_ADH_ETAB_MOIS)
        INNER JOIN
    individu i ON (i.ID_ADH_ETAB_MOIS = adh.ID_ADH_ETAB_MOIS)
		INNER JOIN
	contrat_travail ct ON (ct.ID_INDIVIDU = i.ID_INDIVIDU)
		INNER JOIN
	affiliation a ON (a.ID_CONTRAT_TRAVAIL = ct.ID_CONTRAT_TRAVAIL)
		INNER JOIN
	base_assujettie ba ON (ba.ID_AFFILIATION = a.ID_AFFILIATION AND ba.DATE_FIN_RATTACHEMENT = p.DATE_FIN_PERIODE)
WHERE
    p.ID_PERIODE = C_ID_PERIODE;
        
UPDATE categorie_quittancement_individu cqi3
SET 
	cqi3.EFFECTIF_DEBUT = EFF_DEB_MAJ,
	cqi3.EFFECTIF_FIN = EFF_FIN_MAJ
WHERE
	cqi3.ID_PERIODE = ID_PER_MAJ;
END LOOP;
END IF;
CLOSE liste_nocat_unique;


BEGIN
DECLARE done2 INT DEFAULT FALSE;
DECLARE CONTINUE HANDLER FOR NOT FOUND SET done2 = TRUE;

OPEN liste_nocat_non_unique;
IF (SELECT FOUND_ROWS()) > 0 THEN
	nocat_non_uni : LOOP 
	FETCH liste_nocat_non_unique INTO C_ID_PERIODE, C_NO_CAT;
		IF done2 THEN 
			LEAVE nocat_non_uni;
		END IF;
		-- Calcul effectifs 
		SELECT 
    COUNT(DISTINCT (CASE
            WHEN (i.IDENTIFIANT_REPERTOIRE = '') THEN i.NTT
            ELSE i.IDENTIFIANT_REPERTOIRE
        END)), p.ID_PERIODE INTO EFF_DEB_MAJ, ID_PER_MAJ
FROM
    periodes_recues p
        INNER JOIN
    rattachement_declarations_recues rda ON (p.ID_PERIODE = rda.ID_PERIODE)
        INNER JOIN
    adhesion_etablissement_mois adh ON (adh.ID_ADH_ETAB_MOIS = rda.ID_ADH_ETAB_MOIS)
        INNER JOIN
    individu i ON (i.ID_ADH_ETAB_MOIS = adh.ID_ADH_ETAB_MOIS)
		INNER JOIN
	contrat_travail ct ON (ct.ID_INDIVIDU = i.ID_INDIVIDU)
		INNER JOIN
	affiliation a ON (a.ID_CONTRAT_TRAVAIL = ct.ID_CONTRAT_TRAVAIL)
		INNER JOIN
	base_assujettie ba ON (ba.ID_AFFILIATION = a.ID_AFFILIATION AND ba.DATE_DEB_RATTACHEMENT = p.DATE_DEBUT_PERIODE)
WHERE
    p.ID_PERIODE = C_ID_PERIODE
	AND (C_NO_CAT collate latin1_general_ci) = (COALESCE(IF((a.CODE_POPULATION = ''),
                    NULL,
                    a.CODE_POPULATION),
                (SELECT 
                        t.NOCAT
                    FROM
                        tarifs t
                    WHERE
                        ((t.NOCO = p.NUMERO_CONTRAT)
                            AND (t.DT_DEBUT_SIT <= p.DATE_DEBUT_PERIODE)
                            AND ((COALESCE(t.DT_FIN_SIT, 99999999) >= p.DATE_DEBUT_PERIODE)
                            OR ((CASE
                            WHEN (t.DT_FIN_SIT = 0) THEN 99999999
                            ELSE t.DT_FIN_SIT
                        END) >= p.DATE_DEBUT_PERIODE)))
                    ORDER BY t.DT_DEBUT_SIT
                    LIMIT 1)));

SELECT 
    COUNT(DISTINCT (CASE
            WHEN (i.IDENTIFIANT_REPERTOIRE = '') THEN i.NTT
            ELSE i.IDENTIFIANT_REPERTOIRE
        END)) INTO EFF_FIN_MAJ
FROM
periodes_recues p
        INNER JOIN
    rattachement_declarations_recues rda ON (p.ID_PERIODE = rda.ID_PERIODE)
        INNER JOIN
    adhesion_etablissement_mois adh ON (adh.ID_ADH_ETAB_MOIS = rda.ID_ADH_ETAB_MOIS)
        INNER JOIN
    individu i ON (i.ID_ADH_ETAB_MOIS = adh.ID_ADH_ETAB_MOIS)
		INNER JOIN
	contrat_travail ct ON (ct.ID_INDIVIDU = i.ID_INDIVIDU)
		INNER JOIN
	affiliation a ON (a.ID_CONTRAT_TRAVAIL = ct.ID_CONTRAT_TRAVAIL)
		INNER JOIN
	base_assujettie ba ON (ba.ID_AFFILIATION = a.ID_AFFILIATION AND ba.DATE_FIN_RATTACHEMENT = p.DATE_FIN_PERIODE)
WHERE
    p.ID_PERIODE = C_ID_PERIODE
	AND (C_NO_CAT collate latin1_general_ci) = (COALESCE(IF((a.CODE_POPULATION = ''),
                    NULL,
                    a.CODE_POPULATION),
                (SELECT 
                        t.NOCAT
                    FROM
                        tarifs t
                    WHERE
                        ((t.NOCO = p.NUMERO_CONTRAT)
                            AND (t.DT_DEBUT_SIT <= p.DATE_DEBUT_PERIODE)
                            AND ((COALESCE(t.DT_FIN_SIT, 99999999) >= p.DATE_DEBUT_PERIODE)
                            OR ((CASE
                            WHEN (t.DT_FIN_SIT = 0) THEN 99999999
                            ELSE t.DT_FIN_SIT
                        END) >= p.DATE_DEBUT_PERIODE)))
                    ORDER BY t.DT_DEBUT_SIT
                    LIMIT 1)));

		UPDATE categorie_quittancement_individu cqi3 
SET 
    cqi3.EFFECTIF_DEBUT = EFF_DEB_MAJ,
    cqi3.EFFECTIF_FIN = EFF_FIN_MAJ
WHERE
    cqi3.ID_PERIODE = ID_PER_MAJ
AND cqi3.NOCAT = (C_NO_CAT collate latin1_general_ci);
	END LOOP;
END IF;
CLOSE liste_nocat_non_unique;
END;
END$$
DELIMITER ;

DROP PROCEDURE IF EXISTS Recherche_gestionnaire_siren;
DELIMITER $$
CREATE PROCEDURE Recherche_gestionnaire_siren(IN n_siren TEXT)
BEGIN

DECLARE VAR_NB_GG bigint(10);		-- variable contenant le nb de groupe de gestion des contrats du SIREN
DECLARE VAR_GG char(100); 			-- variable contenant la liste des groupes de gestion des contrats du SIREN

DECLARE VAR_NB_GEST bigint(10);		-- variable contenant le nb de gestionnaires assignés aux périodes du SIREN
DECLARE VAR_GEST char(200); 		-- variable contenant la liste des gestionnaires assignés aux périodes du SIREN

DECLARE VAR_GG_AUTO char(100);		-- variable contenant le résultat de la recherche pour savoir si le groupe de gestion est "autorisé"
DECLARE VAR_GEST_EXISTE char(100);	-- variable contenant le résultat de la recherche de l'existance du gestionnaire
DECLARE VAR_RECHERCHE char(100);	-- variable contenant le résultat des recherches des groupes de gestion dans les domaines
DECLARE VAR_NB_FICTIF bigint(10);	-- variable contenant le nb de gestionnaire fictif répondant aux critères
DECLARE VAR_INCLUS_1 char(100);		-- variable contenant le domaine inclus du gestionnaire fictif
DECLARE VAR_EXCLUS char (100);		-- variable contenant le domaine exclus du gestionnaire fictif

DECLARE OUT_CODE_USER TEXT;			-- variable contenant le code de l'utilisateurs
DECLARE OUT_NOM_USER TEXT;			-- variable contenant le nom du gestionnaire
DECLARE OUT_PRENOM_USER TEXT;		-- variable contenant le prenom du gestionnaire 
DECLARE OUT_NIVEAU_1 TEXT;			-- variable contenant le niveau 1 d'habilitation du gestionnaire
DECLARE OUT_NIVEAU_2 TEXT;			-- variable contenant le niveau 2 d'habilitation du gestionnaire
DECLARE OUT_NIVEAU_3 TEXT;			-- variable contenant le niveau 3 d'habilitation du gestionnaire

-- Pour le SIREN en entrée : recherche de ses groupes de gestion et gestionnaires assignés (count et group_concat)
		-- requete de recherche des contrats valides liés au SIREN (via clients ou extensions_entreprise_affilié)
		
		SELECT count(distinct c.nmgrpges) as nbgg, group_concat(distinct c.nmgrpges order by c.nmgrpges separator '%') as gg
				INTO VAR_NB_GG, VAR_GG
		FROM contrats c
		inner join param_etat_contrat p on p.COETACO = c.coetaco and c.tmp_batch = p.tmp_batch
		left join clients cl on cl.nocli = c.nocli and c.tmp_batch = cl.tmp_batch and cl.nosiren = (n_siren collate latin1_general_ci)
		left join extensions_entreprises_affiliees ext on ext.noco=c.NOCO and ext.siren = (n_siren collate latin1_general_ci)
		WHERE c.tmp_batch IS FALSE
		AND p.ACTIF = 'O'
		AND (c.eligdsn = 'SRN' OR c.eligdsn = 'SRT')
		AND (c.dt_fin_sit IS NULL)
		AND (NOT EXISTS (SELECT 1 FROM param_siren_faux s WHERE s.SIREN=(n_siren collate latin1_general_ci)))
		AND NOT ( cl.nosiren is null and ext.siren is null );
		
-- si le nombre de groupe = 0
IF (VAR_NB_GG = 0) THEN
	-- !!!!!! cas non prévu mais existant
	-- => A qui associer ce SIREN ??? => A enlever du fichier 
	SELECT null, null, null, null, null, null
	INTO OUT_CODE_USER, OUT_PRENOM_USER, OUT_NOM_USER, OUT_NIVEAU_1, OUT_NIVEAU_2, OUT_NIVEAU_3;
	
-- sinon, si le nombre de groupe = 1
ELSEIF (VAR_NB_GG = 1) THEN
	
	-- recherche si le groupe est "autorisé"
		-- recherche si retour pour le gestionnaire fictif de type GEST_AUTRE avec GG_INCLUS_1 = '' and GG_INCLUS_2 = '' and GG_EXCLUS <> '' et GG_exclus like groupe du gestion du SIREN
		-- si retour => groupe "autorisé"
		-- si pas de retour => groupe non "autorisé"
		SELECT pug.code_user INTO VAR_GG_AUTO 
		FROM param_utilisateur_gestionnaire pug 
		WHERE pug.TYPE = 'GEST_AUTRE'
		AND pug.GG_INCLUS_1 = '' and pug.GG_INCLUS_2 = '' and pug.GG_EXCLUS <> ''
		AND pug.GG_EXCLUS like concat('%', (VAR_GG collate latin1_general_ci), '%');
	
	-- si le groupe n'est pas "autorisé"
	IF (VAR_GG_AUTO is null) THEN
		-- Cas 6.1 : 
		-- => le SIREN est associé au gestionnaire fictif de type : GEST_AUTRE avec GG_INCLUS_1 = '' and GG_INCLUS_2 = '' and GG_EXCLUS <> '' 
		SELECT pug.code_user, pug.PRENOM, pug.NOM, pug.NIVEAU_1, pug.NIVEAU_2, pug.NIVEAU_3 
		INTO OUT_CODE_USER, OUT_PRENOM_USER, OUT_NOM_USER, OUT_NIVEAU_1, OUT_NIVEAU_2, OUT_NIVEAU_3
		FROM param_utilisateur_gestionnaire pug 
		WHERE pug.TYPE = 'GEST_AUTRE'
		AND pug.GG_INCLUS_1 = '' and pug.GG_INCLUS_2 = '' and pug.GG_EXCLUS <> '';
		
	-- sinon (le groupe est "autorisé")
	ELSE 
	
		-- recherche du nombre de gestionnaires assignés aux dernières périodes des contrats du SIREN
		SELECT COUNT(DISTINCT(LIGNES)), group_concat(distinct lignes) 
		INTO VAR_NB_GEST, VAR_GEST
		FROM
		(SELECT _periode.A_TRAITER_PAR AS LIGNES FROM
			(SELECT pr.A_TRAITER_PAR, pr.NUMERO_CONTRAT, pr.DATE_FIN_PERIODE FROM periodes_recues pr where numero_contrat in (
			SELECT distinct c.noco
			FROM contrats c
			inner join param_etat_contrat p on p.COETACO = c.coetaco and c.tmp_batch = p.tmp_batch
			left join clients cl on cl.nocli = c.nocli and c.tmp_batch = cl.tmp_batch and cl.nosiren = (n_siren collate latin1_general_ci)
			left join extensions_entreprises_affiliees ext on ext.noco=c.NOCO and ext.siren = (n_siren collate latin1_general_ci)
			WHERE c.tmp_batch IS FALSE
			AND p.ACTIF = 'O'
			AND (c.eligdsn = 'SRN' OR c.eligdsn = 'SRT')
			AND (c.dt_fin_sit IS NULL)
			AND (NOT EXISTS (SELECT 1 FROM param_siren_faux s WHERE s.SIREN=(n_siren collate latin1_general_ci)))
			and ( (cl.nosiren is null and ext.siren is not null) or (cl.nosiren is not null and ext.siren is null) ))
			order by date_fin_periode DESC, date_creation desc) AS _periode
			GROUP BY _periode.NUMERO_CONTRAT) as _pug1ionnaire;
	
	
		-- si le nombre de gestionnaire assigné = 0
		IF (VAR_NB_GEST = 0) THEN
			-- Cas 1.3 + 2.3 : mono-groupe / aucun gestionnaire
			-- => Le SIREN est associé au gestionnaire « Non assigné » en groupe unitaire pour le groupe de gestion de ce contrat (infos des niveaux de ce gestionnaire)
				-- recherche du gestionnaire fictif de type : NON_ASS_GU et GG_EGAL = groupe de gestion du SIREN
			SELECT pug.code_user, pug.PRENOM, pug.NOM, pug.NIVEAU_1, pug.NIVEAU_2, pug.NIVEAU_3 
			INTO OUT_CODE_USER, OUT_PRENOM_USER, OUT_NOM_USER, OUT_NIVEAU_1, OUT_NIVEAU_2, OUT_NIVEAU_3
			FROM param_utilisateur_gestionnaire pug 
			WHERE pug.TYPE = 'NON_ASS_GU'
			AND pug.GG_EGAL = (VAR_GG collate latin1_general_ci);
			
		-- sinon, si le nombre de gestionnaire assigné = 1
		ELSEIF (VAR_NB_GEST = 1) THEN
		
			-- recherche si le gestionnaire existe
			SELECT pug.code_user
			INTO VAR_GEST_EXISTE
			FROM param_utilisateur_gestionnaire pug 
			WHERE pug.TYPE = 'GEST'
			AND pug.CODE_USER = (VAR_GEST collate latin1_general_ci);
			
			-- si le gestionnaire existe 
			IF (VAR_GEST_EXISTE is not null) THEN
				-- Cas 1.1 + Cas 2.1 : mono-groupe / mono-gestionnaire existant 
				-- => le SIREN est associé au gestionnaire existant (infos des niveaux de ce gestionnaire)
				SELECT pug.code_user, pug.PRENOM, pug.NOM, pug.NIVEAU_1, pug.NIVEAU_2, pug.NIVEAU_3 
				INTO OUT_CODE_USER, OUT_PRENOM_USER, OUT_NOM_USER, OUT_NIVEAU_1, OUT_NIVEAU_2, OUT_NIVEAU_3
				FROM param_utilisateur_gestionnaire pug 
				WHERE pug.TYPE = 'GEST'
				AND pug.CODE_USER = (VAR_GEST collate latin1_general_ci);
				
			-- sinon (le gestionnaire n'existe pas)
			ELSE
				-- Cas 1.2 + Cas 2.2 : mono-groupe / mono-gestionnaire non existant
				-- => Le SIREN est associé au gestionnaire « Autre » en groupe unitaire pour le groupe de gestion de ce contrat (infos des niveaux de ce gestionnaire)
					-- recherche du gestionnaire fictif de type : AUTRE_GU et GG_EGAL = groupe de gestion du SIREN
				SELECT pug.code_user, pug.PRENOM, pug.NOM, pug.NIVEAU_1, pug.NIVEAU_2, pug.NIVEAU_3 
				INTO OUT_CODE_USER, OUT_PRENOM_USER, OUT_NOM_USER, OUT_NIVEAU_1, OUT_NIVEAU_2, OUT_NIVEAU_3
				FROM param_utilisateur_gestionnaire pug 
				WHERE pug.TYPE = 'AUTRE_GU'
				AND pug.GG_EGAL = (VAR_GG collate latin1_general_ci);
			
			-- end (gestionnaire existant ou non)
			END IF;
			
		-- sinon (le nombre de gestionnaire assigné > 1)
		ELSE
			-- Cas 2.4 : mono-groupe / multi-gestionnaire
			-- => Le SIREN est associé au gestionnaire « Multi » en groupe unitaire pour le groupe de gestion de ces contrats (infos des niveaux de ce gestionnaire)
				-- recherche du gestionnaire fictif de type : MULTI_GU et GG_EGAL = groupe de gestion du SIREN
			SELECT pug.code_user, pug.PRENOM, pug.NOM, pug.NIVEAU_1, pug.NIVEAU_2, pug.NIVEAU_3 
			INTO OUT_CODE_USER, OUT_PRENOM_USER, OUT_NOM_USER, OUT_NIVEAU_1, OUT_NIVEAU_2, OUT_NIVEAU_3
			FROM param_utilisateur_gestionnaire pug 
			WHERE pug.TYPE = 'MULTI_GU'
			AND pug.GG_EGAL = (VAR_GG collate latin1_general_ci);
		
		-- end (nombre de gestionnaire assigné)
		END IF;
		
	-- end (group "autorisé" ou non)
	END IF;
	
-- sinon (nombre de groupe > 1)
ELSE
	
	-- recherche du nombre de gestionnaires assignés aux dernières périodes des contrats du SIREN
	SELECT COUNT(DISTINCT(LIGNES)), group_concat(distinct lignes) 
	INTO VAR_NB_GEST, VAR_GEST
	FROM
	(SELECT _periode.A_TRAITER_PAR AS LIGNES FROM
		(SELECT pr.A_TRAITER_PAR, pr.NUMERO_CONTRAT, pr.DATE_FIN_PERIODE FROM periodes_recues pr where numero_contrat in (
		SELECT distinct c.noco
		FROM contrats c
		inner join param_etat_contrat p on p.COETACO = c.coetaco and c.tmp_batch = p.tmp_batch
		left join clients cl on cl.nocli = c.nocli and c.tmp_batch = cl.tmp_batch and cl.nosiren = (n_siren collate latin1_general_ci)
		left join extensions_entreprises_affiliees ext on ext.noco=c.NOCO and ext.siren = (n_siren collate latin1_general_ci)
		WHERE c.tmp_batch IS FALSE
		AND p.ACTIF = 'O'
		AND (c.eligdsn = 'SRN' OR c.eligdsn = 'SRT')
		AND (c.dt_fin_sit IS NULL)
		AND (NOT EXISTS (SELECT 1 FROM param_siren_faux s WHERE s.SIREN=(n_siren collate latin1_general_ci)))
		and ( (cl.nosiren is null and ext.siren is not null) or (cl.nosiren is not null and ext.siren is null) ))
		order by date_fin_periode DESC, date_creation desc) AS _periode
		GROUP BY _periode.NUMERO_CONTRAT) as _pug1ionnaire;
	
	
	-- recherche si les groupes du SIREN sont mono-domaine
		-- recherche du gestionnaire fictif de type GEST_MULTI avec GG_INCLUS_1 <> '' and GG_INCLUS_2 = '' and GG_EXCLUS = '' et GG_INCLUS_1 like groupes du gestion du SIREN
		-- si retour => groupes mono-domaine
		-- si pas de retour => groupes non mono-domaine
	SELECT pug.code_user
	INTO VAR_RECHERCHE
	FROM param_utilisateur_gestionnaire pug 
	WHERE pug.TYPE = 'GEST_MULTI'
	AND pug.GG_INCLUS_1 <> '' and pug.GG_INCLUS_2 = '' and pug.GG_EXCLUS = ''
	AND pug.GG_INCLUS_1 like (concat('%', (VAR_GG collate latin1_general_ci), '%'));
		
		
	-- si groupes mono-domaine
	IF (VAR_RECHERCHE is not null) THEN
	
		-- si le nombre de gestionnaire assigné = 0
		IF (VAR_NB_GEST = 0) THEN
			-- Cas 3.3 : multi-groupe / mono-domaine / aucun gestionnaire
			-- => Le SIREN est associé au gestionnaire « Non assigné » en multi-groupes de ce domaine (infos des niveaux de ce gestionnaire)
				-- recherche du gestionnaire fictif de type : NON_ASS_MULTI avec GG_INCLUS_1 <> '' and GG_INCLUS_2 = '' and GG_EXCLUS = '' et GG_INCLUS_1 like groupes du gestion du SIREN
			SELECT pug.code_user, pug.PRENOM, pug.NOM, pug.NIVEAU_1, pug.NIVEAU_2, pug.NIVEAU_3 
			INTO OUT_CODE_USER, OUT_PRENOM_USER, OUT_NOM_USER, OUT_NIVEAU_1, OUT_NIVEAU_2, OUT_NIVEAU_3
			FROM param_utilisateur_gestionnaire pug 
			WHERE pug.TYPE = 'NON_ASS_MULTI'
			AND pug.GG_INCLUS_1 <> '' and pug.GG_INCLUS_2 = '' and pug.GG_EXCLUS = ''
			AND pug.GG_INCLUS_1 like (concat('%', (VAR_GG collate latin1_general_ci), '%'));
		
		-- sinon, si le nombre de gestionnaire assigné = 1
		ELSEIF (VAR_NB_GEST = 1) THEN
		
			-- recherche si le gestionnaire existe
			SELECT pug.code_user
			INTO VAR_GEST_EXISTE
			FROM param_utilisateur_gestionnaire pug 
			WHERE pug.TYPE = 'GEST'
			AND pug.CODE_USER = (VAR_GEST collate latin1_general_ci);
			
			-- si le gestionnaire existe 
			IF (VAR_GEST_EXISTE is not null) THEN
				-- Cas 3.1 : multi-groupe / mono-domaine / mono-gestionnaire existant 
				-- => Le SIREN est associé à ce gestionnaire (infos des niveaux du gestionnaire en multi-groupe du domaine)
						-- Pour les infos de niveaux : recherche du gestionnaire fictif de type : GEST_MULTI avec GG_INCLUS_1 <> '' and GG_INCLUS_2 = '' and GG_EXCLUS = '' et GG_INCLUS_1 like groupes du gestion du SIREN
				SELECT pug.code_user, pug.PRENOM, pug.NOM
				INTO OUT_CODE_USER, OUT_PRENOM_USER, OUT_NOM_USER
				FROM param_utilisateur_gestionnaire pug 
				WHERE pug.TYPE = 'GEST'
				AND pug.CODE_USER = (VAR_GEST collate latin1_general_ci);
				
				SELECT pug.NIVEAU_1, pug.NIVEAU_2, pug.NIVEAU_3 
				INTO OUT_NIVEAU_1, OUT_NIVEAU_2, OUT_NIVEAU_3
				FROM param_utilisateur_gestionnaire pug 
				WHERE pug.TYPE = 'GEST_MULTI'
				AND pug.GG_INCLUS_1 <> '' and pug.GG_INCLUS_2 = '' and pug.GG_EXCLUS = ''
				AND pug.GG_INCLUS_1 like (concat('%', (VAR_GG collate latin1_general_ci), '%'));
					
			-- sinon (le gestionnaire n'existe pas)
			ELSE
				-- Cas 3.2 : multi-groupe / mono-domaine / mono-gestionnaire non existant
				-- => Le SIREN est associé au gestionnaire « Autre » en multi-groupes de ce domaine (infos des niveaux de ce gestionnaire)
					-- recherche du gestionnaire fictif de type : AUTRE_MULTI avec GG_INCLUS_1 <> '' and GG_INCLUS_2 = '' and GG_EXCLUS = '' et GG_INCLUS_1 like groupes du gestion du SIREN
				SELECT pug.code_user, pug.PRENOM, pug.NOM, pug.NIVEAU_1, pug.NIVEAU_2, pug.NIVEAU_3 
				INTO OUT_CODE_USER, OUT_PRENOM_USER, OUT_NOM_USER, OUT_NIVEAU_1, OUT_NIVEAU_2, OUT_NIVEAU_3
				FROM param_utilisateur_gestionnaire pug 
				WHERE pug.TYPE = 'AUTRE_MULTI'
				AND pug.GG_INCLUS_1 <> '' and pug.GG_INCLUS_2 = '' and pug.GG_EXCLUS = ''
				AND pug.GG_INCLUS_1 like (concat('%', (VAR_GG collate latin1_general_ci), '%'));
			
			-- end (gestionnaire existant ou non)
			END IF;
			
		-- sinon (le nombre de gestionnaire assigné > 1)
		ELSE
			-- Cas 3.4 : multi-groupe / mono-domaine / multi-gestionnaire
			-- => Le SIREN est associé au gestionnaire « Multi » en multi-groupes de ce domaine (infos des niveaux de ce gestionnaire)
				-- recherche du gestionnaire fictif de type : MULTI_MULTI avec GG_INCLUS_1 <> '' and GG_INCLUS_2 = '' and GG_EXCLUS = '' et GG_INCLUS_1 like groupes du gestion du SIREN
			SELECT pug.code_user, pug.PRENOM, pug.NOM, pug.NIVEAU_1, pug.NIVEAU_2, pug.NIVEAU_3 
			INTO OUT_CODE_USER, OUT_PRENOM_USER, OUT_NOM_USER, OUT_NIVEAU_1, OUT_NIVEAU_2, OUT_NIVEAU_3
			FROM param_utilisateur_gestionnaire pug 
			WHERE pug.TYPE = 'MULTI_MULTI'
			AND pug.GG_INCLUS_1 <> '' and pug.GG_INCLUS_2 = '' and pug.GG_EXCLUS = ''
			AND pug.GG_INCLUS_1 like (concat('%', (VAR_GG collate latin1_general_ci), '%'));
		
		-- end (nombre de gestionnaire assigné)
		END IF;
		
	-- sinon (groupes non mono_domaine)
	ELSE
		
		-- recherche si les groupes du SIREN sont uniquement sur les 2 domaines
			-- recherche du gestionnaire fictif de type GEST_MULTI avec GG_INCLUS_1 <> '' and GG_INCLUS_2 <> '' and GG_EXCLUS = '' 
				-- et concat(GG_INCLUS_1, GG_INCLUS_2) like groupes du gestion du SIREN
			-- si retour => groupes uniquement sur les 2 domaines
			-- si pas de retour => groupes non uniquement sur les 2 domaines
		SELECT pug.code_user
		INTO VAR_RECHERCHE
		FROM param_utilisateur_gestionnaire pug 
		WHERE pug.TYPE = 'GEST_MULTI'
		AND pug.GG_INCLUS_1 <> '' and pug.GG_INCLUS_2 <> '' and pug.GG_EXCLUS = ''
		AND concat(pug.GG_INCLUS_1, pug.GG_INCLUS_2) like (concat('%', (VAR_GG collate latin1_general_ci), '%'));
		
		-- si groupes uniquement sur les 2 domaines
		IF (VAR_RECHERCHE is not null) THEN
			
			-- si le nombre de gestionnaire assigné = 0
			IF (VAR_NB_GEST = 0) THEN
				-- Cas 4.3 : multi-groupe / 2 domaines / aucun gestionnaire
				-- => Le SIREN est associé au gestionnaire « Non assigné » en multi-groupes des 2 domaines (infos des niveaux de ce gestionnaire)
					-- recherche du gestionnaire fictif de type : NON_ASS_MULTI avec GG_INCLUS_1 <> '' and GG_INCLUS_2 <> '' and GG_EXCLUS = '' (pas sur que utile : et concat(GG_INCLUS_1, GG_INCLUS_2) like groupes du gestion du SIREN)
				SELECT pug.code_user, pug.PRENOM, pug.NOM, pug.NIVEAU_1, pug.NIVEAU_2, pug.NIVEAU_3 
				INTO OUT_CODE_USER, OUT_PRENOM_USER, OUT_NOM_USER, OUT_NIVEAU_1, OUT_NIVEAU_2, OUT_NIVEAU_3
				FROM param_utilisateur_gestionnaire pug 
				WHERE pug.TYPE = 'NON_ASS_MULTI'
				AND pug.GG_INCLUS_1 <> '' and pug.GG_INCLUS_2 <> '' and pug.GG_EXCLUS = '';
			
			-- sinon, si le nombre de gestionnaire assigné = 1
			ELSEIF (VAR_NB_GEST = 1) THEN
			
				-- recherche si le gestionnaire existe
				SELECT pug.code_user
				INTO VAR_GEST_EXISTE
				FROM param_utilisateur_gestionnaire pug 
				WHERE pug.TYPE = 'GEST'
				AND pug.CODE_USER = (VAR_GEST collate latin1_general_ci);
				
				-- si le gestionnaire existe 
				IF (VAR_GEST_EXISTE is not null) THEN
					-- Cas 4.1 : multi-groupe / 2 domaines / mono-gestionnaire existant 
					-- => Le SIREN est associé à ce gestionnaire (infos des niveaux du gestionnaire en multi-groupes des 2 domaines)
							-- Pour les infos de niveaux : recherche du gestionnaire fictif de type : GEST_MULTI avec GG_INCLUS_1 <> '' and GG_INCLUS_2 <> '' and GG_EXCLUS = '' (pas sur que utile : et concat(GG_INCLUS_1, GG_INCLUS_2) like groupes du gestion du SIREN)
					SELECT pug.code_user, pug.PRENOM, pug.NOM
					INTO OUT_CODE_USER, OUT_PRENOM_USER, OUT_NOM_USER
					FROM param_utilisateur_gestionnaire pug 
					WHERE pug.TYPE = 'GEST'
					AND pug.CODE_USER = (VAR_GEST collate latin1_general_ci);
					
					SELECT pug.NIVEAU_1, pug.NIVEAU_2, pug.NIVEAU_3 
					INTO OUT_NIVEAU_1, OUT_NIVEAU_2, OUT_NIVEAU_3
					FROM param_utilisateur_gestionnaire pug 
					WHERE pug.TYPE = 'GEST_MULTI'
					AND pug.GG_INCLUS_1 <> '' and pug.GG_INCLUS_2 <> '' and pug.GG_EXCLUS = '';
					
				-- sinon (le gestionnaire n'existe pas)
				ELSE
					-- Cas 4.2 : multi-groupe / 2 domaines / mono-gestionnaire non existant
					-- => Le SIREN est associé au gestionnaire « Autre » en multi-groupes des 2 domaines (infos des niveaux de ce gestionnaire)
						-- recherche du gestionnaire fictif de type : AUTRE_MULTI avec GG_INCLUS_1 <> '' and GG_INCLUS_2 <>'' and GG_EXCLUS = '' (pas sur que utile : et concat(GG_INCLUS_1, GG_INCLUS_2) like groupes du gestion du SIREN)
					SELECT pug.code_user, pug.PRENOM, pug.NOM, pug.NIVEAU_1, pug.NIVEAU_2, pug.NIVEAU_3 
					INTO OUT_CODE_USER, OUT_PRENOM_USER, OUT_NOM_USER, OUT_NIVEAU_1, OUT_NIVEAU_2, OUT_NIVEAU_3
					FROM param_utilisateur_gestionnaire pug 
					WHERE pug.TYPE = 'AUTRE_MULTI'
					AND pug.GG_INCLUS_1 <> '' and pug.GG_INCLUS_2 <> '' and pug.GG_EXCLUS = '';
				
				-- end (gestionnaire existant ou non)
				END IF;
				
			-- sinon (le nombre de gestionnaire assigné > 1)
			ELSE
				-- Cas 4.4 : multi-groupe / 2 domaines / multi-gestionnaire
				-- => Le SIREN est associé au gestionnaire « Multi » en multi-groupes des 2 domaines (infos des niveaux de ce gestionnaire)
					-- recherche du gestionnaire fictif de type : MULTI_MULTI avec GG_INCLUS_1 <> '' and GG_INCLUS_2 <> '' and GG_EXCLUS = '' (pas sur que utile : et concat(GG_INCLUS_1, GG_INCLUS_2) like groupes du gestion du SIREN)
				SELECT pug.code_user, pug.PRENOM, pug.NOM, pug.NIVEAU_1, pug.NIVEAU_2, pug.NIVEAU_3 
				INTO OUT_CODE_USER, OUT_PRENOM_USER, OUT_NOM_USER, OUT_NIVEAU_1, OUT_NIVEAU_2, OUT_NIVEAU_3
				FROM param_utilisateur_gestionnaire pug 
				WHERE pug.TYPE = 'MULTI_MULTI'
				AND pug.GG_INCLUS_1 <> '' and pug.GG_INCLUS_2 <> '' and pug.GG_EXCLUS = '';
			
			-- end (nombre de gestionnaire assigné)
			END IF;
		
		-- sinon (groupes non uniquement sur les 2 domaines)
		ELSE
			
			-- recherche si les groupes du SIREN sont sur 1 seul domaine + autre
				-- recherche du gestionnaire fictif de type GEST_MULTI avec GG_INCLUS_1 <> '' and GG_INCLUS_2 = '' and GG_EXCLUS <> '' 
					-- on a au moins 1 groupe dans le inclus et aucun groupe dans le exclus
				-- si retour = 0 => groupes sur aucun domaine
				-- si retour = 1 => groupes sur 1 seul domaine + autre
				-- si retour = 2 => groupes sur 2 domaine + autre
			SELECT count(distinct pug2.code_user), pug2.GG_INCLUS_1, pug2.GG_EXCLUS INTO VAR_NB_FICTIF, VAR_INCLUS_1, VAR_EXCLUS
			FROM (
				SELECT distinct c.nmgrpges
				FROM contrats c
				inner join param_etat_contrat p on p.COETACO = c.coetaco and c.tmp_batch = p.tmp_batch
				left join clients cl on cl.nocli = c.nocli and c.tmp_batch = cl.tmp_batch and cl.nosiren = (n_siren collate latin1_general_ci)
				left join extensions_entreprises_affiliees ext on ext.noco=c.NOCO and ext.siren = (n_siren collate latin1_general_ci)
				WHERE c.tmp_batch IS FALSE
				AND p.ACTIF = 'O'
				AND (c.eligdsn = 'SRN' OR c.eligdsn = 'SRT')
				AND (c.dt_fin_sit IS NULL)
				AND (NOT EXISTS (SELECT 1 FROM param_siren_faux s WHERE s.SIREN=(n_siren collate latin1_general_ci)))
				and ( (cl.nosiren is null and ext.siren is not null) or (cl.nosiren is not null and ext.siren is null) )
			) as tmp_siren_liste_gg
			inner join param_utilisateur_gestionnaire pug2 
				on pug2.GG_inclus_1 like concat('%',tmp_siren_liste_gg.nmgrpges,'%') 
				and pug2.GG_exclus not like concat('%',tmp_siren_liste_gg.nmgrpges,'%')
				and concat(pug2.GG_inclus_1, ',', pug2.GG_exclus) not like concat('%', (VAR_GG collate latin1_general_ci), '%') 
				and concat(pug2.GG_exclus, ',', pug2.GG_inclus_1) not like concat('%', (VAR_GG collate latin1_general_ci), '%') 
				and pug2.`TYPE` = 'GEST_MULTI'
				and pug2.GG_inclus_1 <> '' and pug2.GG_INCLUS_2 = '' and pug2.GG_EXCLUS <> '';
				
			-- si groupes sur aucun domaine
			IF (VAR_NB_FICTIF = 0) THEN
				-- Cas 6.1 : 
				-- => le SIREN est associé au gestionnaire fictif de type : GEST_AUTRE avec GG_INCLUS_1 = '' and GG_INCLUS_2 = '' and GG_EXCLUS <> '' 
				SELECT pug.code_user, pug.PRENOM, pug.NOM, pug.NIVEAU_1, pug.NIVEAU_2, pug.NIVEAU_3 
				INTO OUT_CODE_USER, OUT_PRENOM_USER, OUT_NOM_USER, OUT_NIVEAU_1, OUT_NIVEAU_2, OUT_NIVEAU_3
				FROM param_utilisateur_gestionnaire pug 
				WHERE pug.TYPE = 'GEST_AUTRE'
				AND pug.GG_INCLUS_1 = '' and pug.GG_INCLUS_2 = '' and pug.GG_EXCLUS <> '';
				
			-- sinon, si groupes uniquement sur 1 seul domaine + autre
			ELSEIF (VAR_NB_FICTIF = 1) THEN
			
				-- si le nombre de gestionnaire assigné = 0
				IF (VAR_NB_GEST = 0) THEN
					-- Cas 5.3 : multi-groupe / 1 seul domaine + autre / aucun gestionnaire
					-- => Le SIREN est associé au gestionnaire « Non assigné » pour le domaine et autre (infos des niveaux de ce gestionnaire)
						-- recherche du gestionnaire fictif de type : NON_ASS_MULTI avec GG_INCLUS_1 <> '' and GG_INCLUS_2 = '' and GG_EXCLUS <> ''
					SELECT pug.code_user, pug.PRENOM, pug.NOM, pug.NIVEAU_1, pug.NIVEAU_2, pug.NIVEAU_3 
					INTO OUT_CODE_USER, OUT_PRENOM_USER, OUT_NOM_USER, OUT_NIVEAU_1, OUT_NIVEAU_2, OUT_NIVEAU_3
					FROM param_utilisateur_gestionnaire pug 
					WHERE pug.TYPE = 'NON_ASS_MULTI'
					AND pug.GG_INCLUS_1 = (VAR_INCLUS_1 collate latin1_general_ci) and pug.GG_INCLUS_2 = '' and pug.GG_EXCLUS = (VAR_EXCLUS collate latin1_general_ci);
				
				-- sinon, si le nombre de gestionnaire assigné = 1
				ELSEIF (VAR_NB_GEST = 1) THEN
				
					-- recherche si le gestionnaire existe
					SELECT pug.code_user
					INTO VAR_GEST_EXISTE
					FROM param_utilisateur_gestionnaire pug 
					WHERE pug.TYPE = 'GEST'
					AND pug.CODE_USER = (VAR_GEST collate latin1_general_ci);
					
					-- si le gestionnaire existe 
					IF (VAR_GEST_EXISTE is not null) THEN
						-- Cas 5.1 : multi-groupe / 1 seul domaine + autre / mono-gestionnaire existant 
						-- => Le SIREN est associé à ce gestionnaire (infos des niveaux du gestionnaire en multi-groupes des 2 domaines)
								-- Pour les infos de niveaux : recherche du gestionnaire fictif de type : GEST_MULTI avec GG_INCLUS_1 <> '' and GG_INCLUS_2 = '' and GG_EXCLUS <> ''
						SELECT pug.code_user, pug.PRENOM, pug.NOM
						INTO OUT_CODE_USER, OUT_PRENOM_USER, OUT_NOM_USER
						FROM param_utilisateur_gestionnaire pug 
						WHERE pug.TYPE = 'GEST'
						AND pug.CODE_USER = (VAR_GEST collate latin1_general_ci);
						
						SELECT pug.NIVEAU_1, pug.NIVEAU_2, pug.NIVEAU_3 
						INTO OUT_NIVEAU_1, OUT_NIVEAU_2, OUT_NIVEAU_3
						FROM param_utilisateur_gestionnaire pug 
						WHERE pug.TYPE = 'GEST_MULTI'
						AND pug.GG_INCLUS_1 = (VAR_INCLUS_1 collate latin1_general_ci) and pug.GG_INCLUS_2 = '' and pug.GG_EXCLUS = (VAR_EXCLUS collate latin1_general_ci);
						
					-- sinon (le gestionnaire n'existe pas)
					ELSE
						-- Cas 5.2 : multi-groupe / 1 seul domaine + autre / mono-gestionnaire non existant
						-- => Le SIREN est associé au gestionnaire « Autre » en multi-groupes des 2 domaines (infos des niveaux de ce gestionnaire)
							-- recherche du gestionnaire fictif de type : AUTRE_MULTI avec GG_INCLUS_1 <> '' and GG_INCLUS_2 ='' and GG_EXCLUS <> ''
						SELECT pug.code_user, pug.PRENOM, pug.NOM, pug.NIVEAU_1, pug.NIVEAU_2, pug.NIVEAU_3 
						INTO OUT_CODE_USER, OUT_PRENOM_USER, OUT_NOM_USER, OUT_NIVEAU_1, OUT_NIVEAU_2, OUT_NIVEAU_3
						FROM param_utilisateur_gestionnaire pug 
						WHERE pug.TYPE = 'AUTRE_MULTI'
						AND pug.GG_INCLUS_1 = (VAR_INCLUS_1 collate latin1_general_ci) and pug.GG_INCLUS_2 = '' and pug.GG_EXCLUS = (VAR_EXCLUS collate latin1_general_ci);
						
					-- end (gestionnaire existant ou non)
					END IF;
					
				-- sinon (le nombre de gestionnaire assigné > 1)
				ELSE
					-- Cas 5.4 : multi-groupe / 1 seul domaine + autre / multi-gestionnaire
					-- => Le SIREN est associé au gestionnaire « Multi » en multi-groupes des 2 domaines (infos des niveaux de ce gestionnaire)
						-- recherche du gestionnaire fictif de type : MULTI_MULTI avec GG_INCLUS_1 <> '' and GG_INCLUS_2 = '' and GG_EXCLUS <> ''
					SELECT pug.code_user, pug.PRENOM, pug.NOM, pug.NIVEAU_1, pug.NIVEAU_2, pug.NIVEAU_3 
					INTO OUT_CODE_USER, OUT_PRENOM_USER, OUT_NOM_USER, OUT_NIVEAU_1, OUT_NIVEAU_2, OUT_NIVEAU_3
					FROM param_utilisateur_gestionnaire pug 
					WHERE pug.TYPE = 'MULTI_MULTI'
					AND pug.GG_INCLUS_1 = (VAR_INCLUS_1 collate latin1_general_ci) and pug.GG_INCLUS_2 = '' and pug.GG_EXCLUS = (VAR_EXCLUS collate latin1_general_ci);
					
				-- end (nombre de gestionnaire assigné)
				END IF;
			
			-- sinon (groupes sur 2 + autre)
			ELSE
				-- Cas 7.1 : reste les cas de groupes sur 2 domaines + autres
				-- !!!!!! cas non prévu mais existant
					-- => A qui associer ce SIREN ???
				SELECT concat(pug.code_user, '_2DA'), pug.PRENOM, pug.NOM, pug.NIVEAU_1, pug.NIVEAU_2, pug.NIVEAU_3 
				INTO OUT_CODE_USER, OUT_PRENOM_USER, OUT_NOM_USER, OUT_NIVEAU_1, OUT_NIVEAU_2, OUT_NIVEAU_3
				FROM param_utilisateur_gestionnaire pug 
				WHERE pug.TYPE = 'MULTI_MULTI'
				AND pug.GG_INCLUS_1 <> '' and pug.GG_INCLUS_2 <> '' and pug.GG_EXCLUS = '';
				-- !!!!!!!!!!!! A voir si on garde ce gest qui est le gest multi 2 domaines ou si on fait un autre gest fictif !!!!!!!!!!!
				
			
			-- end (groupe sur aucun domaine ou 1 seul domaine + autre ou 2 domaines + autre)
			END IF;
			
		-- end (groupes uniquement sur les 2 domaines ou non)
		END IF;
		
	-- end (groupes mono-domaine ou non)
	END IF;
	
-- end (nb de groupes de gestion)
END IF;
-- Select pour renvoyer la ligne avec les données au traitements java
SELECT OUT_CODE_USER, OUT_NOM_USER, OUT_PRENOM_USER, OUT_NIVEAU_1, OUT_NIVEAU_2, OUT_NIVEAU_3;
END$$
DELIMITER ;


