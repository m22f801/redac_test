-- Calcul Montants Individus - batch 405 - utilisé dans V_Categorie_quittancement_salaires_a_inserer

DELIMITER $$
CREATE FUNCTION CalculMontantsIndividus (nocat char(30), idPeriode bigint(20), identifiantRepertoire char(13),ntt char(40)) 
RETURNS decimal
DETERMINISTIC
BEGIN 
  DECLARE montant decimal(10,2);
  SET montant = 0;

    SELECT SUM(ba.MONTANT_COTISATION) INTO montant
	FROM  (((`INDIVIDU` `ind`
			left join `CONTRAT_TRAVAIL` `ct` ON ((`ind`.`ID_INDIVIDU` = `ct`.`ID_INDIVIDU`)))
			left join `AFFILIATION` `a` ON ((`ct`.`ID_CONTRAT_TRAVAIL` = `a`.`ID_CONTRAT_TRAVAIL`)))
			left join `BASE_ASSUJETTIE` `ba` ON ((`a`.`ID_AFFILIATION` = `ba`.`ID_AFFILIATION`)))
	WHERE (CASE WHEN identifiantRepertoire <> '' THEN convert(ind.IDENTIFIANT_REPERTOIRE using latin1)=(identifiantRepertoire collate latin1_general_ci) ELSE convert(ind.NTT using latin1)=(ntt collate latin1_general_ci) END)
	AND ba.ID_BASE_ASSUJETTIE IN (SELECT tcba.ID_BASE_ASSUJETTIE FROM TRANCHE_CATEGORIE_BASE_ASSUJETTIE tcba WHERE convert(tcba.NOCAT using latin1)=(nocat collate latin1_general_ci) AND tcba.ID_PERIODE =idPeriode);


  RETURN montant;
END$$
DELIMITER ;