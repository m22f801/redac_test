-- Création des procédures stockées REDAC
DROP PROCEDURE IF EXISTS Inserer_CATEGORIE_QUITTANCEMENT_INDIVIDU_EFFECTIF;

DELIMITER $$
CREATE PROCEDURE Inserer_CATEGORIE_QUITTANCEMENT_INDIVIDU_EFFECTIF(IN p_audit_nom_batch VARCHAR(20))
BEGIN
-- Calcul et insersion des nouveaux éléments des périodes à traiter
INSERT INTO CATEGORIE_QUITTANCEMENT_INDIVIDU
(
ID_PERIODE
,NOCAT
,INDIVIDU
,LICAT
,CONBCOT
,MT_COTISATION
,EFFECTIF_DEBUT
,EFFECTIF_FIN
,USER_CREATION
)
SELECT 
ID_PERIODE
,NOCAT
,INDIVIDU
,LICAT
,CONBCOT
,COTISATION
,EFFECTIF_DEBUT
,EFFECTIF_FIN
,p_audit_nom_batch
FROM V_Selection_categorie_quittancement_effectif;
END$$
DELIMITER ;

DROP PROCEDURE IF EXISTS Inserer_CATEGORIE_QUITTANCEMENT_INDIVIDU_SALAIRE;

DELIMITER $$
CREATE PROCEDURE Inserer_CATEGORIE_QUITTANCEMENT_INDIVIDU_SALAIRE(IN p_audit_nom_batch VARCHAR(20))
BEGIN
-- Calcul et insersion des nouveaux éléments des périodes à traiter
INSERT INTO CATEGORIE_QUITTANCEMENT_INDIVIDU (
ID_PERIODE
,NOCAT
,INDIVIDU
,LICAT
,CONBCOT
,MT_COTISATION
,EFFECTIF_DEBUT
,EFFECTIF_FIN
,USER_CREATION
)
select cai.*,
p_audit_nom_batch
FROM V_Categorie_quittancement_salaires_a_inserer cai;

END$$
DELIMITER ;

DROP PROCEDURE IF EXISTS Inserer_CATEGORIE_QUITTANCEMENT_INDIVIDU_SALAIRE_Pour_Periodes;

DELIMITER $$
CREATE PROCEDURE Inserer_CATEGORIE_QUITTANCEMENT_INDIVIDU_SALAIRE_Pour_Periodes(IN p_audit_nom_batch VARCHAR(20), IN p_liste_ids_periodes TEXT)
BEGIN
INSERT INTO CATEGORIE_QUITTANCEMENT_INDIVIDU (
ID_PERIODE
,NOCAT
,INDIVIDU
,LICAT
,CONBCOT
,MT_COTISATION
,EFFECTIF_DEBUT
,EFFECTIF_FIN
,USER_CREATION
)
SELECT 
    p.ID_PERIODE,
    t.NOCAT,
    (CASE
        WHEN (`i`.`IDENTIFIANT_REPERTOIRE` = '') THEN `i`.`NTT`
        ELSE `i`.`IDENTIFIANT_REPERTOIRE`
    END),
    t.LICAT,
    t.CONBCOT,
    COALESCE(CALCULMONTANTSINDIVIDUS(`t`.`NOCAT`,
                    `p`.`ID_PERIODE`,
                    `i`.`IDENTIFIANT_REPERTOIRE`,
                    `i`.`NTT`),
            0),
    NULL,
    NULL,
    p_audit_nom_batch
FROM
    TARIFS t
        INNER JOIN
    PERIODES_RECUES p ON t.NOCO = p.NUMERO_CONTRAT
        INNER JOIN
    RATTACHEMENT_DECLARATIONS_RECUES r ON r.ID_PERIODE = p.ID_PERIODE
        INNER JOIN
    INDIVIDU i ON i.ID_ADH_ETAB_MOIS = r.ID_ADH_ETAB_MOIS
        INNER JOIN
    V_Selection_periode_mocalcot_salaires s ON s.ID_PERIODE = p.ID_PERIODE
        AND s.TYPE_CONSO = 'INDIV'
        LEFT JOIN
    (SELECT 
        ba.ID_BASE_ASSUJETTIE,
            (CASE
                WHEN (`ind`.`IDENTIFIANT_REPERTOIRE` = '') THEN `ind`.`NTT`
                ELSE `ind`.`IDENTIFIANT_REPERTOIRE`
            END) identifiant,
            SUM(ba.MONTANT_COTISATION) montant_cotisation
    FROM
        BASE_ASSUJETTIE ba
    LEFT JOIN AFFILIATION a ON a.id_affiliation = ba.id_affiliation
    LEFT JOIN CONTRAT_TRAVAIL ct ON ct.ID_CONTRAT_TRAVAIL = a.ID_CONTRAT_TRAVAIL
    LEFT JOIN INDIVIDU ind ON ind.ID_INDIVIDU = ct.ID_INDIVIDU
    GROUP BY identifiant) montant ON (CASE
        WHEN (`i`.`IDENTIFIANT_REPERTOIRE` = '') THEN `i`.`NTT`
        ELSE `i`.`IDENTIFIANT_REPERTOIRE`
    END) = montant.identifiant
        AND ID_BASE_ASSUJETTIE IN (SELECT 
            tcba.ID_BASE_ASSUJETTIE
        FROM
            TRANCHE_CATEGORIE_BASE_ASSUJETTIE tcba
        WHERE
            tcba.ID_PERIODE = p.ID_PERIODE
                AND tcba.NOCAT = t.NOCAT)
WHERE
    ((`t`.`NOCO` = `p`.`NUMERO_CONTRAT`)
        AND (`t`.`DT_DEBUT_SIT` <= `p`.`DATE_DEBUT_PERIODE`)
        AND ((COALESCE(`t`.`DT_FIN_SIT`, 99999999) >= `p`.`DATE_DEBUT_PERIODE`)
        OR ((CASE
        WHEN (`t`.`DT_FIN_SIT` = 0) THEN 99999999
        ELSE `t`.`DT_FIN_SIT`
    END) >= `p`.`DATE_DEBUT_PERIODE`)))
        AND CONCAT(t.NOCAT, p.ID_PERIODE) IN (SELECT DISTINCT
            CONCAT(t.NOCAT, p.ID_PERIODE)
        FROM
            TARIFS t
                INNER JOIN
            PERIODES_RECUES p ON t.NOCO = p.NUMERO_CONTRAT
        WHERE
            t.NOCO = p.NUMERO_CONTRAT
                AND ((`t`.`NOCO` = `p`.`NUMERO_CONTRAT`)
                AND (`t`.`DT_DEBUT_SIT` <= `p`.`DATE_DEBUT_PERIODE`)
                AND ((COALESCE(`t`.`DT_FIN_SIT`, 99999999) >= `p`.`DATE_DEBUT_PERIODE`)
                OR ((CASE
                WHEN (`t`.`DT_FIN_SIT` = 0) THEN 99999999
                ELSE `t`.`DT_FIN_SIT`
            END) >= `p`.`DATE_DEBUT_PERIODE`))))
        AND CONCAT(i.IDENTIFIANT_REPERTOIRE, i.NTT) IN (SELECT DISTINCT
            CONCAT(i.IDENTIFIANT_REPERTOIRE, i.NTT)
        FROM
            INDIVIDU i
                LEFT JOIN
            ADHESION_ETABLISSEMENT_MOIS adh ON adh.ID_ADH_ETAB_MOIS = i.ID_ADH_ETAB_MOIS
        WHERE
            adh.TMP_BATCH IS FALSE
                AND adh.ID_ADH_ETAB_MOIS IN (SELECT 
                    r.ID_ADH_ETAB_MOIS
                FROM
                    RATTACHEMENT_DECLARATIONS_RECUES r
                WHERE
                    r.ID_PERIODE = p.ID_PERIODE))
        AND FIND_IN_SET(p.ID_PERIODE, p_liste_ids_periodes) > 0 
UNION SELECT 
    p.ID_PERIODE,
    t.NOCAT,
    'TOUS',
    t.LICAT,
    t.CONBCOT,
    COALESCE((SELECT 
                    SUM(ba.MONTANT_COTISATION)
                FROM
                    BASE_ASSUJETTIE ba
                WHERE
                    ba.ID_BASE_ASSUJETTIE IN (SELECT 
                            tcba.ID_BASE_ASSUJETTIE
                        FROM
                            TRANCHE_CATEGORIE_BASE_ASSUJETTIE tcba
                        WHERE
                            tcba.ID_PERIODE = p.ID_PERIODE
                                AND tcba.NOCAT = t.nocat)),
            0) AS MONTANT_COTISATION,
    NULL,
    NULL,
    p_audit_nom_batch
FROM
    TARIFS t
        INNER JOIN
    PERIODES_RECUES p ON t.NOCO = p.NUMERO_CONTRAT
        INNER JOIN
    V_Selection_periode_mocalcot_salaires s ON s.ID_PERIODE = p.ID_PERIODE
        AND s.TYPE_CONSO = 'TOUS'
WHERE
    ((`t`.`NOCO` = `p`.`NUMERO_CONTRAT`)
        AND (`t`.`DT_DEBUT_SIT` <= `p`.`DATE_DEBUT_PERIODE`)
        AND ((COALESCE(`t`.`DT_FIN_SIT`, 99999999) >= `p`.`DATE_DEBUT_PERIODE`)
        OR ((CASE
        WHEN (`t`.`DT_FIN_SIT` = 0) THEN 99999999
        ELSE `t`.`DT_FIN_SIT`
    END) >= `p`.`DATE_DEBUT_PERIODE`)))
        AND CONCAT(t.NOCAT, p.ID_PERIODE) IN (SELECT DISTINCT
            CONCAT(t.NOCAT, p.ID_PERIODE)
        FROM
            TARIFS t
                INNER JOIN
            PERIODES_RECUES p ON t.NOCO = p.NUMERO_CONTRAT
        WHERE
            t.NOCO = p.NUMERO_CONTRAT
                AND ((`t`.`NOCO` = `p`.`NUMERO_CONTRAT`)
                AND (`t`.`DT_DEBUT_SIT` <= `p`.`DATE_DEBUT_PERIODE`)
                AND ((COALESCE(`t`.`DT_FIN_SIT`, 99999999) >= `p`.`DATE_DEBUT_PERIODE`)
                OR ((CASE
                WHEN (`t`.`DT_FIN_SIT` = 0) THEN 99999999
                ELSE `t`.`DT_FIN_SIT`
            END) >= `p`.`DATE_DEBUT_PERIODE`)))
                AND p.ID_PERIODE = s.ID_PERIODE)
        AND FIND_IN_SET(p.ID_PERIODE, p_liste_ids_periodes) > 0;
END$$
DELIMITER ;

DROP PROCEDURE IF EXISTS Inserer_EFFECTIF_CATEGORIE_MVT;

DELIMITER $$
CREATE PROCEDURE Inserer_EFFECTIF_CATEGORIE_MVT(IN p_audit_nom_batch VARCHAR(20))
BEGIN
-- Calcul et insersion des nouveaux éléments des périodes à traiter
INSERT INTO EFFECTIF_CATEGORIE_MVT (ID_PERIODE, NOCAT, DATE_EFFET_MVT, NB_MOUVEMENT, USER_CREATION)
SELECT ID_PERIODE
, NOCAT
, DATE_CONSERVEE
, NB_MVT_FINAL
, p_audit_nom_batch
FROM V_Effectif_categorie_mouvement_data
WHERE concat(ID_PERIODE, NOCAT, DATE_CONSERVEE) NOT IN (SELECT concat(ID_PERIODE, NOCAT, DATE_SUPPR) FROM V_Effectif_categorie_mouvement_suppression1)
AND concat(ID_PERIODE, NOCAT, DATE_CONSERVEE, NB_MVT_FINAL) NOT IN (SELECT concat(ID_PERIODE, NOCAT, DATE_SUPPR, NB_MVT_INITIAL) FROM V_Effectif_categorie_mouvement_suppression2)
ORDER BY ID_PERIODE, NOCAT, DATE_CONSERVEE;

END$$
DELIMITER ;


DROP PROCEDURE IF EXISTS Inserer_TRANCHE_CATEGORIE;


DELIMITER $$
CREATE PROCEDURE Inserer_TRANCHE_CATEGORIE(IN p_audit_nom_batch VARCHAR(20))
BEGIN
-- Calcul et insersion des nouveaux éléments des périodes à traiter
INSERT INTO TRANCHE_CATEGORIE (ID_PERIODE, NOCAT, INDIVIDU, NUM_TRANCHE, LIB_TRANCHE, MT_TRANCHE, USER_CREATION)
select c.ID_PERIODE, c.NOCAT, c.INDIVIDU, p.NUM_TRANCHE, p.LIB_TRANCHE, 
coalesce(case WHEN (c.INDIVIDU = 'TOUS') THEN (SELECT SUM(tcba.MT_TRANCHE) 
FROM TRANCHE_CATEGORIE_BASE_ASSUJETTIE tcba 
WHERE tcba.ID_PERIODE = c.ID_PERIODE
AND tcba.NOCAT = c.NOCAT
AND tcba.NUM_TRANCHE = p.NUM_TRANCHE) 
ELSE (SELECT SUM(tcba.MT_TRANCHE) 
FROM TRANCHE_CATEGORIE_BASE_ASSUJETTIE tcba 
LEFT JOIN AFFILIATION a ON tcba.ID_AFFILIATION = a.ID_AFFILIATION
LEFT JOIN CONTRAT_TRAVAIL ct ON a.ID_CONTRAT_TRAVAIL = ct.ID_CONTRAT_TRAVAIL
LEFT JOIN INDIVIDU i ON ct.ID_INDIVIDU = i.ID_INDIVIDU 
WHERE tcba.ID_PERIODE = c.ID_PERIODE
AND tcba.NOCAT = c.NOCAT
AND tcba.NUM_TRANCHE = p.NUM_TRANCHE
AND (i.IDENTIFIANT_REPERTOIRE = c.INDIVIDU OR i.ntt = c.INDIVIDU)) END, 0), 
p_audit_nom_batch
from CATEGORIE_QUITTANCEMENT_INDIVIDU c
INNER JOIN (select distinct pnb.NUM_TRANCHE, pnb.LIB_TRANCHE, pnb.CONBCOT from PARAM_NATURE_BASE pnb) p ON p.CONBCOT = c.CONBCOT
INNER JOIN V_Selection_periode_mocalcot_salaires s on s.ID_PERIODE = c.ID_PERIODE;

END$$
DELIMITER ;

DROP PROCEDURE IF EXISTS Inserer_TRANCHE_CATEGORIE_BASE_ASSUJETTIE;

DELIMITER $$
CREATE PROCEDURE Inserer_TRANCHE_CATEGORIE_BASE_ASSUJETTIE(IN p_audit_nom_batch VARCHAR(20))
BEGIN
INSERT INTO TRANCHE_CATEGORIE_BASE_ASSUJETTIE 
(
ID_PERIODE
,ID_BASE_ASSUJETTIE
,MOIS_RATTACHEMENT
,ID_AFFILIATION
,DATE_DEB_RATTACHEMENT
,NUM_TRANCHE
,NOCAT
,LIB_TRANCHE
,MT_TRANCHE
,MT_COTISATION
,TMP_BATCH
,USER_CREATION
)
SELECT 
sel.ID_PERIODE
,sel.ID_BASE_ASSUJETTIE
,sel.MOIS_RATTACHEMENT
,sel.ID_AFFILIATION
,sel.DATE_DEB_RATTACHEMENT
,p.NUM_TRANCHE
,t.NOCAT
,p.LIB_TRANCHE
,
coalesce(
(SELECT SUM(c.MONTANT_COMPO_BASE_ASSUJ) 
FROM COMPOSANT_BASE_ASSUJETTIE c 
WHERE c.ID_BASE_ASSUJETTIE = sel.ID_BASE_ASSUJETTIE
AND c.TYPE_COMPO_BASE_ASSUJ IN (select pc.NUM_TCBA
                                                                             from PARAM_NATURE_BASE_COMPOSANTS pc 
                                                                             where p.CONBCOT=pc.CONBCOT
                                                                            and p.NUM_TRANCHE=pc.NUM_TRANCHE
                                                                             and p.TXCALCU_REMPLI=pc.TXCALCU_REMPLI
                                                                              and pc.mode_conso='S' and pc.TMP_BATCH IS FALSE)
AND c.TMP_BATCH IS FALSE
), 0)   MT_TRANCHE
, null MT_COTISATION
, sel.tmp_batch
, p_audit_nom_batch
from
V_Selection_base_assujettie sel
left join TARIFS t ON    sel.NUMERO_CONTRAT = t.NOCO 
                                  AND sel.NOCAT = t.nocat
                                  AND     t.DT_DEBUT_SIT <= sel.DATE_FIN_RATTACHEMENT 
                                  AND (coalesce(t.DT_FIN_SIT, 99999999) >= sel.DATE_FIN_RATTACHEMENT OR (CASE WHEN (t.DT_FIN_SIT = 0) THEN 99999999 ELSE t.DT_FIN_SIT END) >= sel.DATE_FIN_RATTACHEMENT)
left join PARAM_NATURE_BASE p on t.CONBCOT=p.CONBCOT
                                                                   and (case when coalesce(t.txcalcu,0)>0 then 'O' else 'N' end) =p.TXCALCU_REMPLI
where not isnull(t.NOCO) and not isnull(t.NOCAT) and not isnull(p.NUM_TRANCHE) 
order by ID_PERIODE, ID_BASE_ASSUJETTIE;
END$$
DELIMITER ;

DROP PROCEDURE IF EXISTS Inserer_TRANCHE_CATEGORIE_BASE_ASSUJETTIE_Pour_Periodes;

DELIMITER $$
CREATE PROCEDURE Inserer_TRANCHE_CATEGORIE_BASE_ASSUJETTIE_Pour_Periodes(IN p_audit_nom_batch VARCHAR(20), IN p_liste_ids_periodes TEXT)
BEGIN
INSERT INTO TRANCHE_CATEGORIE_BASE_ASSUJETTIE 
(
ID_PERIODE
,ID_BASE_ASSUJETTIE
,MOIS_RATTACHEMENT
,ID_AFFILIATION
,DATE_DEB_RATTACHEMENT
,NUM_TRANCHE
,NOCAT
,LIB_TRANCHE
,MT_TRANCHE
,MT_COTISATION
,TMP_BATCH
,USER_CREATION
)
SELECT 
    sel.ID_PERIODE,
    sel.ID_BASE_ASSUJETTIE,
    sel.MOIS_RATTACHEMENT,
    sel.ID_AFFILIATION,
    sel.DATE_DEB_RATTACHEMENT,
    p.NUM_TRANCHE,
    t.NOCAT,
    p.LIB_TRANCHE,
    (SELECT 
            SUM(c.MONTANT_COMPO_BASE_ASSUJ)
        FROM
            COMPOSANT_BASE_ASSUJETTIE c
        WHERE
            c.ID_BASE_ASSUJETTIE = sel.ID_BASE_ASSUJETTIE
                AND c.TYPE_COMPO_BASE_ASSUJ IN (SELECT 
                    pc.NUM_TCBA
                FROM
                    PARAM_NATURE_BASE_COMPOSANTS pc
                WHERE
                    p.CONBCOT = pc.CONBCOT
                        AND p.NUM_TRANCHE = pc.NUM_TRANCHE
                        AND p.TXCALCU_REMPLI = pc.TXCALCU_REMPLI
                        AND pc.mode_conso = 'S'
                        AND pc.TMP_BATCH IS FALSE)
                AND c.TMP_BATCH IS FALSE) MT_TRANCHE,
    NULL MT_COTISATION,
    sel.tmp_batch,
    p_audit_nom_batch
FROM
    V_Selection_base_assujettie sel
        LEFT JOIN
    TARIFS t ON sel.NUMERO_CONTRAT = t.NOCO
        AND sel.NOCAT = t.nocat
        AND t.DT_DEBUT_SIT <= sel.DATE_FIN_RATTACHEMENT
        AND (COALESCE(t.DT_FIN_SIT, 99999999) >= sel.DATE_FIN_RATTACHEMENT
        OR (CASE
        WHEN (t.DT_FIN_SIT = 0) THEN 99999999
        ELSE t.DT_FIN_SIT
    END) >= sel.DATE_FIN_RATTACHEMENT)

LEFT JOIN PARAM_NATURE_BASE p ON t.CONBCOT = p.CONBCOT
        AND (CASE
        WHEN COALESCE(t.txcalcu, 0) > 0 THEN 'O'
        ELSE 'N'
    END) = p.TXCALCU_REMPLI
WHERE
    NOT ISNULL(t.NOCO)
        AND NOT ISNULL(t.NOCAT)
        AND NOT ISNULL(p.NUM_TRANCHE)
        AND FIND_IN_SET(sel.ID_PERIODE, p_liste_ids_periodes) > 0
ORDER BY ID_PERIODE , ID_BASE_ASSUJETTIE;
END$$
DELIMITER ;

DROP PROCEDURE IF EXISTS Inserer_TRANCHE_CATEGORIE_Pour_Periodes;

DELIMITER $$
CREATE PROCEDURE Inserer_TRANCHE_CATEGORIE_Pour_Periodes(IN p_audit_nom_batch VARCHAR(20), IN p_liste_ids_periodes TEXT)
BEGIN
INSERT INTO TRANCHE_CATEGORIE (ID_PERIODE, NOCAT, INDIVIDU, NUM_TRANCHE, LIB_TRANCHE, MT_TRANCHE, USER_CREATION)
select c.ID_PERIODE, c.NOCAT, c.INDIVIDU, p.NUM_TRANCHE, p.LIB_TRANCHE, 
coalesce(case WHEN (c.INDIVIDU = 'TOUS') THEN (SELECT SUM(tcba.MT_TRANCHE) 
FROM TRANCHE_CATEGORIE_BASE_ASSUJETTIE tcba 
WHERE tcba.ID_PERIODE = c.ID_PERIODE
AND tcba.NOCAT = c.NOCAT
AND tcba.NUM_TRANCHE = p.NUM_TRANCHE) 
ELSE (SELECT SUM(tcba.MT_TRANCHE) 
FROM TRANCHE_CATEGORIE_BASE_ASSUJETTIE tcba 
LEFT JOIN AFFILIATION a ON tcba.ID_AFFILIATION = a.ID_AFFILIATION
LEFT JOIN CONTRAT_TRAVAIL ct ON a.ID_CONTRAT_TRAVAIL = ct.ID_CONTRAT_TRAVAIL
LEFT JOIN INDIVIDU i ON ct.ID_INDIVIDU = i.ID_INDIVIDU 
WHERE tcba.ID_PERIODE = c.ID_PERIODE
AND tcba.NOCAT = c.NOCAT
AND tcba.NUM_TRANCHE = p.NUM_TRANCHE
AND (i.IDENTIFIANT_REPERTOIRE = c.INDIVIDU OR i.ntt = c.INDIVIDU)) END, 0), 
p_audit_nom_batch
from CATEGORIE_QUITTANCEMENT_INDIVIDU c
INNER JOIN PARAM_NATURE_BASE p ON p.CONBCOT = c.CONBCOT
INNER JOIN V_Selection_periode_mocalcot_salaires s on s.ID_PERIODE = c.ID_PERIODE
WHERE FIND_IN_SET(c.ID_PERIODE, p_liste_ids_periodes) > 0;
END$$
DELIMITER ;

DROP PROCEDURE IF EXISTS Purger_elements_declaratifs_en_masse;

DELIMITER $$
CREATE PROCEDURE Purger_elements_declaratifs_en_masse(IN p_liste_ids_periodes TEXT)
BEGIN
	IF p_liste_ids_periodes = 'ALL' THEN BEGIN
		DELETE ecm FROM EFFECTIF_CATEGORIE_MVT ecm 
		INNER JOIN V_Selection_periode_mocalcot_effectifs s ON s.ID_PERIODE = ecm.ID_PERIODE; 

		DELETE cqi FROM CATEGORIE_QUITTANCEMENT_INDIVIDU cqi
		INNER JOIN V_Selection_periode_mocalcot_effectifs s ON s.ID_PERIODE = cqi.ID_PERIODE; 

		DELETE ecct FROM EFFECTIF_CATEGORIE_CONTRAT_TRAVAIL ecct
		INNER JOIN V_Selection_periode_mocalcot_effectifs s ON s.ID_PERIODE = ecct.ID_PERIODE; 


		DELETE tc FROM TRANCHE_CATEGORIE tc
		INNER JOIN V_Selection_periode_mocalcot_salaires s ON s.ID_PERIODE = tc.ID_PERIODE; 

		DELETE cqi FROM CATEGORIE_QUITTANCEMENT_INDIVIDU cqi
		INNER JOIN V_Selection_periode_mocalcot_salaires s ON s.ID_PERIODE = cqi.ID_PERIODE; 

		DELETE tcba FROM TRANCHE_CATEGORIE_BASE_ASSUJETTIE tcba
		INNER JOIN V_Selection_periode_mocalcot_salaires s ON s.ID_PERIODE = tcba.ID_PERIODE; 

		END;
	ELSE BEGIN
		DELETE FROM CATEGORIE_QUITTANCEMENT_INDIVIDU
		WHERE FIND_IN_SET(ID_PERIODE, p_liste_ids_periodes) > 0;

		DELETE FROM EFFECTIF_CATEGORIE_MVT
		WHERE FIND_IN_SET(ID_PERIODE, p_liste_ids_periodes) > 0;

		DELETE FROM TRANCHE_CATEGORIE
		WHERE FIND_IN_SET(ID_PERIODE, p_liste_ids_periodes) > 0;

		DELETE FROM EFFECTIF_CATEGORIE_CONTRAT_TRAVAIL
		WHERE FIND_IN_SET(ID_PERIODE, p_liste_ids_periodes) > 0;

		DELETE FROM TRANCHE_CATEGORIE_BASE_ASSUJETTIE
		WHERE FIND_IN_SET(ID_PERIODE, p_liste_ids_periodes) > 0;		
		END;
	END IF;
END$$
DELIMITER ;
