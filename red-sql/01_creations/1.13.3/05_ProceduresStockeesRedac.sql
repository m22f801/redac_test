-- Création des procédures stockées REDAC
DROP PROCEDURE IF EXISTS Inserer_CATEGORIE_QUITTANCEMENT_INDIVIDU_EFFECTIF;

DELIMITER $$
CREATE PROCEDURE Inserer_CATEGORIE_QUITTANCEMENT_INDIVIDU_EFFECTIF(IN p_audit_nom_batch VARCHAR(20))
BEGIN
-- Calcul et insersion des nouveaux éléments des périodes à traiter
INSERT INTO CATEGORIE_QUITTANCEMENT_INDIVIDU
(
ID_PERIODE
,NOCAT
,INDIVIDU
,LICAT
,CONBCOT
,MT_COTISATION
,EFFECTIF_DEBUT
,EFFECTIF_FIN
,ESTIMATION_COTISATION
,USER_CREATION
)
SELECT 
ID_PERIODE
,NOCAT
,INDIVIDU
,LICAT
,CONBCOT
,COTISATION
,EFFECTIF_DEBUT
,EFFECTIF_FIN
,ESTIMATION_COTISATION
,p_audit_nom_batch
FROM V_Selection_categorie_quittancement_effectif;
END$$
DELIMITER ;

DROP PROCEDURE IF EXISTS Inserer_CATEGORIE_QUITTANCEMENT_INDIVIDU_SALAIRE_Pour_Periodes;

DELIMITER $$
CREATE PROCEDURE Inserer_CATEGORIE_QUITTANCEMENT_INDIVIDU_SALAIRE_Pour_Periodes(IN p_audit_nom_batch VARCHAR(20), IN p_liste_ids_periodes MEDIUMTEXT)
BEGIN
																	   
INSERT INTO CATEGORIE_QUITTANCEMENT_INDIVIDU (
ID_PERIODE
,NOCAT
,INDIVIDU
,LICAT
,CONBCOT
,MT_COTISATION
,ESTIMATION_COTISATION
,EFFECTIF_DEBUT
,EFFECTIF_FIN
,USER_CREATION
)
SELECT ID_PERIODE, NOCAT, INDIVIDU, LICAT, CONBCOT, sum(MONTANT_COTISATION), SUM(ESTIMATION_COTISATION) AS ESTIMATION_COTISATION, CHAMP_NULL_1, CHAMP_NULL_2, 'RR405' -- p_audit_nom_batch
FROM (

	SELECT 
        p.ID_PERIODE AS ID_PERIODE,
        t.NOCAT AS NOCAT,
        (CASE
            WHEN (i.IDENTIFIANT_REPERTOIRE = '') THEN i.NTT
            ELSE i.IDENTIFIANT_REPERTOIRE
        END) AS INDIVIDU,
        t.LICAT AS LICAT,
        t.CONBCOT AS CONBCOT,
		COALESCE(s.montant_cot, 0) AS MONTANT_COTISATION,
		SUM(tcba.ESTIMATION_COTISATION) AS ESTIMATION_COTISATION,
		NULL AS CHAMP_NULL_1,
        NULL AS CHAMP_NULL_2,
        p_audit_nom_batch
    FROM
        TARIFS t
            INNER JOIN
        PERIODES_RECUES p ON t.NOCO = p.NUMERO_CONTRAT
            INNER JOIN
        RATTACHEMENT_DECLARATIONS_RECUES r ON r.ID_PERIODE = p.ID_PERIODE
            INNER JOIN
        INDIVIDU i ON i.ID_ADH_ETAB_MOIS = r.ID_ADH_ETAB_MOIS
            AND i.TMP_BATCH IS FALSE
            INNER JOIN
        CONTRATS c ON c.NOCO = t.NOCO
            INNER JOIN
        PARAM_FAMILLE_CONTRAT par ON (par.NOFAM = c.NOFAM AND par.NMGRPGES = c.NMGRPGES)
            LEFT JOIN
        CONTRAT_TRAVAIL ct ON ct.ID_INDIVIDU = i.ID_INDIVIDU
            INNER JOIN
        AFFILIATION a ON  (`ct`.`ID_CONTRAT_TRAVAIL` = `a`.`ID_CONTRAT_TRAVAIL`)
        AND ISNULL(IF(`a`.`CODE_POPULATION` = '', NULL, a.CODE_POPULATION)) = 1
			LEFT JOIN
        BASE_ASSUJETTIE ba ON a.ID_AFFILIATION = ba.ID_AFFILIATION
			LEFT JOIN tranche_categorie_base_assujettie tcba ON (tcba.NOCAT = t.NOCAT
                    AND tcba.ID_PERIODE = p.ID_PERIODE
                    AND tcba.ID_BASE_ASSUJETTIE = ba.ID_BASE_ASSUJETTIE)
         LEFT JOIN
         	(SELECT 
                        SUM(ba.MONTANT_COTISATION) as montant_cot,  
								(CASE
            					WHEN (ind.IDENTIFIANT_REPERTOIRE = '') THEN ind.NTT
            					ELSE ind.IDENTIFIANT_REPERTOIRE
        						END) as id_indiv,
        						af.CODE_POPULATION,
        						rdr.id_periode
                    FROM
                        RATTACHEMENT_DECLARATIONS_RECUES rdr
									INNER JOIN 
								INDIVIDU ind ON rdr.ID_ADH_ETAB_MOIS = ind.ID_ADH_ETAB_MOIS
                            INNER JOIN
                        CONTRAT_TRAVAIL ct ON ind.ID_INDIVIDU = ct.ID_INDIVIDU
                            INNER JOIN
                        AFFILIATION af ON ct.ID_CONTRAT_TRAVAIL = af.ID_CONTRAT_TRAVAIL
                            INNER JOIN
                        BASE_ASSUJETTIE ba ON af.ID_AFFILIATION = ba.ID_AFFILIATION
                    WHERE
                    		FIND_IN_SET(rdr.ID_PERIODE, p_liste_ids_periodes) > 0
                     GROUP BY 
								(CASE
            					WHEN (ind.IDENTIFIANT_REPERTOIRE = '') THEN ind.NTT
            					ELSE ind.IDENTIFIANT_REPERTOIRE
        						END),
        						af.CODE_POPULATION, rdr.id_periode
				) as s on s.id_indiv = (CASE
            					WHEN (i.IDENTIFIANT_REPERTOIRE = '') THEN i.NTT
            					ELSE i.IDENTIFIANT_REPERTOIRE
        						END) and s.CODE_POPULATION = a.code_population and s.id_periode = p.id_periode
    WHERE
		  
        p.RECONSOLIDER = 'O' AND t.NATURE_CONTRAT = 2
            AND (t.TMP_BATCH IS FALSE)
            AND par.TYPE_CONSO_SALAIRE = 'INDIV'
            AND (par.TMP_BATCH IS FALSE)
            AND (c.TMP_BATCH IS FALSE)
            AND (c.DT_DEBUT_SIT <= p.DATE_DEBUT_PERIODE)
            AND (COALESCE(c.DT_FIN_SIT, 99999999) >= p.DATE_DEBUT_PERIODE)
            AND ((t.NOCO = p.NUMERO_CONTRAT)
            AND (t.DT_DEBUT_SIT <= p.DATE_DEBUT_PERIODE)
            AND (COALESCE(t.DT_FIN_SIT, 99999999) >= p.DATE_DEBUT_PERIODE)
            AND r.ID_ADH_ETAB_MOIS IN (SELECT 
                adh.ID_ADH_ETAB_MOIS
            FROM
                ADHESION_ETABLISSEMENT_MOIS adh
            WHERE
                adh.ID_ADH_ETAB_MOIS = r.ID_ADH_ETAB_MOIS
                    AND adh.TMP_BATCH IS FALSE))
					AND FIND_IN_SET(p.ID_PERIODE, p_liste_ids_periodes) > 0
    GROUP BY p.ID_PERIODE, INDIVIDU, COALESCE(
        IF( (a.CODE_POPULATION = ''),NULL,a.CODE_POPULATION),
            (SELECT t.NOCAT
             FROM tarifs t
             WHERE((t.NOCO = p.NUMERO_CONTRAT)
             AND (t.DT_DEBUT_SIT <= p.DATE_DEBUT_PERIODE)
             AND ((COALESCE(t.DT_FIN_SIT, 99999999) >= p.DATE_DEBUT_PERIODE)
             ))
             ORDER BY t.DT_DEBUT_SIT
             LIMIT 1))
	UNION ALL
	SELECT 
        p.ID_PERIODE AS ID_PERIODE,
        a.CODE_POPULATION AS NOCAT,
        (CASE
            WHEN (i.IDENTIFIANT_REPERTOIRE = '') THEN i.NTT
            ELSE i.IDENTIFIANT_REPERTOIRE
        END) AS INDIVIDU,
        t.LICAT AS LICAT,
        t.CONBCOT AS CONBCOT,
		COALESCE(s.montant_cot, 0) AS MONTANT_COTISATION,
		SUM(tcba.ESTIMATION_COTISATION) AS ESTIMATION_COTISATION,
        NULL AS CHAMP_NULL_1,
        NULL AS CHAMP_NULL_2,
        p_audit_nom_batch
    FROM
        TARIFS t
            INNER JOIN
        PERIODES_RECUES p ON t.NOCO = p.NUMERO_CONTRAT
            INNER JOIN
        RATTACHEMENT_DECLARATIONS_RECUES r ON r.ID_PERIODE = p.ID_PERIODE
            INNER JOIN
        INDIVIDU i ON i.ID_ADH_ETAB_MOIS = r.ID_ADH_ETAB_MOIS
            AND i.TMP_BATCH IS FALSE
            INNER JOIN
        CONTRATS c ON c.NOCO = t.NOCO
            INNER JOIN
        PARAM_FAMILLE_CONTRAT par ON (par.NOFAM = c.NOFAM AND par.NMGRPGES = c.NMGRPGES)
            LEFT JOIN
        CONTRAT_TRAVAIL ct ON ct.ID_INDIVIDU = i.ID_INDIVIDU
            INNER JOIN
        AFFILIATION a ON  ((`ct`.`ID_CONTRAT_TRAVAIL` = `a`.`ID_CONTRAT_TRAVAIL`)
                AND (`t`.`NOCAT` = `a`.`CODE_POPULATION`))
				AND ISNULL(IF(`a`.`CODE_POPULATION` = '', NULL, a.CODE_POPULATION)) = 0
			LEFT JOIN
        BASE_ASSUJETTIE ba ON a.ID_AFFILIATION = ba.ID_AFFILIATION
			LEFT JOIN tranche_categorie_base_assujettie tcba ON (tcba.NOCAT = t.NOCAT
                    AND tcba.ID_PERIODE = p.ID_PERIODE
                    AND tcba.ID_BASE_ASSUJETTIE = ba.ID_BASE_ASSUJETTIE)
         LEFT JOIN
         	(SELECT 
                        SUM(ba.MONTANT_COTISATION) as montant_cot,  
								(CASE
            					WHEN (ind.IDENTIFIANT_REPERTOIRE = '') THEN ind.NTT
            					ELSE ind.IDENTIFIANT_REPERTOIRE
        						END) as id_indiv,
        						af.CODE_POPULATION,
        						rdr.id_periode
                    FROM
                        RATTACHEMENT_DECLARATIONS_RECUES rdr
									INNER JOIN 
								INDIVIDU ind ON rdr.ID_ADH_ETAB_MOIS = ind.ID_ADH_ETAB_MOIS
                            INNER JOIN
                        CONTRAT_TRAVAIL ct ON ind.ID_INDIVIDU = ct.ID_INDIVIDU
                            INNER JOIN
                        AFFILIATION af ON ct.ID_CONTRAT_TRAVAIL = af.ID_CONTRAT_TRAVAIL
                            INNER JOIN
                        BASE_ASSUJETTIE ba ON af.ID_AFFILIATION = ba.ID_AFFILIATION
                    WHERE
                    		FIND_IN_SET(rdr.ID_PERIODE, p_liste_ids_periodes) > 0
                     GROUP BY 
								(CASE
            					WHEN (ind.IDENTIFIANT_REPERTOIRE = '') THEN ind.NTT
            					ELSE ind.IDENTIFIANT_REPERTOIRE
        						END),
        						af.CODE_POPULATION, rdr.id_periode
				) as s on s.id_indiv = (CASE
            					WHEN (i.IDENTIFIANT_REPERTOIRE = '') THEN i.NTT
            					ELSE i.IDENTIFIANT_REPERTOIRE
        						END) and s.CODE_POPULATION = a.code_population and s.id_periode = p.id_periode
    WHERE
        p.RECONSOLIDER = 'O' AND t.NATURE_CONTRAT = 2
            AND (t.TMP_BATCH IS FALSE)
            AND par.TYPE_CONSO_SALAIRE = 'INDIV'
            AND (par.TMP_BATCH IS FALSE)
            AND (c.TMP_BATCH IS FALSE)
            AND (c.DT_DEBUT_SIT <= p.DATE_DEBUT_PERIODE)
            AND (COALESCE(c.DT_FIN_SIT, 99999999) >= p.DATE_DEBUT_PERIODE)
            AND ((t.NOCO = p.NUMERO_CONTRAT)
            AND (t.DT_DEBUT_SIT <= p.DATE_DEBUT_PERIODE)
            AND (COALESCE(t.DT_FIN_SIT, 99999999) >= p.DATE_DEBUT_PERIODE)
            AND r.ID_ADH_ETAB_MOIS IN (SELECT 
                adh.ID_ADH_ETAB_MOIS
            FROM
                ADHESION_ETABLISSEMENT_MOIS adh
            WHERE
                adh.ID_ADH_ETAB_MOIS = r.ID_ADH_ETAB_MOIS
                    AND adh.TMP_BATCH IS FALSE))
			AND FIND_IN_SET(p.ID_PERIODE, p_liste_ids_periodes) > 0
    GROUP BY p.ID_PERIODE, INDIVIDU, COALESCE(
        IF( (a.CODE_POPULATION = ''),NULL,a.CODE_POPULATION),
            (SELECT t.NOCAT
             FROM tarifs t
             WHERE (t.NOCO = p.NUMERO_CONTRAT
             AND t.DT_DEBUT_SIT <= p.DATE_DEBUT_PERIODE
             AND COALESCE(t.DT_FIN_SIT, 99999999) >= p.DATE_DEBUT_PERIODE)
             ORDER BY t.DT_DEBUT_SIT
             LIMIT 1))
									

	) AS U GROUP BY ID_PERIODE, INDIVIDU, NOCAT    

    UNION SELECT 
        p.ID_PERIODE AS ID_PERIODE,
        t.NOCAT AS NOCAT,
        'TOUS' AS INDIVIDU,
        t.LICAT AS LICAT,
        t.CONBCOT AS CONBCOT,
        COALESCE(s.montant_cot, 0) AS MONTANT_COTISATION,
        (SELECT 
                SUM(tcba.ESTIMATION_COTISATION)
            FROM
                TRANCHE_CATEGORIE_BASE_ASSUJETTIE tcba
            WHERE
                ((tcba.ID_PERIODE = p.ID_PERIODE)
                    AND (tcba.NOCAT = t.NOCAT))) AS ESTIMATION_COTISATION,
        NULL AS CHAMP_NULL_1,
        NULL AS CHAMP_NULL_2,
		p_audit_nom_batch
    FROM
        (TARIFS t
        JOIN PERIODES_RECUES p ON ((t.NOCO = p.NUMERO_CONTRAT)))
            INNER JOIN
        CONTRATS c ON c.NOCO = t.NOCO
            INNER JOIN
        PARAM_FAMILLE_CONTRAT par ON (par.NOFAM = c.NOFAM AND par.NMGRPGES = c.NMGRPGES)
                 LEFT JOIN
         	(SELECT 
				SUM(ba.MONTANT_COTISATION) as montant_cot,  
						COALESCE(IF( (af.CODE_POPULATION = ''),NULL,af.CODE_POPULATION),
					   (SELECT t.NOCAT
						FROM tarifs t
						WHERE t.NOCO = per.NUMERO_CONTRAT
						AND t.DT_DEBUT_SIT <= per.DATE_DEBUT_PERIODE
						AND COALESCE(t.DT_FIN_SIT, 99999999) >= per.DATE_DEBUT_PERIODE
						ORDER BY t.DT_DEBUT_SIT
						LIMIT 1)) as CODE_POPULATION, rdr.id_periode
				FROM
					RATTACHEMENT_DECLARATIONS_RECUES rdr
					INNER JOIN 
						INDIVIDU ind ON rdr.ID_ADH_ETAB_MOIS = ind.ID_ADH_ETAB_MOIS
					INNER JOIN
						CONTRAT_TRAVAIL ct ON ind.ID_INDIVIDU = ct.ID_INDIVIDU
					INNER JOIN
						AFFILIATION af ON ct.ID_CONTRAT_TRAVAIL = af.ID_CONTRAT_TRAVAIL
					INNER JOIN
						BASE_ASSUJETTIE ba ON af.ID_AFFILIATION = ba.ID_AFFILIATION
					INNER JOIN
					periodes_recues per ON per.ID_PERIODE = rdr.ID_PERIODE
				WHERE
					FIND_IN_SET(rdr.ID_PERIODE, p_liste_ids_periodes) > 0
				GROUP BY COALESCE(IF( (af.CODE_POPULATION = ''),NULL,af.CODE_POPULATION),
									   (SELECT t.NOCAT
										FROM tarifs t
										WHERE t.NOCO = per.NUMERO_CONTRAT
										AND t.DT_DEBUT_SIT <= per.DATE_DEBUT_PERIODE
										AND COALESCE(t.DT_FIN_SIT, 99999999) >= per.DATE_DEBUT_PERIODE
										ORDER BY t.DT_DEBUT_SIT
										LIMIT 1)), rdr.id_periode
				) as s on 
						COALESCE(IF( (s.CODE_POPULATION = ''),NULL,s.CODE_POPULATION),
			            (SELECT t.NOCAT
			             FROM tarifs t
			             WHERE t.NOCO = p.NUMERO_CONTRAT
			             AND t.DT_DEBUT_SIT <= p.DATE_DEBUT_PERIODE
			             AND COALESCE(t.DT_FIN_SIT, 99999999) >= p.DATE_DEBUT_PERIODE
			             ORDER BY t.DT_DEBUT_SIT
			             LIMIT 1)) = t.NOCAT 
				 and s.id_periode = p.id_periode
        
    WHERE
        (t.NOCO = p.NUMERO_CONTRAT
            AND t.DT_DEBUT_SIT <= p.DATE_DEBUT_PERIODE
            AND COALESCE(t.DT_FIN_SIT, 99999999) >= p.DATE_DEBUT_PERIODE
            AND p.RECONSOLIDER = 'O'
            AND t.NATURE_CONTRAT = 2
            AND t.TMP_BATCH IS FALSE
            AND par.TYPE_CONSO_SALAIRE = 'TOUS'
            AND par.TMP_BATCH IS FALSE
            AND c.TMP_BATCH IS FALSE
            AND c.DT_DEBUT_SIT <= p.DATE_DEBUT_PERIODE
            AND COALESCE(c.DT_FIN_SIT, 99999999) >= p.DATE_DEBUT_PERIODE)
        AND FIND_IN_SET(p.ID_PERIODE, p_liste_ids_periodes) > 0
		  ;
END$$
DELIMITER ;

DROP PROCEDURE IF EXISTS Inserer_EFFECTIF_CATEGORIE_MVT;

DELIMITER $$
CREATE PROCEDURE Inserer_EFFECTIF_CATEGORIE_MVT(IN p_audit_nom_batch VARCHAR(20))
BEGIN
-- Calcul et insersion des nouveaux éléments des périodes à traiter
INSERT INTO EFFECTIF_CATEGORIE_MVT (ID_PERIODE, NOCAT, DATE_EFFET_MVT, NB_MOUVEMENT, USER_CREATION)
SELECT ID_PERIODE
, NOCAT
, DATE_CONSERVEE
, NB_MVT_FINAL
, p_audit_nom_batch
FROM V_Effectif_categorie_mouvement_data
WHERE concat(ID_PERIODE, NOCAT, DATE_CONSERVEE) NOT IN (SELECT concat(ID_PERIODE, NOCAT, DATE_SUPPR) FROM V_Effectif_categorie_mouvement_suppression1)
AND concat(ID_PERIODE, NOCAT, DATE_CONSERVEE, NB_MVT_FINAL) NOT IN (SELECT concat(ID_PERIODE, NOCAT, DATE_SUPPR, NB_MVT_INITIAL) FROM V_Effectif_categorie_mouvement_suppression2)
ORDER BY ID_PERIODE, NOCAT, DATE_CONSERVEE;

END$$
DELIMITER ;

DROP PROCEDURE IF EXISTS Inserer_TRANCHE_CATEGORIE_BASE_ASSUJETTIE_Pour_Periodes;

DELIMITER $$
CREATE PROCEDURE Inserer_TRANCHE_CATEGORIE_BASE_ASSUJETTIE_Pour_Periodes(IN p_audit_nom_batch VARCHAR(20), IN p_liste_ids_periodes MEDIUMTEXT)
BEGIN
INSERT INTO TRANCHE_CATEGORIE_BASE_ASSUJETTIE 
(
ID_PERIODE
,ID_BASE_ASSUJETTIE
,MOIS_RATTACHEMENT
,ID_AFFILIATION
,DATE_DEB_RATTACHEMENT
,NUM_TRANCHE
,NOCAT
,LIB_TRANCHE
,MT_TRANCHE
,ESTIMATION_COTISATION
,TMP_BATCH
,USER_CREATION
)
SELECT 
    sel.ID_PERIODE,
    sel.ID_BASE_ASSUJETTIE,
    sel.MOIS_RATTACHEMENT,
    sel.ID_AFFILIATION,
    sel.DATE_DEB_RATTACHEMENT,
    p.NUM_TRANCHE,
    t.NOCAT,
    p.LIB_TRANCHE,
    @MT_TRANCHE_SAVE:=COALESCE((SELECT 
                    SUM(c.MONTANT_COMPO_BASE_ASSUJ)
                FROM
                    COMPOSANT_BASE_ASSUJETTIE c
                WHERE
                    c.ID_BASE_ASSUJETTIE = sel.ID_BASE_ASSUJETTIE
                        AND c.TYPE_COMPO_BASE_ASSUJ IN (SELECT 
                            pc.NUM_TCBA
                        FROM
                            PARAM_NATURE_BASE_COMPOSANTS pc
                        WHERE
                            p.CONBCOT = pc.CONBCOT
                                AND p.NUM_TRANCHE = pc.NUM_TRANCHE
                                AND p.TXCALCU_REMPLI = pc.TXCALCU_REMPLI
                                AND pc.mode_conso = 'S'
                                AND pc.TMP_BATCH IS FALSE)
                        AND c.TMP_BATCH IS FALSE),
            0) AS MT_TRANCHE,
    CAST((@MT_TRANCHE_SAVE * 
    ((CASE 
    WHEN p.NUM_TRANCHE = 1 THEN t.TXBASE1
    WHEN p.NUM_TRANCHE = 2 THEN t.TXBASE2
    WHEN p.NUM_TRANCHE = 3 THEN t.TXBASE3
    WHEN p.NUM_TRANCHE = 4 THEN t.TXBASE4
    END) / 1000000 )
    * (c.TXAPPCOT / 10000)) AS DECIMAL(15,2)) AS ESTIMATION_COTISATION,
    sel.tmp_batch,
    p_audit_nom_batch
FROM
    V_Selection_base_assujettie sel
        LEFT JOIN
    TARIFS t ON sel.NUMERO_CONTRAT = t.NOCO
        AND sel.NOCAT = t.nocat
        AND t.DT_DEBUT_SIT <= sel.DATE_FIN_RATTACHEMENT
        AND (COALESCE(t.DT_FIN_SIT, 99999999) >= sel.DATE_FIN_RATTACHEMENT
        OR (CASE
        WHEN (t.DT_FIN_SIT = 0) THEN 99999999
        ELSE t.DT_FIN_SIT
    END) >= sel.DATE_FIN_RATTACHEMENT)
        LEFT JOIN
    PARAM_NATURE_BASE p ON t.CONBCOT = p.CONBCOT
        AND (CASE
        WHEN COALESCE(t.txcalcu, 0) > 0 THEN 'O'
        ELSE 'N'
    END) = p.TXCALCU_REMPLI
        INNER JOIN
        CONTRATS c ON (c.TMP_BATCH IS FALSE
	AND c.NOCO = sel.NUMERO_CONTRAT
    AND c.DT_DEBUT_SIT <= sel.DATE_DEBUT_PERIODE
        AND COALESCE(c.DT_FIN_SIT, 99999999) >= sel.DATE_DEBUT_PERIODE)
WHERE
    NOT ISNULL(t.NOCO)
        AND NOT ISNULL(t.NOCAT)
        AND NOT ISNULL(p.NUM_TRANCHE)
        AND FIND_IN_SET(sel.ID_PERIODE, p_liste_ids_periodes) > 0
ORDER BY ID_PERIODE , ID_BASE_ASSUJETTIE;
END$$
DELIMITER ;

DROP PROCEDURE IF EXISTS Inserer_TRANCHE_CATEGORIE_Pour_Periodes;

DELIMITER $$
CREATE PROCEDURE Inserer_TRANCHE_CATEGORIE_Pour_Periodes(IN p_audit_nom_batch VARCHAR(20), IN p_liste_ids_periodes MEDIUMTEXT)
BEGIN
-- Calcul et insersion des nouveaux éléments des périodes à traiter
INSERT INTO TRANCHE_CATEGORIE (ID_PERIODE, NOCAT, INDIVIDU, NUM_TRANCHE, LIB_TRANCHE, MT_TRANCHE, USER_CREATION)
select c.ID_PERIODE, c.NOCAT, c.INDIVIDU, p.NUM_TRANCHE, p.LIB_TRANCHE, 
coalesce(case WHEN (c.INDIVIDU = 'TOUS') THEN (SELECT SUM(tcba.MT_TRANCHE) 
FROM TRANCHE_CATEGORIE_BASE_ASSUJETTIE tcba 
WHERE tcba.ID_PERIODE = c.ID_PERIODE
AND tcba.NOCAT = c.NOCAT
AND tcba.NUM_TRANCHE = p.NUM_TRANCHE) 
ELSE (SELECT SUM(tcba.MT_TRANCHE) 
FROM TRANCHE_CATEGORIE_BASE_ASSUJETTIE tcba 
LEFT JOIN AFFILIATION a ON tcba.ID_AFFILIATION = a.ID_AFFILIATION
LEFT JOIN CONTRAT_TRAVAIL ct ON a.ID_CONTRAT_TRAVAIL = ct.ID_CONTRAT_TRAVAIL
LEFT JOIN INDIVIDU i ON ct.ID_INDIVIDU = i.ID_INDIVIDU 
WHERE tcba.ID_PERIODE = c.ID_PERIODE
AND tcba.NOCAT = c.NOCAT
AND tcba.NUM_TRANCHE = p.NUM_TRANCHE
AND (i.NTT  = c.INDIVIDU)) END, 0) +
coalesce(case WHEN (c.INDIVIDU = 'TOUS') THEN (null) 
ELSE (SELECT SUM(tcba.MT_TRANCHE) 
FROM TRANCHE_CATEGORIE_BASE_ASSUJETTIE tcba 
LEFT JOIN AFFILIATION a ON tcba.ID_AFFILIATION = a.ID_AFFILIATION
LEFT JOIN CONTRAT_TRAVAIL ct ON a.ID_CONTRAT_TRAVAIL = ct.ID_CONTRAT_TRAVAIL
LEFT JOIN INDIVIDU i ON ct.ID_INDIVIDU = i.ID_INDIVIDU 
WHERE tcba.ID_PERIODE = c.ID_PERIODE
AND tcba.NOCAT = c.NOCAT
AND tcba.NUM_TRANCHE = p.NUM_TRANCHE
AND (i.IDENTIFIANT_REPERTOIRE  = c.INDIVIDU)) END, 0) as MT_TRANCHE,
p_audit_nom_batch
from CATEGORIE_QUITTANCEMENT_INDIVIDU c
INNER JOIN (select distinct pnb.NUM_TRANCHE, pnb.LIB_TRANCHE, pnb.CONBCOT from PARAM_NATURE_BASE pnb) p ON p.CONBCOT = c.CONBCOT
INNER JOIN V_Selection_periode_nature_contrat_salaires s on s.ID_PERIODE = c.ID_PERIODE
WHERE FIND_IN_SET(c.ID_PERIODE, p_liste_ids_periodes) > 0;
END$$
DELIMITER ;

DROP PROCEDURE IF EXISTS Purger_elements_declaratifs_en_masse;

DELIMITER $$
CREATE PROCEDURE Purger_elements_declaratifs_en_masse(IN p_liste_ids_periodes MEDIUMTEXT)
BEGIN
	IF p_liste_ids_periodes = 'ALL' THEN BEGIN
		DELETE ecm FROM EFFECTIF_CATEGORIE_MVT ecm 
		INNER JOIN V_Selection_periode_nature_contrat_effectifs s ON s.ID_PERIODE = ecm.ID_PERIODE; 

		DELETE cqi FROM CATEGORIE_QUITTANCEMENT_INDIVIDU cqi
		INNER JOIN V_Selection_periode_nature_contrat_effectifs s ON s.ID_PERIODE = cqi.ID_PERIODE; 

		DELETE ecct FROM EFFECTIF_CATEGORIE_CONTRAT_TRAVAIL ecct
		INNER JOIN V_Selection_periode_nature_contrat_effectifs s ON s.ID_PERIODE = ecct.ID_PERIODE; 


		DELETE tc FROM TRANCHE_CATEGORIE tc
		INNER JOIN V_Selection_periode_nature_contrat_salaires s ON s.ID_PERIODE = tc.ID_PERIODE; 

		DELETE cqi FROM CATEGORIE_QUITTANCEMENT_INDIVIDU cqi
		INNER JOIN V_Selection_periode_nature_contrat_salaires s ON s.ID_PERIODE = cqi.ID_PERIODE; 

		DELETE tcba FROM TRANCHE_CATEGORIE_BASE_ASSUJETTIE tcba
		INNER JOIN V_Selection_periode_nature_contrat_salaires s ON s.ID_PERIODE = tcba.ID_PERIODE; 

		END;
	ELSE BEGIN
		DELETE FROM CATEGORIE_QUITTANCEMENT_INDIVIDU
		WHERE FIND_IN_SET(ID_PERIODE, p_liste_ids_periodes) > 0;

		DELETE FROM EFFECTIF_CATEGORIE_MVT
		WHERE FIND_IN_SET(ID_PERIODE, p_liste_ids_periodes) > 0;

		DELETE FROM TRANCHE_CATEGORIE
		WHERE FIND_IN_SET(ID_PERIODE, p_liste_ids_periodes) > 0;

		DELETE FROM EFFECTIF_CATEGORIE_CONTRAT_TRAVAIL
		WHERE FIND_IN_SET(ID_PERIODE, p_liste_ids_periodes) > 0;

		DELETE FROM TRANCHE_CATEGORIE_BASE_ASSUJETTIE
		WHERE FIND_IN_SET(ID_PERIODE, p_liste_ids_periodes) > 0;		
		END;
	END IF;
END$$
DELIMITER ;

DROP PROCEDURE IF EXISTS Creation_MessageControle_en_masse;
DELIMITER $$
CREATE PROCEDURE Creation_MessageControle_en_masse(IN p_liste_ids_periodes MEDIUMTEXT,IN message CHAR(100) ,IN idControle VARCHAR(30), IN param1 CHAR(50),IN param2 CHAR(50),IN param3 CHAR(50),IN param4 CHAR(50), IN niveau CHAR(10) , IN origine CHAR(6) , IN dateJour DECIMAL(8,0) , IN userCreation VARCHAR(100))
BEGIN
	
INSERT INTO message_controle(ID_PERIODE,ID_CONTROLE,ORIGINE,DATE_TRAITEMENT,NIVEAU_ALERTE,MESSAGE_UTILISATEUR,PARAM1,PARAM2,PARAM3,PARAM4,USER_CREATION)
SELECT p.ID_PERIODE,idControle,origine,dateJour,niveau,message,param1,param2,param3,param4,userCreation
FROM periodes_recues p WHERE FIND_IN_SET(p.ID_PERIODE, p_liste_ids_periodes) > 0 AND p.ID_PERIODE NOT IN(SELECT m.ID_PERIODE from message_controle m WHERE m.ID_CONTROLE=(idControle COLLATE latin1_general_ci)  AND m.ORIGINE=(origine COLLATE latin1_general_ci) AND FIND_IN_SET(m.ID_PERIODE, p_liste_ids_periodes) > 0 );

END$$
DELIMITER ;


DROP PROCEDURE IF EXISTS Creation_HistoriqueEtatPeriode_en_masse;
DELIMITER $$
CREATE PROCEDURE Creation_HistoriqueEtatPeriode_en_masse(IN p_liste_ids_periodes MEDIUMTEXT,IN datehms NUMERIC(14), IN etatPeriode CHAR(3), IN origine CHAR(4),
IN utilisateur CHAR(30), IN commentaire VARCHAR(1000), IN userCreation VARCHAR(20))
BEGIN
	
INSERT INTO HISTO_ETAT_PERIODE(ID_PERIODE, DATEHMS_CHANGEMENT, ETAT_PERIODE, ORIGINE, UTILISATEUR, COMMENTAIRE, USER_CREATION)
SELECT p.ID_PERIODE,datehms,etatPeriode,origine,utilisateur,commentaire,userCreation
FROM periodes_recues p WHERE FIND_IN_SET(ID_PERIODE, p_liste_ids_periodes) > 0;

END$$
DELIMITER ;


DROP PROCEDURE IF EXISTS Creation_HistoriqueCommentairePeriode_en_masse;
DELIMITER $$
CREATE PROCEDURE Creation_HistoriqueCommentairePeriode_en_masse(IN p_liste_ids_periodes MEDIUMTEXT,IN datehms NUMERIC(14), IN utilisateur CHAR(30), IN commentaire VARCHAR(1000), IN userCreation VARCHAR(20))
BEGIN
	
INSERT INTO HISTO_COMMENTAIRE_PERIODE(ID_PERIODE, UTILISATEUR, DATEHMS_SAISIE, COMMENTAIRE, USER_CREATION)
SELECT p.ID_PERIODE, utilisateur, datehms, commentaire, userCreation
FROM periodes_recues p WHERE FIND_IN_SET(ID_PERIODE, p_liste_ids_periodes) > 0;

END$$
DELIMITER ;


DROP PROCEDURE IF EXISTS Creation_HistoriqueAssignationPeriode_en_masse;
DELIMITER $$
CREATE PROCEDURE Creation_HistoriqueAssignationPeriode_en_masse(IN p_liste_ids_periodes MEDIUMTEXT,IN datehms NUMERIC(14), IN aTraiterPar CHAR(30), IN userCreation VARCHAR(20))
BEGIN
	
INSERT INTO HISTO_ASSIGNATION_PERIODE(ID_PERIODE, A_TRAITER_PAR, DATEHMS_ASSIGNE, USER_CREATION)
SELECT p.ID_PERIODE, aTraiterPar, datehms, userCreation
FROM periodes_recues p WHERE FIND_IN_SET(ID_PERIODE, p_liste_ids_periodes) > 0;

END$$
DELIMITER ;


DROP PROCEDURE IF EXISTS Creation_HistoriqueAttenteRetourEtpPeriode_en_masse;
DELIMITER $$
CREATE PROCEDURE Creation_HistoriqueAttenteRetourEtpPeriode_en_masse(IN p_liste_ids_periodes MEDIUMTEXT,IN datehms NUMERIC(14), IN utilisateur CHAR(30), IN action VARCHAR(20), IN userCreation VARCHAR(20))
BEGIN
	
INSERT INTO HISTO_ATTENTE_RETOUR_ETP_PERIODE(ID_PERIODE, ACTION, UTILISATEUR, DATEHMS_RETOUR, USER_CREATION)
SELECT p.ID_PERIODE, action, utilisateur, datehms, userCreation
FROM periodes_recues p WHERE FIND_IN_SET(ID_PERIODE, p_liste_ids_periodes) > 0
AND action collate latin1_general_ci != coalesce((SELECT h.action FROM histo_attente_retour_etp_periode h WHERE h.ID_PERIODE=p.ID_PERIODE ORDER BY h.DT_CREATION DESC LIMIT 1), 'NA');

END$$
DELIMITER ;


DROP PROCEDURE IF EXISTS Calcul_effectif_indiv_salaire_pour_periodes;
DELIMITER $$
CREATE PROCEDURE Calcul_effectif_indiv_salaire_pour_periodes(IN p_audit_nom_batch VARCHAR(20), in p_liste_ids_periodes MEDIUMTEXT)
BEGIN

DECLARE done INT DEFAULT FALSE;
DECLARE C_NO_CAT char(30);

DECLARE C_ID_PERIODE bigint(20);
DECLARE ID_PER_MAJ bigint(20);
DECLARE EFF_DEB_MAJ decimal(10,0);
DECLARE EFF_FIN_MAJ decimal(10,0);

DECLARE liste_nocat_unique CURSOR FOR
(SELECT cqi.ID_PERIODE,cqi.NOCAT
FROM categorie_quittancement_individu cqi INNER JOIN V_selection_periode_nature_contrat_salaires vpns ON (cqi.ID_PERIODE=vpns.ID_PERIODE)
WHERE cqi.individu='TOUS'
AND FIND_IN_SET(cqi.ID_PERIODE, p_liste_ids_periodes)
GROUP BY cqi.ID_PERIODE
HAVING COUNT(DISTINCT cqi.NOCAT)<=1);

DECLARE liste_nocat_non_unique CURSOR FOR
(SELECT cqi.ID_PERIODE,cqi.NOCAT
FROM categorie_quittancement_individu cqi INNER JOIN V_selection_periode_nature_contrat_salaires vpns ON (cqi.ID_PERIODE=vpns.ID_PERIODE)
WHERE cqi.individu='TOUS'
 AND FIND_IN_SET(cqi.ID_PERIODE, p_liste_ids_periodes)
 AND (SELECT COUNT(DISTINCT cqi2.NOCAT) FROM categorie_quittancement_individu cqi2 WHERE cqi2.ID_PERIODE=cqi.ID_PERIODE)>1
GROUP BY cqi.ID_PERIODE, cqi.NOCAT);

DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

OPEN liste_nocat_unique;
IF (Select FOUND_ROWS()) > 0 THEN
	nocat_uni: LOOP 
	FETCH liste_nocat_unique INTO C_ID_PERIODE, C_NO_CAT;
		IF done THEN 
			LEAVE nocat_uni;
		END IF;
		-- Calcul effectifs
		SELECT 
    COUNT(DISTINCT (CASE
            WHEN (i.IDENTIFIANT_REPERTOIRE = '') THEN i.NTT
            ELSE i.IDENTIFIANT_REPERTOIRE
        END)), p.ID_PERIODE INTO EFF_DEB_MAJ, ID_PER_MAJ
FROM
    periodes_recues p
        INNER JOIN
    rattachement_declarations_recues rda ON (p.ID_PERIODE = rda.ID_PERIODE)
        INNER JOIN
    adhesion_etablissement_mois adh ON (adh.ID_ADH_ETAB_MOIS = rda.ID_ADH_ETAB_MOIS)
        INNER JOIN
    individu i ON (i.ID_ADH_ETAB_MOIS = adh.ID_ADH_ETAB_MOIS)
		INNER JOIN
	contrat_travail ct ON (ct.ID_INDIVIDU = i.ID_INDIVIDU)
		INNER JOIN
	affiliation a ON (a.ID_CONTRAT_TRAVAIL = ct.ID_CONTRAT_TRAVAIL)
		INNER JOIN
	base_assujettie ba ON (ba.ID_AFFILIATION = a.ID_AFFILIATION AND ba.DATE_DEB_RATTACHEMENT = p.DATE_DEBUT_PERIODE)
WHERE
    p.ID_PERIODE = C_ID_PERIODE;
					
		SELECT 
    COUNT(DISTINCT (CASE
            WHEN (i.IDENTIFIANT_REPERTOIRE = '') THEN i.NTT
            ELSE i.IDENTIFIANT_REPERTOIRE
        END)) INTO EFF_FIN_MAJ
FROM
    periodes_recues p
        INNER JOIN
    rattachement_declarations_recues rda ON (p.ID_PERIODE = rda.ID_PERIODE)
        INNER JOIN
    adhesion_etablissement_mois adh ON (adh.ID_ADH_ETAB_MOIS = rda.ID_ADH_ETAB_MOIS)
        INNER JOIN
    individu i ON (i.ID_ADH_ETAB_MOIS = adh.ID_ADH_ETAB_MOIS)
		INNER JOIN
	contrat_travail ct ON (ct.ID_INDIVIDU = i.ID_INDIVIDU)
		INNER JOIN
	affiliation a ON (a.ID_CONTRAT_TRAVAIL = ct.ID_CONTRAT_TRAVAIL)
		INNER JOIN
	base_assujettie ba ON (ba.ID_AFFILIATION = a.ID_AFFILIATION AND ba.DATE_FIN_RATTACHEMENT = p.DATE_FIN_PERIODE)
WHERE
    p.ID_PERIODE = C_ID_PERIODE;
        
UPDATE categorie_quittancement_individu cqi3
SET 
	cqi3.EFFECTIF_DEBUT = EFF_DEB_MAJ,
	cqi3.EFFECTIF_FIN = EFF_FIN_MAJ
WHERE
	cqi3.ID_PERIODE = ID_PER_MAJ;
END LOOP;
END IF;
CLOSE liste_nocat_unique;


BEGIN
DECLARE done2 INT DEFAULT FALSE;
DECLARE CONTINUE HANDLER FOR NOT FOUND SET done2 = TRUE;

OPEN liste_nocat_non_unique;
IF (SELECT FOUND_ROWS()) > 0 THEN
	nocat_non_uni : LOOP 
	FETCH liste_nocat_non_unique INTO C_ID_PERIODE, C_NO_CAT;
		IF done2 THEN 
			LEAVE nocat_non_uni;
		END IF;
		-- Calcul effectifs 
		SELECT 
    COUNT(DISTINCT (CASE
            WHEN (i.IDENTIFIANT_REPERTOIRE = '') THEN i.NTT
            ELSE i.IDENTIFIANT_REPERTOIRE
        END)), p.ID_PERIODE INTO EFF_DEB_MAJ, ID_PER_MAJ
FROM
    periodes_recues p
        INNER JOIN
    rattachement_declarations_recues rda ON (p.ID_PERIODE = rda.ID_PERIODE)
        INNER JOIN
    adhesion_etablissement_mois adh ON (adh.ID_ADH_ETAB_MOIS = rda.ID_ADH_ETAB_MOIS)
        INNER JOIN
    individu i ON (i.ID_ADH_ETAB_MOIS = adh.ID_ADH_ETAB_MOIS)
		INNER JOIN
	contrat_travail ct ON (ct.ID_INDIVIDU = i.ID_INDIVIDU)
		INNER JOIN
	affiliation a ON (a.ID_CONTRAT_TRAVAIL = ct.ID_CONTRAT_TRAVAIL)
		INNER JOIN
	base_assujettie ba ON (ba.ID_AFFILIATION = a.ID_AFFILIATION AND ba.DATE_DEB_RATTACHEMENT = p.DATE_DEBUT_PERIODE)
WHERE
    p.ID_PERIODE = C_ID_PERIODE
	AND (C_NO_CAT collate latin1_general_ci) = (COALESCE(IF((a.CODE_POPULATION = ''),
                    NULL,
                    a.CODE_POPULATION),
                (SELECT 
                        t.NOCAT
                    FROM
                        tarifs t
                    WHERE
                        ((t.NOCO = p.NUMERO_CONTRAT)
                            AND (t.DT_DEBUT_SIT <= p.DATE_DEBUT_PERIODE)
                            AND ((COALESCE(t.DT_FIN_SIT, 99999999) >= p.DATE_DEBUT_PERIODE)
                            OR ((CASE
                            WHEN (t.DT_FIN_SIT = 0) THEN 99999999
                            ELSE t.DT_FIN_SIT
                        END) >= p.DATE_DEBUT_PERIODE)))
                    ORDER BY t.DT_DEBUT_SIT
                    LIMIT 1)));

SELECT 
    COUNT(DISTINCT (CASE
            WHEN (i.IDENTIFIANT_REPERTOIRE = '') THEN i.NTT
            ELSE i.IDENTIFIANT_REPERTOIRE
        END)) INTO EFF_FIN_MAJ
FROM
periodes_recues p
        INNER JOIN
    rattachement_declarations_recues rda ON (p.ID_PERIODE = rda.ID_PERIODE)
        INNER JOIN
    adhesion_etablissement_mois adh ON (adh.ID_ADH_ETAB_MOIS = rda.ID_ADH_ETAB_MOIS)
        INNER JOIN
    individu i ON (i.ID_ADH_ETAB_MOIS = adh.ID_ADH_ETAB_MOIS)
		INNER JOIN
	contrat_travail ct ON (ct.ID_INDIVIDU = i.ID_INDIVIDU)
		INNER JOIN
	affiliation a ON (a.ID_CONTRAT_TRAVAIL = ct.ID_CONTRAT_TRAVAIL)
		INNER JOIN
	base_assujettie ba ON (ba.ID_AFFILIATION = a.ID_AFFILIATION AND ba.DATE_FIN_RATTACHEMENT = p.DATE_FIN_PERIODE)
WHERE
    p.ID_PERIODE = C_ID_PERIODE
	AND (C_NO_CAT collate latin1_general_ci) = (COALESCE(IF((a.CODE_POPULATION = ''),
                    NULL,
                    a.CODE_POPULATION),
                (SELECT 
                        t.NOCAT
                    FROM
                        tarifs t
                    WHERE
                        ((t.NOCO = p.NUMERO_CONTRAT)
                            AND (t.DT_DEBUT_SIT <= p.DATE_DEBUT_PERIODE)
                            AND ((COALESCE(t.DT_FIN_SIT, 99999999) >= p.DATE_DEBUT_PERIODE)
                            OR ((CASE
                            WHEN (t.DT_FIN_SIT = 0) THEN 99999999
                            ELSE t.DT_FIN_SIT
                        END) >= p.DATE_DEBUT_PERIODE)))
                    ORDER BY t.DT_DEBUT_SIT
                    LIMIT 1)));

		UPDATE categorie_quittancement_individu cqi3 
SET 
    cqi3.EFFECTIF_DEBUT = EFF_DEB_MAJ,
    cqi3.EFFECTIF_FIN = EFF_FIN_MAJ
WHERE
    cqi3.ID_PERIODE = ID_PER_MAJ
AND cqi3.NOCAT = (C_NO_CAT collate latin1_general_ci);
	END LOOP;
END IF;
CLOSE liste_nocat_non_unique;
END;
END$$
DELIMITER ;

