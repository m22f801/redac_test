-- Vues communes -- 
DROP VIEW IF EXISTS V_Selection_type_consolidation;
CREATE VIEW V_Selection_type_consolidation AS select par.TYPE_CONSO_SALAIRE AS TYPE_CONSO_SALAIRE,per.ID_PERIODE AS ID_PERIODE from ((PARAM_FAMILLE_CONTRAT par join CONTRATS c on((par.NOFAM = c.NOFAM))) join PERIODES_RECUES per on((per.NUMERO_CONTRAT = c.NOCO))) where ((par.TMP_BATCH is false) and (c.TMP_BATCH is false) and (c.DT_DEBUT_SIT <= per.DATE_FIN_PERIODE) and ((coalesce(c.DT_FIN_SIT,99999999) >= per.DATE_FIN_PERIODE) or ((case when (c.DT_FIN_SIT = 0) then 99999999 else c.DT_FIN_SIT end) >= per.DATE_FIN_PERIODE)));

DROP VIEW IF EXISTS V_Selection_periode_nature_contrat_effectifs;
CREATE VIEW V_Selection_periode_nature_contrat_effectifs AS
    SELECT 
        p.ID_PERIODE AS ID_PERIODE,
        p.TYPE_PERIODE AS TYPE_PERIODE,
        p.DATE_CREATION AS DATE_CREATION,
        p.NUMERO_CONTRAT AS NUMERO_CONTRAT,
        p.DATE_DEBUT_PERIODE AS DATE_DEBUT_PERIODE,
        p.DATE_FIN_PERIODE AS DATE_FIN_PERIODE,
        p.PREMIER_MOIS_DECL AS PREMIER_MOIS_DECL,
        p.ETAT_PERIODE AS ETAT_PERIODE,
        p.RECONSOLIDER AS RECONSOLIDER,
        p.DATE_ECHEANCE AS DATE_ECHEANCE
    FROM
        (PERIODES_RECUES p
        JOIN TARIFS t)
    WHERE
        ((p.RECONSOLIDER = 'O')
            AND (t.NOCO = p.NUMERO_CONTRAT)
            AND (t.TMP_BATCH IS FALSE)
            AND ((t.DT_DEBUT_SIT , t.NOCAT) = (SELECT 
                tf.DT_DEBUT_SIT, tf.NOCAT
            FROM
                TARIFS tf
            WHERE
                ((tf.NOCO = p.NUMERO_CONTRAT)
                    AND (tf.DT_DEBUT_SIT <= p.DATE_DEBUT_PERIODE)
                    AND ((COALESCE(tf.DT_FIN_SIT, 99999999) >= p.DATE_DEBUT_PERIODE)
                    OR ((CASE
                    WHEN (tf.DT_FIN_SIT = 0) THEN 99999999
                    ELSE tf.DT_FIN_SIT
                END) >= p.DATE_DEBUT_PERIODE)))
            LIMIT 1))
            AND (t.NATURE_CONTRAT = 1));

DROP VIEW IF EXISTS V_Selection_periode_nature_contrat_salaires;
CREATE VIEW V_Selection_periode_nature_contrat_salaires AS
    SELECT 
        p.ID_PERIODE AS ID_PERIODE,
        p.TYPE_PERIODE AS TYPE_PERIODE,
        p.DATE_CREATION AS DATE_CREATION,
        p.NUMERO_CONTRAT AS NUMERO_CONTRAT,
        p.DATE_DEBUT_PERIODE AS DATE_DEBUT_PERIODE,
        p.DATE_FIN_PERIODE AS DATE_FIN_PERIODE,
        p.PREMIER_MOIS_DECL AS PREMIER_MOIS_DECL,
        p.ETAT_PERIODE AS ETAT_PERIODE,
        p.RECONSOLIDER AS RECONSOLIDER,
        p.DATE_ECHEANCE AS DATE_ECHEANCE,
        s.TYPE_CONSO_SALAIRE AS TYPE_CONSO
    FROM
        ((PERIODES_RECUES p
        JOIN TARIFS t)
        JOIN V_Selection_type_consolidation s)
    WHERE
        ((p.RECONSOLIDER = 'O')
            AND (t.NOCO = p.NUMERO_CONTRAT)
            AND (t.TMP_BATCH IS FALSE)
            AND ((t.DT_DEBUT_SIT , t.NOCAT) = (SELECT 
                tf.DT_DEBUT_SIT, tf.NOCAT
            FROM
                TARIFS tf
            WHERE
                ((tf.NOCO = p.NUMERO_CONTRAT)
                    AND (tf.DT_DEBUT_SIT <= p.DATE_DEBUT_PERIODE)
                    AND ((COALESCE(tf.DT_FIN_SIT, 99999999) >= p.DATE_DEBUT_PERIODE)
                    OR ((CASE
                    WHEN (tf.DT_FIN_SIT = 0) THEN 99999999
                    ELSE tf.DT_FIN_SIT
                END) >= p.DATE_DEBUT_PERIODE)))
            LIMIT 1))
            AND (t.NATURE_CONTRAT = 2)
            AND (s.ID_PERIODE = p.ID_PERIODE));

-- CQI SAL -- 
DROP VIEW IF EXISTS V_Categorie_quittancement_salaires_a_inserer;				
CREATE VIEW V_Categorie_quittancement_salaires_a_inserer AS
SELECT 
        p.ID_PERIODE AS ID_PERIODE,
        COALESCE(IF((`a`.`CODE_POPULATION` = ''), NULL, a.CODE_POPULATION), t.NOCAT) AS NOCAT,
        (CASE
            WHEN (i.IDENTIFIANT_REPERTOIRE = '') THEN i.NTT
            ELSE i.IDENTIFIANT_REPERTOIRE
        END) AS INDIVIDU,
        t.LICAT AS LICAT,
        t.CONBCOT AS CONBCOT,
       COALESCE((SELECT 
                        SUM(ba.MONTANT_COTISATION)
                    FROM
                        INDIVIDU ind
                            INNER JOIN
                        CONTRAT_TRAVAIL ct ON ind.ID_INDIVIDU = ct.ID_INDIVIDU
                            INNER JOIN
                        AFFILIATION af ON ct.ID_CONTRAT_TRAVAIL = af.ID_CONTRAT_TRAVAIL
                            INNER JOIN
                        BASE_ASSUJETTIE ba ON af.ID_AFFILIATION = ba.ID_AFFILIATION
                    WHERE
                        (CASE
                            WHEN i.IDENTIFIANT_REPERTOIRE <> '' THEN ind.IDENTIFIANT_REPERTOIRE = i.IDENTIFIANT_REPERTOIRE
                            ELSE (ind.NTT = i.NTT
								AND ind.IDENTIFIANT_REPERTOIRE = '')
                        END)
                        
                        AND ba.ID_BASE_ASSUJETTIE IN (SELECT 
                                tcba.ID_BASE_ASSUJETTIE
                            FROM
                                TRANCHE_CATEGORIE_BASE_ASSUJETTIE tcba
                            WHERE
                                tcba.NOCAT = COALESCE(IF((`a`.`CODE_POPULATION` = ''), NULL, a.CODE_POPULATION), t.NOCAT)
                                    AND tcba.ID_PERIODE = p.ID_PERIODE)), 0) AS MONTANT_COTISATION,
										 
		SUM(tcba.ESTIMATION_COTISATION) AS ESTIMATION_COTISATION,
        NULL AS CHAMP_NULL_1,
        NULL AS CHAMP_NULL_2
    FROM
        TARIFS t
            INNER JOIN
        PERIODES_RECUES p ON t.NOCO = p.NUMERO_CONTRAT
            INNER JOIN
        RATTACHEMENT_DECLARATIONS_RECUES r ON r.ID_PERIODE = p.ID_PERIODE
            INNER JOIN
        INDIVIDU i ON i.ID_ADH_ETAB_MOIS = r.ID_ADH_ETAB_MOIS
            AND i.TMP_BATCH IS FALSE
            INNER JOIN
        CONTRATS c ON c.NOCO = t.NOCO
            INNER JOIN
        PARAM_FAMILLE_CONTRAT par ON par.NOFAM = c.NOFAM
            LEFT JOIN
        CONTRAT_TRAVAIL ct ON ct.ID_INDIVIDU = i.ID_INDIVIDU
            JOIN
        AFFILIATION a ON ( CASE 
        WHEN (ISNULL(IF(`a`.`CODE_POPULATION` = '', NULL, a.CODE_POPULATION))) THEN (`ct`.`ID_CONTRAT_TRAVAIL` = `a`.`ID_CONTRAT_TRAVAIL`)
            ELSE ((`ct`.`ID_CONTRAT_TRAVAIL` = `a`.`ID_CONTRAT_TRAVAIL`)
                AND (`t`.`NOCAT` = `a`.`CODE_POPULATION`))
        END
        )
			LEFT JOIN
        BASE_ASSUJETTIE ba ON a.ID_AFFILIATION = ba.ID_AFFILIATION
			LEFT JOIN tranche_categorie_base_assujettie tcba ON (tcba.NOCAT = t.NOCAT
                    AND tcba.ID_PERIODE = p.ID_PERIODE
                    AND tcba.ID_BASE_ASSUJETTIE = ba.ID_BASE_ASSUJETTIE)
    WHERE
							   
        p.RECONSOLIDER = 'O' AND t.NATURE_CONTRAT = 2
            AND (t.TMP_BATCH IS FALSE)
            AND par.TYPE_CONSO_SALAIRE = 'INDIV'
            AND (par.TMP_BATCH IS FALSE)
            AND (c.TMP_BATCH IS FALSE)
            AND (c.DT_DEBUT_SIT <= p.DATE_DEBUT_PERIODE)
            AND ((COALESCE(c.DT_FIN_SIT, 99999999) >= p.DATE_DEBUT_PERIODE)
            OR ((CASE
            WHEN (c.DT_FIN_SIT = 0) THEN 99999999
            ELSE c.DT_FIN_SIT
        END) >= p.DATE_DEBUT_PERIODE))
            AND ((t.NOCO = p.NUMERO_CONTRAT)
            AND (t.DT_DEBUT_SIT <= p.DATE_DEBUT_PERIODE)
            AND ((COALESCE(t.DT_FIN_SIT, 99999999) >= p.DATE_DEBUT_PERIODE)
            OR ((CASE
            WHEN (t.DT_FIN_SIT = 0) THEN 99999999
            ELSE t.DT_FIN_SIT
        END) >= p.DATE_DEBUT_PERIODE))
            AND r.ID_ADH_ETAB_MOIS IN (SELECT 
                adh.ID_ADH_ETAB_MOIS
            FROM
                ADHESION_ETABLISSEMENT_MOIS adh
            WHERE
                adh.ID_ADH_ETAB_MOIS = r.ID_ADH_ETAB_MOIS
                    AND adh.TMP_BATCH IS FALSE))	
    GROUP BY p.ID_PERIODE, INDIVIDU, COALESCE(
        IF( (a.CODE_POPULATION = ''),NULL,a.CODE_POPULATION),
								   
            (SELECT t.NOCAT
						   
					
             FROM tarifs t
					 
             WHERE((t.NOCO = p.NUMERO_CONTRAT)
             AND (t.DT_DEBUT_SIT <= p.DATE_DEBUT_PERIODE)
             AND ((COALESCE(t.DT_FIN_SIT, 99999999) >= p.DATE_DEBUT_PERIODE)
             OR ((CASE
                WHEN (t.DT_FIN_SIT = 0) THEN 99999999
                ELSE t.DT_FIN_SIT
                END) >= p.DATE_DEBUT_PERIODE)))
             ORDER BY t.DT_DEBUT_SIT
             LIMIT 1))
    UNION ALL SELECT 
        p.ID_PERIODE AS ID_PERIODE,
        t.NOCAT AS NOCAT,
        'TOUS' AS INDIVIDU,
        t.LICAT AS LICAT,
        t.CONBCOT AS CONBCOT,
        COALESCE((SELECT 
                        SUM(ba.MONTANT_COTISATION)
                    FROM
                        BASE_ASSUJETTIE ba
                    WHERE
                        ba.ID_BASE_ASSUJETTIE IN (SELECT 
                                tcba.ID_BASE_ASSUJETTIE
                            FROM
                                TRANCHE_CATEGORIE_BASE_ASSUJETTIE tcba
                            WHERE
                                ((tcba.ID_PERIODE = p.ID_PERIODE)
                                    AND (tcba.NOCAT = t.NOCAT)))),
                0) AS MONTANT_COTISATION,
        (SELECT 
                SUM(tcba.ESTIMATION_COTISATION)
            FROM
                TRANCHE_CATEGORIE_BASE_ASSUJETTIE tcba
            WHERE
                ((tcba.ID_PERIODE = p.ID_PERIODE)
                    AND (tcba.NOCAT = t.NOCAT))) AS ESTIMATION_COTISATION,
        NULL AS CHAMP_NULL_1,
        NULL AS CHAMP_NULL_2
    FROM
        (TARIFS t
        JOIN PERIODES_RECUES p ON ((t.NOCO = p.NUMERO_CONTRAT)))
            INNER JOIN
        CONTRATS c ON c.NOCO = t.NOCO
            INNER JOIN
        PARAM_FAMILLE_CONTRAT par ON (par.NOFAM = c.NOFAM)
    WHERE
        (t.NOCO = p.NUMERO_CONTRAT
            AND t.DT_DEBUT_SIT <= p.DATE_DEBUT_PERIODE
            AND (COALESCE(t.DT_FIN_SIT, 99999999) >= p.DATE_DEBUT_PERIODE
            OR ((CASE
            WHEN (t.DT_FIN_SIT = 0) THEN 99999999
            ELSE t.DT_FIN_SIT
        END) >= p.DATE_DEBUT_PERIODE))
            AND p.RECONSOLIDER = 'O'
            AND t.NATURE_CONTRAT = 2
            AND (t.TMP_BATCH IS FALSE)
            AND par.TYPE_CONSO_SALAIRE = 'TOUS'
            AND (par.TMP_BATCH IS FALSE)
            AND (c.TMP_BATCH IS FALSE)
            AND (c.DT_DEBUT_SIT <= p.DATE_DEBUT_PERIODE)
            AND ((COALESCE(c.DT_FIN_SIT, 99999999) >= p.DATE_DEBUT_PERIODE)
            OR ((CASE
            WHEN (c.DT_FIN_SIT = 0) THEN 99999999
            ELSE c.DT_FIN_SIT
        END) >= p.DATE_DEBUT_PERIODE)));
   

-- ECM -- 
DROP VIEW IF EXISTS V_Liste_effectif_categorie_contrat_travail;
CREATE VIEW V_Liste_effectif_categorie_contrat_travail AS select ct.ID_PERIODE AS ID_PERIODE,ct.NOCAT AS NOCAT,ct.DEBUT_BASE AS DEBUT_BASE,ct.FIN_BASE AS FIN_BASE,ct.TOTAL_EFFECTIF AS TOTAL_EFFECTIF,s.DATE_DEBUT_PERIODE AS DATE_DEBUT_PERIODE,s.DATE_FIN_PERIODE AS DATE_FIN_PERIODE from ((EFFECTIF_CATEGORIE_CONTRAT_TRAVAIL ct join V_Selection_periode_nature_contrat_effectifs s on((s.ID_PERIODE = ct.ID_PERIODE))) join CATEGORIE_QUITTANCEMENT_INDIVIDU cq on(((cq.ID_PERIODE = s.ID_PERIODE) and (cq.NOCAT = ct.NOCAT)))) where (ct.TOTAL_EFFECTIF <> 0);
DROP VIEW IF EXISTS V_Dates_mouvement;
CREATE VIEW V_Dates_mouvement AS select distinct V_Liste_effectif_categorie_contrat_travail.ID_PERIODE AS ID_PERIODE,V_Liste_effectif_categorie_contrat_travail.NOCAT AS NOCAT,V_Liste_effectif_categorie_contrat_travail.DEBUT_BASE AS DATE_MOUVEMENT from V_Liste_effectif_categorie_contrat_travail where (V_Liste_effectif_categorie_contrat_travail.DEBUT_BASE <> V_Liste_effectif_categorie_contrat_travail.DATE_DEBUT_PERIODE) union select distinct V_Liste_effectif_categorie_contrat_travail.ID_PERIODE AS ID_PERIODE,V_Liste_effectif_categorie_contrat_travail.NOCAT AS NOCAT,V_Liste_effectif_categorie_contrat_travail.FIN_BASE AS DATE_MOUVEMENT from V_Liste_effectif_categorie_contrat_travail where (V_Liste_effectif_categorie_contrat_travail.FIN_BASE <> V_Liste_effectif_categorie_contrat_travail.DATE_FIN_PERIODE) order by ID_PERIODE,NOCAT,DATE_MOUVEMENT;
DROP VIEW IF EXISTS V_Effectifs_mouvements;
CREATE VIEW V_Effectifs_mouvements AS select dmd.ID_PERIODE AS ID_PERIODE,dmd.NOCAT AS NOCAT,dmd.DATE_MOUVEMENT AS DATE_MOUVEMENT,sum(li.TOTAL_EFFECTIF) AS EFF_DEB,0 AS EFF_FIN,'DEB' AS TYPE_DATE from (V_Liste_effectif_categorie_contrat_travail li join V_Dates_mouvement dmd on(((dmd.ID_PERIODE = li.ID_PERIODE) and (dmd.NOCAT = li.NOCAT) and (dmd.DATE_MOUVEMENT = li.DEBUT_BASE)))) group by dmd.ID_PERIODE,dmd.NOCAT,dmd.DATE_MOUVEMENT union select dmf.ID_PERIODE AS ID_PERIODE,dmf.NOCAT AS NOCAT,dmf.DATE_MOUVEMENT AS DATE_MOUVEMENT,0 AS EFF_DEB,sum(li.TOTAL_EFFECTIF) AS EFF_FIN,'FIN' AS TYPE_DATE from (V_Liste_effectif_categorie_contrat_travail li join V_Dates_mouvement dmf on(((dmf.ID_PERIODE = li.ID_PERIODE) and (dmf.NOCAT = li.NOCAT) and (dmf.DATE_MOUVEMENT = li.FIN_BASE)))) group by dmf.ID_PERIODE,dmf.NOCAT,dmf.DATE_MOUVEMENT;
DROP VIEW IF EXISTS V_Nombre_mouvements;
CREATE VIEW V_Nombre_mouvements AS select V_Effectifs_mouvements.ID_PERIODE AS ID_PERIODE,V_Effectifs_mouvements.NOCAT AS NOCAT,V_Effectifs_mouvements.DATE_MOUVEMENT AS DATE_MOUVEMENT,(sum(V_Effectifs_mouvements.EFF_DEB) - sum(V_Effectifs_mouvements.EFF_FIN)) AS NB_MVT from V_Effectifs_mouvements group by V_Effectifs_mouvements.ID_PERIODE,V_Effectifs_mouvements.NOCAT,V_Effectifs_mouvements.DATE_MOUVEMENT having (NB_MVT <> 0);
DROP VIEW IF EXISTS V_Effectif_categorie_mouvement_data;
CREATE VIEW V_Effectif_categorie_mouvement_data AS SELECT  `mvt1`.`ID_PERIODE` AS `ID_PERIODE`, `mvt1`.`NOCAT` AS `NOCAT`,`mvt1`.`DATE_MOUVEMENT` AS `DATE_MOUVEMENT_TRAITEE`,(`mvt1`.`NB_MVT` + COALESCE(`mvt2`.`NB_MVT`, 0)) AS `NB_MVT_FINAL`, (CASE WHEN (ABS(`mvt1`.`NB_MVT`) > ABS(COALESCE(`mvt2`.`NB_MVT`, 0))) THEN `mvt1`.`DATE_MOUVEMENT` ELSE `mvt2`.`DATE_MOUVEMENT` END) AS `DATE_CONSERVEE`, `mvt1`.`NB_MVT` AS `NB_MVT_INITIAL_TRAITE`, `mvt2`.`NB_MVT` AS `NB_MVT_INITIAL_SUIVANT`, (CASE WHEN (ABS(`mvt1`.`NB_MVT`) > ABS(COALESCE(`mvt2`.`NB_MVT`, 0))) THEN `mvt2`.`DATE_MOUVEMENT`  ELSE `mvt1`.`DATE_MOUVEMENT`  END) AS `DATE_SUPPRIMEE` FROM (`V_Nombre_mouvements` `mvt1` LEFT JOIN `V_Nombre_mouvements` `mvt2` ON (((`mvt2`.`ID_PERIODE` = `mvt1`.`ID_PERIODE`) AND (`mvt2`.`NOCAT` = `mvt1`.`NOCAT`) AND (CAST(`mvt2`.`DATE_MOUVEMENT` AS DATE) = (`mvt1`.`DATE_MOUVEMENT` + INTERVAL 1 DAY)) AND (SIGN(`mvt2`.`NB_MVT`) > 0)  AND (SIGN(`mvt1`.`NB_MVT`) < 0))));
DROP VIEW IF EXISTS V_Effectif_categorie_mouvement_suppression1;
CREATE VIEW V_Effectif_categorie_mouvement_suppression1 AS select V_Effectif_categorie_mouvement_data.ID_PERIODE AS ID_PERIODE,V_Effectif_categorie_mouvement_data.NOCAT AS NOCAT,V_Effectif_categorie_mouvement_data.DATE_CONSERVEE AS DATE_SUPPR from V_Effectif_categorie_mouvement_data where (V_Effectif_categorie_mouvement_data.NB_MVT_FINAL = 0) union select V_Effectif_categorie_mouvement_data.ID_PERIODE AS ID_PERIODE,V_Effectif_categorie_mouvement_data.NOCAT AS NOCAT,V_Effectif_categorie_mouvement_data.DATE_SUPPRIMEE AS DATE_SUPPR from V_Effectif_categorie_mouvement_data where (V_Effectif_categorie_mouvement_data.NB_MVT_FINAL = 0) order by ID_PERIODE;
DROP VIEW IF EXISTS V_Effectif_categorie_mouvement_suppression2;
CREATE VIEW V_Effectif_categorie_mouvement_suppression2 AS select V_Effectif_categorie_mouvement_data.ID_PERIODE AS ID_PERIODE,V_Effectif_categorie_mouvement_data.NOCAT AS NOCAT,(case when (V_Effectif_categorie_mouvement_data.DATE_MOUVEMENT_TRAITEE = V_Effectif_categorie_mouvement_data.DATE_CONSERVEE) then V_Effectif_categorie_mouvement_data.DATE_SUPPRIMEE else V_Effectif_categorie_mouvement_data.DATE_CONSERVEE end) AS DATE_SUPPR,V_Effectif_categorie_mouvement_data.NB_MVT_INITIAL_SUIVANT AS NB_MVT_INITIAL from V_Effectif_categorie_mouvement_data where (V_Effectif_categorie_mouvement_data.DATE_SUPPRIMEE is not null) order by V_Effectif_categorie_mouvement_data.ID_PERIODE;


-- CQI EFF -- 
DROP VIEW IF EXISTS V_Distinct_noco_nocat_licat_pour_contrat_et_plage;
CREATE VIEW V_Distinct_noco_nocat_licat_pour_contrat_et_plage AS select distinct s.ID_PERIODE AS ID_PERIODE,t.NOCO AS NOCO,t.NOCAT AS NOCAT,t.LICAT AS LICAT,t.CONBCOT AS CONBCOT from (TARIFS t join V_Selection_periode_nature_contrat_effectifs s on((t.NOCO = s.NUMERO_CONTRAT))) where ((t.DT_DEBUT_SIT <= s.DATE_FIN_PERIODE) and ((coalesce(t.DT_FIN_SIT,99999999) >= s.DATE_DEBUT_PERIODE) or ((case when (t.DT_FIN_SIT = 0) then 99999999 else t.DT_FIN_SIT end) >= s.DATE_DEBUT_PERIODE)));

DROP VIEW IF EXISTS V_Distinct_noco_nocat_pour_contrat_et_plage;
CREATE VIEW `V_Distinct_noco_nocat_pour_contrat_et_plage` AS
    SELECT DISTINCT
        `s`.`ID_PERIODE` AS `ID_PERIODE`,
        `t`.`NOCO` AS `NOCO`,
        `t`.`NOCAT` AS `NOCAT`
    FROM
        (`TARIFS` `t`
        JOIN `V_Selection_periode_nature_contrat_effectifs` `s` ON ((`t`.`NOCO` = `s`.`NUMERO_CONTRAT`)))
    WHERE
        ((`t`.`DT_DEBUT_SIT` <= `s`.`DATE_DEBUT_PERIODE`)
            AND ((COALESCE(`t`.`DT_FIN_SIT`, 99999999) >= `s`.`DATE_DEBUT_PERIODE`)
            OR ((CASE
            WHEN (`t`.`DT_FIN_SIT` = 0) THEN 99999999
            ELSE `t`.`DT_FIN_SIT`
        END) >= `s`.`DATE_DEBUT_PERIODE`)));

DROP VIEW IF EXISTS V_Selection_categorie_quittancement_effectif_data;
CREATE VIEW V_Selection_categorie_quittancement_effectif_data AS
   SELECT 
        d.ID_PERIODE AS ID_PERIODE,
        d.NOCAT AS NOCAT,
        e.DEBUT_BASE AS DEBUT_BASE,
        e.FIN_BASE AS FIN_BASE,
        (CASE WHEN e.PRORATA_COTISATION IS NULL THEN 0 ELSE e.PRORATA_COTISATION END)  AS PRORATA_COTISATION,
        (CASE WHEN e.ESTIMATION_COTISATION IS NULL THEN 0 ELSE e.ESTIMATION_COTISATION END)  AS ESTIM_COTISATION,
        e.TOTAL_EFFECTIF AS TOTAL_EFFECTIF,
        (CASE
            WHEN (e.DEBUT_BASE = s.DATE_DEBUT_PERIODE) THEN e.TOTAL_EFFECTIF
            ELSE 0
        END) AS effectif_debut,
        (CASE
            WHEN (e.FIN_BASE = s.DATE_FIN_PERIODE) THEN e.TOTAL_EFFECTIF
            ELSE 0
        END) AS effectif_fin,
        s.DATE_DEBUT_PERIODE AS DATE_DEBUT_PERIODE,
        s.DATE_FIN_PERIODE AS DATE_FIN_PERIODE, 
        t.CONBCOT AS CONBCOT,
        t.LICAT AS LICAT
    FROM
        (((EFFECTIF_CATEGORIE_CONTRAT_TRAVAIL e
        RIGHT JOIN V_Distinct_noco_nocat_pour_contrat_et_plage d ON (((d.ID_PERIODE = e.ID_PERIODE)
            AND (d.NOCAT = e.NOCAT))))
        JOIN V_Selection_periode_nature_contrat_effectifs s ON ((s.ID_PERIODE = d.ID_PERIODE)))
        JOIN TARIFS t ON (((t.NOCAT = d.NOCAT)
            AND (t.NOCO = d.NOCO)
            AND (t.DT_DEBUT_SIT <= s.DATE_DEBUT_PERIODE)
            AND ((COALESCE(t.DT_FIN_SIT, 99999999) >= s.DATE_DEBUT_PERIODE)
            OR ((CASE
            WHEN (t.DT_FIN_SIT = 0) THEN 99999999
            ELSE t.DT_FIN_SIT
        END) >= s.DATE_DEBUT_PERIODE)))));

DROP VIEW IF EXISTS V_Selection_categorie_quittancement_effectif;
CREATE VIEW V_Selection_categorie_quittancement_effectif AS
    SELECT 
        ecct.ID_PERIODE AS ID_PERIODE,
        ecct.NOCAT AS NOCAT,
        'TOUS' AS INDIVIDU,
        ecct.LICAT AS LICAT,
        ecct.CONBCOT AS CONBCOT,
        SUM(ecct.PRORATA_COTISATION) AS COTISATION,
        SUM(ecct.effectif_debut) AS EFFECTIF_DEBUT,
        SUM(ecct.effectif_fin) AS EFFECTIF_FIN,
        SUM(ecct.ESTIM_COTISATION) AS ESTIMATION_COTISATION
    FROM
        V_Selection_categorie_quittancement_effectif_data ecct
    GROUP BY ecct.ID_PERIODE , ecct.NOCAT;

-- TCBA -- 
DROP VIEW IF EXISTS V_Selection_base_assujettie;
CREATE VIEW V_Selection_base_assujettie AS SELECT 
    `ba`.`ID_BASE_ASSUJETTIE` AS `ID_BASE_ASSUJETTIE`,
    `ba`.`ID_AFFILIATION` AS `ID_AFFILIATION`,
    `ba`.`DATE_DEB_RATTACHEMENT` AS `DATE_DEB_RATTACHEMENT`,
    `ba`.`DATE_FIN_RATTACHEMENT` AS `DATE_FIN_RATTACHEMENT`,
    `ba`.`TMP_BATCH` AS `TMP_BATCH`,
    `s`.`ID_PERIODE` AS `id_periode`,
    `s`.`NUMERO_CONTRAT` AS `numero_contrat`,
    `s`.`TYPE_PERIODE` AS `TYPE_PERIODE`,
    `s`.`DATE_CREATION` AS `DATE_CREATION`,
    `s`.`DATE_DEBUT_PERIODE` AS `DATE_DEBUT_PERIODE`,
    `s`.`DATE_FIN_PERIODE` AS `DATE_FIN_PERIODE`,
    `adh`.`MOIS_RATTACHEMENT` AS `mois_rattachement`,
    COALESCE(IF((`a`.`CODE_POPULATION` = ''),
                NULL,
                `a`.`CODE_POPULATION`),
            (SELECT 
                    `t`.`NOCAT`
                FROM
                    `TARIFS` `t`
                WHERE
                    ((`t`.`NOCO` = `s`.`NUMERO_CONTRAT`)
                        AND (`t`.`DT_DEBUT_SIT` <= `s`.`DATE_DEBUT_PERIODE`)
                        AND ((COALESCE(`t`.`DT_FIN_SIT`, 99999999) >= `s`.`DATE_DEBUT_PERIODE`)
                        OR ((CASE
                        WHEN (`t`.`DT_FIN_SIT` = 0) THEN 99999999
                        ELSE `t`.`DT_FIN_SIT`
                    END) >= `s`.`DATE_DEBUT_PERIODE`)))
                ORDER BY `t`.`DT_DEBUT_SIT`
                LIMIT 1)) AS `NOCAT`
FROM
    ((((((`BASE_ASSUJETTIE` `ba`
    LEFT JOIN `AFFILIATION` `a` ON ((`ba`.`ID_AFFILIATION` = `a`.`ID_AFFILIATION`)))
    LEFT JOIN `CONTRAT_TRAVAIL` `ct` ON ((`a`.`ID_CONTRAT_TRAVAIL` = `ct`.`ID_CONTRAT_TRAVAIL`)))
    LEFT JOIN `INDIVIDU` `i` ON ((`ct`.`ID_INDIVIDU` = `i`.`ID_INDIVIDU`)))
    LEFT JOIN `ADHESION_ETABLISSEMENT_MOIS` `adh` ON ((`i`.`ID_ADH_ETAB_MOIS` = `adh`.`ID_ADH_ETAB_MOIS`)))
    JOIN `RATTACHEMENT_DECLARATIONS_RECUES` `r` ON ((`adh`.`ID_ADH_ETAB_MOIS` = `r`.`ID_ADH_ETAB_MOIS`)))
    JOIN `V_Selection_periode_nature_contrat_salaires` `s` ON ((`s`.`ID_PERIODE` = `r`.`ID_PERIODE`)))
WHERE
    ((`ba`.`MONTANT_COTISATION` <> 0)
        AND (`adh`.`TMP_BATCH` IS FALSE));
   
-- Vue pour le partitionning -- 
DROP VIEW IF EXISTS V_Effectif_categorie_contrat_travail_a_calculer;
CREATE VIEW V_Effectif_categorie_contrat_travail_a_calculer AS select s.ID_PERIODE AS ID_PERIODE,count(0) AS NB_ENTITES_ECCT from (((((V_Selection_periode_nature_contrat_effectifs s join V_Distinct_noco_nocat_licat_pour_contrat_et_plage d on((d.ID_PERIODE = s.ID_PERIODE))) join RATTACHEMENT_DECLARATIONS_RECUES r on((r.ID_PERIODE = d.ID_PERIODE))) join ADHESION_ETABLISSEMENT_MOIS a on((a.ID_ADH_ETAB_MOIS = r.ID_ADH_ETAB_MOIS))) join INDIVIDU i on((i.ID_ADH_ETAB_MOIS = a.ID_ADH_ETAB_MOIS))) join CONTRAT_TRAVAIL ct on((ct.ID_INDIVIDU = i.ID_INDIVIDU))) group by s.ID_PERIODE;

-- Vues pour les performances export Individu
DROP VIEW IF EXISTS V_ctr_export_individus;
CREATE VIEW `V_ctr_export_individus` AS SELECT `r`.`ID_PERIODE` AS `ID_PERIODE`, `ind`.`IDENTIFIANT_REPERTOIRE` AS `IDENTIFIANT_REPERTOIRE`, `ind`.`NTT` AS `NTT`, `ctr`.`MOTIF_RUPTURE` AS `MOTIF_RUPTURE`, `ctr`.`ID_CONTRAT_TRAVAIL` AS `ID_CONTRAT_TRAVAIL`, `ctr`.`DATE_DEBUT_CONTRAT` AS `DATE_DEBUT_CONTRAT`, `ctr`.`DATE_FIN_PREVISIONNELLE` AS `DATE_FIN_PREVISIONNELLE`,p.DATE_DEBUT_PERIODE AS DATE_DEBUT_PERIODE,p.DATE_FIN_PERIODE AS DATE_FIN_PERIODE FROM (((`CONTRAT_TRAVAIL` `ctr` LEFT JOIN `INDIVIDU` `ind` ON (((`ind`.`ID_INDIVIDU` = `ctr`.`ID_INDIVIDU`) AND (`ind`.`TMP_BATCH` = `ctr`.`TMP_BATCH`)))) LEFT JOIN `ADHESION_ETABLISSEMENT_MOIS` `adh` ON (((`adh`.`ID_ADH_ETAB_MOIS` = `ind`.`ID_ADH_ETAB_MOIS`) AND (`adh`.`TMP_BATCH` = `ind`.`TMP_BATCH`)))) JOIN `RATTACHEMENT_DECLARATIONS_RECUES` `r` ON ((`r`.`ID_ADH_ETAB_MOIS` = `adh`.`ID_ADH_ETAB_MOIS`))) JOIN `PERIODES_RECUES` `p` ON ((`r`.`ID_PERIODE` = `p`.`ID_PERIODE`)) WHERE (`adh`.`TMP_BATCH` IS FALSE);

-- Vues pour export excel periodes
DROP VIEW IF EXISTS V_Informations_Client_Export;
CREATE VIEW V_Informations_Client_Export AS SELECT c.NOCO AS NOCO,c.DT_DEBUT_SIT AS DT_DEBUT_SIT,c.DT_FIN_SIT AS DT_FIN_SIT,c.NMGRPGES AS NMGRPGES,c.NOFAM AS NOFAM,c.NCPROD AS NCPROD,c.NCENC AS NCENC,c.NOCLI AS NOCLI, cl.NOSIREN AS NOSIREN, cl.NOSIRET AS NOSIRET, cl.LRSO AS LRSO FROM CONTRATS c INNER JOIN CLIENTS cl ON c.NOCLI=cl.NOCLI AND c.TMP_BATCH=cl.TMP_BATCH  WHERE c.TMP_BATCH IS FALSE ORDER BY c.DT_DEBUT_SIT DESC;
DROP VIEW IF EXISTS V_Indicateurs_Dsn_Export;
CREATE VIEW V_Indicateurs_Dsn_Export AS SELECT i.NOCO AS NOCO,i.DEBUT_PERIODE AS DEBUT_PERIODE,i.FIN_PERIODE AS FIN_PERIODE,i.MODE_GEST AS MODE_GEST,i.TRANSFERT AS TRANSFERT_DSN  FROM INDICATEURS_DSN_PERIODES i;

-- Vue erreurs 405 - salaires
DROP VIEW IF EXISTS V_Trace_tranche_categorie_base_assujettie;
CREATE VIEW V_Trace_tranche_categorie_base_assujettie AS SELECT DISTINCT
        sel.id_periode AS ID_PERIODE,
        sel.numero_contrat AS NUMERO_CONTRAT,
        sel.DATE_DEB_RATTACHEMENT AS DATE_DEB_RATTACHEMENT,
        sel.DATE_FIN_RATTACHEMENT AS DATE_FIN_RATTACHEMENT,
        sel.NOCAT AS NOCAT,
        CONCAT('La situation tarifaire [numContrat=',
                sel.numero_contrat,
                ', numCategorie=',
                sel.NOCAT,
                ', date=',
                sel.DATE_FIN_RATTACHEMENT,
                '] n\'existe pas. Periode traitee : [PeriodeRecue(idPeriode=',
                sel.id_periode,
                ', typePeriode=',
                sel.TYPE_PERIODE,
                ', dateDebutPeriode=',
                sel.DATE_DEBUT_PERIODE,
                ', dateFinPeriode=',
                sel.DATE_FIN_PERIODE,
                ', dateCreation=',
                sel.DATE_CREATION,
                ')]') AS MESSAGE
    FROM
        (V_Selection_base_assujettie sel
        LEFT JOIN TARIFS t ON (((sel.numero_contrat = t.NOCO)
            AND (sel.NOCAT = t.NOCAT)
            AND (t.DT_DEBUT_SIT <= sel.DATE_FIN_RATTACHEMENT)
            AND ((COALESCE(t.DT_FIN_SIT, 99999999) >= sel.DATE_FIN_RATTACHEMENT)
            OR ((CASE
            WHEN (t.DT_FIN_SIT = 0) THEN 99999999
            ELSE t.DT_FIN_SIT
        END) >= sel.DATE_FIN_RATTACHEMENT)))))
    WHERE
        (ISNULL(t.NOCO)
            OR ISNULL(t.NOCAT)) 
    UNION SELECT DISTINCT
        sel.id_periode AS ID_PERIODE,
        sel.numero_contrat AS NUMERO_CONTRAT,
        sel.DATE_DEB_RATTACHEMENT AS DATE_DEB_RATTACHEMENT,
        sel.DATE_FIN_RATTACHEMENT AS DATE_FIN_RATTACHEMENT,
        sel.NOCAT AS NOCAT,
        (CONCAT(CONVERT( CONCAT('La nature base [CONBCOT=',
                        (CASE
                            WHEN ISNULL(t.CONBCOT) THEN 'NULL'
                            ELSE t.CONBCOT
                        END),
                        ', TXCALCU_REMPLI=',
                        (CASE
                            WHEN (COALESCE(t.TXCALCU, 0) > 0) THEN 'O'
                            ELSE 'N'
                        END),
                        '] n\'existe pas. Situation tarifaire : numContrat=') USING LATIN1),
                sel.numero_contrat,
                ', numCategorie=',
                sel.NOCAT,
                ', date=',
                sel.DATE_FIN_RATTACHEMENT,
                '. Periode traitee : [PeriodeRecue(idPeriode=',
                sel.id_periode,
                ', typePeriode=',
                sel.TYPE_PERIODE,
                ', dateDebutPeriode=',
                sel.DATE_DEBUT_PERIODE,
                ', dateFinPeriode=',
                sel.DATE_FIN_PERIODE,
                ', dateCreation=',
                sel.DATE_CREATION,
                ')]') COLLATE latin1_general_ci) AS trace
    FROM
        ((V_Selection_base_assujettie sel
        JOIN TARIFS t ON (((sel.numero_contrat = t.NOCO)
            AND (sel.NOCAT = t.NOCAT)
            AND (t.DT_DEBUT_SIT <= sel.DATE_FIN_RATTACHEMENT)
            AND ((COALESCE(t.DT_FIN_SIT, 99999999) >= sel.DATE_FIN_RATTACHEMENT)
            OR ((CASE
            WHEN (t.DT_FIN_SIT = 0) THEN 99999999
            ELSE t.DT_FIN_SIT
        END) >= sel.DATE_FIN_RATTACHEMENT)))))
        LEFT JOIN PARAM_NATURE_BASE p ON (((t.CONBCOT = p.CONBCOT)
            AND (CONVERT( (CASE
            WHEN (COALESCE(t.TXCALCU, 0) > 0) THEN 'O'
            ELSE 'N'
        END) USING LATIN1) = (p.TXCALCU_REMPLI COLLATE latin1_general_ci)))))
    WHERE
        ISNULL(p.NUM_TRANCHE)
        UNION SELECT DISTINCT
        sel.id_periode AS ID_PERIODE,
        sel.numero_contrat AS NUMERO_CONTRAT,
        sel.DATE_DEB_RATTACHEMENT AS DATE_DEB_RATTACHEMENT,
        sel.DATE_FIN_RATTACHEMENT AS DATE_FIN_RATTACHEMENT,
        sel.NOCAT AS NOCAT,
        (CONCAT(CONVERT( CONCAT('La situation contrat [numContrat=') USING LATIN1),
                sel.numero_contrat,
                ', date=(',
                sel.DATE_DEBUT_PERIODE,
                ')] n\'existe pas. Periode traitee : [(idPeriode=',
                sel.id_periode,
                ', typePeriode=',
                sel.TYPE_PERIODE,
                ', dateDebutPeriode=',
                sel.DATE_DEBUT_PERIODE,
                ', dateCreation=',
                sel.DATE_CREATION,
                ')]') COLLATE latin1_general_ci) AS trace
    FROM
        V_Selection_base_assujettie sel
        JOIN contrats ON (contrats.NOCO = sel.NUMERO_CONTRAT
        AND contrats.DT_DEBUT_SIT <= sel.DATE_DEBUT_PERIODE
        AND COALESCE(contrats.DT_FIN_SIT, 99999999) >= sel.DATE_DEBUT_PERIODE)
    WHERE
        ISNULL(contrats.NOCO);

-- Vue F04_RG_P5_03 pour contrats sur salaires
DROP VIEW IF EXISTS V_Trace_Nocat_Inconnu_salaires;
CREATE VIEW V_Trace_Nocat_Inconnu_salaires AS 
SELECT DISTINCT
    vpms.ID_PERIODE,CONCAT('La situation tarifaire [numContrat=',
                vpms.NUMERO_CONTRAT,
                ', numCategorieQuittancement=',
                a.CODE_POPULATION,
                ', dateDebutPeriode=',
                vpms.DATE_DEBUT_PERIODE,
                '] n\'existe pas. Periode traitee : [PeriodeRecue(idPeriode=',
                vpms.ID_PERIODE,
                ', typePeriode=',
                vpms.TYPE_PERIODE,
                ', dateDebutPeriode=',
                vpms.DATE_DEBUT_PERIODE,
                ', dateFinPeriode=',
                vpms.DATE_FIN_PERIODE,
                ', dateCreation=',
                vpms.DATE_CREATION,
                ')]') AS MESSAGE
FROM
	V_Selection_periode_nature_contrat_salaires vpms,
    AFFILIATION a
        LEFT JOIN
    CONTRAT_TRAVAIL ct ON a.ID_CONTRAT_TRAVAIL = ct.ID_CONTRAT_TRAVAIL
        LEFT JOIN
    INDIVIDU i ON ct.ID_INDIVIDU = i.ID_INDIVIDU
        LEFT JOIN
    ADHESION_ETABLISSEMENT_MOIS adh ON i.ID_ADH_ETAB_MOIS = adh.ID_ADH_ETAB_MOIS
WHERE
    adh.TMP_BATCH IS FALSE
		AND a.CODE_POPULATION <> ''
        AND adh.ID_ADH_ETAB_MOIS IN (SELECT 
            r.ID_ADH_ETAB_MOIS
        FROM
            RATTACHEMENT_DECLARATIONS_RECUES r
        WHERE
            r.ID_PERIODE = vpms.ID_PERIODE)
        AND a.CODE_POPULATION NOT IN (SELECT DISTINCT
            t.NOCAT
        FROM
            TARIFS t
        WHERE
            (t.NOCO = vpms.NUMERO_CONTRAT)
            AND (t.NOCAT <> '')
                AND (t.DT_DEBUT_SIT <= vpms.DATE_DEBUT_PERIODE)
                AND ((COALESCE(t.DT_FIN_SIT, 99999999) >= vpms.DATE_DEBUT_PERIODE)
                OR ((CASE
                WHEN (t.DT_FIN_SIT = 0) THEN 99999999
                ELSE t.DT_FIN_SIT
            END) >= vpms.DATE_DEBUT_PERIODE)));