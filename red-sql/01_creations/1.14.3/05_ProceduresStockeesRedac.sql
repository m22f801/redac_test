-- Création des procédures stockées REDAC
DROP PROCEDURE IF EXISTS Inserer_CATEGORIE_QUITTANCEMENT_INDIVIDU_EFFECTIF;

DELIMITER $$
CREATE PROCEDURE Inserer_CATEGORIE_QUITTANCEMENT_INDIVIDU_EFFECTIF(IN p_audit_nom_batch VARCHAR(20))
BEGIN
-- Calcul et insersion des nouveaux éléments des périodes à traiter
INSERT INTO CATEGORIE_QUITTANCEMENT_INDIVIDU
(
ID_PERIODE
,NOCAT
,INDIVIDU
,LICAT
,CONBCOT
,MT_COTISATION
,EFFECTIF_DEBUT
,EFFECTIF_FIN
,ESTIMATION_COTISATION
,USER_CREATION
)
SELECT 
ID_PERIODE
,NOCAT
,INDIVIDU
,LICAT
,CONBCOT
,COTISATION
,EFFECTIF_DEBUT
,EFFECTIF_FIN
,ESTIMATION_COTISATION
,p_audit_nom_batch
FROM V_Selection_categorie_quittancement_effectif;
END$$
DELIMITER ;

DROP PROCEDURE IF EXISTS Inserer_CATEGORIE_QUITTANCEMENT_INDIVIDU_SALAIRE_Pour_Periodes;

DELIMITER $$
CREATE PROCEDURE Inserer_CATEGORIE_QUITTANCEMENT_INDIVIDU_SALAIRE_Pour_Periodes(IN p_audit_nom_batch VARCHAR(20), IN p_liste_ids_periodes MEDIUMTEXT)
BEGIN
																	   
INSERT INTO CATEGORIE_QUITTANCEMENT_INDIVIDU (
ID_PERIODE
,NOCAT
,INDIVIDU
,LICAT
,CONBCOT
,MT_COTISATION
,ESTIMATION_COTISATION
,EFFECTIF_DEBUT
,EFFECTIF_FIN
,USER_CREATION
)
SELECT ID_PERIODE, NOCAT, INDIVIDU, LICAT, CONBCOT, sum(MONTANT_COTISATION), SUM(ESTIMATION_COTISATION) AS ESTIMATION_COTISATION, CHAMP_NULL_1, CHAMP_NULL_2, 'RR405' -- p_audit_nom_batch
FROM (

	SELECT 
        p.ID_PERIODE AS ID_PERIODE,
        t.NOCAT AS NOCAT,
        (CASE
            WHEN (i.IDENTIFIANT_REPERTOIRE = '') THEN i.NTT
            ELSE i.IDENTIFIANT_REPERTOIRE
        END) AS INDIVIDU,
        t.LICAT AS LICAT,
        t.CONBCOT AS CONBCOT,
		COALESCE(s.montant_cot, 0) AS MONTANT_COTISATION,
		SUM(tcba.ESTIMATION_COTISATION) AS ESTIMATION_COTISATION,
		NULL AS CHAMP_NULL_1,
        NULL AS CHAMP_NULL_2,
        p_audit_nom_batch
    FROM
        TARIFS t
            INNER JOIN
        PERIODES_RECUES p ON t.NOCO = p.NUMERO_CONTRAT
            INNER JOIN
        RATTACHEMENT_DECLARATIONS_RECUES r ON r.ID_PERIODE = p.ID_PERIODE
            INNER JOIN
        INDIVIDU i ON i.ID_ADH_ETAB_MOIS = r.ID_ADH_ETAB_MOIS
            AND i.TMP_BATCH IS FALSE
            INNER JOIN
        CONTRATS c ON c.NOCO = t.NOCO
            INNER JOIN
        PARAM_FAMILLE_CONTRAT par ON (par.NOFAM = c.NOFAM AND par.NMGRPGES = c.NMGRPGES)
            LEFT JOIN
        CONTRAT_TRAVAIL ct ON ct.ID_INDIVIDU = i.ID_INDIVIDU
            INNER JOIN
        AFFILIATION a ON  (`ct`.`ID_CONTRAT_TRAVAIL` = `a`.`ID_CONTRAT_TRAVAIL`)
        AND ISNULL(IF(`a`.`CODE_POPULATION` = '', NULL, a.CODE_POPULATION)) = 1
			LEFT JOIN
        BASE_ASSUJETTIE ba ON a.ID_AFFILIATION = ba.ID_AFFILIATION
			LEFT JOIN tranche_categorie_base_assujettie tcba ON (tcba.NOCAT = t.NOCAT
                    AND tcba.ID_PERIODE = p.ID_PERIODE
                    AND tcba.ID_BASE_ASSUJETTIE = ba.ID_BASE_ASSUJETTIE)
         LEFT JOIN
         	(SELECT 
                        SUM(ba.MONTANT_COTISATION) as montant_cot,  
								(CASE
            					WHEN (ind.IDENTIFIANT_REPERTOIRE = '') THEN ind.NTT
            					ELSE ind.IDENTIFIANT_REPERTOIRE
        						END) as id_indiv,
        						af.CODE_POPULATION,
        						rdr.id_periode
                    FROM
                        RATTACHEMENT_DECLARATIONS_RECUES rdr
									INNER JOIN 
								INDIVIDU ind ON rdr.ID_ADH_ETAB_MOIS = ind.ID_ADH_ETAB_MOIS
                            INNER JOIN
                        CONTRAT_TRAVAIL ct ON ind.ID_INDIVIDU = ct.ID_INDIVIDU
                            INNER JOIN
                        AFFILIATION af ON ct.ID_CONTRAT_TRAVAIL = af.ID_CONTRAT_TRAVAIL
                            INNER JOIN
                        BASE_ASSUJETTIE ba ON af.ID_AFFILIATION = ba.ID_AFFILIATION
                    WHERE
                    		FIND_IN_SET(rdr.ID_PERIODE, p_liste_ids_periodes) > 0
                     GROUP BY 
								(CASE
            					WHEN (ind.IDENTIFIANT_REPERTOIRE = '') THEN ind.NTT
            					ELSE ind.IDENTIFIANT_REPERTOIRE
        						END),
        						af.CODE_POPULATION, rdr.id_periode
				) as s on s.id_indiv = (CASE
            					WHEN (i.IDENTIFIANT_REPERTOIRE = '') THEN i.NTT
            					ELSE i.IDENTIFIANT_REPERTOIRE
        						END) and s.CODE_POPULATION = a.code_population and s.id_periode = p.id_periode
    WHERE
		  
        p.RECONSOLIDER = 'O' AND t.NATURE_CONTRAT = 2
            AND (t.TMP_BATCH IS FALSE)
            AND par.TYPE_CONSO_SALAIRE = 'INDIV'
            AND (par.TMP_BATCH IS FALSE)
            AND (c.TMP_BATCH IS FALSE)
            AND (c.DT_DEBUT_SIT <= p.DATE_DEBUT_PERIODE)
            AND (COALESCE(c.DT_FIN_SIT, 99999999) >= p.DATE_DEBUT_PERIODE)
            AND ((t.NOCO = p.NUMERO_CONTRAT)
            AND (t.DT_DEBUT_SIT <= p.DATE_DEBUT_PERIODE)
            AND (COALESCE(t.DT_FIN_SIT, 99999999) >= p.DATE_DEBUT_PERIODE)
            AND r.ID_ADH_ETAB_MOIS IN (SELECT 
                adh.ID_ADH_ETAB_MOIS
            FROM
                ADHESION_ETABLISSEMENT_MOIS adh
            WHERE
                adh.ID_ADH_ETAB_MOIS = r.ID_ADH_ETAB_MOIS
                    AND adh.TMP_BATCH IS FALSE))
					AND FIND_IN_SET(p.ID_PERIODE, p_liste_ids_periodes) > 0
    GROUP BY p.ID_PERIODE, INDIVIDU, COALESCE(
        IF( (a.CODE_POPULATION = ''),NULL,a.CODE_POPULATION),
            (SELECT t.NOCAT
             FROM tarifs t
             WHERE((t.NOCO = p.NUMERO_CONTRAT)
             AND (t.DT_DEBUT_SIT <= p.DATE_DEBUT_PERIODE)
             AND ((COALESCE(t.DT_FIN_SIT, 99999999) >= p.DATE_DEBUT_PERIODE)
             ))
             ORDER BY t.DT_DEBUT_SIT
             LIMIT 1))
	UNION ALL
	SELECT 
        p.ID_PERIODE AS ID_PERIODE,
        a.CODE_POPULATION AS NOCAT,
        (CASE
            WHEN (i.IDENTIFIANT_REPERTOIRE = '') THEN i.NTT
            ELSE i.IDENTIFIANT_REPERTOIRE
        END) AS INDIVIDU,
        t.LICAT AS LICAT,
        t.CONBCOT AS CONBCOT,
		COALESCE(s.montant_cot, 0) AS MONTANT_COTISATION,
		SUM(tcba.ESTIMATION_COTISATION) AS ESTIMATION_COTISATION,
        NULL AS CHAMP_NULL_1,
        NULL AS CHAMP_NULL_2,
        p_audit_nom_batch
    FROM
        TARIFS t
            INNER JOIN
        PERIODES_RECUES p ON t.NOCO = p.NUMERO_CONTRAT
            INNER JOIN
        RATTACHEMENT_DECLARATIONS_RECUES r ON r.ID_PERIODE = p.ID_PERIODE
            INNER JOIN
        INDIVIDU i ON i.ID_ADH_ETAB_MOIS = r.ID_ADH_ETAB_MOIS
            AND i.TMP_BATCH IS FALSE
            INNER JOIN
        CONTRATS c ON c.NOCO = t.NOCO
            INNER JOIN
        PARAM_FAMILLE_CONTRAT par ON (par.NOFAM = c.NOFAM AND par.NMGRPGES = c.NMGRPGES)
            LEFT JOIN
        CONTRAT_TRAVAIL ct ON ct.ID_INDIVIDU = i.ID_INDIVIDU
            INNER JOIN
        AFFILIATION a ON  ((`ct`.`ID_CONTRAT_TRAVAIL` = `a`.`ID_CONTRAT_TRAVAIL`)
                AND (`t`.`NOCAT` = `a`.`CODE_POPULATION`))
				AND ISNULL(IF(`a`.`CODE_POPULATION` = '', NULL, a.CODE_POPULATION)) = 0
			LEFT JOIN
        BASE_ASSUJETTIE ba ON a.ID_AFFILIATION = ba.ID_AFFILIATION
			LEFT JOIN tranche_categorie_base_assujettie tcba ON (tcba.NOCAT = t.NOCAT
                    AND tcba.ID_PERIODE = p.ID_PERIODE
                    AND tcba.ID_BASE_ASSUJETTIE = ba.ID_BASE_ASSUJETTIE)
         LEFT JOIN
         	(SELECT 
                        SUM(ba.MONTANT_COTISATION) as montant_cot,  
								(CASE
            					WHEN (ind.IDENTIFIANT_REPERTOIRE = '') THEN ind.NTT
            					ELSE ind.IDENTIFIANT_REPERTOIRE
        						END) as id_indiv,
        						af.CODE_POPULATION,
        						rdr.id_periode
                    FROM
                        RATTACHEMENT_DECLARATIONS_RECUES rdr
									INNER JOIN 
								INDIVIDU ind ON rdr.ID_ADH_ETAB_MOIS = ind.ID_ADH_ETAB_MOIS
                            INNER JOIN
                        CONTRAT_TRAVAIL ct ON ind.ID_INDIVIDU = ct.ID_INDIVIDU
                            INNER JOIN
                        AFFILIATION af ON ct.ID_CONTRAT_TRAVAIL = af.ID_CONTRAT_TRAVAIL
                            INNER JOIN
                        BASE_ASSUJETTIE ba ON af.ID_AFFILIATION = ba.ID_AFFILIATION
                    WHERE
                    		FIND_IN_SET(rdr.ID_PERIODE, p_liste_ids_periodes) > 0
                     GROUP BY 
								(CASE
            					WHEN (ind.IDENTIFIANT_REPERTOIRE = '') THEN ind.NTT
            					ELSE ind.IDENTIFIANT_REPERTOIRE
        						END),
        						af.CODE_POPULATION, rdr.id_periode
				) as s on s.id_indiv = (CASE
            					WHEN (i.IDENTIFIANT_REPERTOIRE = '') THEN i.NTT
            					ELSE i.IDENTIFIANT_REPERTOIRE
        						END) and s.CODE_POPULATION = a.code_population and s.id_periode = p.id_periode
    WHERE
        p.RECONSOLIDER = 'O' AND t.NATURE_CONTRAT = 2
            AND (t.TMP_BATCH IS FALSE)
            AND par.TYPE_CONSO_SALAIRE = 'INDIV'
            AND (par.TMP_BATCH IS FALSE)
            AND (c.TMP_BATCH IS FALSE)
            AND (c.DT_DEBUT_SIT <= p.DATE_DEBUT_PERIODE)
            AND (COALESCE(c.DT_FIN_SIT, 99999999) >= p.DATE_DEBUT_PERIODE)
            AND ((t.NOCO = p.NUMERO_CONTRAT)
            AND (t.DT_DEBUT_SIT <= p.DATE_DEBUT_PERIODE)
            AND (COALESCE(t.DT_FIN_SIT, 99999999) >= p.DATE_DEBUT_PERIODE)
            AND r.ID_ADH_ETAB_MOIS IN (SELECT 
                adh.ID_ADH_ETAB_MOIS
            FROM
                ADHESION_ETABLISSEMENT_MOIS adh
            WHERE
                adh.ID_ADH_ETAB_MOIS = r.ID_ADH_ETAB_MOIS
                    AND adh.TMP_BATCH IS FALSE))
			AND FIND_IN_SET(p.ID_PERIODE, p_liste_ids_periodes) > 0
    GROUP BY p.ID_PERIODE, INDIVIDU, COALESCE(
        IF( (a.CODE_POPULATION = ''),NULL,a.CODE_POPULATION),
            (SELECT t.NOCAT
             FROM tarifs t
             WHERE (t.NOCO = p.NUMERO_CONTRAT
             AND t.DT_DEBUT_SIT <= p.DATE_DEBUT_PERIODE
             AND COALESCE(t.DT_FIN_SIT, 99999999) >= p.DATE_DEBUT_PERIODE)
             ORDER BY t.DT_DEBUT_SIT
             LIMIT 1))
									

	) AS U GROUP BY ID_PERIODE, INDIVIDU, NOCAT    

    UNION SELECT 
        p.ID_PERIODE AS ID_PERIODE,
        t.NOCAT AS NOCAT,
        'TOUS' AS INDIVIDU,
        t.LICAT AS LICAT,
        t.CONBCOT AS CONBCOT,
        COALESCE(s.montant_cot, 0) AS MONTANT_COTISATION,
        (SELECT 
                SUM(tcba.ESTIMATION_COTISATION)
            FROM
                TRANCHE_CATEGORIE_BASE_ASSUJETTIE tcba
            WHERE
                ((tcba.ID_PERIODE = p.ID_PERIODE)
                    AND (tcba.NOCAT = t.NOCAT))) AS ESTIMATION_COTISATION,
        NULL AS CHAMP_NULL_1,
        NULL AS CHAMP_NULL_2,
		p_audit_nom_batch
    FROM
        (TARIFS t
        JOIN PERIODES_RECUES p ON ((t.NOCO = p.NUMERO_CONTRAT)))
            INNER JOIN
        CONTRATS c ON c.NOCO = t.NOCO
            INNER JOIN
        PARAM_FAMILLE_CONTRAT par ON (par.NOFAM = c.NOFAM AND par.NMGRPGES = c.NMGRPGES)
                 LEFT JOIN
         	(SELECT 
				SUM(ba.MONTANT_COTISATION) as montant_cot,  
						COALESCE(IF( (af.CODE_POPULATION = ''),NULL,af.CODE_POPULATION),
					   (SELECT t.NOCAT
						FROM tarifs t
						WHERE t.NOCO = per.NUMERO_CONTRAT
						AND t.DT_DEBUT_SIT <= per.DATE_DEBUT_PERIODE
						AND COALESCE(t.DT_FIN_SIT, 99999999) >= per.DATE_DEBUT_PERIODE
						ORDER BY t.DT_DEBUT_SIT
						LIMIT 1)) as CODE_POPULATION, rdr.id_periode
				FROM
					RATTACHEMENT_DECLARATIONS_RECUES rdr
					INNER JOIN 
						INDIVIDU ind ON rdr.ID_ADH_ETAB_MOIS = ind.ID_ADH_ETAB_MOIS
					INNER JOIN
						CONTRAT_TRAVAIL ct ON ind.ID_INDIVIDU = ct.ID_INDIVIDU
					INNER JOIN
						AFFILIATION af ON ct.ID_CONTRAT_TRAVAIL = af.ID_CONTRAT_TRAVAIL
					INNER JOIN
						BASE_ASSUJETTIE ba ON af.ID_AFFILIATION = ba.ID_AFFILIATION
					INNER JOIN
					periodes_recues per ON per.ID_PERIODE = rdr.ID_PERIODE
				WHERE
					FIND_IN_SET(rdr.ID_PERIODE, p_liste_ids_periodes) > 0
				GROUP BY COALESCE(IF( (af.CODE_POPULATION = ''),NULL,af.CODE_POPULATION),
									   (SELECT t.NOCAT
										FROM tarifs t
										WHERE t.NOCO = per.NUMERO_CONTRAT
										AND t.DT_DEBUT_SIT <= per.DATE_DEBUT_PERIODE
										AND COALESCE(t.DT_FIN_SIT, 99999999) >= per.DATE_DEBUT_PERIODE
										ORDER BY t.DT_DEBUT_SIT
										LIMIT 1)), rdr.id_periode
				) as s on 
						COALESCE(IF( (s.CODE_POPULATION = ''),NULL,s.CODE_POPULATION),
			            (SELECT t.NOCAT
			             FROM tarifs t
			             WHERE t.NOCO = p.NUMERO_CONTRAT
			             AND t.DT_DEBUT_SIT <= p.DATE_DEBUT_PERIODE
			             AND COALESCE(t.DT_FIN_SIT, 99999999) >= p.DATE_DEBUT_PERIODE
			             ORDER BY t.DT_DEBUT_SIT
			             LIMIT 1)) = t.NOCAT 
				 and s.id_periode = p.id_periode
        
    WHERE
        (t.NOCO = p.NUMERO_CONTRAT
            AND t.DT_DEBUT_SIT <= p.DATE_DEBUT_PERIODE
            AND COALESCE(t.DT_FIN_SIT, 99999999) >= p.DATE_DEBUT_PERIODE
            AND p.RECONSOLIDER = 'O'
            AND t.NATURE_CONTRAT = 2
            AND t.TMP_BATCH IS FALSE
            AND par.TYPE_CONSO_SALAIRE = 'TOUS'
            AND par.TMP_BATCH IS FALSE
            AND c.TMP_BATCH IS FALSE
            AND c.DT_DEBUT_SIT <= p.DATE_DEBUT_PERIODE
            AND COALESCE(c.DT_FIN_SIT, 99999999) >= p.DATE_DEBUT_PERIODE)
        AND FIND_IN_SET(p.ID_PERIODE, p_liste_ids_periodes) > 0
		  ;
END$$
DELIMITER ;

DROP PROCEDURE IF EXISTS Inserer_EFFECTIF_CATEGORIE_MVT;

DELIMITER $$
CREATE PROCEDURE Inserer_EFFECTIF_CATEGORIE_MVT(IN p_audit_nom_batch VARCHAR(20))
BEGIN
-- Calcul et insersion des nouveaux éléments des périodes à traiter
INSERT INTO EFFECTIF_CATEGORIE_MVT (ID_PERIODE, NOCAT, DATE_EFFET_MVT, NB_MOUVEMENT, USER_CREATION)
SELECT ID_PERIODE
, NOCAT
, DATE_CONSERVEE
, NB_MVT_FINAL
, p_audit_nom_batch
FROM V_Effectif_categorie_mouvement_data
WHERE concat(ID_PERIODE, NOCAT, DATE_CONSERVEE) NOT IN (SELECT concat(ID_PERIODE, NOCAT, DATE_SUPPR) FROM V_Effectif_categorie_mouvement_suppression1)
AND concat(ID_PERIODE, NOCAT, DATE_CONSERVEE, NB_MVT_FINAL) NOT IN (SELECT concat(ID_PERIODE, NOCAT, DATE_SUPPR, NB_MVT_INITIAL) FROM V_Effectif_categorie_mouvement_suppression2)
ORDER BY ID_PERIODE, NOCAT, DATE_CONSERVEE;

END$$
DELIMITER ;

DROP PROCEDURE IF EXISTS Inserer_TRANCHE_CATEGORIE_BASE_ASSUJETTIE_Pour_Periodes;

DELIMITER $$
CREATE PROCEDURE Inserer_TRANCHE_CATEGORIE_BASE_ASSUJETTIE_Pour_Periodes(IN p_audit_nom_batch VARCHAR(20), IN p_liste_ids_periodes MEDIUMTEXT)
BEGIN
INSERT INTO TRANCHE_CATEGORIE_BASE_ASSUJETTIE 
(
ID_PERIODE
,ID_BASE_ASSUJETTIE
,MOIS_RATTACHEMENT
,ID_AFFILIATION
,DATE_DEB_RATTACHEMENT
,NUM_TRANCHE
,NOCAT
,LIB_TRANCHE
,MT_TRANCHE
,ESTIMATION_COTISATION
,TMP_BATCH
,USER_CREATION
)
SELECT 
    sel.ID_PERIODE,
    sel.ID_BASE_ASSUJETTIE,
    sel.MOIS_RATTACHEMENT,
    sel.ID_AFFILIATION,
    sel.DATE_DEB_RATTACHEMENT,
    p.NUM_TRANCHE,
    t.NOCAT,
    p.LIB_TRANCHE,
    @MT_TRANCHE_SAVE:=COALESCE((SELECT 
                    SUM(c.MONTANT_COMPO_BASE_ASSUJ)
                FROM
                    COMPOSANT_BASE_ASSUJETTIE c
                WHERE
                    c.ID_BASE_ASSUJETTIE = sel.ID_BASE_ASSUJETTIE
                        AND c.TYPE_COMPO_BASE_ASSUJ IN (SELECT 
                            pc.NUM_TCBA
                        FROM
                            PARAM_NATURE_BASE_COMPOSANTS pc
                        WHERE
                            p.CONBCOT = pc.CONBCOT
                                AND p.NUM_TRANCHE = pc.NUM_TRANCHE
                                AND p.TXCALCU_REMPLI = pc.TXCALCU_REMPLI
                                AND pc.mode_conso = 'S'
                                AND pc.TMP_BATCH IS FALSE)
                        AND c.TMP_BATCH IS FALSE),
            0) AS MT_TRANCHE,
    CAST((@MT_TRANCHE_SAVE * 
    ((CASE 
    WHEN p.NUM_TRANCHE = 1 THEN t.TXBASE1
    WHEN p.NUM_TRANCHE = 2 THEN t.TXBASE2
    WHEN p.NUM_TRANCHE = 3 THEN t.TXBASE3
    WHEN p.NUM_TRANCHE = 4 THEN t.TXBASE4
    END) / 1000000 )
    * (c.TXAPPCOT / 10000)) AS DECIMAL(15,2)) AS ESTIMATION_COTISATION,
    sel.tmp_batch,
    p_audit_nom_batch
FROM
    V_Selection_base_assujettie sel
        LEFT JOIN
    TARIFS t ON sel.NUMERO_CONTRAT = t.NOCO
        AND sel.NOCAT = t.nocat
        AND t.DT_DEBUT_SIT <= sel.DATE_FIN_RATTACHEMENT
        AND (COALESCE(t.DT_FIN_SIT, 99999999) >= sel.DATE_FIN_RATTACHEMENT
        OR (CASE
        WHEN (t.DT_FIN_SIT = 0) THEN 99999999
        ELSE t.DT_FIN_SIT
    END) >= sel.DATE_FIN_RATTACHEMENT)
        LEFT JOIN
    PARAM_NATURE_BASE p ON t.CONBCOT = p.CONBCOT
        AND (CASE
        WHEN COALESCE(t.txcalcu, 0) > 0 THEN 'O'
        ELSE 'N'
    END) = p.TXCALCU_REMPLI
        INNER JOIN
        CONTRATS c ON (c.TMP_BATCH IS FALSE
	AND c.NOCO = sel.NUMERO_CONTRAT
    AND c.DT_DEBUT_SIT <= sel.DATE_DEBUT_PERIODE
        AND COALESCE(c.DT_FIN_SIT, 99999999) >= sel.DATE_DEBUT_PERIODE)
WHERE
    NOT ISNULL(t.NOCO)
        AND NOT ISNULL(t.NOCAT)
        AND NOT ISNULL(p.NUM_TRANCHE)
        AND FIND_IN_SET(sel.ID_PERIODE, p_liste_ids_periodes) > 0
ORDER BY ID_PERIODE , ID_BASE_ASSUJETTIE;
END$$
DELIMITER ;

DROP PROCEDURE IF EXISTS Inserer_TRANCHE_CATEGORIE_Pour_Periodes;

DELIMITER $$
CREATE PROCEDURE Inserer_TRANCHE_CATEGORIE_Pour_Periodes(IN p_audit_nom_batch VARCHAR(20), IN p_liste_ids_periodes MEDIUMTEXT)
BEGIN
-- Calcul et insersion des nouveaux éléments des périodes à traiter
INSERT INTO TRANCHE_CATEGORIE (ID_PERIODE, NOCAT, INDIVIDU, NUM_TRANCHE, LIB_TRANCHE, MT_TRANCHE, USER_CREATION)
select c.ID_PERIODE, c.NOCAT, c.INDIVIDU, p.NUM_TRANCHE, p.LIB_TRANCHE, 
coalesce(case WHEN (c.INDIVIDU = 'TOUS') THEN (SELECT SUM(tcba.MT_TRANCHE) 
FROM TRANCHE_CATEGORIE_BASE_ASSUJETTIE tcba 
WHERE tcba.ID_PERIODE = c.ID_PERIODE
AND tcba.NOCAT = c.NOCAT
AND tcba.NUM_TRANCHE = p.NUM_TRANCHE) 
ELSE (SELECT SUM(tcba.MT_TRANCHE) 
FROM TRANCHE_CATEGORIE_BASE_ASSUJETTIE tcba 
LEFT JOIN AFFILIATION a ON tcba.ID_AFFILIATION = a.ID_AFFILIATION
LEFT JOIN CONTRAT_TRAVAIL ct ON a.ID_CONTRAT_TRAVAIL = ct.ID_CONTRAT_TRAVAIL
LEFT JOIN INDIVIDU i ON ct.ID_INDIVIDU = i.ID_INDIVIDU 
WHERE tcba.ID_PERIODE = c.ID_PERIODE
AND tcba.NOCAT = c.NOCAT
AND tcba.NUM_TRANCHE = p.NUM_TRANCHE
AND (i.NTT  = c.INDIVIDU)) END, 0) +
coalesce(case WHEN (c.INDIVIDU = 'TOUS') THEN (null) 
ELSE (SELECT SUM(tcba.MT_TRANCHE) 
FROM TRANCHE_CATEGORIE_BASE_ASSUJETTIE tcba 
LEFT JOIN AFFILIATION a ON tcba.ID_AFFILIATION = a.ID_AFFILIATION
LEFT JOIN CONTRAT_TRAVAIL ct ON a.ID_CONTRAT_TRAVAIL = ct.ID_CONTRAT_TRAVAIL
LEFT JOIN INDIVIDU i ON ct.ID_INDIVIDU = i.ID_INDIVIDU 
WHERE tcba.ID_PERIODE = c.ID_PERIODE
AND tcba.NOCAT = c.NOCAT
AND tcba.NUM_TRANCHE = p.NUM_TRANCHE
AND (i.IDENTIFIANT_REPERTOIRE  = c.INDIVIDU)) END, 0) as MT_TRANCHE,
p_audit_nom_batch
from CATEGORIE_QUITTANCEMENT_INDIVIDU c
INNER JOIN (select distinct pnb.NUM_TRANCHE, pnb.LIB_TRANCHE, pnb.CONBCOT from PARAM_NATURE_BASE pnb) p ON p.CONBCOT = c.CONBCOT
INNER JOIN V_Selection_periode_nature_contrat_salaires s on s.ID_PERIODE = c.ID_PERIODE
WHERE FIND_IN_SET(c.ID_PERIODE, p_liste_ids_periodes) > 0;
END$$
DELIMITER ;

DROP PROCEDURE IF EXISTS Purger_elements_declaratifs_en_masse;

DELIMITER $$
CREATE PROCEDURE Purger_elements_declaratifs_en_masse(IN p_liste_ids_periodes MEDIUMTEXT)
BEGIN
	IF p_liste_ids_periodes = 'ALL' THEN BEGIN
		DELETE ecm FROM EFFECTIF_CATEGORIE_MVT ecm 
		INNER JOIN V_Selection_periode_nature_contrat_effectifs s ON s.ID_PERIODE = ecm.ID_PERIODE; 

		DELETE cqi FROM CATEGORIE_QUITTANCEMENT_INDIVIDU cqi
		INNER JOIN V_Selection_periode_nature_contrat_effectifs s ON s.ID_PERIODE = cqi.ID_PERIODE; 

		DELETE ecct FROM EFFECTIF_CATEGORIE_CONTRAT_TRAVAIL ecct
		INNER JOIN V_Selection_periode_nature_contrat_effectifs s ON s.ID_PERIODE = ecct.ID_PERIODE; 


		DELETE tc FROM TRANCHE_CATEGORIE tc
		INNER JOIN V_Selection_periode_nature_contrat_salaires s ON s.ID_PERIODE = tc.ID_PERIODE; 

		DELETE cqi FROM CATEGORIE_QUITTANCEMENT_INDIVIDU cqi
		INNER JOIN V_Selection_periode_nature_contrat_salaires s ON s.ID_PERIODE = cqi.ID_PERIODE; 

		DELETE tcba FROM TRANCHE_CATEGORIE_BASE_ASSUJETTIE tcba
		INNER JOIN V_Selection_periode_nature_contrat_salaires s ON s.ID_PERIODE = tcba.ID_PERIODE; 

		END;
	ELSE BEGIN
		DELETE FROM CATEGORIE_QUITTANCEMENT_INDIVIDU
		WHERE FIND_IN_SET(ID_PERIODE, p_liste_ids_periodes) > 0;

		DELETE FROM EFFECTIF_CATEGORIE_MVT
		WHERE FIND_IN_SET(ID_PERIODE, p_liste_ids_periodes) > 0;

		DELETE FROM TRANCHE_CATEGORIE
		WHERE FIND_IN_SET(ID_PERIODE, p_liste_ids_periodes) > 0;

		DELETE FROM EFFECTIF_CATEGORIE_CONTRAT_TRAVAIL
		WHERE FIND_IN_SET(ID_PERIODE, p_liste_ids_periodes) > 0;

		DELETE FROM TRANCHE_CATEGORIE_BASE_ASSUJETTIE
		WHERE FIND_IN_SET(ID_PERIODE, p_liste_ids_periodes) > 0;		
		END;
	END IF;
END$$
DELIMITER ;

DROP PROCEDURE IF EXISTS Creation_MessageControle_en_masse;
DELIMITER $$
CREATE PROCEDURE Creation_MessageControle_en_masse(IN p_liste_ids_periodes MEDIUMTEXT,IN message CHAR(100) ,IN idControle VARCHAR(30), IN param1 CHAR(50),IN param2 CHAR(50),IN param3 CHAR(50),IN param4 CHAR(50), IN niveau CHAR(10) , IN origine CHAR(6) , IN dateJour DECIMAL(8,0) , IN userCreation VARCHAR(100))
BEGIN
	
INSERT INTO message_controle(ID_PERIODE,ID_CONTROLE,ORIGINE,DATE_TRAITEMENT,NIVEAU_ALERTE,MESSAGE_UTILISATEUR,PARAM1,PARAM2,PARAM3,PARAM4,USER_CREATION)
SELECT p.ID_PERIODE,idControle,origine,dateJour,niveau,message,param1,param2,param3,param4,userCreation
FROM periodes_recues p WHERE FIND_IN_SET(p.ID_PERIODE, p_liste_ids_periodes) > 0 AND p.ID_PERIODE NOT IN(SELECT m.ID_PERIODE from message_controle m WHERE m.ID_CONTROLE=(idControle COLLATE latin1_general_ci)  AND m.ORIGINE=(origine COLLATE latin1_general_ci) AND FIND_IN_SET(m.ID_PERIODE, p_liste_ids_periodes) > 0 );

END$$
DELIMITER ;


DROP PROCEDURE IF EXISTS Creation_HistoriqueEtatPeriode_en_masse;
DELIMITER $$
CREATE PROCEDURE Creation_HistoriqueEtatPeriode_en_masse(IN p_liste_ids_periodes MEDIUMTEXT,IN datehms NUMERIC(14), IN etatPeriode CHAR(3), IN origine CHAR(4),
IN utilisateur CHAR(30), IN commentaire VARCHAR(1000), IN userCreation VARCHAR(20))
BEGIN
	
INSERT INTO HISTO_ETAT_PERIODE(ID_PERIODE, DATEHMS_CHANGEMENT, ETAT_PERIODE, ORIGINE, UTILISATEUR, COMMENTAIRE, USER_CREATION)
SELECT p.ID_PERIODE,datehms,etatPeriode,origine,utilisateur,commentaire,userCreation
FROM periodes_recues p WHERE FIND_IN_SET(ID_PERIODE, p_liste_ids_periodes) > 0;

END$$
DELIMITER ;


DROP PROCEDURE IF EXISTS Creation_HistoriqueCommentairePeriode_en_masse;
DELIMITER $$
CREATE PROCEDURE Creation_HistoriqueCommentairePeriode_en_masse(IN p_liste_ids_periodes MEDIUMTEXT,IN datehms NUMERIC(14), IN utilisateur CHAR(30), IN commentaire VARCHAR(1000), IN userCreation VARCHAR(20))
BEGIN
	
INSERT INTO HISTO_COMMENTAIRE_PERIODE(ID_PERIODE, UTILISATEUR, DATEHMS_SAISIE, COMMENTAIRE, USER_CREATION)
SELECT p.ID_PERIODE, utilisateur, datehms, commentaire, userCreation
FROM periodes_recues p WHERE FIND_IN_SET(ID_PERIODE, p_liste_ids_periodes) > 0;

END$$
DELIMITER ;


DROP PROCEDURE IF EXISTS Creation_HistoriqueAssignationPeriode_en_masse;
DELIMITER $$
CREATE PROCEDURE Creation_HistoriqueAssignationPeriode_en_masse(IN p_liste_ids_periodes MEDIUMTEXT,IN datehms NUMERIC(14), IN aTraiterPar CHAR(30), IN userCreation VARCHAR(20))
BEGIN
	
INSERT INTO HISTO_ASSIGNATION_PERIODE(ID_PERIODE, A_TRAITER_PAR, DATEHMS_ASSIGNE, USER_CREATION)
SELECT p.ID_PERIODE, aTraiterPar, datehms, userCreation
FROM periodes_recues p WHERE FIND_IN_SET(ID_PERIODE, p_liste_ids_periodes) > 0;

END$$
DELIMITER ;


DROP PROCEDURE IF EXISTS Creation_HistoriqueAttenteRetourEtpPeriode_en_masse;
DELIMITER $$
CREATE PROCEDURE Creation_HistoriqueAttenteRetourEtpPeriode_en_masse(IN p_liste_ids_periodes MEDIUMTEXT,IN datehms NUMERIC(14), IN utilisateur CHAR(30), IN action VARCHAR(20), IN userCreation VARCHAR(20))
BEGIN
	
INSERT INTO HISTO_ATTENTE_RETOUR_ETP_PERIODE(ID_PERIODE, ACTION, UTILISATEUR, DATEHMS_RETOUR, USER_CREATION)
SELECT p.ID_PERIODE, action, utilisateur, datehms, userCreation
FROM periodes_recues p WHERE FIND_IN_SET(ID_PERIODE, p_liste_ids_periodes) > 0
AND action collate latin1_general_ci != coalesce((SELECT h.action FROM histo_attente_retour_etp_periode h WHERE h.ID_PERIODE=p.ID_PERIODE ORDER BY h.DT_CREATION DESC LIMIT 1), 'NA');

END$$
DELIMITER ;


DROP PROCEDURE IF EXISTS Calcul_effectif_indiv_salaire_pour_periodes;
DELIMITER $$
CREATE PROCEDURE Calcul_effectif_indiv_salaire_pour_periodes(IN p_audit_nom_batch VARCHAR(20), in p_liste_ids_periodes MEDIUMTEXT)
BEGIN

DECLARE done INT DEFAULT FALSE;
DECLARE C_NO_CAT char(30);

DECLARE C_ID_PERIODE bigint(20);
DECLARE ID_PER_MAJ bigint(20);
DECLARE EFF_DEB_MAJ decimal(10,0);
DECLARE EFF_FIN_MAJ decimal(10,0);

DECLARE liste_nocat_unique CURSOR FOR
(SELECT cqi.ID_PERIODE,cqi.NOCAT
FROM categorie_quittancement_individu cqi INNER JOIN V_selection_periode_nature_contrat_salaires vpns ON (cqi.ID_PERIODE=vpns.ID_PERIODE)
WHERE cqi.individu='TOUS'
AND FIND_IN_SET(cqi.ID_PERIODE, p_liste_ids_periodes)
GROUP BY cqi.ID_PERIODE
HAVING COUNT(DISTINCT cqi.NOCAT)<=1);

DECLARE liste_nocat_non_unique CURSOR FOR
(SELECT cqi.ID_PERIODE,cqi.NOCAT
FROM categorie_quittancement_individu cqi INNER JOIN V_selection_periode_nature_contrat_salaires vpns ON (cqi.ID_PERIODE=vpns.ID_PERIODE)
WHERE cqi.individu='TOUS'
 AND FIND_IN_SET(cqi.ID_PERIODE, p_liste_ids_periodes)
 AND (SELECT COUNT(DISTINCT cqi2.NOCAT) FROM categorie_quittancement_individu cqi2 WHERE cqi2.ID_PERIODE=cqi.ID_PERIODE)>1
GROUP BY cqi.ID_PERIODE, cqi.NOCAT);

DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

OPEN liste_nocat_unique;
IF (Select FOUND_ROWS()) > 0 THEN
	nocat_uni: LOOP 
	FETCH liste_nocat_unique INTO C_ID_PERIODE, C_NO_CAT;
		IF done THEN 
			LEAVE nocat_uni;
		END IF;
		-- Calcul effectifs
		SELECT 
    COUNT(DISTINCT (CASE
            WHEN (i.IDENTIFIANT_REPERTOIRE = '') THEN i.NTT
            ELSE i.IDENTIFIANT_REPERTOIRE
        END)), p.ID_PERIODE INTO EFF_DEB_MAJ, ID_PER_MAJ
FROM
    periodes_recues p
        INNER JOIN
    rattachement_declarations_recues rda ON (p.ID_PERIODE = rda.ID_PERIODE)
        INNER JOIN
    adhesion_etablissement_mois adh ON (adh.ID_ADH_ETAB_MOIS = rda.ID_ADH_ETAB_MOIS)
        INNER JOIN
    individu i ON (i.ID_ADH_ETAB_MOIS = adh.ID_ADH_ETAB_MOIS)
		INNER JOIN
	contrat_travail ct ON (ct.ID_INDIVIDU = i.ID_INDIVIDU)
		INNER JOIN
	affiliation a ON (a.ID_CONTRAT_TRAVAIL = ct.ID_CONTRAT_TRAVAIL)
		INNER JOIN
	base_assujettie ba ON (ba.ID_AFFILIATION = a.ID_AFFILIATION AND ba.DATE_DEB_RATTACHEMENT = p.DATE_DEBUT_PERIODE)
WHERE
    p.ID_PERIODE = C_ID_PERIODE;
					
		SELECT 
    COUNT(DISTINCT (CASE
            WHEN (i.IDENTIFIANT_REPERTOIRE = '') THEN i.NTT
            ELSE i.IDENTIFIANT_REPERTOIRE
        END)) INTO EFF_FIN_MAJ
FROM
    periodes_recues p
        INNER JOIN
    rattachement_declarations_recues rda ON (p.ID_PERIODE = rda.ID_PERIODE)
        INNER JOIN
    adhesion_etablissement_mois adh ON (adh.ID_ADH_ETAB_MOIS = rda.ID_ADH_ETAB_MOIS)
        INNER JOIN
    individu i ON (i.ID_ADH_ETAB_MOIS = adh.ID_ADH_ETAB_MOIS)
		INNER JOIN
	contrat_travail ct ON (ct.ID_INDIVIDU = i.ID_INDIVIDU)
		INNER JOIN
	affiliation a ON (a.ID_CONTRAT_TRAVAIL = ct.ID_CONTRAT_TRAVAIL)
		INNER JOIN
	base_assujettie ba ON (ba.ID_AFFILIATION = a.ID_AFFILIATION AND ba.DATE_FIN_RATTACHEMENT = p.DATE_FIN_PERIODE)
WHERE
    p.ID_PERIODE = C_ID_PERIODE;
        
UPDATE categorie_quittancement_individu cqi3
SET 
	cqi3.EFFECTIF_DEBUT = EFF_DEB_MAJ,
	cqi3.EFFECTIF_FIN = EFF_FIN_MAJ
WHERE
	cqi3.ID_PERIODE = ID_PER_MAJ;
END LOOP;
END IF;
CLOSE liste_nocat_unique;


BEGIN
DECLARE done2 INT DEFAULT FALSE;
DECLARE CONTINUE HANDLER FOR NOT FOUND SET done2 = TRUE;

OPEN liste_nocat_non_unique;
IF (SELECT FOUND_ROWS()) > 0 THEN
	nocat_non_uni : LOOP 
	FETCH liste_nocat_non_unique INTO C_ID_PERIODE, C_NO_CAT;
		IF done2 THEN 
			LEAVE nocat_non_uni;
		END IF;
		-- Calcul effectifs 
		SELECT 
    COUNT(DISTINCT (CASE
            WHEN (i.IDENTIFIANT_REPERTOIRE = '') THEN i.NTT
            ELSE i.IDENTIFIANT_REPERTOIRE
        END)), p.ID_PERIODE INTO EFF_DEB_MAJ, ID_PER_MAJ
FROM
    periodes_recues p
        INNER JOIN
    rattachement_declarations_recues rda ON (p.ID_PERIODE = rda.ID_PERIODE)
        INNER JOIN
    adhesion_etablissement_mois adh ON (adh.ID_ADH_ETAB_MOIS = rda.ID_ADH_ETAB_MOIS)
        INNER JOIN
    individu i ON (i.ID_ADH_ETAB_MOIS = adh.ID_ADH_ETAB_MOIS)
		INNER JOIN
	contrat_travail ct ON (ct.ID_INDIVIDU = i.ID_INDIVIDU)
		INNER JOIN
	affiliation a ON (a.ID_CONTRAT_TRAVAIL = ct.ID_CONTRAT_TRAVAIL)
		INNER JOIN
	base_assujettie ba ON (ba.ID_AFFILIATION = a.ID_AFFILIATION AND ba.DATE_DEB_RATTACHEMENT = p.DATE_DEBUT_PERIODE)
WHERE
    p.ID_PERIODE = C_ID_PERIODE
	AND (C_NO_CAT collate latin1_general_ci) = (COALESCE(IF((a.CODE_POPULATION = ''),
                    NULL,
                    a.CODE_POPULATION),
                (SELECT 
                        t.NOCAT
                    FROM
                        tarifs t
                    WHERE
                        ((t.NOCO = p.NUMERO_CONTRAT)
                            AND (t.DT_DEBUT_SIT <= p.DATE_DEBUT_PERIODE)
                            AND ((COALESCE(t.DT_FIN_SIT, 99999999) >= p.DATE_DEBUT_PERIODE)
                            OR ((CASE
                            WHEN (t.DT_FIN_SIT = 0) THEN 99999999
                            ELSE t.DT_FIN_SIT
                        END) >= p.DATE_DEBUT_PERIODE)))
                    ORDER BY t.DT_DEBUT_SIT
                    LIMIT 1)));

SELECT 
    COUNT(DISTINCT (CASE
            WHEN (i.IDENTIFIANT_REPERTOIRE = '') THEN i.NTT
            ELSE i.IDENTIFIANT_REPERTOIRE
        END)) INTO EFF_FIN_MAJ
FROM
periodes_recues p
        INNER JOIN
    rattachement_declarations_recues rda ON (p.ID_PERIODE = rda.ID_PERIODE)
        INNER JOIN
    adhesion_etablissement_mois adh ON (adh.ID_ADH_ETAB_MOIS = rda.ID_ADH_ETAB_MOIS)
        INNER JOIN
    individu i ON (i.ID_ADH_ETAB_MOIS = adh.ID_ADH_ETAB_MOIS)
		INNER JOIN
	contrat_travail ct ON (ct.ID_INDIVIDU = i.ID_INDIVIDU)
		INNER JOIN
	affiliation a ON (a.ID_CONTRAT_TRAVAIL = ct.ID_CONTRAT_TRAVAIL)
		INNER JOIN
	base_assujettie ba ON (ba.ID_AFFILIATION = a.ID_AFFILIATION AND ba.DATE_FIN_RATTACHEMENT = p.DATE_FIN_PERIODE)
WHERE
    p.ID_PERIODE = C_ID_PERIODE
	AND (C_NO_CAT collate latin1_general_ci) = (COALESCE(IF((a.CODE_POPULATION = ''),
                    NULL,
                    a.CODE_POPULATION),
                (SELECT 
                        t.NOCAT
                    FROM
                        tarifs t
                    WHERE
                        ((t.NOCO = p.NUMERO_CONTRAT)
                            AND (t.DT_DEBUT_SIT <= p.DATE_DEBUT_PERIODE)
                            AND ((COALESCE(t.DT_FIN_SIT, 99999999) >= p.DATE_DEBUT_PERIODE)
                            OR ((CASE
                            WHEN (t.DT_FIN_SIT = 0) THEN 99999999
                            ELSE t.DT_FIN_SIT
                        END) >= p.DATE_DEBUT_PERIODE)))
                    ORDER BY t.DT_DEBUT_SIT
                    LIMIT 1)));

		UPDATE categorie_quittancement_individu cqi3 
SET 
    cqi3.EFFECTIF_DEBUT = EFF_DEB_MAJ,
    cqi3.EFFECTIF_FIN = EFF_FIN_MAJ
WHERE
    cqi3.ID_PERIODE = ID_PER_MAJ
AND cqi3.NOCAT = (C_NO_CAT collate latin1_general_ci);
	END LOOP;
END IF;
CLOSE liste_nocat_non_unique;
END;
END$$
DELIMITER ;
DROP PROCEDURE IF EXISTS Recherche_gestionnaire_siren;
DELIMITER $$
CREATE PROCEDURE Recherche_gestionnaire_siren()
BEGIN
-- ####################################################################
-- ####################################################################
-- CAS 1.1 + Cas 2.1 : 1 groupe et 1 gestionnaire existant
-- ####################################################################
-- ####################################################################
select distinct tmp_siren.NOSIREN, pug.code_user, pug.NOM, pug.PRENOM, pug2.NIVEAU_1, pug2.NIVEAU_2, pug2.NIVEAU_3
from  (
		select cl.nosiren, group_concat( distinct co.nmgrpges) gg, group_concat( distinct pr.a_traiter_par) gest
		from 
			(	select distinct contrats.noco as noco, clients.nosiren as nosiren from clients inner join contrats on clients.nocli = contrats.nocli
			union 
				select distinct ext.noco as noco, ext.siren as nosiren from extensions_entreprises_affiliees ext
			) as cl
		inner join contrats co on cl.noco = co.noco and co.DT_FIN_SIT is null and co.tmp_batch IS FALSE AND (co.eligdsn = 'SRN' OR co.eligdsn = 'SRT')
		inner join param_etat_contrat p on p.COETACO = co.coetaco and co.tmp_batch = p.tmp_batch AND p.ACTIF = 'O'
		left join (
					select distinct a_traiter_par, p.numero_contrat from periodes_recues p
					inner join contrats ct on p.NUMERO_CONTRAT = ct.NOCO
					inner join (
						select p1.numero_contrat, tmp1.dt_rec, max(p1.dt_creation) dt_crea 
						from periodes_recues p1
						inner join (
							select numero_contrat, max(date_debut_periode) dt_rec
							from periodes_recues
							group by numero_contrat
						) as tmp1 on tmp1.numero_contrat = p1.numero_contrat and tmp1.dt_rec = p1.DATE_DEBUT_PERIODE
						group by numero_contrat, tmp1.dt_rec
					) as tmp2 on tmp2.numero_contrat = p.numero_contrat and tmp2.dt_rec = p.DATE_DEBUT_PERIODE and tmp2.dt_crea = p.DT_CREATION
				) as pr on pr.numero_contrat = co.noco
		where cl.nosiren not in (select siren from param_siren_faux)
		group by cl.nosiren
		having count( distinct co.nmgrpges) = 1 and count( distinct pr.a_traiter_par) = 1
		) as tmp_siren
inner join param_utilisateur_gestionnaire pug on pug.code_user = tmp_siren.gest and pug.GRP_GEST = tmp_siren.gg
inner join param_utilisateur_gestionnaire pug2 on pug2.TYPE = 'GEST_GU' and pug2.GG_EGAL = tmp_siren.gg
where exists (select 1 from param_utilisateur_gestionnaire pug 
		WHERE pug.TYPE = 'GEST_AUTRE'
		AND pug.GG_INCLUS_1 = '' and pug.GG_INCLUS_2 = '' and pug.GG_EXCLUS <> ''
		AND pug.GG_EXCLUS like concat('%', tmp_siren.gg, '%')
	)

union

select distinct tmp_siren.NOSIREN, concat(pug2.GG_EGAL, pug.code_user), pug.NOM, pug.PRENOM, pug2.NIVEAU_1, pug2.NIVEAU_2, pug2.NIVEAU_3
from  (
		select cl.nosiren, group_concat( distinct co.nmgrpges) gg, group_concat( distinct pr.a_traiter_par) gest
		from 
			(	select distinct contrats.noco as noco, clients.nosiren as nosiren from clients inner join contrats on clients.nocli = contrats.nocli
			union 
				select distinct ext.noco as noco, ext.siren as nosiren from extensions_entreprises_affiliees ext
			) as cl
		inner join contrats co on cl.noco = co.noco and co.DT_FIN_SIT is null and co.tmp_batch IS FALSE AND (co.eligdsn = 'SRN' OR co.eligdsn = 'SRT')
		inner join param_etat_contrat p on p.COETACO = co.coetaco and co.tmp_batch = p.tmp_batch AND p.ACTIF = 'O'
		left join (
					select distinct a_traiter_par, p.numero_contrat from periodes_recues p
					inner join contrats ct on p.NUMERO_CONTRAT = ct.NOCO
					inner join (
						select p1.numero_contrat, tmp1.dt_rec, max(p1.dt_creation) dt_crea 
						from periodes_recues p1
						inner join (
							select numero_contrat, max(date_debut_periode) dt_rec
							from periodes_recues
							group by numero_contrat
						) as tmp1 on tmp1.numero_contrat = p1.numero_contrat and tmp1.dt_rec = p1.DATE_DEBUT_PERIODE
						group by numero_contrat, tmp1.dt_rec
					) as tmp2 on tmp2.numero_contrat = p.numero_contrat and tmp2.dt_rec = p.DATE_DEBUT_PERIODE and tmp2.dt_crea = p.DT_CREATION
				) as pr on pr.numero_contrat = co.noco
		where cl.nosiren not in (select siren from param_siren_faux)
		group by cl.nosiren
		having count( distinct co.nmgrpges) = 1 and count( distinct pr.a_traiter_par) = 1
		) as tmp_siren
inner join param_utilisateur_gestionnaire pug on pug.code_user = tmp_siren.gest and pug.GRP_GEST <> tmp_siren.gg
inner join param_utilisateur_gestionnaire pug2 on pug2.TYPE = 'GEST_GU' and pug2.GG_EGAL = tmp_siren.gg
where exists (select 1 from param_utilisateur_gestionnaire pug 
		WHERE pug.TYPE = 'GEST_AUTRE'
		AND pug.GG_INCLUS_1 = '' and pug.GG_INCLUS_2 = '' and pug.GG_EXCLUS <> ''
		AND pug.GG_EXCLUS like concat('%', tmp_siren.gg, '%')
	)


-- ####################################################################
-- ####################################################################
-- CAS 1.2 + Cas 2.2 : 1 groupe et 1 gestionnaire non existant
-- ####################################################################
-- ####################################################################
union

select distinct tmp_siren.NOSIREN, pug2.code_user, pug2.NOM, pug2.PRENOM, pug2.NIVEAU_1, pug2.NIVEAU_2, pug2.NIVEAU_3
from  (
		select cl.nosiren, group_concat( distinct co.nmgrpges) gg, group_concat( distinct pr.a_traiter_par) gest
		from 
			(	select distinct contrats.noco as noco, clients.nosiren as nosiren from clients inner join contrats on clients.nocli = contrats.nocli
			union 
				select distinct ext.noco as noco, ext.siren as nosiren from extensions_entreprises_affiliees ext
			) as cl
		inner join contrats co on cl.noco = co.noco and co.DT_FIN_SIT is null and co.tmp_batch IS FALSE AND (co.eligdsn = 'SRN' OR co.eligdsn = 'SRT')
		inner join param_etat_contrat p on p.COETACO = co.coetaco and co.tmp_batch = p.tmp_batch AND p.ACTIF = 'O'
		left join (
					select distinct a_traiter_par, p.numero_contrat from periodes_recues p
					inner join contrats ct on p.NUMERO_CONTRAT = ct.NOCO
					inner join (
						select p1.numero_contrat, tmp1.dt_rec, max(p1.dt_creation) dt_crea 
						from periodes_recues p1
						inner join (
							select numero_contrat, max(date_debut_periode) dt_rec
							from periodes_recues
							group by numero_contrat
						) as tmp1 on tmp1.numero_contrat = p1.numero_contrat and tmp1.dt_rec = p1.DATE_DEBUT_PERIODE
						group by numero_contrat, tmp1.dt_rec
					) as tmp2 on tmp2.numero_contrat = p.numero_contrat and tmp2.dt_rec = p.DATE_DEBUT_PERIODE and tmp2.dt_crea = p.DT_CREATION
				) as pr on pr.numero_contrat = co.noco
		where cl.nosiren not in (select siren from param_siren_faux)
		group by cl.nosiren
		having count( distinct co.nmgrpges) = 1 and count( distinct pr.a_traiter_par) = 1
		) as tmp_siren
left join param_utilisateur_gestionnaire pug on pug.code_user = tmp_siren.gest
inner join param_utilisateur_gestionnaire pug2 on pug2.GG_EGAL =  tmp_siren.gg and pug2.`TYPE` = 'AUTRE_GU'
where pug.code_user is null and tmp_siren.gest is not null



-- ####################################################################
-- ####################################################################
-- CAS 1.3 + Cas 2.3 : 1 groupe et 0 gestionnaire assigné
-- ####################################################################
-- ####################################################################
union

select distinct tmp_siren.NOSIREN, pug2.code_user, pug2.NOM, pug2.PRENOM, pug2.NIVEAU_1, pug2.NIVEAU_2, pug2.NIVEAU_3
from  (
		select cl.nosiren, group_concat( distinct co.nmgrpges) gg, group_concat( distinct pr.a_traiter_par) gest
		from 
			(	select distinct contrats.noco as noco, clients.nosiren as nosiren from clients inner join contrats on clients.nocli = contrats.nocli
			union 
				select distinct ext.noco as noco, ext.siren as nosiren from extensions_entreprises_affiliees ext
			) as cl
		inner join contrats co on cl.noco = co.noco and co.DT_FIN_SIT is null and co.tmp_batch IS FALSE AND (co.eligdsn = 'SRN' OR co.eligdsn = 'SRT')
		inner join param_etat_contrat p on p.COETACO = co.coetaco and co.tmp_batch = p.tmp_batch AND p.ACTIF = 'O'
		left join (
					select distinct a_traiter_par, p.numero_contrat from periodes_recues p
					inner join contrats ct on p.NUMERO_CONTRAT = ct.NOCO
					inner join (
						select p1.numero_contrat, tmp1.dt_rec, max(p1.dt_creation) dt_crea 
						from periodes_recues p1
						inner join (
							select numero_contrat, max(date_debut_periode) dt_rec
							from periodes_recues
							group by numero_contrat
						) as tmp1 on tmp1.numero_contrat = p1.numero_contrat and tmp1.dt_rec = p1.DATE_DEBUT_PERIODE
						group by numero_contrat, tmp1.dt_rec
					) as tmp2 on tmp2.numero_contrat = p.numero_contrat and tmp2.dt_rec = p.DATE_DEBUT_PERIODE and tmp2.dt_crea = p.DT_CREATION
				) as pr on pr.numero_contrat = co.noco
		where cl.nosiren not in (select siren from param_siren_faux)
		group by cl.nosiren
		having count( distinct co.nmgrpges) = 1 and count( distinct pr.a_traiter_par) = 0
		) as tmp_siren
inner join param_utilisateur_gestionnaire pug2 on pug2.GG_EGAL =  tmp_siren.gg and pug2.`TYPE` = 'NON_ASS_GU'



-- ####################################################################
-- ####################################################################
-- CAS 2.4 : 1 groupe et plusieurs gestionnaires
-- ####################################################################
-- ####################################################################
union

select distinct tmp_siren.NOSIREN, pug2.code_user, pug2.NOM, pug2.PRENOM, pug2.NIVEAU_1, pug2.NIVEAU_2, pug2.NIVEAU_3
from  (
		select cl.nosiren, group_concat( distinct co.nmgrpges) gg, group_concat( distinct pr.a_traiter_par) gest
		from 
			(	select distinct contrats.noco as noco, clients.nosiren as nosiren from clients inner join contrats on clients.nocli = contrats.nocli
			union 
				select distinct ext.noco as noco, ext.siren as nosiren from extensions_entreprises_affiliees ext
			) as cl
		inner join contrats co on cl.noco = co.noco and co.DT_FIN_SIT is null and co.tmp_batch IS FALSE AND (co.eligdsn = 'SRN' OR co.eligdsn = 'SRT')
		inner join param_etat_contrat p on p.COETACO = co.coetaco and co.tmp_batch = p.tmp_batch AND p.ACTIF = 'O'
		left join (
					select distinct a_traiter_par, p.numero_contrat from periodes_recues p
					inner join contrats ct on p.NUMERO_CONTRAT = ct.NOCO
					inner join (
						select p1.numero_contrat, tmp1.dt_rec, max(p1.dt_creation) dt_crea 
						from periodes_recues p1
						inner join (
							select numero_contrat, max(date_debut_periode) dt_rec
							from periodes_recues
							group by numero_contrat
						) as tmp1 on tmp1.numero_contrat = p1.numero_contrat and tmp1.dt_rec = p1.DATE_DEBUT_PERIODE
						group by numero_contrat, tmp1.dt_rec
					) as tmp2 on tmp2.numero_contrat = p.numero_contrat and tmp2.dt_rec = p.DATE_DEBUT_PERIODE and tmp2.dt_crea = p.DT_CREATION
				) as pr on pr.numero_contrat = co.noco
		where cl.nosiren not in (select siren from param_siren_faux)
		group by cl.nosiren
		having count( distinct co.nmgrpges) = 1 and count( distinct pr.a_traiter_par) > 1
		) as tmp_siren
inner join param_utilisateur_gestionnaire pug2 on pug2.GG_EGAL =  tmp_siren.gg and pug2.`TYPE` = 'MULTI_GU'



-- ####################################################################
-- ####################################################################
-- CAS 3.1 : multi-groupes / mono domaine et 1 gestionnaire existant
-- ####################################################################
-- ####################################################################
union

select distinct tmp_siren.NOSIREN, concat('1',pug.code_user), pug.NOM, pug.PRENOM, pug2.NIVEAU_1, pug2.NIVEAU_2, pug2.NIVEAU_3
from  (
		select cl.nosiren, group_concat(distinct co.nmgrpges order by co.nmgrpges separator '%') gg, group_concat( distinct pr.a_traiter_par) gest
		from 
			(	select distinct contrats.noco as noco, clients.nosiren as nosiren from clients inner join contrats on clients.nocli = contrats.nocli
			union 
				select distinct ext.noco as noco, ext.siren as nosiren from extensions_entreprises_affiliees ext
			) as cl
		inner join contrats co on cl.noco = co.noco and co.DT_FIN_SIT is null and co.tmp_batch IS FALSE AND (co.eligdsn = 'SRN' OR co.eligdsn = 'SRT')
		inner join param_etat_contrat p on p.COETACO = co.coetaco and co.tmp_batch = p.tmp_batch AND p.ACTIF = 'O'
		left join (
					select distinct a_traiter_par, p.numero_contrat from periodes_recues p
					inner join contrats ct on p.NUMERO_CONTRAT = ct.NOCO
					inner join (
						select p1.numero_contrat, tmp1.dt_rec, max(p1.dt_creation) dt_crea 
						from periodes_recues p1
						inner join (
							select numero_contrat, max(date_debut_periode) dt_rec
							from periodes_recues
							group by numero_contrat
						) as tmp1 on tmp1.numero_contrat = p1.numero_contrat and tmp1.dt_rec = p1.DATE_DEBUT_PERIODE
						group by numero_contrat, tmp1.dt_rec
					) as tmp2 on tmp2.numero_contrat = p.numero_contrat and tmp2.dt_rec = p.DATE_DEBUT_PERIODE and tmp2.dt_crea = p.DT_CREATION
				) as pr on pr.numero_contrat = co.noco
		where cl.nosiren not in (select siren from param_siren_faux)
		group by cl.nosiren
		having count( distinct co.nmgrpges) > 1 and count( distinct pr.a_traiter_par) = 1
		) as tmp_siren
inner join param_utilisateur_gestionnaire pug2 on pug2.GG_inclus_1 like concat('%',tmp_siren.gg,'%') and pug2.`TYPE` = 'GEST_MULTI'
inner join param_utilisateur_gestionnaire pug on pug.code_user = tmp_siren.gest 
where pug2.GG_INCLUS_2 = '' and pug2.GG_EXCLUS = ''
group by tmp_siren.NOSIREN



-- ####################################################################
-- ####################################################################
-- CAS 3.2 : multi-groupes / mono domaine et 1 gestionnaire non existant
-- ####################################################################
-- ####################################################################
union

select distinct tmp_siren.NOSIREN, pug2.code_user, pug2.NOM, pug2.PRENOM, pug2.NIVEAU_1, pug2.NIVEAU_2, pug2.NIVEAU_3
from  (
		select cl.nosiren, group_concat(distinct co.nmgrpges order by co.nmgrpges separator '%') gg, group_concat( distinct pr.a_traiter_par) gest
		from 
			(	select distinct contrats.noco as noco, clients.nosiren as nosiren from clients inner join contrats on clients.nocli = contrats.nocli
			union 
				select distinct ext.noco as noco, ext.siren as nosiren from extensions_entreprises_affiliees ext
			) as cl
		inner join contrats co on cl.noco = co.noco and co.DT_FIN_SIT is null and co.tmp_batch IS FALSE AND (co.eligdsn = 'SRN' OR co.eligdsn = 'SRT')
		inner join param_etat_contrat p on p.COETACO = co.coetaco and co.tmp_batch = p.tmp_batch AND p.ACTIF = 'O'
		left join (
					select distinct a_traiter_par, p.numero_contrat from periodes_recues p
					inner join contrats ct on p.NUMERO_CONTRAT = ct.NOCO
					inner join (
						select p1.numero_contrat, tmp1.dt_rec, max(p1.dt_creation) dt_crea 
						from periodes_recues p1
						inner join (
							select numero_contrat, max(date_debut_periode) dt_rec
							from periodes_recues
							group by numero_contrat
						) as tmp1 on tmp1.numero_contrat = p1.numero_contrat and tmp1.dt_rec = p1.DATE_DEBUT_PERIODE
						group by numero_contrat, tmp1.dt_rec
					) as tmp2 on tmp2.numero_contrat = p.numero_contrat and tmp2.dt_rec = p.DATE_DEBUT_PERIODE and tmp2.dt_crea = p.DT_CREATION
				) as pr on pr.numero_contrat = co.noco
		where cl.nosiren not in (select siren from param_siren_faux)
		group by cl.nosiren
		having count( distinct co.nmgrpges) > 1 and count( distinct pr.a_traiter_par) = 1
		) as tmp_siren
inner join param_utilisateur_gestionnaire pug2 on pug2.GG_inclus_1 like concat('%',tmp_siren.gg,'%') and pug2.`TYPE` = 'AUTRE_MULTI'
left join param_utilisateur_gestionnaire pug on pug.code_user = tmp_siren.gest 
where pug2.GG_INCLUS_2 = '' and pug2.GG_EXCLUS = '' 
and pug.code_user is null and tmp_siren.gest is not null
group by tmp_siren.NOSIREN



-- ####################################################################
-- ####################################################################
-- CAS 3.3 : multi-groupes / mono domaine et 0 gestionnaire assigné
-- ####################################################################
-- ####################################################################
union

select distinct tmp_siren.NOSIREN, pug2.code_user, pug2.NOM, pug2.PRENOM, pug2.NIVEAU_1, pug2.NIVEAU_2, pug2.NIVEAU_3
from  (
		select cl.nosiren, group_concat(distinct co.nmgrpges order by co.nmgrpges separator '%') gg, group_concat( distinct pr.a_traiter_par) gest
		from 
			(	select distinct contrats.noco as noco, clients.nosiren as nosiren from clients inner join contrats on clients.nocli = contrats.nocli
			union 
				select distinct ext.noco as noco, ext.siren as nosiren from extensions_entreprises_affiliees ext
			) as cl
		inner join contrats co on cl.noco = co.noco and co.DT_FIN_SIT is null and co.tmp_batch IS FALSE AND (co.eligdsn = 'SRN' OR co.eligdsn = 'SRT')
		inner join param_etat_contrat p on p.COETACO = co.coetaco and co.tmp_batch = p.tmp_batch AND p.ACTIF = 'O'
		left join (
					select distinct a_traiter_par, p.numero_contrat from periodes_recues p
					inner join contrats ct on p.NUMERO_CONTRAT = ct.NOCO
					inner join (
						select p1.numero_contrat, tmp1.dt_rec, max(p1.dt_creation) dt_crea 
						from periodes_recues p1
						inner join (
							select numero_contrat, max(date_debut_periode) dt_rec
							from periodes_recues
							group by numero_contrat
						) as tmp1 on tmp1.numero_contrat = p1.numero_contrat and tmp1.dt_rec = p1.DATE_DEBUT_PERIODE
						group by numero_contrat, tmp1.dt_rec
					) as tmp2 on tmp2.numero_contrat = p.numero_contrat and tmp2.dt_rec = p.DATE_DEBUT_PERIODE and tmp2.dt_crea = p.DT_CREATION
				) as pr on pr.numero_contrat = co.noco
		where cl.nosiren not in (select siren from param_siren_faux)
		group by cl.nosiren
		having count( distinct co.nmgrpges) > 1 and count( distinct pr.a_traiter_par) = 0
		) as tmp_siren
 inner join param_utilisateur_gestionnaire pug2 on pug2.GG_inclus_1 like concat('%',tmp_siren.gg,'%') and pug2.`TYPE` = 'NON_ASS_MULTI'
 where pug2.GG_INCLUS_2 = '' and pug2.GG_EXCLUS = ''
group by tmp_siren.NOSIREN



-- ####################################################################
-- ####################################################################
-- CAS 3.4 : multi-groupes / mono domaine et plusieurs gestionnaires 
-- ####################################################################
-- ####################################################################
union

select distinct tmp_siren.NOSIREN, pug2.code_user, pug2.NOM, pug2.PRENOM, pug2.NIVEAU_1, pug2.NIVEAU_2, pug2.NIVEAU_3
from  (
		select cl.nosiren, group_concat(distinct co.nmgrpges order by co.nmgrpges separator '%') gg, group_concat( distinct pr.a_traiter_par) gest
		from 
			(	select distinct contrats.noco as noco, clients.nosiren as nosiren from clients inner join contrats on clients.nocli = contrats.nocli
			union 
				select distinct ext.noco as noco, ext.siren as nosiren from extensions_entreprises_affiliees ext
			) as cl
		inner join contrats co on cl.noco = co.noco and co.DT_FIN_SIT is null and co.tmp_batch IS FALSE AND (co.eligdsn = 'SRN' OR co.eligdsn = 'SRT')
		inner join param_etat_contrat p on p.COETACO = co.coetaco and co.tmp_batch = p.tmp_batch AND p.ACTIF = 'O'
		left join (
					select distinct a_traiter_par, p.numero_contrat from periodes_recues p
					inner join contrats ct on p.NUMERO_CONTRAT = ct.NOCO
					inner join (
						select p1.numero_contrat, tmp1.dt_rec, max(p1.dt_creation) dt_crea 
						from periodes_recues p1
						inner join (
							select numero_contrat, max(date_debut_periode) dt_rec
							from periodes_recues
							group by numero_contrat
						) as tmp1 on tmp1.numero_contrat = p1.numero_contrat and tmp1.dt_rec = p1.DATE_DEBUT_PERIODE
						group by numero_contrat, tmp1.dt_rec
					) as tmp2 on tmp2.numero_contrat = p.numero_contrat and tmp2.dt_rec = p.DATE_DEBUT_PERIODE and tmp2.dt_crea = p.DT_CREATION
				) as pr on pr.numero_contrat = co.noco
		where cl.nosiren not in (select siren from param_siren_faux)
		group by cl.nosiren
		having count( distinct co.nmgrpges) > 1 and count( distinct pr.a_traiter_par) > 1
		) as tmp_siren
inner join param_utilisateur_gestionnaire pug2 on pug2.GG_inclus_1 like concat('%',tmp_siren.gg,'%') and pug2.`TYPE` = 'MULTI_MULTI'
where pug2.GG_INCLUS_2 = '' and pug2.GG_EXCLUS = ''



-- ####################################################################
-- ####################################################################
-- CAS 4.1 : multi-groupes / 2 domaines et 1 gestionnaire existant
-- ####################################################################
-- ####################################################################
union

select distinct tmp_siren.NOSIREN, concat('2',pug.code_user), pug.NOM, pug.PRENOM, pug2.NIVEAU_1, pug2.NIVEAU_2, pug2.NIVEAU_3
from  (
		select cl.nosiren, group_concat(distinct co.nmgrpges order by co.nmgrpges separator '%') gg, group_concat( distinct pr.a_traiter_par) gest
		from 
			(	select distinct contrats.noco as noco, clients.nosiren as nosiren from clients inner join contrats on clients.nocli = contrats.nocli
			union 
				select distinct ext.noco as noco, ext.siren as nosiren from extensions_entreprises_affiliees ext
			) as cl
		inner join contrats co on cl.noco = co.noco and co.DT_FIN_SIT is null and co.tmp_batch IS FALSE AND (co.eligdsn = 'SRN' OR co.eligdsn = 'SRT')
		inner join param_etat_contrat p on p.COETACO = co.coetaco and co.tmp_batch = p.tmp_batch AND p.ACTIF = 'O'
		left join (
					select distinct a_traiter_par, p.numero_contrat from periodes_recues p
					inner join contrats ct on p.NUMERO_CONTRAT = ct.NOCO
					inner join (
						select p1.numero_contrat, tmp1.dt_rec, max(p1.dt_creation) dt_crea 
						from periodes_recues p1
						inner join (
							select numero_contrat, max(date_debut_periode) dt_rec
							from periodes_recues
							group by numero_contrat
						) as tmp1 on tmp1.numero_contrat = p1.numero_contrat and tmp1.dt_rec = p1.DATE_DEBUT_PERIODE
						group by numero_contrat, tmp1.dt_rec
					) as tmp2 on tmp2.numero_contrat = p.numero_contrat and tmp2.dt_rec = p.DATE_DEBUT_PERIODE and tmp2.dt_crea = p.DT_CREATION
				) as pr on pr.numero_contrat = co.noco
		where cl.nosiren not in (select siren from param_siren_faux)
		group by cl.nosiren
		having count( distinct co.nmgrpges) > 1 and count( distinct pr.a_traiter_par) = 1
		) as tmp_siren
inner join param_utilisateur_gestionnaire pug2 
		on concat(pug2.GG_inclus_1, ',', pug2.GG_inclus_2) like concat('%',tmp_siren.gg,'%') 
		and pug2.GG_inclus_1 not like concat('%',tmp_siren.gg,'%')
		and pug2.GG_inclus_2 not like concat('%',tmp_siren.gg,'%')
		and pug2.`TYPE` = 'GEST_MULTI'
inner join param_utilisateur_gestionnaire pug on pug.code_user = tmp_siren.gest 
where pug2.GG_INCLUS_1 <> '' and pug2.GG_INCLUS_2 <> '' and pug2.GG_EXCLUS = ''
group by tmp_siren.NOSIREN



-- ####################################################################
-- ####################################################################
-- CAS 4.2 : multi-groupes / 2 domaines et 1 gestionnaire non existant
-- ####################################################################
-- ####################################################################
union

select distinct tmp_siren.NOSIREN, pug2.code_user, pug2.NOM, pug2.PRENOM, pug2.NIVEAU_1, pug2.NIVEAU_2, pug2.NIVEAU_3
from  (
		select cl.nosiren, group_concat(distinct co.nmgrpges order by co.nmgrpges separator '%') gg, group_concat( distinct pr.a_traiter_par) gest
		from 
			(	select distinct contrats.noco as noco, clients.nosiren as nosiren from clients inner join contrats on clients.nocli = contrats.nocli
			union 
				select distinct ext.noco as noco, ext.siren as nosiren from extensions_entreprises_affiliees ext
			) as cl
		inner join contrats co on cl.noco = co.noco and co.DT_FIN_SIT is null and co.tmp_batch IS FALSE AND (co.eligdsn = 'SRN' OR co.eligdsn = 'SRT')
		inner join param_etat_contrat p on p.COETACO = co.coetaco and co.tmp_batch = p.tmp_batch AND p.ACTIF = 'O'
		left join (
					select distinct a_traiter_par, p.numero_contrat from periodes_recues p
					inner join contrats ct on p.NUMERO_CONTRAT = ct.NOCO
					inner join (
						select p1.numero_contrat, tmp1.dt_rec, max(p1.dt_creation) dt_crea 
						from periodes_recues p1
						inner join (
							select numero_contrat, max(date_debut_periode) dt_rec
							from periodes_recues
							group by numero_contrat
						) as tmp1 on tmp1.numero_contrat = p1.numero_contrat and tmp1.dt_rec = p1.DATE_DEBUT_PERIODE
						group by numero_contrat, tmp1.dt_rec
					) as tmp2 on tmp2.numero_contrat = p.numero_contrat and tmp2.dt_rec = p.DATE_DEBUT_PERIODE and tmp2.dt_crea = p.DT_CREATION
				) as pr on pr.numero_contrat = co.noco
		where cl.nosiren not in (select siren from param_siren_faux)
		group by cl.nosiren
		having count( distinct co.nmgrpges) > 1 and count( distinct pr.a_traiter_par) = 1
		) as tmp_siren
inner join param_utilisateur_gestionnaire pug2 
		on concat(pug2.GG_inclus_1, ',', pug2.GG_inclus_2) like concat('%',tmp_siren.gg,'%') 
		and pug2.GG_inclus_1 not like concat('%',tmp_siren.gg,'%')
		and pug2.GG_inclus_2 not like concat('%',tmp_siren.gg,'%')
		and pug2.`TYPE` = 'AUTRE_MULTI'
left join param_utilisateur_gestionnaire pug on pug.code_user = tmp_siren.gest 
where pug2.GG_INCLUS_1 <> '' and pug2.GG_INCLUS_2 <> '' and pug2.GG_EXCLUS = ''
and pug.code_user is null and tmp_siren.gest is not null
group by tmp_siren.NOSIREN



-- ####################################################################
-- ####################################################################
-- CAS 4.3 : multi-groupes / 2 domaines et 0 gestionnaire assigné
-- ####################################################################
-- ####################################################################
union

select distinct tmp_siren.NOSIREN, pug2.code_user, pug2.NOM, pug2.PRENOM, pug2.NIVEAU_1, pug2.NIVEAU_2, pug2.NIVEAU_3
from  (
		select cl.nosiren, group_concat(distinct co.nmgrpges order by co.nmgrpges separator '%') gg, group_concat( distinct pr.a_traiter_par) gest
		from 
			(	select distinct contrats.noco as noco, clients.nosiren as nosiren from clients inner join contrats on clients.nocli = contrats.nocli
			union 
				select distinct ext.noco as noco, ext.siren as nosiren from extensions_entreprises_affiliees ext
			) as cl
		inner join contrats co on cl.noco = co.noco and co.DT_FIN_SIT is null and co.tmp_batch IS FALSE AND (co.eligdsn = 'SRN' OR co.eligdsn = 'SRT')
		inner join param_etat_contrat p on p.COETACO = co.coetaco and co.tmp_batch = p.tmp_batch AND p.ACTIF = 'O'
		left join (
					select distinct a_traiter_par, p.numero_contrat from periodes_recues p
					inner join contrats ct on p.NUMERO_CONTRAT = ct.NOCO
					inner join (
						select p1.numero_contrat, tmp1.dt_rec, max(p1.dt_creation) dt_crea 
						from periodes_recues p1
						inner join (
							select numero_contrat, max(date_debut_periode) dt_rec
							from periodes_recues
							group by numero_contrat
						) as tmp1 on tmp1.numero_contrat = p1.numero_contrat and tmp1.dt_rec = p1.DATE_DEBUT_PERIODE
						group by numero_contrat, tmp1.dt_rec
					) as tmp2 on tmp2.numero_contrat = p.numero_contrat and tmp2.dt_rec = p.DATE_DEBUT_PERIODE and tmp2.dt_crea = p.DT_CREATION
				) as pr on pr.numero_contrat = co.noco
		where cl.nosiren not in (select siren from param_siren_faux)
		group by cl.nosiren
		having count( distinct co.nmgrpges) > 1 and count( distinct pr.a_traiter_par) = 0
		) as tmp_siren
inner join param_utilisateur_gestionnaire pug2 
		on concat(pug2.GG_inclus_1, ',', pug2.GG_inclus_2) like concat('%',tmp_siren.gg,'%') 
		and pug2.GG_inclus_1 not like concat('%',tmp_siren.gg,'%')
		and pug2.GG_inclus_2 not like concat('%',tmp_siren.gg,'%')
		and pug2.`TYPE` = 'NON_ASS_MULTI'
where pug2.GG_INCLUS_1 <> '' and pug2.GG_INCLUS_2 <> '' and pug2.GG_EXCLUS = ''
group by tmp_siren.NOSIREN



-- ####################################################################
-- ####################################################################
-- CAS 4.4 : multi-groupes / 2 domaines et plusieurs gestionnaires 
-- ####################################################################
-- ####################################################################
union

select distinct tmp_siren.NOSIREN, pug2.code_user, pug2.NOM, pug2.PRENOM, pug2.NIVEAU_1, pug2.NIVEAU_2, pug2.NIVEAU_3
from  (
		select cl.nosiren, group_concat(distinct co.nmgrpges order by co.nmgrpges separator '%') gg, group_concat( distinct pr.a_traiter_par) gest
		from 
			(	select distinct contrats.noco as noco, clients.nosiren as nosiren from clients inner join contrats on clients.nocli = contrats.nocli
			union 
				select distinct ext.noco as noco, ext.siren as nosiren from extensions_entreprises_affiliees ext
			) as cl
		inner join contrats co on cl.noco = co.noco and co.DT_FIN_SIT is null and co.tmp_batch IS FALSE AND (co.eligdsn = 'SRN' OR co.eligdsn = 'SRT')
		inner join param_etat_contrat p on p.COETACO = co.coetaco and co.tmp_batch = p.tmp_batch AND p.ACTIF = 'O'
		left join (
					select distinct a_traiter_par, p.numero_contrat from periodes_recues p
					inner join contrats ct on p.NUMERO_CONTRAT = ct.NOCO
					inner join (
						select p1.numero_contrat, tmp1.dt_rec, max(p1.dt_creation) dt_crea 
						from periodes_recues p1
						inner join (
							select numero_contrat, max(date_debut_periode) dt_rec
							from periodes_recues
							group by numero_contrat
						) as tmp1 on tmp1.numero_contrat = p1.numero_contrat and tmp1.dt_rec = p1.DATE_DEBUT_PERIODE
						group by numero_contrat, tmp1.dt_rec
					) as tmp2 on tmp2.numero_contrat = p.numero_contrat and tmp2.dt_rec = p.DATE_DEBUT_PERIODE and tmp2.dt_crea = p.DT_CREATION
				) as pr on pr.numero_contrat = co.noco
		where cl.nosiren not in (select siren from param_siren_faux)
		group by cl.nosiren
		having count( distinct co.nmgrpges) > 1 and count( distinct pr.a_traiter_par) > 1
		) as tmp_siren
inner join param_utilisateur_gestionnaire pug2 
		on concat(pug2.GG_inclus_1, ',', pug2.GG_inclus_2) like concat('%',tmp_siren.gg,'%') 
		and pug2.GG_inclus_1 not like concat('%',tmp_siren.gg,'%')
		and pug2.GG_inclus_2 not like concat('%',tmp_siren.gg,'%')
		and pug2.`TYPE` = 'MULTI_MULTI'
where pug2.GG_INCLUS_1 <> '' and pug2.GG_INCLUS_2 <> '' and pug2.GG_EXCLUS = ''
group by tmp_siren.NOSIREN



-- ####################################################################
-- ####################################################################
-- CAS 5.1 : multi-groupes / 1 domaine + autre et 1 gestionnaire existant
-- ####################################################################
-- ####################################################################
union

select tmp_siren_liste_gg.NOSIREN, concat('3',pug.code_user), pug.NOM, pug.PRENOM, pug2.NIVEAU_1, pug2.NIVEAU_2, pug2.NIVEAU_3
from (		
		select distinct cl1.NOSIREN, co1.noco, co1.nmgrpges
		from  (
				select cl.nosiren, group_concat(distinct co.nmgrpges order by co.nmgrpges separator '%') gg, count( distinct pr.a_traiter_par)
				from 
					(	select distinct contrats.noco as noco, clients.nosiren as nosiren from clients inner join contrats on clients.nocli = contrats.nocli
					union 
						select distinct ext.noco as noco, ext.siren as nosiren from extensions_entreprises_affiliees ext
					) as cl
				inner join contrats co on cl.noco = co.noco and co.DT_FIN_SIT is null and co.tmp_batch IS FALSE AND (co.eligdsn = 'SRN' OR co.eligdsn = 'SRT')
				inner join param_etat_contrat p on p.COETACO = co.coetaco and co.tmp_batch = p.tmp_batch AND p.ACTIF = 'O'
				left join (
					select distinct a_traiter_par, p.numero_contrat from periodes_recues p
					inner join contrats ct on p.NUMERO_CONTRAT = ct.NOCO
					inner join (
						select p1.numero_contrat, tmp1.dt_rec, max(p1.dt_creation) dt_crea 
						from periodes_recues p1
						inner join (
							select numero_contrat, max(date_debut_periode) dt_rec
							from periodes_recues
							group by numero_contrat
						) as tmp1 on tmp1.numero_contrat = p1.numero_contrat and tmp1.dt_rec = p1.DATE_DEBUT_PERIODE
						group by numero_contrat, tmp1.dt_rec
					) as tmp2 on tmp2.numero_contrat = p.numero_contrat and tmp2.dt_rec = p.DATE_DEBUT_PERIODE and tmp2.dt_crea = p.DT_CREATION
				) as pr on pr.numero_contrat = co.noco
				where cl.nosiren not in (select siren from param_siren_faux)
				group by cl.nosiren
				having count( distinct co.nmgrpges) > 1 and count( distinct pr.a_traiter_par) = 1
				) as tmp_siren
		inner join 
			(	select distinct contrats.noco as noco, clients.nosiren as nosiren from clients inner join contrats on clients.nocli = contrats.nocli
			union 
				select distinct ext.noco as noco, ext.siren as nosiren from extensions_entreprises_affiliees ext
			) as cl1 on cl1.NOSIREN = tmp_siren.nosiren
		inner join contrats co1 on cl1.noco = co1.noco and co1.DT_FIN_SIT is null and co1.tmp_batch IS FALSE AND (co1.eligdsn = 'SRN' OR co1.eligdsn = 'SRT')
		inner join param_etat_contrat p1 on p1.COETACO = co1.coetaco and co1.tmp_batch = p1.tmp_batch AND p1.ACTIF = 'O'
	) as tmp_siren_liste_gg
inner join (select cl.nosiren, group_concat(distinct co.nmgrpges order by co.nmgrpges separator '%') gg, group_concat( distinct pr.a_traiter_par) gest
				from 
					(	select distinct contrats.noco as noco, clients.nosiren as nosiren from clients inner join contrats on clients.nocli = contrats.nocli
					union 
						select distinct ext.noco as noco, ext.siren as nosiren from extensions_entreprises_affiliees ext
					) as cl
				inner join contrats co on cl.noco = co.noco and co.DT_FIN_SIT is null and co.tmp_batch IS FALSE AND (co.eligdsn = 'SRN' OR co.eligdsn = 'SRT')
				inner join param_etat_contrat p on p.COETACO = co.coetaco and co.tmp_batch = p.tmp_batch AND p.ACTIF = 'O'
				left join (
					select distinct a_traiter_par, p.numero_contrat from periodes_recues p
					inner join contrats ct on p.NUMERO_CONTRAT = ct.NOCO
					inner join (
						select p1.numero_contrat, tmp1.dt_rec, max(p1.dt_creation) dt_crea 
						from periodes_recues p1
						inner join (
							select numero_contrat, max(date_debut_periode) dt_rec
							from periodes_recues
							group by numero_contrat
						) as tmp1 on tmp1.numero_contrat = p1.numero_contrat and tmp1.dt_rec = p1.DATE_DEBUT_PERIODE
						group by numero_contrat, tmp1.dt_rec
					) as tmp2 on tmp2.numero_contrat = p.numero_contrat and tmp2.dt_rec = p.DATE_DEBUT_PERIODE and tmp2.dt_crea = p.DT_CREATION
				) as pr on pr.numero_contrat = co.noco
				where cl.nosiren not in (select siren from param_siren_faux)
				group by cl.nosiren
				having count( distinct co.nmgrpges) > 1 and count( distinct pr.a_traiter_par) = 1
				) as tmp_siren2 on tmp_siren2.nosiren = tmp_siren_liste_gg.nosiren
inner join param_utilisateur_gestionnaire pug2 
		on pug2.GG_inclus_1 like concat('%',tmp_siren_liste_gg.nmgrpges,'%') 
		and pug2.GG_exclus not like concat('%',tmp_siren_liste_gg.nmgrpges,'%')
 	  	and concat(pug2.GG_inclus_1, ',', pug2.GG_exclus) not like concat('%',tmp_siren2.gg,'%') 
 	  	and concat(pug2.GG_exclus, ',', pug2.GG_inclus_1) not like concat('%',tmp_siren2.gg,'%') 
		and pug2.`TYPE` = 'GEST_MULTI'
inner join param_utilisateur_gestionnaire pug on pug.code_user = tmp_siren2.gest 
where pug2.GG_INCLUS_1 <> '' and pug2.GG_INCLUS_2 = '' and pug2.GG_EXCLUS <> ''
and tmp_siren2.nosiren not in (
	select distinct cl.NOSIREN
	from
		(	select distinct contrats.noco as noco, clients.nosiren as nosiren from clients inner join contrats on clients.nocli = contrats.nocli
		union 
			select distinct ext.noco as noco, ext.siren as nosiren from extensions_entreprises_affiliees ext
		) as cl
	inner join (
		select distinct nosiren, nmgrpges 
		from 
			(	select distinct contrats.noco as noco, clients.nosiren as nosiren from clients inner join contrats on clients.nocli = contrats.nocli
			union 
				select distinct ext.noco as noco, ext.siren as nosiren from extensions_entreprises_affiliees ext
			) as cl1
		inner join contrats co1 on cl1.noco = co1.noco and co1.DT_FIN_SIT is null and co1.tmp_batch IS FALSE AND (co1.eligdsn = 'SRN' OR co1.eligdsn = 'SRT')
		inner join param_etat_contrat p1 on p1.COETACO = co1.coetaco and co1.tmp_batch = p1.tmp_batch AND p1.ACTIF = 'O'
		) tmp1 on cl.NOSIREN = tmp1.nosiren
	inner join (
		select distinct nosiren, nmgrpges 
		from 
			(	select distinct contrats.noco as noco, clients.nosiren as nosiren from clients inner join contrats on clients.nocli = contrats.nocli
			union 
				select distinct ext.noco as noco, ext.siren as nosiren from extensions_entreprises_affiliees ext
			) as cl1
		inner join contrats co1 on cl1.noco = co1.noco and co1.DT_FIN_SIT is null and co1.tmp_batch IS FALSE AND (co1.eligdsn = 'SRN' OR co1.eligdsn = 'SRT')
		inner join param_etat_contrat p1 on p1.COETACO = co1.coetaco and co1.tmp_batch = p1.tmp_batch AND p1.ACTIF = 'O'
		) tmp2 on cl.NOSIREN = tmp2.nosiren
	inner join (
		select distinct nosiren, nmgrpges 
		from 
			(	select distinct contrats.noco as noco, clients.nosiren as nosiren from clients inner join contrats on clients.nocli = contrats.nocli
			union 
				select distinct ext.noco as noco, ext.siren as nosiren from extensions_entreprises_affiliees ext
			) as cl1
		inner join contrats co1 on cl1.noco = co1.noco and co1.DT_FIN_SIT is null and co1.tmp_batch IS FALSE AND (co1.eligdsn = 'SRN' OR co1.eligdsn = 'SRT')
		inner join param_etat_contrat p1 on p1.COETACO = co1.coetaco and co1.tmp_batch = p1.tmp_batch AND p1.ACTIF = 'O'
		) tmp3 on cl.NOSIREN = tmp3.nosiren
	inner join param_utilisateur_gestionnaire pug on pug.`TYPE` = 'GEST_MULTI'
		and pug.GG_INCLUS_1 <> '' and pug.GG_INCLUS_2 = '' and pug.GG_EXCLUS <> ''
	where exists (
		select 1 from param_utilisateur_gestionnaire pug2 
		where 1 = 1 
		and pug2.GG_inclus_1 not like concat('%',tmp1.nmgrpges,'%') 
		and pug2.GG_exclus not like concat('%',tmp1.nmgrpges,'%')
		and pug2.`TYPE` = 'GEST_MULTI'
		and pug2.GG_INCLUS_1 <> '' and pug2.GG_INCLUS_2 = '' and pug2.GG_EXCLUS <> ''
		and pug2.code_user = pug.code_user
		)
	and exists (
		select 1 from param_utilisateur_gestionnaire pug2 
		where 1 = 1 
		and pug2.GG_inclus_1 like concat('%',tmp2.nmgrpges,'%')
		and pug2.`TYPE` = 'GEST_MULTI'
		and pug2.GG_INCLUS_1 <> '' and pug2.GG_INCLUS_2 = '' and pug2.GG_EXCLUS <> ''
		and pug2.code_user = pug.code_user
		)
	and exists (
		select 1 from param_utilisateur_gestionnaire pug2 
		where 1 = 1 
		and pug2.GG_exclus like concat('%',tmp3.nmgrpges,'%')
		and pug2.`TYPE` = 'GEST_MULTI'
		and pug2.GG_INCLUS_1 <> '' and pug2.GG_INCLUS_2 = '' and pug2.GG_EXCLUS <> ''
		and pug2.code_user = pug.code_user
		)
	)
group by tmp_siren_liste_gg.NOSIREN



-- ####################################################################
-- ####################################################################
-- CAS 5.2 : multi-groupes / 1 domaine + autre et 1 gestionnaire non existant
-- ####################################################################
-- ####################################################################
union

select tmp_siren_liste_gg.NOSIREN, pug2.code_user, pug2.NOM, pug2.PRENOM, pug2.NIVEAU_1, pug2.NIVEAU_2, pug2.NIVEAU_3
from (		
		select distinct cl1.NOSIREN, co1.noco, co1.nmgrpges
		from  (
				select cl.nosiren, group_concat(distinct co.nmgrpges order by co.nmgrpges separator '%') gg, count( distinct pr.a_traiter_par)
				from 
					(	select distinct contrats.noco as noco, clients.nosiren as nosiren from clients inner join contrats on clients.nocli = contrats.nocli
					union 
						select distinct ext.noco as noco, ext.siren as nosiren from extensions_entreprises_affiliees ext
					) as cl
				inner join contrats co on cl.noco = co.noco and co.DT_FIN_SIT is null and co.tmp_batch IS FALSE AND (co.eligdsn = 'SRN' OR co.eligdsn = 'SRT')
				inner join param_etat_contrat p on p.COETACO = co.coetaco and co.tmp_batch = p.tmp_batch AND p.ACTIF = 'O'
				left join (
					select distinct a_traiter_par, p.numero_contrat from periodes_recues p
					inner join contrats ct on p.NUMERO_CONTRAT = ct.NOCO
					inner join (
						select p1.numero_contrat, tmp1.dt_rec, max(p1.dt_creation) dt_crea 
						from periodes_recues p1
						inner join (
							select numero_contrat, max(date_debut_periode) dt_rec
							from periodes_recues
							group by numero_contrat
						) as tmp1 on tmp1.numero_contrat = p1.numero_contrat and tmp1.dt_rec = p1.DATE_DEBUT_PERIODE
						group by numero_contrat, tmp1.dt_rec
					) as tmp2 on tmp2.numero_contrat = p.numero_contrat and tmp2.dt_rec = p.DATE_DEBUT_PERIODE and tmp2.dt_crea = p.DT_CREATION
				) as pr on pr.numero_contrat = co.noco
				where cl.nosiren not in (select siren from param_siren_faux)
				group by cl.nosiren
				having count( distinct co.nmgrpges) > 1 and count( distinct pr.a_traiter_par) = 1
				) as tmp_siren
		inner join 
			(	select distinct contrats.noco as noco, clients.nosiren as nosiren from clients inner join contrats on clients.nocli = contrats.nocli
			union 
				select distinct ext.noco as noco, ext.siren as nosiren from extensions_entreprises_affiliees ext
			) as cl1 on cl1.NOSIREN = tmp_siren.nosiren
		inner join contrats co1 on cl1.noco = co1.noco and co1.DT_FIN_SIT is null and co1.tmp_batch IS FALSE AND (co1.eligdsn = 'SRN' OR co1.eligdsn = 'SRT')
		inner join param_etat_contrat p1 on p1.COETACO = co1.coetaco and co1.tmp_batch = p1.tmp_batch AND p1.ACTIF = 'O'
	) as tmp_siren_liste_gg
inner join (select cl.nosiren, group_concat(distinct co.nmgrpges order by co.nmgrpges separator '%') gg, group_concat( distinct pr.a_traiter_par) gest
				from 
					(	select distinct contrats.noco as noco, clients.nosiren as nosiren from clients inner join contrats on clients.nocli = contrats.nocli
					union 
						select distinct ext.noco as noco, ext.siren as nosiren from extensions_entreprises_affiliees ext
					) as cl
				inner join contrats co on cl.noco = co.noco and co.DT_FIN_SIT is null and co.tmp_batch IS FALSE AND (co.eligdsn = 'SRN' OR co.eligdsn = 'SRT')
				inner join param_etat_contrat p on p.COETACO = co.coetaco and co.tmp_batch = p.tmp_batch AND p.ACTIF = 'O'
				left join (
					select distinct a_traiter_par, p.numero_contrat from periodes_recues p
					inner join contrats ct on p.NUMERO_CONTRAT = ct.NOCO
					inner join (
						select p1.numero_contrat, tmp1.dt_rec, max(p1.dt_creation) dt_crea 
						from periodes_recues p1
						inner join (
							select numero_contrat, max(date_debut_periode) dt_rec
							from periodes_recues
							group by numero_contrat
						) as tmp1 on tmp1.numero_contrat = p1.numero_contrat and tmp1.dt_rec = p1.DATE_DEBUT_PERIODE
						group by numero_contrat, tmp1.dt_rec
					) as tmp2 on tmp2.numero_contrat = p.numero_contrat and tmp2.dt_rec = p.DATE_DEBUT_PERIODE and tmp2.dt_crea = p.DT_CREATION
				) as pr on pr.numero_contrat = co.noco
				where cl.nosiren not in (select siren from param_siren_faux)
				group by cl.nosiren
				having count( distinct co.nmgrpges) > 1 and count( distinct pr.a_traiter_par) = 1
				) as tmp_siren2 on tmp_siren2.nosiren = tmp_siren_liste_gg.nosiren
inner join param_utilisateur_gestionnaire pug2 
		on pug2.GG_inclus_1 like concat('%',tmp_siren_liste_gg.nmgrpges,'%') 
		and pug2.GG_exclus not like concat('%',tmp_siren_liste_gg.nmgrpges,'%')
 	  	and concat(pug2.GG_inclus_1, ',', pug2.GG_exclus) not like concat('%',tmp_siren2.gg,'%') 
 	  	and concat(pug2.GG_exclus, ',', pug2.GG_inclus_1) not like concat('%',tmp_siren2.gg,'%') 
		and pug2.`TYPE` = 'AUTRE_MULTI'
left join param_utilisateur_gestionnaire pug on pug.code_user = tmp_siren2.gest
where pug2.GG_INCLUS_1 <> '' and pug2.GG_INCLUS_2 = '' and pug2.GG_EXCLUS <> ''
and pug.code_user is null and tmp_siren2.gest is not null
and tmp_siren2.nosiren not in (
	select distinct cl.NOSIREN
	from
		(	select distinct contrats.noco as noco, clients.nosiren as nosiren from clients inner join contrats on clients.nocli = contrats.nocli
		union 
			select distinct ext.noco as noco, ext.siren as nosiren from extensions_entreprises_affiliees ext
		) as cl
	inner join (
		select distinct nosiren, nmgrpges 
		from 
			(	select distinct contrats.noco as noco, clients.nosiren as nosiren from clients inner join contrats on clients.nocli = contrats.nocli
			union 
				select distinct ext.noco as noco, ext.siren as nosiren from extensions_entreprises_affiliees ext
			) as cl1
		inner join contrats co1 on cl1.noco = co1.noco and co1.DT_FIN_SIT is null and co1.tmp_batch IS FALSE AND (co1.eligdsn = 'SRN' OR co1.eligdsn = 'SRT')
		inner join param_etat_contrat p1 on p1.COETACO = co1.coetaco and co1.tmp_batch = p1.tmp_batch AND p1.ACTIF = 'O'
		) tmp1 on cl.NOSIREN = tmp1.nosiren
	inner join (
		select distinct nosiren, nmgrpges 
		from 
			(	select distinct contrats.noco as noco, clients.nosiren as nosiren from clients inner join contrats on clients.nocli = contrats.nocli
			union 
				select distinct ext.noco as noco, ext.siren as nosiren from extensions_entreprises_affiliees ext
			) as cl1
		inner join contrats co1 on cl1.noco = co1.noco and co1.DT_FIN_SIT is null and co1.tmp_batch IS FALSE AND (co1.eligdsn = 'SRN' OR co1.eligdsn = 'SRT')
		inner join param_etat_contrat p1 on p1.COETACO = co1.coetaco and co1.tmp_batch = p1.tmp_batch AND p1.ACTIF = 'O'
		) tmp2 on cl.NOSIREN = tmp2.nosiren
	inner join (
		select distinct nosiren, nmgrpges 
		from 
			(	select distinct contrats.noco as noco, clients.nosiren as nosiren from clients inner join contrats on clients.nocli = contrats.nocli
			union 
				select distinct ext.noco as noco, ext.siren as nosiren from extensions_entreprises_affiliees ext
			) as cl1
		inner join contrats co1 on cl1.noco = co1.noco and co1.DT_FIN_SIT is null and co1.tmp_batch IS FALSE AND (co1.eligdsn = 'SRN' OR co1.eligdsn = 'SRT')
		inner join param_etat_contrat p1 on p1.COETACO = co1.coetaco and co1.tmp_batch = p1.tmp_batch AND p1.ACTIF = 'O'
		) tmp3 on cl.NOSIREN = tmp3.nosiren
	inner join param_utilisateur_gestionnaire pug on pug.`TYPE` = 'AUTRE_MULTI'
		and pug.GG_INCLUS_1 <> '' and pug.GG_INCLUS_2 = '' and pug.GG_EXCLUS <> ''
	where exists (
		select 1 from param_utilisateur_gestionnaire pug2 
		where 1 = 1 
		and pug2.GG_inclus_1 not like concat('%',tmp1.nmgrpges,'%') 
		and pug2.GG_exclus not like concat('%',tmp1.nmgrpges,'%')
		and pug2.`TYPE` = 'AUTRE_MULTI'
		and pug2.GG_INCLUS_1 <> '' and pug2.GG_INCLUS_2 = '' and pug2.GG_EXCLUS <> ''
		and pug2.code_user = pug.code_user
		)
	and exists (
		select 1 from param_utilisateur_gestionnaire pug2 
		where 1 = 1 
		and pug2.GG_inclus_1 like concat('%',tmp2.nmgrpges,'%')
		and pug2.`TYPE` = 'AUTRE_MULTI'
		and pug2.GG_INCLUS_1 <> '' and pug2.GG_INCLUS_2 = '' and pug2.GG_EXCLUS <> ''
		and pug2.code_user = pug.code_user
		)
	and exists (
		select 1 from param_utilisateur_gestionnaire pug2 
		where 1 = 1 
		and pug2.GG_exclus like concat('%',tmp3.nmgrpges,'%')
		and pug2.`TYPE` = 'AUTRE_MULTI'
		and pug2.GG_INCLUS_1 <> '' and pug2.GG_INCLUS_2 = '' and pug2.GG_EXCLUS <> ''
		and pug2.code_user = pug.code_user
		)
	)
group by tmp_siren_liste_gg.NOSIREN





-- ####################################################################
-- ####################################################################
-- CAS 5.3 : multi-groupes / 1 domaine + autre et 0 gestionnaire 
-- ####################################################################
-- ####################################################################
union

select tmp_siren_liste_gg.NOSIREN, pug2.code_user, pug2.NOM, pug2.PRENOM, pug2.NIVEAU_1, pug2.NIVEAU_2, pug2.NIVEAU_3
from (		
		select distinct cl1.NOSIREN, co1.noco, co1.nmgrpges
		from  (
				select cl.nosiren, group_concat(distinct co.nmgrpges order by co.nmgrpges separator '%') gg, count( distinct pr.a_traiter_par)
				from 
					(	select distinct contrats.noco as noco, clients.nosiren as nosiren from clients inner join contrats on clients.nocli = contrats.nocli
					union 
						select distinct ext.noco as noco, ext.siren as nosiren from extensions_entreprises_affiliees ext
					) as cl
				inner join contrats co on cl.noco = co.noco and co.DT_FIN_SIT is null and co.tmp_batch IS FALSE AND (co.eligdsn = 'SRN' OR co.eligdsn = 'SRT')
				inner join param_etat_contrat p on p.COETACO = co.coetaco and co.tmp_batch = p.tmp_batch AND p.ACTIF = 'O'
				left join (
					select distinct a_traiter_par, p.numero_contrat from periodes_recues p
					inner join contrats ct on p.NUMERO_CONTRAT = ct.NOCO
					inner join (
						select p1.numero_contrat, tmp1.dt_rec, max(p1.dt_creation) dt_crea 
						from periodes_recues p1
						inner join (
							select numero_contrat, max(date_debut_periode) dt_rec
							from periodes_recues
							group by numero_contrat
						) as tmp1 on tmp1.numero_contrat = p1.numero_contrat and tmp1.dt_rec = p1.DATE_DEBUT_PERIODE
						group by numero_contrat, tmp1.dt_rec
					) as tmp2 on tmp2.numero_contrat = p.numero_contrat and tmp2.dt_rec = p.DATE_DEBUT_PERIODE and tmp2.dt_crea = p.DT_CREATION
				) as pr on pr.numero_contrat = co.noco
				where cl.nosiren not in (select siren from param_siren_faux)
				group by cl.nosiren
				having count( distinct co.nmgrpges) > 1 and count( distinct pr.a_traiter_par) = 0
				) as tmp_siren
		inner join 
			(	select distinct contrats.noco as noco, clients.nosiren as nosiren from clients inner join contrats on clients.nocli = contrats.nocli
			union 
				select distinct ext.noco as noco, ext.siren as nosiren from extensions_entreprises_affiliees ext
			) as cl1 on cl1.NOSIREN = tmp_siren.nosiren
		inner join contrats co1 on cl1.noco = co1.noco and co1.DT_FIN_SIT is null and co1.tmp_batch IS FALSE AND (co1.eligdsn = 'SRN' OR co1.eligdsn = 'SRT')
		inner join param_etat_contrat p1 on p1.COETACO = co1.coetaco and co1.tmp_batch = p1.tmp_batch AND p1.ACTIF = 'O'
	) as tmp_siren_liste_gg
inner join (select cl.nosiren, group_concat(distinct co.nmgrpges order by co.nmgrpges separator '%') gg, count( distinct pr.a_traiter_par)
				from 
					(	select distinct contrats.noco as noco, clients.nosiren as nosiren from clients inner join contrats on clients.nocli = contrats.nocli
					union 
						select distinct ext.noco as noco, ext.siren as nosiren from extensions_entreprises_affiliees ext
					) as cl
				inner join contrats co on cl.noco = co.noco and co.DT_FIN_SIT is null and co.tmp_batch IS FALSE AND (co.eligdsn = 'SRN' OR co.eligdsn = 'SRT')
				inner join param_etat_contrat p on p.COETACO = co.coetaco and co.tmp_batch = p.tmp_batch AND p.ACTIF = 'O'
				left join (
					select distinct a_traiter_par, p.numero_contrat from periodes_recues p
					inner join contrats ct on p.NUMERO_CONTRAT = ct.NOCO
					inner join (
						select p1.numero_contrat, tmp1.dt_rec, max(p1.dt_creation) dt_crea 
						from periodes_recues p1
						inner join (
							select numero_contrat, max(date_debut_periode) dt_rec
							from periodes_recues
							group by numero_contrat
						) as tmp1 on tmp1.numero_contrat = p1.numero_contrat and tmp1.dt_rec = p1.DATE_DEBUT_PERIODE
						group by numero_contrat, tmp1.dt_rec
					) as tmp2 on tmp2.numero_contrat = p.numero_contrat and tmp2.dt_rec = p.DATE_DEBUT_PERIODE and tmp2.dt_crea = p.DT_CREATION
				) as pr on pr.numero_contrat = co.noco
				where cl.nosiren not in (select siren from param_siren_faux)
				group by cl.nosiren
				having count( distinct co.nmgrpges) > 1 and count( distinct pr.a_traiter_par) = 0
				) as tmp_siren2 on tmp_siren2.nosiren = tmp_siren_liste_gg.nosiren
inner join param_utilisateur_gestionnaire pug2 
		on pug2.GG_inclus_1 like concat('%',tmp_siren_liste_gg.nmgrpges,'%') 
		and pug2.GG_exclus not like concat('%',tmp_siren_liste_gg.nmgrpges,'%')
 	  	and concat(pug2.GG_inclus_1, ',', pug2.GG_exclus) not like concat('%',tmp_siren2.gg,'%') 
 	  	and concat(pug2.GG_exclus, ',', pug2.GG_inclus_1) not like concat('%',tmp_siren2.gg,'%') 
		and pug2.`TYPE` = 'NON_ASS_MULTI'
where pug2.GG_INCLUS_1 <> '' and pug2.GG_INCLUS_2 = '' and pug2.GG_EXCLUS <> ''
and tmp_siren2.nosiren not in (
	select distinct cl.NOSIREN
	from
		(	select distinct contrats.noco as noco, clients.nosiren as nosiren from clients inner join contrats on clients.nocli = contrats.nocli
		union 
			select distinct ext.noco as noco, ext.siren as nosiren from extensions_entreprises_affiliees ext
		) as cl
	inner join (
		select distinct nosiren, nmgrpges 
		from 
			(	select distinct contrats.noco as noco, clients.nosiren as nosiren from clients inner join contrats on clients.nocli = contrats.nocli
			union 
				select distinct ext.noco as noco, ext.siren as nosiren from extensions_entreprises_affiliees ext
			) as cl1
		inner join contrats co1 on cl1.noco = co1.noco and co1.DT_FIN_SIT is null and co1.tmp_batch IS FALSE AND (co1.eligdsn = 'SRN' OR co1.eligdsn = 'SRT')
		inner join param_etat_contrat p1 on p1.COETACO = co1.coetaco and co1.tmp_batch = p1.tmp_batch AND p1.ACTIF = 'O'
		) tmp1 on cl.NOSIREN = tmp1.nosiren
	inner join (
		select distinct nosiren, nmgrpges 
		from 
			(	select distinct contrats.noco as noco, clients.nosiren as nosiren from clients inner join contrats on clients.nocli = contrats.nocli
			union 
				select distinct ext.noco as noco, ext.siren as nosiren from extensions_entreprises_affiliees ext
			) as cl1
		inner join contrats co1 on cl1.noco = co1.noco and co1.DT_FIN_SIT is null and co1.tmp_batch IS FALSE AND (co1.eligdsn = 'SRN' OR co1.eligdsn = 'SRT')
		inner join param_etat_contrat p1 on p1.COETACO = co1.coetaco and co1.tmp_batch = p1.tmp_batch AND p1.ACTIF = 'O'
		) tmp2 on cl.NOSIREN = tmp2.nosiren
	inner join (
		select distinct nosiren, nmgrpges 
		from 
			(	select distinct contrats.noco as noco, clients.nosiren as nosiren from clients inner join contrats on clients.nocli = contrats.nocli
			union 
				select distinct ext.noco as noco, ext.siren as nosiren from extensions_entreprises_affiliees ext
			) as cl1
		inner join contrats co1 on cl1.noco = co1.noco and co1.DT_FIN_SIT is null and co1.tmp_batch IS FALSE AND (co1.eligdsn = 'SRN' OR co1.eligdsn = 'SRT')
		inner join param_etat_contrat p1 on p1.COETACO = co1.coetaco and co1.tmp_batch = p1.tmp_batch AND p1.ACTIF = 'O'
		) tmp3 on cl.NOSIREN = tmp3.nosiren
	inner join param_utilisateur_gestionnaire pug on pug.`TYPE` = 'NON_ASS_MULTI'
		and pug.GG_INCLUS_1 <> '' and pug.GG_INCLUS_2 = '' and pug.GG_EXCLUS <> ''
	where exists (
		select 1 from param_utilisateur_gestionnaire pug2 
		where 1 = 1 
		and pug2.GG_inclus_1 not like concat('%',tmp1.nmgrpges,'%') 
		and pug2.GG_exclus not like concat('%',tmp1.nmgrpges,'%')
		and pug2.`TYPE` = 'NON_ASS_MULTI'
		and pug2.GG_INCLUS_1 <> '' and pug2.GG_INCLUS_2 = '' and pug2.GG_EXCLUS <> ''
		and pug2.code_user = pug.code_user
		)
	and exists (
		select 1 from param_utilisateur_gestionnaire pug2 
		where 1 = 1 
		and pug2.GG_inclus_1 like concat('%',tmp2.nmgrpges,'%')
		and pug2.`TYPE` = 'NON_ASS_MULTI'
		and pug2.GG_INCLUS_1 <> '' and pug2.GG_INCLUS_2 = '' and pug2.GG_EXCLUS <> ''
		and pug2.code_user = pug.code_user
		)
	and exists (
		select 1 from param_utilisateur_gestionnaire pug2 
		where 1 = 1 
		and pug2.GG_exclus like concat('%',tmp3.nmgrpges,'%')
		and pug2.`TYPE` = 'NON_ASS_MULTI'
		and pug2.GG_INCLUS_1 <> '' and pug2.GG_INCLUS_2 = '' and pug2.GG_EXCLUS <> ''
		and pug2.code_user = pug.code_user
		)
	)
group by tmp_siren_liste_gg.NOSIREN



-- ####################################################################
-- ####################################################################
-- CAS 5.4 : multi-groupes / 1 domaine + autre et plusieurs gestionnaires 
-- ####################################################################
-- ####################################################################
union

select tmp_siren_liste_gg.NOSIREN, pug2.code_user, pug2.NOM, pug2.PRENOM, pug2.NIVEAU_1, pug2.NIVEAU_2, pug2.NIVEAU_3
from (		
		select distinct cl1.NOSIREN, co1.noco, co1.nmgrpges
		from  (
				select cl.nosiren, group_concat(distinct co.nmgrpges order by co.nmgrpges separator '%') gg, count( distinct pr.a_traiter_par)
				from 
					(	select distinct contrats.noco as noco, clients.nosiren as nosiren from clients inner join contrats on clients.nocli = contrats.nocli
					union 
						select distinct ext.noco as noco, ext.siren as nosiren from extensions_entreprises_affiliees ext
					) as cl
				inner join contrats co on cl.noco = co.noco and co.DT_FIN_SIT is null and co.tmp_batch IS FALSE AND (co.eligdsn = 'SRN' OR co.eligdsn = 'SRT')
				inner join param_etat_contrat p on p.COETACO = co.coetaco and co.tmp_batch = p.tmp_batch AND p.ACTIF = 'O'
				left join (
							select distinct a_traiter_par, p.numero_contrat from periodes_recues p
							inner join contrats ct on p.NUMERO_CONTRAT = ct.NOCO
							inner join (select numero_contrat, max(date_debut_periode) dt_rec
								from periodes_recues where type_periode = 'DEC'
								group by numero_contrat
							) as tmp2 on tmp2.numero_contrat = p.numero_contrat and tmp2.dt_rec = p.DATE_DEBUT_PERIODE
						) as pr on pr.numero_contrat = co.noco
				where cl.nosiren not in (select siren from param_siren_faux)
				group by cl.nosiren
				having count( distinct co.nmgrpges) > 1 and count( distinct pr.a_traiter_par) > 1
				) as tmp_siren
		inner join  
			(	select distinct contrats.noco as noco, clients.nosiren as nosiren from clients inner join contrats on clients.nocli = contrats.nocli
			union 
				select distinct ext.noco as noco, ext.siren as nosiren from extensions_entreprises_affiliees ext
			) as cl1 on cl1.NOSIREN = tmp_siren.nosiren
		inner join contrats co1 on cl1.noco = co1.noco and co1.DT_FIN_SIT is null and co1.tmp_batch IS FALSE AND (co1.eligdsn = 'SRN' OR co1.eligdsn = 'SRT')
		inner join param_etat_contrat p1 on p1.COETACO = co1.coetaco and co1.tmp_batch = p1.tmp_batch AND p1.ACTIF = 'O'
	) as tmp_siren_liste_gg
inner join (select cl.nosiren, group_concat(distinct co.nmgrpges order by co.nmgrpges separator '%') gg, count( distinct pr.a_traiter_par)
				from 
					(	select distinct contrats.noco as noco, clients.nosiren as nosiren from clients inner join contrats on clients.nocli = contrats.nocli
					union 
						select distinct ext.noco as noco, ext.siren as nosiren from extensions_entreprises_affiliees ext
					) as cl
				inner join contrats co on cl.noco = co.noco and co.DT_FIN_SIT is null and co.tmp_batch IS FALSE AND (co.eligdsn = 'SRN' OR co.eligdsn = 'SRT')
				inner join param_etat_contrat p on p.COETACO = co.coetaco and co.tmp_batch = p.tmp_batch AND p.ACTIF = 'O'
				left join (
					select distinct a_traiter_par, p.numero_contrat from periodes_recues p
					inner join contrats ct on p.NUMERO_CONTRAT = ct.NOCO
					inner join (
						select p1.numero_contrat, tmp1.dt_rec, max(p1.dt_creation) dt_crea 
						from periodes_recues p1
						inner join (
							select numero_contrat, max(date_debut_periode) dt_rec
							from periodes_recues
							group by numero_contrat
						) as tmp1 on tmp1.numero_contrat = p1.numero_contrat and tmp1.dt_rec = p1.DATE_DEBUT_PERIODE
						group by numero_contrat, tmp1.dt_rec
					) as tmp2 on tmp2.numero_contrat = p.numero_contrat and tmp2.dt_rec = p.DATE_DEBUT_PERIODE and tmp2.dt_crea = p.DT_CREATION
				) as pr on pr.numero_contrat = co.noco
				where cl.nosiren not in (select siren from param_siren_faux)
				group by cl.nosiren
				having count( distinct co.nmgrpges) > 1 and count( distinct pr.a_traiter_par) > 1
				) as tmp_siren2 on tmp_siren2.nosiren = tmp_siren_liste_gg.nosiren
inner join param_utilisateur_gestionnaire pug2 
		on pug2.GG_inclus_1 like concat('%',tmp_siren_liste_gg.nmgrpges,'%') 
		and pug2.GG_exclus not like concat('%',tmp_siren_liste_gg.nmgrpges,'%')
		and concat(pug2.GG_inclus_1, ',', pug2.GG_exclus) not like concat('%',tmp_siren2.gg,'%') 
		and concat(pug2.GG_exclus, ',', pug2.GG_inclus_1) not like concat('%',tmp_siren2.gg,'%') 
		and pug2.`TYPE` = 'MULTI_MULTI'
where pug2.GG_INCLUS_1 <> '' and pug2.GG_INCLUS_2 = '' and pug2.GG_EXCLUS <> ''
and tmp_siren2.nosiren not in (
	select distinct cl.NOSIREN
	from
		(	select distinct contrats.noco as noco, clients.nosiren as nosiren from clients inner join contrats on clients.nocli = contrats.nocli
		union 
			select distinct ext.noco as noco, ext.siren as nosiren from extensions_entreprises_affiliees ext
		) as cl
	inner join (
		select distinct nosiren, nmgrpges 
		from 
			(	select distinct contrats.noco as noco, clients.nosiren as nosiren from clients inner join contrats on clients.nocli = contrats.nocli
			union 
				select distinct ext.noco as noco, ext.siren as nosiren from extensions_entreprises_affiliees ext
			) as cl1
		inner join contrats co1 on cl1.noco = co1.noco and co1.DT_FIN_SIT is null and co1.tmp_batch IS FALSE AND (co1.eligdsn = 'SRN' OR co1.eligdsn = 'SRT')
		inner join param_etat_contrat p1 on p1.COETACO = co1.coetaco and co1.tmp_batch = p1.tmp_batch AND p1.ACTIF = 'O'
		) tmp1 on cl.NOSIREN = tmp1.nosiren
	inner join (
		select distinct nosiren, nmgrpges 
		from 
			(	select distinct contrats.noco as noco, clients.nosiren as nosiren from clients inner join contrats on clients.nocli = contrats.nocli
			union 
				select distinct ext.noco as noco, ext.siren as nosiren from extensions_entreprises_affiliees ext
			) as cl1
		inner join contrats co1 on cl1.noco = co1.noco and co1.DT_FIN_SIT is null and co1.tmp_batch IS FALSE AND (co1.eligdsn = 'SRN' OR co1.eligdsn = 'SRT')
		inner join param_etat_contrat p1 on p1.COETACO = co1.coetaco and co1.tmp_batch = p1.tmp_batch AND p1.ACTIF = 'O'
		) tmp2 on cl.NOSIREN = tmp2.nosiren
	inner join (
		select distinct nosiren, nmgrpges 
		from 
			(	select distinct contrats.noco as noco, clients.nosiren as nosiren from clients inner join contrats on clients.nocli = contrats.nocli
			union 
				select distinct ext.noco as noco, ext.siren as nosiren from extensions_entreprises_affiliees ext
			) as cl1
		inner join contrats co1 on cl1.noco = co1.noco and co1.DT_FIN_SIT is null and co1.tmp_batch IS FALSE AND (co1.eligdsn = 'SRN' OR co1.eligdsn = 'SRT')
		inner join param_etat_contrat p1 on p1.COETACO = co1.coetaco and co1.tmp_batch = p1.tmp_batch AND p1.ACTIF = 'O'
		) tmp3 on cl.NOSIREN = tmp3.nosiren
	inner join param_utilisateur_gestionnaire pug on pug.`TYPE` = 'MULTI_MULTI'
		and pug.GG_INCLUS_1 <> '' and pug.GG_INCLUS_2 = '' and pug.GG_EXCLUS <> ''
	where exists (
		select 1 from param_utilisateur_gestionnaire pug2 
		where 1 = 1 
		and pug2.GG_inclus_1 not like concat('%',tmp1.nmgrpges,'%') 
		and pug2.GG_exclus not like concat('%',tmp1.nmgrpges,'%')
		and pug2.`TYPE` = 'MULTI_MULTI'
		and pug2.GG_INCLUS_1 <> '' and pug2.GG_INCLUS_2 = '' and pug2.GG_EXCLUS <> ''
		and pug2.code_user = pug.code_user
		)
	and exists (
		select 1 from param_utilisateur_gestionnaire pug2 
		where 1 = 1 
		and pug2.GG_inclus_1 like concat('%',tmp2.nmgrpges,'%')
		and pug2.`TYPE` = 'MULTI_MULTI'
		and pug2.GG_INCLUS_1 <> '' and pug2.GG_INCLUS_2 = '' and pug2.GG_EXCLUS <> ''
		and pug2.code_user = pug.code_user
		)
	and exists (
		select 1 from param_utilisateur_gestionnaire pug2 
		where 1 = 1 
		and pug2.GG_exclus like concat('%',tmp3.nmgrpges,'%')
		and pug2.`TYPE` = 'MULTI_MULTI'
		and pug2.GG_INCLUS_1 <> '' and pug2.GG_INCLUS_2 = '' and pug2.GG_EXCLUS <> ''
		and pug2.code_user = pug.code_user
		)
	)
group by tmp_siren_liste_gg.NOSIREN






-- ####################################################################
-- ####################################################################
-- CAS 6.1 : 1 ou plusieurs groupes hors 2 domaines (quelques soit le nombre de gestionnaires)
-- ####################################################################
-- ####################################################################
union

select tmp_siren2.NOSIREN, pug2.code_user, pug2.NOM, pug2.PRENOM, pug2.NIVEAU_1, pug2.NIVEAU_2, pug2.NIVEAU_3
from 
	(select cl.nosiren, group_concat(distinct co.nmgrpges order by co.nmgrpges separator '%') gg
				from 
					(	select distinct contrats.noco as noco, clients.nosiren as nosiren from clients inner join contrats on clients.nocli = contrats.nocli
					union 
						select distinct ext.noco as noco, ext.siren as nosiren from extensions_entreprises_affiliees ext
					) as cl
				inner join contrats co on cl.noco = co.noco and co.DT_FIN_SIT is null and co.tmp_batch IS FALSE AND (co.eligdsn = 'SRN' OR co.eligdsn = 'SRT')
				inner join param_etat_contrat p on p.COETACO = co.coetaco and co.tmp_batch = p.tmp_batch AND p.ACTIF = 'O'
				left join (
					select distinct a_traiter_par, p.numero_contrat from periodes_recues p
					inner join contrats ct on p.NUMERO_CONTRAT = ct.NOCO
					inner join (
						select p1.numero_contrat, tmp1.dt_rec, max(p1.dt_creation) dt_crea 
						from periodes_recues p1
						inner join (
							select numero_contrat, max(date_debut_periode) dt_rec
							from periodes_recues
							group by numero_contrat
						) as tmp1 on tmp1.numero_contrat = p1.numero_contrat and tmp1.dt_rec = p1.DATE_DEBUT_PERIODE
						group by numero_contrat, tmp1.dt_rec
					) as tmp2 on tmp2.numero_contrat = p.numero_contrat and tmp2.dt_rec = p.DATE_DEBUT_PERIODE and tmp2.dt_crea = p.DT_CREATION
				) as pr on pr.numero_contrat = co.noco
				where cl.nosiren not in (select siren from param_siren_faux)
				group by cl.nosiren
				) as tmp_siren2
inner join param_utilisateur_gestionnaire pug2 on pug2.`TYPE` = 'GEST_AUTRE'
where pug2.GG_INCLUS_1 = '' and pug2.GG_INCLUS_2 = '' and pug2.GG_EXCLUS <> ''
and tmp_siren2.nosiren not in (
	select distinct cl.NOSIREN
	from
		(	select distinct contrats.noco as noco, clients.nosiren as nosiren from clients inner join contrats on clients.nocli = contrats.nocli
		union 
			select distinct ext.noco as noco, ext.siren as nosiren from extensions_entreprises_affiliees ext
		) as cl
	inner join (
		select distinct nosiren, nmgrpges 
		from 
			(	select distinct contrats.noco as noco, clients.nosiren as nosiren from clients inner join contrats on clients.nocli = contrats.nocli
			union 
				select distinct ext.noco as noco, ext.siren as nosiren from extensions_entreprises_affiliees ext
			) as cl1
		inner join contrats co1 on cl1.noco = co1.noco and co1.DT_FIN_SIT is null and co1.tmp_batch IS FALSE AND (co1.eligdsn = 'SRN' OR co1.eligdsn = 'SRT')
		inner join param_etat_contrat p1 on p1.COETACO = co1.coetaco and co1.tmp_batch = p1.tmp_batch AND p1.ACTIF = 'O'
		) tmp3 on cl.NOSIREN = tmp3.nosiren
	where	exists (
		select 1 from param_utilisateur_gestionnaire pug2 
		where pug2.GG_exclus like concat('%',tmp3.nmgrpges,'%')
		and pug2.`TYPE` = 'GEST_AUTRE'
		and pug2.GG_INCLUS_1 = '' and pug2.GG_INCLUS_2 = '' and pug2.GG_EXCLUS <> ''
		) 
	)
group by tmp_siren2.NOSIREN



-- ####################################################################
-- ####################################################################
-- CAS 7.1 : OUBLIE DANS LA SFD : plusieurs groupes dans les 2 domaines + autre (quelque soit le nb de gestionnaires)
-- ####################################################################
-- ####################################################################
union

select tmp_siren2.NOSIREN, pug2.code_user, pug2.NOM, pug2.PRENOM, pug2.NIVEAU_1, pug2.NIVEAU_2, pug2.NIVEAU_3
from 
	(select cl.nosiren, group_concat(distinct co.nmgrpges order by co.nmgrpges separator '%') gg
				from 
					(	select distinct contrats.noco as noco, clients.nosiren as nosiren from clients inner join contrats on clients.nocli = contrats.nocli
					union 
						select distinct ext.noco as noco, ext.siren as nosiren from extensions_entreprises_affiliees ext
					) as cl
				inner join contrats co on cl.noco = co.noco and co.DT_FIN_SIT is null and co.tmp_batch IS FALSE AND (co.eligdsn = 'SRN' OR co.eligdsn = 'SRT')
				inner join param_etat_contrat p on p.COETACO = co.coetaco and co.tmp_batch = p.tmp_batch AND p.ACTIF = 'O'
				left join (
					select distinct a_traiter_par, p.numero_contrat from periodes_recues p
					inner join contrats ct on p.NUMERO_CONTRAT = ct.NOCO
					inner join (
						select p1.numero_contrat, tmp1.dt_rec, max(p1.dt_creation) dt_crea 
						from periodes_recues p1
						inner join (
							select numero_contrat, max(date_debut_periode) dt_rec
							from periodes_recues
							group by numero_contrat
						) as tmp1 on tmp1.numero_contrat = p1.numero_contrat and tmp1.dt_rec = p1.DATE_DEBUT_PERIODE
						group by numero_contrat, tmp1.dt_rec
					) as tmp2 on tmp2.numero_contrat = p.numero_contrat and tmp2.dt_rec = p.DATE_DEBUT_PERIODE and tmp2.dt_crea = p.DT_CREATION
				) as pr on pr.numero_contrat = co.noco
				where cl.nosiren not in (select siren from param_siren_faux)
				group by cl.nosiren
				) as tmp_siren2
inner join param_utilisateur_gestionnaire pug2 on pug2.`TYPE` = 'MULTI_2DA'
where pug2.GG_INCLUS_1 <> '' and pug2.GG_INCLUS_2 <> '' and pug2.GG_EXCLUS = ''
and tmp_siren2.nosiren in (
	select distinct cl.NOSIREN
	from 
		(	select distinct contrats.noco as noco, clients.nosiren as nosiren from clients inner join contrats on clients.nocli = contrats.nocli
		union 
			select distinct ext.noco as noco, ext.siren as nosiren from extensions_entreprises_affiliees ext
		) as cl
	inner join (
		select distinct nosiren, nmgrpges 
		from 
			(	select distinct contrats.noco as noco, clients.nosiren as nosiren from clients inner join contrats on clients.nocli = contrats.nocli
			union 
				select distinct ext.noco as noco, ext.siren as nosiren from extensions_entreprises_affiliees ext
			) as cl1
		inner join contrats co1 on cl1.noco = co1.noco and co1.DT_FIN_SIT is null and co1.tmp_batch IS FALSE AND (co1.eligdsn = 'SRN' OR co1.eligdsn = 'SRT')
		inner join param_etat_contrat p1 on p1.COETACO = co1.coetaco and co1.tmp_batch = p1.tmp_batch AND p1.ACTIF = 'O'
		) tmp1 on cl.NOSIREN = tmp1.nosiren
	inner join (
		select distinct nosiren, nmgrpges 
		from 
			(	select distinct contrats.noco as noco, clients.nosiren as nosiren from clients inner join contrats on clients.nocli = contrats.nocli
			union 
				select distinct ext.noco as noco, ext.siren as nosiren from extensions_entreprises_affiliees ext
			) as cl1
		inner join contrats co1 on cl1.noco = co1.noco and co1.DT_FIN_SIT is null and co1.tmp_batch IS FALSE AND (co1.eligdsn = 'SRN' OR co1.eligdsn = 'SRT')
		inner join param_etat_contrat p1 on p1.COETACO = co1.coetaco and co1.tmp_batch = p1.tmp_batch AND p1.ACTIF = 'O'
		) tmp2 on cl.NOSIREN = tmp2.nosiren
	inner join (
		select distinct nosiren, nmgrpges 
		from 
			(	select distinct contrats.noco as noco, clients.nosiren as nosiren from clients inner join contrats on clients.nocli = contrats.nocli
			union 
				select distinct ext.noco as noco, ext.siren as nosiren from extensions_entreprises_affiliees ext
			) as cl1
		inner join contrats co1 on cl1.noco = co1.noco and co1.DT_FIN_SIT is null and co1.tmp_batch IS FALSE AND (co1.eligdsn = 'SRN' OR co1.eligdsn = 'SRT')
		inner join param_etat_contrat p1 on p1.COETACO = co1.coetaco and co1.tmp_batch = p1.tmp_batch AND p1.ACTIF = 'O'
		) tmp3 on cl.NOSIREN = tmp3.nosiren
	inner join param_utilisateur_gestionnaire pug on pug.`TYPE` = 'MULTI_MULTI'
		and pug.GG_INCLUS_1 <> '' and pug.GG_INCLUS_2 <> '' and pug.GG_EXCLUS = ''
	where exists (
		select 1 from param_utilisateur_gestionnaire pug2 
		where 1 = 1 
		and pug2.GG_inclus_1 not like concat('%',tmp1.nmgrpges,'%') 
		and pug2.GG_INCLUS_2 not like concat('%',tmp1.nmgrpges,'%')
		and pug2.`TYPE` = 'MULTI_MULTI'
		and pug2.GG_INCLUS_1 <> '' and pug2.GG_INCLUS_2 <> '' and pug2.GG_EXCLUS = ''
		and pug2.code_user = pug.code_user
		)
	and exists (
		select 1 from param_utilisateur_gestionnaire pug2 
		where 1 = 1 
		and pug2.GG_inclus_1 like concat('%',tmp2.nmgrpges,'%')
		and pug2.`TYPE` = 'MULTI_MULTI'
		and pug2.GG_INCLUS_1 <> '' and pug2.GG_INCLUS_2 <> '' and pug2.GG_EXCLUS = ''
		and pug2.code_user = pug.code_user
		)
	and exists (
		select 1 from param_utilisateur_gestionnaire pug2 
		where 1 = 1 
		and pug2.GG_INCLUS_2 like concat('%',tmp3.nmgrpges,'%')
		and pug2.`TYPE` = 'MULTI_MULTI'
		and pug2.GG_INCLUS_1 <> '' and pug2.GG_INCLUS_2 <> '' and pug2.GG_EXCLUS = ''
		and pug2.code_user = pug.code_user
		)
	)
group by tmp_siren2.NOSIREN

order by NOSIREN;
END$$
DELIMITER ;


