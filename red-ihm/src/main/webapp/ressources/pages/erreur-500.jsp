<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" isErrorPage="true" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>REDAC - Erreur inattendue</title>
</head>
<body>
	<h1>REDAC - CDE</h1>
	<p>
		Une erreur technique inattendue a été détectée, veuillez s'il vous plaît en informer l'administrateur de l'application.
	</p>
	<p>
		<a href="${pageContext.request.contextPath}/ihm">&gt; Revenir à  l'écran d'accueil</a>
	</p>
</body>
</html>
<% response.setStatus(500); %>
