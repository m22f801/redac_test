// Déclaration des dropdowns multiselect
var multiselectDefaultConfig = {
	nonSelectedText : 'Aucune sélection',
	buttonWidth : '170px',
	numberDisplayed : 1,
	maxHeight : 300,
	buttonClass : 'multiselect',
	nSelectedText : ' sélections',
	allSelectedText : 'Sélection complète'
};
var multiselectDefaultConfigCourt = {
	nonSelectedText : 'Aucune sélection',
	buttonWidth : '70px',
	numberDisplayed : 1,
	maxHeight : 300,
	buttonClass : 'multiselect',
	nSelectedText : ' sélections',
	allSelectedText : 'Sélection complète'
};

$('#indicExploitationSaisi').multiselect(multiselectDefaultConfigCourt);
$('#indicConsignePaiementSaisi').multiselect(multiselectDefaultConfigCourt);
$('#indicTransfertSaisi').multiselect(multiselectDefaultConfigCourt);
$('#vipSaisi').multiselect(multiselectDefaultConfig);
$('#baseAssuj').multiselect(multiselectDefaultConfig);
$('#gestionDirecte').multiselect(multiselectDefaultConfig);
$('#modeReaffectation').multiselect(multiselectDefaultConfig);
$('#modeDeclaration').multiselect(multiselectDefaultConfig);

/**
 * Affiche un message de retour sur une création si "ok" vaut true ou false.
 * 
 */
afficheMessageSuppression = function(url) {
	if (confirm('Voulez-vous supprimer les spécificités de ce contrat ?')) {
		document.suppressionFormulaire.action = url;
		document.suppressionFormulaire.submit();
	}
};

var secondCallSpecif = false;
function actionSpecifContratBeforeSubmit(url, buttonName,buttonValue) {
	
	if (secondCallSpecif) {
        return;
    }
	
	if(!confirm('Voulez-vous enregistrer les caractéristiques de ce contrat ?')) {
		return false;
	}
	else{
		// Suppression du masque sur +
		$("#dateEffet").val($("#dateEffet").mask());

		document.detailFormulaire.action = url;
		secondCallSpecif = true;
		document.detailFormulaire.submit();
	} 
	return true;
}

/**
 * Vérifie le couple siren/nic saisi puis l'ajoute à la liste si OK.
 * 
 * @param eligibilite
 *            indique l'eligibilité du contrat affiché
 */
function ajoutSirenNic(eligibilite) {
	var champSirenOK = true, champNicOK = true, siretUnique = true;
	var champSiren = $.trim($("#sirenSaisi").val()), champNic = $.trim($("#nicSaisi")
			.val());

	if (!champSiren) {
		champSirenOK = false;
		// Ajout message d'erreur si besoin
		var testDivErreurSiren = document.getElementById('erreurSiren');
		if (!testDivErreurSiren) {
			var divErreurSiren = document.createElement('div');
			divErreurSiren.id = 'erreurSiren';
			divErreurSiren.innerHTML = 'Le SIREN est obligatoire';

			document.getElementById('informations').appendChild(divErreurSiren);
		}
	}

	if (!champNic) {
		champNicOK = false;
		// Ajout message d'erreur si besoin
		var testDivErreurNic = document.getElementById('erreurNic');
		if (!testDivErreurNic) {
			var divErreurNic = document.createElement('div');
			divErreurNic.id = 'erreurNic';
			divErreurNic.innerHTML = 'Le NIC est obligatoire';

			document.getElementById('informations').appendChild(divErreurNic);
		}
	}
	if (champSirenOK) {
		supprimerMessageErreur('erreurSiren');
	}
	if (champNicOK) {
		supprimerMessageErreur('erreurNic');
	}
	if (champSirenOK && champNicOK) {
		var divListeSirenNic = document.getElementById('listeSirenNic');
		var siret = champSiren + ' ' + champNic;
		if (divListeSirenNic.value.indexOf(siret) > -1) {
			siretUnique = false;
			// Ajout message d'erreur si besoin
			var testDivSiretDoublon = document.getElementById('siretDoublon');
			if (!testDivSiretDoublon) {
				var divSiretDoublon = document.createElement('div');
				divSiretDoublon.id = 'siretDoublon';
				divSiretDoublon.innerHTML = 'Le SIRET ' + siret + ' est déjà enregistré';

				document.getElementById('informations').appendChild(divSiretDoublon);
			}
		} else {
			// Création couple Siren/Nic
			var nbCouple = $("#nbCoupleSirenNic").val() + 'i';
			// compte le nombre de 'i' dans nbCouple (= nombre de siren/nic saisis) et en déduit le tabIndex courant
			var tabIndex = 16 + (nbCouple.match(/i/g) || []).length;			
			// alert('nb de couples brut: ' + $("#nbCoupleSirenNic").val());
			$("#nbCoupleSirenNic").val(nbCouple);
			// alert('nouveau nb de couples: ' + $("#nbCoupleSirenNic").val());

			var div = document.createElement('div');
			div.className = 'coupleSirenNic';
			div.innerHTML = siret
					+ '&nbsp;<button id="couple_'
					+ nbCouple
					+ '" class="buttonSirenNic" type="button" onclick="supprimerSirenNic(this)" tabindex="'
					+ tabIndex
					+ '" ><span class="texteSupprimeSirenNic">-</span></button>'

			document.getElementById('sirenNicAffilies').appendChild(div);

			document.getElementById("enregistre").tabIndex = tabIndex + 1;
			ajouterAListeSirenNic(siret);
		}
	}
	if (siretUnique) {
		supprimerMessageErreur('siretDoublon');
	}
};

/**
 * Ajoute un couple siren/nic à la liste.
 * 
 * @param champSiren
 *            valeur du champ siren à ajouter
 * @param champNic
 *            valeur du champ nic à ajouter
 */
function ajouterAListeSirenNic(siret) {
	var divListeSirenNic = document.getElementById('listeSirenNic');
	divListeSirenNic.value += siret + ';';
	$("#sirenSaisi").val('');
	$("#nicSaisi").val('');
	// alert(divListeSirenNic.value);
}

/**
 * Ajoute un couple siren/nic à la liste.
 * 
 * @param div
 *            l'élément dom du couple à supprimer
 */
function supprimerSirenNic(div) {
	// Suppression de la table
	document.getElementById('sirenNicAffilies').removeChild(div.parentNode);

	// Suppression du couple de la liste
	var index = div.id.split('i').length - 2;
	// alert('id : ' +div.id + ' | index: '+ index);
	var divListeSirenNic = document.getElementById('listeSirenNic');
	var strArray = divListeSirenNic.value.split(';');
	strArray[index] = '';
	divListeSirenNic.value = strArray.join(';');
	// alert(divListeSirenNic.value);

	// Suppression éventuelle des messages d'erreur
	supprimerMessageErreur('erreurSiren');
	supprimerMessageErreur('erreurNic');
	supprimerMessageErreur('siretDoublon');
}

/**
 * Supprime un message d'erreur si besoin (si affiché).
 * 
 * @param id
 *            l'identifiant de l'élement message à supprimer
 */
function supprimerMessageErreur(id) {
	// 
	var divErreur = document.getElementById(id);
	if (divErreur) {
		document.getElementById('informations').removeChild(divErreur);
	}
}

window.onload = function() {
	if ($("#roleGestionCde").val() == 'false' || $("#habilitationVIP").val() == 'false' && $("#contratVIP").val() == 'true') {
		$("#suppressionFormulaire :input").prop("disabled", true);
		$("#detailFormulaire :input").prop("disabled", true);
		$("#detailFormulaire :button").prop("disabled", true);
		$('[id^=couple_]').prop("disabled", true);
	}
	if ($("#extensionContratExiste").val() == 'false') {
		$("#boutonSuppresion").prop("disabled", true);
	}
};

$(function() {
	$.mask.definitions['~'] = "[+-]";
	$("#dateEffet").mask("99/99/9999");
});
