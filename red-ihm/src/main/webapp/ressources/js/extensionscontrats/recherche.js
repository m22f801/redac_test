// Déclaration des dropdowns multiselect
var multiselectDefaultConfig = {
    nonSelectedText: 'Aucune sélection',
    buttonWidth: '170px',
    numberDisplayed: 1,
    maxHeight: 300,
    buttonClass: 'multiselect',
    nSelectedText: ' sélections',
    allSelectedText: 'Sélection complète'
};
$('#groupesGestion').multiselect(multiselectDefaultConfig);
$('#eligibiliteContratDsn').multiselect(multiselectDefaultConfig);
$('#familles').multiselect(multiselectDefaultConfig);
$('#indicExploitation').multiselect(multiselectDefaultConfig);
$('#modesCalculCotisation').multiselect(multiselectDefaultConfig);
$('#indicConsignePaiement').multiselect(multiselectDefaultConfig);
$('#modeNatureContrat').multiselect(multiselectDefaultConfig);
$('#indicTransfert').multiselect(multiselectDefaultConfig);
$('#entrepriseAffiliee').multiselect(multiselectDefaultConfig);
$('#vip').multiselect(multiselectDefaultConfig);


window.onload = function() {
	if ($("#creerSpecificite").val() == 'false') {
		$("#nouvelleSpecificite").prop("disabled", true);
	}
};

//validation num client
$(document).ready(function() {
	
	$('input').on("keypress", function(event) {
	var code = event.charCode || event.keyCode;
	
	// codes valides (enter, backspace, left/right arrow, del )
	var valideCode=[8,13,37,39,46];
	
	// en autorisant c/v, le copier/coller est permis.
	// la validation du champ empechera c/v d'être envoyé si saisis
	var flagCC= event.charCode==99 ||  event.charCode==118;
	
    // numbers only
	if((code < 48 || code > 57) && event.target.id=="numClient" && valideCode.indexOf(code)== -1 && !flagCC){
	        event.preventDefault();
	        return false;
	}
	
	// gestion enter
	if( ((code == 13) && (validationFunction() == false))) {
      // prevent form submit if validation fails
      event.preventDefault();
      return false;
    }
    
	});

});



//Validation des champs
function validationFunction() {
	  // check if numclient a number

	  var num_cli = Number($("#numClient").val());
	  return ( (!isNaN(num_cli)) || $("#numClient").val()=="") ;
}

// Call onsubmit form
function ValidationFormulaire() {
	if(validationFunction() == false){
		// prevent form submit if validation fails
	      event.preventDefault();
	      return false;
	}
}
