// Déclaration des dropdowns multiselect
var multiselectDefaultConfig = {
    nonSelectedText: 'Aucune sélection',
    buttonWidth: '170px',
    numberDisplayed: 1,
    maxHeight: 300,
    buttonClass: 'multiselect',
    nSelectedText: ' sélections',
    allSelectedText: 'Sélection complète'
};
$('#statuts').multiselect(multiselectDefaultConfig);
$('#groupesGestion').multiselect(multiselectDefaultConfig);
$('#messages').multiselect(multiselectDefaultConfig);
$('#familles').multiselect(multiselectDefaultConfig);
$('#idControles').multiselect(multiselectDefaultConfig);
$('#typesPeriode').multiselect(multiselectDefaultConfig);
$('#trimestres').multiselect(multiselectDefaultConfig);
$('#versements').multiselect(multiselectDefaultConfig);
$('#vip').multiselect(multiselectDefaultConfig);

// configuration lib selectize sur listes gestionnaires
$('#affecteA').selectize(
		{
			maxItems: null,
			placeholder:"Aucune sélection",
			plugins: ['remove_button']
		});

$('#aTraiterPar').selectize(
		{
			maxItems: null,
			placeholder:"Aucune sélection",
			plugins: ['remove_button']	
		});

$('#traitePar').selectize(
		{
			maxItems: null,
			placeholder:"Aucune sélection",
			plugins: ['remove_button']
		});



// Gestion des messages de contrôle dans la liste des résultats
var tooltipsterConfig = {
    animation: 'fade',
    speed: 0, // Pas d'animation
    content: 'Chargement...',
    contentAsHTML: true,
    minWidth: 200,
    position: 'left',
    theme: 'tooltip-theme',
    functionBefore: function(origin, continueTooltip) {
        continueTooltip();
        if (origin.data('ajax') !== 'cached') {
            $.ajax({
                type: 'GET',
                url: $(origin[0]).attr('url-messages'),
                success: function(data) {
                    // update our tooltip content with our returned data and cache it
                    origin.tooltipster('content', data).data('ajax', 'cached');
                }
            });
        }
    }
};
$('div.actionTableau.rejet').tooltipster(tooltipsterConfig);
$('div.actionTableau.signal').tooltipster(tooltipsterConfig);
$('div.actionTableau.aTraiterPar').tooltipster(tooltipsterConfig);

$(document).ready(function() {
	//gestion de la taille des libelles messages
	$('button[tabindex=7]').on("click", function(event) {
		$('button[tabindex=7]').next().css("width", "auto");
		});
	
	// validation num client
	$('input').on("keypress", function(event) {
	var code = event.charCode || event.keyCode;
	
	// codes valides (enter, backspace, left/right arrow, del )
	var valideCode=[8,13,37,39,46];
	
	// en autorisant c/v, le copier/coller est permis.
	// la validation du champ empechera c/v d'être envoyé si saisis
	var flagCC= event.charCode==99 ||  event.charCode==118;
	
    // numbers only
	if((code < 48 || code > 57) && event.target.id=="numClient" && valideCode.indexOf(code)== -1 && !flagCC ){
	        event.preventDefault();
	        return false;
	}
	
	// gestion enter
	if( ((code == 13) && (validationFunction() == false))) {
      // prevent form submit if validation fails
      event.preventDefault();
      return false;
    }
    
	});
	
	//si la checkbox de sélection globale est cochée alors cocher toutes périodes sinon les décocher
    $('#select_all').on('click',function(){
        if(this.checked){
			$(".ligneTableauResultat input[type='checkbox']").each(function(){
                this.checked = true;
            });
        }else{
             $(".ligneTableauResultat input[type='checkbox']").each(function(){
                this.checked = false;
            });
        }
    });
    
    //si toutes les périodes sont cochées une à une alors cocher la checkbox de sélection globale
    $(".ligneTableauResultat input[type='checkbox']").on('click',function(){
        if($(".ligneTableauResultat input[type='checkbox']:checked").length == $(".resultats input[type='checkbox']").length){
            $('#select_all').prop('checked',true);
        }else{
            $('#select_all').prop('checked',false);
        }
    });
  
});

//Validation des champs
function validationFunction() {
	  // check if numclient a number

	  var num_cli = Number($("#numClient").val());
	  return ( (!isNaN(num_cli)) || $("#numClient").val()=="") ;
}

// Call onsubmit form
function ValidationFormulaire() {
	// on verifie pour l'instant que le num client
	if(validationFunction() == false){
		// prevent form submit if validation fails
	      event.preventDefault();
	      return false;
	}
}