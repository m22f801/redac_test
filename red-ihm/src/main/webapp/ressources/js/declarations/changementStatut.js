
// onglet changement statut 
$(document).ready(function(){
		
		//Etat initial de la zone de commentaire
		
		$('#commentaireUtilisateur').on('keyup',function(){
	 		//si l'état de la checkbox change de son état initial on active le bouton "Valider"
		    if($('#commentaireUtilisateur').val() == ''){
		    	$('#commentairebtn').prop('disabled', true);
		    }
		    else{
		    	$('#commentairebtn').prop('disabled', false);	
		    }
		});
	
		//état initial de la checkbox au chargement de la page
		var state = $('#attenteRetourEntreprise').is(':checked');
		$('#attenteRetourEntreprise').on('click',function(){
		 		//si l'état de la checkbox change de son état initial on active le bouton "Valider"
			    if($('#attenteRetourEntreprise').is(':checked') == state){
			    	$('#validerRetourEntreprise').prop('disabled', true);
			    }
			    else{
			    	$('#validerRetourEntreprise').prop('disabled', false);	
			    }
			});
		
		//Désactivation du boutton d'assignation si l'element séléctionné est l'élément initial
		 var gestionnaireAffecte = $('#gestionnaireAffecteA').val();
		 $('#bouttonAffectation').prop('disabled', true);
		 
		    $('#gestionnaireAffecteA').on('change',function(){
		    	//Si la valeur est identique on désactive le boutton
		    	if(gestionnaireAffecte == $('#gestionnaireAffecteA').val() || $('#gestionnaireAffecteA').val() == "" ){
		    		$('#bouttonAffectation').prop('disabled', true);
		    	}else{
		    		$('#bouttonAffectation').prop('disabled', false);
		    	}	
			});
		
		//Désactivation du boutton d'assignation si l'element séléctionné est l'élément initial
		 var gestionnaireAssigne = $('#gestionnaireATraiterPar').val();
		 $('#bouttonAssignation').prop('disabled', true);
		   
		    $('#gestionnaireATraiterPar').on('change',function(){
		    	//Si la valeur est identique on désactive le boutton
		    	if(gestionnaireAssigne == $('#gestionnaireATraiterPar').val() || $('#gestionnaireATraiterPar').val() == ""){
		    		$('#bouttonAssignation').prop('disabled', true);
		    	}else{
		    		$('#bouttonAssignation').prop('disabled', false);
		    	}	
			});

		//Désactivation du 
		 var gestionnaireStatut = $('#etatPeriode').val();
		 $('#changementstatutbtn').prop('disabled', true);
		   
		    $('#etatPeriode').on('change',function(){
		    	//Si la valeur est identique on désactive le boutton
		    	if(gestionnaireStatut == $('#etatPeriode').val() || $('#etatPeriode').val() == ""){
		    		$('#changementstatutbtn').prop('disabled', true);
		    	}else{
		    		$('#changementstatutbtn').prop('disabled', false);
		    	}	
			});		    
		    
			// initialisation selectize
			$('#gestionnaireAffecteA').selectize({maxItems: 1,placeholder:"",plugins: ['remove_button']});
			$('#gestionnaireATraiterPar').selectize({maxItems: 1,placeholder:"",plugins: ['remove_button']});
});
	
	