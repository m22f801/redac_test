
// On fusionne au minimum 2 périodes - F09_RG_1_15 
function validationNombrePeriodes(listeids,messagesControles){
	var plusieursPeriodes =  listeids.length>1;
	
	if(!plusieursPeriodes){
		messagesControles.push("- Veuillez sélectionner à minima 2 périodes à fusionner ");
	}
	
	return plusieursPeriodes;
}

// toute les périodes sélectionnées doivent concerner le même contrat - F09_RG_1_15
function validationNumeroContrat(listeids,messagesControles){
	var nocoIdentique = true;
	
	for(var i=1;i<listeids.length;i++){
		if($("#numeroContrat_"+listeids[i]+" span").html() != $("#numeroContrat_"+listeids[i-1]+" span").html()){
			nocoIdentique= false;
			messagesControles.push("- Les périodes sélectionnées pour fusion doivent toutes porter sur le même contrat ");
			break;
		}
	}
	
	return nocoIdentique;
}

//toute les périodes sélectionnées doivent avoir même date début / date de fin - F09_RG_1_15
function validationDateDebutFinPeriode(listeids,messagesControles){
	var datesIdentique = true;
	
	
	for(var i=1;i<listeids.length;i++){
		if($("#dateDebutPeriode_"+listeids[i]).html() != $("#dateDebutPeriode_"+listeids[i-1]).html()){
			datesIdentique = false;
			messagesControles.push("- Les périodes sélectionnées pour fusion doivent toutes avoir les mêmes dates de début et de fin ");
			break;
		}
	}
	
	// si les dates de debut sont pas identiques, inutile de tester les dates de fin, le controle est déjà invalide
	if(datesIdentique){
		for(var i=1;i<listeids.length;i++){
			if($("#dateFinPeriode_"+listeids[i]).html() != $("#dateFinPeriode_"+listeids[i-1]).html()){
				datesIdentique = false;
				messagesControles.push("- Les périodes sélectionnées pour fusion doivent toutes avoir les mêmes dates de début et de fin ");
				break;
			}
		}
	}

	return datesIdentique;
}

//toute les périodes sélectionnées doivent avoir un statut valide - F09_RG_1_15
function validationStatut(listeids,messagesControles){
	var verificationStatut = true;
	var statusValides = [ "RCP","NIN"];
	
	for(var i=0;i<listeids.length;i++){
		if(statusValides.indexOf($("#etat_"+listeids[i]).html()) === -1 ){
			verificationStatut= false;
			messagesControles.push("- Les périodes sélectionnées pour fusion doivent être au statut « En réception » ou « Non intégrée » ");
			break;
		}
	}
	
	return verificationStatut;
}

//toute les périodes sélectionnées doivent avoir un statut valide - F09_RG_1_15
function validationBlocageGestionnaire(listeids,messagesControles){
	var verificationBlocage = true;
	
	for(var i=1;i<listeids.length;i++){
		if($("#estBloque_"+listeids[i]).html() != $("#estBloque_"+listeids[i-1]).html()){
			verificationBlocage = false;
			messagesControles.push("- Les périodes sélectionnées pour fusion doivent toutes être bloquées manuellement par la gestion ou aucune ne doit être bloquée manuellement par la gestion ");
			break;
		}
	}
	
	return verificationBlocage;
}

// decoche les cases, annulation de la fusion F09_RG_1_26
function decocheCasesFusion(){
	$('.ligneTableauResultat input[type=checkbox]:checked').attr('checked',false);
	$('#select_all').attr('checked',false);
}

function generationResumePeriode(listeids){
	var texte = "";
	for(var i=0;i<listeids.length;i++){
		var tmp  = "- "+$("#libelleType_"+listeids[i]+" span").html() +" "+$("#periode_"+listeids[i]+" span").html() + " pour le Contrat " + $("#numeroContrat_"+listeids[i]+" span").html() + " " + $("#raisonSociale_"+listeids[i]).html()+ "\n";
		texte += tmp;
	}
	return texte;
}

// Call onsubmit form - F09_RG_1_15
function ValidationFusion() {
	
	// tableau avec les id des périodes selectionnées pour fusion
	var listeids = [];
	
	// indique si tout les controles sont valides
	var controlesValides = true;
	var messagesControles = [];
	
	$('.ligneTableauResultat input[type=checkbox]:checked').each(function() {
		listeids.push($(this).attr('id'));
	});
	
	// on ajoute les id selectionnés dans le champ du formulaire
	$('#idsPeriodesFusion').val(listeids.join(','));
	
	$('#parametresRechercheFusion').val($("#parametresRecherche").val());
	
	validationNombrePeriodes(listeids, messagesControles);
	validationNumeroContrat(listeids,messagesControles);
	validationDateDebutFinPeriode(listeids, messagesControles);
	validationStatut(listeids, messagesControles);
	validationBlocageGestionnaire(listeids, messagesControles);
	
	if(messagesControles.length>0){
		alert(messagesControles.join("\n"));
        return false;
	}else{
		// F09_RG_1_16 et F09_RG_1_26
		var textePupupConfirmation = "Attention, vous demandez la fusion des "+ listeids.length + " périodes suivantes, confirmez-vous votre demande ? \n" 
		+ generationResumePeriode(listeids);
		
		if(! confirm(textePupupConfirmation)){
			decocheCasesFusion();
			
	        return false;
		}
	}
	
}