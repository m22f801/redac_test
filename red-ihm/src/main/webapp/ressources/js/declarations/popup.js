$(document).ready(function()
{
	// init des listes selectize
	$('#listeStatuts').selectize({maxItems: 1,placeholder:"Aucune sélection",plugins: ['remove_button']});
	$('#gestionnaireATraiterParEnMasse').selectize({maxItems: 1,placeholder:"Aucune sélection",plugins: ['remove_button']});
	$('#gestionnaireAffecteAEnMasse').selectize({maxItems: 1,placeholder:"Aucune sélection",plugins: ['remove_button']});
	
	// clean initial listes avec selectize
    cleanListeSelectize('#gestionnaireATraiterParEnMasse');
    cleanListeSelectize('#gestionnaireAffecteAEnMasse');
    cleanListeSelectize('#listeStatuts');

    // Reset modal content
    $('.modal').on('hidden.bs.modal', function(e)
    { 
    	$(this)
        .find(".modal-body textarea,.modal-body select")
           .val('')
           .end()
        .find(".modal-body input[type=checkbox],.modal-body input[type=radio]")
           .prop("checked", "")
           .end();
    	
    	$("button.spaceInputPopup").attr("disabled","disabled");
    	$("button.spaceInputPopup").attr("style","color: gray;");
    	
    	
        // clean listes avec selectize
        cleanListeSelectize('#gestionnaireATraiterParEnMasse');
        cleanListeSelectize('#gestionnaireAffecteAEnMasse');
        cleanListeSelectize('#listeStatuts');

  
    });
    
    var gestionnaireAffecte = $('#gestionnaireAffecteAEnMasse').val();
  	 $('#flagAffecterGestionnaireEnMasse').prop('disabled', true);
  	 
  	    $('#gestionnaireAffecteAEnMasse').on('change',function(){
  	    	//Si la valeur est identique on désactive le boutton
  	    	if(gestionnaireAffecte == $('#gestionnaireAffecteAEnMasse').val()){
  	    		$('#flagAffecterGestionnaireEnMasse').prop('disabled', true);
  	    		$("#flagAffecterGestionnaireEnMasse").attr("style","color: gray;");
  	    	}else{
  	    		$('#flagAffecterGestionnaireEnMasse').prop('disabled', false);
  	    		supprimeAttribut('#flagAffecterGestionnaireEnMasse','style');
  	    	}	
  		});
   	
   		
   	 //Désactivation du boutton d'assignation si l'element séléctionné est l'élément initial
   	 var gestionnaireATraiter = $('#gestionnaireATraiterParEnMasse').val();
   	 $('#flagAssignerGestionnaireEnMasse').prop('disabled', true);
   	 
   	    $('#gestionnaireATraiterParEnMasse').on('change',function(){
   	    	//Si la valeur est identique on désactive le boutton
   	    	if(gestionnaireATraiter == $('#gestionnaireATraiterParEnMasse').val()){
   	    		$('#flagAssignerGestionnaireEnMasse').prop('disabled', true);
   	    		$("#flagAssignerGestionnaireEnMasse").attr("style","color: gray;");
   	    	}else{
   	    		$('#flagAssignerGestionnaireEnMasse').prop('disabled', false);
   	    		supprimeAttribut('#flagAssignerGestionnaireEnMasse','style');
   	    	}	
   		});
   	    
   	//Désactivation du boutton d'assignation si l'element séléctionné est l'élément initial
   	    var statutBlanc = $('#listeStatuts').val();
   	    $('#flagEnregistrerChangementStatutEnMasse').prop('disabled', true);
   	    
   	    $('#listeStatuts').on('change',function(){
   	    
   	    //Si la valeur est identique on désactive le boutton
   	  	    if($('#listeStatuts').val().length<1){
   	  	    	$('#flagEnregistrerChangementStatutEnMasse').prop('disabled', true);
   	  	    	$("#flagEnregistrerChangementStatutEnMasse").attr("style","color: gray;");
   	  	    }else{
   	  	    	$('#flagEnregistrerChangementStatutEnMasse').prop('disabled', false);
   	  	    	supprimeAttribut('#flagEnregistrerChangementStatutEnMasse','style');
   	  	    }	
    
   	  });
   	    
   	  //Désactivation du boutton d'assignation si l'element séléctionné est l'élément initial
	$('#flagEnregistrerCommentaireEnMasse').prop('disabled', true);
	
	$('#commentaireEnMasse').on('keyup',function(){
	
	//Si la valeur est identique on désactive le boutton
	    if($('#commentaireEnMasse').val() == ""){
	    	$('#flagEnregistrerCommentaireEnMasse').prop('disabled', true);
	    	$("#flagEnregistrerCommentaireEnMasse").attr("style","color: gray;");
	    }else{
	    	$('#flagEnregistrerCommentaireEnMasse').prop('disabled', false);
	    	supprimeAttribut('#flagEnregistrerCommentaireEnMasse','style');
	  	    }	
	
	  });
    
});


function RecupererSelectionsListe(elementId){
	var selected=[];
	var i=0;
	 $('#'+ elementId +' :selected').each(function(){
	     selected[i]=$(this).text();
	     i=i+1;
	    });
	 
	 return selected;
}


function AfficherCriteres(array){
	 var e = "";   
	   for (var i=0; i<array.length; i++)
	   {
	     e +=" " + array[i] + "<br/>";
	   }
	return e;
}

function AfficheCriteresRecherchePopup(){
	//afficher bloc A' dans la popup
	if($(".ligneTableauResultat input[type='checkbox']:checked").length != 0){
		$('#blocA').css("visibility","hidden");
		$('#blocA').css("display","none");
		$('#blocAPrime').css("visibility","visible");
		$('#blocAPrime').css("display","inline-block");
				
	}
	// affiche bloc A dans la popup
	else{
		$('#blocA').css("visibility","visible");
		$('#blocA').css("display","inline-block");
		$('#blocAPrime').css("visibility","hidden");
		$('#blocAPrime').css("display","none");
	}
	
	

	//afficher les critères de recherche 
	   $('#statutsSelected')[0].innerHTML = RecupererSelectionsListe('statuts').length ==0 ? " Aucune sélection " : AfficherCriteres( RecupererSelectionsListe('statuts') );
	   $('#groupesGestionSelected')[0].innerHTML =RecupererSelectionsListe('groupesGestion').length ==0 ? " Aucune sélection " : AfficherCriteres(RecupererSelectionsListe('groupesGestion'));
	   $('#numClientSelected')[0].innerHTML = $('#numClient').val();
	   $('#messagesSelected')[0].innerHTML = RecupererSelectionsListe('messages').length ==0 ? " Aucune sélection " :AfficherCriteres(RecupererSelectionsListe('messages'));
	   $('#famillesSelected')[0].innerHTML = RecupererSelectionsListe('familles').length ==0 ? " Aucune sélection " :AfficherCriteres(RecupererSelectionsListe('familles'));
	   $('#sirenSelected')[0].innerHTML = $('#siren').val();
	   $('#idControlesSelected')[0].innerHTML = RecupererSelectionsListe('idControles').length ==0 ? " Aucune sélection " :AfficherCriteres(RecupererSelectionsListe('idControles'));
	   $('#trimestresSelected')[0].innerHTML = RecupererSelectionsListe('trimestres').length ==0 ? " Aucune sélection " :AfficherCriteres(RecupererSelectionsListe('trimestres'));
	   $('#nicSelected')[0].innerHTML = $('#nic').val();
	   $('#typesPeriodeSelected')[0].innerHTML = RecupererSelectionsListe('typesPeriode').length ==0 ? " Aucune sélection " :AfficherCriteres(RecupererSelectionsListe('typesPeriode'));
	   $('#cptEncSelected')[0].innerHTML = $('#cptEnc').val();
	   $('#raisonSocialeSelected')[0].innerHTML = $('#raisonSociale').val();
	   $('#numContratSelected')[0].innerHTML = $('#numContrat').val();
	   $('#cptProdSelected')[0].innerHTML = $('#cptProd').val();
	   $('#vipSelected')[0].innerHTML =RecupererSelectionsListe('vip').length ==0 ? " Aucune sélection " : AfficherCriteres(RecupererSelectionsListe('vip'));
	   $('#versementsSelected')[0].innerHTML =RecupererSelectionsListe('versements').length ==0 ? " Aucune sélection " : AfficherCriteres(RecupererSelectionsListe('versements'));
	   $('#affecteASelected')[0].innerHTML = RecupererSelectionsListe('affecteA').length ==0 ? " Aucune sélection " :AfficherCriteres(RecupererSelectionsListe('affecteA'));
	   $('#aTraiterParSelected')[0].innerHTML = RecupererSelectionsListe('aTraiterPar').length ==0 ? " Aucune sélection " :AfficherCriteres(RecupererSelectionsListe('aTraiterPar'));
	   $('#traiteParSelected')[0].innerHTML = RecupererSelectionsListe('traitePar').length ==0 ? " Aucune sélection " :AfficherCriteres(RecupererSelectionsListe('traitePar'));

	   // Bloc B
	   if($(".ligneTableauResultat input[type='checkbox']:checked").length !=0 ){
		   // cbx présentes
		   $('#blocB')[0].innerHTML = ' Nombre de périodes impactées: <span id="blocBval">' + $(".ligneTableauResultat input[type='checkbox']:checked").length+"</span>";

	   }else {
		   // nb périodes impactées par critères de recherche
		   $('#blocB')[0].innerHTML = ' Nombre de périodes impactées: <span id="blocBval">' + $("#nbPeriodesFiltrees").val()+"</span>";
	   }
	   
}



// Reset selection liste selectize
function cleanListeSelectize(idliste){
    $(idliste).selectize()[0].selectize.clear();
}



//Call onsubmit form - mise en attente retour entreprise
function actionMiseAttenteBeforeSubmit() {
	
	// récupération des paramètres de recherche
	$('#parametresRechercheMiseEnAttente').val($("#parametresRecherche").val());
	
	// tableau avec les id des périodes selectionnées
	var listeids = [];
    $('.ligneTableauResultat input[type=checkbox]:checked').each(function() {
		listeids.push($(this).attr('id'));
	});
	
	// on ajoute les id selectionnés dans le champ du formulaire
	$('.idsPeriodes').val(listeids.join(','));
	
	// nombre de périodes impactées
	$(".nbPeriodes").val($("#blocBval").text().replace(/\s/g,''));
	
	var action = $("#miseEnAttente:checked").prop("checked") ? "" : " d'enlever";
	
	if(!confirm('Attention, vous demandez'+action+' la mise en attente de retour entreprise des ' + $("#blocBval").text()+' périodes impactées, confirmez-vous votre demande ? ')){
		return false;
	}
	return true;
}

//Call onsubmit form - Affecter A Periodes
function actionAffecterBeforeSubmit() {
	
	// récupération des paramètres de recherche
	$('#parametresRechercheAffecterGestionnaire').val($("#parametresRecherche").val());
	
	// tableau avec les id des périodes selectionnées
	var listeids = [];
    $('.ligneTableauResultat input[type=checkbox]:checked').each(function() {
		listeids.push($(this).attr('id'));
	});
	
	// on ajoute les id selectionnés dans le champ du formulaire
	$('.idsPeriodes').val(listeids.join(','));
	
	// nombre de périodes impactées
	$(".nbPeriodes").val($("#blocBval").text().replace(/\s/g,''));
	
	
	if(!confirm("Attention, vous demandez l’affectation des " + $("#blocBval").text() + " périodes impactées au" 
			+ " gestionnaire " + $('#gestionnaireAffecteAEnMasse :selected').text().trim() + ", confirmez-vous votre demande ?")){
		return false;
	}
	return true;
}

//Call onsubmit form - aTraiter Par sur Periodes
function actionaTraiterParBeforeSubmit() {
	
	// récupération des paramètres de recherche
	$('#parametresRechercheAssignerGestionnaire').val($("#parametresRecherche").val());
	
	// tableau avec les id des périodes selectionnées
	var listeids = [];
    $('.ligneTableauResultat input[type=checkbox]:checked').each(function() {
		listeids.push($(this).attr('id'));
	});
	
	// on ajoute les id selectionnés dans le champ du formulaire
	$('.idsPeriodes').val(listeids.join(','));
	
	// nombre de périodes impactées
	$(".nbPeriodes").val($("#blocBval").text().replace(/\s/g,''));
	
	
	if(!confirm("Attention, vous demandez l’assignation pour traitement des " + $("#blocBval").text() + " périodes impactées au" 
			+ " gestionnaire " + $('#gestionnaireATraiterParEnMasse :selected').text().trim() + ", confirmez-vous votre demande ?")){
		return false;
	}
	return true;
}


var secondCallComm = false;
//Call onsubmit form - commentaire sur Periodes
function actionCommentaireBeforeSubmit(buttonName,buttonValue) {

	if (secondCallComm) {
        return;
    }
	
	// récupération des paramètres de recherche
	$('#parametresRechercheCommentaireEnMasse').val($("#parametresRecherche").val());
	
	// tableau avec les id des périodes selectionnées
	var listeids = [];
    $('.ligneTableauResultat input[type=checkbox]:checked').each(function() {
		listeids.push($(this).attr('id'));
	});
	
	// on ajoute les id selectionnés dans le champ du formulaire
	$('.idsPeriodes').val(listeids.join(','));

	// nombre de périodes impactées
	$(".nbPeriodes").val($("#blocBval").text().replace(/\s/g,''));
	
	
	if(!confirm("Attention, vous demandez l'enregistrement du commentaire saisi pour les " + $("#blocBval").text() + " périodes impactées" 
			+ ", confirmez-vous votre demande ?")){
		return false;
	}
	else{
		secondCallComm = true;
		submitFormWithforceContentType('fCommentaireEnMasse',buttonName,buttonValue);
	}
	return true;
}

var secondCall = false;
// Call onsubmit form - blocage Periodes
function actionBlocageBeforeSubmit() {
	
	if (secondCall) {
        return;
    }

	// récupération des paramètres de recherche
	$('#parametresRechercheBlocage').val($("#parametresRecherche").val());
	
	// tableau avec les id des périodes selectionnées
	var listeids = [];
    $('.ligneTableauResultat input[type=checkbox]:checked').each(function() {
		listeids.push($(this).attr('id'));
	});
	
	// on ajoute les id selectionnés dans le champ du formulaire
	$('.idsPeriodes').val(listeids.join(','));
	
	// nombre de périodes impactées
	$(".nbPeriodes").val($("#blocBval").text().replace(/\s/g,''));
	
	
	// controle statuts periodes
	$.ajax({
        type: 'GET',
        url: window.location.href.split('?')[0]+"/changementEnMasse/blocage/verificationStatus",
        data : {
        	'parametresRecherche' : $("#parametresRecherche").val(),
        	'idsPeriodes' : listeids.join(','),
        	'nbPeriodes' : 	$("#blocBval").text()
        	},
        success: function(data) {

        	var action = $("#bloquerPeriodesEnMasse:checked").prop("checked") ? "blocage" : "déblocage";
	
        	if(confirm("Attention, vous demandez le "+action+" des "+$("#blocBval").text()+" périodes impactées, confirmez-vous votre demande ?")){
        		secondCall = true;
            	$('#fBlocagePeriodes').submit();
        	}

        },
        error: function(data){
        	// controle KO
        	var message = "Les périodes impactées doivent toutes être au statut « En réception » pour pouvoir être bloquées ou débloquées," +
    		" or certaines périodes impactées sont au statut " + data.responseText;
    
        	alert(message);
        }
	});
	
	return false;
}

var secondCallStatut = false;
//Call onsubmit form - changement statut en masse
function actionChangementStatutBeforeSubmit(buttonName,buttonValue) {
	
	if (secondCallStatut) {
        return;
    }
	
	// récupération des paramètres de recherche
	$('#parametresRechercheChangementDeStatutEnMasse').val($("#parametresRecherche").val());
	
	// tableau avec les id des périodes selectionnées
	var listeids = [];
    $('.ligneTableauResultat input[type=checkbox]:checked').each(function() {
		listeids.push($(this).attr('id'));
	});
	
	// on ajoute les id selectionnés dans le champ du formulaire
	$('.idsPeriodes').val(listeids.join(','));
	
	// nombre de périodes impactées
	$(".nbPeriodes").val($("#blocBval").text().replace(/\s/g,''));
	
	// controle statuts periodes
	$.ajax({
        type: 'GET',
        url: window.location.href.split('?')[0]+"/changementStatutEnMasse/verificationStatuts",
        data : {
        	'parametresRecherche' : $("#parametresRecherche").val(),
        	'idsPeriodes' : listeids.join(','),
        	'nbPeriodes' : 	$("#blocBval").text(),
        	'codeLibelleChangementDeStatutEnMasse' : $("#listeStatuts").val()
        	},
        success: function(data) {

        	if(confirm("Attention, vous demandez le changement de statut des "+$("#blocBval").text()+" périodes impactées vers le statut «" +
        			$("#listeStatuts").text().trim() + "», confirmez-vous votre demande ?")){
        		secondCallStatut = true;
        		submitFormWithforceContentType('fChangementStatutEnMasse',buttonName,buttonValue);
        	}

        },
        error: function(data){
        	// controle KO
        	
    
        	alert(data.responseText);
        }
	});
	
	return false;
}

// @param idcomposant l'identifiant du composant html
// @param attribut l'attribut à supprimer
function supprimeAttribut(idcomposant,attribut){
	$(idcomposant).removeAttr(attribut);
}
