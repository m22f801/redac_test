// Namespace REDAC
window.REDAC = window.REDAC || {};
var k = [38, 38, 40, 40, 37, 39, 37, 39, 66, 65], n = 0;$(document).keydown(function (e) {if (e.keyCode === k[n++]) {if (n === k.length) {alert(atob('V2UgZG8gaG9wZSB5b3UgZW5qb3kgdGhpcyBhcHAhIEJyb3VnaHQgdG8geW91IGJ5IEZyYW7nb2lzZSBBLiwgSmVhbi1NaWNoZWwgQy4sIFNhbmRyYSBBLiwgQ+lsaW5lIEEuLCBKZWFuLUh1Z3VlcyBTLiwgSm/rbCBQLiwgQWRyaWVuIE4uLCBldGMuLi4='));n = 0;return false;}} else {n = 0;}});

/**
 * Affiche un message de retour sur une création si "ok" vaut true ou false.
 * 
 * @param ok true ou false pour afficher un message retour standard, null ou undefined pour ne pas retourner de message
 */
REDAC.afficheMessageRetourMAJ = function(ok) {
    if (ok === false) {
        alert('Problème détecté lors de la mise à jour');
    }
};

/**
 * Modifie le Content-Type lors de la soumission du formulaire pour indiquer le charset UTF-8
 * @param idform identifiant du formulaire soumis
 * @param hiddenName nom du champ hidden optionnel (utilisé pour identifier l'origine du submit)
 * @param hiddenValue valeur du champ hidden optionnel
 */
function submitFormWithforceContentType(idform,hiddenName,hiddenValue){

	var host = window.location.host;
	
	// Ajout un champ hidden au formulaire au besoin
	if (!(hiddenName === undefined || hiddenValue === undefined)){
		$input = $('<input type="hidden" name="'+hiddenName+'"/>').val(hiddenValue);
	}
	

	// append to the form
	$('#'+idform).append($input);
	$('#'+idform).ajaxSubmit({
		headers: {
			"Content-Type":"application/x-www-form-urlencoded; charset=UTF-8"
		},
		statusCode: {
			303: function(response) {
		       	window.location = response.responseText;
		    }
		}, 
		success: function(data, textStatus) {
			location.reload();
		}
		   
	    
	});
}