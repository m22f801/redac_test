//Call onsubmit form - changement statut en masse
function actionPassageSansSuiteBeforeSubmit() {
	var messagesControles = [];
	
	// tableau avec les id des périodes selectionnées
	var listeids = [];
    $('.ligneTableauResultat input[type=checkbox]:checked').each(function() {
		listeids.push($(this).attr('id'));
	});
    
	if(listeids.length < 1){
		alert("- Veuillez sélectionner à minima 1 DSN non traitée à passer en sans suite gestion ");
		return false;
	}
		
   // Bloc B
   if($(".ligneTableauResultat input[type='checkbox']:checked").length !=0 ){
	   // cbx présentes
	   document.getElementById("nbAdhesion").value = $(".ligneTableauResultat input[type='checkbox']:checked").length;

   }else {
	   // nb périodes impactées par critères de recherche
	   document.getElementById("nbAdhesion").value = $("#nbPeriodesFiltrees").val();
   }

	// on ajoute les id selectionnés dans le champ du formulaire
	$('.idsAdhesion').val(listeids.join(','));
	// controle statuts periodes
	$.ajax({
        type: 'GET',
        url: window.location.href.split('?')[0]+"/miseAJourSSG/passageSansSuite",
        data : {
        	'parametresRecherche' : $("#parametresRecherche").val(),
        	'idsAdhesion' : listeids.join(','),
        	'nbAdhesion' : 	$("#nbAdhesion").text()
        	},
        success: function(data) {

        	if(confirm("Attention, vous demandez le passage en sans suite gestion des "+ $(".ligneTableauResultat input[type='checkbox']:checked").length +" DSN non traitées suivantes, confirmez-vous votre demande ?")){
        		$('form#fValiderPassageSansSuite').submit();
        	}
        },
        error: function(data){
        	// controle KO    
        	alert(data.responseText);
        }
	});
	
	return false;
}
