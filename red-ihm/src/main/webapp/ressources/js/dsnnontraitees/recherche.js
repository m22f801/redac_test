// Déclaration des dropdowns multiselect
var multiselectDefaultConfig = {
    nonSelectedText: 'Aucune sélection',
    buttonWidth: '170px',
    numberDisplayed: 1,
    maxHeight: 300,
    buttonClass: 'multiselect',
    nSelectedText: ' sélections',
    allSelectedText: 'Sélection complète'
};
$('#statuts').multiselect(multiselectDefaultConfig);
$('#groupesGestion').multiselect(multiselectDefaultConfig);
$('#familles').multiselect(multiselectDefaultConfig);
$('#versements').multiselect(multiselectDefaultConfig);


// Gestion des messages de contrôle dans la liste des résultats
var tooltipsterConfig = {
    animation: 'fade',
    speed: 0, // Pas d'animation
    content: 'Chargement...',
    contentAsHTML: true,
    minWidth: 200,
    position: 'left',
    theme: 'tooltip-theme',
    functionBefore: function(origin, continueTooltip) {
        continueTooltip();
        if (origin.data('ajax') !== 'cached') {
            $.ajax({
                type: 'GET',
                url: $(origin[0]).attr('url-messages'),
                success: function(data) {
                    // update our tooltip content with our returned data and cache it
                    origin.tooltipster('content', data).data('ajax', 'cached');
                }
            });
        }
    }
};


$(document).ready(function() {
		
	// validation num client
	$('input').on("keypress", function(event) {
	var code = event.charCode || event.keyCode;
	
	// codes valides (enter, backspace, left/right arrow, del )
	var valideCode=[8,13,37,39,46];
	
	// en autorisant c/v, le copier/coller est permis.
	// la validation du champ empechera c/v d'être envoyé si saisis
	var flagCC= event.charCode==99 ||  event.charCode==118;
	
    // numbers only
	//if((code < 48 || code > 57) && event.target.id=="numClient" && valideCode.indexOf(code)== -1 && !flagCC ){
	//        event.preventDefault();
	//        return false;
	//}
	
	// gestion enter
	if( ((code == 13) && (validationFunction() == false))) {
      // prevent form submit if validation fails
      event.preventDefault();
      return false;
    }
    
	});
	
	//si la checkbox de sélection globale est cochée alors cocher toutes périodes sinon les décocher
    $('#select_all').on('click',function(){
        if(this.checked){
			$(".ligneTableauResultat input[type='checkbox']").each(function(){
                this.checked = true;
            });
        }else{
             $(".ligneTableauResultat input[type='checkbox']").each(function(){
                this.checked = false;
            });
        }
    });
    
    //si toutes les périodes sont cochées une à une alors cocher la checkbox de sélection globale
    $(".ligneTableauResultat input[type='checkbox']").on('click',function(){
        if($(".ligneTableauResultat input[type='checkbox']:checked").length == $(".resultats input[type='checkbox']").length){
            $('#select_all').prop('checked',true);
        }else{
            $('#select_all').prop('checked',false);
        }
    });
  
});

function validationMois(){
	  /**var reg = /(0[1-9]|1[0-2])[- /.](0[1-2]|1[0-9]|2[1-9]);*/
	  var reg = /^(0[1-9]|1[0-2])\/(19\d\d|20\d\d)$/;
	  var num_cli = $("#moisRattachement").val();
	  if (num_cli.match(reg) || num_cli == "") {
		return true
	  }
	  else {
	    return false
	  }
}

// Call onsubmit form
function ValidationFormulaireDsnNonTraitees() {
	// on verifie pour l'instant que le num client
	var validation = validationMois();
	if(validation == false ){
		// prevent form submit if validation fails
		  alert("- La date saisie doit être au format MM/AAAA -");
		  event.preventDefault();
	      return false;
	}else{
		  return true;
	}
}