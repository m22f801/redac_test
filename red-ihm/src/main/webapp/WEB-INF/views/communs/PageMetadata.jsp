<meta http-equiv="X-Ua-Compatible" content="IE=edge;chrome=1" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>REDAC</title>
<meta name="author" content="CGI" />
<link rel="shortcut icon" type="image/x-icon" href="<c:url value="/images/favicon.ico"/>" />
   
<!-- CSS -->
<link rel="stylesheet" href="<c:url value="/ressources/libs/tooltipster-master/css/tooltipster.css?v=${version}" />"></link>
<link rel="stylesheet" href="<c:url value="/ressources/libs/bootstrap/css/bootstrap.min.css?v=${version}" />"></link>
<link rel="stylesheet" href="<c:url value="/ressources/libs/bootstrap-multiselect/dist/css/bootstrap-multiselect.css?v=${version}" />"></link>
<link rel="stylesheet" href="<c:url value="/ressources/css/commun.css?v=${version}" />"></link>
<link rel="stylesheet" href="<c:url value="/ressources/libs/selectize.js-0.12.4/dist/css/selectize.css?v=${version}" />"></link>
<!-- JAVASCRIPT -->
<script type="text/javascript" src="<c:url value="/ressources/libs/jquery-1.11.3.min.js?v=${version}" />"></script>
<script type="text/javascript" src="<c:url value="/ressources/libs/tooltipster-master/js/jquery.tooltipster.min.js?v=${version}" />"></script>
<script type="text/javascript" src="<c:url value="/ressources/libs/bootstrap/js/bootstrap.min.js?v=${version}" />"></script>
<script type="text/javascript" src="<c:url value="/ressources/libs/bootstrap-multiselect/dist/js/bootstrap-multiselect.js?v=${version}" />"></script>
<script type="text/javascript" src="<c:url value="/ressources/libs/jquery.form.min.js?v=${version}" />"></script>
<script type="text/javascript" src="<c:url value="/ressources/js/commun.js?v=${version}" />"></script>
<script type="text/javascript" src="<c:url value="/ressources/libs/selectize.js-0.12.4/dist/js/standalone/selectize.min.js?v=${version}" />"></script>