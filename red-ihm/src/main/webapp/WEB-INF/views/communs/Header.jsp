<div class="header">
	<div class="wrapperTitre">
		<div class="logoApplication">
			<img src="<c:url value="/images/logo-miniature.jpg" />" class="logoApplication"/>
		</div>
		<div class="titre">
			<span>REDAC-CDE</span> <span class="sousTitre">Gestionnaire du R�f�rentiel et des Flux DSN</span>
		</div>
	</div>
	<div class="wrapperFilAriane">
		<div class="filAriane">
			<ol>
				<c:forEach items="${filAriane.fil}" var="page" varStatus="loop">
					<li><a href="<c:url value="${page.lien}" />">${page.nom}</a></li> 
				</c:forEach>
			</ol>
		</div>
	</div>
	<div class="wrapperOutils">
		<div class="infoConnexion">
			<span class="nomUtilisateur">${utilisateur.nomComplet}</span>
			<form class="formulaireDeconnexion" action="<c:url value="/ihm/deconnexion" />" method="post" onsubmit="if (document.execCommand('ClearAuthenticationCache')){location.reload(); return false;} else return true;">
				<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
				<button style="visibility: hidden;" type="submit">D�connexion</button>
			</form>
		</div>
		<div class="outils">
			<ul>
				<li class="infoDigest">Grpe gest: ${digest.groupeGestion} - Famille: ${digest.famille}</li>
				<li><fmt:formatNumber type="number" groupingUsed="true" value="${digest.nbContratsEligiblesDSN}" />  contrats �ligibles DSN</li>
				<li><fmt:formatNumber type="number" groupingUsed="true" value="${digest.nbPeriodesEnCoursReception}" /> p�riodes en cours de r�ception</li>
				<li><fmt:formatNumber type="number" groupingUsed="true" value="${digest.nbPeriodesATraiter}" /> p�riodes � traiter</li>
			</ul>
		</div>
	</div>
</div>
