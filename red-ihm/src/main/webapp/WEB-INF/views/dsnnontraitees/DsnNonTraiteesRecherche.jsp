<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!DOCTYPE html>
<html>
<head>
<%@include file="../communs/PageMetadata.jsp"%>
<link rel="stylesheet"
	href="<c:url value="/ressources/css/dsnnontraitees.css" />"></link>
<script language="javascript" type="text/javascript">
	function SetFocus(InputID) {
		document.getElementById(InputID).focus();
		setTimeout(function() {
			document.getElementById(InputID).focus();
		}, 10);
		
		if(InputID==='statuts'){
			// set focus pour IE10 sur la dropdown "status"
			$('button[tabindex=1]')[0].focus();
		}

	}
</script>

</head>
<body on="SetFocus('statuts')">
	<div class="appWrapper">
		<%@include file="../communs/Header.jsp"%>
		<div class="bodyWrapper">
			<%@include file="../communs/Menu.jsp"%>
			<div class="content declarations recherche">
				<div class="titreContenu">DSN non traitées</div>

				<!-- Formulaire de recherche -->
				<div class="rechercheWrapper">
					<div class="titreFormulaire">Caractéristiques des DSN
						présentées :</div>
					<form class="rechercheFormulaire"
						action="<c:url value="/ihm/dsnnontraitees" />"
						onsubmit="ValidationFormulaireDsnNonTraitees()" method="get">
						<table>
							<tr>
								<td><label for="statuts">Etat :</label> <select
									id="statuts" name="statuts" multiple="multiple" tabindex="1">
										<c:forEach var="item" items="${reference.listeStatuts}">
											<option value="${item.code}"
												${contexte.reinitialisation eq true && contexte.critereStatutsParDefaut.contains(item.code) || contexte.criteres.statutsAsList.contains(item.code) ? 'selected=\'selected\'' : ''}>${item.libelleCourt}</option>
										</c:forEach>
								</select></td>
								<td><label for="groupesGestion">Grpe gestion :</label> <select
									id="groupesGestion" name="groupesGestion" multiple="multiple"
									tabindex="2">
										<c:forEach var="item" items="${reference.listeGroupesGestion}">
											<option value="${item.code}"
												${contexte.criteres.groupesGestionAsList.contains(item.code) || empty contexte.criteres.groupesGestionAsList && contexte.reinitialisation eq true && utilisateur.groupeGestion eq item.code ? 'selected=\'selected\'' : ''}>${item.libelleCourt}</option>
										</c:forEach>
								</select></td>
								<td><label for="sirenEntreprise">SIREN :</label> <input id="sirenEntreprise"
									name="sirenEntreprise" maxlength="9"
									value="<c:out value="${contexte.criteres.sirenEntreprise}"/>"
									tabindex="3" /></td>
							</tr>
							<tr>
								<td><label for="numContrat">Contrat :</label> <input
									type="text" id="numContrat" name="numContrat" maxlength="15"
									value="${contexte.criteres.numContrat}" tabindex="4" /></td>
								
								<td><label for="familles">Famille :</label> <select
									id="familles" name="familles" multiple="multiple" tabindex="5">
										<c:forEach var="item" items="${reference.listeFamilles}">
											<option value="${item.code}"
												${contexte.criteres.famillesAsList.contains(item.code) ? 'selected=\'selected\'' : ''}>${item.libelleCourt}</option>
										</c:forEach>
								</select></td>	
								<td><label for="nicEtablissement">NIC :</label> <input id="nicEtablissement"
									name="nicEtablissement" maxlength="5"
									value="<c:out value="${contexte.criteres.nicEtablissement}"/>" tabindex="6" />
								</td>
							</tr>
							<tr>
								<td><label for="moisRattachement">Mois rattachement :</label> 
								<input
									id="moisRattachement" name="moisRattachement" maxlength="7" placeholder="_ _/_ _ _ _"
									value="<c:out value="${contexte.criteres.moisRattachement}"/>" tabindex="7" />																			
								</td>
								<td><label for="cptEnc">Cpt enc :</label> <input
									id="cptEnc" name="cptEnc" maxlength="6"
									value="<c:out value="${contexte.criteres.cptEnc}"/>"
									tabindex="8" /></td>
								<td><label for="raisonSociale">Raison soc. :</label> <input
									id="raisonSociale" name="raisonSociale" maxlength="38"
									value="<c:out value="${contexte.criteres.raisonSociale}"/>"
									tabindex="9" /></td>
							</tr>
							<tr>
								<td><label for="versements">Versement :</label> <select
									id="versements" name="versements" multiple="multiple"
									tabindex="10">
										<option value="false"
											${contexte.criteres.versementsAsList.contains(false) ? 'selected=\'selected\'' : ''}>Aucun</option>
										<option value="true"
											${contexte.criteres.versementsAsList.contains(true) ? 'selected=\'selected\'' : ''}>Au
											moins 1</option>
								</select></td>
								<td><label for="cptProd">Cpt prod :</label> <input
									id="cptProd" name="cptProd" maxlength="6"
									value="<c:out value="${contexte.criteres.cptProd}"/>"
									tabindex="11" /></td>
							</tr>
						</table>
						<input type="hidden" id="pageNumero" name="pageNumero" value="1" />
						<input type="hidden" id="pageTaille" name="pageTaille" value="50">
						<input type="hidden" id="triChamp" name="triChamp" value="spec">
						<input type="hidden" id="triAsc" name="triChamp" value="true">
						<div class="boutonsFormulaire">
							<button type="submit" class="boutonRecherche" tabindex="16">
								<div class="boutonIcone"></div>
								<span>Rechercher</span>
							</button>
						</div>
					</form>
				</div>

				<!-- Tableau de résultats de recherche -->
				<div class="informations">
					<span>${informations}</span>
				</div>
				<div class="resultatsWrapper">
					<table class="resultats">
						<thead>
							<tr>
								<td> <input type="checkbox" id="select_all"   /> </td>
								<c:if test="${contexte.criteres.triChamp != 'groupeGestion'}">
									<td><a
										href="?${fn:replace(contexte.urlQueryString, 'triChamp='.concat(contexte.criteres.triChamp), 'triChamp=groupeGestion')}"
										title="Trier par groupe de gestion">G.</a></td>
								</c:if>
								<c:if test="${contexte.criteres.triChamp == 'groupeGestion'}">
									<td><span>G.</span> <c:if
											test="${contexte.criteres.triAsc}">
											<a
												href="?${fn:replace(contexte.urlQueryString, 'triAsc=true', 'triAsc=false')}"
												class="triAsc" title="Trier par ordre décroissant"></a>
										</c:if> <c:if test="${!contexte.criteres.triAsc}">
											<a
												href="?${fn:replace(contexte.urlQueryString, 'triAsc=false', 'triAsc=true')}"
												class="triDesc" title="Trier par ordre croissant"></a>
										</c:if></td>
								</c:if>
								<c:if test="${contexte.criteres.triChamp != 'libelleFamille'}">
									<td><a
										href="?${fn:replace(contexte.urlQueryString, 'triChamp='.concat(contexte.criteres.triChamp), 'triChamp=libelleFamille')}"
										title="Trier par famille de produit">Fam.</a></td>
								</c:if>
								<c:if test="${contexte.criteres.triChamp == 'libelleFamille'}">
									<td><span>Fam.</span> <c:if
											test="${contexte.criteres.triAsc}">
											<a
												href="?${fn:replace(contexte.urlQueryString, 'triAsc=true', 'triAsc=false')}"
												class="triAsc" title="Trier par ordre décroissant"></a>
										</c:if> <c:if test="${!contexte.criteres.triAsc}">
											<a
												href="?${fn:replace(contexte.urlQueryString, 'triAsc=false', 'triAsc=true')}"
												class="triDesc" title="Trier par ordre croissant"></a>
										</c:if></td>
								</c:if>
								<c:if test="${contexte.criteres.triChamp != 'refContrat'}">
									<td><a
										href="?${fn:replace(contexte.urlQueryString, 'triChamp='.concat(contexte.criteres.triChamp), 'triChamp=refContrat')}"
										title="Trier par numéro de contrat">Contrat</a></td>
								</c:if>
								<c:if test="${contexte.criteres.triChamp == 'refContrat'}">
									<td><span>Contrat</span> <c:if
											test="${contexte.criteres.triAsc}">
											<a
												href="?${fn:replace(contexte.urlQueryString, 'triAsc=true', 'triAsc=false')}"
												class="triAsc" title="Trier par ordre décroissant"></a>
										</c:if> <c:if test="${!contexte.criteres.triAsc}">
											<a
												href="?${fn:replace(contexte.urlQueryString, 'triAsc=false', 'triAsc=true')}"
												class="triDesc" title="Trier par ordre croissant"></a>
										</c:if></td>
								</c:if>
								<c:if test="${contexte.criteres.triChamp != 'moisRattachement'}">
									<td><a
										href="?${fn:replace(contexte.urlQueryString, 'triChamp='.concat(contexte.criteres.triChamp), 'triChamp=moisRattachement')}"
										title="Trier par mois de rattachement">Mois rattachement</a></td>
								</c:if>
								<c:if test="${contexte.criteres.triChamp == 'moisRattachement'}">
									<td><span>Mois rattachement</span> <c:if
											test="${contexte.criteres.triAsc}">
											<a
												href="?${fn:replace(contexte.urlQueryString, 'triAsc=true', 'triAsc=false')}"
												class="triAsc" title="Trier par ordre décroissant"></a>
										</c:if> <c:if test="${!contexte.criteres.triAsc}">
											<a
												href="?${fn:replace(contexte.urlQueryString, 'triAsc=false', 'triAsc=true')}"
												class="triDesc" title="Trier par ordre croissant"></a>
										</c:if></td>
								</c:if>
								
								<c:if test="${contexte.criteres.triChamp != 'sirenEntreprise'}">
									<td><a
										href="?${fn:replace(contexte.urlQueryString, 'triChamp='.concat(contexte.criteres.triChamp), 'triChamp=sirenEntreprise')}"
										title="Trier par SIREN">SIREN</a></td>
								</c:if>
								<c:if test="${contexte.criteres.triChamp == 'sirenEntreprise'}">
									<td><span>SIREN</span> <c:if
											test="${contexte.criteres.triAsc}">
											<a
												href="?${fn:replace(contexte.urlQueryString, 'triAsc=true', 'triAsc=false')}"
												class="triAsc" title="Trier par ordre décroissant"></a>
										</c:if> <c:if test="${!contexte.criteres.triAsc}">
											<a
												href="?${fn:replace(contexte.urlQueryString, 'triAsc=false', 'triAsc=true')}"
												class="triDesc" title="Trier par ordre croissant"></a>
										</c:if></td>
								</c:if>
								
								<c:if test="${contexte.criteres.triChamp != 'nicEtablissement'}">
									<td><a
										href="?${fn:replace(contexte.urlQueryString, 'triChamp='.concat(contexte.criteres.triChamp), 'triChamp=nicEtablissement')}"
										title="Trier par NIC">NIC</a></td>
								</c:if>
								<c:if test="${contexte.criteres.triChamp == 'nicEtablissement'}">
									<td><span>NIC</span> <c:if
											test="${contexte.criteres.triAsc}">
											<a
												href="?${fn:replace(contexte.urlQueryString, 'triAsc=true', 'triAsc=false')}"
												class="triAsc" title="Trier par ordre décroissant"></a>
										</c:if> <c:if test="${!contexte.criteres.triAsc}">
											<a
												href="?${fn:replace(contexte.urlQueryString, 'triAsc=false', 'triAsc=true')}"
												class="triDesc" title="Trier par ordre croissant"></a>
										</c:if></td>
								</c:if>
								
								<c:if test="${contexte.criteres.triChamp != 'raisonSociale'}">
									<td><a
										href="?${fn:replace(contexte.urlQueryString, 'triChamp='.concat(contexte.criteres.triChamp), 'triChamp=raisonSociale')}"
										title="Trier par raison social">Raison Sociale</a></td>
								</c:if>
								<c:if test="${contexte.criteres.triChamp == 'raisonSociale'}">
									<td><span>Raison Social</span> <c:if
											test="${contexte.criteres.triAsc}">
											<a
												href="?${fn:replace(contexte.urlQueryString, 'triAsc=true', 'triAsc=false')}"
												class="triAsc" title="Trier par ordre décroissant"></a>
										</c:if> <c:if test="${!contexte.criteres.triAsc}">
											<a
												href="?${fn:replace(contexte.urlQueryString, 'triAsc=false', 'triAsc=true')}"
												class="triDesc" title="Trier par ordre croissant"></a>
										</c:if></td>
								</c:if>
								
								<c:if test="${contexte.criteres.triChamp != 'dateCreation'}">
									<td><a
										href="?${fn:replace(contexte.urlQueryString, 'triChamp='.concat(contexte.criteres.triChamp), 'triChamp=dateCreation')}"
										title="Trier par date de création">Date Création</a></td>
								</c:if>
								<c:if test="${contexte.criteres.triChamp == 'dateCreation'}">
									<td><span>Date Création</span> <c:if
											test="${contexte.criteres.triAsc}">
											<a
												href="?${fn:replace(contexte.urlQueryString, 'triAsc=true', 'triAsc=false')}"
												class="triAsc" title="Trier par ordre décroissant"></a>
										</c:if> <c:if test="${!contexte.criteres.triAsc}">
											<a
												href="?${fn:replace(contexte.urlQueryString, 'triAsc=false', 'triAsc=true')}"
												class="triDesc" title="Trier par ordre croissant"></a>
										</c:if></td>
								</c:if>
								
								<c:if test="${contexte.criteres.triChamp != 'mtCotisation'}">
									<td><a
										href="?${fn:replace(contexte.urlQueryString, 'triChamp='.concat(contexte.criteres.triChamp), 'triChamp=mtCotisation')}"
										title="Trier par montant de cotisation">Mtt cotis.</a></td>
								</c:if>
								<c:if test="${contexte.criteres.triChamp == 'mtCotisation'}">
									<td><span>Mtt cotis.</span> <c:if
											test="${contexte.criteres.triAsc}">
											<a
												href="?${fn:replace(contexte.urlQueryString, 'triAsc=true', 'triAsc=false')}"
												class="triAsc" title="Trier par ordre décroissant"></a>
										</c:if> <c:if test="${!contexte.criteres.triAsc}">
											<a
												href="?${fn:replace(contexte.urlQueryString, 'triAsc=false', 'triAsc=true')}"
												class="triDesc" title="Trier par ordre croissant"></a>
										</c:if></td>
								</c:if>
										
								<c:if test="${contexte.criteres.triChamp != 'libelleEtat'}">
									<td><a
										href="?${fn:replace(contexte.urlQueryString, 'triChamp='.concat(contexte.criteres.triChamp), 'triChamp=libelleEtat')}"
										title="Trier par état">Etat</a></td>
								</c:if>
								<c:if test="${contexte.criteres.triChamp == 'libelleEtat'}">
									<td><span>Etat</span> <c:if
											test="${contexte.criteres.triAsc}">
											<a
												href="?${fn:replace(contexte.urlQueryString, 'triAsc=true', 'triAsc=false')}"
												class="triAsc" title="Trier par ordre décroissant"></a>
										</c:if> <c:if test="${!contexte.criteres.triAsc}">
											<a
												href="?${fn:replace(contexte.urlQueryString, 'triAsc=false', 'triAsc=true')}"
												class="triDesc" title="Trier par ordre croissant"></a>
										</c:if></td>
								</c:if>
						</tr>
						</thead>
						<tbody class="ligneTableauResultat">
							<c:forEach items="${resultats}" var="dsnnontraitees">
								<tr>
									<td><input type="checkbox" name="selectionPeriode_${dsnnontraitees.idAdhesionEtablissementMois}" id="${dsnnontraitees.idAdhesionEtablissementMois}"  /> </td>
									<td id="groupeGestion_${dsnnontraitees.idAdhesionEtablissementMois}"><span>${dsnnontraitees.nmgrpges}</span></td>
									<td id="famille_${dsnnontraitees.idAdhesionEtablissementMois}"><span>${dsnnontraitees.famille}</span></td>
									<td id="refContrat_${dsnnontraitees.idAdhesionEtablissementMois}"><span>${dsnnontraitees.referenceContrat}</span></td>
									<td id="moisRattachement_${dsnnontraitees.idAdhesionEtablissementMois}"><span>${dsnnontraitees.moisRattachement}</span></td>
									<td id="siren_${dsnnontraitees.idAdhesionEtablissementMois}"><span>${dsnnontraitees.sirenEntreprise}</span></td>
									<td id="nicEtablissement_${dsnnontraitees.idAdhesionEtablissementMois}"><span>${dsnnontraitees.nicEtablissement}</span></td>
									<td id="raisonSociale_${dsnnontraitees.idAdhesionEtablissementMois}">${dsnnontraitees.raisonSocialeEntreprise}</td>
									<td id="dateCreation_${dsnnontraitees.idAdhesionEtablissementMois}">${dsnnontraitees.dateCreation}</td>
									<td id="mtCotis_${dsnnontraitees.idAdhesionEtablissementMois}" align = "right">
										<c:choose>
											<c:when test="${dsnnontraitees.montantCotisation eq value_dsn_neant}">
												DSN Néant
											</c:when>
											<c:otherwise>
												<fmt:formatNumber type="currency"  maxFractionDigits="2" currencySymbol="€" value="${dsnnontraitees.montantCotisation}" />
										   </c:otherwise>
										</c:choose>
									</td>
									<td id="etat_${dsnnontraitees.idAdhesionEtablissementMois}">${dsnnontraitees.etat}</td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
				<div class="NbPeriodesFiltrees">
					<span>Nombre de DSN filtrées: ${nbPeriodesFiltrees}</span>
					<input type="hidden" id="nbPeriodesFiltrees" value="${nbPeriodesFiltrees}">
				</div>
				<div class="outilsPagination">
					<ol>
						<c:if test="${resultatsMeta.pageNumero > 1}">
							<li class="lienPagination"><a
								href="?${fn:replace(contexte.urlQueryString, 'pageNumero='.concat(resultatsMeta.pageNumero), 'pageNumero='.concat(resultatsMeta.pageNumero - 1))}">Préc.</a></li>
						</c:if>
						<c:if test="${resultatsMeta.pageNumero == 1}">
							<li class="lienPagination pageActuelle">1</li>
						</c:if>
						<c:if test="${resultatsMeta.pageNumero != 1}">
							<li class="lienPagination"><a
								href="?${fn:replace(contexte.urlQueryString, 'pageNumero='.concat(resultatsMeta.pageNumero), 'pageNumero=1')}">1</a></li>
						</c:if>
						<c:if test="${resultatsMeta.maxPage > 1}">
							<c:if test="${resultatsMeta.pageNumero == 2}">
								<li class="lienPagination pageActuelle">2</li>
							</c:if>
							<c:if test="${resultatsMeta.pageNumero != 2}">
								<li class="lienPagination"><a
									href="?${fn:replace(contexte.urlQueryString, 'pageNumero='.concat(resultatsMeta.pageNumero), 'pageNumero=2')}">2</a></li>
							</c:if>
						</c:if>
						<c:if test="${resultatsMeta.maxPage > 2}">
							<c:if test="${resultatsMeta.pageNumero == 3}">
								<li class="lienPagination pageActuelle">3</li>
								<c:if test="${resultatsMeta.maxPage > 4}">
									<li class="lienPagination"><a
										href="?${fn:replace(contexte.urlQueryString, 'pageNumero='.concat(resultatsMeta.pageNumero), 'pageNumero=4')}">4</a></li>
								</c:if>
							</c:if>
							<c:if test="${resultatsMeta.pageNumero != 3}">
								<li class="lienPagination"><a
									href="?${fn:replace(contexte.urlQueryString, 'pageNumero='.concat(resultatsMeta.pageNumero), 'pageNumero=3')}">3</a></li>
							</c:if>
						</c:if>
						<c:if test="${resultatsMeta.maxPage > 3}">
							<c:if
								test="${resultatsMeta.pageNumero < resultatsMeta.maxPage - 2}">
								<c:if test="${resultatsMeta.pageNumero > 3}">
									<c:if test="${resultatsMeta.pageNumero > 5}">
										<li>...</li>
									</c:if>
									<c:if test="${resultatsMeta.pageNumero > 4}">
										<li class="lienPagination"><a
											href="?${fn:replace(contexte.urlQueryString, 'pageNumero='.concat(resultatsMeta.pageNumero), 'pageNumero='.concat(resultatsMeta.pageNumero - 1))}">${resultatsMeta.pageNumero - 1}</a></li>
									</c:if>
									<li class="lienPagination pageActuelle">${resultatsMeta.pageNumero}</li>
									<li class="lienPagination"><a
										href="?${fn:replace(contexte.urlQueryString, 'pageNumero='.concat(resultatsMeta.pageNumero), 'pageNumero='.concat(resultatsMeta.pageNumero + 1))}">${resultatsMeta.pageNumero + 1}</a></li>
								</c:if>
								<li>...</li>
								<li class="lienPagination"><a
									href="?${fn:replace(contexte.urlQueryString, 'pageNumero='.concat(resultatsMeta.pageNumero), 'pageNumero='.concat(resultatsMeta.maxPage))}">${resultatsMeta.maxPage}</a></li>
							</c:if>
							<c:if
								test="${resultatsMeta.pageNumero >= resultatsMeta.maxPage - 2}">
								<c:if
									test="${resultatsMeta.pageNumero > 6 || resultatsMeta.pageNumero == 6 && resultatsMeta.maxPage > 6 }">
									<li>...</li>
								</c:if>
								<c:if test="${resultatsMeta.maxPage == 5}">
									<c:if test="${resultatsMeta.pageNumero == 4}">
										<li class="lienPagination pageActuelle">4</li>
									</c:if>
									<c:if test="${resultatsMeta.pageNumero == 5}">
										<li class="lienPagination"><a
											href="?${fn:replace(contexte.urlQueryString, 'pageNumero='.concat(resultatsMeta.pageNumero), 'pageNumero=4')}">4</a></li>
									</c:if>
								</c:if>
								<c:if test="${resultatsMeta.maxPage > 5}">
									<c:if
										test="${resultatsMeta.pageNumero == resultatsMeta.maxPage - 2}">
										<c:if test="${resultatsMeta.pageNumero > 4}">
											<li class="lienPagination"><a
												href="?${fn:replace(contexte.urlQueryString, 'pageNumero='.concat(resultatsMeta.pageNumero), 'pageNumero='.concat(resultatsMeta.maxPage - 3))}">${resultatsMeta.maxPage - 3}</a></li>
										</c:if>
										<li class="lienPagination pageActuelle">${resultatsMeta.maxPage - 2}</li>
									</c:if>
									<c:if
										test="${resultatsMeta.pageNumero != resultatsMeta.maxPage - 2}">
										<li class="lienPagination"><a
											href="?${fn:replace(contexte.urlQueryString, 'pageNumero='.concat(resultatsMeta.pageNumero), 'pageNumero='.concat(resultatsMeta.maxPage - 2))}">${resultatsMeta.maxPage - 2}</a></li>
									</c:if>
									<c:if
										test="${resultatsMeta.pageNumero == resultatsMeta.maxPage - 1}">
										<li class="lienPagination pageActuelle">${resultatsMeta.maxPage - 1}</li>
									</c:if>
									<c:if
										test="${resultatsMeta.pageNumero != resultatsMeta.maxPage - 1}">
										<li class="lienPagination"><a
											href="?${fn:replace(contexte.urlQueryString, 'pageNumero='.concat(resultatsMeta.pageNumero), 'pageNumero='.concat(resultatsMeta.maxPage - 1))}">${resultatsMeta.maxPage - 1}</a></li>
									</c:if>
								</c:if>
								<c:if
									test="${resultatsMeta.pageNumero == resultatsMeta.maxPage}">
									<li class="lienPagination pageActuelle">${resultatsMeta.maxPage}</li>
								</c:if>
								<c:if
									test="${resultatsMeta.pageNumero != resultatsMeta.maxPage}">
									<li class="lienPagination"><a
										href="?${fn:replace(contexte.urlQueryString, 'pageNumero='.concat(resultatsMeta.pageNumero), 'pageNumero='.concat(resultatsMeta.maxPage))}">${resultatsMeta.maxPage}</a></li>
								</c:if>
							</c:if>
						</c:if>
						<c:if test="${resultatsMeta.pageNumero < resultatsMeta.maxPage}">
							<li class="lienPagination"><a
								href="?${fn:replace(contexte.urlQueryString, 'pageNumero='.concat(resultatsMeta.pageNumero), 'pageNumero='.concat(resultatsMeta.pageNumero + 1))}">Suiv.</a></li>
						</c:if>
					</ol>
				</div>
				<div>
					<form  id="fValiderPassageSansSuite" action="<c:url value="/ihm/dsnnontraitees/miseAJourSSG" />" method="post"> 
	                   			<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
	                   			<input type="hidden" class="parametresRecherche" id="parametresRechercheDsnNonTraitees" name="parametresRecherche" value="${contexte.urlQueryString}" />
								<input type="hidden" id="nbAdhesion" name="nbAdhesion" value="${nbPeriodesFiltrees}" />
								<input type="hidden" name="idsAdhesion" class="idsAdhesion" id="idsAdhesion"/>
								<div class="boutonFormulaireSansSuite">
									<button ${(contexte.droitGroupe2 && contexte.droitGroupe3 && nbPeriodesFiltrees != '0' ) ? '' : 'disabled=\"disabled\"' } 
									type="button" class="boutonFusion" 
									id="flagEnregistrerChangementStatutEnMasse" onclick="actionPassageSansSuiteBeforeSubmit();">Passer en sans suite gestion
									</button>						
								</div>
					</form>					
				</div>
				<div class="exportExcel">
					<div class="exportExcelIcone"></div>
					<span><a target="_blank"
						href="<c:url value="/ihm/dsnnontraitees/exports/excel?${contexte.urlQueryString}" />">Extraire
							cette liste complète en Excel</a></span>
				</div>
			</div>
		</div>
		<%@include file="../communs/Footer.jsp"%>
	</div>
	
<script type="text/javascript" src="<c:url value="/ressources/libs/jquery.maskedinput-1.3.1.js" />"></script>
<script type="text/javascript" src="<c:url value="/ressources/js/dsnnontraitees/recherche.js?v=${version}" />"></script>
<script type="text/javascript" src="<c:url value="/ressources/js/dsnnontraitees/popup_dsnnontraitees.js?v=${version}" />"></script>

</body>
</html>