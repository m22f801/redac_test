<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <%@include file="communs/PageMetadata.jsp" %>
</head>
<body>
   	<div class="appWrapper">
	   	<%@include file="communs/Header.jsp" %>
	   	<div class="bodyWrapper">
	    	<%@include file="communs/Menu.jsp" %>
	    	<div class="content accueil">
	    	<p>
		    	Bienvenue sur l'application REDAC !<br/>
		    	<br/>
		    	Veuillez sélectionner une page dans le menu de gauche.
	    	</p>
	    	</div>
	   	</div>
	   	<%@include file="communs/Footer.jsp" %>
   	</div>
</body>
</html>