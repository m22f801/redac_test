<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<div class="titreContenu">Déclaration ${declaration.libellePeriode} pour le Contrat	<span>${periode.numeroContrat}</span> ${raisonSociale}</div>
<div class="outilsPeriode">
	<button style="visibility: hidden;" ${idPeriodePrecedente == null ? 'disabled=\'disabled\'' : ''} class="navPeriodePrecedente" type="button" onclick="location.href='<c:url value="/ihm/declarations/${idPeriodePrecedente}" />'">&lt;&lt; Période précédente</button>
	<div class="alertesPeriode">
		<ul>
			<c:forEach items="${declaration.messagesControles}" var="msg">
				<li class="alerte${msg.niveauAlerte == 'SIGNAL' ? 'Avertissement' : 'Bloquante'}">${msg.messageUtilisateur}</li>
			</c:forEach>
		</ul>
	</div>
	<button style="visibility: hidden;" ${idPeriodeSuivante == null ? 'disabled=\'disabled\'' : ''} class="navPeriodeSuivante" type="button" onclick="location.href='<c:url value="/ihm/declarations/${idPeriodeSuivante}" />'">Période suivante &gt;&gt;</button>
</div>
<div class="navDetailsPeriode">
	<ul>
		<c:if test="${param.page == 'synthese'}">
			<li class="selected"><span>Synthèse</span></li>
		</c:if>
		<c:if test="${param.page != 'synthese'}">
			<li><a href="<c:url value="/ihm/declarations/${declaration.idPeriode}/synthese?${contexte.queryString}" />">Synthèse</a></li>
		</c:if>
		
		<c:if test="${param.page == 'dsn'}">
			<li class="selected"><span>DSN(s) de la période</span></li>
		</c:if>
		<c:if test="${param.page != 'dsn'}">
			<li><a href="<c:url value="/ihm/declarations/${declaration.idPeriode}/dsn?${contexte.queryString}" />">DSN(s) de la période</a></li>
		</c:if>
		
		<c:if test="${param.page == 'individus'}">
			<li class="selected"><span>Individus déclarés</span></li>
		</c:if>
		<c:if test="${param.page != 'individus' && utilisateur.droitIndividu}">
			<li><a href="<c:url value="/ihm/declarations/${declaration.idPeriode}/individus?${contexte.queryString}" />">Individus déclarés</a></li>
		</c:if>
		
		<c:if test="${param.page == 'statut'}">
    		<li class="selected"><span>Changement de Statut</span></li>
    	</c:if>
		<c:if test="${param.page != 'statut'}">
			<li><a href="<c:url value="/ihm/declarations/${declaration.idPeriode}/statut?${contexte.queryString}" />">Changement de Statut</a></li>
		</c:if>
	</ul>
</div>