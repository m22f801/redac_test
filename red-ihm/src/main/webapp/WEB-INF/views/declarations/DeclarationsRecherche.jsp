<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!DOCTYPE html>
<html>
<head>
<%@include file="../communs/PageMetadata.jsp"%>
<link rel="stylesheet"
	href="<c:url value="/ressources/css/declarations.css" />"></link>
<script language="javascript" type="text/javascript">
	function SetFocus(InputID) {
		document.getElementById(InputID).focus();
		setTimeout(function() {
			document.getElementById(InputID).focus();
		}, 10);
		
		if(InputID==='statuts'){
			// set focus pour IE10 sur la dropdown "status"
			$('button[tabindex=1]')[0].focus();
		}

	}
</script>

</head>
<body on="SetFocus('statuts')">
	<div class="appWrapper">
		<%@include file="../communs/Header.jsp"%>
		<div class="bodyWrapper">
			<%@include file="../communs/Menu.jsp"%>
			<div class="content declarations recherche">
				<div class="titreContenu">Périodes contractuelles déclarées
					par DSN</div>

				<!-- Formulaire de recherche -->
				<div class="rechercheWrapper">
					<div class="titreFormulaire">Caractéristiques des périodes
						présentées :</div>
					<form class="rechercheFormulaire"
						action="<c:url value="/ihm/declarations" />"
						onsubmit="ValidationFormulaire()" method="get">
						<table>
							<tr>
								<td><label for="statuts">Statut :</label> <select
									id="statuts" name="statuts" multiple="multiple" tabindex="1">
										<c:forEach var="item" items="${reference.listeStatuts}">
											<option value="${item.code}"
												${contexte.reinitialisation eq true && contexte.critereStatutsParDefaut.contains(item.code) || contexte.criteres.statutsAsList.contains(item.code) ? 'selected=\'selected\'' : ''}>${item.libelleCourt}</option>
										</c:forEach>
								</select></td>
								<td><label for="groupesGestion">Grpe gestion :</label> <select
									id="groupesGestion" name="groupesGestion" multiple="multiple"
									tabindex="2">
										<c:forEach var="item" items="${reference.listeGroupesGestion}">
											<option value="${item.code}"
												${contexte.criteres.groupesGestionAsList.contains(item.code) || empty contexte.criteres.groupesGestionAsList && contexte.reinitialisation eq true && utilisateur.groupeGestion eq item.code ? 'selected=\'selected\'' : ''}>${item.libelleCourt}</option>
										</c:forEach>
								</select></td>
								<td><label for="numClient">N° Client :</label> <input
									type="text" id="numClient" maxlength="6" name="numClient"
									value="<c:out value="${contexte.criteres.numClient}"/>"
									tabindex="3" /></td>
							</tr>
							<tr>
								<td><label for="messages">Message :</label> <select
									id="messages" name="messages" multiple="multiple" tabindex="4">
										<option value="AUCUN"
											${contexte.criteres.messagesAsList.contains('AUCUN')  ? 'selected=\'selected\'' : ''}>sans
											message</option>
										<option value="SIGNAL"
											${contexte.criteres.messagesAsList.contains('SIGNAL') ? 'selected=\'selected\'' : ''}>avec
											signalement</option>
										<option value="REJET"
											${contexte.criteres.messagesAsList.contains('REJET') ? 'selected=\'selected\'' : ''}>avec
											rejet</option>
								</select></td>
								<td><label for="familles">Famille :</label> <select
									id="familles" name="familles" multiple="multiple" tabindex="5">
										<c:forEach var="item" items="${reference.listeFamilles}">
											<option value="${item.code}"
												${contexte.criteres.famillesAsList.contains(item.code) ? 'selected=\'selected\'' : ''}>${item.libelleCourt}</option>
										</c:forEach>
								</select></td>
								<td><label for="siren">SIREN :</label> <input id="siren"
									name="siren" maxlength="9"
									value="<c:out value="${contexte.criteres.siren}"/>"
									tabindex="6" /></td>
							</tr>
							<tr>
								<td><label for="idControles">Lib. Message :</label> 
								<select	id="idControles" name="idControles" multiple="multiple" 
									tabindex="7"> 
										<c:forEach var="item" items="${reference.listeMessagesControle}">
											<option value="${item.identifiantControle}"
												${contexte.criteres.idControlesAsList.contains(item.identifiantControle) ? 'selected=\'selected\'' : ''}>${item.messageAlerte.replace("$1","XX")}</option>
										</c:forEach>
										
								</select></td>
								<td><label for="trimestres">Période :</label> <select
									id="trimestres" name="trimestres" multiple="multiple"
									tabindex="8">
										<option value="Toutes"
											${empty contexte.criteres.trimestresAsList && contexte.reinitialisation eq true || contexte.criteres.trimestresAsList.contains('Toutes')? 'selected=\'selected\'' : ''}>Toutes</option>
										<c:forEach var="item" items="${reference.listePeriodes}">
											<option value="${item.dateDebutTrimestre}"
												${contexte.criteres.trimestresAsList.contains(item.dateDebutTrimestre) ? 'selected=\'selected\'' : ''}>${item.libelleTrimestre}</option>
										</c:forEach>
								</select></td>
								<td><label for="nic">NIC :</label> <input id="nic"
									name="nic" maxlength="5"
									value="<c:out value="${contexte.criteres.nic}"/>" tabindex="9" />
								</td>
							</tr>
							<tr>
								<td><label for="typesPeriode">Nature :</label> <select
									id="typesPeriode" name="typesPeriode" multiple="multiple"
									tabindex="10">
										<c:forEach var="item" items="${reference.listeNatures}">
											<option value="${item.code}"
												${contexte.criteres.typesPeriodeAsList.contains(item.code) ? 'selected=\'selected\'' : ''}>${item.libelleCourt}</option>
										</c:forEach>
								</select></td>
								<td><label for="cptEnc">Cpt enc :</label> <input
									id="cptEnc" name="cptEnc" maxlength="6"
									value="<c:out value="${contexte.criteres.cptEnc}"/>"
									tabindex="11" /></td>
								<td><label for="raisonSociale">Raison soc. :</label> <input
									id="raisonSociale" name="raisonSociale" maxlength="38"
									value="<c:out value="${contexte.criteres.raisonSociale}"/>"
									tabindex="12" /></td>
							</tr>
							<tr>
								<td><label for="numContrat">Contrat :</label> <input
									type="text" id="numContrat" name="numContrat" maxlength="15"
									value="${contexte.criteres.numContrat}" tabindex="13" /></td>
								<td><label for="cptProd">Cpt prod :</label> <input
									id="cptProd" name="cptProd" maxlength="6"
									value="<c:out value="${contexte.criteres.cptProd}"/>"
									tabindex="14" /></td>
								<td><label for="vip">VIP :</label> <select id="vip"
									name="vip" multiple="multiple" tabindex="15">
										<c:forEach var="item" items="${reference.listeVIP}">
											<c:if
												test="${contexte.critereVipParDefaut.contains(item.code)}">
												<option value="${item.code}"
													${contexte.reinitialisation eq true || contexte.criteres.vipAsList.contains(item.code)? 'selected=\'selected\'' : ''}>${item.libelleCourt}</option>
											</c:if>
											<c:if
												test="${!contexte.critereVipParDefaut.contains(item.code) && utilisateur.roleGestionCde && utilisateur.droitVIP}">
												<option value="${item.code}"
													${contexte.criteres.vipAsList.contains(item.code)? 'selected=\'selected\'' : ''}>${item.libelleCourt}</option>
											</c:if>
										</c:forEach>
								</select></td>
							</tr>
							<tr>
								<td><label for="versements">Versement :</label> <select
									id="versements" name="versements" multiple="multiple"
									tabindex="16">
										<option value="false"
											${contexte.criteres.versementsAsList.contains(false) ? 'selected=\'selected\'' : ''}>Aucun</option>
										<option value="true"
											${contexte.criteres.versementsAsList.contains(true) ? 'selected=\'selected\'' : ''}>Au
											moins 1</option>
								</select></td>
							</tr>
							<tr>
								<td><label for="affecteA">Affecté à :</label> <select
									id="affecteA" name="affecteA" multiple="multiple" class="customCriteresGestionnaire" tabindex="17">
										<c:forEach var="item" items="${reference.listeUsersAffecteA}">
											<option value="${item.identifiantActiveDirectory}"
												${ contexte.criteres.affecteAAsList.contains(item.identifiantActiveDirectory) ? 'selected=\'selected\'' : ''} data-name="${item.nom}" >
												
												<c:choose>
													<c:when test="${item.identifiantActiveDirectory == 'NON_AFFECTE' || item.identifiantActiveDirectory == 'AUTRE_GEST' }">
													${item.nom}
													</c:when>
													<c:otherwise>
													${item.nom} ${item.prenom} - ${item.identifiantActiveDirectory}
													</c:otherwise>
												</c:choose>
												
												</option>
											
											
										</c:forEach>
								</select></td>
								<td><label for="aTraiterPar">A traiter par :</label> <select
									id="aTraiterPar" name="aTraiterPar" multiple="multiple"
									class="customCriteresGestionnaire" tabindex="18">
										<c:forEach var="item" items="${reference.listeUsersATraiterPar}">
											<option value="${item.identifiantActiveDirectory}"
												${ contexte.criteres.aTraiterParAsList.contains(item.identifiantActiveDirectory) ? 'selected=\'selected\'' : ''}>
												
												<c:choose>
													<c:when test="${item.identifiantActiveDirectory == 'NON_ASSIGNE' || item.identifiantActiveDirectory == 'AUTRE_GEST' }">
													${item.nom}
													</c:when>
													<c:otherwise>
													${item.nom} ${item.prenom} - ${item.identifiantActiveDirectory}
													</c:otherwise>
												</c:choose>
												
													
												</option>
										</c:forEach>
								</select></td>
								<td><label for="traitePar">Traité par :</label> <select
									id="traitePar" name="traitePar" multiple="multiple"
									class="customCriteresGestionnaire" tabindex="19">
										<c:forEach var="item" items="${reference.listeUsersTraitePar}">
											<option value="${item.identifiantActiveDirectory}"
												${ contexte.criteres.traiteParAsList.contains(item.identifiantActiveDirectory) ? 'selected=\'selected\'' : ''}>
													
													<c:choose>
													<c:when test="${item.identifiantActiveDirectory == 'TRAITE_AUTO' || item.identifiantActiveDirectory == 'AUTRE_GEST' }">
													${item.nom}
													</c:when>
													<c:otherwise>
													${item.nom} ${item.prenom} - ${item.identifiantActiveDirectory}
													</c:otherwise>
												</c:choose>
												
												</option>
										</c:forEach>
								</select></td>
								
								
							</tr>
						</table>
						<input type="hidden" id="pageNumero" name="pageNumero" value="1" />
						<input type="hidden" id="pageTaille" name="pageTaille" value="50">
						<input type="hidden" id="triChamp" name="triChamp" value="spec">
						<input type="hidden" id="triAsc" name="triChamp" value="true">
						<div class="boutonsFormulaire">
							<button type="submit" class="boutonRecherche" tabindex="16">
								<div class="boutonIcone"></div>
								<span>Rechercher</span>
							</button>
						</div>
					</form>
				</div>

				<!-- Tableau de résultats de recherche -->
				<div class="informations">
					<span>${informations}</span>
				</div>
				<div class="resultatsWrapper">
					<table class="resultats">
						<thead>
							<tr>
								<td> <input type="checkbox" id="select_all"   /> </td>
								<c:if test="${contexte.criteres.triChamp != 'groupeGestion'}">
									<td><a
										href="?${fn:replace(contexte.urlQueryString, 'triChamp='.concat(contexte.criteres.triChamp), 'triChamp=groupeGestion')}"
										title="Trier par groupe de gestion">G.</a></td>
								</c:if>
								<c:if test="${contexte.criteres.triChamp == 'groupeGestion'}">
									<td><span>G.</span> <c:if
											test="${contexte.criteres.triAsc}">
											<a
												href="?${fn:replace(contexte.urlQueryString, 'triAsc=true', 'triAsc=false')}"
												class="triAsc" title="Trier par ordre décroissant"></a>
										</c:if> <c:if test="${!contexte.criteres.triAsc}">
											<a
												href="?${fn:replace(contexte.urlQueryString, 'triAsc=false', 'triAsc=true')}"
												class="triDesc" title="Trier par ordre croissant"></a>
										</c:if></td>
								</c:if>
								<c:if test="${contexte.criteres.triChamp != 'libelleFamille'}">
									<td><a
										href="?${fn:replace(contexte.urlQueryString, 'triChamp='.concat(contexte.criteres.triChamp), 'triChamp=libelleFamille')}"
										title="Trier par famille de produit">Fam.</a></td>
								</c:if>
								<c:if test="${contexte.criteres.triChamp == 'libelleFamille'}">
									<td><span>Fam.</span> <c:if
											test="${contexte.criteres.triAsc}">
											<a
												href="?${fn:replace(contexte.urlQueryString, 'triAsc=true', 'triAsc=false')}"
												class="triAsc" title="Trier par ordre décroissant"></a>
										</c:if> <c:if test="${!contexte.criteres.triAsc}">
											<a
												href="?${fn:replace(contexte.urlQueryString, 'triAsc=false', 'triAsc=true')}"
												class="triDesc" title="Trier par ordre croissant"></a>
										</c:if></td>
								</c:if>
								<c:if test="${contexte.criteres.triChamp != 'numeroContrat'}">
									<td><a
										href="?${fn:replace(contexte.urlQueryString, 'triChamp='.concat(contexte.criteres.triChamp), 'triChamp=numeroContrat')}"
										title="Trier par numéro de contrat">Contrat</a></td>
								</c:if>
								<c:if test="${contexte.criteres.triChamp == 'numeroContrat'}">
									<td><span>Contrat</span> <c:if
											test="${contexte.criteres.triAsc}">
											<a
												href="?${fn:replace(contexte.urlQueryString, 'triAsc=true', 'triAsc=false')}"
												class="triAsc" title="Trier par ordre décroissant"></a>
										</c:if> <c:if test="${!contexte.criteres.triAsc}">
											<a
												href="?${fn:replace(contexte.urlQueryString, 'triAsc=false', 'triAsc=true')}"
												class="triDesc" title="Trier par ordre croissant"></a>
										</c:if></td>
								</c:if>
								<c:if test="${contexte.criteres.triChamp != 'periode'}">
									<td><a
										href="?${fn:replace(contexte.urlQueryString, 'triChamp='.concat(contexte.criteres.triChamp), 'triChamp=periode')}"
										title="Trier par période">Période</a></td>
								</c:if>
								<c:if test="${contexte.criteres.triChamp == 'periode'}">
									<td><span>Période</span> <c:if
											test="${contexte.criteres.triAsc}">
											<a
												href="?${fn:replace(contexte.urlQueryString, 'triAsc=true', 'triAsc=false')}"
												class="triAsc" title="Trier par ordre décroissant"></a>
										</c:if> <c:if test="${!contexte.criteres.triAsc}">
											<a
												href="?${fn:replace(contexte.urlQueryString, 'triAsc=false', 'triAsc=true')}"
												class="triDesc" title="Trier par ordre croissant"></a>
										</c:if></td>
								</c:if>
								<c:if test="${contexte.criteres.triChamp != 'libelleType'}">
									<td><a
										href="?${fn:replace(contexte.urlQueryString, 'triChamp='.concat(contexte.criteres.triChamp), 'triChamp=libelleType')}"
										title="Trier par nature">Nature</a></td>
								</c:if>
								<c:if test="${contexte.criteres.triChamp == 'libelleType'}">
									<td><span>Nature</span> <c:if
											test="${contexte.criteres.triAsc}">
											<a
												href="?${fn:replace(contexte.urlQueryString, 'triAsc=true', 'triAsc=false')}"
												class="triAsc" title="Trier par ordre décroissant"></a>
										</c:if> <c:if test="${!contexte.criteres.triAsc}">
											<a
												href="?${fn:replace(contexte.urlQueryString, 'triAsc=false', 'triAsc=true')}"
												class="triDesc" title="Trier par ordre croissant"></a>
										</c:if></td>
								</c:if>
								<c:if test="${contexte.criteres.triChamp != 'aTraiterPar'}">
									<td><a
										href="?${fn:replace(contexte.urlQueryString, 'triChamp='.concat(contexte.criteres.triChamp), 'triChamp=aTraiterPar')}"
										title="Trier par nature">A traiter par</a></td>
								</c:if>
								<c:if test="${contexte.criteres.triChamp == 'aTraiterPar'}">
									<td><span>A traiter par</span> <c:if
											test="${contexte.criteres.triAsc}">
											<a
												href="?${fn:replace(contexte.urlQueryString, 'triAsc=true', 'triAsc=false')}"
												class="triAsc" title="Trier par ordre décroissant"></a>
										</c:if> <c:if test="${!contexte.criteres.triAsc}">
											<a
												href="?${fn:replace(contexte.urlQueryString, 'triAsc=false', 'triAsc=true')}"
												class="triDesc" title="Trier par ordre croissant"></a>
										</c:if></td>
								</c:if>
								<c:if test="${contexte.criteres.triChamp != 'libelleEtat'}">
									<td><a
										href="?${fn:replace(contexte.urlQueryString, 'triChamp='.concat(contexte.criteres.triChamp), 'triChamp=libelleEtat')}"
										title="Trier par statut">Statut</a></td>
								</c:if>
								<c:if test="${contexte.criteres.triChamp == 'libelleEtat'}">
									<td><span>Statut</span> <c:if
											test="${contexte.criteres.triAsc}">
											<a
												href="?${fn:replace(contexte.urlQueryString, 'triAsc=true', 'triAsc=false')}"
												class="triAsc" title="Trier par ordre décroissant"></a>
										</c:if> <c:if test="${!contexte.criteres.triAsc}">
											<a
												href="?${fn:replace(contexte.urlQueryString, 'triAsc=false', 'triAsc=true')}"
												class="triDesc" title="Trier par ordre croissant"></a>
										</c:if></td>
								</c:if>
								<c:if
									test="${contexte.criteres.triChamp != 'derniereDateChangementStatut'}">
									<td><a
										href="?${fn:replace(contexte.urlQueryString, 'triChamp='.concat(contexte.criteres.triChamp), 'triChamp=derniereDateChangementStatut')}"
										title="Trier par date de statut">Date Statut</a></td>
								</c:if>
								<c:if
									test="${contexte.criteres.triChamp == 'derniereDateChangementStatut'}">
									<td><span>Date Statut</span> <c:if
											test="${contexte.criteres.triAsc}">
											<a
												href="?${fn:replace(contexte.urlQueryString, 'triAsc=true', 'triAsc=false')}"
												class="triAsc" title="Trier par ordre décroissant"></a>
										</c:if> <c:if test="${!contexte.criteres.triAsc}">
											<a
												href="?${fn:replace(contexte.urlQueryString, 'triAsc=false', 'triAsc=true')}"
												class="triDesc" title="Trier par ordre croissant"></a>
										</c:if></td>
								</c:if>
								<td><span>Msg</span></td>
								<td><span>Action</span></td>
							</tr>
						</thead>
						<tbody class="ligneTableauResultat">
							<c:forEach items="${resultats}" var="periode">
								<tr>
									<td><input type="checkbox" name="selectionPeriode_${periode.idPeriode}" id="${periode.idPeriode}"  /> </td>
									<td id="groupeGestion_${periode.idPeriode}"><span>${periode.groupeGestion}</span></td>
									<td id="famille_${periode.idPeriode}"><span>${periode.libelleFamille}</span></td>
									<td id="numeroContrat_${periode.idPeriode}"><span>${periode.numeroContrat}</span></td>
									<td id="periode_${periode.idPeriode}"><span>${periode.periode}</span></td>
									<td id="libelleType_${periode.idPeriode}"><span>${periode.libelleType}</span></td>
									<td id="aTraiterPar_${periode.idPeriode}">
										<c:if test="${periode.aTraiterPar != null }">
										<div class="actionTableau aTraiterPar" url-messages="<c:url value="/ihm/declarations/${periode.idPeriode}/${periode.aTraiterPar}" />" >
												<span>${periode.aTraiterPar}</span>
										</div>
										</c:if>
									</td>			
									<td id="libelleEtat_${periode.idPeriode}"><span>${periode.libelleEtat}</span></td>
									<td><span>${periode.derniereDateChangementStatutAsDate}</span></td>
									<td><c:if test="${periode.avecMessageRejet}">
											<div class="actionTableau rejet"
												url-messages="<c:url value="/ihm/declarations/${periode.idPeriode}/messages/rejet" />"></div>
										</c:if> <c:if test="${periode.avecMessageSignal}">
											<div class="actionTableau signal"
												url-messages="<c:url value="/ihm/declarations/${periode.idPeriode}/messages/signal" />"></div>
										</c:if></td>
									<td>
										<div class="actionTableau lienConsultation">
											<a
												href="<c:url value="/ihm/declarations/${periode.idPeriode}/synthese?criteresRecherche=${contexte.contratCriteresRecherche}" />"></a>
										</div> <c:if
											test="${utilisateur.roleGestionRedac && periode.etatPeriodeModifiable}">
											<div class="actionTableau lienModification">
												<a
													href="<c:url value="/ihm/declarations/${periode.idPeriode}/statut?criteresRecherche=${contexte.contratCriteresRecherche}" />"></a>
											</div>
										</c:if>
									</td>
									<td id="estBloque_${periode.idPeriode}" hidden="hidden">${periode.estBloque}</td>
									<td id="raisonSociale_${periode.idPeriode}" hidden="hidden">${periode.raisonSociale}</td>
									<td id="dateDebutPeriode_${periode.idPeriode}" hidden="hidden">${periode.dateDebutPeriode}</td>
									<td id="dateFinPeriode_${periode.idPeriode}" hidden="hidden">${periode.dateFinPeriode}</td>
									<td id="etat_${periode.idPeriode}" hidden="hidden">${periode.etatPeriode}</td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
				<div class="NbPeriodesFiltrees">
					<span>Nombre de périodes filtrées: ${nbPeriodesFiltrees}</span>
					<input type="hidden" id="nbPeriodesFiltrees" value="${nbPeriodesFiltrees}">
				</div>
				<div class="outilsPagination">
					<ol>
						<c:if test="${resultatsMeta.pageNumero > 1}">
							<li class="lienPagination"><a
								href="?${fn:replace(contexte.urlQueryString, 'pageNumero='.concat(resultatsMeta.pageNumero), 'pageNumero='.concat(resultatsMeta.pageNumero - 1))}">Préc.</a></li>
						</c:if>
						<c:if test="${resultatsMeta.pageNumero == 1}">
							<li class="lienPagination pageActuelle">1</li>
						</c:if>
						<c:if test="${resultatsMeta.pageNumero != 1}">
							<li class="lienPagination"><a
								href="?${fn:replace(contexte.urlQueryString, 'pageNumero='.concat(resultatsMeta.pageNumero), 'pageNumero=1')}">1</a></li>
						</c:if>
						<c:if test="${resultatsMeta.maxPage > 1}">
							<c:if test="${resultatsMeta.pageNumero == 2}">
								<li class="lienPagination pageActuelle">2</li>
							</c:if>
							<c:if test="${resultatsMeta.pageNumero != 2}">
								<li class="lienPagination"><a
									href="?${fn:replace(contexte.urlQueryString, 'pageNumero='.concat(resultatsMeta.pageNumero), 'pageNumero=2')}">2</a></li>
							</c:if>
						</c:if>
						<c:if test="${resultatsMeta.maxPage > 2}">
							<c:if test="${resultatsMeta.pageNumero == 3}">
								<li class="lienPagination pageActuelle">3</li>
								<c:if test="${resultatsMeta.maxPage > 4}">
									<li class="lienPagination"><a
										href="?${fn:replace(contexte.urlQueryString, 'pageNumero='.concat(resultatsMeta.pageNumero), 'pageNumero=4')}">4</a></li>
								</c:if>
							</c:if>
							<c:if test="${resultatsMeta.pageNumero != 3}">
								<li class="lienPagination"><a
									href="?${fn:replace(contexte.urlQueryString, 'pageNumero='.concat(resultatsMeta.pageNumero), 'pageNumero=3')}">3</a></li>
							</c:if>
						</c:if>
						<c:if test="${resultatsMeta.maxPage > 3}">
							<c:if
								test="${resultatsMeta.pageNumero < resultatsMeta.maxPage - 2}">
								<c:if test="${resultatsMeta.pageNumero > 3}">
									<c:if test="${resultatsMeta.pageNumero > 5}">
										<li>...</li>
									</c:if>
									<c:if test="${resultatsMeta.pageNumero > 4}">
										<li class="lienPagination"><a
											href="?${fn:replace(contexte.urlQueryString, 'pageNumero='.concat(resultatsMeta.pageNumero), 'pageNumero='.concat(resultatsMeta.pageNumero - 1))}">${resultatsMeta.pageNumero - 1}</a></li>
									</c:if>
									<li class="lienPagination pageActuelle">${resultatsMeta.pageNumero}</li>
									<li class="lienPagination"><a
										href="?${fn:replace(contexte.urlQueryString, 'pageNumero='.concat(resultatsMeta.pageNumero), 'pageNumero='.concat(resultatsMeta.pageNumero + 1))}">${resultatsMeta.pageNumero + 1}</a></li>
								</c:if>
								<li>...</li>
								<li class="lienPagination"><a
									href="?${fn:replace(contexte.urlQueryString, 'pageNumero='.concat(resultatsMeta.pageNumero), 'pageNumero='.concat(resultatsMeta.maxPage))}">${resultatsMeta.maxPage}</a></li>
							</c:if>
							<c:if
								test="${resultatsMeta.pageNumero >= resultatsMeta.maxPage - 2}">
								<c:if
									test="${resultatsMeta.pageNumero > 6 || resultatsMeta.pageNumero == 6 && resultatsMeta.maxPage > 6 }">
									<li>...</li>
								</c:if>
								<c:if test="${resultatsMeta.maxPage == 5}">
									<c:if test="${resultatsMeta.pageNumero == 4}">
										<li class="lienPagination pageActuelle">4</li>
									</c:if>
									<c:if test="${resultatsMeta.pageNumero == 5}">
										<li class="lienPagination"><a
											href="?${fn:replace(contexte.urlQueryString, 'pageNumero='.concat(resultatsMeta.pageNumero), 'pageNumero=4')}">4</a></li>
									</c:if>
								</c:if>
								<c:if test="${resultatsMeta.maxPage > 5}">
									<c:if
										test="${resultatsMeta.pageNumero == resultatsMeta.maxPage - 2}">
										<c:if test="${resultatsMeta.pageNumero > 4}">
											<li class="lienPagination"><a
												href="?${fn:replace(contexte.urlQueryString, 'pageNumero='.concat(resultatsMeta.pageNumero), 'pageNumero='.concat(resultatsMeta.maxPage - 3))}">${resultatsMeta.maxPage - 3}</a></li>
										</c:if>
										<li class="lienPagination pageActuelle">${resultatsMeta.maxPage - 2}</li>
									</c:if>
									<c:if
										test="${resultatsMeta.pageNumero != resultatsMeta.maxPage - 2}">
										<li class="lienPagination"><a
											href="?${fn:replace(contexte.urlQueryString, 'pageNumero='.concat(resultatsMeta.pageNumero), 'pageNumero='.concat(resultatsMeta.maxPage - 2))}">${resultatsMeta.maxPage - 2}</a></li>
									</c:if>
									<c:if
										test="${resultatsMeta.pageNumero == resultatsMeta.maxPage - 1}">
										<li class="lienPagination pageActuelle">${resultatsMeta.maxPage - 1}</li>
									</c:if>
									<c:if
										test="${resultatsMeta.pageNumero != resultatsMeta.maxPage - 1}">
										<li class="lienPagination"><a
											href="?${fn:replace(contexte.urlQueryString, 'pageNumero='.concat(resultatsMeta.pageNumero), 'pageNumero='.concat(resultatsMeta.maxPage - 1))}">${resultatsMeta.maxPage - 1}</a></li>
									</c:if>
								</c:if>
								<c:if
									test="${resultatsMeta.pageNumero == resultatsMeta.maxPage}">
									<li class="lienPagination pageActuelle">${resultatsMeta.maxPage}</li>
								</c:if>
								<c:if
									test="${resultatsMeta.pageNumero != resultatsMeta.maxPage}">
									<li class="lienPagination"><a
										href="?${fn:replace(contexte.urlQueryString, 'pageNumero='.concat(resultatsMeta.pageNumero), 'pageNumero='.concat(resultatsMeta.maxPage))}">${resultatsMeta.maxPage}</a></li>
								</c:if>
							</c:if>
						</c:if>
						<c:if test="${resultatsMeta.pageNumero < resultatsMeta.maxPage}">
							<li class="lienPagination"><a
								href="?${fn:replace(contexte.urlQueryString, 'pageNumero='.concat(resultatsMeta.pageNumero), 'pageNumero='.concat(resultatsMeta.pageNumero + 1))}">Suiv.</a></li>
						</c:if>
					</ol>
				</div>
				<div>
				<input type="hidden" id="parametresRecherche" name="parametresRecherche" value="${contexte.urlQueryString}" />
					<form id="fusionFormulaire"
							action="<c:url value="/ihm/declarations/fusion" />"
							onsubmit="return ValidationFusion()" method="post">
							<input type="hidden" id="idsPeriodesFusion" name="idsPeriodesFusion"/>
							<input type="hidden" id="parametresRechercheFusion" name="parametresRecherche"/>
							<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
						<div class="boutonFormulaireFusion">
								<button  ${reference.droitFusionerPeriode ? '' : 'disabled=\"disabled\"' } type="submit" class="boutonFusion" >
									<span>Fusionner les périodes cochées </span>
								</button>
								<button  ${(contexte.droitGroupe2 && contexte.droitGroupe3 && nbPeriodesFiltrees != '0' ) ? '' : 'disabled=\"disabled\"' } type="button" class="boutonChangementMasse" data-toggle="modal" data-target="#modalChangementEnMasse" onclick="AfficheCriteresRecherchePopup()" >
									<span>Changer en masse les périodes </span>
								</button>
						</div>
					</form>
					
					
					
				</div>
				<div class="exportExcel">
					<div class="exportExcelIcone"></div>
					<span><a target="_blank"
						href="<c:url value="/ihm/declarations/exports/excel?${contexte.urlQueryString}" />">Extraire
							cette liste complète en Excel</a></span>
				</div>
			</div>
		</div>
		<%@include file="../communs/Footer.jsp"%>
	</div>
	
<script type="text/javascript" src="<c:url value="/ressources/libs/jquery.maskedinput-1.3.1.js" />"></script>
<script type="text/javascript" src="<c:url value="/ressources/js/declarations/recherche.js?v=${version}" />"></script>
<script type="text/javascript" src="<c:url value="/ressources/js/declarations/fusion.js?v=${version}" />"></script>

<!-- Popup Modal -->
<%@include file="ChangementEnMasse.jsp"%>
</body>
</html>