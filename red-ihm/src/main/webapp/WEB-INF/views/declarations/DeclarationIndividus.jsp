<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <%@include file="../communs/PageMetadata.jsp" %>
	<link rel="stylesheet" href="<c:url value="/ressources/css/declarations.css" />"></link>
</head>
<body>

   	<div class="appWrapper">
	   	<%@include file="../communs/Header.jsp" %>
	   	<div class="bodyWrapper">
	    	<%@include file="../communs/Menu.jsp" %>
	    	<div class="content declaration">
	    		<jsp:include page="DeclarationNavigation.jsp" flush="true">
					<jsp:param name="page" value="individus" />
				</jsp:include>
	    		<div class="detailsPeriodeWrapper individusPeriodeWrapper">
    				<h1>
    					Individus déclarés :
    				</h1>
    				<c:if test="${affichagePartielIndividus}">
	    				<div class="informations">
	    					<span>Seul les 1000 premiers individus de ce contrat sont présentés, reportez-vous à l’export Excel pour une liste exhaustive des individus.</span>
	    				</div>
	    			</c:if>
    				<div class="exportExcel">
	    				<div class="exportExcelIcone"></div>
	    				<span><a target="_blank" href="<c:url value="/ihm/declarations/${declaration.idPeriode}/individus/exports/excel" />">Extraire cette liste complète en Excel</a></span>
	    			</div>
	    			
	    			<div class="exportExcel">
	    				<div class="exportExcelIcone"></div>
	    				<span><a target="_blank" href="<c:url value="/ihm/declarations/${declaration.idPeriode}/individus/exports/excelMontants" />">Extraire liste allégée avec montant cotisation en Excel</a></span>
	    			</div>
	    			<div class="exportExcel">
	    				<div class="exportExcelIcone"></div>
	    				<span><a target="_blank" href="<c:url value="/ihm/declarations/${declaration.idPeriode}/individus/exports/excelMessages" />">Extraire liste allégée avec messages en Excel</a></span>
	    			</div>

	    			<table class="donneesConsultation individusPeriode">
    					<thead>
    						<tr>
    							<td><span>Nom famille</span></td>
    							<td><span>Nom usage</span></td>
    							<td><span>Prénom</span></td>
    							<td><span>NIR/NTT</span></td>
    							<td><span>Matricule</span></td>
    							<td><span>Début contrat travail</span></td>
    							<td><span>Fin contrat travail</span></td>
    							<td><span>Code Pop</span></td>
    							<td><span>Signalement</span></td>
    							
    						</tr>
    					</thead>
    					<tbody>
   							<c:forEach items="${affiliations}" var="affiliation">
								<tr>
									<c:set var="nirOuNtt" value="${not empty affiliation.identifiantRepertoire ? affiliation.identifiantRepertoire : affiliation.numeroTechniqueTemporaire}"></c:set>
									<c:set var="nirNttId" value="${affiliation.identifiantRepertoire}|${affiliation.numeroTechniqueTemporaire}"></c:set>
									
									<td><span>${affiliation.nomFamille}</span></td>
									<td><span>${affiliation.nomUsage}</span></td>
									<td><span>${affiliation.prenom}</span></td>
									<td><span>${nirOuNtt}</span></td>
									<td><span>${affiliation.matricule}</span></td>
									<td><span>${affiliation.dateDebutContratSansValidation}</span></td>
									<td><span>${affiliation.dateFinPrevisionnelleSansValidation}</span></td>
									<td><span>${affiliation.codePopulation}</span></td>
	    							<td class="alertesPeriode">
	    								<ul>
											<c:forEach items="${messagesControlesIndividus[nirNttId]}" var="msg">
												<li class="alerte${msg.niveauAlerte == 'SIGNAL' ? 'Avertissement' : 'Bloquante'}">${msg.messageUtilisateur}</li>
											</c:forEach>
										</ul>
									</td>
	    							
								</tr>
   							</c:forEach>
    					</tbody>
    				</table>	
	    		</div>
		    	<div class="outilsPeriode">
	    			<button class="navRecherche" type="button" onclick="location.href='<c:url value="/ihm/declarations?${contexte.rechercheUrlQueryString}" />'">&lt;&lt; Retour à la liste</button>
	    		</div>
	    	</div>
	   	</div>
	   	<%@include file="../communs/Footer.jsp" %>
   	</div>
   	
</body>
</html>