<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <%@include file="../communs/PageMetadata.jsp" %>
	<link rel="stylesheet" href="<c:url value="/ressources/css/declarations.css" />"></link>
</head>
<body>
   	<div class="appWrapper">
	   	<%@include file="../communs/Header.jsp" %>
	   	<div class="bodyWrapper">
	    	<%@include file="../communs/Menu.jsp" %>
	    	<div class="content declaration">
	    		<jsp:include page="DeclarationNavigation.jsp" flush="true">
					<jsp:param name="page" value="statut" />
				</jsp:include>
	    		<div class="detailsPeriodeWrapper changementStatutPeriodeWrapper">
	    			<form id="formChangement" class="changementStatutPeriode" 
	    			method="post">
						<h1>
	    					Saisie de commentaire sur la période :
	    				</h1>
	    				<table>
	    					<tr>
	    						<td class="champsCommentaire">
	    							<div>
		    							<label for="commentaireUtilisateur" class="textareaLabel valign-top">Commentaire :</label>
		    							<textarea id="commentaireUtilisateur" name="commentaireUtilisateur" cols="50" rows="5" maxlength="1000" ${reference.droitCommentaire ? '' : 'disabled=\"disabled\"'}></textarea>
	    							</div>
	    						</td>
	    						<td class="valign-bottom">
									<button class="btnCommentaire" ${reference.droitCommentaire ? '' : 'disabled=\"disabled\"'} value="Enregistrer" type="button" name="flagAjouterCommentaire" id="commentairebtn" onclick="submitFormWithforceContentType('formChangement','flagAjouterCommentaire','Enregistrer');"  disabled="disabled">Enregistrer</button>
	    						</td>
	    					</tr>
	    				</table>
						
						<div class="historiqueCommentairesPeriodeWrapper">
		    				
		    				<h1>
		    					Historique des commentaires pour cette période :
		    				</h1>
	    				
		    				<table class="resultats historiqueStatutsPeriode" >
		    					<thead>
		    						<tr>
		    							<td class="td-date">Date</td>
		    							<td class="td-par">Par</td>
		    							<td>Commentaire</td>
		    						</tr>
		    					</thead>
		    					<tbody>
									<c:forEach items="${historiqueCommentaires}" var="commentaire" varStatus="loop">								
										<tr>								    
											<td class="td-date">${commentaire.dateHeureSaisieAsDate}</td>
											<td class="td-par">${commentaire.identifiantUtilisateur}</td>
											<td>${commentaire.commentaireUtilisateur}</td>
										</tr>
									</c:forEach>
									<c:if test="${empty historiqueCommentaires}">
	    								<tr>								    
											<td colspan="3">Aucun historique présent</td>
										</tr>
									</c:if>
		    					</tbody>
		    				</table>
						
						</div>
							<h1>Blocage de la période :</h1>
							<table>
								<tr>
									<td>
										<div>
											<label class="infoMessageControle ${reference.niveauAlerte == 'SIGNAL' ? 'signal' : reference.niveauAlerte == 'REJET' ? 'rejet' : ''}">&nbsp;</label><label for="bloquerPeriodeCbx">Bloquer la période : </label> <input
												type="checkbox" name="bloquerPeriodeCbx" id="bloquerPeriodeCbx"
												class="cbxBloquerPeriode"
												${reference.estBloque ? 'checked=\"checked\"' : '' } 
												${reference.droitBloquerPeriode ? '' : 'disabled=\"disabled\"' } />
										</div>
									</td>
									<td>
										
										<button ${reference.droitBloquerPeriode ? '' : 'disabled=\"disabled\"' }  value="Valider" type="button" name="flagBloquerPeriode" id="bloquerperiodebtn" onclick="submitFormWithforceContentType('formChangement','flagBloquerPeriode','Valider');" >Valider</button>
										
									</td>
								</tr>
							</table>
						
						<h1>
	    					Changer le statut de la période :
	    				</h1>
	    				<table>
	    					<tr>
	    						<td class="champsChangementStatutPeriode">
	    							<div>
		    							<label for="etatPeriode">Statut :</label>
		    							<select id="etatPeriode" name="etatPeriode" ${reference.changementStatutImpossible ? 'disabled=\"disabled\"' : ''} class="minWidthList">
											<option value=""></option>
											<c:forEach var="item" items="${reference.listeStatuts}">
												<option value="${item.code}">${item.libelleCourt}</option>
											</c:forEach>
		    							</select>
	    							</div>
	    						</td>
	    						<td>
									<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
									<button ${reference.changementStatutImpossible ? 'disabled=\"disabled\"' : ''} value="Enregistrer" type="button" name="flagChangementStatut" id="changementstatutbtn" onclick="submitFormWithforceContentType('formChangement','flagChangementStatut','Enregistrer');" >Enregistrer</button>
	    						
	    						</td>
	    					</tr>
	    			</table>
	    			
	    			<div class="historiqueStatutsPeriodeWrapper">
	    				<h1>
	    					Historique des changements de statut pour cette période :
	    				</h1>
	    				<table class="resultats historiqueStatutsPeriode">
	    					<thead>
	    						<tr>
	    							<td>Statut</td>
	    							<td>Date</td>
	    							<td>Par</td>
	    							<td>Commentaire</td>
	    						</tr>
	    					</thead>
	    					<tbody>
								<c:forEach items="${historiqueEtats}" var="etat" varStatus="loop">								
									<tr>								    
									    <td>${reference.mapStatuts[etat.etatPeriode.codeEtat].libelleEdition}</td>
										<td>${etat.dateHeureChangementAsDate}</td>
										<td>${etat.identifiantUtilisateur}</td>
										<td>${etat.commentaireUtilisateur}</td>
									</tr>
								</c:forEach>
	    					</tbody>
	    				</table>
	    			</div>
	    			
	    			<h1>Période en attente de retour entreprise :</h1>
	    			<table>
	    				<tr>
	    					<td>
	    					 	<label class="infoMessageControle" for="attenteRetourEntreprise">En attente de retour entreprise :</label>
	    					 	 <input type="checkbox" name="attenteRetourEntreprise" id="attenteRetourEntreprise" 	 
							     class="cbxAttentePeriode" 
							     ${reference.droitAttenteRetourEntreprise ? '' : 'disabled=\"disabled\"' }
							     ${reference.attenteRetourEntreprise ? 'checked=\"checked\"' : '' }
							     />
	    					</td>
	    					<td>
	    						<button value="Valider" type="button" name="flagValiderRetourEntreprise" id="validerRetourEntreprise" onclick="submitFormWithforceContentType('formChangement','flagValiderRetourEntreprise','Valider');" disabled="disabled">Valider</button>    						
	    					</td>
	    				</tr>
	    			</table>
	    			
	    			<div class="historiqueAttenteRetourEtpPeriodeWrapper">
	    				<h1>
	    					Historique des mises en attente pour cette période :
	    				</h1>
	    				<table class="resultats historiqueStatutsPeriode">
		    					<thead>
		    						<tr>
		    							<td>Action</td>
		    							<td>Date</td>
		    							<td>Par</td>
		    						</tr>
		    					</thead>
		    					<tbody>
									<c:forEach items="${historiqueAttenteEtp}" var="ligneHistorique" varStatus="loop">								
										<tr>								    
											<td>${ligneHistorique.libelleAction}</td>
											<td>${ligneHistorique.dateHeureRetourAsDate}</td>
											<td>${ligneHistorique.identifiantUtilisateur}</td>
										</tr>
									</c:forEach>
									<c:if test="${empty historiqueAttenteEtp}">
	    								<tr>								    
											<td colspan="3">Aucun historique présent</td>
										</tr>
									</c:if>
		    					</tbody>
		    				</table>
	    			</div>
	    			
	    			<h1>Affecter le responsable de gestion à la période :</h1>
	    			<table>
	    					<tr>
							  <td class = "main-gest aligntop">
							  <label for="gestionnaireAffecteA" class="stylelabel" >Gestionnaire :<br><span class="subTitre">(Affecté à)</span></label> 
							  	<select id="gestionnaireAffecteA" name="gestionnaireAffecteA" class="customCriteresGestionnaire"
							  	${reference.droitsSurLesListes ? '' : 'disabled=\"disabled\"'} class="aligntop">
									<c:forEach var="item" items="${reference.listeUsersAffecteA}">
										<option value="${item.identifiantActiveDirectory}" 
											${reference.gestionnaireAffecteA == item.identifiantActiveDirectory ? 'selected=\'selected\'' : ''} >
											<c:choose>
												<c:when test="${item.identifiantActiveDirectory == 'NON_AFFECTE' }">
												${item.nom}
												</c:when>
												<c:otherwise>
												${item.nom} ${item.prenom} ${item.identifiantActiveDirectory}
												</c:otherwise>
											</c:choose>
										</option>			
									</c:forEach>
								</select>
							</td>
							<td class="aligntop">
							<button value="Affecter" type="button" name="flagAffecterGestionnaire" id="bouttonAffectation" onclick="submitFormWithforceContentType('formChangement','flagAffecterGestionnaire','Affecter');" >Affecter</button>
							</td>	
						</tr>
					</table>
					
					<h1>Assigner la période pour traitement :</h1>
					<table>
						<tr>							
							<td class = "main-gest aligntop">
								<label for="gestionnaireATraiterPar" class="stylelabel" >Gestionnaire :<br><span class="subTitre">(A traiter par)</span> </label> 
								<select id="gestionnaireATraiterPar" name="gestionnaireATraiterPar" class="customCriteresGestionnaire"
								${reference.droitsSurLesListes ? '' : 'disabled=\"disabled\"'} class="aligntop" >
									<c:forEach var="item" items="${reference.listeUsersATraiterPar}">
										<option value="${item.identifiantActiveDirectory}" 
											${reference.gestionnaireATraiterPar == item.identifiantActiveDirectory ? 'selected=\'selected\'' : ''} >
											<c:choose>
												<c:when test="${item.identifiantActiveDirectory == 'NON_ASSIGNE' }">
												${item.nom}
												</c:when>
												<c:otherwise>
												${item.nom} ${item.prenom} ${item.identifiantActiveDirectory}
												</c:otherwise>
											</c:choose>
										</option>		
									</c:forEach>
								</select>
							</td>
							<td class="aligntop">
						        <button value="Assigner" type="button" name="flagAssignerGestionnaire" id="bouttonAssignation" onclick="submitFormWithforceContentType('formChangement','flagAssignerGestionnaire','Assigner');" >Assigner</button>					        
							</td>
						</tr>
	    				</table>
	    			</form>
	    			
	    			<div class="historiqueStatutsPeriodeWrapper">
		    				<h1>
		    					Historique des assignations pour cette période :
		    				</h1>
		    				<table class="resultats historiqueStatutsPeriode historiqueAssignationsPeriode">
		    					<thead>
		    						<tr>
		    							<td>A traiter par</td>
		    							<td>Depuis le</td>
		    						</tr>
		    					</thead>
		    					<tbody>
									<c:forEach items="${historiqueAssignations}" var="assignation" varStatus="loop">								
										<tr>								    
											<td>${assignation.recupLibelle}</td>
										
											<td>${assignation.dateHeureAssignationAsDate}</td>
										</tr>
									</c:forEach>
									<c:if test="${empty historiqueAssignations}">
	    								<tr>								    
											<td colspan="3">Aucun historique présent</td>
										</tr>
									</c:if>
		    					</tbody>
		    				</table>
	    			</div>	
	    		</div>
		    	<div class="outilsPeriode">
	    			<button class="navRecherche" type="button" onclick="location.href='<c:url value="/ihm/declarations?${contexte.rechercheUrlQueryString}" />'">&lt;&lt; Retour à la liste</button>
	    		</div>
	    	</div>
	   	</div>
	   	<%@include file="../communs/Footer.jsp" %>
   	</div>
   	
   	<!-- Script JS associés à la page   -->
   	<script type="text/javascript" src="<c:url value="/ressources/js/declarations/changementStatut.js?v=${version}" />"></script>
   	
   	<script type="text/javascript">
 		// gestion flag retour création historique
	REDAC.afficheMessageRetourMAJ(${param["_creationOK"]});
   	</script>
</body>
</html>