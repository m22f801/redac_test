<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" href="<c:url value="/ressources/css/declarations.css" />"></link>
</head>

<body>
<!-- Modal HTML -->
    <div id="modalChangementEnMasse" class="modal fade" data-backdrop="static" data-keyboard="false" >
        <div class="modal-dialog">
            <div class="modal-content popupGeneral">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h1 class="modal-title titre">Changement en masse des périodes</h1>
                </div>
                <div class="modal-body">
                	<div id="divCriteresRecherche">
                    		<div id="blocA" class="popupCriteres">
                    		<span class="souligne">Critères de sélection :</span>
		                    	 <table class="spaceVertical">
									<tr>
										<td><label class="marginleftPopup" >Statut :</label></td>
										<td><label class="marginleftPopup">Grpe gestion :</label></td>
										<td ><label class="marginleftPopup">N° Client :</label></td>
									</tr>
									<tr class="criteresLast">
										<td><div id="statutsSelected"  class="criteresSelected"></div></td>
										<td><div id="groupesGestionSelected"  class="criteresSelected"></div></td>
										<td><div id="numClientSelected" class="criteresSelected"></div></td>
									</tr>
									
									<tr>
										<td><label class="marginleftPopup">Avec message :</label></td>
										<td><label class="marginleftPopup">Famille :</label></td>
										<td><label class="marginleftPopup">SIREN :</label></td>
									</tr>
									<tr class="criteresLast">
										<td><div id="messagesSelected" class="criteresSelected"></div></td>
										<td><div id="famillesSelected" class="criteresSelected"></div></td>
										<td><div id="sirenSelected" class="criteresSelected"></div></td>
									</tr>
									<tr>
										<td><label class="marginleftPopup">Lib. Message :</label></td>
										<td><label class="marginleftPopup">Période :</label></td>
										<td><label class="marginleftPopup">NIC :</label></td>
									</tr>
									<tr class="criteresLast">
										<td><div id="idControlesSelected" class="criteresSelected"></div></td>
										<td><div id="trimestresSelected" class="criteresSelected"></div></td>
										<td><div id="nicSelected" class="criteresSelected"></div></td>
									</tr>
									<tr>
										<td><label class="marginleftPopup">Nature :</label></td>
										<td><label class="marginleftPopup">Cpt enc :</label></td>
										<td><label class="marginleftPopup">Raison soc. :</label></td>
									</tr>
									<tr class="criteresLast">
										<td><div id="typesPeriodeSelected" class="criteresSelectedNatureTr"></div></td>
										<td><div id="cptEncSelected" class="criteresSelectedNatureTr"></div></td>
										<td><div id="raisonSocialeSelected" class="criteresSelectedNatureTr"></div></td>
									</tr>
									<tr>
										<td><label class="marginleftPopup">Contrat :</label></td>
										<td><label class="marginleftPopup">Cpt prod :</label></td>
										<td><label class="marginleftPopup">VIP :</label></td>
									</tr>
									<tr class="criteresLast">
										<td><div id="numContratSelected" class="criteresSelectedContratTr"></div></td>
										<td><div id="cptProdSelected" class="criteresSelectedContratTr"></div></td>
										<td><div id="vipSelected" class="criteresSelectedContratTr"></div></td>
									</tr>
									<tr>
										<td><label class="marginleftPopup">Versement :</label></td>
									</tr>
									<tr class="criteresLast">
										<td><div id="versementsSelected" class="criteresSelectedVersementTr"></div></td>
									</tr>
									<tr>
										<td><label class="marginleftPopup">Affecté à :</label></td>
										<td><label class="marginleftPopup">A traiter par :</label></td>
										<td><label class="marginleftPopup">Traité par :</label></td>
									</tr>
									<tr class="criteresLast">
										<td><div id="affecteASelected" class="criteresSelected"></div></td>
										<td><div id="aTraiterParSelected" class="criteresSelected"></div></td>
										<td><div id="traiteParSelected" class="criteresSelected"></div></td>
									</tr>
								</table>
							</div>
							<div id="blocAPrime"  class="popupCriteres">
								<span class="souligne">Critères de sélection :</span></br>
								<div class="spaceVertical">
								<span>Périodes cochées dans la liste des périodes</span>
								</div>
								
							</div>
							<br/><br/>
							<div id="blocB"><span class="souligne">Nombre de périodes impactées :</span> <span id="blocBval" > ${nbPeriodesFiltrees}</span></div>
						</div>
						
                    <hr/>
                    <div id="divBlocagePeriode" >
                    <h3 class="titreSecondaire">Blocage des périodes :</h3>
                    <form id="fBlocagePeriodes" action="<c:url value="/ihm/declarations/changementEnMasse/blocage" />" onsubmit="return actionBlocageBeforeSubmit();" method="post">
                    <table class="marginleftPopup">
							<tr>
								<td class="alignrbPopup">
									<span>Bloquer les périodes :</span>
								</td>
								<td>
								<input type="radio" value="bloquer" name="bloquerPeriodesEnMasse" id="bloquerPeriodesEnMasse" class="spaceInputPopup" value="bloquer" onclick="supprimeAttribut('#flagBloquerPeriodesEnMasse','disabled');supprimeAttribut('#flagBloquerPeriodesEnMasse','style');">
								</td>
								<td><td>
							</tr>
							<tr>
								<td class="alignrbPopup">
								<span>Débloquer les périodes : </span>
								</td>
								<td>
									<input type="radio" value="debloquer" name="bloquerPeriodesEnMasse" id="debloquerPeriodesEnMasse" class="spaceInputPopup" value="debloquer" onclick="supprimeAttribut('#flagBloquerPeriodesEnMasse','disabled');supprimeAttribut('#flagBloquerPeriodesEnMasse','style');" >
								</td>
								<td>
									<button type="submit" id="flagBloquerPeriodesEnMasse" name="flagBloquerPeriodesEnMasse" class="spaceInputPopup" disabled="disabled">Valider</button>
								</td>
							</tr>
						</table>
						<!-- Les paramètres de recherches sont injectés via JS dans le champ parametresRecherche -->
						<input type="hidden" name="idsPeriodes" class="idsPeriodes" id="idsPeriodesBLocage" />
						<input type="hidden" name="nbPeriodes" class="nbPeriodes" id="nbPeriodesBLocage" />
						<input type="hidden" name="parametresRecherche" class="parametresRecherche" id="parametresRechercheBlocage" />
						<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
						</form>
                    </div>
                    <hr/>
                    
                    
                    <form  id="fChangementStatutEnMasse" action="<c:url value="/ihm/declarations/changementEnMasse/changementStatut" />" 
                    method="post">
                    <div id="divChangementStatutPeriode" >
                    <h3 class="titreSecondaire">Changer le statut des périodes :</h3>
                    </div>
	                    <table class="marginleftPopup">
	                    	<tr>
		                    	<td class = "alignrbPopup">
		                    		<span class="stylelabel" >Statut :</span> 
		                    	</td>
		                    	<td class = "main-gest">
			                    	<select id="listeStatuts" name="codeLibelleChangementDeStatutEnMasse" class="customCriteresGestionnaire" >
										<c:forEach var="item" items="${listesPopup.listeStatuts}">
											<option value="${item.code}">
												${item.libelleCourt}
											</option>	
										</c:forEach>
									</select>
		                    	</td>
		                    	<td class="alignbottom" style="padding-left:30px;">
	                   			 	<button class="spaceInputPopup" type="button"  
	                   			 	name="flagValiderChangementStatutEnMasse" id="flagEnregistrerChangementStatutEnMasse" value="Enregistrer" onclick="actionChangementStatutBeforeSubmit('flagValiderChangementStatutEnMasse','Enregistrer');" disabled="disabled">Enregistrer</button>
	                   			</td>
	                    	</tr>
	                    </table>
	                    <!-- Les paramètres de recherches sont injectés via JS dans le champ parametresRecherche -->
						<input type="hidden" name="idsPeriodes" class="idsPeriodes" id="idsPeriodesChangementDeStatutEnMasse"/>
						<input type="hidden" name="nbPeriodes" class="nbPeriodes" id="nbPeriodesChangementDeStatutEnMasse" />
						<input type="hidden" name="parametresRecherche" class="parametresRecherche" id="parametresRechercheChangementDeStatutEnMasse" />
	                    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                    </form>

                    <hr/>
                    <div id="divAttenteRetourPeriode">
                    <h3 class="titreSecondaire">Périodes en attente de retour entreprise :</h3>
                    
                    <form  id="fMiseEnAttentePeriodes" action="<c:url value="/ihm/declarations/changementEnMasse/miseEnAttente" />" 
                    onsubmit="return actionMiseAttenteBeforeSubmit();" method="post">
                    	<table class="marginleftPopup">
	                    	<tr>
	                    		<td class = "alignrbPopup">
	                    			<span class="stylelabel">Mettre en attente : </span>
	                    		</td>
	                    		<td>
	                    			<input class="spaceInputPopup" type= "radio" name="attenteRetourEntrepriseEnMasse" id = "miseEnAttente" value="attente" onclick="supprimeAttribut('#validerRetourEntreprise','disabled');supprimeAttribut('#validerRetourEntreprise','style');">
	                   	 		</td>
	                   	 		<td></td>
	                    	</tr>
	                    	<tr>
	                    
								<td class = "alignrbPopup">
							 		<span class="stylelabel">Enlever l'attente : </span>
								</td>
								<td>
									<input class="spaceInputPopup"type= "radio" name="attenteRetourEntrepriseEnMasse" id = "enleverAttente" value="enlever" onclick="supprimeAttribut('#validerRetourEntreprise','disabled');supprimeAttribut('#validerRetourEntreprise','style');">
								</td>
		    					<td>
		    						<button class="spaceInputPopup" type="submit"  id="validerRetourEntreprise"  name="validerRetourEntreprise" disabled="disabled">Valider</button>
		    			    	</td>
							</tr>
						</table>
						<!-- Les paramètres de recherches sont injectés via JS dans le champ parametresRecherche -->
						<input type="hidden" name="idsPeriodes" class="idsPeriodes" id="idsPeriodesMiseEnAttente"/>
						<input type="hidden" name="nbPeriodes" class="nbPeriodes" id="nbPeriodesMiseEnAttente" />
						<input type="hidden" name="parametresRecherche" class="parametresRecherche" id="parametresRechercheMiseEnAttente" />
						<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
					</form>
                    </div>
                    <hr/>
                   
                    <form  id="fAffecterAGestionnaire" action="<c:url value="/ihm/declarations/changementEnMasse/affecterA" />" 
                    onsubmit="return actionAffecterBeforeSubmit();" method="post">
                    <div id="divAffecterAPeriode" >
                    <h3 class="titreSecondaire">Affecter le responsable de gestion aux périodes :</h3>
                    	<table class="marginleftPopup">
	                    	<tr>
							  <td class = "main-gest aligntop">
							  	<label for="gestionnaireAffecteAEnMasse" class="stylelabel" >Gestionnaire :<br><span class="subTitre">(Affecté à)</span></label> 
							  	<select id="gestionnaireAffecteAEnMasse" name="gestionnaireAffecteAEnMasse" class="aligntop customCriteresGestionnaire">
									<c:forEach var="item" items="${listesPopup.listeUsersAffecteA}">
									
										<option value="${item.identifiantActiveDirectory}">
											<c:choose>
													<c:when test="${item.identifiantActiveDirectory == 'NON_AFFECTE'}">
													${item.nom}
													</c:when>
													<c:otherwise>
													${item.nom} ${item.prenom} ${item.identifiantActiveDirectory}
													</c:otherwise>
												</c:choose>	
										</option>	
									</c:forEach>
								</select>
							</td>
							<td class="aligntop">
							<button type="submit" class="spaceInputPopup" name="flagAffecterGestionnaireEnMasse" id="flagAffecterGestionnaireEnMasse" disabled="disabled" >Affecter</button>
							</td>	
						</tr>
						</table>
						<!-- Les paramètres de recherches sont injectés via JS dans le champ parametresRecherche -->
						<input type="hidden" name="idsPeriodes" class="idsPeriodes" id="idsPeriodesAffecterGestionnaire" />
						<input type="hidden" name="nbPeriodes" class="nbPeriodes" id="nbPeriodesAffecterGestionnaire" />
						<input type="hidden" name="parametresRecherche" class="parametresRecherche" id="parametresRechercheAffecterGestionnaire" />
						<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                    </div>
                    </form>
                    
                    <form  id="fAtraiterParGestionnaire" action="<c:url value="/ihm/declarations/changementEnMasse/ATraiterPar" />" 
                    onsubmit="return actionaTraiterParBeforeSubmit();" method="post">
                    <div id="divAssignerAPeriode" >
                    <h3 class="titreSecondaire">Assigner les périodes pour traitement :</h3>
                    </div>
                    <table class="marginleftPopup">
							<tr>							
									<td class = "main-gest aligntop">
									<label for="gestionnaireATraiterParEnMasse" class="stylelabel" >Gestionnaire :<br><span class="subTitre">(A traiter par)</span> </label>
									<select id="gestionnaireATraiterParEnMasse" name="gestionnaireATraiterParEnMasse" class="aligntop customCriteresGestionnaire" >
										<c:forEach var="item" items="${listesPopup.listeUsersATraiterPar}">
											<option value="${item.identifiantActiveDirectory}">
												<c:choose>
													<c:when test="${item.identifiantActiveDirectory == 'NON_ASSIGNE' }">
													${item.nom}
													</c:when>
													<c:otherwise>
													${item.nom} ${item.prenom} ${item.identifiantActiveDirectory}
													</c:otherwise>
												</c:choose>

											</option>			
										</c:forEach>
									</select>
								</td>
								<td class="aligntop">
							        <button type="submit" class="spaceInputPopup" name="flagAssignerGestionnaireEnMasse" id="flagAssignerGestionnaireEnMasse" disabled="disabled" >Assigner</button>
								</td>
							</tr>
						</table>
						<!-- Les paramètres de recherches sont injectés via JS dans le champ parametresRecherche -->
						<input type="hidden" name="idsPeriodes" class="idsPeriodes" id="idsPeriodesAssignerGestionnaire" />
						<input type="hidden" name="nbPeriodes" class="nbPeriodes" id="nbPeriodesAssignerGestionnaire" />
						<input type="hidden" name="parametresRecherche" class="parametresRecherche" id="parametresRechercheAssignerGestionnaire" />
						<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                    </form>
                    <hr />
                    <form  id="fCommentaireEnMasse" action="<c:url value="/ihm/declarations/changementEnMasse/commentaire" />" 
                    method="post">
                    <div id="divCommentairePeriode" >
                    <h3 class="titreSecondaire">Saisie de commentaire sur les périodes :</h3>
                    </div>
	                    <table class="marginleftPopup">
	                   		 <tr>
	                   			<td class="alignrbPopup">
	                    			<span class="stylelabel">Commentaire :</span>
	                   			 </td>
	                   			 <td class="spaceComVertical">
		    						<textarea id="commentaireEnMasse" name="commentaireEnMasse" cols="40" rows="5" maxlength="1000"></textarea>
	                   			 </td>
	                   			 <td class="alignbottom">
	                   			 	<button class="spaceInputPopup" type="button"  
	                   			 	name="flagValiderCommentaireEnMasse" id="flagEnregistrerCommentaireEnMasse" value="Enregistrer" onclick="actionCommentaireBeforeSubmit('flagValiderCommentaireEnMasse','Enregistrer');">Enregistrer</button>
	                   			 	
	                   			 </td>
	                    	 </tr>
	                    </table>
	                    <!-- Les paramètres de recherches sont injectés via JS dans le champ parametresRecherche -->
						<input type="hidden" name="idsPeriodes" class="idsPeriodes" id="idsPeriodesCommentaireEnMasse"/>
						<input type="hidden" name="nbPeriodes" class="nbPeriodes" id="nbPeriodesCommentaireEnMasse" />
						<input type="hidden" name="parametresRecherche" class="parametresRecherche" id="parametresRechercheCommentaireEnMasse" />
	                    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                    </form>
                </div>
            </div>
        </div>
    </div>
</body>
		<script type="text/javascript" src="<c:url value="/ressources/libs/selectize.js-0.12.4/dist/js/standalone/selectize.min.js?v=${version}" />"></script>
		<script type="text/javascript" src="<c:url value="/ressources/js/declarations/popup.js?v=${version}" />"></script>
</html>