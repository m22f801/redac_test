<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <%@include file="../communs/PageMetadata.jsp" %>
	<link rel="stylesheet" href="<c:url value="/ressources/css/declarations.css" />"></link>
</head>
<body>

   	<div class="appWrapper">
	   	<%@include file="../communs/Header.jsp" %>
	   	<div class="bodyWrapper">
	    	<%@include file="../communs/Menu.jsp" %>
	    	<div class="content declaration">
	    		<jsp:include page="DeclarationNavigation.jsp" flush="true">
					<jsp:param name="page" value="dsn" />
				</jsp:include>
	    		<div class="detailsPeriodeWrapper dsnPeriodeWrapper">
    				<h1>
    					Déclarations reçues par les différents établissements :
    				</h1>
    				<table class="donneesConsultation dsnPeriode">
    					<thead>
    						<tr>
    							<td><span>Etablissement \ Période</span></td>
    							<c:forEach items="${dsn.moisPeriode}" var="mois">
							    	<fmt:parseDate value="${mois}" var="moisParse" pattern="yyyyMMdd" />
	    							<td><span><fmt:formatDate value="${moisParse}" pattern="MM/yyyy" /></span></td>
    							</c:forEach>
    						</tr>
    					</thead>
    					<tbody>
   							<c:forEach items="${dsn.adhesionsEtablissements}" var="adhEtab">
								<tr>
									<td>${adhEtab.value['sirenNicLocalite']}</td>
	    							<c:forEach items="${dsn.moisPeriode}" var="mois">
		    							<td>
		    							    <c:set var="moisAsString">${mois}</c:set>
		    							    <c:set var="lienMois">${mois}-lien</c:set>
			    							<c:if test="${not empty adhEtab.value[moisAsString]}">
						    					<fmt:parseDate value="${adhEtab.value[moisAsString]}" var="moisFichierParse" pattern="yyyyMMdd" />
				    							<span>Le <fmt:formatDate value="${moisFichierParse}" pattern="dd/MM/yyyy" /></span><br/>
				    							
				    							<c:if test="${not empty adhEtab.value[lienMois]}">
				    								<span><a target="_blank" href="${adhEtab.value[lienMois]}">&gt; Consulter</a></span>
				    							</c:if>
				    						</c:if>
			    							<c:if test="${empty adhEtab.value[moisAsString] && not empty dsn.moisAvecDeclaration[mois]}">
			    								<div class="alerteAvertissement"></div>
				    						</c:if>
			    							<c:if test="${empty adhEtab.value[moisAsString] && empty dsn.moisAvecDeclaration[mois]}">
			    								<div class="alerteBloquante"></div>
				    						</c:if>
			    						</td>
	    							</c:forEach>
								</tr>
   							</c:forEach>
    					</tbody>
    				</table>
	    		</div>
		    	<div class="outilsPeriode">
	    			<button class="navRecherche" type="button" onclick="location.href='<c:url value="/ihm/declarations?${contexte.rechercheUrlQueryString}" />'">&lt;&lt; Retour à la liste</button>
	    		</div>
	    	</div>
	   	</div>
	   	<%@include file="../communs/Footer.jsp" %>
   	</div>
   	
</body>
</html>