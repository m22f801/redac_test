<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>
<head>
    <%@include file="../communs/PageMetadata.jsp" %>
	<link rel="stylesheet" href="<c:url value="/ressources/css/declarations.css" />"></link>
</head>
<body>

   	<div class="appWrapper">
	   	<%@include file="../communs/Header.jsp" %>
	   	<div class="bodyWrapper">
	    	<%@include file="../communs/Menu.jsp" %>
	    	<div class="content declaration">
	    		<jsp:include page="DeclarationNavigation.jsp" flush="true">
					<jsp:param name="page" value="synthese" />
				</jsp:include>
	    		<div class="detailsPeriodeWrapper synthesePeriodeWrapper">
	   				<h1>
	   					Contrat / Entreprise :
	   				</h1>
	   				<div class="infoSynthese row">
		   				<div class="blockInfoSynthese col-md-6">
		   					<div class="info row">
		   						<label class="col-xs-6">Contrat :</label>
		   						<span class="col-xs-6">${contrat.numContrat}</span>
		   					</div>
		   					<div class="info row">
		   						<label class="col-xs-6">Gestion :</label>
		   						<span class="col-xs-6"><c:out value="${infoSynthese.gestion}" /></span>
		   					</div>
		   					<div class="info row">
		   						<label class="col-xs-6">Groupe de gestion :</label>
		   						<span class="col-xs-6"><c:out value="${contrat.groupeGestion}" /></span>
		   					</div>
		   					<div class="info row">
		   						<label class="col-xs-6">Famille :</label>
		   						<span class="col-xs-6"><c:out value="${infoSynthese.famille}" /></span>
		   					</div>
		   					<div class="info row">
		   						<label class="col-xs-6">Nature du contrat :</label>
		   						<span class="col-xs-6"><c:out value="${infoSynthese.natureContrat}" /></span>
		   					</div>
		   					<div class="info row">
		   						<label class="col-xs-6">Compte encaisseur :</label>
		   						<span class="col-xs-6"><c:out value="${contrat.compteEncaissement}" /></span>
		   					</div>
		   					<div class="info row">
		   						<label class="col-xs-6">Date d'effet du contrat :</label>
		    					<span><fmt:formatDate value="${contrat.dateEffetContratAsDate}" pattern="dd/MM/yyyy" /></span>
		   					</div>
		   					<div class="info row">
		   						<label class="col-xs-6 infoMessageControle ${infoSynthese.niveauResiliation == 'SIGNAL' ? 'signal' : infoSynthese.niveauResiliation == 'REJET' ? 'rejet' : ''}" >Date de résiliation :</label>
		   						<span class="col-xs-6"><c:out value="${infoSynthese.dateResiliation}" /></span>
		   					</div>
		   					<div class="lienRapide">
		   						<c:choose>
				   					<c:when test="${utilisateur.roleGestionCde && infoSynthese.extensionContrat != null}">
			   							<a href="<c:url value="/ihm/extensionscontrats/${infoSynthese.extensionContrat.numContrat}/?criteresRecherche=numContrat=${infoSynthese.extensionContrat.numContrat}" />"><span>&gt; Consulter paramètre contrat</span></a>
				   					</c:when>
				   					<c:otherwise>
			   							<a href="#" class="inactive"><span>&gt; Consulter paramètre contrat</span></a>
				   					</c:otherwise>
		   						</c:choose>
		   					</div>
		   				</div>
		   				<div class="blockInfoSynthese col-md-6">
		   					<div class="infoEntreprise souscripteur">
			   					<div class="info row">
			   						<label class="col-xs-6">SIREN/SIRET du déclaré :</label>
			   						<span class="col-xs-6"><c:out value="${contrat.client.numSiren}" />&nbsp;<c:out value="${contrat.client.numSiret}" /></span>
			   					</div>
			   					<div class="info row">
			   						<label class="col-xs-6">Raison sociale du déclaré :</label>
			   						<span class="col-xs-6"><c:out value="${contrat.client.raisonSociale}" /></span>
			   					</div>
			   					<div class="lienRapide">
			   						<c:choose>
			   							<c:when test="${not empty contrat.client.numSiret}">
			   								<a target="_blank" href="${urlBriqueDSN}rechercherConsultationSyntheseEntreprise.xhtml?siret=${contrat.client.numSiren}${contrat.client.numSiret}&moisPaieDeclare=${moisDeclare}"><span>&gt; Consulter entreprise/établissement</span></a>
			   							</c:when>
			   							<c:otherwise>
			   								<a target="_blank" href="${urlBriqueDSN}rechercherConsultationSyntheseEntreprise.xhtml?siret=${contrat.client.numSiren}&moisPaieDeclare=${moisDeclare}"><span>&gt; Consulter entreprise/établissement</span></a>
			   							</c:otherwise>
			   						</c:choose>
			   					</div>
		   					</div>
		   					
		   					<c:forEach items="${infoSynthese.etablissementsLies}" var="etablissement">
			   					<div class="infoEntreprise">
				   					<div class="info row">
				   						<label class="col-xs-6">SIREN/SIRET du déclaré :</label>
				   						<span class="col-xs-6"><c:out value="${etablissement.sirenEntreprise}" />&nbsp;<c:out value="${etablissement.nicEtablissement}" /></span>
				   					</div>
				   					<div class="info row">
				   						<label class="col-xs-6">Raison sociale du déclaré :</label>
				   						<span class="col-xs-6"><c:out value="${etablissement.raisonSociale}" /></span>
				   					</div>
				   					<div class="lienRapide">
				   						<a target="_blank" href="${urlBriqueDSN}rechercherConsultationSyntheseEntreprise.xhtml?siret=${etablissement.sirenEntreprise}${etablissement.nicEtablissement}&moisPaieDeclare=${moisDeclare}"><span>&gt; Consulter entreprise/établissement</span></a>
				   					</div>
			   					</div>
		   					</c:forEach>
		   				</div>
	   				</div>
	   				<div class="row">
		   				<h1 class="col-md-6">
		   					Détail de la période :
		   				</h1>
		   				<div class="outilsInfoSynthese col-md-6">
		   					<div class="exportExcel">
			    				<div class="exportExcelIcone"></div>
			    				<span>
			   						<c:choose>
					   					<c:when test="${utilisateur.droitIndividu}">
						    				<a target="_blank" href="<c:url value="/ihm/declarations/${declaration.idPeriode}/synthese/exports/excel" />">Extraire la synthèse pour cette période  en Excel</a>
					   					</c:when>
					   					<c:otherwise>
				   							<a href="#" class="inactive">Extraire la synthèse pour cette période  en Excel</a>
					   					</c:otherwise>
			   						</c:choose>
			    				</span>
			    			</div>
		   				</div>
		   			</div>
	   				<div class="infoSynthese row">
		   				<div class="blockInfoSynthese col-md-6">
		   					<div class="info row">
		   						<label class="col-xs-6">Début de période :</label>
		    					<span><fmt:formatDate value="${periode.dateDebutPeriodeAsDate}" pattern="dd/MM/yyyy" /></span>
		   					</div>
		   					<div class="info row">
		   						<label class="col-xs-6">Fin de période :</label>
		    					<span><fmt:formatDate value="${periode.dateFinPeriodeAsDate}" pattern="dd/MM/yyyy" /></span>
		   					</div>
		   					<div class="info row">
		   						<label class="col-xs-6">Statut :</label>
		    					<span><c:out value="${infoSynthese.statut}" /></span>
		   					</div>
		   					<div class="info row">
		   						<label class="col-xs-6">Nature :</label>
		    					<span><c:out value="${infoSynthese.nature}" /></span>
		   					</div>
		   					<div class="info row">
		   						<label class="col-xs-6">Date de création :</label>
		    					<span><fmt:formatDate value="${periode.dateCreationAsDate}" pattern="dd/MM/yyyy" /></span>
		   					</div>
		   					<div class="info row">
		   						<label class="col-xs-6">V.I.P. :</label>
		    					<span>${infoSynthese.vip}</span>
		   					</div>
		   					<div class="info row">
		   						<label class="col-xs-6">Date réception dernière DSN :</label>
		    					<span><fmt:formatDate value="${infoSynthese.dateDerniereReceptionDSN}" pattern="dd/MM/yyyy" /></span>
		   					</div>
		   					<div class="info row">
		   						<label class="col-xs-6">Date d'intégration dans le S.I. :</label>
		    					<span><fmt:formatDate value="${infoSynthese.compteRenduIntegration != null ? infoSynthese.compteRenduIntegration.dateTraitementAsDate : null}" pattern="dd/MM/yyyy" /></span>
		   					</div>
		   				</div>
		   				<div class="blockInfoSynthese col-md-6">
		   					<div class="info row">
		   						<label class="col-xs-6">Date première DSN :</label>
		    					<span><fmt:formatDate value="${infoSynthese.indicateursDsn != null ? infoSynthese.indicateursDsn.dateDebutReceptionDSNAsDate : null}" pattern="dd/MM/yyyy" /></span>
		   					</div>
		   					<div class="info row">
		   						<label class="col-xs-6">Exploitation DSN :</label>
		   						<span>
		   							<c:if test="${infoSynthese.indicateursDsn != null}">
				   						<c:choose>
				   							<c:when test="${infoSynthese.indicateursDsn.exploitationDSN}">
				   								OUI, depuis le <fmt:formatDate value="${infoSynthese.indicateursDsn.dateDebutExploitationDSNAsDate}" pattern="dd/MM/yyyy" />
				   							</c:when>
				   							<c:otherwise>
				   								NON
				   							</c:otherwise>
				   						</c:choose>
				   					</c:if>
		    					</span>
		   					</div>
		   					<div class="info row">
		   						<label class="col-xs-6">Edit° consigne paiemt :</label>
		   						<span>
		   							<c:if test="${infoSynthese.indicateursDsn != null}">
				   						<c:choose>
				   							<c:when test="${infoSynthese.indicateursDsn.editionConsignesPaiementDSN}">
				   								OUI, depuis le <fmt:formatDate value="${infoSynthese.indicateursDsn.dateDebutEditionConsignesPaiementAsDate}" pattern="dd/MM/yyyy" />
				   							</c:when>
				   							<c:otherwise>
				   								NON
				   							</c:otherwise>
				   						</c:choose>
				   					</c:if>
		    					</span>
		   					</div>
		   					<div class="info row">
		   						<label class="col-xs-6">Mode gestion DSN :</label>
		   						<span>
		   							<c:if test="${infoSynthese.indicateursDsn != null}">
		   								<c:out value="${infoSynthese.indicateursDsn.modeGestionDSN}" />, depuis le <fmt:formatDate value="${infoSynthese.indicateursDsn.dateDebutModeGestionDSNAsDate}" pattern="dd/MM/yyyy" />
		   							</c:if>
		   						</span>
		   					</div>
		   					<div class="info row">
		   						<label class="col-xs-6">Transfert DSN :</label>
		   						<span>
		   							<c:if test="${infoSynthese.indicateursDsn != null}">
				   						<c:choose>
				   							<c:when test="${infoSynthese.indicateursDsn.transfertDSN}">
				   								OUI, depuis le <fmt:formatDate value="${infoSynthese.indicateursDsn.dateDebutTransfertDSNAsDate}" pattern="dd/MM/yyyy" />
				   							</c:when>
				   							<c:otherwise>
				   								NON
				   							</c:otherwise>
				   						</c:choose>
				   					</c:if>
		    					</span>
		   					</div>
		   					<div class="info row">
		   						<label class="col-xs-6">Dernière modif :</label>
		   						<span>
		   							<c:if test="${infoSynthese.indicateursDsn != null && (infoSynthese.indicateursDsn.dateModifIndicateursAsDate != null || infoSynthese.indicateursDsn.gestionnaireModifIndicateurs != null)}">
				   						<fmt:formatDate value="${infoSynthese.indicateursDsn.dateModifIndicateursAsDate}" pattern="dd/MM/yyyy" />, par <c:out value="${infoSynthese.indicateursDsn.gestionnaireModifIndicateurs}" />
				   					</c:if>
		    					</span>
		   					</div>
		   					<div class="info row">
		   						<label class="col-xs-6">Motif du changt :</label>
		   						<span>
		   							<c:if test="${infoSynthese.indicateursDsn != null}"><c:out value="${infoSynthese.indicateursDsn.motifChangementIndicateurs}" /></c:if>
		   						</span>
		   					</div>
		   				</div>
		   			</div>
	   				<h1>
	   					Contenu des déclarations :
	   				</h1>
	   				<div class="infoSynthese row">
		   				<div class="blockInfoSynthese col-md-6">
		   					<div class="info row">
		   						<label class="col-xs-6 infoMessageControle ${infoSynthese.niveauNbEtablissements == 'SIGNAL' ? 'signal' : infoSynthese.niveauNbEtablissements == 'REJET' ? 'rejet' : ''}">Nb Etablissements :</label>
		    					<span class="col-xs-6"><c:out value="${infoSynthese.nbEtablissements}" /></span>
		   					</div>
		   					<div class="info row">
		   						<label class="col-xs-6">Nb Contrats Travail :</label>
		    					<span class="col-xs-6"><c:out value="${infoSynthese.nbContratsTravail}" /></span>
		   					</div>
		   					<div class="info row">
		   						<label class="col-xs-6">Nb Fins Contrat :</label>
		    					<span class="col-xs-6"><c:out value="${infoSynthese.nbFinsContrat}" /></span>
		   					</div>
		   				</div>
		   				<div class="blockInfoSynthese col-md-6">
		   					<div class="info row">
		   						<label class="col-xs-6 infoMessageControle ${infoSynthese.niveauNbSalaries == 'SIGNAL' ? 'signal' : infoSynthese.niveauNbSalaries == 'REJET' ? 'rejet' : ''}">Nb de Salariés :</label>
		    					<span class="col-xs-6"><c:out value="${infoSynthese.nbSalaries}" /></span>
		   					</div>
		   				</div>
		   			</div>
		   			
		   			<c:if test="${infoSynthese.natureContratPourPeriodeMinimalNoCat == 1 && infoSynthese.existeBaseAssujetiePourPeriode && infoSynthese.existeCategorieQuittancementIndividuPourPeriode}">
		   				<!-- Consolidation sur effectif -->
		   				<h1>
		   					Consolidation des déclarations sur effectif :
		   				</h1>
		   				<table class="donneesConsultation synthesePeriode">
	    					<thead>
	    						<tr>
	    							<td><span>Population :</span></td>
	    							<c:forEach items="${infoSynthese.consolidation.numerosCategorieQuittancement}" var="numCategorie">
		    							<td><span>${numCategorie}</span></td>
	    							</c:forEach>
	    							<td><span>Total</span></td>
	    						</tr>
	    					</thead>
	    					<tbody>
	    						<fmt:setLocale value="fr_FR"/>
	    						<tr>
	    							<td><span>Montant calculé cotisation :</span></td>
	    							<c:forEach items="${infoSynthese.consolidation.montantsCotisations}" var="montantCotisation">
		    							<td><span><fmt:formatNumber type="currency" maxFractionDigits="2" currencySymbol="€" value="${montantCotisation}" /></span></td>
	    							</c:forEach>
	    							<td><span><fmt:formatNumber type="currency" maxFractionDigits="2" currencySymbol="€" value="${infoSynthese.consolidation.montantTotalCotisations}" /></span></td>
	    						</tr>
	    						<tr>
	    							<td><span>Effectifs calculés Début :</span></td>
	    							<c:forEach items="${infoSynthese.consolidation.effectifsDebut}" var="effectif">
			    							<td><span>${effectif}</span></td>
	    							</c:forEach>
	    							<td><span>${infoSynthese.consolidation.effectifTotalDebut}</span></td>
	    						</tr>
	    						<c:forEach items="${infoSynthese.consolidation.mouvements}" var="mouvement">
		    						<tr>
		    							<td><span>${mouvement.libelle} :</span></td>
		    								<c:forEach items="${infoSynthese.consolidation.numerosCategorieQuittancement}" var="numCategorie">
		    									<c:if test="${mouvement.nombres[numCategorie] <= 0}">
			    								<td><span>${mouvement.nombres[numCategorie]}</span></td>
			    								</c:if>
			    								<c:if test="${mouvement.nombres[numCategorie] > 0}">
			    								<td><span>+${mouvement.nombres[numCategorie]}</span></td>
			    								</c:if>
		    								</c:forEach>
		    							
		    							<c:if test="${mouvement.nombreTotal <= 0 }">
		    								<td><span>${mouvement.nombreTotal}</span></td>
		    							</c:if>
		    							<c:if test="${mouvement.nombreTotal > 0 }">
		    								<td><span>+${mouvement.nombreTotal}</span></td>
		    							</c:if>
		    						</tr>
		    					</c:forEach>
	    						<tr>
	    							<td><span>Effectifs calculés Fin :</span></td>
	    							<c:forEach items="${infoSynthese.consolidation.effectifsFin}" var="effectif">
		    							<td><span>${effectif}</span></td>
	    							</c:forEach>
	    							<td><span>${infoSynthese.consolidation.effectifTotalFin}</span></td>
	    						</tr>
	    					</tbody>
	    				</table>
		   			</c:if>
		   			
		   			
		   			<c:if test="${infoSynthese.natureContratPourPeriodeMinimalNoCat == 2 && infoSynthese.existeBaseAssujetiePourPeriode && infoSynthese.existeCategorieQuittancementIndividuPourPeriode}">
		   				<!-- Consolidation sur salaire -->
		   				<h1>
		   					Consolidation des déclarations sur salaire :
		   				</h1>
		   				<table class="donneesConsultation synthesePeriode">
	    					<thead>
	    						<tr>
	    							<td><span>Population :</span></td>
	    							<c:forEach items="${infoSynthese.consolidation.numerosCategorieQuittancement}" var="numCategorie">
		    							<td><span>${numCategorie}</span></td>
	    							</c:forEach>
	    							<td><span>Total</span></td>
	    						</tr>
	    					</thead>
	    					<tbody>
	    						<fmt:setLocale value="fr_FR"/>
	    						<tr>
	    							<td><span>Montant déclaré cotisation :</span></td>
	    							<c:forEach items="${infoSynthese.consolidation.montantsCotisations}" var="montantCotisation">
		    							<td><span><fmt:formatNumber type="currency" maxFractionDigits="2" currencySymbol="€" value="${montantCotisation}" /></span></td>
	    							</c:forEach>
	    							<td><span><fmt:formatNumber type="currency" maxFractionDigits="2" currencySymbol="€" value="${infoSynthese.consolidation.montantTotalCotisations}" /></span></td>
	    						</tr>
    							<c:forEach items="${infoSynthese.consolidation.tranches}" var="tranche">
		    						<tr>
		    							<td><span>${tranche.libelle} :</span></td>
		    							<c:forEach items="${infoSynthese.consolidation.numerosCategorieQuittancement}" var="numCategorie">
			    							<td><span><fmt:formatNumber type="currency" maxFractionDigits="2" currencySymbol="€" value="${tranche.montants[numCategorie]}" /></span></td>
		    							</c:forEach>
		    							<td><span><fmt:formatNumber type="currency" maxFractionDigits="2" currencySymbol="€" value="${tranche.montantTotal}" /></span></td>
		    						</tr>
    							</c:forEach>
	    					</tbody>
	    				</table>
		   			
		   			</c:if>
		   			
	   				<c:forEach items="${infoSynthese.cotisationsEtablissements}" var="cotisation">
		   				<h1>
		   					Cotisation établissement ${cotisation.adhesionEtablissementMois.sirenEntreprise} ${cotisation.adhesionEtablissementMois.nicEtablissement} : <label class="infoMessageControle ${infoSynthese.niveauAlertePresenceCotisation == 'SIGNAL' ? 'signal' : infoSynthese.niveauAlertePresenceCotisation == 'REJET' ? 'rejet' : ''}"></label>
		   				</h1>
		   				<div class="infoSynthese row">
			   				<div class="blockInfoSynthese col-md-6">
			   					<div class="info row">
			   						<label class="col-xs-6">Début période rattachement :</label>
			    					<span class="col-xs-6"><fmt:formatDate value="${cotisation.dateDebutPeriodeRattachementAsDate}" pattern="dd/MM/yyyy" /></span>
			   					</div>
			   					<div class="info row">
			   						<label class="col-xs-6">Fin période rattachement :</label>
			    					<span class="col-xs-6"><fmt:formatDate value="${cotisation.dateFinPeriodeRattachementAsDate}" pattern="dd/MM/yyyy" /></span>
			   					</div>
			   				</div>
			   				<div class="blockInfoSynthese col-md-6">
			   					<div class="info row">
			   						<label class="col-xs-6">Montant de cotisation :</label>
			    					<span class="col-xs-6"><fmt:formatNumber type="currency" maxFractionDigits="2" currencySymbol="€" value="${cotisation.valeurCotisation}" /></span>
			   					</div>
			   					<div class="info row">
			   						<label class="col-xs-6">Code cotisation :</label>
			    					<span class="col-xs-6">${cotisation.codeCotisation} <c:out value="${cotisation.libelleCodeCotisation}" /></span>
			   					</div>
			   				</div>
			   			</div>
	   				</c:forEach>		   			
		   			
	   				<c:forEach items="${infoSynthese.versements}" var="versement">
		   				<h1>
		   					Versement ${versement.idVersement} :
		   				</h1>
		   				<div class="infoSynthese row">
			   				<div class="blockInfoSynthese col-md-6">
			   					<div class="info row">
			   						<label class="col-xs-6">Entité d'affectation :</label>
			    					<span class="col-xs-6"><c:out value="${versement.entiteAffectation}" /></span>
			   					</div>
			   					<div class="info row">
			   						<label class="col-xs-6">Début période rattachement :</label>
			    					<span class="col-xs-6">${infoSynthese.infoVersements[versement.idVersement].dateDebutPeriodeRattachement}</span>
			   					</div>
			   					<div class="info row">
			   						<label class="col-xs-6">Fin période rattachement :</label>
			    					<span class="col-xs-6">${infoSynthese.infoVersements[versement.idVersement].dateFinPeriodeRattachement}</span>
			   					</div>
			   					<div class="info row">
			   						<label class="col-xs-6">SIRET payeur :</label>
			    					<span class="col-xs-6">${fn:substring(versement.siretPayeur, 0, 9)}&nbsp;${fn:substring(versement.siretPayeur, 9, 14)}</span>
			   					</div>
			   					<div class="info row">
			   						<label class="col-xs-6">Délégataire gestion :</label>
			    					<span class="col-xs-6"><c:out value="${versement.codeDelegataireGestion}" /></span>
			   					</div>
			   					<div class="info row">
			   						<label class="col-xs-6">Identifiant du fonds :</label>
			    					<span class="col-xs-6"><c:out value="${versement.codeIdentifiantFonds}" /></span>
			   					</div>
			   				</div>
			   				<div class="blockInfoSynthese col-md-6">
			   					<div class="info row">
			   						<label class="col-xs-6">BIC :</label>
			    					<span class="col-xs-6"><c:out value="${versement.bic}" /></span>
			   					</div>
			   					<div class="info row">
			   						<label class="col-xs-6">IBAN :</label>
			    					<span class="col-xs-6">${fn:substring(versement.iban, 0, 4)}&nbsp;${fn:substring(versement.iban, 4, 8)}&nbsp;${fn:substring(versement.iban, 8, 12)}&nbsp;${fn:substring(versement.iban, 12, 16)}&nbsp;${fn:substring(versement.iban, 16, 20)}&nbsp;${fn:substring(versement.iban, 20, 24)}&nbsp;${fn:substring(versement.iban, 24, 27)}</span>
			   					</div>
			   					<div class="info row">
			   						<label class="col-xs-6">Montant versement :</label>
				    				<span class="col-xs-6"><fmt:formatNumber type="currency" maxFractionDigits="2" currencySymbol="€" value="${versement.montantVersement}" /></span>
			   					</div>
			   					<div class="info row">
			   						<label class="col-xs-6">Date paiement :</label>
			    					<span class="col-xs-6">${versement.datePaiementSansFormatage}</span>
			   					</div>
			   					<div class="info row">
			   						<label class="col-xs-6 infoMessageControle ${infoSynthese.niveauModePaiement == 'SIGNAL' ? 'signal' : infoSynthese.niveauModePaiement == 'REJET' ? 'rejet' : ''}">Mode paiement :</label>
			    					<span class="col-xs-6"><c:out value="${versement.libelleModePaiement}" /></span>
			   					</div>
			   					<div class="info row">
			   						<label class="col-xs-6">Réf.paiement calculée :</label>
			    					<span class="col-xs-6"><c:out value="${versement.referencePaiement}" /></span>
			   					</div>
			   				</div>
			   			</div>
			   			
		   				<table class="donneesConsultation synthesePeriode">
	    					<thead>
	    						<c:if test="${fn:length(infoSynthese.infoVersements[versement.idVersement].periodesAffectation) gt 0}">
		    						<tr>
		    							<td><span>&nbsp;</span></td>
		    							<td><span>Population :</span></td>
		    							<c:forEach items="${infoSynthese.infoVersements[versement.idVersement].typesPopulationDisctincts}" var="typePopulation">
			    							<td><span>${typePopulation}</span></td>
		    							</c:forEach>
		    						</tr>
								</c:if>
	    					</thead>
	    					<tbody>
	    						<c:forEach items="${infoSynthese.infoVersements[versement.idVersement].periodesAffectation}" var="periodeAffectation">
		    						<tr>
		    							<td rowspan="2"><label class="infoMessageControle ${periodeAffectation.value.niveauAlertePeriodeInvalide == 'SIGNAL' ? 'signal' : periodeAffectation.value.niveauAlertePeriodeInvalide == 'REJET' ? 'rejet' : ''}">&nbsp;</label><span>Période d'affectation ${periodeAffectation.key} :</span></td>
		    							<td><span>Montant :</span></td>
		    							<c:forEach items="${infoSynthese.infoVersements[versement.idVersement].typesPopulationDisctincts}" var="typePopulation">
			    							<td><span><fmt:formatNumber type="currency" maxFractionDigits="2" currencySymbol="€" value="${periodeAffectation.value.montantsVerses[typePopulation]}" /></span></td>
		    							</c:forEach>
		    						</tr>
		    						<tr>
		    							<td><span>Id sous-fonds :</span></td>
		    							<c:forEach items="${infoSynthese.infoVersements[versement.idVersement].typesPopulationDisctincts}" var="typePopulation">
			    							<td><span>${periodeAffectation.value.idsSousFonds[typePopulation]}</span></td>
		    							</c:forEach>
		    						</tr>
	    						</c:forEach>
	    					</tbody>
	    				</table>
	   				</c:forEach>
		   			
	    		</div>
		    	<div class="outilsPeriode">
	    			<button class="navRecherche" type="button" onclick="location.href='<c:url value="/ihm/declarations?${contexte.rechercheUrlQueryString}" />'">&lt;&lt; Retour à la liste</button>
	    		</div>
	    	</div>
	   	</div>
	   	<%@include file="../communs/Footer.jsp" %>
   	</div>
   	
</body>
</html>