<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>
<head>
    <%@include file="../communs/PageMetadata.jsp" %>
	<link rel="stylesheet" href="<c:url value="/ressources/css/extensionscontrats.css" />"></link>
	<script language="javascript" type="text/javascript">  
		function SetFocus(InputID)  
		{  
			document.getElementById(InputID).focus();
			setTimeout(function() { document.getElementById(InputID).focus(); }, 10);			   
		}  
	</script>  
</head>  
<body onload="SetFocus('dateEffet')"> 
   	<div class="appWrapper">
	   	<%@include file="../communs/Header.jsp" %>
	   	<div class="bodyWrapper">
	    	<%@include file="../communs/Menu.jsp" %>
	    	<div class="content contrat details">
	    		<div class="titreContenu">Caractéristiques du Contrat ${contrat.numContrat}</div>
	    		<div class="informations" id="informations">
	    			<span>${fn:replace(listeErreurs, ';', '<br/>')}</span>
	    		</div>
	    		
	    		<div class="recapitulatifWrapper">
	    				<h1>
	    					Récapitulatif Contrat S.I. aval :
	    				</h1>
	    				<table>
	    					<tr>
	    						<td>
	    							Contrat :
	    						</td>
	    						<td>
	    							<label>${contrat.numContrat}</label>
	    						</td>
	    						<td>
	    							Groupe de gestion :
	    						</td>
	    						<td>
	    							<label>${libellesContrat.groupeGestion}</label>
	    						</td>
	    					</tr>
	    					<tr>
	    						<td>
	    							Gestion :
	    						</td>
	    						<td>
	    							<label><c:out value="${contrat.libelleGestion}" /></label>
	    						</td>
	    						<td>
	    							Famille :
	    						</td>
	    						<td>
	    							<label><c:out value="${libellesContrat.famille}" /></label>
	    						</td>
	    					</tr>
	    					<tr>
	    						<td>
	    							Compte encaisseur :
	    						</td>
	    						<td>
	    							<label><c:out value="${contrat.compteEncaissement}" /></label>
	    						</td>
	    						<td>
	    							Périodicité contrat :
	    						</td>
	    						<td>
	    							<label><c:out value="${libellesContrat.periodicite}" /></label>
	    						</td>
	    					</tr>
	    					<tr>
	    						<td>
	    							Nature du contrat : 
	    						</td>
	    						<td>
	    							<label><c:out value="${libellesContrat.nature}" /></label>
	    						</td>
	    						<td>
	    						</td>
	    						<td>
	    						</td>
	    					</tr>
	    					<tr>
	    						<td>
	    							Date d'effet du contrat : 
	    						</td>
	    						<td>
							    	<c:if test="${not empty contrat}">
	    								<label><fmt:formatDate value="${contrat.dateEffetContratAsDate}" pattern="dd/MM/yyyy" /></label>
							    	</c:if>
	    						</td>
	    						<td>
	    							N° Client :
	    						</td>
	    						<td>
	    							<label><c:out value="${contrat.numSouscripteur}" /></label>
	    						</td>
	    					</tr>
	    					<tr>
	    						<td>
	    							Date de résiliation :
	    						</td>
	    						<td>
							    	<c:if test="${not empty contrat}">
		    							<label><fmt:formatDate value="${champsContrat.dateResiliation}" pattern="dd/MM/yyyy" /></label>
		    						</c:if>
	    						</td>
	    						<td>
	    							SIREN/SIRET du Client :
	    						</td>
	    						<td>
	    							<label><c:out value="${champsContrat.souscripteurNumSiret}" /></label>
	    						</td>
	    					</tr>
	    					<tr>
	    						<td>
	    							Eligibilité DSN :
	    						</td>
	    						<td>
							    	<c:if test="${not empty contrat}">
		    							<label><c:out value="${libellesContrat.eligibilite}" />, depuis le <fmt:formatDate value="${contrat.dateDerniereModifSituationLigneAsDate}" pattern="dd/MM/yyyy" /></label>
	    							</c:if>
	    						</td>
	    						<td>
	    							Raison sociale du Client :
	    						</td>
	    						<td>
	    							<label><c:out value="${champsContrat.souscripteurRaisonSociale}" /></label>
	    						</td>
	    					</tr>
	    				</table>
	    				
	    				<table>
	    					<tr>
	    						<td>
						    		<div class="lienSpecifique">
						    			<a href="<c:url value="/ihm/declarations?numContrat=" />${contrat.numContrat}" >> Consulter les périodes en cours de ce contrat</a>
						    		</div>
	    						</td>
	    						<td>
						    		<div class="boutonSpecifique">
	    								<form id="suppressionFormulaire" name="suppressionFormulaire" method="post">
							    			<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
							    			<button type="button" id="boutonSuppresion" class="boutonDetails" onclick="afficheMessageSuppression('<c:url value="/ihm/extensionscontrats/suppression/${details.numeroContrat}?${contexte.rechercheUrlQueryString}" />')">Supprimer les spécificités de ce contrat</button>
	    								</form>	
							    		<input type="hidden" id="roleGestionCde" name="roleGestionCde" value="${utilisateur.roleGestionCde}"/>
							    		<input type="hidden" id="habilitationVIP" name="habilitationVIP" value="${habilitationVIP}"/>
							    		<input type="hidden" id="contratVIP" name="contratVIP" value="${contratVIP}"/>
							    		<input type="hidden" id="extensionContratExiste" name="extensionContratExiste" value="${extensionContratExiste}"/>
						    		</div>
	    						</td>
	    					</tr>
	    				</table>
	    				
	    			<form id="detailFormulaire" name="detailFormulaire" class="changementStatutPeriode" method="post"  accept-charset="ISO-8859-1">
	    				<h1>
	    					Paramétrage DSN pour ce contrat :
	    				</h1>
	    				<table>
	    					<tr>
	    						<td>
	    							Date de dernière modif :&nbsp;
	    						</td>
	    						<td>
	    							<c:if test="${empty details.dateDerniereModificationIndicateurs}">
	    								<label></label>
			    					</c:if>
		    						<c:if test="${not empty details.dateDerniereModificationIndicateurs}">
	    								<label><c:out value="${details.dateDerniereModificationIndicateurs}" /></label>
			    					</c:if>
	    						</td>
	    						<td>
	    							Modifs réalisées par :&nbsp;
	    						</td>
	    						<td>
	    							<c:if test="${empty details.gestionnaireDerniereModificationIndicateurs}">
	    								<label></label>
			    					</c:if>
		    						<c:if test="${not empty details.gestionnaireDerniereModificationIndicateurs}">
		    							<label><c:out value="${details.gestionnaireDerniereModificationIndicateurs}" /></label>
			    					</c:if>
	    						</td>
	    					</tr>
	    					<tr class="textAreaLigne">
	    						<td>
	    							Date début effet des paramètres : <span class="obligatoire">*&nbsp;</span> 
	    						</td>
	    						<td class="value">
	    							<c:if test="${empty details.dateEffet}">
		    							<input id="dateEffet" name="dateEffet" maxlength="10" type="text" value="" tabindex="1" />
			    					</c:if>
		    						<c:if test="${not empty details.dateEffet}">
		    							<input id="dateEffet" name="dateEffet" maxlength="10" type="text" value="${details.dateEffet}" tabindex="1" />
			    					</c:if>
	    						</td>
	    						<td>
	    							Motif du changement :&nbsp; 
	    						</td>
	    						<td class="value" rowspan="2">
	    							<textarea id="commentaireUtilisateur" name="commentaireUtilisateur" cols="50" rows="2" maxlength="1000" tabindex="2" ><c:out value="${details.commentaireUtilisateur}" /></textarea>
	    						</td>
	    					</tr>
	    					<tr>
	    						<td>
	    							Exploitation DSN : <span class="obligatoire">*&nbsp;</span> 
	    						</td>
	    						<td class="value">
	    							<select id="indicExploitationSaisi" name="indicExploitationSaisi" tabindex="3" >
										<c:forEach var="item" items="${reference.listeIndicsExploitation}">
											<option value="${item.code}" ${details.indicExploitationSaisi == item.code ? 'selected=\'selected\'' : ''}>${item.libelleCourt}</option>
										</c:forEach>
	    							</select>
	    						</td>
	    						<td>
	    						</td>
	    					</tr>	    				
	    					<tr>
	    						<td>
	    							Edit° consigne paiemt : <span class="obligatoire">*&nbsp;</span> 
	    						</td>
	    						<td class="value">
	    							<select id="indicConsignePaiementSaisi" name="indicConsignePaiementSaisi" tabindex="4" >
										<c:forEach var="item" items="${reference.listeIndicsConsignePaiement}">
											<option value="${item.code}" ${details.indicConsignePaiementSaisi == item.code ? 'selected=\'selected\'' : ''}>${item.libelleCourt}</option>
										</c:forEach>
	    							</select>
	    						</td>
	    						<td>
	    							Alerter si nb établ. varie d'au - : <span class="obligatoire">*&nbsp;</span> 
	    						</td>
	    						<td class="value">
									<input type="text" id="nbEtablissements" name="nbEtablissements" maxlength="6" value="${details.nbEtablissements}" tabindex="5" />
									établissements et de 
									<input type="text" id="pcEtablissements" name="pcEtablissements" maxlength="3" value="${details.pcEtablissements}" tabindex="6" />
									%
	    						</td>
	    					</tr>	    				
	    					<tr>
	    						<td>
	    							Transfert DSN : <span class="obligatoire">*&nbsp;</span> 
	    						</td>
	    						<td class="value">
	    							<select id="indicTransfertSaisi" name="indicTransfertSaisi" tabindex="7" >
										<c:forEach var="item" items="${reference.listeIndicsTransfert}">
											<option value="${item.code}" ${details.indicTransfertSaisi == item.code ? 'selected=\'selected\'' : ''}>${item.libelleCourt}</option>
										</c:forEach>
	    							</select>
	    						</td>
	    						<td>
	    							Alerter si nb salariés varie d'au - : <span class="obligatoire">*&nbsp;</span> 
	    						</td>
	    						<td class="value">
									<input type="text" id="nbSalaries" name="nbSalaries" maxlength="6" value="${details.nbSalaries}" tabindex="8" />
									salariés et de 
									<input type="text" id="pcSalaries" name="pcSalaries" maxlength="3" value="${details.pcSalaries}" tabindex="9" />
									%
	    						</td>
	    					</tr>
	    					<tr>
	    						<td>
	    							Mode de déclaration :<span>&nbsp;</span>
	    						</td>
	    						<td class="value">
	    							<select id="modeDeclaration" name="modeDeclaration" tabindex="10" >
										<c:forEach var="item" items="${reference.listeModeDeclaration}">
											<option value="${item.code}" ${details.modeDeclaration == item.code ? 'selected=\'selected\'' : ''}>${item.libelleCourt}</option>
										</c:forEach>
											<option value="" ${empty details.modeDeclaration ? 'selected=\'selected\'' : ''}>Aucune sélection</option>
	    							</select>
	    						</td>
	    						<td></td>
	    						<td></td>
	    					</tr>
	    					<tr>
	    						<td>
	    							V.I.P. : <span class="obligatoire">*&nbsp;</span> 
	    						</td>
	    						<td class="value">
	    							<select id="vipSaisi" name="vipSaisi" tabindex="11" >
										<c:forEach var="item" items="${reference.listeVIP}">
			    							<c:if test="${habilitationVIP eq false && details.vipSaisi == item.code}">
												<option value="${item.code}" ${'selected=\'selected\''}>${item.libelleCourt}</option>
					    					</c:if>
			    							<c:if test="${habilitationVIP eq true}">
												<option value="${item.code}" ${details.vipSaisi == item.code ? 'selected=\'selected\'' : ''}>${item.libelleCourt}</option>
					    					</c:if>
										</c:forEach>
	    							</select>
	    						</td>
	    						<td>
	    							Contrôle type base assuj. : <span class="obligatoire">*&nbsp;</span> 
	    						</td>
	    						<td class="value">
									<select id="baseAssuj" name="baseAssuj" tabindex="12" >
										<c:forEach var="item" items="${reference.listeBasesAssujetties}">
											<option value="${item.code}" ${details.baseAssuj == item.code ? 'selected=\'selected\'' : ''}>${item.libelleCourt}</option>
										</c:forEach>
	    							</select>
	    						</td>
	    					</tr>	    				
	    					<tr>
	    						<td>
	    							Force Gestion directe/déléguée : <span class="obligatoire">*&nbsp;</span> 
	    						</td>
	    						<td class="value">
	    							<select id="gestionDirecte" name="gestionDirecte" tabindex="13" >
										<c:forEach var="item" items="${reference.listeGestionsDirectes}">
											<option value="${item.code}" ${details.gestionDirecte == item.code ? 'selected=\'selected\'' : ''}>${item.libelleCourt}</option>
										</c:forEach>
	    							</select>
	    						</td>
	    						<td>
	    							Réaffectation effectifs : <span class="obligatoire">*&nbsp;</span> 
	    						</td>
	    						<td class="value">
									<select id="modeReaffectation" name="modeReaffectation" tabindex="14" >
										<c:forEach var="item" items="${reference.listeModeReaffectation}">
											<option value="${item.code}" ${details.modeReaffectation == item.code? 'selected=\'selected\'' : ''}>${item.libelleCourt}</option>
										</c:forEach>
	    							</select>
	    						</td>
	    					</tr>
	    					<tr class="zoneSirenNic">
	    						<td  class="value" colspan="2">
	    							LISTE DES SIREN/SIRET AFFILIES A CE CONTRAT :
	    						</td>
	    						<td>
	    							NOUVELLE AFFILIATION :&nbsp;
	    						</td>
	    						<td>
	    						</td>
	    					</tr>
	    					<tr>
	    						<td>
		    						<div id="sirenNicAffilies">
			    						<c:forEach var="couple" items="${listeSiret}">
											<c:set var="pos" value="${pos}i" />
											<c:set var="nbCouples" value="${fn:length(pos)}" />
											<div class="coupleSirenNic">
												${couple}&nbsp;<button id="couple_${pos}" class="buttonSirenNic" type="button" onclick="supprimerSirenNic(this)" tabindex="${16 + nbCouples}" ><span class="texteSupprimeSirenNic">-</span></button>
				    						</div>
											<c:set var="liste" value="${liste}${couple};" />
										</c:forEach>
			    						<input type="hidden" id="listeSirenNic" name="listeSirenNic" value="${liste}"></input>
			    						<input type="hidden" id="nbCoupleSirenNic" name="nbCoupleSirenNic" value="couple_${pos}"></input>
		    						</div>
	    						</td>
	    						<td>
	    						</td>
	    						<td class="libellesSirenNic">
									SIREN :&nbsp;<br/>
									NIC :&nbsp;
								</td>
	    						<td class="value champsSirenNic">
	    							<input id="sirenSaisi" name="sirenSaisi" maxlength="9" value="" tabindex="15" /><br/>
	    							<input id="nicSaisi" name="nicSaisi" maxlength="5" value="" tabindex="16" />&nbsp;&nbsp;
	    							<button id="couple_0" class="buttonSirenNic" type="button" onclick="ajoutSirenNic('${contrat.eligibiliteContratDsn}')" tabindex="17" ><span class="texteAjouteSirenNic">+</span></button>
	    						</td>
	    					</tr>
	    					<tr>
	    						<td>
	    						</td>
	    						<td>
	    						</td>
	    						<td>
	    						</td>
	    						<td>
						    		<div class="boutonSpecifique">
						    			<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
	    								<input type="hidden" id="numeroContrat" name="numeroContrat" value="${details.numeroContrat}" />
	    								<input type="hidden" id="rechercheUrlQueryString" name="rechercheUrlQueryString" value="${contexte.rechercheUrlQueryString}" />
						    			<button type="button" id="enregistre" class="boutonDetails" onclick="actionSpecifContratBeforeSubmit('<c:url value="/ihm/extensionscontrats?${contexte.rechercheUrlQueryString}" />', 'enregistre', 'Enregistrer')" tabindex="${18 + nbCouples}" >Enregistrer</button>
						    		</div>
	    						</td>
	    					</tr>
	    				</table>
	    			</form>
	    		</div>
		    	<div class="outilsContrat">
	    			<button class="navRecherche" type="button" onclick="location.href='<c:url value="/ihm/extensionscontrats?${contexte.rechercheUrlQueryString}" />'">&lt;&lt; Retour à la liste</button>
	    		</div>
	    	</div>
	   	</div>
	   	<%@include file="../communs/Footer.jsp" %>
   	</div>
	<script type="text/javascript" src="<c:url value="/ressources/libs/jquery.maskedinput-1.3.1.js" />"></script>
	<script type="text/javascript" src="<c:url value="/ressources/js/extensionscontrats/saisie.js?v=${version}" />"></script>
	

</body>
</html>