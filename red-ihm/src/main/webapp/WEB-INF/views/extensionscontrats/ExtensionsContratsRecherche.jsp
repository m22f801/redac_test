<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>
<head>
    <%@include file="../communs/PageMetadata.jsp" %>
	<link rel="stylesheet" href="<c:url value="/ressources/css/extensionscontrats.css" />"></link>
	<script language="javascript" type="text/javascript">  
		function SetFocus(InputID)  
		{  
			document.getElementById(InputID).focus();
			setTimeout(function() { document.getElementById(InputID).focus(); }, 10);		
			
			if(InputID=='numContrat1'){
				// set focus pour IE10 sur la dropdown "numContrat1"
				$('button[tabindex=1]')[0].focus();
			}
			
		}  
	</script>  
</head>  
<body onload="SetFocus('numContrat1')"> 
   	<div class="appWrapper">
	   	<%@include file="../communs/Header.jsp" %>
	   	<div class="bodyWrapper">
	    	<%@include file="../communs/Menu.jsp" %>
	    	<div class="content contrats recherche">
	    		<div class="titreContenu">Spécificités Contrats</div>
	    		<div class="informations">
	    			<span>${informations}</span>
	    		</div>
	    		<!-- Formulaire de recherche -->
	    		<div class="rechercheWrapper">
	    			<div class="titreFormulaire">Caractéristiques des contrats présentés :</div>
	    			<form class="rechercheFormulaire" action="<c:url value="/ihm/extensionscontrats" />" onsubmit="ValidationFormulaire()" method="get">
	    				<table>
	    					<tr>
	    						<td>
	    							<label for="numContrat">Contrat :</label>
	    							<input type="text" id="numContrat" name="numContrat" maxlength="15" value="${contexte.criteres.numContrat}" tabindex="1" />
	    						</td>
	    						<td>
	    							<label for="groupesGestion">Grpe gestion :</label>
	    							<select id="groupesGestion" name="groupesGestion" multiple="multiple" tabindex="2" >
										<c:forEach var="item" items="${reference.listeGroupesGestion}">
											<option value="${item.code}" ${contexte.criteres.groupesGestionAsList.contains(item.code) || empty contexte.criteres.groupesGestionAsList && contexte.reinitialisation eq true && utilisateur.groupeGestion eq item.code ? 'selected=\'selected\'' : ''}>${item.libelleCourt}</option>
										</c:forEach>
	    							</select>
	    						</td>
	    						<td>
	    							<label for="numClient">N° Client :</label>
	    							<input type="text" id="numClient" maxlength="6" name="numClient" value="<c:out value="${contexte.criteres.numClient}"/>" tabindex="3"  />
	    						</td>
	    					</tr>
	    					<tr>
	    						<td>
	    							<label for="eligibiliteContratDsn">Eligibilité :</label>
	    							<select id="eligibiliteContratDsn" name="eligibiliteContratDsn" multiple="multiple" tabindex="4" >
										<c:forEach var="item" items="${reference.listeEligibiliteContratDsn}">
											<option value="${item.code}" ${contexte.criteres.eligibiliteContratDsnAsList.contains(item.code) ? 'selected=\'selected\'' : ''}>${item.libelleCourt}</option>
										</c:forEach>
	    							</select>
	    						</td>
	    						<td>
	    							<label for="familles">Famille :</label>
	    							<select id="familles" name="familles" multiple="multiple" tabindex="5" >
										<c:forEach var="item" items="${reference.listeFamilles}">
											<option value="${item.code}" ${contexte.criteres.famillesAsList.contains(item.code) ? 'selected=\'selected\'' : ''}>${item.libelleCourt}</option>
										</c:forEach>
	    							</select>
	    						</td>
	    						<td>
	    							<label for="raisonSociale">Raison soc. :</label>
	    							<input id="raisonSociale" name="raisonSociale" maxlength="38" value="<c:out value="${contexte.criteres.raisonSociale}"/>" tabindex="6" />
	    						</td>
	    					</tr>
	    					<tr>
	    						<td>
	    							<label for="indicExploitation">Ind. Exploit :</label>
	    							<select id="indicExploitation" name="indicExploitation" multiple="multiple" tabindex="7" >
										<c:forEach var="item" items="${reference.listeIndicsExploitation}">
											<option value="${item.code}" ${contexte.criteres.indicExploitationAsList.contains(item.code) ? 'selected=\'selected\'' : ''}>${item.libelleCourt}</option>
										</c:forEach>
	    							</select>
	    						</td>
	    						<td>
	    							<label for="modesCalculCotisation">Nature :</label>
	    							<select id="modesCalculCotisation" name="modesCalculCotisation" multiple="multiple" tabindex="8" >
										<c:forEach var="item" items="${reference.listeNatures}">
											<option value="${item.code}" ${contexte.criteres.modesCalculCotisationAsList.contains(item.code) ? 'selected=\'selected\'' : ''}>${item.libelleCourt}</option>
										</c:forEach>
	    							</select>
	    						</td>
	    						<td>
	    							<label for="siren">SIREN :</label>
	    							<input id="siren" name="siren" maxlength="9" value="<c:out value="${contexte.criteres.siren}"/>" tabindex="9" />
	    						</td>
	    					</tr>
	    					<tr>
	    						<td>
	    							<label for="indicConsignePaiement">Ind Cs Paiemt :</label>
	    							<select id="indicConsignePaiement" name="indicConsignePaiement" multiple="multiple" tabindex="10" >
										<c:forEach var="item" items="${reference.listeIndicsConsignePaiement}">
											<option value="${item.code}" ${contexte.criteres.indicConsignePaiementAsList.contains(item.code) ? 'selected=\'selected\'' : ''}>${item.libelleCourt}</option>
										</c:forEach>
	    							</select>
	    						</td>
	    						<td>
	    							<label for="modeNatureContrat">Gest.directe :</label>
	    							<select id="modeNatureContrat" name="modeNatureContrat" multiple="multiple" tabindex="11" >
										<c:forEach var="item" items="${reference.listeGestionsDirectes}">
											<option value="${item.code}" ${contexte.criteres.modeNatureContratAsList.contains(item.code) ? 'selected=\'selected\'' : ''}>${item.libelleCourt}</option>
										</c:forEach>
	    							</select>
	    						</td>
	    						<td>
	    							<label for="nic">NIC :</label>
	    							<input id="nic" name="nic" maxlength="5" value="<c:out value="${contexte.criteres.nic}"/>" tabindex="12" />
	    						</td>
	    					</tr>
	    					<tr>
	    						<td>
	    							<label for="indicTransfert">Ind Transfert :</label>
	    							<select id="indicTransfert" name="indicTransfert" multiple="multiple" tabindex="13" >
										<c:forEach var="item" items="${reference.listeIndicsTransfert}">
											<option value="${item.code}" ${contexte.criteres.indicTransfertAsList.contains(item.code) ? 'selected=\'selected\'' : ''}>${item.libelleCourt}</option>
										</c:forEach>
	    							</select>
	    						</td>
	    						<td>
	    							<label for="cptEnc">Cpt enc :</label>
	    							<input id="cptEnc" name="cptEnc" maxlength="6" value="<c:out value="${contexte.criteres.cptEnc}"/>" tabindex="14" />
	    						</td>
	    						<td>
	    							<label for="entrepriseAffiliee">Ent.affil :</label>
	    							<select id="entrepriseAffiliee" name="entrepriseAffiliee" multiple="multiple" tabindex="15" >
										<option value="true" ${contexte.criteres.entrepriseAffilieeAsList.contains(true) ? 'selected=\'selected\'' : ''}>Oui</option>
										<option value="false" ${contexte.criteres.entrepriseAffilieeAsList.contains(false) ? 'selected=\'selected\'' : ''}>Non</option>
	    							</select>
	    						</td>
	    					</tr>
	    					<tr>
	    						<td>
	    							<label for="vip">VIP :</label>
	    							<select id="vip" name="vip" multiple="multiple" tabindex="16" >
										<c:forEach var="item" items="${reference.listeVIP}">
											<c:if test="${contexte.critereVipParDefaut.contains(item.code)}">
												<option value="${item.code}" ${contexte.reinitialisation eq true || contexte.criteres.vipAsList.contains(item.code) || !utilisateur.droitVIP ? 'selected=\'selected\'' : ''}>${item.libelleCourt}</option>
											</c:if>
											<c:if test="${!contexte.critereVipParDefaut.contains(item.code) && utilisateur.roleGestionCde && utilisateur.droitVIP}">
												<option value="${item.code}" ${contexte.criteres.vipAsList.contains(item.code)? 'selected=\'selected\'' : ''}>${item.libelleCourt}</option>
											</c:if>
										</c:forEach>
	    							</select>
	    						</td>
	    						<td>
	    							<label for="cptProd">Cpt prod :</label>
	    							<input id="cptProd" name="cptProd" maxlength="6" value="<c:out value="${contexte.criteres.cptProd}"/>" tabindex="17" />
	    						</td>
	    						<td>
	    						</td>
	    					</tr>
	    				</table>
	    				<input type="hidden" id="pageNumero" name="pageNumero" value="1" />
	    				<input type="hidden" id="pageTaille" name="pageTaille" value="50">
	    				<input type="hidden" id="triChamp" name="triChamp" value="numContrat">
	    				<input type="hidden" id="triAsc" name="triChamp" value="true">
	    				<div class="boutonsFormulaire">
							<button type="submit" class="boutonRecherche" tabindex="18" >
							    <div class="boutonIcone"></div><span>Rechercher</span>
							</button>
	    				</div>
	    			</form>
	    		</div>
	    		
	    		<!-- Tableau de résultats de recherche -->
	    		<div class="resultatsWrapper">
	    			<table class="resultats">
	    				<thead>
	    					<tr>
	    						<c:if test="${contexte.criteres.triChamp != 'groupeGestion'}">
	    							<td>
		    							<a href="?${fn:replace(contexte.urlQueryString, 'triChamp='.concat(contexte.criteres.triChamp), 'triChamp=groupeGestion')}" title="Trier par groupe de gestion">G.</a>
		    						</td>
	    						</c:if>
	    						<c:if test="${contexte.criteres.triChamp == 'groupeGestion'}">
	    							<td>
		    							<span>G.</span>
    									<c:if test="${contexte.criteres.triAsc}">
    										<a href="?${fn:replace(contexte.urlQueryString, 'triAsc=true', 'triAsc=false')}" class="triAsc" title="Trier par ordre décroissant"></a>
	    								</c:if>
    									<c:if test="${!contexte.criteres.triAsc}">
    										<a href="?${fn:replace(contexte.urlQueryString, 'triAsc=false', 'triAsc=true')}" class="triDesc" title="Trier par ordre croissant"></a>
	    								</c:if>
		    						</td>
	    						</c:if>
	    						<c:if test="${contexte.criteres.triChamp != 'designationFamille'}">
	    							<td>
		    							<a href="?${fn:replace(contexte.urlQueryString, 'triChamp='.concat(contexte.criteres.triChamp), 'triChamp=designationFamille')}" title="Trier par famille de produit">Fam.</a>
		    						</td>
	    						</c:if>
	    						<c:if test="${contexte.criteres.triChamp == 'designationFamille'}">
	    							<td>
		    							<span>Fam.</span>
    									<c:if test="${contexte.criteres.triAsc}">
    										<a href="?${fn:replace(contexte.urlQueryString, 'triAsc=true', 'triAsc=false')}" class="triAsc" title="Trier par ordre décroissant"></a>
	    								</c:if>
    									<c:if test="${!contexte.criteres.triAsc}">
    										<a href="?${fn:replace(contexte.urlQueryString, 'triAsc=false', 'triAsc=true')}" class="triDesc" title="Trier par ordre croissant"></a>
	    								</c:if>
		    						</td>
	    						</c:if>
	    						<c:if test="${contexte.criteres.triChamp != 'numContrat'}">
	    							<td>
		    							<a href="?${fn:replace(contexte.urlQueryString, 'triChamp='.concat(contexte.criteres.triChamp), 'triChamp=numContrat')}" title="Trier par numéro de contrat">Contrat</a>
		    						</td>
	    						</c:if>
	    						<c:if test="${contexte.criteres.triChamp == 'numContrat'}">
	    							<td>
		    							<span>Contrat</span>
    									<c:if test="${contexte.criteres.triAsc}">
    										<a href="?${fn:replace(contexte.urlQueryString, 'triAsc=true', 'triAsc=false')}" class="triAsc" title="Trier par ordre décroissant"></a>
	    								</c:if>
    									<c:if test="${!contexte.criteres.triAsc}">
    										<a href="?${fn:replace(contexte.urlQueryString, 'triAsc=false', 'triAsc=true')}" class="triDesc" title="Trier par ordre croissant"></a>
	    								</c:if>
		    						</td>
	    						</c:if>
	    						<c:if test="${contexte.criteres.triChamp != 'compteEncaissement'}">
	    							<td>
		    							<a href="?${fn:replace(contexte.urlQueryString, 'triChamp='.concat(contexte.criteres.triChamp), 'triChamp=compteEncaissement')}" title="Trier par compte d'encaissement">Cpt.enc</a>
		    						</td>
	    						</c:if>
	    						<c:if test="${contexte.criteres.triChamp == 'compteEncaissement'}">
	    							<td>
		    							<span>Cpt.enc</span>
    									<c:if test="${contexte.criteres.triAsc}">
    										<a href="?${fn:replace(contexte.urlQueryString, 'triAsc=true', 'triAsc=false')}" class="triAsc" title="Trier par ordre décroissant"></a>
	    								</c:if>
    									<c:if test="${!contexte.criteres.triAsc}">
    										<a href="?${fn:replace(contexte.urlQueryString, 'triAsc=false', 'triAsc=true')}" class="triDesc" title="Trier par ordre croissant"></a>
	    								</c:if>
		    						</td>
	    						</c:if>
	    						<c:if test="${contexte.criteres.triChamp != 'numSouscripteur'}">
	    							<td>
		    							<a href="?${fn:replace(contexte.urlQueryString, 'triChamp='.concat(contexte.criteres.triChamp), 'triChamp=numSouscripteur')}" title="Trier par numéro client">N°Client</a>
		    						</td>
	    						</c:if>
	    						<c:if test="${contexte.criteres.triChamp == 'numSouscripteur'}">
	    							<td>
		    							<span>N°Client</span>
    									<c:if test="${contexte.criteres.triAsc}">
    										<a href="?${fn:replace(contexte.urlQueryString, 'triAsc=true', 'triAsc=false')}" class="triAsc" title="Trier par ordre décroissant"></a>
	    								</c:if>
    									<c:if test="${!contexte.criteres.triAsc}">
    										<a href="?${fn:replace(contexte.urlQueryString, 'triAsc=false', 'triAsc=true')}" class="triDesc" title="Trier par ordre croissant"></a>
	    								</c:if>
		    						</td>
	    						</c:if>
	    						<c:if test="${contexte.criteres.triChamp != 'souscripteurNumSiret'}">
	    							<td>
		    							<a href="?${fn:replace(contexte.urlQueryString, 'triChamp='.concat(contexte.criteres.triChamp), 'triChamp=souscripteurNumSiret')}" title="Trier par SIREN NIC du client">SIREN NIC</a>
		    						</td>
	    						</c:if>
	    						<c:if test="${contexte.criteres.triChamp == 'souscripteurNumSiret'}">
	    							<td>
		    							<span>SIREN NIC</span>
    									<c:if test="${contexte.criteres.triAsc}">
    										<a href="?${fn:replace(contexte.urlQueryString, 'triAsc=true', 'triAsc=false')}" class="triAsc" title="Trier par ordre décroissant"></a>
	    								</c:if>
    									<c:if test="${!contexte.criteres.triAsc}">
    										<a href="?${fn:replace(contexte.urlQueryString, 'triAsc=false', 'triAsc=true')}" class="triDesc" title="Trier par ordre croissant"></a>
	    								</c:if>
		    						</td>
	    						</c:if>
	    						<c:if test="${contexte.criteres.triChamp != 'souscripteurRaisonSociale'}">
	    							<td>
		    							<a href="?${fn:replace(contexte.urlQueryString, 'triChamp='.concat(contexte.criteres.triChamp), 'triChamp=souscripteurRaisonSociale')}" title="Trier par raison sociale du client">Raison sociale</a>
		    						</td>
	    						</c:if>
	    						<c:if test="${contexte.criteres.triChamp == 'souscripteurRaisonSociale'}">
	    							<td>
		    							<span>Raison sociale</span>
    									<c:if test="${contexte.criteres.triAsc}">
    										<a href="?${fn:replace(contexte.urlQueryString, 'triAsc=true', 'triAsc=false')}" class="triAsc" title="Trier par ordre décroissant"></a>
	    								</c:if>
    									<c:if test="${!contexte.criteres.triAsc}">
    										<a href="?${fn:replace(contexte.urlQueryString, 'triAsc=false', 'triAsc=true')}" class="triDesc" title="Trier par ordre croissant"></a>
	    								</c:if>
		    						</td>
	    						</c:if>
	    						<td>Action</td>
	    					</tr>
	    				</thead>
	    				<tbody>
							<c:forEach items="${resultatsExtensions}" var="extensionContrat">
								<tr>
		    						<td>${extensionContrat.groupeGestion}</td>
		    						<td>${extensionContrat.designationFamille}</td>
		    						<td>${extensionContrat.numContrat}</td>
		    						<td>${extensionContrat.compteEncaissement}</td>
		    						<td>${extensionContrat.numSouscripteur}</td>
		    						<td>${extensionContrat.souscripteurNumSiret}</td>
		    						<c:if test="${fn:length(extensionContrat.souscripteurRaisonSociale) <= 25}">
			    						<td><span>${extensionContrat.souscripteurRaisonSociale}</span></td>
			    					</c:if>
		    						<c:if test="${fn:length(extensionContrat.souscripteurRaisonSociale) > 25}">
			    						<td><span>${fn:substring(extensionContrat.souscripteurRaisonSociale, 0, 25)}...</span></td>
			    					</c:if>
		    						<td>
		    							<div class="actionTableau lienModification"><a href="<c:url value="/ihm/extensionscontrats/${extensionContrat.numContrat}/?criteresRecherche=${contexte.extensionContratCriteresRecherche}" />"></a></div>
		    							<c:if test="${utilisateur.roleGestionCde && (extensionContrat.vipAsText == 'N' || extensionContrat.vipAsText == 'O' && utilisateur.droitVIP == true && (empty numContratsVIP || fn:contains(numContratsVIP, extensionContrat.numContrat)))}">
		    							</c:if>
		    						</td>
								</tr>
							</c:forEach>
	    				</tbody>
	    			</table>
	    		</div>

    			<input type="hidden" id="creerSpecificite" name="creerSpecificite" value="${creerSpecificite}"/>
   				<div class="boutonsFormulaire">
					<button id="nouvelleSpecificite" type="button" class="boutonRecherche" onclick="location.href='<c:url value="/ihm/extensionscontrats/${contexte.criteres.numContrat}/?criteresRecherche=${contexte.extensionContratCriteresRecherche}" />'">
						Nouvelle Spécificité pour le contrat filtré
					</button>
   				</div>

    			<div class="outilsPagination">
    				<ol>
    					<c:if test="${resultatsMeta.pageNumero > 1}">
    						<li class="lienPagination"><a href="?${fn:replace(contexte.urlQueryString, 'pageNumero='.concat(resultatsMeta.pageNumero), 'pageNumero='.concat(resultatsMeta.pageNumero - 1))}">Préc.</a></li>
    					</c:if>
    					<c:if test="${resultatsMeta.pageNumero == 1}">
    						<li class="lienPagination pageActuelle">1</li>
    					</c:if>
    					<c:if test="${resultatsMeta.pageNumero != 1}">
    						<li class="lienPagination"><a href="?${fn:replace(contexte.urlQueryString, 'pageNumero='.concat(resultatsMeta.pageNumero), 'pageNumero=1')}">1</a></li>
    					</c:if>
    					<c:if test="${resultatsMeta.maxPage > 1}">
	    					<c:if test="${resultatsMeta.pageNumero == 2}">
	    						<li class="lienPagination pageActuelle">2</li>
	    					</c:if>
	    					<c:if test="${resultatsMeta.pageNumero != 2}">
    							<li class="lienPagination"><a href="?${fn:replace(contexte.urlQueryString, 'pageNumero='.concat(resultatsMeta.pageNumero), 'pageNumero=2')}">2</a></li>
	    					</c:if>
    					</c:if>
    					<c:if test="${resultatsMeta.maxPage > 2}">
	    					<c:if test="${resultatsMeta.pageNumero == 3}">
	    						<li class="lienPagination pageActuelle">3</li>
		    					<c:if test="${resultatsMeta.maxPage > 4}">
    								<li class="lienPagination"><a href="?${fn:replace(contexte.urlQueryString, 'pageNumero='.concat(resultatsMeta.pageNumero), 'pageNumero=4')}">4</a></li>
		    					</c:if>
	    					</c:if>
	    					<c:if test="${resultatsMeta.pageNumero != 3}">
    							<li class="lienPagination"><a href="?${fn:replace(contexte.urlQueryString, 'pageNumero='.concat(resultatsMeta.pageNumero), 'pageNumero=3')}">3</a></li>
	    					</c:if>
    					</c:if>
    					<c:if test="${resultatsMeta.maxPage > 3}">
    						<c:if test="${resultatsMeta.pageNumero < resultatsMeta.maxPage - 2}">
    							<c:if test="${resultatsMeta.pageNumero > 3}">
		    						<c:if test="${resultatsMeta.pageNumero > 5}">
			    						<li>...</li>
			    					</c:if>
		    						<c:if test="${resultatsMeta.pageNumero > 4}">
		    							<li class="lienPagination"><a href="?${fn:replace(contexte.urlQueryString, 'pageNumero='.concat(resultatsMeta.pageNumero), 'pageNumero='.concat(resultatsMeta.pageNumero - 1))}">${resultatsMeta.pageNumero - 1}</a></li>
		    						</c:if>
		    						<li class="lienPagination pageActuelle">${resultatsMeta.pageNumero}</li>
	    							<li class="lienPagination"><a href="?${fn:replace(contexte.urlQueryString, 'pageNumero='.concat(resultatsMeta.pageNumero), 'pageNumero='.concat(resultatsMeta.pageNumero + 1))}">${resultatsMeta.pageNumero + 1}</a></li>
		    					</c:if>
	    						<li>...</li>
	    						<li class="lienPagination"><a href="?${fn:replace(contexte.urlQueryString, 'pageNumero='.concat(resultatsMeta.pageNumero), 'pageNumero='.concat(resultatsMeta.maxPage))}">${resultatsMeta.maxPage}</a></li>
    						</c:if>
    						<c:if test="${resultatsMeta.pageNumero >= resultatsMeta.maxPage - 2}">
	    						<c:if test="${resultatsMeta.pageNumero > 6 || resultatsMeta.pageNumero == 6 && resultatsMeta.maxPage > 6 }">
		    						<li>...</li>
		    					</c:if>
	    						<c:if test="${resultatsMeta.maxPage == 5}">
			    					<c:if test="${resultatsMeta.pageNumero == 4}">
			    						<li class="lienPagination pageActuelle">4</li>
			    					</c:if>
			    					<c:if test="${resultatsMeta.pageNumero == 5}">
	    								<li class="lienPagination"><a href="?${fn:replace(contexte.urlQueryString, 'pageNumero='.concat(resultatsMeta.pageNumero), 'pageNumero=4')}">4</a></li>
			    					</c:if>
		    					</c:if>
	    						<c:if test="${resultatsMeta.maxPage > 5}">
			    					<c:if test="${resultatsMeta.pageNumero == resultatsMeta.maxPage - 2}">
			    						<c:if test="${resultatsMeta.pageNumero > 4}">
				    						<li class="lienPagination"><a href="?${fn:replace(contexte.urlQueryString, 'pageNumero='.concat(resultatsMeta.pageNumero), 'pageNumero='.concat(resultatsMeta.maxPage - 3))}">${resultatsMeta.maxPage - 3}</a></li>
				    					</c:if>
			    						<li class="lienPagination pageActuelle">${resultatsMeta.maxPage - 2}</li>
			    					</c:if>
			    					<c:if test="${resultatsMeta.pageNumero != resultatsMeta.maxPage - 2}">
			    						<li class="lienPagination"><a href="?${fn:replace(contexte.urlQueryString, 'pageNumero='.concat(resultatsMeta.pageNumero), 'pageNumero='.concat(resultatsMeta.maxPage - 2))}">${resultatsMeta.maxPage - 2}</a></li>
			    					</c:if>
			    					<c:if test="${resultatsMeta.pageNumero == resultatsMeta.maxPage - 1}">
			    						<li class="lienPagination pageActuelle">${resultatsMeta.maxPage - 1}</li>
			    					</c:if>
			    					<c:if test="${resultatsMeta.pageNumero != resultatsMeta.maxPage - 1}">
		    							<li class="lienPagination"><a href="?${fn:replace(contexte.urlQueryString, 'pageNumero='.concat(resultatsMeta.pageNumero), 'pageNumero='.concat(resultatsMeta.maxPage - 1))}">${resultatsMeta.maxPage - 1}</a></li>
			    					</c:if>
		    					</c:if>
		    					<c:if test="${resultatsMeta.pageNumero == resultatsMeta.maxPage}">
	    							<li class="lienPagination pageActuelle">${resultatsMeta.maxPage}</li>
		    					</c:if>
		    					<c:if test="${resultatsMeta.pageNumero != resultatsMeta.maxPage}">
	    							<li class="lienPagination"><a href="?${fn:replace(contexte.urlQueryString, 'pageNumero='.concat(resultatsMeta.pageNumero), 'pageNumero='.concat(resultatsMeta.maxPage))}">${resultatsMeta.maxPage}</a></li>
		    					</c:if>
    						</c:if>
    					</c:if>
    					<c:if test="${resultatsMeta.pageNumero < resultatsMeta.maxPage}">
    						<li class="lienPagination"><a href="?${fn:replace(contexte.urlQueryString, 'pageNumero='.concat(resultatsMeta.pageNumero), 'pageNumero='.concat(resultatsMeta.pageNumero + 1))}">Suiv.</a></li>
    					</c:if>
    				</ol>
    			</div>
    		</div>
	   	</div>
	   	<%@include file="../communs/Footer.jsp" %>
   	</div>
   	
	<script type="text/javascript" src="<c:url value="/ressources/libs/jquery.maskedinput-1.3.1.js" />"></script>
	<script type="text/javascript" src="<c:url value="/ressources/js/extensionscontrats/recherche.js?v=${version}" />"></script>
</body>
</html>