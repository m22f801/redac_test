<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <%@include file="communs/PageMetadata.jsp" %>
</head>
<body>
   	<div class="appWrapper deconnexionWrapper">
		Vous avez bien été déconnecté(e).
		<br/><br/>
		<button class="navPeriodePrecedente" type="button" onclick="location.href='<c:url value="/ihm" />'">&lt;&lt; Revenir à l'accueil</button>
	</div>
</body>
</html>