package fr.si2m.red.ihm.securite;

import java.io.IOException;
import java.security.Principal;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.Set;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import lombok.Setter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.GenericFilterBean;

/**
 * Filtre abstrait pour extraire des token d'authentification des requêtes HTTP.<br/>
 * <br/>
 * Ce filtre fait le pont avec les rôles Active Directory d'un utilisateur REDAC.
 * 
 * @author nortaina
 * 
 */
public class RedacAuthenticationTokenProcessingFilter extends GenericFilterBean {
    private static final Logger LOGGER = LoggerFactory.getLogger(RedacAuthenticationTokenProcessingFilter.class);

    private static final String SESSION_ATTRIBUTE_AUTHENTICATION = "X-REDAC-USER";

    private static final String CHEMIN_RESSOURCE_PARAMETRAGE = "classpath:parametrage.properties";
    private static final String CHEMIN_RESSOURCE_MAPPING_ROLES_DROITS = "classpath:mapping-roles-droits.properties";
    private static final String PLACEHOLDER_ENVIRONNEMENT_MAPPING_ROLES_DROITS = "{ENV}";

    /**
     * Le gestionnaire d'authentification.
     * 
     * @param authenticationManager
     *            le gestionnaire d'authentification à mettre à jour
     */
    @Autowired
    @Setter
    private AuthenticationManager authenticationManager;

    @Autowired
    private ApplicationContext applicationContext;

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest httpRequest = (HttpServletRequest) request;
        // Instancie la session si elle n'existe pas encore
        HttpSession session = httpRequest.getSession(true);
        Authentication authentication = (Authentication) session.getAttribute(SESSION_ATTRIBUTE_AUTHENTICATION);
        if (authentication == null) {
            Principal principal = httpRequest.getUserPrincipal();
            if (principal == null) {
                ((HttpServletResponse) response).sendError(HttpServletResponse.SC_UNAUTHORIZED, "Utilisateur non authentifié");
                return;
            }
            List<GrantedAuthority> authorities = resolveGrantedAuthorities(httpRequest);
            UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(principal, null, authorities);
            token.setDetails(new WebAuthenticationDetailsSource().buildDetails((HttpServletRequest) request));
            try {
                authentication = authenticationManager.authenticate(token);
            } catch (AuthenticationException e) {
                LOGGER.error("Erreur lors de l'authentification de l'utilisateur " + principal, e);
                ((HttpServletResponse) response).sendError(HttpServletResponse.SC_UNAUTHORIZED, e.getLocalizedMessage());
                return;
            } catch (Exception e) {
                String message = "Erreur lors de l'authentification de l'utilisateur ";
                LOGGER.error(message + principal, e);
                ((HttpServletResponse) response).sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, message + principal.getName());
                return;
            }
            // On sauvegarde l'authentification en session pour limiter le nombre de requêtes à la base des utilisateurs
            session.setAttribute(SESSION_ATTRIBUTE_AUTHENTICATION, authentication);
        }
        SecurityContextHolder.getContext().setAuthentication(authentication);

        chain.doFilter(httpRequest, response);
    }

    /**
     * Algorithme de résolution des rôles de l'utilisateur à partir des droits AD contenus dans la requête HTTP.
     * 
     * @param httpRequest
     *            la requête HTTP
     * @return les rôles de l'utilisateur reconnus par l'application REDAC
     * @throws IOException
     *             si les paramètres de configuration de l'application ne peuvent pas être chargés correctement
     */
    private List<GrantedAuthority> resolveGrantedAuthorities(HttpServletRequest httpRequest) throws IOException {

        // Récupération de l'environnement d'exécution
        Properties parametrage = new Properties();
        parametrage.load(applicationContext.getResource(CHEMIN_RESSOURCE_PARAMETRAGE).getInputStream());
        String environnement = parametrage.getProperty("red.environnement");

        // Résolution des rôles de l'utilisateur
        List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
        Properties mapping = new Properties();
        mapping.load(applicationContext.getResource(CHEMIN_RESSOURCE_MAPPING_ROLES_DROITS).getInputStream());
        @SuppressWarnings("unchecked")
        Set<String> droitsActiveDirectoryParametrables = (Set<String>) (Set<?>) mapping.keySet();
        for (String droitActiveDirectoryParametrable : droitsActiveDirectoryParametrables) {
            String droitActiveDirectory = StringUtils.replace(droitActiveDirectoryParametrable, PLACEHOLDER_ENVIRONNEMENT_MAPPING_ROLES_DROITS,
                    environnement);
            if (httpRequest.isUserInRole(droitActiveDirectory)) {
                String roleRedacConnu = mapping.getProperty(droitActiveDirectoryParametrable);
                authorities.add(new SimpleGrantedAuthority(roleRedacConnu));
            }
        }

        return authorities;
    }
}
