package fr.si2m.red.ihm;

import lombok.Getter;
import lombok.Setter;

/**
 * Configuration technique de l'IHM.
 * 
 * @author nortaina
 *
 */
public class ConfigurationIHM {
    /**
     * L'URL de la Brique DSN.
     * 
     * @param urlBriqueDsn
     *            l'URL de la Brique DSN
     * @return l'URL de la Brique DSN
     */
    @Getter
    @Setter
    private String urlBriqueDsn;
}
