package fr.si2m.red.ihm.accueil;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ResourceBundle;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;

import fr.si2m.red.RedacUnexpectedException;
import fr.si2m.red.ihm.RedacIhmController;
import fr.si2m.red.ihm.RoleRedac;
import fr.si2m.red.ihm.exceptions.RessourceNonTrouveeException;

/**
 * Contrôleur des fonctionnalités d'accueil de l'application, accessibles uniquement aux utilisateurs authentifiés.
 * 
 * @author nortaina
 * 
 */
@Controller
@RequestMapping("")
public class AccueilController extends RedacIhmController {

    private static final Logger LOGGER = LoggerFactory.getLogger(AccueilController.class);

    /**
     * Récupère la page d'accueil (redirige par défaut vers l'écran des déclarations selon demande du métier).
     * 
     * @return l'indicateur de redirection
     */
    @Secured(RoleRedac.ROLE_REDCONSULT)
    @RequestMapping(value = "/")
    @ResponseStatus(value = HttpStatus.MOVED_PERMANENTLY)
    public String getAccueil() {
        return "redirect:/ihm/declarations";
    }

    /**
     * Télécharge l'aide utilisateur.
     * 
     * @param response
     *            la réponse à la requête
     * @throws RessourceNonTrouveeException
     *             levée si l'aide utilisateur est introuvable
     */
    @Secured(RoleRedac.ROLE_REDCONSULT)
    @RequestMapping(value = "/aide", method = RequestMethod.GET)
    @ResponseStatus(code = HttpStatus.OK)
    public void telechargeAideUtilisateur(HttpServletResponse response) throws RessourceNonTrouveeException {
        ResourceBundle ressource = ResourceBundle.getBundle("parametrage");
        String cheminComplet = ressource.getString("red.manuel_utilisateur.chemin_complet");
        FileInputStream fluxFichier;
        try {
            fluxFichier = new FileInputStream(cheminComplet);
        } catch (FileNotFoundException e) {
            LOGGER.error("L'aide utilisateur est introuvable", e);
            throw new RessourceNonTrouveeException();
        }
        try {
            response.setContentType("application/pdf");
            response.setHeader("Content-Disposition", "attachment; filename=Aide.pdf");
            IOUtils.copy(fluxFichier, response.getOutputStream());
            response.flushBuffer();
        } catch (IOException e) {
            throw new RedacUnexpectedException("Erreur lors de la lecture du manuel utilisateur", e);
        } finally {
            IOUtils.closeQuietly(fluxFichier);
        }
    }

    /**
     * Déconnecte l'utilisateur.
     * 
     * @param request
     *            la requête HTTP de demande de la page
     * 
     * @return le modèle et la vue de la page d'accueil
     */
    @RequestMapping(value = "/deconnexion", method = RequestMethod.POST, produces = MediaType.TEXT_HTML_VALUE)
    @ResponseStatus(code = HttpStatus.UNAUTHORIZED)
    public String deconnecteUtilisateur(HttpServletRequest request) {
        request.getSession(true).invalidate();
        return "Deconnexion";
    }

    /**
     * Gestion des redirections vers la page d'accueil par défaut.
     * 
     * @return la commande de redirection vers la racine de l'application
     */
    @ResponseStatus(value = HttpStatus.MOVED_PERMANENTLY)
    @RequestMapping(value = "/*")
    public String redirect() {
        return "redirect:/ihm/";
    }

}
