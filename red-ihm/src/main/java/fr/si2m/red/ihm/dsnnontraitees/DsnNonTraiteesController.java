package fr.si2m.red.ihm.dsnnontraitees;

import java.io.IOException;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.si2m.red.reconciliation.*;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.ModelAndView;

import fr.si2m.red.dsn.EtatPeriode;
import fr.si2m.red.ihm.FilAriane;
import fr.si2m.red.ihm.Pages;
import fr.si2m.red.ihm.RedacIhmController;
import fr.si2m.red.ihm.RoleRedac;
import fr.si2m.red.ihm.exceptions.RessourceNonTrouveeException;
import fr.si2m.red.ihm.session.UtilisateurIHM;
import fr.si2m.red.parametrage.ParamCodeLibelle;
import fr.si2m.red.parametrage.ParamCodeLibelleRepository;
import fr.si2m.red.parametrage.ParamControleSignalRejetRepository;
import fr.si2m.red.parametrage.ParamUtilisateurGestionnaireRepository;

/**
 * Contrôleur des pages de gestion des DSN.
 * 
 * @author nortaina
 * 
 */
@Controller
@RequestMapping("/dsnnontraitees")
public class DsnNonTraiteesController extends RedacIhmController {

    private static final Logger LOGGER = LoggerFactory.getLogger(DsnNonTraiteesController.class);

    private static final String CODE_VIP_FALSE = "N";

    private static final String TBL_ADHESION = "ADHESION_ETAB_MOIS";
    private static final String REDIRECT_DSN = "redirect:/ihm/dsnnontraitees";

    private static final int TAILLE_PAGE = 1000;

    @Autowired
    private IDsnNonTraiteesRepository periodeRecueDsnNonTraitees;
    @Autowired
    private HistoriqueEtatPeriodeRepository historiqueEtatPeriodeRepository;
    @Autowired
    private MessageControleRepository messageControleRepository;
    @Autowired
    private DsnNonTraiteesExportExcel declarationsExportExcel;
    @Autowired
    private ParamCodeLibelleRepository paramCodeLibelleRepository;
    @Autowired
    private ParamControleSignalRejetRepository paramControleSignalRejetRepository;
    @Autowired
    private ParamUtilisateurGestionnaireRepository paramUtilisateurGestionnaireRepository;

    private static final SimpleDateFormat formater = new SimpleDateFormat("yyyyMMddHHmmss");

    /**
     * Récupère la page de recherche des DSN.
     * 
     * @param criteres
     *            les critères de recherche
     * @param httpRequest
     *            la requête HTTP de demande de la page
     * 
     * @return le modèle et la vue de la page
     * 
     */
    @Secured(RoleRedac.ROLE_REDCONSULT)
    @RequestMapping(value = "", method = RequestMethod.GET, produces = MediaType.TEXT_HTML_VALUE)
    @Transactional(propagation = Propagation.REQUIRES_NEW, rollbackFor = Throwable.class)
    public ModelAndView getRechercheDeclarations(@RequestParam MultiValueMap<String, Object> criteres, HttpServletRequest httpRequest) {
        // Récupération des critères de recherche
        DsnNonTraiteesCriteresRecherche criteresRecherche = DsnNonTraiteesCriteresRecherche.from(criteres);
        UtilisateurIHM utilisateur = getUtilisateurAuthentifie();

        // Préparation des critères pour le bloc de synthèse
        if (criteres.isEmpty()) {
            initialisationGroupesGestion(utilisateur, criteresRecherche);
        }

        String libelleGroupeGestionUnique = criteresRecherche.getGroupesGestionAsList().size() == 1 ? paramCodeLibelleRepository
                .getLibelleCourt(ParamCodeLibelle.TABLE_CONTRAT, "NMGRPGES", criteresRecherche.getGroupesGestionAsList().get(0)) : null;

        List<String> listeFamilles = criteresRecherche.getFamillesAsList();
        String libelleFamilleUnique = listeFamilles.size() == 1
                ? paramCodeLibelleRepository.getLibelleCourt(ParamCodeLibelle.TABLE_CONTRAT, "NOFAM", listeFamilles.get(0)) : null;

        List<Integer> listeFamillesAsIntegers = new ArrayList<>(listeFamilles.size());
        for (String famille : listeFamilles) {
            listeFamillesAsIntegers.add(Integer.valueOf(famille));
        }

        // Initialisation du MAV pour l'écran DSN Non traitees
        ModelAndView mav = initialiseModeleVue("dsnnontraitees/DsnNonTraiteesRecherche", httpRequest, criteresRecherche.getGroupesGestionAsList(),
                libelleGroupeGestionUnique, listeFamillesAsIntegers, libelleFamilleUnique);

        // Info fil d'Ariane
        FilAriane filAriane = new FilAriane();
        filAriane.setFil(Arrays.asList(Pages.ACCUEIL, Pages.DSNNONTRAITEES));
        mav.addObject("filAriane", filAriane);

        // Infos résultats de recherche
        long total = 0;

        // On effectue une recherche avec les valeurs par defaut du bloc de
        // recherche lors de l'initialisation/reinitialisation
        // = on effectue une recherche quoi qu'il arrive
        if (criteres.isEmpty()) {
            // filtre grpe gestion déjà set
            String[] strArray = new String[] { "VIDE" };
            criteresRecherche.setStatuts(strArray);
            criteresRecherche.setTriChamp("spec");
        }

        mav.addObject("resultats", periodeRecueDsnNonTraitees.rechercheDsnNonTraiteesIHM(criteresRecherche));
        mav.addObject("value_dsn_neant", DsnNonTraitees.DSN_NEANT);
        total = periodeRecueDsnNonTraitees.countResults();

        if (total == 0L) {
            mav.addObject("informations", "Aucune DSN détachée pour cette sélection");
        }
        // nombre de périodes impactées avec séparatuer "espace" de milliers
        mav.addObject("nbPeriodesFiltrees", NumberFormat.getInstance(Locale.FRENCH).format(total).toString());

        mav.addObject("resultatsMeta", creeResultatsInfoPagination(total, criteresRecherche));

        // Données de référence pour la recherche
        Map<String, Object> reference = new HashMap<String, Object>();
        reference.put("listeStatuts", getEtatsPeriodesSelectionnables());
        reference.put("listeGroupesGestion", getGroupesGestionsSelectionnables());
        reference.put("listeFamilles", getFamillesSelectionnables());
        reference.put("listeNatures", getNaturesPeriodesSelectionnables());
        reference.put("listeVIP", getVipSelectionnables());
        reference.put("listeUsersAffecteA", paramUtilisateurGestionnaireRepository
                .getListeNomsEtCodeUsers(DsnNonTraiteesCriteresRecherche.NON_AFFECTE_LIBELLE, DsnNonTraiteesCriteresRecherche.NON_AFFECTE_CODE));
        reference.put("listeUsersATraiterPar", paramUtilisateurGestionnaireRepository
                .getListeNomsEtCodeUsers(DsnNonTraiteesCriteresRecherche.NON_ASSIGNE_LIBELLE, DsnNonTraiteesCriteresRecherche.NON_ASSIGNE_CODE));
        reference.put("listeUsersTraitePar", paramUtilisateurGestionnaireRepository
                .getListeNomsEtCodeUsers(DsnNonTraiteesCriteresRecherche.TRAITE_AUTO_LIBELLE, DsnNonTraiteesCriteresRecherche.TRAITE_AUTO_CODE));
        reference.put("droitFusionerPeriode", (Boolean) getInfoUtilisateur(httpRequest).get(RoleRedac.ID_ROLE_GESTION_REDAC));
        mav.addObject("reference", reference);

        // Construction du contexte de la page
        Map<String, Object> contexte = new HashMap<String, Object>();
        contexte.put("criteres", criteresRecherche);
        contexte.put("reinitialisation", criteres.isEmpty());
        contexte.put("critereVipParDefaut", Arrays.asList(CODE_VIP_FALSE));
        contexte.put("critereStatutsParDefaut", "VIDE");
        String criteresRechercheQueryString = criteresRecherche.toQueryString();
        contexte.put("urlQueryString", criteresRechercheQueryString);
        contexte.put("contratCriteresRecherche", StringUtils.replace(criteresRechercheQueryString, "&", ";"));
        contexte.put("droitGroupe1", (Boolean) getInfoUtilisateur(httpRequest).get(RoleRedac.ROLE_REDCONSULT));
        contexte.put("droitGroupe2", (Boolean) getInfoUtilisateur(httpRequest).get(RoleRedac.ID_ROLE_GESTION_CDE));
        contexte.put("droitGroupe3", (Boolean) getInfoUtilisateur(httpRequest).get(RoleRedac.ID_ROLE_GESTION_REDAC));

        mav.addObject("contexte", contexte);

        // Construction des listes de la popup (sans autre gestionnaire)
        Map<String, Object> listesPopup = new HashMap<String, Object>();

        listesPopup.put("listeUsersAffecteA", paramUtilisateurGestionnaireRepository.getlistesAffecteAEtAtraiterParSansAutreGest(
                DsnNonTraiteesCriteresRecherche.NON_AFFECTE_LIBELLE, DsnNonTraiteesCriteresRecherche.NON_AFFECTE_CODE));
        listesPopup.put("listeUsersATraiterPar", paramUtilisateurGestionnaireRepository.getlistesAffecteAEtAtraiterParSansAutreGest(
                DsnNonTraiteesCriteresRecherche.NON_ASSIGNE_LIBELLE, DsnNonTraiteesCriteresRecherche.NON_ASSIGNE_CODE));

        // Construction de la liste de changement de statut en masse
        List<ParamCodeLibelle> listeStatutsInitiaux = paramCodeLibelleRepository.get(ParamCodeLibelle.TABLE_PERIODE_RECUE, "ETAT");
        HashMap<String, ParamCodeLibelle> mapStatuts = new HashMap<>(listeStatutsInitiaux.size());

        for (ParamCodeLibelle paramCodeLibelle : listeStatutsInitiaux) {
            mapStatuts.put(paramCodeLibelle.getCode(), paramCodeLibelle);
        }

        List<ParamCodeLibelle> statutsSelectionnables = Arrays.asList(mapStatuts.get(EtatPeriode.ARI.name()), mapStatuts.get(EtatPeriode.NIN.name()),
                mapStatuts.get(EtatPeriode.ING.name()), mapStatuts.get(EtatPeriode.INS.name()), mapStatuts.get(EtatPeriode.SSG.name()),
                mapStatuts.get(EtatPeriode.TPG.name()));

        // on conserve que les statuts selectionnables
        listeStatutsInitiaux.retainAll(statutsSelectionnables);

        ParamCodeLibelle nonRenseigneUser = new ParamCodeLibelle();
        listeStatutsInitiaux.add(0, nonRenseigneUser);
        listesPopup.put("listeStatuts", listeStatutsInitiaux);

        mav.addObject("listesPopup", listesPopup);

        LOGGER.error( "end of controller");
        return mav;
    }

    /**
     * Effectue le controle sur les status des périodes données en paramètres pour changement d'état F09_RG_7_15
     * 
     * @param criteres
     *            multivalue map contenant les critères
     * @return la liste des etats invalides parmi les périodes ciblées
     * @throws RessourceNonTrouveeException
     */

    @Secured({ RoleRedac.ROLE_REDCDE, RoleRedac.ROLE_REDREDAC })
    @RequestMapping(value = "/miseAJourSSG", method = RequestMethod.POST, produces = MediaType.TEXT_HTML_VALUE)
    @Transactional(propagation = Propagation.REQUIRES_NEW, rollbackFor = Throwable.class)
    @ResponseStatus(HttpStatus.SEE_OTHER)
    public String miseAJourSSG(@RequestParam MultiValueMap<String, Object> criteres) {
        PopupPassageSansSuiteGestion changementsAdhesion = PopupPassageSansSuiteGestion.from(criteres);
        UtilisateurIHM utilisateur = getUtilisateurAuthentifie();
        if (!StringUtils.isEmpty(changementsAdhesion.getIdsAdhesion())) {
            periodeRecueDsnNonTraitees.setAdhesionSSG(changementsAdhesion.getIdsAdhesionAsString(), utilisateur.getIdentifiantActiveDirectory());
        }
        return REDIRECT_DSN
                + (StringUtils.isNotBlank(changementsAdhesion.getParametresRecherche()) ? "?" + changementsAdhesion.getParametresRecherche() : "");
    }

    /**
     * Effectue le controle sur les status des périodes données en paramètres pour changement d'état F09_RG_7_15
     * 
     * @param criteres
     *            multivalue map contenant les critères
     * @return la liste des etats invalides parmi les périodes ciblées
     * @throws RessourceNonTrouveeException
     */

    @Secured({ RoleRedac.ROLE_REDCDE, RoleRedac.ROLE_REDREDAC })
    @RequestMapping(value = "/miseAJourSSG/passageSansSuite", method = RequestMethod.GET, produces = MediaType.TEXT_HTML_VALUE)
    @Transactional(propagation = Propagation.REQUIRES_NEW, rollbackFor = Throwable.class)
    public ResponseEntity<String> PassageSansSuiteGestion(@RequestParam MultiValueMap<String, Object> criteres) {
        /**
         * Mapping des critères envoyés
         */
        PopupPassageSansSuiteGestion changementsAdhesion = PopupPassageSansSuiteGestion.from(criteres);

        // List<String> adhesionVide = new ArrayList<>();
        // Infos résultats de recherche
        long adhesionVide = 0;
        // List<String> statutsKO = new ArrayList<>();
        if (!StringUtils.isEmpty(changementsAdhesion.getIdsAdhesion())) {
            // Liste des statuts des périodes ciblées
            adhesionVide = periodeRecueDsnNonTraitees.getAdhesionVide(changementsAdhesion.getIdsAdhesionAsString());
        }

        /**
         * Génération de la réponse
         */

        if (adhesionVide == 0) {
            return new ResponseEntity<String>("OK", HttpStatus.OK);
        } else {
            StringBuilder message = new StringBuilder();
            message.append("Les DSN non traitées sélectionnées ne doivent pas être déjà en sans suite gestion");

            // message.append(org.springframework.util.StringUtils.collectionToDelimitedString(statutsKO, ", ", "« ", " »"));

            return new ResponseEntity<String>(message.toString(), HttpStatus.FORBIDDEN);
        }
    }

    /**
     * 
     * Détermine la valeur par defaut du GroupesGestion - F09_RG_1_03 à l'initialisation/réinitialisation.
     * 
     * @param utilisateur
     *            l'utilisateur associé à la session
     * @param criteresRecherche
     *            les critères de la recherche
     */
    private void initialisationGroupesGestion(UtilisateurIHM utilisateur, DsnNonTraiteesCriteresRecherche criteresRecherche) {
        String groupeGestUser = utilisateur.getGroupeGestion();
        if (StringUtils.isNotBlank(groupeGestUser)) {
            for (ParamCodeLibelle groupegestion : getGroupesGestionsSelectionnables()) {
                if (StringUtils.equals(groupeGestUser, groupegestion.getCode())) {
                    criteresRecherche.setGroupesGestion(new String[] { groupeGestUser });
                }
            }
        }
    }

    /**
     * Récupère la liste des états de période connus de l'application.
     * 
     * @return la liste des états de période connus de l'application
     */
    private List<ParamCodeLibelle> getEtatsPeriodesSelectionnables() {
        return paramCodeLibelleRepository.get(TBL_ADHESION, "ETAT");
    }

    /**
     * Récupère l'export Excel des contrats.
     * 
     * @param criteres
     *            les critères de la recherche des contrats
     * @param httpResponse
     *            la réponse à la requête de récupération
     * 
     * @throws IOException
     *             levée si une erreur d'écriture de flux est détectée
     */
    @Secured(RoleRedac.ROLE_REDCONSULT)
    @RequestMapping(value = "/exports/excel", method = RequestMethod.GET)
    @Transactional(propagation = Propagation.REQUIRES_NEW, rollbackFor = Throwable.class)
    @ResponseStatus(HttpStatus.OK)
    public void getExport(@RequestParam MultiValueMap<String, Object> criteres, HttpServletResponse httpResponse) throws IOException {
        // Récupération des critères de recherche
        DsnNonTraiteesCriteresRecherche criteresRecherche = DsnNonTraiteesCriteresRecherche.from(criteres);
        if (criteres.isEmpty()) {
            // filtre grpe gestion déjà set
            criteresRecherche.setSirenEntreprise("");
            criteresRecherche.setNumContrat("");
            criteresRecherche.setNicEtablissement("");
            criteresRecherche.setMoisRattachement("");
            criteresRecherche.setCptEnc("");
            criteresRecherche.setRaisonSociale("");
            criteresRecherche.setCptProd("");
            criteresRecherche.setTriChamp("spec");
        }

        try {
            httpResponse.setContentType("application/vnd.ms-excel");
            httpResponse.setHeader("Content-Disposition", "attachment; filename=\"Liste des Dsn non traitées.xlsx\"");
            declarationsExportExcel.exporte(criteresRecherche, httpResponse.getOutputStream());
            httpResponse.flushBuffer();
        } catch (Exception e) {
            httpResponse.reset();
            httpResponse.setHeader("Content-Type", "text/plain; charset=utf-8");
            throw e;
        }
    }

    /**
     * Gestion des redirections vers la page de recherche de déclarations par défaut.
     * 
     * @return la commande de redirection vers la page de recherche de déclarations par défaut
     */
    @ResponseStatus(value = HttpStatus.MOVED_PERMANENTLY)
    @RequestMapping(value = "/*")
    public String redirect() {
        return REDIRECT_DSN;
    }

}
