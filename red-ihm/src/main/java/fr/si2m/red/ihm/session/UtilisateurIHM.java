package fr.si2m.red.ihm.session;

import java.security.Principal;
import java.util.Collections;
import java.util.List;

import lombok.Getter;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;

import fr.si2m.red.parametrage.ParamUtilisateurGestionnaire;

/**
 * Utilisateur ayant accès à l'IHM.
 * 
 * @author nortaina
 *
 */
public final class UtilisateurIHM extends ParamUtilisateurGestionnaire implements Principal {
    /**
     * UID de version.
     */
    private static final long serialVersionUID = -7124630263039106628L;

    /**
     * Les numéros de contrats VIP sur lesquels l'utilisateur a des droits.
     * 
     * @return les numéros de contrats VIP sur lesquels l'utilisateur a des droits
     */
    @Getter
    private final List<String> numContratsVIP;

    /**
     * Constructeur d'un utilisateur de session IHM.
     * 
     * @param utilisateurGestionnaire
     *            l'utilisateur gestionnaire correspondant
     * @param numContratsVIP
     *            les numéros de contrats VIP sur lesquels l'utilisateur a des droits
     */
    public UtilisateurIHM(ParamUtilisateurGestionnaire utilisateurGestionnaire, List<String> numContratsVIP) {
        super();
        BeanUtils.copyProperties(utilisateurGestionnaire, this);
        this.numContratsVIP = Collections.unmodifiableList(numContratsVIP);
    }

    @Override
    public String getName() {
        return getIdentifiantActiveDirectory();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null || obj.getClass() != this.getClass()) {
            return false;
        }
        return StringUtils.equals(this.getIdentifiantActiveDirectory(), ((UtilisateurIHM) obj).getIdentifiantActiveDirectory());
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((this.getIdentifiantActiveDirectory() == null) ? 0 : this.getIdentifiantActiveDirectory().hashCode());
        return result;
    }

    @Override
    public String toString() {
        return new StringBuilder("Utilisateur IHM [").append(this.getIdentifiantActiveDirectory()).append("]").toString();
    }
}
