package fr.si2m.red.ihm.extensionscontrats;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.ModelAndView;

import fr.si2m.red.complement.ExtensionContratRepository;
import fr.si2m.red.complement.ExtensionsContratsCriteresRecherche;
import fr.si2m.red.contrat.ContratRepository;
import fr.si2m.red.ihm.FilAriane;
import fr.si2m.red.ihm.Pages;
import fr.si2m.red.ihm.RedacIhmController;
import fr.si2m.red.ihm.RoleRedac;
import fr.si2m.red.ihm.session.UtilisateurIHM;
import fr.si2m.red.parametrage.ParamCodeLibelle;
import fr.si2m.red.parametrage.ParamCodeLibelleRepository;

/**
 * Contrôleur des pages de gestion des extensions de contrats.
 * 
 * @author poidij
 * 
 */
@Controller
@RequestMapping("/extensionscontrats")
public class ExtensionsContratsController extends RedacIhmController {

    @Autowired
    private ExtensionContratRepository extensionContratRepository;

    @Autowired
    private ContratRepository contratRepository;

    @Autowired
    private ParamCodeLibelleRepository paramCodeLibelleRepository;

    private static final String CODE_VIP_FALSE = "N";

    /**
     * Récupère la page de recherche des extensions de contrats.
     * 
     * @param criteres
     *            les critères de la recherche
     * @param httpRequest
     *            la requête de récupération
     * 
     * @return le modèle et la vue de la page d'accueil
     */
    @Secured(RoleRedac.ROLE_REDCONSULT)
    @RequestMapping(value = "", method = RequestMethod.GET, produces = MediaType.TEXT_HTML_VALUE)
    @Transactional(propagation = Propagation.REQUIRES_NEW, rollbackFor = Throwable.class)
    public ModelAndView getExtensionsContrats(@RequestParam MultiValueMap<String, Object> criteres, HttpServletRequest httpRequest) {

        // Récupération des critères de recherche
        ExtensionsContratsCriteresRecherche criteresRecherche = ExtensionsContratsCriteresRecherche.from(criteres);

        UtilisateurIHM utilisateur = getUtilisateurAuthentifie();

        // Préparation des critères pour le bloc de synthèse
        if (criteres.isEmpty()) {
            initialisationGroupesGestion(utilisateur, criteresRecherche);
        }

        String libelleGroupeGestionUnique = criteresRecherche.getGroupesGestionAsList().size() == 1 ? paramCodeLibelleRepository
                .getLibelleCourt(ParamCodeLibelle.TABLE_CONTRAT, "NMGRPGES", criteresRecherche.getGroupesGestionAsList().get(0)) : null;

        List<String> listeFamilles = criteresRecherche.getFamillesAsList();
        String libelleFamilleUnique = listeFamilles.size() == 1
                ? paramCodeLibelleRepository.getLibelleCourt(ParamCodeLibelle.TABLE_CONTRAT, "NOFAM", listeFamilles.get(0)) : null;

        List<Integer> listeFamillesAsIntegers = new ArrayList<>(listeFamilles.size());
        for (String famille : listeFamilles) {
            listeFamillesAsIntegers.add(Integer.valueOf(famille));
        }

        // Initialisation du MAV
        ModelAndView mav = initialiseModeleVue("extensionscontrats/ExtensionsContratsRecherche", httpRequest,
                criteresRecherche.getGroupesGestionAsList(), libelleGroupeGestionUnique, listeFamillesAsIntegers, libelleFamilleUnique);

        // Info fil d'Ariane
        FilAriane filAriane = new FilAriane();
        filAriane.setFil(Arrays.asList(Pages.ACCUEIL, Pages.EXTENSIONSCONTRATS));
        mav.addObject("filAriane", filAriane);

        // On prend en compte une liste de contratVIP vide si tous peuvent être
        // consultés
        if (utilisateur.isDroitVIP()) {
            criteresRecherche.getFiltreContratsVIP().addAll(utilisateur.getNumContratsVIP());
        }

        // valeurs pour la recherche par defaut
        if (criteres.isEmpty()) {
            criteresRecherche.setTriChamp("numContrat");
            criteresRecherche.setVip(new String[] { CODE_VIP_FALSE });
        }

        // On effectue une recherche avec les valeurs par defaut du bloc de
        // recherche lors de l'initialisation/reinitialisation
        // = on effectue une recherche quoi qu'il arrive
        effectueRecherche(mav, httpRequest, criteresRecherche);

        // Données de référence pour la recherche
        Map<String, Object> reference = new HashMap<String, Object>();
        reference.put("listeGroupesGestion", getGroupesGestionsSelectionnables());
        reference.put("listeFamilles", getFamillesSelectionnables());
        reference.put("listeNatures", getNaturesContratsSelectionnables());
        reference.put("listeGestionsDirectes", getModesNatureContratSelectionnables());
        reference.put("listeEligibiliteContratDsn", getEligibilitesSelectionnables());
        reference.put("listeVIP", getVipSelectionnables());
        reference.put("listeIndicsExploitation", getIndicExploitationSelectionnables());
        reference.put("listeIndicsConsignePaiement", getIndicConsignePaiementSelectionnables());
        reference.put("listeIndicsTransfert", getIndicTransfertSelectionnables());

        mav.addObject("reference", reference);

        // Construction du contexte de la page
        Map<String, Object> contexte = new HashMap<String, Object>();
        contexte.put("criteres", criteresRecherche);
        contexte.put("reinitialisation", criteres.isEmpty());
        contexte.put("critereVipParDefaut", Arrays.asList(CODE_VIP_FALSE));
        String extensionContratCriteresRecherche = criteresRecherche.toQueryString();
        contexte.put("urlQueryString", extensionContratCriteresRecherche);
        contexte.put("extensionContratCriteresRecherche", StringUtils.replace(extensionContratCriteresRecherche, "&", ";"));
        mav.addObject("contexte", contexte);

        return mav;
    }

    /**
     * 
     * Détermine la valeur par defaut du GroupesGestion - F02_RG_1_08 à l'initialisation/réinitialisation.
     * 
     * @param utilisateur
     *            l'utilisateur associé à la session
     * @param criteresRecherche
     *            les critères de la recherche
     */
    private void initialisationGroupesGestion(UtilisateurIHM utilisateur, ExtensionsContratsCriteresRecherche criteresRecherche) {
        String groupeGestUser = utilisateur.getGroupeGestion();
        if (StringUtils.isNotBlank(groupeGestUser)) {
            for (ParamCodeLibelle groupegestion : getGroupesGestionsSelectionnables()) {
                if (StringUtils.equals(groupeGestUser, groupegestion.getCode())) {
                    criteresRecherche.setGroupesGestion(new String[] { groupeGestUser });
                }
            }
        }
    }

    /**
     * Effectue la recherche suivant les critères saisis
     * 
     * @param mav
     *            le modele-vue à afficher
     * @param httpRequest
     *            la requête
     * @param criteresRecherche
     *            les critères formatés pour REDAC
     */
    private void effectueRecherche(ModelAndView mav, HttpServletRequest httpRequest, ExtensionsContratsCriteresRecherche criteresRecherche) {
        long total = 0;
        boolean creerSpecificite = false;

        mav.addObject("resultatsExtensions", extensionContratRepository.rechercheExtensionsContrats(criteresRecherche));
        total = extensionContratRepository.compte(criteresRecherche);

        // Activation du bouton "Créer spécificité contrat"
        if (total == 0L) {
            mav.addObject("informations", "Aucun contrat pour cette sélection");

            if (StringUtils.isNotBlank(criteresRecherche.getNumContrat())) {
                creerSpecificite = contratRepository.existeContrat(criteresRecherche.getNumContrat());
            }
        }
        if (!(Boolean) getInfoUtilisateur(httpRequest).get(RoleRedac.ID_ROLE_GESTION_CDE)
                || extensionContratRepository.existeExtensionContrat(criteresRecherche.getNumContrat())) {
            creerSpecificite = false;
        }

        mav.addObject("resultatsMeta", creeResultatsInfoPagination(total, criteresRecherche));
        mav.addObject("creerSpecificite", creerSpecificite);
    }

    /**
     * Gestion des redirections vers la page de recherche de contrats par défaut.
     * 
     * @return la commande de redirection vers la page de recherche de contrats par défaut
     */
    @ResponseStatus(value = HttpStatus.MOVED_PERMANENTLY)
    @RequestMapping(value = "/*")
    public String redirect() {
        return "redirect:/ihm/extensionscontrats";
    }
}
