package fr.si2m.red.ihm.declarations;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.DefaultTransactionDefinition;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.ModelAndView;

import fr.si2m.red.DateRedac;
import fr.si2m.red.RedacUnexpectedException;
import fr.si2m.red.complement.ExtensionContrat;
import fr.si2m.red.complement.ExtensionContratRepository;
import fr.si2m.red.complement.ExtensionEntrepriseAffiliee;
import fr.si2m.red.complement.ExtensionEntrepriseAffilieeRepository;
import fr.si2m.red.contrat.Contrat;
import fr.si2m.red.contrat.ContratRepository;
import fr.si2m.red.contrat.TarifRepository;
import fr.si2m.red.dsn.AdhesionEtablissementMois;
import fr.si2m.red.dsn.BaseAssujettieRepository;
import fr.si2m.red.dsn.ComposantVersementRepository;
import fr.si2m.red.dsn.EtatPeriode;
import fr.si2m.red.dsn.ResumeAffiliation;
import fr.si2m.red.dsn.Versement;
import fr.si2m.red.ihm.ConfigurationIHM;
import fr.si2m.red.ihm.FilAriane;
import fr.si2m.red.ihm.Pages;
import fr.si2m.red.ihm.RedacIhmController;
import fr.si2m.red.ihm.RoleRedac;
import fr.si2m.red.ihm.exceptions.HttpException;
import fr.si2m.red.ihm.exceptions.MauvaiseRequeteException;
import fr.si2m.red.ihm.exceptions.RessourceNonAutoriseeException;
import fr.si2m.red.ihm.exceptions.RessourceNonTrouveeException;
import fr.si2m.red.ihm.session.UtilisateurIHM;
import fr.si2m.red.parametrage.ParamCodeLibelle;
import fr.si2m.red.parametrage.ParamCodeLibelleRepository;
import fr.si2m.red.parametrage.ParamControleSignalRejet;
import fr.si2m.red.parametrage.ParamControleSignalRejetRepository;
import fr.si2m.red.parametrage.ParamUtilisateurGestionnaireRepository;
import fr.si2m.red.parametrage.ParamValeurDefaut;
import fr.si2m.red.parametrage.ParamValeurDefautRepository;
import fr.si2m.red.parametrage.ResumeParamUtilisateurGestionnaire;
import fr.si2m.red.parametrage.Tranche;
import fr.si2m.red.reconciliation.ActionOngletChangementStatut;
import fr.si2m.red.reconciliation.CategorieQuittancementIndividuRepository;
import fr.si2m.red.reconciliation.CompteRenduIntegration;
import fr.si2m.red.reconciliation.CompteRenduIntegrationRepository;
import fr.si2m.red.reconciliation.EffectifCategorieMouvementRepository;
import fr.si2m.red.reconciliation.HistoriqueAssignationPeriode;
import fr.si2m.red.reconciliation.HistoriqueAssignationPeriodeRepository;
import fr.si2m.red.reconciliation.HistoriqueAttenteRetourEtpPeriode;
import fr.si2m.red.reconciliation.HistoriqueAttenteRetourEtpPeriodeRepository;
import fr.si2m.red.reconciliation.HistoriqueCommentairePeriode;
import fr.si2m.red.reconciliation.HistoriqueCommentairePeriodeRepository;
import fr.si2m.red.reconciliation.HistoriqueEtatPeriode;
import fr.si2m.red.reconciliation.HistoriqueEtatPeriodeRepository;
import fr.si2m.red.reconciliation.IndicateursDSNPeriode;
import fr.si2m.red.reconciliation.IndicateursDSNPeriodeRepository;
import fr.si2m.red.reconciliation.MessageControle;
import fr.si2m.red.reconciliation.MessageControleRepository;
import fr.si2m.red.reconciliation.PeriodeRecue;
import fr.si2m.red.reconciliation.PeriodeRecueRepository;
import fr.si2m.red.reconciliation.PeriodesRecuesCriteresRecherche;
import fr.si2m.red.reconciliation.RattachementDeclarationsRecuesRepository;
import fr.si2m.red.reconciliation.TrancheCategorieRepository;
import fr.si2m.red.reconciliation.TypeActionPopupChangementEnMasse;

/**
 * Contrôleur des pages de détails d'une DSN.
 * 
 * @author nortaina
 * 
 */
@Controller
@RequestMapping("/declarations")
public class DeclarationController extends RedacIhmController {
    private static final Logger LOGGER = LoggerFactory.getLogger(DeclarationController.class);

    @Autowired
    private HistoriqueEtatPeriodeRepository historiqueEtatPeriodeRepository;
    @Autowired
    private HistoriqueCommentairePeriodeRepository historiqueCommentairePeriodeRepository;
    @Autowired
    private HistoriqueAssignationPeriodeRepository historiqueAssignationPeriodeRepository;
    @Autowired
    private HistoriqueAttenteRetourEtpPeriodeRepository historiqueAttenteRetourEtpPeriodeRepository;
    @Autowired
    private PeriodeRecueRepository periodeRecueRepository;
    @Autowired
    private IndicateursDSNPeriodeRepository indicateursDSNPeriodeRepository;
    @Autowired
    private RattachementDeclarationsRecuesRepository rattachementDeclarationsRecuesRepository;
    @Autowired
    private CategorieQuittancementIndividuRepository categorieQuittancementIndividuRepository;
    @Autowired
    private EffectifCategorieMouvementRepository effectifCategorieMouvementRepository;
    @Autowired
    private TrancheCategorieRepository trancheCategorieRepository;
    @Autowired
    private CompteRenduIntegrationRepository compteRenduIntegrationRepository;
    @Autowired
    private MessageControleRepository messageControleRepository;
    @Autowired
    private ComposantVersementRepository composantVersementRepository;
    @Autowired
    private ExtensionEntrepriseAffilieeRepository extensionEntrepriseAffilieeRepository;
    @Autowired
    private ExtensionContratRepository extensionContratRepository;
    @Autowired
    private ContratRepository contratRepository;
    @Autowired
    private TarifRepository tarifRepository;
    @Autowired
    private ParamCodeLibelleRepository paramCodeLibelleRepository;
    @Autowired
    private ParamValeurDefautRepository paramValeurDefautRepository;
    @Autowired
    private ParamControleSignalRejetRepository paramControleSignalRejetRepository;
    @Autowired
    private ParamUtilisateurGestionnaireRepository paramUtilisateurGestionnaireRepository;
    @Autowired
    private DeclarationIndividusExportExcel declarationIndividusExportExcel;
    @Autowired
    private DeclarationIndividusExportExcelMontants declarationIndividusExportExcelMontant;
    @Autowired
    private DeclarationIndividusExportExcelMessages declarationIndividusExportExcelMessage;
    @Autowired
    private DeclarationSyntheseExportExcel declarationSyntheseExportExcel;

    @Autowired
    private ApplicationContext appContext;
    @Autowired
    private ConfigurationIHM configurationIHM;
    @Autowired
    private BaseAssujettieRepository baseAssujettieRepository;

    private static final String FORMAT_MMYYYY = "MMyyyy";

    private static final String LIEN = "-lien";

    /**
     * Récupère les messages d'alertes d'une période pour un certain niveau d'alerte.
     * 
     * @param idPeriode
     *            l'identifiant de la période DSN
     * @param niveauAlerte
     *            le niveau d'alerte des messages à récupérer
     * 
     * @return le modèle et la vue de la page
     * 
     * @throws RessourceNonTrouveeException
     *             levée si la période ou le contrat rattaché à la période ne sont pas trouvés
     */
    @Secured(RoleRedac.ROLE_REDCONSULT)
    @RequestMapping(value = "/{idPeriode}/messages/{niveauAlerte}", method = RequestMethod.GET, produces = MediaType.TEXT_PLAIN_VALUE)
    @Transactional(propagation = Propagation.REQUIRES_NEW, rollbackFor = Throwable.class)
    public ResponseEntity<String> getDeclarationMessagesControles(@PathVariable("idPeriode") Long idPeriode,
            @PathVariable("niveauAlerte") String niveauAlerte) throws RessourceNonTrouveeException {
        List<MessageControle> messagesControle = messageControleRepository.getPourPeriodeEtNiveauAlerte(idPeriode, niveauAlerte.toUpperCase());
        List<String> messages = new ArrayList<>(messagesControle.size());
        for (MessageControle messageControle : messagesControle) {
            messages.add(messageControle.getMessageUtilisateur());
        }
        return new ResponseEntity<String>(StringUtils.join(messages, "<br/>"), HttpStatus.OK);
    }

    /**
     * Récupère le nom - prénom à partir de l'identifiant
     * 
     * @param identifiantActiveDirectory
     *            identifiantUser
     * @return modèle and view
     * @throws RessourceNonTrouveeException
     *             levée si l'user n'est pas trouvé
     */
    @Secured(RoleRedac.ROLE_REDCONSULT)
    @RequestMapping(value = "/{idPeriode}/{identifiantActiveDirectory}", method = RequestMethod.GET, produces = MediaType.TEXT_PLAIN_VALUE)
    @Transactional(propagation = Propagation.REQUIRES_NEW, rollbackFor = Throwable.class)
    public ResponseEntity<String> getNomEtPenomUtilisateur(@PathVariable("identifiantActiveDirectory") String identifiantActiveDirectory)
            throws RessourceNonTrouveeException {

        String nomEtPenom = paramUtilisateurGestionnaireRepository.getNomEtPrenomAvecCodeUser(identifiantActiveDirectory);

        return new ResponseEntity<String>(StringUtils.join(nomEtPenom, "<br/>"), HttpStatus.OK);
    }

    /**
     * Récupère la page de synthèse d'une période DSN.
     * 
     * @param idPeriode
     *            l'identifiant de la période DSN
     * @param criteresRecherche
     *            les critères de recherche ayant permis d'accéder à cette période (facultatif)
     * @param httpRequest
     *            la requête de récupération
     * 
     * @return le modèle et la vue de la page
     * 
     * @throws HttpException
     *             levée si la période ou le contrat lié à la période n'existe pas ou si la période ou le contrat rattaché à la période n'est pas accessible
     */
    @Secured(RoleRedac.ROLE_REDCONSULT)
    @RequestMapping(value = "/{idPeriode}/synthese", method = RequestMethod.GET, produces = MediaType.TEXT_HTML_VALUE)
    @Transactional(propagation = Propagation.REQUIRES_NEW, rollbackFor = Throwable.class)
    public ModelAndView getDeclarationSynthese(@PathVariable("idPeriode") Long idPeriode,
            @RequestParam(value = "criteresRecherche", required = false) String criteresRecherche, HttpServletRequest httpRequest)
            throws HttpException {

        // Info générique sur la période de déclaration en paramètre
        PeriodeRecue periodeRecue = getPeriode(idPeriode);

        ModelAndView mav = initModeleVueDeclaration("declarations/DeclarationSynthese", httpRequest, periodeRecue);
        initFilAriane(mav);

        // Construction du contexte de la page
        initContexteRequete(mav, criteresRecherche, httpRequest);
        mav.addObject("moisDeclare", DateRedac.formate(FORMAT_MMYYYY,
                rattachementDeclarationsRecuesRepository.getDernierMoisDeclareAdhesionEtablissementMois(periodeRecue.getIdPeriode())));

        // Informations complémentaires pour la synthèse
        Map<String, Object> infoSynthese = new HashMap<>();
        ajouteInfoSynthese(infoSynthese, periodeRecue, (Contrat) mav.getModel().get("contrat"));
        mav.addObject("infoSynthese", infoSynthese);

        return mav;
    }

    /**
     * Récupère l'export Excel de la synthèse d'une déclaration.
     * 
     * @param idPeriode
     *            l'identifiant de la période
     * @param httpResponse
     *            la réponse à la requête de récupération
     * 
     * @throws HttpException
     *             levée si la période ou le contrat lié à la période n'existe pas ou si la période ou le contrat rattaché à la période n'est pas accessible
     */
    @Secured(RoleRedac.ROLE_REDCONSULT)
    @RequestMapping(value = "/{idPeriode}/synthese/exports/excel", method = RequestMethod.GET)
    @Transactional(propagation = Propagation.REQUIRES_NEW, rollbackFor = Throwable.class)
    @ResponseStatus(HttpStatus.OK)
    public void getExportSynthese(@PathVariable("idPeriode") Long idPeriode, HttpServletResponse httpResponse) throws HttpException {
        genereExtraction(declarationSyntheseExportExcel, "Synthese", idPeriode, httpResponse);
    }

    /**
     * Récupère l'export Excel de la synthèse allégé avec seulement les montants
     * 
     * @param idPeriode
     *            l'identifiant de la période
     * @param httpResponse
     *            la réponse à la requête de récupération
     * 
     * @throws HttpException
     *             levée si la période ou le contrat lié à la période n'existe pas ou si la période ou le contrat rattaché à la période n'est pas accessible
     * 
     * @Secured(RoleRedac.ROLE_REDCONSULT) @RequestMapping(value = "/{idPeriode}/synthese/exports/excelMontants", method = RequestMethod.GET)
     * @Transactional(propagation = Propagation.REQUIRES_NEW, rollbackFor = Throwable.class)
     * @ResponseStatus(HttpStatus.OK) public void getExportSyntheseMontant(@PathVariable("idPeriode") Long idPeriode, HttpServletResponse httpResponse) throws
     *                                HttpException { genereExtractionAbMt(declarationSyntheseExportExcel, "Export Allege Individus Mt Cotisation", idPeriode,
     *                                httpResponse); }
     */
    /**
     * Récupère l'export Excel de la synthèse allégé avec seulement les messages
     * 
     * @param idPeriode
     *            l'identifiant de la période
     * @param httpResponse
     *            la réponse à la requête de récupération
     * 
     * @throws HttpException
     *             levée si la période ou le contrat lié à la période n'existe pas ou si la période ou le contrat rattaché à la période n'est pas accessible
     * 
     * @Secured(RoleRedac.ROLE_REDCONSULT) @RequestMapping(value = "/{idPeriode}/synthese/exports/excelMessages", method = RequestMethod.GET)
     * @Transactional(propagation = Propagation.REQUIRES_NEW, rollbackFor = Throwable.class)
     * @ResponseStatus(HttpStatus.OK) public void getExportSyntheseMessage(@PathVariable("idPeriode") Long idPeriode, HttpServletResponse httpResponse) throws
     *                                HttpException { genereExtractionAbMt(declarationSyntheseExportExcel, "Export Allege Individus Messages", idPeriode,
     *                                httpResponse); }
     */
    /**
     * Récupère la page des DSN d'une période.
     * 
     * @param idPeriode
     *            l'identifiant de la période
     * @param criteresRecherche
     *            les critères de recherche ayant permis d'accéder à cette période (facultatif)
     * @param httpRequest
     *            la requête de récupération
     * 
     * @return le modèle et la vue de la page
     * 
     * @throws HttpException
     *             levée si la période ou le contrat lié à la période n'existe pas ou si la période ou le contrat rattaché à la période n'est pas accessible
     */
    @Secured(RoleRedac.ROLE_REDCONSULT)
    @RequestMapping(value = "/{idPeriode}/dsn", method = RequestMethod.GET, produces = MediaType.TEXT_HTML_VALUE)
    @Transactional(propagation = Propagation.REQUIRES_NEW, rollbackFor = Throwable.class)
    public ModelAndView getDeclarationDSN(@PathVariable("idPeriode") Long idPeriode,
            @RequestParam(value = "criteresRecherche", required = false) String criteresRecherche, HttpServletRequest httpRequest)
            throws HttpException {
        // Info générique sur la période de déclaration en paramètre
        PeriodeRecue periodeRecue = getPeriode(idPeriode);

        ModelAndView mav = initModeleVueDeclaration("declarations/DeclarationDSN", httpRequest, periodeRecue);
        initFilAriane(mav);

        // Données DSN à afficher
        initDonneesDSN(mav, periodeRecue);

        // Construction du contexte de la page
        initContexteRequete(mav, criteresRecherche, httpRequest);

        return mav;
    }

    /**
     * Récupère la page des individus d'une période DSN.
     * 
     * @param idPeriode
     *            l'identifiant de la période DSN
     * @param criteresRecherche
     *            les critères de recherche ayant permis d'accéder à cette période (facultatif)
     * @param httpRequest
     *            la requête de récupération
     * 
     * @return le modèle et la vue de la page
     * 
     * @throws HttpException
     *             levée si la période ou le contrat lié à la période n'existe pas ou si la période ou le contrat rattaché à la période n'est pas accessible ou
     *             si l'utilisateur n'a pas accès aux individus d'une déclaration
     */
    @Secured(RoleRedac.ROLE_REDCONSULT)
    @RequestMapping(value = "/{idPeriode}/individus", method = RequestMethod.GET, produces = MediaType.TEXT_HTML_VALUE)
    @Transactional(propagation = Propagation.REQUIRES_NEW, rollbackFor = Throwable.class)
    public ModelAndView getDeclarationIndividus(@PathVariable("idPeriode") Long idPeriode,
            @RequestParam(value = "criteresRecherche", required = false) String criteresRecherche, HttpServletRequest httpRequest)
            throws HttpException {
        // Sécurisation fonctionnelle
        UtilisateurIHM utilisateur = getUtilisateurAuthentifie();
        if (!utilisateur.isDroitIndividu()) {
            throw new RessourceNonAutoriseeException();
        }

        // Info générique sur la période de déclaration en paramètre
        PeriodeRecue periodeRecue = getPeriode(idPeriode);

        ModelAndView mav = initModeleVueDeclaration("declarations/DeclarationIndividus", httpRequest, periodeRecue);
        initFilAriane(mav);

        int pageTaille = 1001;
        int page = 0;

        // Récupération CodePopulation + vérification si Unique
        boolean codePopulationEstUnique = false;

        List<String> codesPopulationTarif = tarifRepository.getDistinctNumCategoriePourContratEtDate(periodeRecue.getNumeroContrat(),
                periodeRecue.getDateDebutPeriode());
        codePopulationEstUnique = codesPopulationTarif != null && codesPopulationTarif.size() == 1;

        String codePopulationUnique = StringUtils.EMPTY;
        if (codePopulationEstUnique) {
            codePopulationUnique = codesPopulationTarif.get(0);
        }

        // Récupération des affiliations...
        List<ResumeAffiliation> affiliations = rattachementDeclarationsRecuesRepository.getResumesAffiliationsDistinctesRattachesPagines(idPeriode,
                page * pageTaille, pageTaille);

        boolean affichagePartielIndividus = false;
        if (affiliations.size() > 1000) {
            affiliations.remove(affiliations.size() - 1);
            affichagePartielIndividus = true;
        }

        if (codePopulationEstUnique) {
            for (ResumeAffiliation affiliation : affiliations) {
                if (StringUtils.isEmpty(affiliation.getCodePopulation())) {
                    affiliation.setCodePopulation(codePopulationUnique);
                }
            }
        }

        mav.addObject("affiliations", affiliations);
        mav.addObject("affichagePartielIndividus", affichagePartielIndividus);

        // ... et des messages de contrôle sur individus
        Map<String, List<MessageControle>> messagesControlesIndividus = new HashMap<>(affiliations.size());
        for (ResumeAffiliation affiliation : affiliations) {
            String nir = affiliation.getIdentifiantRepertoire();
            String ntt = affiliation.getNumeroTechniqueTemporaire();
            if (!messagesControlesIndividus.containsKey(nir + "|" + ntt)) {
                messagesControlesIndividus.put(nir + "|" + ntt,
                        messageControleRepository.getPourPeriodeEtIndividu(idPeriode, StringUtils.isNotBlank(nir) ? nir : ntt));
            }
        }
        mav.addObject("messagesControlesIndividus", messagesControlesIndividus);

        // Construction du contexte de la page
        initContexteRequete(mav, criteresRecherche, httpRequest);

        return mav;
    }

    /**
     * Récupère l'export Excel des individus d'une déclaration.
     * 
     * @param idPeriode
     *            l'identifiant de la période
     * @param httpResponse
     *            la réponse à la requête de récupération
     * 
     * @throws HttpException
     *             levée si la période ou le contrat lié à la période n'existe pas ou si la période ou le contrat rattaché à la période n'est pas accessible ou
     *             si l'utilisateur n'a pas accès aux individus d'une déclaration
     */
    @Secured(RoleRedac.ROLE_REDCONSULT)
    @RequestMapping(value = "/{idPeriode}/individus/exports/excel", method = RequestMethod.GET)
    @Transactional(propagation = Propagation.REQUIRES_NEW, rollbackFor = Throwable.class)
    @ResponseStatus(HttpStatus.OK)
    public void getExportIndividus(@PathVariable("idPeriode") Long idPeriode, HttpServletResponse httpResponse) throws HttpException {
        // Sécurisation fonctionnelle
        UtilisateurIHM utilisateur = getUtilisateurAuthentifie();
        if (!utilisateur.isDroitIndividu()) {
            throw new RessourceNonAutoriseeException();
        }
        genereExtraction(declarationIndividusExportExcel, "Individus", idPeriode, httpResponse);
    }

    /**
     * Récupère l'export Excel des individus allégés avec montant de cotisation.
     * 
     * @param idPeriode
     *            l'identifiant de la période
     * @param httpResponse
     *            la réponse à la requête de récupération
     * 
     * @throws HttpException
     *             levée si la période ou le contrat lié à la période n'existe pas ou si la période ou le contrat rattaché à la période n'est pas accessible ou
     *             si l'utilisateur n'a pas accès aux individus d'une déclaration
     */
    @Secured(RoleRedac.ROLE_REDCONSULT)
    @RequestMapping(value = "/{idPeriode}/individus/exports/excelMontants", method = RequestMethod.GET)
    @Transactional(propagation = Propagation.REQUIRES_NEW, rollbackFor = Throwable.class)
    @ResponseStatus(HttpStatus.OK)
    public void getExportIndividusMontant(@PathVariable("idPeriode") Long idPeriode, HttpServletResponse httpResponse) throws HttpException {
        // Sécurisation fonctionnelle
        UtilisateurIHM utilisateur = getUtilisateurAuthentifie();
        if (!utilisateur.isDroitIndividu()) {
            throw new RessourceNonAutoriseeException();
        }
        genereExtractionAbMt(declarationIndividusExportExcelMontant, "Individus_Allegées_Montant", idPeriode, httpResponse);
    }

    /**
     * Récupère l'export Excel des individus allégés avec messages.
     * 
     * @param idPeriode
     *            l'identifiant de la période
     * @param httpResponse
     *            la réponse à la requête de récupération
     * 
     * @throws HttpException
     *             levée si la période ou le contrat lié à la période n'existe pas ou si la période ou le contrat rattaché à la période n'est pas accessible ou
     *             si l'utilisateur n'a pas accès aux individus d'une déclaration
     */
    @Secured(RoleRedac.ROLE_REDCONSULT)
    @RequestMapping(value = "/{idPeriode}/individus/exports/excelMessages", method = RequestMethod.GET)
    @Transactional(propagation = Propagation.REQUIRES_NEW, rollbackFor = Throwable.class)
    @ResponseStatus(HttpStatus.OK)
    public void getExportIndividusMessage(@PathVariable("idPeriode") Long idPeriode, HttpServletResponse httpResponse) throws HttpException {
        // Sécurisation fonctionnelle
        UtilisateurIHM utilisateur = getUtilisateurAuthentifie();
        if (!utilisateur.isDroitIndividu()) {
            throw new RessourceNonAutoriseeException();
        }
        genereExtractionAbMess(declarationIndividusExportExcelMessage, "Individus_Allégées_Message", idPeriode, httpResponse);
    }

    /**
     * Récupère la page de changement de statut d'une période DSN.
     * 
     * @param idPeriode
     *            l'identifiant de la période DSN
     * @param criteresRecherche
     *            les critères de recherche ayant permis d'accéder à cette période (facultatif)
     * @param httpRequest
     *            la requête de récupération
     * 
     * @return le modèle et la vue de la page
     * 
     * @throws HttpException
     *             levée si la période ou le contrat lié à la période n'existe pas ou si la période ou le contrat rattaché à la période n'est pas accessible ou
     *             si l'utilisateur n'a pas accès aux individus d'une déclaration
     */
    @Secured(RoleRedac.ROLE_REDCONSULT)
    @RequestMapping(value = "/{idPeriode}/statut", method = RequestMethod.GET, produces = MediaType.TEXT_HTML_VALUE)
    @Transactional(propagation = Propagation.REQUIRES_NEW, rollbackFor = Throwable.class)
    public ModelAndView getDeclarationChangementStatut(@PathVariable("idPeriode") Long idPeriode,
            @RequestParam(value = "criteresRecherche", required = false) String criteresRecherche, HttpServletRequest httpRequest)
            throws HttpException {

        // Info générique sur la période de déclaration en paramètre
        PeriodeRecue periodeRecue = getPeriode(idPeriode);

        ModelAndView mav = initModeleVueDeclaration("declarations/DeclarationChangementStatut", httpRequest, periodeRecue);
        initFilAriane(mav);

        // Données de référence pour le formulaire
        Map<String, Object> reference = new HashMap<String, Object>();

        EtatPeriode etatPeriode = periodeRecue.getEtatPeriode();

        reference.put("etatPeriode", etatPeriode.getCodeEtat());

        reference.put("droitBloquerPeriode",
                (Boolean) getInfoUtilisateur(httpRequest).get(RoleRedac.ID_ROLE_GESTION_REDAC) && "RCP".equals(etatPeriode.getCodeEtat()));

        MessageControle msgExistant = messageControleRepository.getPourPeriodeEtIdentifiantControleEtOrigine(idPeriode,
                MessageControle.IDENTIFIANT_CONTROLE_GEST_REJET, MessageControle.ORIGINE_MESSAGE_GEST);

        reference.put("estBloque", msgExistant != null);

        MessageControle msgAttenteRetourEntrepriseExistant = messageControleRepository.getPourPeriodeEtIdentifiantControleEtOrigine(idPeriode,
                MessageControle.IDENTIFIANT_CONTROLE_GEST_RETOUR_ETP, MessageControle.ORIGINE_MESSAGE_GEST);

        reference.put("attenteRetourEntreprise", msgAttenteRetourEntrepriseExistant != null);

        reference.put("droitAttenteRetourEntreprise", (Boolean) getInfoUtilisateur(httpRequest).get(RoleRedac.ID_ROLE_GESTION_REDAC));

        reference.put("droitCommentaire", (Boolean) getInfoUtilisateur(httpRequest).get(RoleRedac.ID_ROLE_GESTION_REDAC));

        reference.put("niveauAlerte", msgExistant != null ? msgExistant.getNiveauAlerte() : StringUtils.EMPTY);

        List<EtatPeriode> etatsDefinitifs = Arrays.asList(EtatPeriode.RCP, EtatPeriode.ENC, EtatPeriode.INT);
        boolean changementStatutImpossible = !(Boolean) getInfoUtilisateur(httpRequest).get(RoleRedac.ID_ROLE_GESTION_REDAC)
                || etatsDefinitifs.contains(etatPeriode);
        reference.put("changementStatutImpossible", changementStatutImpossible);

        List<ParamCodeLibelle> listeStatuts = paramCodeLibelleRepository.get(ParamCodeLibelle.TABLE_PERIODE_RECUE, "ETAT");
        HashMap<String, ParamCodeLibelle> mapStatuts = new HashMap<>(listeStatuts.size());

        for (ParamCodeLibelle paramCodeLibelle : listeStatuts) {
            mapStatuts.put(paramCodeLibelle.getCode(), paramCodeLibelle);
        }

        reference.put("mapStatuts", mapStatuts);

        List<ParamCodeLibelle> statutsSelectionnables = new ArrayList<>(0);
        if (!changementStatutImpossible) {
            if (Arrays.asList(EtatPeriode.ARI, EtatPeriode.SSG, EtatPeriode.TPG).contains(etatPeriode)) {
                statutsSelectionnables = Arrays.asList(mapStatuts.get(EtatPeriode.NIN.name()));
            } else if (etatPeriode == EtatPeriode.NIN) {
                statutsSelectionnables = Arrays.asList(mapStatuts.get(EtatPeriode.ARI.name()), mapStatuts.get(EtatPeriode.SSG.name()),
                        mapStatuts.get(EtatPeriode.TPG.name()));
            } else if (etatPeriode == EtatPeriode.INS) {
                statutsSelectionnables = Arrays.asList(mapStatuts.get(EtatPeriode.ING.name()));
            } else if (etatPeriode == EtatPeriode.ING) {
                statutsSelectionnables = Arrays.asList(mapStatuts.get(EtatPeriode.INS.name()));
            }
        }

        listeStatuts.retainAll(statutsSelectionnables);

        reference.put("listeStatuts", listeStatuts);

        reference.put("gestionnaireAffecteA",
                periodeRecue.getAffecteA() == null ? PeriodesRecuesCriteresRecherche.NON_AFFECTE_CODE : periodeRecue.getAffecteA());
        reference.put("gestionnaireATraiterPar",
                periodeRecue.getATraiterPar() == null ? PeriodesRecuesCriteresRecherche.NON_ASSIGNE_CODE : periodeRecue.getATraiterPar());

        List<ResumeParamUtilisateurGestionnaire> listeUsersAffecteA = paramUtilisateurGestionnaireRepository
                .getlistesAffecteAEtAtraiterParSansAutreGest(PeriodesRecuesCriteresRecherche.NON_AFFECTE_LIBELLE,
                        PeriodesRecuesCriteresRecherche.NON_AFFECTE_CODE);

        // on ajoute l'user absent, pour qu'il soit visible dans l'IHM, bien que plus présent dans le paramétrage
        if (periodeRecue.getAffecteA() != null && !existeCodeUser(listeUsersAffecteA, periodeRecue.getAffecteA())) {
            listeUsersAffecteA.add(0, new ResumeParamUtilisateurGestionnaire(periodeRecue.getAffecteA()));
        }
        reference.put("listeUsersAffecteA", listeUsersAffecteA);

        List<ResumeParamUtilisateurGestionnaire> listeUsersATraiterPar = paramUtilisateurGestionnaireRepository
                .getlistesAffecteAEtAtraiterParSansAutreGest(PeriodesRecuesCriteresRecherche.NON_ASSIGNE_LIBELLE,
                        PeriodesRecuesCriteresRecherche.NON_ASSIGNE_CODE);

        // on ajoute l'user absent, pour qu'il soit visible dans l'IHM, bien que plus présent dans le paramétrage
        if (periodeRecue.getATraiterPar() != null && !existeCodeUser(listeUsersATraiterPar, periodeRecue.getATraiterPar())) {
            listeUsersATraiterPar.add(0, new ResumeParamUtilisateurGestionnaire(periodeRecue.getATraiterPar()));
        }

        reference.put("listeUsersATraiterPar", listeUsersATraiterPar);

        reference.put("droitsSurLesListes", (Boolean) getInfoUtilisateur(httpRequest).get(RoleRedac.ID_ROLE_GESTION_CDE));

        mav.addObject("reference", reference);

        // Récupération de l'historique
        List<HistoriqueEtatPeriode> historiqueEtats = historiqueEtatPeriodeRepository.getHistoriqueEtatsPeriode(idPeriode);
        mav.addObject("historiqueEtats", historiqueEtats);

        // Récupération de l'historique des commentaires
        List<HistoriqueCommentairePeriode> historiqueCommentaires = historiqueCommentairePeriodeRepository
                .getHistoriqueCommentairesPeriode(idPeriode);
        mav.addObject("historiqueCommentaires", historiqueCommentaires);

        // Récupération de l'historique des assignations
        List<HistoriqueAssignationPeriode> historiqueAssignations = historiqueAssignationPeriodeRepository
                .getHistoriqueAssignationsPeriode(idPeriode);
        mav.addObject("historiqueAssignations", historiqueAssignations);

        // Récupération de l'historique attente retour entreprise
        List<HistoriqueAttenteRetourEtpPeriode> historiqueAttenteEtp = historiqueAttenteRetourEtpPeriodeRepository
                .getHistoriqueAttenteRetourEtpPeriode(idPeriode);
        mav.addObject("historiqueAttenteEtp", historiqueAttenteEtp);

        // Construction du contexte de la page
        initContexteRequete(mav, criteresRecherche, httpRequest);

        return mav;
    }

    /**
     * Publie un nouveau statut pour une période DSN et renvoie la page des changements de statut mise à jour.
     * 
     * @param idPeriode
     *            l'identifiant de la période DSN
     * @param historiqueEtatPeriode
     *            la version cible de l'état de période
     * @param actionOngletChangementStatut
     *            la version cible de l'état de l'onglet changement statut
     * @param flagBloquerPeriode
     *            Le flag du bouton de blocage de période
     * @param flagChangementStatut
     *            Le flag du boutton d'enregistrement de changement de statut
     * @param flagAffecterGestionnaire
     *            Le flag de l'affectation de gestionnaire à la période
     * @param flagAssignerGestionnaire
     *            Le flag de l'affectation de gestionnaire au traitement de la période
     * @param flagValiderRetourEntreprise
     *            Le flag de validation de retour entreprise
     * @param flagAjouterCommentaire
     *            Le flag d'ajout de commentaire
     * @param request
     *            la requête HTTP de demande de la page
     * @return la redirection vers la page de l'historique des statuts
     */
    @Secured(RoleRedac.ROLE_REDREDAC)
    @RequestMapping(value = "/{idPeriode}/statut", method = RequestMethod.POST, produces = MediaType.TEXT_HTML_VALUE)
    @ResponseStatus(code = HttpStatus.SEE_OTHER)
    public ResponseEntity<String> createDeclarationChangementStatut(@PathVariable("idPeriode") Long idPeriode,
            @ModelAttribute HistoriqueEtatPeriode historiqueEtatPeriode, @ModelAttribute ActionOngletChangementStatut actionOngletChangementStatut,
            @ModelAttribute HistoriqueCommentairePeriode historiqueCommentairePeriode,
            @ModelAttribute HistoriqueAssignationPeriode historiqueAssignationPeriode,
            @ModelAttribute HistoriqueAttenteRetourEtpPeriode historiqueAttenteRetourEtpPeriode,
            @RequestParam(required = false, value = "flagBloquerPeriode") String flagBloquerPeriode,
            @RequestParam(required = false, value = "flagAffecterGestionnaire") String flagAffecterGestionnaire,
            @RequestParam(required = false, value = "flagAssignerGestionnaire") String flagAssignerGestionnaire,
            @RequestParam(required = false, value = "flagValiderRetourEntreprise") String flagValiderRetourEntreprise,
            @RequestParam(required = false, value = "flagAjouterCommentaire") String flagAjouterCommentaire,
            @RequestParam(required = false, value = "flagChangementStatut") String flagChangementStatut, HttpServletRequest request) {

        boolean creationOK = true;
        String redirection = "/red-ihm/ihm/declarations/" + idPeriode.toString() + "/statut?" + request.getQueryString();

        // Transaction manuelle
        PlatformTransactionManager txManager = appContext.getBean(PlatformTransactionManager.class);
        TransactionStatus txStatus = txManager.getTransaction(new DefaultTransactionDefinition());
        try {
            PeriodeRecue periodeRecue = periodeRecueRepository.get(idPeriode);
            UtilisateurIHM utilisateur = getUtilisateurAuthentifie();

            List<Long> idsPeriodes = new ArrayList<Long>();

            idsPeriodes.add(idPeriode);

            if (flagAffecterGestionnaire != null) {
                periodeRecueRepository.modifieAuditUserEnMasse(idsPeriodes, utilisateur.getIdentifiantActiveDirectory(),
                        actionOngletChangementStatut.getGestionnaireAffecteA(), TypeActionPopupChangementEnMasse.AFFECTER);

                periodeRecueRepository.modifiegestionnaireAffecteA(idsPeriodes, actionOngletChangementStatut.getGestionnaireAffecteA());

            } else if (flagAssignerGestionnaire != null) {
                periodeRecueRepository.modifieAuditUserEnMasse(idsPeriodes, utilisateur.getIdentifiantActiveDirectory(),
                        actionOngletChangementStatut.getGestionnaireATraiterPar(), TypeActionPopupChangementEnMasse.ASSIGNER);
                periodeRecueRepository.modifieGestionnaireATraiterPar(idsPeriodes, actionOngletChangementStatut.getGestionnaireATraiterPar());

                // Ajout d'une ligne d'historique d'assignation
                creationHistoAssignation(periodeRecue, historiqueAssignationPeriode, actionOngletChangementStatut.getGestionnaireATraiterPar());

            } else if (flagValiderRetourEntreprise != null) {

                // Création ou suppression de ligne dans MessageControle
                if (actionOngletChangementStatut.isAttenteRetourEntreprise()) {
                    ParamControleSignalRejet paramControleSignalRejetGlobal = paramControleSignalRejetRepository
                            .getParamControleSignalRejetPourControleEtNofam(MessageControle.IDENTIFIANT_CONTROLE_GEST_RETOUR_ETP, "ALL");

                    periodeRecueRepository.modifieGestionnaireTraitePar(idsPeriodes, utilisateur.getIdentifiantActiveDirectory(), null,
                            TypeActionPopupChangementEnMasse.ATTENTE);
                    periodeRecueRepository.modifieAuditUserEnMasse(idsPeriodes, utilisateur.getIdentifiantActiveDirectory(), null,
                            TypeActionPopupChangementEnMasse.ATTENTE);
                    messageControleRepository.miseEnAttenteEnMasse(idsPeriodes, paramControleSignalRejetGlobal,
                            utilisateur.getIdentifiantActiveDirectory());

                    // Ajout d'une ligne d'historique d'assignation
                    creationHistoAttenteRetourEtp(periodeRecue, historiqueAttenteRetourEtpPeriode, TypeActionPopupChangementEnMasse.ATTENTE);

                } else {
                    periodeRecueRepository.modifieGestionnaireTraitePar(idsPeriodes, utilisateur.getIdentifiantActiveDirectory(), null,
                            TypeActionPopupChangementEnMasse.FIN_ATTENTE);
                    periodeRecueRepository.modifieAuditUserEnMasse(idsPeriodes, utilisateur.getIdentifiantActiveDirectory(), null,
                            TypeActionPopupChangementEnMasse.FIN_ATTENTE);
                    messageControleRepository.leveeMiseEnAttenteEnMasse(idsPeriodes);

                    // Ajout d'une ligne d'historique d'assignation
                    creationHistoAttenteRetourEtp(periodeRecue, historiqueAttenteRetourEtpPeriode, TypeActionPopupChangementEnMasse.FIN_ATTENTE);

                }

            } else if (flagBloquerPeriode != null) {

                if (actionOngletChangementStatut.isBloquerPeriodeCbx()) {

                    ParamControleSignalRejet paramControleSignalRejetGlobal = paramControleSignalRejetRepository
                            .getParamControleSignalRejetPourControleEtNofam(MessageControle.IDENTIFIANT_CONTROLE_GEST_REJET, "ALL");
                    periodeRecueRepository.modifieGestionnaireTraitePar(idsPeriodes, utilisateur.getIdentifiantActiveDirectory(), null,
                            TypeActionPopupChangementEnMasse.BLOCAGE);
                    periodeRecueRepository.modifieAuditUserEnMasse(idsPeriodes, utilisateur.getIdentifiantActiveDirectory(), null,
                            TypeActionPopupChangementEnMasse.BLOCAGE);
                    messageControleRepository.blocageEnMasse(idsPeriodes, paramControleSignalRejetGlobal,
                            utilisateur.getIdentifiantActiveDirectory());

                } else {
                    periodeRecueRepository.modifieGestionnaireTraitePar(idsPeriodes, utilisateur.getIdentifiantActiveDirectory(), null,
                            TypeActionPopupChangementEnMasse.DEBLOCAGE);
                    periodeRecueRepository.modifieAuditUserEnMasse(idsPeriodes, utilisateur.getIdentifiantActiveDirectory(), null,
                            TypeActionPopupChangementEnMasse.DEBLOCAGE);
                    messageControleRepository.deblocageEnMasse(idsPeriodes);

                }

            } else if (flagChangementStatut != null) {
                // Changement statut
                creationChangement(periodeRecue, historiqueEtatPeriode);

                String flagKey = "_creationOK=";
                String queryStringEtFlag;
                if (StringUtils.contains(request.getQueryString(), flagKey)) {
                    // On renverse la valeur du flag si elle est présente en valeur
                    // inversée
                    queryStringEtFlag = StringUtils.replace(request.getQueryString(), flagKey + !creationOK, flagKey + creationOK);
                } else {
                    // On crée un nouveau flag OK (le flag peut-être redondant si
                    // corruption mais cela reste sans effet)
                    String flag = flagKey + creationOK;
                    queryStringEtFlag = StringUtils.isNotBlank(request.getQueryString()) ? request.getQueryString() + "&" + flag : flag;
                }

                // rmq : champ traitePar / userMise à jour update & merge par hibernate dans creationChangement()

                redirection = "/red-ihm/ihm/declarations/" + idPeriode.toString() + "/statut?" + queryStringEtFlag;

            } else if (flagAjouterCommentaire != null) {
                periodeRecueRepository.modifieGestionnaireTraitePar(idsPeriodes, utilisateur.getIdentifiantActiveDirectory(), null,
                        TypeActionPopupChangementEnMasse.COMMENTAIRE);
                periodeRecueRepository.modifieAuditUserEnMasse(idsPeriodes, utilisateur.getIdentifiantActiveDirectory(), null,
                        TypeActionPopupChangementEnMasse.COMMENTAIRE);

                // Ajout d'un commentaire
                creationCommentaire(periodeRecue, historiqueCommentairePeriode);

                String flagKey = "_creationOK=";
                String queryStringEtFlag;
                if (StringUtils.contains(request.getQueryString(), flagKey)) {
                    // On renverse la valeur du flag si elle est présente en valeur
                    // inversée
                    queryStringEtFlag = StringUtils.replace(request.getQueryString(), flagKey + !creationOK, flagKey + creationOK);
                } else {
                    // On crée un nouveau flag OK (le flag peut-être redondant si
                    // corruption mais cela reste sans effet)
                    String flag = flagKey + creationOK;
                    queryStringEtFlag = StringUtils.isNotBlank(request.getQueryString()) ? request.getQueryString() + "&" + flag : flag;
                }

                // rmq : champ traitePar / userMise à jour update & merge par hibernate dans creationCommentaire()

                redirection = "/red-ihm/ihm/declarations/" + idPeriode.toString() + "/statut?" + queryStringEtFlag;

            } else {
                throw new MauvaiseRequeteException();
            }

            txManager.commit(txStatus);
        } catch (Exception e) {
            creationOK = false;
            LOGGER.error("Erreur lors de la création d'une entrée d'historique sur les états d'une période", e);
            // On rollback la transaction
            txManager.rollback(txStatus);
        }

        // Retour de la page en mode redirection
        return new ResponseEntity<String>(redirection, HttpStatus.SEE_OTHER);

    }

    /**
     * Crée la déclaration de changement statut.
     * 
     * @param periodeRecue
     *            la période traitée
     * 
     * @param historiqueEtatPeriode
     *            l'historique en cours de traitement pour la période
     * 
     * @throws MauvaiseRequeteException
     */
    private void creationChangement(PeriodeRecue periodeRecue, HistoriqueEtatPeriode historiqueEtatPeriode) throws MauvaiseRequeteException {

        Long idPeriode = periodeRecue.getIdPeriode();
        EtatPeriode nouvelEtatPeriode = historiqueEtatPeriode.getEtatPeriode();

        if (nouvelEtatPeriode == null) {
            throw new MauvaiseRequeteException();
        }

        // Supprimer le msg controle si l'etat demandee de la periode est ARI :F09_RG_6_07
        if ("ARI".equals(nouvelEtatPeriode.toString())) {
            messageControleRepository.supressionMessageControleEnMasse(Arrays.asList(idPeriode), MessageControle.ORIGINE_MESSAGE_GEST,
                    MessageControle.IDENTIFIANT_CONTROLE_GEST_REJET);
        }

        // Création de l'entrée d'historique
        historiqueEtatPeriode.setIdentifiantUtilisateur(getUtilisateurAuthentifie().getIdentifiantActiveDirectory());
        historiqueEtatPeriode.setIdPeriode(idPeriode);
        historiqueEtatPeriode.setOrigineChangement("IHM");
        historiqueEtatPeriode.setDateHeureChangement(DateRedac.convertitEnDateHeureRedac(Calendar.getInstance().getTime()));
        historiqueEtatPeriode.setAuditUtilisateurCreation(getUtilisateurAuthentifie().getIdentifiantActiveDirectory());
        historiqueEtatPeriodeRepository.create(historiqueEtatPeriode);

        // Mise à jour effective de la période
        periodeRecue.setEtatPeriode(nouvelEtatPeriode);
        periodeRecue.setAuditUtilisateurDerniereModification(getUtilisateurAuthentifie().getIdentifiantActiveDirectory());
        periodeRecue.setTraitePar(getUtilisateurAuthentifie().getIdentifiantActiveDirectory());
        periodeRecueRepository.modifieEntiteExistante(periodeRecue);
    }

    /**
     * Crée un commentaire.
     * 
     * @param periodeRecue
     *            la période traitée
     * 
     * @param historiqueCommentairePeriode
     *            l'historique des commentaires pour la période
     *
     * @throws MauvaiseRequeteException
     */
    private void creationCommentaire(PeriodeRecue periodeRecue, HistoriqueCommentairePeriode historiqueCommentairePeriode)
            throws MauvaiseRequeteException {

        Long idPeriode = periodeRecue.getIdPeriode();

        // Création de l'entrée d'historique
        historiqueCommentairePeriode.setIdentifiantUtilisateur(getUtilisateurAuthentifie().getIdentifiantActiveDirectory());
        historiqueCommentairePeriode.setIdPeriode(idPeriode);
        historiqueCommentairePeriode.setDateHeureSaisie(DateRedac.convertitEnDateHeureRedac(Calendar.getInstance().getTime()));
        historiqueCommentairePeriode.setAuditUtilisateurCreation(getUtilisateurAuthentifie().getIdentifiantActiveDirectory());
        historiqueCommentairePeriodeRepository.create(historiqueCommentairePeriode);
    }

    /**
     * Crée un historique d'assignation.
     * 
     * @param periodeRecue
     *            la période traitée
     * 
     * @param historiqueAssignationPeriode
     *            l'historique des assignations pour la période
     * 
     * @param aTraiterPar
     *            identifiant de l'utilisateur assigné
     * 
     * @throws MauvaiseRequeteException
     */
    private void creationHistoAssignation(PeriodeRecue periodeRecue, HistoriqueAssignationPeriode historiqueAssignationPeriode, String aTraiterPar)
            throws MauvaiseRequeteException {

        Long idPeriode = periodeRecue.getIdPeriode();

        // Création de l'entrée d'historique
        historiqueAssignationPeriode.setIdPeriode(idPeriode);
        historiqueAssignationPeriode.setDateHeureAssignation(DateRedac.convertitEnDateHeureRedac(Calendar.getInstance().getTime()));
        historiqueAssignationPeriode.setATraiterPar(aTraiterPar);
        historiqueAssignationPeriode.setAuditUtilisateurCreation(getUtilisateurAuthentifie().getIdentifiantActiveDirectory());
        historiqueAssignationPeriodeRepository.create(historiqueAssignationPeriode);
    }

    /**
     * Créée un historique d'attente retour etp.
     * 
     * @param periodeRecue
     *            la période traitée
     * 
     * @param historiqueAttenteRetourEtpPeriode
     *            l'historique des attente retour etp pour la période
     * 
     * @param action
     *            libellé de l'action
     * 
     * @throws MauvaiseRequeteException
     */
    private void creationHistoAttenteRetourEtp(PeriodeRecue periodeRecue, HistoriqueAttenteRetourEtpPeriode historiqueAttenteRetourEtpPeriode,
            TypeActionPopupChangementEnMasse action) throws MauvaiseRequeteException {

        Long idPeriode = periodeRecue.getIdPeriode();

        // Création de l'entrée d'historique
        historiqueAttenteRetourEtpPeriode.setIdPeriode(idPeriode);
        historiqueAttenteRetourEtpPeriode.setDateHeureRetour(DateRedac.convertitEnDateHeureRedac(Calendar.getInstance().getTime()));
        historiqueAttenteRetourEtpPeriode.setAction(action.getDesignation());
        historiqueAttenteRetourEtpPeriode.setIdentifiantUtilisateur(getUtilisateurAuthentifie().getIdentifiantActiveDirectory());
        historiqueAttenteRetourEtpPeriode.setAuditUtilisateurCreation(getUtilisateurAuthentifie().getIdentifiantActiveDirectory());
        historiqueAttenteRetourEtpPeriodeRepository.create(historiqueAttenteRetourEtpPeriode);
    }

    /**
     * Gestion des redirections vers la page de synthèse par défaut.
     * 
     * @param idPeriode
     *            l'identifiant de la période DSN
     * @param request
     *            la requête HTTP de demande de la page
     * 
     * @return la commande de redirection vers la page de synthèse par défaut
     */
    @ResponseStatus(value = HttpStatus.MOVED_PERMANENTLY)
    @RequestMapping(value = "/{idPeriode}")
    public String redirect(@PathVariable("idPeriode") Long idPeriode, HttpServletRequest request) {
        String queryString = request.getQueryString();
        return "redirect:/ihm/declarations/" + idPeriode + "/synthese" + (StringUtils.isNotBlank(queryString) ? "?" + queryString : "");
    }

    /**
     * Récupère la période ciblée par un identifiant donné.
     * 
     * @param idPeriode
     *            l'identifiant de la période
     * @return la période ciblée si elle est trouvée et accessible
     * @throws RessourceNonTrouveeException
     *             si la période n'existe pas
     * 
     */
    private PeriodeRecue getPeriode(Long idPeriode) throws RessourceNonTrouveeException {
        PeriodeRecue periodeRecue = periodeRecueRepository.get(idPeriode);
        if (periodeRecue == null) {
            throw new RessourceNonTrouveeException();
        }
        return periodeRecue;
    }

    /**
     * Initialise le fil d'Ariane pour une page gérée par ce contrôleur.
     * 
     * @param mav
     *            le modèle et la vue de la page
     */
    private void initFilAriane(ModelAndView mav) {
        FilAriane filAriane = new FilAriane();
        filAriane.setFil(Arrays.asList(Pages.ACCUEIL, Pages.PERIODE));
        mav.addObject("filAriane", filAriane);
    }

    /**
     * Initialise dans le modèle les informations sur la période visée.
     * 
     * @param mav
     *            le modèle-vue
     * @param periodeRecue
     *            la période ciblée
     * @return le contrat lié à la déclaration
     * 
     * @throws RessourceNonTrouveeException
     *             si le contrat lié à la période est introuvable
     * @throws RessourceNonAutoriseeException
     *             si le contrat lié à la période n'est pas accessible à l'utilisateur courant
     */
    private ModelAndView initModeleVueDeclaration(String pageCiblee, HttpServletRequest httpRequest, PeriodeRecue periodeRecue)
            throws RessourceNonTrouveeException, RessourceNonAutoriseeException {
        String numContrat = periodeRecue.getNumeroContrat();
        if (!verifieAccesContrat(numContrat)) {
            throw new RessourceNonAutoriseeException();
        }

        Contrat contrat = contratRepository.getContratValidePourDate(numContrat, periodeRecue.getDateFinPeriode());
        // Peut être null......

        ModelAndView mav;
        if (contrat != null) {
            mav = initialiseModeleVue(pageCiblee, httpRequest, Arrays.asList(contrat.getGroupeGestion()),
                    paramCodeLibelleRepository.getLibelleCourt(ParamCodeLibelle.TABLE_CONTRAT, "NMGRPGES", contrat.getGroupeGestion()),
                    Arrays.asList(contrat.getNumFamilleProduit()),
                    paramCodeLibelleRepository.getLibelleCourt(ParamCodeLibelle.TABLE_CONTRAT, "NOFAM", contrat.getNumFamilleProduit().toString()));
        } else {
            mav = initialiseModeleVue(pageCiblee, httpRequest);
        }
        mav.addObject("contrat", contrat);

        mav.addObject("periode", periodeRecue);
        AdhesionEtablissementMois adhesionsEtablissement = rattachementDeclarationsRecuesRepository
                .getDerniereAdhesionEtablissementMoisRattachee(periodeRecue.getIdPeriode());
        mav.addObject("raisonSociale", adhesionsEtablissement.getRaisonSociale());

        List<PeriodeRecue> periodesLiees = periodeRecueRepository.getPeriodesRecuesPourContrat(numContrat);
        int periodeIdx = 0;
        while (periodeIdx < periodesLiees.size()) {
            PeriodeRecue periodeLiee = periodesLiees.get(periodeIdx);
            if (periodeLiee.getId().equals(periodeRecue.getId())) {
                break;
            }
            periodeIdx++;
        }
        mav.addObject("idPeriodePrecedente", periodeIdx > 0 ? periodesLiees.get(periodeIdx - 1) : null);
        mav.addObject("idPeriodeSuivante", periodeIdx < periodesLiees.size() - 1 ? periodesLiees.get(periodeIdx + 1) : null);

        Map<String, Object> declaration = new HashMap<String, Object>();
        Long idPeriode = periodeRecue.getIdPeriode();
        declaration.put("idPeriode", idPeriode);
        declaration.put("libellePeriode", periodeRecueRepository.getLibellePeriode(periodeRecue));
        declaration.put("messagesControles", messageControleRepository.getGenerauxPourPeriode(idPeriode));
        mav.addObject("declaration", declaration);

        return mav;

    }

    /**
     * Initialise dans le modèle du contexte de requête d'une page de détail de déclaration.
     * 
     * @param mav
     *            le modèle-vue
     * @param criteresRecherche
     *            les critères de recherche sous forme textuelle (avec ';' comme séparateur de critères)
     * @param httpRequest
     *            la requête HTTP
     */
    private void initContexteRequete(ModelAndView mav, String criteresRecherche, HttpServletRequest httpRequest) {
        Map<String, Object> contexte = new HashMap<String, Object>();
        contexte.put("queryString", httpRequest.getQueryString());
        contexte.put("rechercheUrlQueryString", StringUtils.replace(StringUtils.defaultString(criteresRecherche), ";", "&"));
        mav.addObject("contexte", contexte);
    }

    /**
     * Initialise les données DSN d'une période donnée.
     * 
     * @param mav
     *            le modèle-vue à renseigner avec les données DSN de la période
     * @param periodeRecue
     *            la période
     */
    private void initDonneesDSN(ModelAndView mav, PeriodeRecue periodeRecue) {
        List<Integer> moisPeriode = resoudMoisADeclarer(periodeRecue);

        List<AdhesionEtablissementMois> adhesionsEtablissements = rattachementDeclarationsRecuesRepository
                .getAdhesionsEtablissementsMoisRattaches(periodeRecue.getIdPeriode());
        // Tri naturel et unicité avec TreeMap
        Map<String, Map<String, String>> adhesionsParSirenNic = new TreeMap<>();
        Map<Integer, Boolean> moisAvecDeclaration = new HashMap<>();
        for (AdhesionEtablissementMois adhesionEtablissementMois : adhesionsEtablissements) {
            String sirenNic = adhesionEtablissementMois.getSirenEntreprise() + adhesionEtablissementMois.getNicEtablissement();
            String sirenNicLocaliteEtablissement = adhesionEtablissementMois.getSirenEntreprise() + " "
                    + adhesionEtablissementMois.getNicEtablissement() + " - " + adhesionEtablissementMois.getLocaliteEtablissement();
            Map<String, String> adhesionEtablissement = adhesionsParSirenNic.get(sirenNic);

            Integer moisRattachement = adhesionEtablissementMois.getMoisRattachement();
            String moisRattachementStr = moisRattachement.toString();
            if (adhesionEtablissement == null) {
                adhesionEtablissement = new HashMap<>();
                adhesionEtablissement.put("sirenNicLocalite", sirenNicLocaliteEtablissement);
                adhesionsParSirenNic.put(sirenNic, adhesionEtablissement);
            } else {
                String dateFichier = adhesionEtablissement.get(moisRattachementStr);
                if (dateFichier == null || Integer.parseInt(dateFichier) < adhesionEtablissementMois.getDateFichier()) {
                    adhesionEtablissement.put("sirenNicLocalite", sirenNicLocaliteEtablissement);
                }
            }

            moisAvecDeclaration.put(moisRattachement, true);
            adhesionEtablissement.put(moisRattachementStr, adhesionEtablissementMois.getDateFichier().toString());

            if (PeriodeRecue.TYPE_PERIODE_REGULARISATION.equals(periodeRecue.getTypePeriode())) {
                String lienVersEtabBriqueDSN = new StringBuilder(configurationIHM.getUrlBriqueDsn())
                        .append("rechercherConsultationSyntheseEntreprise.xhtml?siret=").append(sirenNic).append("&moisPaieDeclare=")
                        .append(DateRedac.formate(FORMAT_MMYYYY, adhesionEtablissementMois.getMoisDeclare())).toString();
                adhesionEtablissement.put(moisRattachementStr + LIEN, lienVersEtabBriqueDSN);
            } else if ((PeriodeRecue.TYPE_PERIODE_DECLARATION.equals(periodeRecue.getTypePeriode())
                    || PeriodeRecue.TYPE_PERIODE_COMPLEMENT.equals(periodeRecue.getTypePeriode()))
                    && (adhesionEtablissementMois.getMoisDeclare().intValue() == adhesionEtablissementMois.getMoisRattachement().intValue())) {
                String lienVersEtabBriqueDSN = new StringBuilder(configurationIHM.getUrlBriqueDsn())
                        .append("rechercherConsultationSyntheseEntreprise.xhtml?siret=").append(sirenNic).append("&moisPaieDeclare=")
                        .append(DateRedac.formate(FORMAT_MMYYYY, adhesionEtablissementMois.getMoisDeclare())).toString();
                adhesionEtablissement.put(moisRattachementStr + LIEN, lienVersEtabBriqueDSN);
            } else {
                // Si la variable est empty, on affichera pas le lien dans l'IHM
                adhesionEtablissement.put(moisRattachementStr + LIEN, StringUtils.EMPTY);
            }

        }
        Map<String, Object> dsn = new HashMap<>();
        dsn.put("moisPeriode", moisPeriode);
        dsn.put("moisAvecDeclaration", moisAvecDeclaration);
        dsn.put("adhesionsEtablissements", adhesionsParSirenNic);
        mav.addObject("dsn", dsn);
    }

    /**
     * Résoud l'ensemble des mois à déclarer sur une période donnée.
     * 
     * @param periodeRecue
     *            la période
     * @return l'ensemble des mois à déclarer sur la période
     */
    private List<Integer> resoudMoisADeclarer(PeriodeRecue periodeRecue) {
        List<Integer> moisADeclarer = new ArrayList<>();
        Date dateDebutPeriode = DateRedac.convertitEnDate(periodeRecue.getDateDebutPeriode());
        Date dateFinPeriode = DateRedac.convertitEnDate(periodeRecue.getDateFinPeriode());
        Calendar calendrier = Calendar.getInstance();
        calendrier.setTime(dateDebutPeriode);
        calendrier.set(Calendar.DAY_OF_MONTH, 1);
        while (calendrier.getTime().before(dateFinPeriode)) {
            moisADeclarer.add(DateRedac.convertitEnDateRedac(calendrier.getTime()));
            calendrier.add(Calendar.MONTH, 1);
        }
        return moisADeclarer;
    }

    /**
     * Génère l'export Excel pour une déclaration.
     * 
     * @param idPeriode
     *            l'identifiant de la période
     * @param httpResponse
     *            la réponse à la requête de récupération de l'export
     * 
     * @throws HttpException
     *             levée si la période ou le contrat lié à la période n'existe pas ou si la période ou le contrat rattaché à la période n'est pas accessible ou
     *             si l'utilisateur n'a pas accès aux individus d'une déclaration
     */
    private void genereExtraction(DeclarationExportExcel declarationExportExcel, String nomFichier, Long idPeriode, HttpServletResponse httpResponse)
            throws HttpException {
        PeriodeRecue periodeRecue = getPeriode(idPeriode);
        String numContrat = periodeRecue.getNumeroContrat();
        if (!verifieAccesContrat(numContrat)) {
            throw new RessourceNonAutoriseeException();
        }
        Contrat contrat = contratRepository.getContratValidePourDate(numContrat, periodeRecue.getDateFinPeriode());
        try {
            httpResponse.setContentType("application/vnd.ms-excel");
            httpResponse.setHeader("Content-Disposition", "attachment; filename=" + nomFichier + ".xlsx");
            declarationExportExcel.exporte(periodeRecue, contrat, httpResponse.getOutputStream());
            httpResponse.flushBuffer();
        } catch (IOException e) {
            httpResponse.reset();
            httpResponse.setHeader("Content-Type", "text/plain; charset=utf-8");
            throw new RedacUnexpectedException(e);
        } catch (Exception e) {
            httpResponse.reset();
            httpResponse.setHeader("Content-Type", "text/plain; charset=utf-8");
            throw e;
        }
    }

    /**
     * Génère l'export Excel Abrégé avec Montant
     * 
     * @param idPeriode
     *            l'identifiant de la période
     * @param httpResponse
     *            la réponse à la requête de récupération de l'export
     * 
     * @throws HttpException
     *             levée si la période ou le contrat lié à la période n'existe pas ou si la période ou le contrat rattaché à la période n'est pas accessible ou
     *             si l'utilisateur n'a pas accès aux individus d'une déclaration
     */
    private void genereExtractionAbMt(DeclarationExportExcel declarationExportExcel, String nomFichier, Long idPeriode,
            HttpServletResponse httpResponse) throws HttpException {
        PeriodeRecue periodeRecue = getPeriode(idPeriode);
        String numContrat = periodeRecue.getNumeroContrat();
        if (!verifieAccesContrat(numContrat)) {
            throw new RessourceNonAutoriseeException();
        }
        Contrat contrat = contratRepository.getContratValidePourDate(numContrat, periodeRecue.getDateFinPeriode());
        try {
            httpResponse.setContentType("application/vnd.ms-excel");
            httpResponse.setHeader("Content-Disposition", "attachment; filename=" + nomFichier + ".xlsx");
            declarationExportExcel.exporteMontant(periodeRecue, contrat, httpResponse.getOutputStream());
            httpResponse.flushBuffer();
        } catch (IOException e) {
            httpResponse.reset();
            httpResponse.setHeader("Content-Type", "text/plain; charset=utf-8");
            throw new RedacUnexpectedException(e);
        } catch (Exception e) {
            httpResponse.reset();
            httpResponse.setHeader("Content-Type", "text/plain; charset=utf-8");
            throw e;
        }
    }

    /**
     * Génère l'export Excel Abrégé avec Messages
     * 
     * @param idPeriode
     *            l'identifiant de la période
     * @param httpResponse
     *            la réponse à la requête de récupération de l'export
     * 
     * @throws HttpException
     *             levée si la période ou le contrat lié à la période n'existe pas ou si la période ou le contrat rattaché à la période n'est pas accessible ou
     *             si l'utilisateur n'a pas accès aux individus d'une déclaration
     */
    private void genereExtractionAbMess(DeclarationExportExcel declarationExportExcel, String nomFichier, Long idPeriode,
            HttpServletResponse httpResponse) throws HttpException {
        PeriodeRecue periodeRecue = getPeriode(idPeriode);
        String numContrat = periodeRecue.getNumeroContrat();
        if (!verifieAccesContrat(numContrat)) {
            throw new RessourceNonAutoriseeException();
        }
        Contrat contrat = contratRepository.getContratValidePourDate(numContrat, periodeRecue.getDateFinPeriode());
        try {
            httpResponse.setContentType("application/vnd.ms-excel");
            httpResponse.setHeader("Content-Disposition", "attachment; filename=" + nomFichier + ".xlsx");
            declarationExportExcel.exporteMessage(periodeRecue, contrat, httpResponse.getOutputStream());
            httpResponse.flushBuffer();
        } catch (IOException e) {
            httpResponse.reset();
            httpResponse.setHeader("Content-Type", "text/plain; charset=utf-8");
            throw new RedacUnexpectedException(e);
        } catch (Exception e) {
            httpResponse.reset();
            httpResponse.setHeader("Content-Type", "text/plain; charset=utf-8");
            throw e;
        }
    }

    private void ajouteInfoSynthese(Map<String, Object> infoSynthese, PeriodeRecue periodeRecue, Contrat contrat) {
        Long idPeriode = periodeRecue.getIdPeriode();

        // Nature contrat
        Integer natureContrat = tarifRepository.getNatureContratPourPeriodeMinimalNoCat(periodeRecue.getNumeroContrat(),
                periodeRecue.getDateDebutPeriode(), periodeRecue.getDateFinPeriode());
        infoSynthese.put("natureContratPourPeriodeMinimalNoCat", natureContrat);

        infoSynthese.put("natureContrat", paramCodeLibelleRepository.getLibelleCourt(ParamCodeLibelle.TABLE_TARIF, "MOCALCOT",
                natureContrat != null ? natureContrat.toString() : null));

        // Date résiliation
        MessageControle messageResiliation = messageControleRepository.getPourPeriodeEtIdentifiantControle(periodeRecue.getIdPeriode(),
                ParamControleSignalRejet.ID_CONTRATS_RESILIES);
        infoSynthese.put("niveauResiliation", messageResiliation != null ? messageResiliation.getNiveauAlerte() : null);
        infoSynthese.put("dateResiliation", messageResiliation != null ? messageResiliation.getParamMessage1() : null);

        // Entreprises / établissements liées et date de dernière réception DSN
        ajouteInfoSyntheseEtablissementsLiesEtDateDerniereReceptionDSN(infoSynthese, idPeriode, periodeRecue.getNumeroContrat());

        // Statut de la période
        infoSynthese.put("statut",
                paramCodeLibelleRepository.getLibelleCourt(ParamCodeLibelle.TABLE_PERIODE_RECUE, "ETAT", periodeRecue.getEtatPeriode().name()));
        // Nature de la période
        infoSynthese.put("nature",
                paramCodeLibelleRepository.getLibelleCourt(ParamCodeLibelle.TABLE_PERIODE_RECUE, "TYPE", periodeRecue.getTypePeriode()));

        if (contrat != null) {
            // Famille
            infoSynthese.put("famille",
                    paramCodeLibelleRepository.getLibelleCourt(ParamCodeLibelle.TABLE_CONTRAT, "NOFAM", contrat.getNumFamilleProduit().toString()));

            // Extension du contrat
            ExtensionContrat extensionContrat = extensionContratRepository.getExtensionContrat(contrat.getNumContrat());
            infoSynthese.put("extensionContrat", extensionContrat);

            if (extensionContrat != null) {
                infoSynthese.put("vip",
                        paramCodeLibelleRepository.getLibelleCourt(ParamCodeLibelle.TABLE_EXTENSION_CONTRAT, "VIP", extensionContrat.getVipAsText()));
            } else {
                ParamValeurDefaut paramValeurDefaut = paramValeurDefautRepository.getParamValeurDefaut();
                infoSynthese.put("vip", paramCodeLibelleRepository.getLibelleCourt(ParamCodeLibelle.TABLE_EXTENSION_CONTRAT, "VIP",
                        paramValeurDefaut.getContratVIPAsText()));
            }
        }

        // Indicateurs DSN de la période
        IndicateursDSNPeriode indicateursDsn = indicateursDSNPeriodeRepository.getIndicateursPourPeriode(periodeRecue.getNumeroContrat(),
                periodeRecue.getDateDebutPeriode(), periodeRecue.getDateFinPeriode());
        infoSynthese.put("indicateursDsn", indicateursDsn);

        // Compte-rendu d'intégration de la période
        CompteRenduIntegration compteRenduIntegration = compteRenduIntegrationRepository.getPourPeriode(idPeriode);
        infoSynthese.put("compteRenduIntegration", compteRenduIntegration);
        // Nb d'établissements
        infoSynthese.put("nbEtablissements", rattachementDeclarationsRecuesRepository.compteNbEtablissementsRattaches(idPeriode));
        MessageControle messageVarNbEtablissements = messageControleRepository.getPourPeriodeEtIdentifiantControle(periodeRecue.getIdPeriode(),
                ParamControleSignalRejet.ID_VARIATION_NB_ETABLISSEMENTS);
        infoSynthese.put("niveauNbEtablissements", messageVarNbEtablissements != null ? messageVarNbEtablissements.getNiveauAlerte() : null);
        // Nb de salariés
        infoSynthese.put("nbSalaries", rattachementDeclarationsRecuesRepository.compteNbSalariesRattaches(idPeriode));
        MessageControle messageVarNbSalaries = messageControleRepository.getPourPeriodeEtIdentifiantControle(periodeRecue.getIdPeriode(),
                ParamControleSignalRejet.ID_VARIATION_NB_SALARIES);
        infoSynthese.put("niveauNbSalaries", messageVarNbSalaries != null ? messageVarNbSalaries.getNiveauAlerte() : null);
        // Nb contrats de travail

        // Syntaxe SQL invalide pour H2 - commenter la ligne pour ne plus avoir d'erreur technique dans l'IHM sur localhost )
        infoSynthese.put("nbContratsTravail", rattachementDeclarationsRecuesRepository.compteContratsTravailRattachesDistincts(idPeriode));

        // Nb fins de contrats de travail
        infoSynthese.put("nbFinsContrat", rattachementDeclarationsRecuesRepository.compteFinsDeContratsRattachees(idPeriode));

        // Consolidation
        ajouteInfoSyntheseConsolidation(infoSynthese, natureContrat, idPeriode);

        if (contrat != null) {
            infoSynthese.put("gestion", determineGestion(indicateursDsn, contrat.getLibelleGestion()));

            // Cotisations des établissements
            MessageControle messageControlePresenceCotisations = messageControleRepository.getPourPeriodeEtIdentifiantControle(idPeriode,
                    ParamControleSignalRejet.ID_PRESENCE_COTISATION_ETABLISSEMENT);
            infoSynthese.put("niveauAlertePresenceCotisation",
                    messageControlePresenceCotisations != null ? messageControlePresenceCotisations.getNiveauAlerte() : StringUtils.EMPTY);
            infoSynthese.put("cotisationsEtablissements", rattachementDeclarationsRecuesRepository.getCotisationsEtablissementsRattachees(idPeriode));
        }

        // Versements
        ajouteInfoSyntheseVersements(infoSynthese, idPeriode);
    }

    /**
     * Détermine si la période doit être affichée en gestion "déléguée" ou "directe".
     * 
     * @param indicateursDsn
     *            l'indicateur de la période
     * @param libelleGestion
     *            le libellé de gestion du contrat
     * @return le mode de gestion déterminé pour la période
     */
    private String determineGestion(IndicateursDSNPeriode indicateursDsn, String libelleGestion) {
        String gestion = libelleGestion;
        if (indicateursDsn != null) {
            if (StringUtils.equals(indicateursDsn.getModeNatureContrat(), "L")
                    || (StringUtils.equals(indicateursDsn.getModeNatureContrat(), "C") && StringUtils.equals(gestion, "Déléguée"))) {
                gestion = "Déléguée";
            } else {
                gestion = "Directe";
            }
        }
        return gestion;
    }

    private void ajouteInfoSyntheseEtablissementsLiesEtDateDerniereReceptionDSN(Map<String, Object> infoSynthese, Long idPeriode, String numContrat) {
        Integer dateDerniereReceptionDSN = rattachementDeclarationsRecuesRepository.getDateDerniereReceptionDSN(idPeriode);

        List<ExtensionEntrepriseAffiliee> entreprisesAffiliees = extensionEntrepriseAffilieeRepository.getEntreprisesAffiliees(numContrat);
        List<AdhesionEtablissementMois> etablissementsLies = new ArrayList<>();

        for (ExtensionEntrepriseAffiliee entrepriseAffiliee : entreprisesAffiliees) {
            AdhesionEtablissementMois etablissementLie = rattachementDeclarationsRecuesRepository
                    .getDerniereAdhesionEtablissementMoisRattachee(idPeriode, entrepriseAffiliee.getSiren(), entrepriseAffiliee.getNic());
            if (etablissementLie == null) {
                etablissementLie = new AdhesionEtablissementMois();
                etablissementLie.setSirenEntreprise(entrepriseAffiliee.getSiren());
                etablissementLie.setNicEtablissement(entrepriseAffiliee.getNic());
            }
            etablissementsLies.add(etablissementLie);
        }
        infoSynthese.put("etablissementsLies", etablissementsLies);
        infoSynthese.put("dateDerniereReceptionDSN", DateRedac.convertitEnDate(dateDerniereReceptionDSN));
    }

    private void ajouteInfoSyntheseConsolidation(Map<String, Object> infoSynthese, Integer natureContrat, Long idPeriode) {
        Map<String, Object> consolidation = new HashMap<>();
        infoSynthese.put("consolidation", consolidation);

        List<String> numerosCategorieQuittancement = categorieQuittancementIndividuRepository.getNumerosCategorieDistinctsPourPeriode(idPeriode);
        consolidation.put("numerosCategorieQuittancement", numerosCategorieQuittancement);

        boolean existeBaseAssujetiePourPeriode = baseAssujettieRepository.existeBaseAssujettiePourPeriode(idPeriode);
        infoSynthese.put("existeBaseAssujetiePourPeriode", existeBaseAssujetiePourPeriode);

        boolean existeCategorieQuittancementIndividuPourPeriode = categorieQuittancementIndividuRepository
                .existeCategorieQuittancementIndividuPourPeriode(idPeriode);
        infoSynthese.put("existeCategorieQuittancementIndividuPourPeriode", existeCategorieQuittancementIndividuPourPeriode);
        // F09_RG_3_01
        if (existeBaseAssujetiePourPeriode && existeCategorieQuittancementIndividuPourPeriode) {
            // Cotisations
            List<Double> montantsCotisations = categorieQuittancementIndividuRepository
                    .getSommesMontantsCotisationPourCategoriesQuittancement(idPeriode, numerosCategorieQuittancement);
            Double montantTotalCotisations = categorieQuittancementIndividuRepository.getSommeMontantsCotisationDeclare(idPeriode);
            consolidation.put("montantsCotisations", montantsCotisations);
            consolidation.put("montantTotalCotisations", montantTotalCotisations);

            if (natureContrat != null && natureContrat == 1) {
                // Sur effectif : effectifs et mouvements d'effectifs
                consolidation.put("effectifsDebut", categorieQuittancementIndividuRepository
                        .getEffectifsDebutPourPeriodeEtCategoriesQuittancement(idPeriode, numerosCategorieQuittancement));
                consolidation.put("effectifTotalDebut", categorieQuittancementIndividuRepository.getEffectifTotalDebutPeriode(idPeriode));

                List<Integer> datesEffetDistinctes = effectifCategorieMouvementRepository.getDatesEffetDistinctesPourPeriode(idPeriode);
                List<Map<String, Object>> mouvements = new ArrayList<>(datesEffetDistinctes.size());
                for (Integer dateEffet : datesEffetDistinctes) {
                    Map<String, Object> mouvement = new HashMap<>();
                    mouvement.put("libelle", "Mouvts calculés au " + DateRedac.convertionJourMoisDateRedacSansValidation(dateEffet));
                    mouvement.put("nombres", effectifCategorieMouvementRepository
                            .getNombresMouvementsPourDateEffetEtCategoriesQuittancement(idPeriode, dateEffet, numerosCategorieQuittancement));
                    mouvement.put("nombreTotal", effectifCategorieMouvementRepository.getNombreTotalMouvementsPourDateEffet(idPeriode, dateEffet));
                    mouvements.add(mouvement);
                }
                consolidation.put("mouvements", mouvements);

                consolidation.put("effectifsFin", categorieQuittancementIndividuRepository
                        .getEffectifsFinPourPeriodeEtCategoriesQuittancement(idPeriode, numerosCategorieQuittancement));
                consolidation.put("effectifTotalFin", categorieQuittancementIndividuRepository.getEffectifTotalFinPeriode(idPeriode));

            } else if (natureContrat != null && natureContrat == 2) {
                // Sur salaire : montants par tranche
                List<Tranche> tranchesDistinctes = trancheCategorieRepository.getTranchesDistinctesPourPeriodeTrieesParNumero(idPeriode);
                List<Map<String, Object>> tranches = new ArrayList<>(tranchesDistinctes.size());
                for (Tranche infoTrancheDistincte : tranchesDistinctes) {
                    Integer numTranche = infoTrancheDistincte.getNumTranche();
                    String libelleTranche = infoTrancheDistincte.getLibelleTranche();
                    Map<String, Object> tranche = new HashMap<>();
                    tranche.put("libelle", numTranche + ". " + infoTrancheDistincte.getLibelleTranche());
                    tranche.put("montants", trancheCategorieRepository.getSommesMontantsTranchePourCategoriesQuittancement(idPeriode, numTranche,
                            libelleTranche, numerosCategorieQuittancement));
                    tranche.put("montantTotal", trancheCategorieRepository.getSommesMontantsTranche(idPeriode, numTranche, libelleTranche));
                    tranches.add(tranche);
                }
                consolidation.put("tranches", tranches);
            }
        }
    }

    private void ajouteInfoSyntheseVersements(Map<String, Object> infoSynthese, Long idPeriode) {
        MessageControle messageModePaiement = messageControleRepository.getPourPeriodeEtIdentifiantControle(idPeriode,
                ParamControleSignalRejet.ID_MODE_PAIEMENT);
        infoSynthese.put("niveauModePaiement", messageModePaiement != null ? messageModePaiement.getNiveauAlerte() : null);
        List<Versement> versements = rattachementDeclarationsRecuesRepository.getVersementsPourPeriode(idPeriode);
        infoSynthese.put("versements", versements);

        Map<String, Map<String, Object>> infoVersements = new HashMap<>(versements.size());
        for (Versement versement : versements) {
            String idVersement = versement.getIdVersement();
            Map<String, Object> infoVersement = new HashMap<>();
            List<String> typesPopulationDisctincts = composantVersementRepository.getTypesPopulationDistincts(idVersement);
            infoVersement.put("typesPopulationDisctincts", typesPopulationDisctincts);
            List<String> periodesAffectationDistinctes = composantVersementRepository.getPeriodesAffectationDistinctes(idVersement);
            Map<String, Object> periodesAffectation = new LinkedHashMap<>(periodesAffectationDistinctes.size());
            for (String periodeAffectation : periodesAffectationDistinctes) {
                Map<String, Object> infoPeriodeAffectation = new HashMap<>(3);
                MessageControle messagePeriodeInvalide = messageControleRepository.getMessagesPeriodesAffectationInvalidesPourPeriode(idPeriode,
                        periodeAffectation);
                infoPeriodeAffectation.put("niveauAlertePeriodeInvalide",
                        messagePeriodeInvalide != null ? messagePeriodeInvalide.getNiveauAlerte() : null);
                infoPeriodeAffectation.put("idsSousFonds",
                        composantVersementRepository.getIdentifiantsSousFondsPourPeriodeAffectation(idVersement, periodeAffectation));
                infoPeriodeAffectation.put("montantsVerses",
                        composantVersementRepository.getSommesMontantsVersesPourPeriodeAffectation(idVersement, periodeAffectation));
                periodesAffectation.put(periodeAffectation, infoPeriodeAffectation);
            }
            infoVersement.put("periodesAffectation", periodesAffectation);
            infoVersement.put("dateDebutPeriodeRattachement",
                    DateRedac.convertionDateRedacSansValidation(composantVersementRepository.getDateDebutPeriodeRattachement(idVersement)));
            infoVersement.put("dateFinPeriodeRattachement",
                    DateRedac.convertionDateRedacSansValidation(composantVersementRepository.getDateFinPeriodeRattachement(idVersement)));
            infoVersements.put(idVersement, infoVersement);
        }
        infoSynthese.put("infoVersements", infoVersements);
    }

    /**
     * Indique la présence d'un identifiantUser dans la liste de gestionnaire
     * 
     * @param gestionnaires
     *            liste de gestionnaires
     * @param identifiantUser
     *            identifiantUser recherché
     * @return true si présence de l'identifiantUser dans la liste
     */
    private boolean existeCodeUser(List<ResumeParamUtilisateurGestionnaire> gestionnaires, String identifiantUser) {
        if (identifiantUser != null) {
            for (ResumeParamUtilisateurGestionnaire gestionnaire : gestionnaires) {
                if (identifiantUser.equals(gestionnaire.getIdentifiantActiveDirectory())) {
                    return true;
                }
            }
        }
        return false;
    }
}
