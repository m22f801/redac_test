package fr.si2m.red.ihm.securite;

import java.util.List;

import lombok.Setter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Component;

import fr.si2m.red.ihm.session.UtilisateurIHM;
import fr.si2m.red.parametrage.ParamUtilisateurContratVIPRepository;
import fr.si2m.red.parametrage.ParamUtilisateurGestionnaire;
import fr.si2m.red.parametrage.ParamUtilisateurGestionnaireRepository;

/**
 * Fournisseur de la logique d'authentification de l'application.<br/>
 * <br/>
 * Ce fournisseur introduit le concept d'"utilisateur REDAC" à partir d'un identifiant et de rôles Active Directory reçus.
 * 
 * @author nortaina
 * 
 */
@Component
public class RedacAuthenticationProvider implements AuthenticationProvider {

    @Setter
    @Autowired
    private ParamUtilisateurGestionnaireRepository paramUtilisateurGestionnaireRepository;

    @Setter
    @Autowired
    private ParamUtilisateurContratVIPRepository paramUtilisateurContratVIPRepository;

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        String identifiantActiveDirectory = authentication.getName();
        ParamUtilisateurGestionnaire gestionnaire = paramUtilisateurGestionnaireRepository.getGestionnaire(identifiantActiveDirectory);
        if (gestionnaire == null) {
            throw new BadCredentialsException("L'utilisateur " + identifiantActiveDirectory + " n'est pas connu de l'application REDAC");
        }
        List<String> contratsVIP = paramUtilisateurContratVIPRepository.getContratsVIPPourUtilisateur(identifiantActiveDirectory);
        UtilisateurIHM utilisateur = new UtilisateurIHM(gestionnaire, contratsVIP);
        return new UsernamePasswordAuthenticationToken(utilisateur, null, authentication.getAuthorities());
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return true;
    }

}
