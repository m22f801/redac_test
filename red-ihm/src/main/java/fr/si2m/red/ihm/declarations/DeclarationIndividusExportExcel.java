package fr.si2m.red.ihm.declarations;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import fr.si2m.red.DateRedac;
import fr.si2m.red.contrat.TarifRepository;
import fr.si2m.red.dsn.AdhesionEtablissementMois;
import fr.si2m.red.dsn.MessageControleExportExcel;
import fr.si2m.red.dsn.ResumeAffiliationExportExcel;
import fr.si2m.red.dsn.ResumeChangementIndividu;
import fr.si2m.red.reconciliation.CategorieQuittancementIndividuRepository;
import fr.si2m.red.reconciliation.ExportIndividuExcel;
import fr.si2m.red.reconciliation.MessageControleRepository;
import fr.si2m.red.reconciliation.PeriodeRecue;
import fr.si2m.red.reconciliation.RattachementDeclarationsRecuesRepository;
import fr.si2m.red.reconciliation.TrancheCategorieBaseAssujettieRepository;

/**
 * Export Excel des individus d'une déclaration.
 * 
 * @author nortaina
 *
 */
@Component
public class DeclarationIndividusExportExcel extends DeclarationExportExcel {

    @Autowired
    private RattachementDeclarationsRecuesRepository rattachementDeclarationsRecuesRepository;

    @Autowired
    private MessageControleRepository messageControleRepository;

    @Autowired
    private CategorieQuittancementIndividuRepository categorieQuittancementIndividuRepository;

    @Autowired
    private TrancheCategorieBaseAssujettieRepository trancheCategorieBaseAssujettieRepository;

    @Autowired
    private TarifRepository tarifRepository;

    private static final int SYNTHESE_POSITION_LIBELLE_2 = 3;
    private static final int SYNTHESE_POSITION_VALEUR_2 = 4;

    @Override
    protected void creeContenuClasseur(Workbook workbook) {
        Assert.notNull(PERIODE.get(), "La période pour l'export n'a pas été définie - appeler la méthode #exporte(PeriodeRecue, OutputStream)");

        // Onglets
        creeOngletSynthese(workbook);
        creeOngletExtraction(workbook);
    }

    /**
     * Crée l'onglet de synthèse de la période.
     * 
     * @param workbook
     *            le classeur
     * @param styleCelluleParDefaut
     *            le style par défaut, réutilisable
     */
    private void creeOngletSynthese(Workbook workbook) {
        Sheet synthese = workbook.createSheet("Synthèse");

        int rowNum = creeEnteteOngletSynthese(workbook, synthese);
        rowNum = creeDetailsPeriodeOngletSynthese(workbook, synthese, rowNum);
        rowNum = creeDetailsPeriodeOngletSyntheseIndividus(workbook, synthese, rowNum);
        rowNum = creeEnteteListeIndividusOngletSynthese(workbook, synthese, rowNum);
        creeListeIndividusOngletSynthese(workbook, synthese, rowNum);

        // nombre de caractères à afficher par colonnes
        int[] widthArray = { 20, 20, 20, 20, 20, 20, 20, 20 };
        redimensionnementColonnes(synthese, widthArray);

    }

    private int creeDetailsPeriodeOngletSyntheseIndividus(Workbook workbook, Sheet synthese, int premiereLigne) {
        PeriodeRecue periode = PERIODE.get();

        // Styles
        CellStyle styleLibelle = creeStyleCelluleParDefaut(workbook, false);
        styleLibelle.setAlignment(CellStyle.ALIGN_RIGHT);

        CellStyle styleValeur = creeStyleCelluleParDefaut(workbook, true);

        // Modification de libelle: SIREN déclaré
        Cell cellSiren = null;
        int rowNum = premiereLigne;

        // Retrieve the row and check for null
        Row syntheseLigne = synthese.getRow(8);
        if (syntheseLigne != null) {
            cellSiren = syntheseLigne.getCell(3);
        }

        if (cellSiren != null) {
            cellSiren.setCellValue("SIREN déclaré :");
        }

        // Modification de libelle: NIC déclaré
        Cell cellNIC = null;

        syntheseLigne = synthese.getRow(9);
        if (syntheseLigne != null) {
            cellNIC = syntheseLigne.getCell(3);
        }

        if (cellNIC != null) {
            cellNIC.setCellValue("NIC déclaré :");
        }

        // Ajout du champ SIREN émetteur
        int createNewRowAt = 10;

        Row ligneSyntheseSirenEmetteur = synthese.createRow(createNewRowAt);
        ligneSyntheseSirenEmetteur = synthese.getRow(createNewRowAt);

        List<AdhesionEtablissementMois> adhesionsEtablissements = rattachementDeclarationsRecuesRepository
                .getAdhesionsEtablissementsMoisRattaches(periode.getIdPeriode());

        // Considerer la premiere adhesion
        creeCelluleFormulaireLibelle(ligneSyntheseSirenEmetteur, SYNTHESE_POSITION_LIBELLE_2, styleLibelle, "SIREN émetteur :");

        String sirenEmetteur = StringUtils.EMPTY;
        if (!adhesionsEtablissements.isEmpty()) {
            sirenEmetteur = adhesionsEtablissements.get(0).getSirenEmetteur();
        }

        creeCelluleFormulaireValeur(ligneSyntheseSirenEmetteur, SYNTHESE_POSITION_VALEUR_2, styleValeur, sirenEmetteur);

        rowNum++;
        return rowNum;

    }

    private int creeEnteteListeIndividusOngletSynthese(Workbook workbook, Sheet synthese, int premiereLigne) {
        int rowNum = premiereLigne;

        // Style

        Font font1 = workbook.createFont();
        font1.setFontHeightInPoints(FONT_TAILLE_DEFAUT);
        font1.setFontName(FONT_NOM);

        Font font2 = workbook.createFont();
        font2.setFontHeightInPoints((short) 8);
        font2.setFontName(FONT_NOM);

        CellStyle styleCellule = workbook.createCellStyle();
        styleCellule.setAlignment(CellStyle.ALIGN_CENTER);
        styleCellule.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
        enrichitStyleCelluleAvecToutesBordures(styleCellule);
        styleCellule.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
        styleCellule.setFillPattern(CellStyle.SOLID_FOREGROUND);
        styleCellule.setWrapText(true);
        styleCellule.setFont(font1);

        Row ligneEntete = synthese.createRow(rowNum++);
        int cellNum = 0;

        creeCellule(ligneEntete, cellNum++, styleCellule, "NIR/NTT");
        creeCellule(ligneEntete, cellNum++, styleCellule, "num.adhér.\n30.019");
        creeCellule(ligneEntete, cellNum++, styleCellule, "nom famille\n30.002");
        creeCellule(ligneEntete, cellNum++, styleCellule, "nom usage\n30.003");
        creeCellule(ligneEntete, cellNum++, styleCellule, "prénom\n30.004");
        creeCellule(ligneEntete, cellNum++, styleCellule, "Mt Cotis.");
        creeCellule(ligneEntete, cellNum++, styleCellule, "Signalements");
        creeCellule(ligneEntete, cellNum++, styleCellule, "Rejets");

        return rowNum;
    }

    private void creeListeIndividusOngletSynthese(Workbook workbook, Sheet synthese, int premiereLigne) {
        int rowNum = premiereLigne;

        // Style
        CellStyle styleCellule = creeStyleCelluleParDefaut(workbook, false);
        styleCellule.setAlignment(CellStyle.ALIGN_CENTER);
        styleCellule.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
        enrichitStyleCelluleAvecToutesBordures(styleCellule);
        styleCellule.setWrapText(true);

        Long idPeriode = PERIODE.get().getIdPeriode();

        int pageTaille = getPageTaille();
        int page = 0;

        List<ExportIndividuExcel> individus = rattachementDeclarationsRecuesRepository.getDistinctIndividusTriesRattachesPagines(idPeriode,
                page * pageTaille, pageTaille);

        boolean inferieurPageTaille = false;

        while (!individus.isEmpty() && !inferieurPageTaille) {
            for (ExportIndividuExcel individu : individus) {
                Row ligneIndividu = synthese.createRow(rowNum++);
                int cellNum = 0;
                String identifiant = StringUtils.isNotBlank(individu.getIdentifiantRepertoire()) ? individu.getIdentifiantRepertoire()
                        : individu.getNumeroTechniqueTemporaire();
                creeCellule(ligneIndividu, cellNum++, styleCellule, identifiant);
                creeCellule(ligneIndividu, cellNum++, styleCellule, individu.getMatricule());
                creeCellule(ligneIndividu, cellNum++, styleCellule, individu.getNomFamille());
                creeCellule(ligneIndividu, cellNum++, styleCellule, individu.getNomUsage());
                creeCellule(ligneIndividu, cellNum++, styleCellule, individu.getPrenom());

                // On veut pouvoir afficher la valeur 0 si valeur présente dans CATEGORIE_QUITTANCEMENT_INDIVIDU
                // ou 'vide' si valeur null
                if (Double.compare(individu.getSommeMontantsCotisation(), 0d) == 0 && individu.isSommeMontantsCotisationExistante()) {
                    creeCellule(ligneIndividu, cellNum++, styleCellule, individu.getSommeMontantsCotisation(), true);
                } else {
                    creeCellule(ligneIndividu, cellNum++, styleCellule, individu.getSommeMontantsCotisation());
                }

                creeCellule(ligneIndividu, cellNum++, styleCellule, individu.getConcatMsgSignal());
                creeCellule(ligneIndividu, cellNum++, styleCellule, individu.getConcatMsgRejet());
            }

            // mise à jour du flag
            inferieurPageTaille = individus.size() < pageTaille;

            // la page était complète, on a potentielement une autre page à récupérer
            if (!inferieurPageTaille) {
                // On récupère la page suivante
                page++;
                individus = rattachementDeclarationsRecuesRepository.getDistinctIndividusTriesRattachesPagines(idPeriode, page * pageTaille,
                        pageTaille);
            }
        }

    }

    private void creeOngletExtraction(Workbook workbook) {
        Sheet extraction = workbook.createSheet("Bases consolidées par Individu");
        PeriodeRecue periode = PERIODE.get();
        Long idPeriode = periode.getIdPeriode();

        int rowNum = remplitLigneEntete(workbook, extraction);

        // Styles
        CellStyle styleCelluleParDefaut = creeStyleCelluleParDefaut(workbook, false);
        enrichitStyleCelluleAvecToutesBordures(styleCelluleParDefaut);
        styleCelluleParDefaut.setWrapText(true);

        // Récupération CodePopulation + vérification si Unique
        boolean codePopulationEstUnique = false;

        List<String> codesPopulationTarif = tarifRepository.getDistinctNumCategoriePourContratEtDate(periode.getNumeroContrat(),
                periode.getDateDebutPeriode());
        codePopulationEstUnique = codesPopulationTarif != null && codesPopulationTarif.size() == 1;

        String codePopulationUnique = StringUtils.EMPTY;
        if (codePopulationEstUnique) {
            codePopulationUnique = codesPopulationTarif.get(0);
        }

        int pageTaille = getPageTaille();
        int page = 0;

        List<ResumeAffiliationExportExcel> affiliations = rattachementDeclarationsRecuesRepository
                .getResumesAffiliationsDistinctesRattachesPaginesExportExcel(idPeriode, periode.getDateDebutPeriode(), periode.getDateFinPeriode(),
                        page * pageTaille, pageTaille);

        boolean inferieurPageTaille = false;

        while (!affiliations.isEmpty() && !inferieurPageTaille) {
            for (ResumeAffiliationExportExcel affiliation : affiliations) {
                Row ligneIndividu = extraction.createRow(rowNum++);

                if (StringUtils.isEmpty(affiliation.getCodePopulation())) {
                    affiliation.setCodePopulation(codePopulationUnique);
                }

                remplitLigneIndividu(ligneIndividu, affiliation, styleCelluleParDefaut);
            }

            // mise à jour du flag
            inferieurPageTaille = affiliations.size() < pageTaille;

            if (!inferieurPageTaille) {
                // On récupère la page suivante
                page++;
                affiliations = rattachementDeclarationsRecuesRepository.getResumesAffiliationsDistinctesRattachesPaginesExportExcel(idPeriode,
                        periode.getDateDebutPeriode(), periode.getDateFinPeriode(), page * pageTaille, pageTaille);
            }
        }

        // nombre de caractères à afficher par colonnes
        int[] widthArray = { 13, 40, 30, 6, 40, 40, 40, 10, // Individus
                10, 13, 40, 40, 10, // Changement individu
                80, 80, // Alerte sur individu
                11, 11, 12, 10, // Contrat travail
                15, 40, 14, 14, 11, 12, // Affiliation
                10, 10, 10, 10, // Libellé et Montant de la tranche correspondante
                10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10 }; // Montant du type de composant de base assujettie
        redimensionnementColonnes(extraction, widthArray);

        extraction.setAutoFilter(new CellRangeAddress(3, 3, 0, 37));
    }

    /**
     * Remplit les cases d'une ligne entête.
     * 
     * @param workbook
     *            le classeur Excel
     * @param extraction
     *            la feuille de l'extraction
     * 
     * @return le numéro de ligne suivant l'entête
     */
    private int remplitLigneEntete(Workbook workbook, Sheet extraction) {
        PeriodeRecue periode = PERIODE.get();

        int rowNum = 0;
        int idxColEntete = 0;

        // Style
        CellStyle styleCellule = creeStyleCelluleEnteteListe(workbook, false);

        // Titre
        Cell celluleTitre = extraction.createRow(rowNum++).createCell(0);
        CellStyle styleTitre = creeStyleCelluleTitreListe(workbook);
        celluleTitre.setCellStyle(styleTitre);
        celluleTitre.setCellValue("Bases consolidées par Individu pour cette période");
        extraction.addMergedRegion(new CellRangeAddress(0, 0, 0, 10));

        // Saut de ligne
        rowNum++;

        // Première ligne entête
        Row premiereLigneEntete = extraction.createRow(rowNum++);
        creeCelluleEntete(extraction, premiereLigneEntete, 0, 7, styleCellule, "Individu");
        creeCelluleEntete(extraction, premiereLigneEntete, 8, 12, styleCellule, "Changement individu");
        creeCelluleEntete(extraction, premiereLigneEntete, 13, 14, styleCellule, "Alerte sur individu");
        creeCelluleEntete(extraction, premiereLigneEntete, 15, 18, styleCellule, "Contrat travail");
        creeCelluleEntete(extraction, premiereLigneEntete, 19, 24, styleCellule, "Affiliation");
        creeCelluleEntete(extraction, premiereLigneEntete, 25, 28, styleCellule, "Libellé et Montant de la tranche correspondante");
        creeCelluleEntete(extraction, premiereLigneEntete, 29, 41, styleCellule, "Montant du type de composant de base assujettie");

        // Seconde ligne entête
        Row secondeLigneEntete = extraction.createRow(rowNum++);
        creeCellule(secondeLigneEntete, idxColEntete++, styleCellule, "NIR\n30.001");
        creeCellule(secondeLigneEntete, idxColEntete++, styleCellule, "NTT\n30.020");
        creeCellule(secondeLigneEntete, idxColEntete++, styleCellule, "num.adhér.\n30.019");
        creeCellule(secondeLigneEntete, idxColEntete++, styleCellule, "sexe\n30.005");
        creeCellule(secondeLigneEntete, idxColEntete++, styleCellule, "nom famille\n30.002");
        creeCellule(secondeLigneEntete, idxColEntete++, styleCellule, "nom usage\n30.003");
        creeCellule(secondeLigneEntete, idxColEntete++, styleCellule, "prénom\n30.004");
        creeCellule(secondeLigneEntete, idxColEntete++, styleCellule, "date nais.\n30.006");
        creeCellule(secondeLigneEntete, idxColEntete++, styleCellule, "dates chgt\n31.001");
        creeCellule(secondeLigneEntete, idxColEntete++, styleCellule, "Anciens NIR\n31.008");
        creeCellule(secondeLigneEntete, idxColEntete++, styleCellule, "Anciens Nom\nfam.\n31.009");
        creeCellule(secondeLigneEntete, idxColEntete++, styleCellule, "Anciens\nPrénoms\n31.010");
        creeCellule(secondeLigneEntete, idxColEntete++, styleCellule, "Anciennes\nDate nais.\n31.011");
        creeCellule(secondeLigneEntete, idxColEntete++, styleCellule, "Signalements");
        creeCellule(secondeLigneEntete, idxColEntete++, styleCellule, "Rejets");
        creeCellule(secondeLigneEntete, idxColEntete++, styleCellule, "Date entrée\n40.001");
        creeCellule(secondeLigneEntete, idxColEntete++, styleCellule, "Date sortie\n40.010");
        creeCellule(secondeLigneEntete, idxColEntete++, styleCellule, "Motif sortie\n62.002");
        creeCellule(secondeLigneEntete, idxColEntete++, styleCellule, "Changement\ndans période");
        creeCellule(secondeLigneEntete, idxColEntete++, styleCellule, "Code Population");
        creeCellule(secondeLigneEntete, idxColEntete++, styleCellule, "Libellé Population\nSI source");
        creeCellule(secondeLigneEntete, idxColEntete++, styleCellule, "Début affil.\n70.014");
        creeCellule(secondeLigneEntete, idxColEntete++, styleCellule, "Fin Affil.\n70.015.");
        creeCellule(secondeLigneEntete, idxColEntete++, styleCellule, "Mt Cotis.");
        creeCellule(secondeLigneEntete, idxColEntete++, styleCellule, "Estimation\ncotisation");

        for (int numTranche = 1; numTranche <= 4; numTranche++) {
            String libelleTranche = trancheCategorieBaseAssujettieRepository.getLibelleTranchePourPeriodeEtNumTranche(periode.getIdPeriode(),
                    numTranche);
            libelleTranche = libelleTranche != null ? libelleTranche : "T" + numTranche;
            creeCellule(secondeLigneEntete, idxColEntete++, styleCellule, libelleTranche);
        }

        creeCellule(secondeLigneEntete, idxColEntete++, styleCellule, "Sal.brut\n10");
        creeCellule(secondeLigneEntete, idxColEntete++, styleCellule, "TA\n11");
        creeCellule(secondeLigneEntete, idxColEntete++, styleCellule, "T2\n12");
        creeCellule(secondeLigneEntete, idxColEntete++, styleCellule, "TB\n13");
        creeCellule(secondeLigneEntete, idxColEntete++, styleCellule, "TC\n14");
        creeCellule(secondeLigneEntete, idxColEntete++, styleCellule, "TD\n15");
        creeCellule(secondeLigneEntete, idxColEntete++, styleCellule, "TD1\n16");
        creeCellule(secondeLigneEntete, idxColEntete++, styleCellule, "Spécif.\n17");
        creeCellule(secondeLigneEntete, idxColEntete++, styleCellule, "Forfait.\n18");
        creeCellule(secondeLigneEntete, idxColEntete++, styleCellule, "Fictive\n19");
        creeCellule(secondeLigneEntete, idxColEntete++, styleCellule, "Mt forfait.\n20");
        creeCellule(secondeLigneEntete, idxColEntete++, styleCellule, "Mt libre\n21");
        creeCellule(secondeLigneEntete, idxColEntete++, styleCellule, "T2U\n24");

        return rowNum;
    }

    /**
     * Remplit les cases d'une ligne individu.
     * 
     * @param ligneIndividu
     *            la ligne individu
     * @param affiliation
     *            l'affiliation de l'individu à exporter
     */
    private void remplitLigneIndividu(Row ligneIndividu, ResumeAffiliationExportExcel affiliation, CellStyle styleCellule) {
        PeriodeRecue periode = PERIODE.get();
        Long idPeriode = PERIODE.get().getIdPeriode();
        int idxcolonne = 0;
        creeCellule(ligneIndividu, idxcolonne++, styleCellule, affiliation.getIdentifiantRepertoire());
        creeCellule(ligneIndividu, idxcolonne++, styleCellule, affiliation.getNumeroTechniqueTemporaire());
        creeCellule(ligneIndividu, idxcolonne++, styleCellule, affiliation.getMatricule());
        creeCellule(ligneIndividu, idxcolonne++, styleCellule, affiliation.getSexe());
        creeCellule(ligneIndividu, idxcolonne++, styleCellule, affiliation.getNomFamille());
        creeCellule(ligneIndividu, idxcolonne++, styleCellule, affiliation.getNomUsage());
        creeCellule(ligneIndividu, idxcolonne++, styleCellule, affiliation.getPrenom());
        creeCellule(ligneIndividu, idxcolonne++, styleCellule, DateRedac.convertionDateRedacSansValidation(affiliation.getDateNaissance()));

        idxcolonne = creeCellulesChangementIndividu(affiliation, ligneIndividu, styleCellule, idxcolonne);

        idxcolonne = creeCellulesMessagesIndividu(periode, affiliation, ligneIndividu, styleCellule, idxcolonne);

        idxcolonne = creeCellulesContratIndividu(affiliation, ligneIndividu, styleCellule, idxcolonne);

        String codePopulation = affiliation.getCodePopulation();
        creeCellule(ligneIndividu, idxcolonne++, styleCellule, codePopulation);

        String libPopulation = affiliation.getLibPopulation() != null ? affiliation.getLibPopulation() : StringUtils.EMPTY;

        creeCellule(ligneIndividu, idxcolonne++, styleCellule, libPopulation);

        String nirNtt = !affiliation.getIdentifiantRepertoire().isEmpty() ? affiliation.getIdentifiantRepertoire()
                : affiliation.getNumeroTechniqueTemporaire();

        creeCellule(ligneIndividu, idxcolonne++, styleCellule, affiliation.getDateDebutAffiliationSansValidation());
        creeCellule(ligneIndividu, idxcolonne++, styleCellule, affiliation.getDateFinAffiliationSansValidation());

        // On veut pouvoir afficher la valeur 0 si valeur présente dans CATEGORIE_QUITTANCEMENT_INDIVIDU
        // ou 'vide' si valeur null

        // F09_RG_5_06
        Double montantEstime;

        if (StringUtils.isEmpty(codePopulation)) {
            montantEstime = categorieQuittancementIndividuRepository.getSommeMontantsCotisationEstime(idPeriode, nirNtt);
        } else {
            montantEstime = categorieQuittancementIndividuRepository.getSommeMontantsCotisationEstimePourPopulation(idPeriode, nirNtt,
                    codePopulation);
        }

        if (affiliation.getSommeMontantsCotisation() != null && Double.compare(affiliation.getSommeMontantsCotisation().doubleValue(), 0d) == 0) {
            creeCellule(ligneIndividu, idxcolonne++, styleCellule, affiliation.getSommeMontantsCotisation(), true);
        } else {
            creeCellule(ligneIndividu, idxcolonne++, styleCellule, affiliation.getSommeMontantsCotisation());
        }

        if (montantEstime != null && Double.compare(montantEstime.doubleValue(), 0d) == 0) {
            creeCellule(ligneIndividu, idxcolonne++, styleCellule, montantEstime, true);
        } else {
            creeCellule(ligneIndividu, idxcolonne++, styleCellule, montantEstime);
        }

        creeCellule(ligneIndividu, idxcolonne++, styleCellule, affiliation.getMontantTranche1());
        creeCellule(ligneIndividu, idxcolonne++, styleCellule, affiliation.getMontantTranche2());
        creeCellule(ligneIndividu, idxcolonne++, styleCellule, affiliation.getMontantTranche3());
        creeCellule(ligneIndividu, idxcolonne++, styleCellule, affiliation.getMontantTranche4());

        // Montant du type de composant de base assujettie
        creeCellule(ligneIndividu, idxcolonne++, styleCellule, affiliation.getMontantCBA10());
        creeCellule(ligneIndividu, idxcolonne++, styleCellule, affiliation.getMontantCBA11());
        creeCellule(ligneIndividu, idxcolonne++, styleCellule, affiliation.getMontantCBA12());
        creeCellule(ligneIndividu, idxcolonne++, styleCellule, affiliation.getMontantCBA13());
        creeCellule(ligneIndividu, idxcolonne++, styleCellule, affiliation.getMontantCBA14());
        creeCellule(ligneIndividu, idxcolonne++, styleCellule, affiliation.getMontantCBA15());
        creeCellule(ligneIndividu, idxcolonne++, styleCellule, affiliation.getMontantCBA16());
        creeCellule(ligneIndividu, idxcolonne++, styleCellule, affiliation.getMontantCBA17());
        creeCellule(ligneIndividu, idxcolonne++, styleCellule, affiliation.getMontantCBA18());
        creeCellule(ligneIndividu, idxcolonne++, styleCellule, affiliation.getMontantCBA19());
        creeCellule(ligneIndividu, idxcolonne++, styleCellule, affiliation.getMontantCBA20());
        creeCellule(ligneIndividu, idxcolonne++, styleCellule, affiliation.getMontantCBA21());
        creeCellule(ligneIndividu, idxcolonne++, styleCellule, affiliation.getMontantCBA24());
    }

    /**
     * Crée les cellules relatives aux changements éventuels pour un individu.
     * 
     * @param affiliation
     *            l'affiliation
     * @param ligneIndividu
     *            la ligne sur laquelle écrire
     * @param styleCellule
     *            le style de cellule à adopter
     * @param idxcolonne
     *            l'index de la colonne en cours
     * @return localIndex la nouvelle valeur de l'index colonne
     */
    private int creeCellulesChangementIndividu(ResumeAffiliationExportExcel affiliation, Row ligneIndividu, CellStyle styleCellule, int idxcolonne) {
        List<ResumeChangementIndividu> changementsIndividu = rattachementDeclarationsRecuesRepository.getChangementsIndividuRattache(
                PERIODE.get().getIdPeriode(), affiliation.getIdentifiantRepertoire(), affiliation.getNumeroTechniqueTemporaire());
        // sauvegarde index
        int localIndex = idxcolonne;
        List<String> datesModifications = new ArrayList<>(changementsIndividu.size());
        List<String> anciensIdentifiants = new ArrayList<>(changementsIndividu.size());
        List<String> anciensNomsFamilles = new ArrayList<>(changementsIndividu.size());
        List<String> anciensPrenoms = new ArrayList<>(changementsIndividu.size());
        List<String> anciennesDatesNais = new ArrayList<>(changementsIndividu.size());
        for (ResumeChangementIndividu changementIndividu : changementsIndividu) {
            if (changementIndividu.getDateModification() != null) {
                datesModifications.add(DateRedac.convertionDateRedacSansValidation(changementIndividu.getDateModification()));
            }
            if (StringUtils.isNotBlank(changementIndividu.getAncienIdentifiant())) {
                anciensIdentifiants.add(changementIndividu.getAncienIdentifiant());
            }
            if (StringUtils.isNotBlank(changementIndividu.getAncienNomFamille())) {
                anciensNomsFamilles.add(changementIndividu.getAncienNomFamille());
            }
            if (StringUtils.isNotBlank(changementIndividu.getAncienPrenom())) {
                anciensPrenoms.add(changementIndividu.getAncienPrenom());
            }
            if (changementIndividu.getAncienneDateNaissance() != null && changementIndividu.getAncienneDateNaissance() != 0) {
                anciennesDatesNais.add(DateRedac.convertionDateRedacSansValidation(changementIndividu.getAncienneDateNaissance()));
            }
        }
        creeCellule(ligneIndividu, localIndex++, styleCellule, datesModifications);
        creeCellule(ligneIndividu, localIndex++, styleCellule, anciensIdentifiants);
        creeCellule(ligneIndividu, localIndex++, styleCellule, anciensNomsFamilles);
        creeCellule(ligneIndividu, localIndex++, styleCellule, anciensPrenoms);
        creeCellule(ligneIndividu, localIndex++, styleCellule, anciennesDatesNais);

        return localIndex;
    }

    /**
     * Crée les cellules relatives aux messages pour un individu.
     * 
     * @param periode
     *            la période pour les messages
     * @param affiliation
     *            l'affiliation
     * @param ligneIndividu
     *            la ligne sur laquelle écrire
     * @param styleCellule
     *            le style de cellule à adopter
     * @param idxcolonne
     *            l'index de la colonne en cours
     * @return localIndex la nouvelle valeur de l'index colonne
     */
    private int creeCellulesMessagesIndividu(PeriodeRecue periode, ResumeAffiliationExportExcel affiliation, Row ligneIndividu,
            CellStyle styleCellule, int idxcolonne) {

        // sauvegarde index
        int localIndex = idxcolonne;

        MessageControleExportExcel messagesIndividus = messageControleRepository.getMessagesSignalRejetPourIndividu(periode.getIdPeriode(),
                StringUtils.isBlank(affiliation.getIdentifiantRepertoire()) ? affiliation.getNumeroTechniqueTemporaire()
                        : affiliation.getIdentifiantRepertoire());

        if (messagesIndividus == null) {
            creeCellule(ligneIndividu, localIndex++, styleCellule, StringUtils.EMPTY);
            creeCellule(ligneIndividu, localIndex++, styleCellule, StringUtils.EMPTY);
        } else {
            creeCellule(ligneIndividu, localIndex++, styleCellule,
                    messagesIndividus.getMessageUtilisateurSignal() != null ? messagesIndividus.getMessageUtilisateurSignal() : StringUtils.EMPTY);
            creeCellule(ligneIndividu, localIndex++, styleCellule,
                    messagesIndividus.getMessageUtilisateurRejet() != null ? messagesIndividus.getMessageUtilisateurRejet() : StringUtils.EMPTY);
        }

        return localIndex;

    }

    /**
     * Crée les cellules relatives au contrat de travail pour un individu.
     * 
     * @param affiliation
     *            l'affiliation
     * @param ligneIndividu
     *            la ligne sur laquelle écrire
     * @param styleCellule
     *            le style de cellule à adopter
     * @param idxcolonne
     *            l'index de la colonne en cours
     * @return localIndex la nouvelle valeur de l'index colonne
     */
    private int creeCellulesContratIndividu(ResumeAffiliationExportExcel affiliation, Row ligneIndividu, CellStyle styleCellule, int idxcolonne) {
        // sauvegarde index
        int localIndex = idxcolonne;

        Integer dateDebutContrat = affiliation.getDateDebutContrat();
        if (dateDebutContrat != null) {
            creeCellule(ligneIndividu, localIndex++, styleCellule, DateRedac.convertionDateRedacSansValidation(dateDebutContrat));
        } else {
            creeCellule(ligneIndividu, localIndex++, styleCellule, StringUtils.EMPTY);
        }
        Integer dateFinContrat = affiliation.getDateFinPrevisionnelle();
        if (dateFinContrat != null) {
            creeCellule(ligneIndividu, localIndex++, styleCellule, DateRedac.convertionDateRedacSansValidation(dateFinContrat));
        } else {
            creeCellule(ligneIndividu, localIndex++, styleCellule, StringUtils.EMPTY);
        }

        // Motif sortie 62.002
        creeCellule(ligneIndividu, localIndex++, styleCellule,
                affiliation.getMotifRupture() != null ? affiliation.getMotifRupture() : StringUtils.EMPTY);

        // Changement dans période
        creeCellule(ligneIndividu, localIndex++, styleCellule, affiliation.getChgtPeriode());

        return localIndex;
    }

    @Override
    protected void creeContenuClasseurMontant(Workbook workbook) {
        // TODO Auto-generated method stub

    }

    @Override
    protected void creeContenuClasseurMessage(Workbook workbook) {
        // TODO Auto-generated method stub

    }

}
