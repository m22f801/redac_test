package fr.si2m.red.ihm.securite;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;

/**
 * Point d'entrée des authentifications pour accès à l'application.
 * 
 * Voir la <a href= "http://docs.spring.io/spring-security/site/docs/3.0.x/reference/core-web-filters.html" >documentation Spring Security</a>.
 * 
 * @author nortaina
 *
 */
public class RedacAuthenticationEntryPoint implements AuthenticationEntryPoint {

    @Override
    public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException authException) throws IOException,
            ServletException {
        response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Unauthorized");
    }

}
