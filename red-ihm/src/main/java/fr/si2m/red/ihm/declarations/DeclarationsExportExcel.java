package fr.si2m.red.ihm.declarations;

import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import fr.si2m.red.DateRedac;
import fr.si2m.red.ihm.ExportExcel;
import fr.si2m.red.parametrage.ParamCodeLibelle;
import fr.si2m.red.parametrage.ParamCodeLibelleRepository;
import fr.si2m.red.parametrage.ParamControleSignalRejet;
import fr.si2m.red.parametrage.ParamControleSignalRejetRepository;
import fr.si2m.red.parametrage.ParamUtilisateurGestionnaireRepository;
import fr.si2m.red.parametrage.ParamValeurDefautRepository;
import fr.si2m.red.reconciliation.ExportPeriodeExcel;
import fr.si2m.red.reconciliation.HistoriqueAssignationPeriode;
import fr.si2m.red.reconciliation.HistoriqueAssignationPeriodeRepository;
import fr.si2m.red.reconciliation.PeriodeRecueRepository;
import fr.si2m.red.reconciliation.PeriodesRecuesCriteresRecherche;

/**
 * Export Excel de déclarations.
 * 
 * @author nortaina
 *
 */
@Component
public class DeclarationsExportExcel extends ExportExcel {
    private static final int CRITERE_POSITION_LIBELLE_1 = 0;
    private static final int CRITERE_POSITION_VALEUR_1 = 1;
    private static final int CRITERE_POSITION_LIBELLE_2 = 3;
    private static final int CRITERE_POSITION_VALEUR_2 = 4;
    private static final int CRITERE_POSITION_LIBELLE_3 = 6;
    private static final int CRITERE_POSITION_VALEUR_3 = 7;

    private static final String CRITERES_JOIN_CHAR = ", ";

    /**
     * Le conteneur de critères de recherche pour le thread courant.
     */
    private static final ThreadLocal<PeriodesRecuesCriteresRecherche> CRITERES_RECHERCHES = new ThreadLocal<>();

    @Autowired
    private PeriodeRecueRepository periodeRecueRepository;
    @Autowired
    private HistoriqueAssignationPeriodeRepository historiqueAssignationPeriodeRepository;
    @Autowired
    private ParamValeurDefautRepository paramValeurDefautRepository;
    @Autowired
    private ParamCodeLibelleRepository paramCodeLibelleRepository;
    @Autowired
    private ParamControleSignalRejetRepository paramControleSignalRejetRepository;
    @Autowired
    private ParamUtilisateurGestionnaireRepository paramUtilisateurGestionnaireRepository;

    /**
     * Map des libelles edition GroupeGestion
     */
    private Map<String, String> mapGroupeGestion = new HashMap<>();

    /**
     * Map des libelles edition NumFamilleProduit
     */
    private Map<String, String> mapNumFamilleProduit = new HashMap<>();

    /**
     * Map des libellés edition VIP
     */
    private Map<String, String> mapVip = new HashMap<>();

    /**
     * Map des libellés type Periode
     */
    private Map<String, String> mapType = new HashMap<>();

    /**
     * Map des libellés mode Paiement
     */
    private Map<String, String> mapModePaiement = new HashMap<>();

    /**
     * Map des libellés état d'une période
     */
    private Map<String, String> mapEtatPeriode = new HashMap<>();

    /**
     * Map des libellés édition Mode Gestion (indicateur DSN)
     */
    private Map<String, String> mapModeGestDSN = new HashMap<>();

    /**
     * Map des libellés édition indicateur Transfert
     */
    private Map<String, String> mapTransfert = new HashMap<>();

    /**
     * Libellé à afficher si aucun résultat
     */
    private static String LIBELLE_INTROUVABLE = "LIBELLE INTROUVABLE";

    /**
     * Exporte le contenu Excel dans le flux sortant donné.
     * 
     * @param criteresRecherche
     *            les critères de recherche pour les déclarations à exporter
     * @param fluxSortant
     *            le flux sortant
     */
    public void exporte(PeriodesRecuesCriteresRecherche criteresRecherche, OutputStream fluxSortant) {
        CRITERES_RECHERCHES.set(criteresRecherche);
        super.exporte(fluxSortant);
    }

    @Override
    protected void creeContenuClasseur(Workbook workbook) {
        Assert.notNull(CRITERES_RECHERCHES.get(),
                "Les critères de recherche pour l'export n'ont pas été définis - appeler la méthode #exporte(PeriodesRecuesCriteresRecherche, OutputStream)");

        // Styles transverses
        CellStyle styleCelluleParDefaut = creeStyleCelluleParDefaut(workbook, false);

        // Onglets
        creeOngletCriteres(workbook, styleCelluleParDefaut);
        creeOngletExtraction(workbook);
    }

    @Override
    protected void creeContenuClasseurMontant(Workbook workbook) {
    }

    @Override
    protected void creeContenuClasseurMessage(Workbook workbook) {
    }

    /**
     * Prépare l'onglet des critères.
     * 
     * @param workbook
     *            le classeur à exporter
     * @param styleCelluleParDefaut
     *            le style par défaut à utiliser pour les cellules
     */
    private void creeOngletCriteres(Workbook workbook, CellStyle styleCelluleParDefaut) {
        Sheet criteres = workbook.createSheet("Critères");

        // Styles
        CellStyle styleLibelle = creeStyleCelluleParDefaut(workbook, false);
        styleLibelle.setAlignment(CellStyle.ALIGN_RIGHT);

        CellStyle styleValeur = creeStyleCelluleParDefaut(workbook, true);

        int rowNum = 0;

        // Titre sur 7 colonnes
        Row ligneTitre = criteres.createRow(rowNum++);
        Cell celluleTitre = ligneTitre.createCell(0);
        CellStyle styleCelluleTitre = creeStyleCelluleTitre(workbook);
        celluleTitre.setCellStyle(styleCelluleTitre);
        celluleTitre.setCellValue("Sélection de périodes REDAC-CDE");
        for (int i = 1; i < 8; i++) {
            ligneTitre.createCell(i).setCellStyle(styleCelluleTitre);
        }
        criteres.addMergedRegion(new CellRangeAddress(0, 0, 0, 7));

        // Ligne date de génération
        Row ligneDateGeneration = criteres.createRow(rowNum++);
        Cell celluleLibelleDateGeneration = ligneDateGeneration.createCell(0);
        celluleLibelleDateGeneration.setCellStyle(styleCelluleParDefaut);
        celluleLibelleDateGeneration.setCellValue("générée le :");
        Cell celluleValeurDateGeneration = ligneDateGeneration.createCell(1);
        celluleValeurDateGeneration.setCellStyle(styleValeur);
        celluleValeurDateGeneration.setCellValue(new SimpleDateFormat("dd/MM/yyyy").format(Calendar.getInstance().getTime()));

        // Saut de ligne
        rowNum++;

        // Ligne critères de sélection
        Cell celluleCriteres = criteres.createRow(rowNum++).createCell(0);
        celluleCriteres.setCellStyle(creeStyleCelluleSousTitre(workbook));
        celluleCriteres.setCellValue("Critères de sélection :");

        // Critères
        PeriodesRecuesCriteresRecherche criteresRecherche = CRITERES_RECHERCHES.get();

        Row ligneCriteres1 = criteres.createRow(rowNum++);
        creeCelluleFormulaireLibelle(ligneCriteres1, CRITERE_POSITION_LIBELLE_1, styleLibelle, "statut :");
        creeCelluleFormulaireValeur(ligneCriteres1, CRITERE_POSITION_VALEUR_1, styleValeur,
                getLibellesEdition(criteresRecherche.getStatutsAsList(), ParamCodeLibelle.TABLE_PERIODE_RECUE, "ETAT"));
        creeCelluleFormulaireLibelle(ligneCriteres1, CRITERE_POSITION_LIBELLE_2, styleLibelle, "grpe gestion :");
        creeCelluleFormulaireValeur(ligneCriteres1, CRITERE_POSITION_VALEUR_2, styleValeur,
                getLibellesEdition(criteresRecherche.getGroupesGestionAsList(), ParamCodeLibelle.TABLE_CONTRAT, "NMGRPGES"));
        creeCelluleFormulaireLibelle(ligneCriteres1, CRITERE_POSITION_LIBELLE_3, styleLibelle, "n° client :");
        creeCelluleFormulaireValeur(ligneCriteres1, CRITERE_POSITION_VALEUR_3, styleValeur, criteresRecherche.getNumClient());

        Row ligneCriteres2 = criteres.createRow(rowNum++);
        creeCelluleFormulaireLibelle(ligneCriteres2, CRITERE_POSITION_LIBELLE_1, styleLibelle, "avec message :");
        creeCelluleFormulaireValeur(ligneCriteres2, CRITERE_POSITION_VALEUR_1, styleValeur,
                getLibellesMessages(criteresRecherche.getMessagesAsList()));
        creeCelluleFormulaireLibelle(ligneCriteres2, CRITERE_POSITION_LIBELLE_2, styleLibelle, "famille :");
        creeCelluleFormulaireValeur(ligneCriteres2, CRITERE_POSITION_VALEUR_2, styleValeur,
                getLibellesEdition(criteresRecherche.getFamillesAsList(), ParamCodeLibelle.TABLE_CONTRAT, "NOFAM"));
        creeCelluleFormulaireLibelle(ligneCriteres2, CRITERE_POSITION_LIBELLE_3, styleLibelle, "siren :");
        creeCelluleFormulaireValeur(ligneCriteres2, CRITERE_POSITION_VALEUR_3, styleValeur, criteresRecherche.getSiren());

        Row ligneCriteres3 = criteres.createRow(rowNum++);
        creeCelluleFormulaireLibelle(ligneCriteres3, CRITERE_POSITION_LIBELLE_1, styleLibelle, "lib. message :");
        creeCelluleFormulaireValeur(ligneCriteres3, CRITERE_POSITION_VALEUR_1, styleValeur,
                getLibellesMessagesControle(criteresRecherche.getIdControlesAsList()));
        creeCelluleFormulaireLibelle(ligneCriteres3, CRITERE_POSITION_LIBELLE_2, styleLibelle, "période :");
        List<String> libellesTrimestres = getLibellesTrimestres(criteresRecherche.getTrimestresAsList());
        creeCelluleFormulaireValeur(ligneCriteres3, CRITERE_POSITION_VALEUR_2, styleValeur, libellesTrimestres);
        creeCelluleFormulaireLibelle(ligneCriteres3, CRITERE_POSITION_LIBELLE_3, styleLibelle, "nic :");
        creeCelluleFormulaireValeur(ligneCriteres3, CRITERE_POSITION_VALEUR_3, styleValeur, criteresRecherche.getNic());

        Row ligneCriteres4 = criteres.createRow(rowNum++);
        creeCelluleFormulaireLibelle(ligneCriteres4, CRITERE_POSITION_LIBELLE_1, styleLibelle, "nature :");
        creeCelluleFormulaireValeur(ligneCriteres4, CRITERE_POSITION_VALEUR_1, styleValeur,
                getLibellesEdition(criteresRecherche.getTypesPeriodeAsList(), "PERIODERECUE", "TYPE"));
        creeCelluleFormulaireLibelle(ligneCriteres4, CRITERE_POSITION_LIBELLE_2, styleLibelle, "cpt enc :");
        creeCelluleFormulaireValeur(ligneCriteres4, CRITERE_POSITION_VALEUR_2, styleValeur, criteresRecherche.getCptEnc());
        creeCelluleFormulaireLibelle(ligneCriteres4, CRITERE_POSITION_LIBELLE_3, styleLibelle, "raison soc. :");
        creeCelluleFormulaireValeur(ligneCriteres4, CRITERE_POSITION_VALEUR_3, styleValeur, criteresRecherche.getRaisonSociale());

        Row ligneCriteres5 = criteres.createRow(rowNum++);
        creeCelluleFormulaireLibelle(ligneCriteres5, CRITERE_POSITION_LIBELLE_1, styleLibelle, "contrat :");
        creeCelluleFormulaireValeur(ligneCriteres5, CRITERE_POSITION_VALEUR_1, styleValeur, criteresRecherche.getNumContrat());
        creeCelluleFormulaireLibelle(ligneCriteres5, CRITERE_POSITION_LIBELLE_2, styleLibelle, "cpt prod :");
        creeCelluleFormulaireValeur(ligneCriteres5, CRITERE_POSITION_VALEUR_2, styleValeur, criteresRecherche.getCptProd());
        creeCelluleFormulaireLibelle(ligneCriteres5, CRITERE_POSITION_LIBELLE_3, styleLibelle, "VIP :");
        creeCelluleFormulaireValeur(ligneCriteres5, CRITERE_POSITION_VALEUR_3, styleValeur,
                getLibellesEdition(criteresRecherche.getVipAsList(), ParamCodeLibelle.TABLE_EXTENSION_CONTRAT, "VIP"));

        Row ligneCriteres6 = criteres.createRow(rowNum++);
        creeCelluleFormulaireLibelle(ligneCriteres6, CRITERE_POSITION_LIBELLE_1, styleLibelle, "versement :");
        List<Boolean> versements = criteresRecherche.getVersementsAsList();
        creeCelluleFormulaireValeur(ligneCriteres6, CRITERE_POSITION_VALEUR_1, styleValeur, getLibellesVersements(versements));

        Row ligneCriteres7 = criteres.createRow(rowNum++);
        creeCelluleFormulaireLibelle(ligneCriteres7, CRITERE_POSITION_LIBELLE_1, styleLibelle, "Affecté à :");
        creeCelluleFormulaireValeur(ligneCriteres7, CRITERE_POSITION_VALEUR_1, styleValeur,
                verificationValeursSpeciales(getNomEtPrenoms(criteresRecherche.getAffecteAAsList())));

        creeCelluleFormulaireLibelle(ligneCriteres7, CRITERE_POSITION_LIBELLE_2, styleLibelle, "A traiter par :");
        creeCelluleFormulaireValeur(ligneCriteres7, CRITERE_POSITION_VALEUR_2, styleValeur,
                verificationValeursSpeciales(getNomEtPrenoms(criteresRecherche.getaTraiterParAsList())));

        creeCelluleFormulaireLibelle(ligneCriteres7, CRITERE_POSITION_LIBELLE_3, styleLibelle, "Traité par :");
        creeCelluleFormulaireValeur(ligneCriteres7, CRITERE_POSITION_VALEUR_3, styleValeur,
                verificationValeursSpeciales(getNomEtPrenoms(criteresRecherche.getTraiteParAsList())));

        for (int col = 0; col < 8; col++) {
            criteres.autoSizeColumn(col);
        }
    }

    /*
     * Gère les cas NON_AFFECTE, NON_ASSIGNE, TRAITE_AUTO dans une liste
     */
    private List<String> verificationValeursSpeciales(List<String> list) {
        Integer i = 0;
        for (String valeurEntree : list) {
            switch (valeurEntree) {

            case PeriodesRecuesCriteresRecherche.NON_AFFECTE_CODE:
                list.set(i, PeriodesRecuesCriteresRecherche.NON_AFFECTE_LIBELLE);
                break;
            case PeriodesRecuesCriteresRecherche.NON_ASSIGNE_CODE:
                list.set(i, PeriodesRecuesCriteresRecherche.NON_ASSIGNE_LIBELLE);
                break;
            case PeriodesRecuesCriteresRecherche.TRAITE_AUTO_CODE:
                list.set(i, PeriodesRecuesCriteresRecherche.TRAITE_AUTO_LIBELLE);
                break;
            default:
                // aucun traitement
                break;
            }
            i = i + 1;
        }
        return list;
    }

    /**
     * Récupère les libellés d'édition pour les critères sur versement.
     * 
     * @param versements
     *            les codes de versement
     * @return les libellés correspondant
     */
    private List<String> getLibellesVersements(List<Boolean> versements) {
        List<String> libellesVersements = new ArrayList<>(versements.size());
        if (versements.contains(Boolean.FALSE)) {
            libellesVersements.add("Aucun");
        }
        if (versements.contains(Boolean.TRUE)) {
            libellesVersements.add("Au moins 1");
        }
        return libellesVersements;
    }

    /**
     * Récupère les libellés des messages d'alerte pour les id controles donnees.
     * 
     * @param idControles
     *            les idControles
     * @return les libellés des messages d'alerte correspondants au idControles
     */
    private List<String> getLibellesMessagesControle(List<String> idControles) {
        List<String> libellesMessagesControles = new ArrayList<>(idControles.size());
        for (String idControle : idControles) {

            ParamControleSignalRejet paramControleSignalRejet = paramControleSignalRejetRepository.getControlePourIdControle(idControle);
            if (paramControleSignalRejet != null) {
                libellesMessagesControles.add(paramControleSignalRejet.getMessageAlerte().replace("$1", "XX"));
            }
        }
        return libellesMessagesControles;
    }

    /**
     * Récupère les libellés de trimestres sélectionnés en tant que critères de recherche à partir des dates de début de ceux-ci.
     * 
     * @param trimestres
     *            les dates de début de trimestre
     * @return les libellés des critères sélectionnés
     */
    private List<String> getLibellesTrimestres(List<String> trimestres) {
        List<String> libellesTrimestres = new ArrayList<>(trimestres.size());
        if (trimestres.isEmpty()) {
            libellesTrimestres = Arrays.asList("Toutes");
        } else {
            Calendar cal = Calendar.getInstance();
            int moisActuel = cal.get(Calendar.MONTH);
            int moisDebutTrimestreEnCours = moisActuel % 3 == 0 ? moisActuel : moisActuel - (moisActuel % 3);
            cal.set(Calendar.MONTH, moisDebutTrimestreEnCours);
            cal.set(Calendar.DAY_OF_MONTH, 1);
            String trimestreEnCours = stringify(DateRedac.convertitEnDateRedac(cal.getTime()));
            for (String trimestre : trimestres) {
                if (StringUtils.equals(trimestreEnCours, trimestre)) {
                    libellesTrimestres.add("En cours");
                } else {
                    libellesTrimestres.add(getLibelleTrimestre(trimestre));
                }
            }
        }
        return libellesTrimestres;
    }

    /**
     * Récupère le libellé d'un trimestre à partir de la date de début de celui-ci.
     * 
     * @param trimestre
     *            la date de début de trimestre
     * @return le libellé correspondant au format "AAAATN"
     */
    private String getLibelleTrimestre(String trimestre) {
        if (!StringUtils.isNumeric(trimestre)) {
            return trimestre;
        }
        String annee = String.valueOf(DateRedac.extraitAnnee(trimestre));
        StringBuilder libelleTrimestre = new StringBuilder(annee).append("T");
        Integer mois = DateRedac.extraitMois(trimestre);
        if (mois == 1) {
            libelleTrimestre.append(1);
        } else if (mois == 4) {
            libelleTrimestre.append(2);
        } else if (mois == 7) {
            libelleTrimestre.append(3);
        } else if (mois == 10) {
            libelleTrimestre.append(4);
        }
        return libelleTrimestre.toString();
    }

    /**
     * Récupère les libellés d'édition pour les critères sur message.
     * 
     * @param messages
     *            les codes de message
     * @return les libellés correspondant
     */
    private List<String> getLibellesMessages(List<String> messages) {
        List<String> libellesMessages = new ArrayList<>(messages.size());
        for (String message : messages) {
            if (StringUtils.equals(ParamControleSignalRejet.NIVEAU_ALERTE_AUCUN, message)) {
                libellesMessages.add("sans message");
            } else if (StringUtils.equals(ParamControleSignalRejet.NIVEAU_ALERTE_SIGNAL, message)) {
                libellesMessages.add("avec signalement");
            } else if (StringUtils.equals(ParamControleSignalRejet.NIVEAU_ALERTE_REJET, message)) {
                libellesMessages.add("avec rejet");
            }
        }
        return libellesMessages;
    }

    /*
     * 
     * 
     * 
     * */
    private List<String> getNomEtPrenoms(List<String> codeUsers) {
        List<String> nomsEtPrenoms = new ArrayList<>(codeUsers.size());
        for (String codeUser : codeUsers) {

            if (paramUtilisateurGestionnaireRepository.getNomEtPrenomAvecCodeUser(codeUser) == null) {
                nomsEtPrenoms.add(codeUser);
            } else {

                nomsEtPrenoms.add(paramUtilisateurGestionnaireRepository.getNomEtPrenomAvecCodeUser(codeUser) + " - " + codeUser);
            }
        }
        return nomsEtPrenoms;
    }

    /**
     * Crée une cellule pour une multi-valeur de formulaire.
     * 
     * @param ligneFormulaire
     *            la ligne du formulaire à remplir
     * @param positionValeur
     *            la position de la valeur dans la ligne
     * @param styleValeur
     *            le style du libellé
     * @param multiValeur
     *            les valeurs du critère
     */
    private void creeCelluleFormulaireValeur(Row ligneFormulaire, int positionValeur, CellStyle styleValeur, List<?> multiValeur) {
        creeCelluleFormulaireValeur(ligneFormulaire, positionValeur, styleValeur, StringUtils.join(multiValeur, CRITERES_JOIN_CHAR));
    }

    private CellStyle creeStyleCelluleTitre(Workbook workbook) {
        Font fontTitre = workbook.createFont();
        fontTitre.setFontHeightInPoints(FONT_TAILLE_TITRE);
        fontTitre.setFontName(FONT_NOM);

        CellStyle styleTitre = workbook.createCellStyle();
        styleTitre.setFont(fontTitre);
        styleTitre.setBorderBottom(CellStyle.BORDER_THIN);
        styleTitre.setBottomBorderColor(IndexedColors.BLACK.getIndex());
        styleTitre.setBorderRight(CellStyle.BORDER_THIN);
        styleTitre.setRightBorderColor(IndexedColors.BLACK.getIndex());
        styleTitre.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
        styleTitre.setAlignment(CellStyle.ALIGN_CENTER);
        return styleTitre;
    }

    private CellStyle creeStyleCelluleSousTitre(Workbook workbook) {
        Font fontSousTitre = workbook.createFont();
        fontSousTitre.setFontHeightInPoints(FONT_TAILLE_SOUS_TITRE);
        fontSousTitre.setFontName(FONT_NOM);
        fontSousTitre.setUnderline(Font.U_SINGLE);

        CellStyle styleSousTitre = workbook.createCellStyle();
        styleSousTitre.setFont(fontSousTitre);
        return styleSousTitre;
    }

    private void creeOngletExtraction(Workbook workbook) {
        Sheet extraction = workbook.createSheet("Extraction");

        // Styles

        CellStyle styleCellule = creeStyleCelluleParDefaut(workbook, false);
        enrichitStyleCelluleAvecToutesBordures(styleCellule);
        styleCellule.setWrapText(true);

        // Paramétrage générique
        String vipParDefaut = paramValeurDefautRepository.getVIPParDefaut();

        PeriodesRecuesCriteresRecherche criteresRecherche = CRITERES_RECHERCHES.get();
        long nbEntitesAExporter = periodeRecueRepository.compte(criteresRecherche);
        int pageTaille = getPageTaille();
        int maxPage = Long.valueOf(nbEntitesAExporter % pageTaille == 0 ? nbEntitesAExporter / pageTaille : (nbEntitesAExporter / pageTaille) + 1)
                .intValue();

        criteresRecherche.setPageTaille(pageTaille);
        int rowNum = 0;
        Row ligneEntete = extraction.createRow(rowNum++);
        remplitLigneEntete(ligneEntete, styleCellule);

        // Recupération des libelles edition

        mapGroupeGestion = paramCodeLibelleRepository.getMap(ParamCodeLibelle.TABLE_CONTRAT, "NMGRPGES");
        mapNumFamilleProduit = paramCodeLibelleRepository.getMap(ParamCodeLibelle.TABLE_CONTRAT, "NOFAM");
        mapVip = paramCodeLibelleRepository.getMap(ParamCodeLibelle.TABLE_EXTENSION_CONTRAT, "VIP");
        mapType = paramCodeLibelleRepository.getMap(ParamCodeLibelle.TABLE_PERIODE_RECUE, "TYPE");
        mapModePaiement = paramCodeLibelleRepository.getMap(ParamCodeLibelle.TABLE_VERSEMENT, "MODE_PAIEMENT");
        mapEtatPeriode = paramCodeLibelleRepository.getMap(ParamCodeLibelle.TABLE_PERIODE_RECUE, "ETAT");
        mapModeGestDSN = paramCodeLibelleRepository.getMap(ParamCodeLibelle.TABLE_INDICATEURS_DSN_PERIODE, "MODE_GEST");
        mapTransfert = paramCodeLibelleRepository.getMap(ParamCodeLibelle.TABLE_EXTENSION_CONTRAT, "TRANSFERT");

        for (int page = 1; page <= maxPage; page++) {
            criteresRecherche.setPageNumero(page);
            List<ExportPeriodeExcel> periodes = periodeRecueRepository.recherchePeriodesRecuesNatifExportExcel(criteresRecherche);

            for (ExportPeriodeExcel periode : periodes) {
                HistoriqueAssignationPeriode dernierHistoriqueAssignation = historiqueAssignationPeriodeRepository
                        .getDerniereAssignationPourPeriode(periode.getIdPeriode());

                if (dernierHistoriqueAssignation == null) {
                    dernierHistoriqueAssignation = new HistoriqueAssignationPeriode();
                }
                Row ligneContrat = extraction.createRow(rowNum++);
                remplitLignePeriode(ligneContrat, periode, styleCellule, vipParDefaut, dernierHistoriqueAssignation);
            }
        }

        // nombre de caractères à afficher par colonnes
        int[] widthArray = { 40, 40, 15, 8, 7, 6, 9, 5, 40, 8, 13, 11, 15, 9, 12, 13, 13, 40, 30, 11, 20, 20, 20, 80, 80, 80, 80, 40, 14, 16 };

        redimensionnementColonnes(extraction, widthArray);

        extraction.setAutoFilter(new CellRangeAddress(0, 0, 0, 26));
    }

    /**
     * Remplit les cases d'une ligne entête.
     * 
     * @param ligneEntete
     *            la ligne d'entête
     * @param styleCellule
     *            le style à appliquer à chaque case
     */
    private void remplitLigneEntete(Row ligneEntete, CellStyle styleCellule) {
        int cellNum = 0;
        creeCellule(ligneEntete, cellNum++, styleCellule, "Grpe Gest.");
        creeCellule(ligneEntete, cellNum++, styleCellule, "Famille");
        creeCellule(ligneEntete, cellNum++, styleCellule, "Contrat");
        creeCellule(ligneEntete, cellNum++, styleCellule, "Cpt prod");
        creeCellule(ligneEntete, cellNum++, styleCellule, "Cpt enc");
        creeCellule(ligneEntete, cellNum++, styleCellule, "Num client ppal");
        creeCellule(ligneEntete, cellNum++, styleCellule, "SIREN client ppal");
        creeCellule(ligneEntete, cellNum++, styleCellule, "NIC client ppal");
        creeCellule(ligneEntete, cellNum++, styleCellule, "Raison soc client ppal");
        creeCellule(ligneEntete, cellNum++, styleCellule, "VIP");
        creeCellule(ligneEntete, cellNum++, styleCellule, "Début Période");
        creeCellule(ligneEntete, cellNum++, styleCellule, "Fin Période");
        creeCellule(ligneEntete, cellNum++, styleCellule, "Nature Période");
        creeCellule(ligneEntete, cellNum++, styleCellule, "Nb établt");
        creeCellule(ligneEntete, cellNum++, styleCellule, "Nb individus");
        creeCellule(ligneEntete, cellNum++, styleCellule, "Mt cotisation");
        creeCellule(ligneEntete, cellNum++, styleCellule, "Montant verst");
        creeCellule(ligneEntete, cellNum++, styleCellule, "Mode paiemt");
        creeCellule(ligneEntete, cellNum++, styleCellule, "Statut");
        creeCellule(ligneEntete, cellNum++, styleCellule, "Date Statut");
        creeCellule(ligneEntete, cellNum++, styleCellule, "Affecté à");
        creeCellule(ligneEntete, cellNum++, styleCellule, "A traiter par");
        creeCellule(ligneEntete, cellNum++, styleCellule, "A traiter depuis");
        creeCellule(ligneEntete, cellNum++, styleCellule, "Traité par");
        creeCellule(ligneEntete, cellNum++, styleCellule, "Signalts REDAC");
        creeCellule(ligneEntete, cellNum++, styleCellule, "Rejets REDAC");
        creeCellule(ligneEntete, cellNum++, styleCellule, "Signalts SI AVAL");
        creeCellule(ligneEntete, cellNum++, styleCellule, "Rejets SI AVAL");
        creeCellule(ligneEntete, cellNum++, styleCellule, "Ind. Mode Gest");
        creeCellule(ligneEntete, cellNum++, styleCellule, "Ind. Transfert");
        creeCellule(ligneEntete, cellNum++, styleCellule, "Date intégration");
    }

    /**
     * Remplit les cases d'une ligne période.
     * 
     * @param lignePeriode
     *            la ligne période
     * @param periodeRecue
     *            la période à exporter
     * @param vipParDefaut
     *            flag VIP par défaut
     */
    private void remplitLignePeriode(Row lignePeriode, ExportPeriodeExcel periodeRecue, CellStyle styleCellule, String vipParDefaut,
            HistoriqueAssignationPeriode dernierHistoriqueAssignation) {
        int cellNum = 0;

        // Libellé du groupe de gestion
        if (periodeRecue.getGroupeGestion() != null) {
            String libelleGestion = mapGroupeGestion.get(periodeRecue.getGroupeGestion());
            creeCellule(lignePeriode, cellNum++, styleCellule, libelleGestion == null ? LIBELLE_INTROUVABLE : libelleGestion);
        } else {
            creeCellule(lignePeriode, cellNum++, styleCellule, LIBELLE_INTROUVABLE);
        }

        // Libellé de la famille
        if (periodeRecue.getNumFamilleProduit() != null) {
            String libelleNumFamille = mapNumFamilleProduit.get(periodeRecue.getNumFamilleProduit());
            creeCellule(lignePeriode, cellNum++, styleCellule, libelleNumFamille == null ? LIBELLE_INTROUVABLE : libelleNumFamille);
        } else {
            creeCellule(lignePeriode, cellNum++, styleCellule, LIBELLE_INTROUVABLE);
        }

        // Libellé du contrat
        creeCellule(lignePeriode, cellNum++, styleCellule, periodeRecue.getNumeroContrat());

        // Libellé du cpt prod
        creeCellule(lignePeriode, cellNum++, styleCellule,
                periodeRecue.getNumCompteProducteur() != null ? periodeRecue.getNumCompteProducteur() : StringUtils.EMPTY);

        // Libellé du cpt enc
        creeCellule(lignePeriode, cellNum++, styleCellule,
                periodeRecue.getCompteEncaissement() != null ? periodeRecue.getCompteEncaissement() : StringUtils.EMPTY);

        // Libellé du numéro de client principal
        creeCellule(lignePeriode, cellNum++, styleCellule,
                periodeRecue.getNumSouscripteur() != null ? stringify(periodeRecue.getNumSouscripteur()) : StringUtils.EMPTY);

        // Libellé du SIREN client ppal
        creeCellule(lignePeriode, cellNum++, styleCellule, periodeRecue.getNumSiren() != null ? periodeRecue.getNumSiren() : StringUtils.EMPTY);

        // Libellé du NIC client ppal
        creeCellule(lignePeriode, cellNum++, styleCellule, periodeRecue.getNumSiret() != null ? periodeRecue.getNumSiret() : StringUtils.EMPTY);

        // Libellé de la raison soc client ppal
        creeCellule(lignePeriode, cellNum++, styleCellule,
                periodeRecue.getRaisonSociale() != null ? periodeRecue.getRaisonSociale() : StringUtils.EMPTY);

        // Libellé de la condition contrat VIP ou non
        String libelleVip = mapVip.get(periodeRecue.getVipAsText());
        creeCellule(lignePeriode, cellNum++, styleCellule, libelleVip == null ? mapVip.get(vipParDefaut) : libelleVip);

        // Libellé du Début Période
        creeCellule(lignePeriode, cellNum++, styleCellule, DateRedac.formate(getFormatDate(), periodeRecue.getDateDebutPeriode()));

        // Libellé du Fin Période
        creeCellule(lignePeriode, cellNum++, styleCellule, DateRedac.formate(getFormatDate(), periodeRecue.getDateFinPeriode()));

        // Libellé de la Nature Période
        String libelleType = mapType.get(periodeRecue.getTypePeriode());
        creeCellule(lignePeriode, cellNum++, styleCellule, libelleType == null ? LIBELLE_INTROUVABLE : libelleType);

        // Libellé du nb d'établissements
        creeCellule(lignePeriode, cellNum++, styleCellule, (double) periodeRecue.getNbEtablissementsRattaches());

        // Libellé du nb d'individus
        // Version Mysql
        creeCellule(lignePeriode, cellNum++, styleCellule, (double) periodeRecue.getNbSalariesRattaches());

        // Libellé du montant de cotisation
        creeCellule(lignePeriode, cellNum++, styleCellule, periodeRecue.getSommeMontantCotisation());

        // Libellé du montant des versements
        creeCellule(lignePeriode, cellNum++, styleCellule, periodeRecue.getSommeComposantVersement());

        // Libellé du mode de paiement
        String libelleModePaiement = StringUtils.EMPTY;
        if (periodeRecue.getModesPaiementDistincts() != null) {
            String[] modesPaiementDistincts = StringUtils.split(periodeRecue.getModesPaiementDistincts(), ",");
            if (modesPaiementDistincts.length == 1) {
                libelleModePaiement = mapModePaiement.get(modesPaiementDistincts[0]);
            } else if (modesPaiementDistincts.length > 1) {
                libelleModePaiement = "MIXTE";
            }
        }

        creeCellule(lignePeriode, cellNum++, styleCellule, libelleModePaiement == null ? LIBELLE_INTROUVABLE : libelleModePaiement);

        // Libellé du statut
        String libelleEtat = mapEtatPeriode.get(periodeRecue.getEtatPeriode());
        creeCellule(lignePeriode, cellNum++, styleCellule, libelleEtat == null ? LIBELLE_INTROUVABLE : libelleEtat);

        // Libellé de la date du statut
        creeCellule(lignePeriode, cellNum++, styleCellule,
                periodeRecue.getDerniereDateChangementStatut() == null || periodeRecue.getDerniereDateChangementStatut() == 0 ? StringUtils.EMPTY
                        : DateRedac.formate(getFormatDate(),
                                Integer.valueOf(periodeRecue.getDerniereDateChangementStatut().toString().substring(0, 8))));

        // Libellé de l'utilisateur gestionnaire affecté à la période
        if (paramUtilisateurGestionnaireRepository.getNomEtPrenomAvecCodeUser(periodeRecue.getAffecteA()) == null) {
            creeCellule(lignePeriode, cellNum++, styleCellule, periodeRecue.getAffecteA());
        } else {
            creeCellule(lignePeriode, cellNum++, styleCellule,
                    paramUtilisateurGestionnaireRepository.getNomEtPrenomAvecCodeUser(periodeRecue.getAffecteA()) + " - "
                            + periodeRecue.getAffecteA());
        }
        // Libellé de l'utilisateur gestionnaire devant traiter la période
        if (paramUtilisateurGestionnaireRepository.getNomEtPrenomAvecCodeUser(periodeRecue.getATraiterPar()) == null) {
            creeCellule(lignePeriode, cellNum++, styleCellule, periodeRecue.getATraiterPar());
        } else {
            creeCellule(lignePeriode, cellNum++, styleCellule,
                    paramUtilisateurGestionnaireRepository.getNomEtPrenomAvecCodeUser(periodeRecue.getATraiterPar()) + " - "
                            + periodeRecue.getATraiterPar());
        }
        // Date de l'assignation la plus récente de la période
        if (dernierHistoriqueAssignation.getDateHeureAssignation() == null || dernierHistoriqueAssignation.getDateHeureAssignation() == 0) {
            creeCellule(lignePeriode, cellNum++, styleCellule, "");
        } else {
            creeCellule(lignePeriode, cellNum++, styleCellule, dernierHistoriqueAssignation.getDateHeureAssignationAsDate());
        }

        // Libellé de l'utilisateur gestionnaire ayant traité la période
        if (paramUtilisateurGestionnaireRepository.getNomEtPrenomAvecCodeUser(periodeRecue.getTraitePar()) == null) {
            creeCellule(lignePeriode, cellNum++, styleCellule,
                    periodeRecue.getTraitePar() != null ? periodeRecue.getTraitePar() : PeriodesRecuesCriteresRecherche.TRAITE_AUTO_LIBELLE);
        } else {
            creeCellule(lignePeriode, cellNum++, styleCellule,
                    paramUtilisateurGestionnaireRepository.getNomEtPrenomAvecCodeUser(periodeRecue.getTraitePar()) + " - "
                            + periodeRecue.getTraitePar());
        }
        // Messages signalements REDAC
        if (periodeRecue.getSignaltsRedac().length() > MAX_CHAR_CELL) {
            creeCellule(lignePeriode, cellNum++, styleCellule, periodeRecue.getSignaltsRedac().substring(0, MAX_CHAR_CELL));
        } else {
            creeCellule(lignePeriode, cellNum++, styleCellule, periodeRecue.getSignaltsRedac());
        }

        // Messages rejets REDAC
        if (periodeRecue.getRejetsRedac().length() > MAX_CHAR_CELL) {
            creeCellule(lignePeriode, cellNum++, styleCellule, periodeRecue.getRejetsRedac().substring(0, MAX_CHAR_CELL));
        } else {
            creeCellule(lignePeriode, cellNum++, styleCellule, periodeRecue.getRejetsRedac());
        }

        // Messages signalements SIAVAL
        if (periodeRecue.getSignaltsSiAval().length() > MAX_CHAR_CELL) {
            creeCellule(lignePeriode, cellNum++, styleCellule, periodeRecue.getSignaltsSiAval().substring(0, MAX_CHAR_CELL));
        } else {
            creeCellule(lignePeriode, cellNum++, styleCellule, periodeRecue.getSignaltsSiAval());
        }

        // Messages rejets SIAVAL
        if (periodeRecue.getRejetsSiAval().length() > MAX_CHAR_CELL) {
            creeCellule(lignePeriode, cellNum++, styleCellule, periodeRecue.getRejetsSiAval().substring(0, MAX_CHAR_CELL));
        } else {
            creeCellule(lignePeriode, cellNum++, styleCellule, periodeRecue.getRejetsSiAval());
        }

        // Libellé de l'indicateur du mode de gestion
        String libelleModeGestion = periodeRecue.getModeGestionDSN() == null ? StringUtils.EMPTY
                : mapModeGestDSN.get(periodeRecue.getModeGestionDSN());
        creeCellule(lignePeriode, cellNum++, styleCellule, libelleModeGestion == null ? LIBELLE_INTROUVABLE : libelleModeGestion);

        // Libellé de l'indicateur de transfert
        String libelleTransfert = periodeRecue.getTransfertDSN() == null ? StringUtils.EMPTY : mapTransfert.get(periodeRecue.getTransfertDSN());
        creeCellule(lignePeriode, cellNum++, styleCellule, libelleTransfert == null ? LIBELLE_INTROUVABLE : libelleTransfert);

        // Libellé de la date d'intégration
        String libelleDateIntegration = periodeRecue.getDateTraitement() != null && periodeRecue.getDateTraitement() != 0
                ? DateRedac.formate(getFormatDate(), periodeRecue.getDateTraitement()) : StringUtils.EMPTY;
        creeCellule(lignePeriode, cellNum++, styleCellule, libelleDateIntegration);

    }
}
