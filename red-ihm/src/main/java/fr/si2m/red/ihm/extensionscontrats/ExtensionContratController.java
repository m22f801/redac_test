package fr.si2m.red.ihm.extensionscontrats;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import fr.si2m.red.DateRedac;
import fr.si2m.red.complement.ExtensionContrat;
import fr.si2m.red.complement.ExtensionContratRepository;
import fr.si2m.red.complement.ExtensionEntrepriseAffiliee;
import fr.si2m.red.complement.ExtensionEntrepriseAffilieeRepository;
import fr.si2m.red.complement.ExtensionsContratsDetailsEnregistrement;
import fr.si2m.red.contrat.Contrat;
import fr.si2m.red.contrat.ContratRepository;
import fr.si2m.red.contrat.TarifRepository;
import fr.si2m.red.core.repository.jpa.conversion.ConvertisseurBooleanON;
import fr.si2m.red.core.repository.jpa.conversion.ConvertisseurBooleanOuiNon;
import fr.si2m.red.ihm.FilAriane;
import fr.si2m.red.ihm.Pages;
import fr.si2m.red.ihm.RedacIhmController;
import fr.si2m.red.ihm.RoleRedac;
import fr.si2m.red.ihm.exceptions.RessourceNonTrouveeException;
import fr.si2m.red.ihm.session.UtilisateurIHM;
import fr.si2m.red.parametrage.ParamCodeLibelle;
import fr.si2m.red.parametrage.ParamCodeLibelleRepository;
import fr.si2m.red.parametrage.ParamFamilleModeCalculContratRepository;
import fr.si2m.red.parametrage.ParamValeurDefaut;
import fr.si2m.red.parametrage.ParamValeurDefautRepository;
import fr.si2m.red.reconciliation.IndicateursDSNPeriodeRepository;

/**
 * Contrôleur d'une page de détail de contrat.
 * 
 * @author nortaina
 *
 */
@Controller
@RequestMapping("/extensionscontrats")
public class ExtensionContratController extends RedacIhmController {
    private static final Logger LOGGER = LoggerFactory.getLogger(ExtensionContratController.class);

    @Autowired
    private ContratRepository contratRepository;

    @Autowired
    private ExtensionContratRepository extensionContratRepository;

    @Autowired
    private ParamCodeLibelleRepository paramCodeLibelleRepository;

    @Autowired
    private TarifRepository tarifRepository;

    @Autowired
    private ParamFamilleModeCalculContratRepository paramFamilleModeCalculContratRepository;

    @Autowired
    private ParamValeurDefautRepository paramValeurDefautRepository;

    @Autowired
    private ExtensionEntrepriseAffilieeRepository extensionEntrepriseAffilieeRepository;

    @Autowired
    private IndicateursDSNPeriodeRepository indicateursDSNPeriodeRepository;

    /**
     * Récupère la page de détail d'un contrat.
     * 
     * @param numContrat
     *            le numéro du contrat
     * @param criteresRecherche
     *            les critères de la recherche ayant permi de naviguer vers ce contrat
     * @param details
     *            les détails du contrat
     * @param httpRequest
     *            la requête de récupération
     * 
     * @return le modèle et la vue de la page de détail
     * 
     * @throws RessourceNonTrouveeException
     *             levée si le contrat n'est pas trouvé
     * 
     */
    @Secured(RoleRedac.ROLE_REDCONSULT)
    @RequestMapping(value = "/{numContrat}", method = RequestMethod.GET, produces = MediaType.TEXT_HTML_VALUE)
    @Transactional(propagation = Propagation.REQUIRES_NEW, rollbackFor = Throwable.class)
    public ModelAndView getExtensionContrat(@PathVariable("numContrat") String numContrat,
            @RequestParam(value = "criteresRecherche", required = false) String criteresRecherche,
            @ModelAttribute("details") ExtensionsContratsDetailsEnregistrement details, HttpServletRequest httpRequest)
            throws RessourceNonTrouveeException {

        // Détermine s'il s'agit d'une réinitialisation de l'écran => ceci est équivalent à un objet "details" vide
        boolean reinitialisation = details.getVipSaisi() == null;

        // Récupération du contrat
        Contrat contrat = contratRepository.getDerniereSituationContrat(numContrat);

        ModelAndView mav = null;
        if (contrat != null) {
            // Libellés complémentaires pour le contrat
            Map<String, String> libelles = new HashMap<String, String>();
            Integer mocalcot = tarifRepository.getModeCalculCotisationPourDetailsContrat(numContrat);
            libelles.put("nature", paramCodeLibelleRepository.getLibelleCourt(ParamCodeLibelle.TABLE_TARIF, "MOCALCOT",
                    mocalcot == null ? "" : mocalcot.toString()));
            libelles.put("eligibilite",
                    paramCodeLibelleRepository.getLibelleCourt(ParamCodeLibelle.TABLE_CONTRAT, "ELIGDSN", contrat.getEligibiliteContratDsn()));
            libelles.put("groupeGestion",
                    paramCodeLibelleRepository.getLibelleCourt(ParamCodeLibelle.TABLE_CONTRAT, "NMGRPGES", contrat.getGroupeGestion()));
            libelles.put("famille",
                    paramCodeLibelleRepository.getLibelleCourt(ParamCodeLibelle.TABLE_CONTRAT, "NOFAM", contrat.getNumFamilleProduit().toString()));
            libelles.put("periodicite",
                    paramCodeLibelleRepository.getLibelleCourt(ParamCodeLibelle.TABLE_CONTRAT, "COFRQUIT", contrat.getCodeFractionnement()));

            String libelleGroupeGestion = paramCodeLibelleRepository.getLibelleCourt(ParamCodeLibelle.TABLE_CONTRAT, "NMGRPGES",
                    contrat.getGroupeGestion());
            String libelleFamille = paramCodeLibelleRepository.getLibelleCourt(ParamCodeLibelle.TABLE_CONTRAT, "NOFAM",
                    contrat.getNumFamilleProduit().toString());

            mav = initialiseModeleVue("extensionscontrats/ExtensionContrat", httpRequest, Arrays.asList(contrat.getGroupeGestion()),
                    libelleGroupeGestion, Arrays.asList(contrat.getNumFamilleProduit()), libelleFamille);

            mav.addObject("libellesContrat", libelles);

            // champs complémentaires pour le contrat
            Map<String, Object> champs = new HashMap<String, Object>();
            champs.put("dateResiliation", DateRedac.convertitEnDate(contratRepository.getDateResiliation(numContrat)));
            champs.put("souscripteurRaisonSociale", contrat.getClient().getRaisonSociale());
            champs.put("souscripteurNumSiret", contrat.getClient().getNumSiren() + " " + contrat.getClient().getNumSiret());
            mav.addObject("champsContrat", champs);

        } else {
            mav = initialiseModeleVue("extensionscontrats/ExtensionContrat", httpRequest);
        }

        mav.addObject("contrat", contrat);

        // Récupération du statut (Standard/VIP) du contrat
        boolean isContratVIP = extensionContratRepository.isContratVIP(numContrat);
        mav.addObject("contratVIP", isContratVIP);

        // Récupération des droits habilitationVIP
        UtilisateurIHM utilisateur = getUtilisateurAuthentifie();
        mav.addObject("habilitationVIP", testeHabilitationVIP(utilisateur, numContrat, isContratVIP));

        // Info fil d'Ariane
        FilAriane filAriane = new FilAriane();
        filAriane.setFil(Arrays.asList(Pages.ACCUEIL, Pages.EXTENSIONSCONTRATS));
        mav.addObject("filAriane", filAriane);

        // Récupération des erreurs (issues d'une redirection)
        boolean erreursDetectees = false;
        if (StringUtils.isNotBlank(details.getListeErreurs())) {
            mav.addObject("listeErreurs", details.getListeErreurs());
            erreursDetectees = true;
            mav.addObject("listeSiret", nettoieListeSirenNic(details.getListeSirenNic()));
        } else {
            mav.addObject("listeSiret", extensionEntrepriseAffilieeRepository.getSiretEtablissementsAffiliesPourAffichage(numContrat));
        }

        remplissageDetails(details, mav, erreursDetectees, contrat, numContrat);

        mav.addObject("details", details);

        // Données de référence pour la recherche
        Map<String, Object> reference = new HashMap<String, Object>();
        reference.put("listeGroupesGestion", getGroupesGestionsSelectionnables());
        reference.put("listeFamilles", getFamillesSelectionnables());
        reference.put("listeNatures", getNaturesContratsSelectionnables());
        reference.put("listeGestionsDirectes", getModesNatureContratSelectionnables());
        reference.put("listeEligibiliteContratDsn", getEligibilitesSelectionnables());
        reference.put("listeVIP", getVipSelectionnables());
        reference.put("listeIndicsExploitation", getIndicExploitationSelectionnables());
        reference.put("listeIndicsConsignePaiement", getIndicConsignePaiementSelectionnables());
        reference.put("listeIndicsTransfert", getIndicTransfertSelectionnables());
        reference.put("listeModeReaffectation", getModesReaffectationSelectionnables());
        reference.put("listeBasesAssujetties", getBasesAssujettiesSelectionnables());
        reference.put("listeModeDeclaration", paramCodeLibelleRepository.get(ParamCodeLibelle.TABLE_EXTENSION_CONTRAT, "DONNEES_A_INTEGRER"));

        mav.addObject("reference", reference);

        // Construction du contexte de la page
        Map<String, Object> contexte = new HashMap<String, Object>();
        String rechercheUrlQueryString = StringUtils.replace(StringUtils.defaultString(criteresRecherche), ";", "&");
        if (StringUtils.isBlank(criteresRecherche)) {
            rechercheUrlQueryString = details.getRechercheUrlQueryString();
        }
        contexte.put("rechercheUrlQueryString", rechercheUrlQueryString);
        contexte.put("reinitialisation", reinitialisation);
        contexte.put("critereVipParDefaut", Arrays.asList("N"));
        mav.addObject("contexte", contexte);

        return mav;
    }

    private List<String> nettoieListeSirenNic(String liste) {
        List<String> sirets = new ArrayList<String>();
        for (String siret : new ArrayList<String>(Arrays.asList(liste.split(";")))) {
            if (StringUtils.isNotBlank(siret)) {
                sirets.add(siret);
            }
        }
        return sirets;
    }

    private boolean testeHabilitationVIP(UtilisateurIHM utilisateur, String numContrat, boolean isContratVIP) {
        boolean habiliteVIP = false;
        if (isContratVIP && utilisateur.isDroitVIP()) {
            if (utilisateur.getNumContratsVIP().isEmpty()
                    || !utilisateur.getNumContratsVIP().isEmpty() && utilisateur.getNumContratsVIP().contains(numContrat)) {
                habiliteVIP = true;
            }
        } else if (!isContratVIP && utilisateur.isDroitVIP()) {
            habiliteVIP = true;
        }

        return habiliteVIP;
    }

    private void remplissageDetails(ExtensionsContratsDetailsEnregistrement details, ModelAndView mav, boolean erreursDetectees, Contrat contrat,
            String numContrat) {
        // Par défaut
        mav.addObject("extensionContratExiste", true);

        // Nécessaire si contrat est nul
        details.setNumeroContrat(numContrat);

        ExtensionContrat extensionContrat = extensionContratRepository.getExtensionContrat(numContrat);

        // Recherche des valeurs par défaut de l'extension
        if (extensionContrat == null && !erreursDetectees) {
            mav.addObject("extensionContratExiste", false);

            // Contrôle type base assuj, si le contrat existe (F02_RG_2_01, F02_RG_2_08)
            if (contrat != null) {
                Integer modeCalculCotisation = tarifRepository.getModeCalculCotisation(numContrat);
                String controleTypeParDefaut = paramFamilleModeCalculContratRepository
                        .getModeControleTypesBasesAssujetties(contrat.getNumFamilleProduit(), contrat.getGroupeGestion(), modeCalculCotisation);
                details.setBaseAssuj(controleTypeParDefaut);
            }

            // Autres valeurs
            ParamValeurDefaut paramValeurDefaut = paramValeurDefautRepository.getParamValeurDefaut();

            details.setNbEtablissements(paramValeurDefaut.getSeuilVariationAlertesEnNbEtablissements().toString());
            details.setPcEtablissements(paramValeurDefaut.getSeuilVariationAlertesEnPourcentageNbEtablissements().toString());
            details.setNbSalaries(paramValeurDefaut.getSeuilVariationAlertesEnNbSalaries().toString());
            details.setPcSalaries(paramValeurDefaut.getSeuilVariationAlertesEnPourcentageNbSalaries().toString());
            details.setModeReaffectation(paramValeurDefaut.getModeReaffectationCategorieEffectifs());
            details.setIndicExploitationSaisi(paramValeurDefaut.getExploitationDSNAsText());
            details.setIndicConsignePaiementSaisi(paramValeurDefaut.getEditionConsignePaiementModeDSNAsText());
            details.setIndicTransfertSaisi(paramValeurDefaut.getTransfertDeclarationsVersQuatremAutoriseAsText());
            details.setVipSaisi(paramValeurDefaut.getContratVIPAsText());
            details.setGestionDirecte(paramValeurDefaut.getModeNatureContrat());
        } else if (extensionContrat != null && !erreursDetectees) {
            details.setDateDerniereModificationIndicateurs(
                    DateRedac.formate(DateRedac.MESSAGES_FORMAT_PAR_DEFAUT, extensionContrat.getDateDerniereModificationIndicateurs()));
            details.setGestionnaireDerniereModificationIndicateurs(extensionContrat.getGestionnaireDerniereModificationIndicateurs());
            details.setDateEffet(DateRedac.formate(DateRedac.MESSAGES_FORMAT_PAR_DEFAUT, extensionContrat.getDateEffetIndicateurs()));
            details.setCommentaireUtilisateur(extensionContrat.getMotifChangementIndicateurs());

            details.setIndicExploitationSaisi(extensionContrat.getExploitationDSNAsText());
            details.setIndicConsignePaiementSaisi(extensionContrat.getEditionConsignesPaiementAsText());
            details.setIndicTransfertSaisi(extensionContrat.getTransfertAsText());

            details.setNbEtablissements(extensionContrat.getSeuilVariationAlertesEnNbEtablissements().toString());
            details.setPcEtablissements(extensionContrat.getSeuilVariationAlertesEnPourcentageNbEtablissements().toString());
            details.setNbSalaries(extensionContrat.getSeuilVariationAlertesEnNbSalaries().toString());
            details.setPcSalaries(extensionContrat.getSeuilVariationAlertesEnPourcentageNbSalaries().toString());

            details.setVipSaisi(extensionContrat.getVipAsText());
            details.setBaseAssuj(extensionContrat.getControleTypeBaseAssujettie());
            details.setGestionDirecte(extensionContrat.getModeNatureContrat());
            details.setModeReaffectation(extensionContrat.getModeReaffectationCategorieEffectifs());
            details.setModeDeclaration(extensionContrat.getModeDeclaration());

        } else if (extensionContrat != null && erreursDetectees) {
            details.setDateDerniereModificationIndicateurs(
                    DateRedac.formate(DateRedac.MESSAGES_FORMAT_PAR_DEFAUT, extensionContrat.getDateDerniereModificationIndicateurs()));
            details.setGestionnaireDerniereModificationIndicateurs(extensionContrat.getGestionnaireDerniereModificationIndicateurs());
        }
    }

    /**
     * Gestion des redirections vers la page de détail de contrat par défaut.
     * 
     * @param request
     *            la requête HTTP de demande de la page
     * 
     * @return la commande de redirection vers la page de détail de contrat par défaut
     */
    @ResponseStatus(value = HttpStatus.MOVED_PERMANENTLY)
    @RequestMapping(value = "/{numContrat}/*")
    public String redirect(HttpServletRequest request) {
        return "redirect:/ihm/extensionscontrats/{numContrat}?" + request.getQueryString();
    }

    /**
     * Supprime les détails d'u contrat.
     * 
     * @param numContrat
     *            le numéro de contrat
     * @param httpRequest
     *            la requête de suppression
     * @return l'ULR de redirection post-traitement
     * @throws RessourceNonTrouveeException
     */
    @Secured(RoleRedac.ROLE_REDCDE)
    @RequestMapping(value = "/suppression/{numContrat}", method = RequestMethod.POST, produces = MediaType.TEXT_HTML_VALUE)
    @Transactional(propagation = Propagation.REQUIRES_NEW, rollbackFor = Throwable.class)
    public String supprimeExtensionContrat(@PathVariable("numContrat") String numContrat, HttpServletRequest httpRequest)
            throws RessourceNonTrouveeException {

        UtilisateurIHM utilisateur = getUtilisateurAuthentifie();

        // Récupération de l'extension contrat
        ExtensionContrat extensionContrat = extensionContratRepository.getExtensionContrat(numContrat);

        // Suppression
        extensionContratRepository.supprimeEntiteExistante(extensionContrat);

        // Traçage
        LOGGER.info("Suppression d'une extension de contrat par " + utilisateur.getIdentifiantActiveDirectory() + " : " + extensionContrat);

        // Récupération des extensions d'entreprises affiliées
        List<ExtensionEntrepriseAffiliee> listeEntreprisesAffiliees = extensionEntrepriseAffilieeRepository.getEntreprisesAffiliees(numContrat);

        // Suppression
        extensionEntrepriseAffilieeRepository.supprimeEntitesEnMasse(listeEntreprisesAffiliees);

        // Traçage
        for (ExtensionEntrepriseAffiliee entrepriseAffiliee : listeEntreprisesAffiliees) {
            LOGGER.info("Suppression d'une extension d'entreprise affiliée par " + utilisateur.getIdentifiantActiveDirectory() + " : "
                    + entrepriseAffiliee);
        }

        return "redirect:/ihm/extensionscontrats?" + httpRequest.getQueryString();
    }

    /**
     * Enregistre les détails d'un contrat.
     * 
     * @param valeurs
     *            les détails du contrat, sous forme de paramètres de requête POST
     * @param redirectAttributes
     *            les attributs de redirection à setter si jamais l'enregistrement est invalide et les détails renseignés doivent être conservés
     * @return l'URL de redirection post-traitement (conformément au pattern POST-REDIRECT-GET)
     * @throws RessourceNonTrouveeException
     */
    @Secured(RoleRedac.ROLE_REDCDE)
    @RequestMapping(value = "", method = RequestMethod.POST, produces = MediaType.TEXT_HTML_VALUE)
    @Transactional(propagation = Propagation.REQUIRES_NEW, rollbackFor = Throwable.class)
    public String enregistreContrat(@RequestParam MultiValueMap<String, Object> valeurs, final RedirectAttributes redirectAttributes)
            throws RessourceNonTrouveeException {

        Integer dateEffetInt = null;

        ExtensionsContratsDetailsEnregistrement details = ExtensionsContratsDetailsEnregistrement.from(valeurs);
        String numContrat = details.getNumeroContrat();
        LOGGER.info("Enregistrement du contrat : " + numContrat);

        boolean modeCreation = false;

        ExtensionContrat extensionContrat = extensionContratRepository.getExtensionContrat(numContrat);
        if (extensionContrat == null) {
            modeCreation = true;
            extensionContrat = new ExtensionContrat();
            extensionContrat.setNumContrat(numContrat);
        }

        if (!valideDetails(details)) {
            redirectAttributes.addFlashAttribute("details", details);
            return "redirect:/ihm/extensionscontrats/" + numContrat + "/?" + details.getRechercheUrlQueryString();
        }

        // Affectation des valeurs saisies
        Integer dateDuJour = DateRedac.convertitEnDateRedac(Calendar.getInstance().getTime());
        extensionContrat.setDateDerniereModificationIndicateurs(dateDuJour);

        UtilisateurIHM utilisateur = getUtilisateurAuthentifie();
        String user = utilisateur.getIdentifiantActiveDirectory();
        extensionContrat.setGestionnaireDerniereModificationIndicateurs(user);

        dateEffetInt = DateRedac.convertitDateFrancaiseEnDateRedac(details.getDateEffet());
        extensionContrat.setDateEffetIndicateurs(dateEffetInt);
        extensionContrat.setMotifChangementIndicateurs(details.getCommentaireUtilisateur());

        extensionContrat.setExploitationDSN(new ConvertisseurBooleanOuiNon().convertToEntityAttribute(details.getIndicExploitationSaisi()));
        extensionContrat
                .setEditionConsignesPaiement(new ConvertisseurBooleanOuiNon().convertToEntityAttribute(details.getIndicConsignePaiementSaisi()));
        extensionContrat.setTransfert(new ConvertisseurBooleanOuiNon().convertToEntityAttribute(details.getIndicTransfertSaisi()));

        extensionContrat.setSeuilVariationAlertesEnNbEtablissements(Integer.valueOf(details.getNbEtablissements()));
        extensionContrat.setSeuilVariationAlertesEnPourcentageNbEtablissements(Integer.valueOf(details.getPcEtablissements()));
        extensionContrat.setSeuilVariationAlertesEnNbSalaries(Integer.valueOf(details.getNbSalaries()));
        extensionContrat.setSeuilVariationAlertesEnPourcentageNbSalaries(Integer.valueOf(details.getPcSalaries()));

        extensionContrat.setControleTypeBaseAssujettie(details.getBaseAssuj());
        extensionContrat.setModeNatureContrat(details.getGestionDirecte());
        extensionContrat.setVip(new ConvertisseurBooleanON().convertToEntityAttribute(details.getVipSaisi()));
        extensionContrat.setModeReaffectationCategorieEffectifs(details.getModeReaffectation());

        if (StringUtils.isBlank(details.getModeDeclaration())) {
            extensionContrat.setModeDeclaration(null);
        } else {
            extensionContrat.setModeDeclaration(details.getModeDeclaration());
        }

        if (modeCreation) {
            extensionContrat.setAuditUtilisateurCreation(user);
            extensionContratRepository.create(extensionContrat);
        } else {
            extensionContrat.setAuditUtilisateurDerniereModification(user);
            extensionContratRepository.modifieEntiteExistante(extensionContrat);
        }

        // Mise à jour des entreprises affiliées
        List<String> listeSiretInitiale = extensionEntrepriseAffilieeRepository.getSiretEtablissementsAffiliesPourAffichage(numContrat);
        List<String> listeSiretSaisie = nettoieListeSirenNic(details.getListeSirenNic());

        // Copie de la liste finale.
        List<String> listeSiretAjoutes = new ArrayList<>(listeSiretSaisie);
        // Retrait des éléments initiaux. La liste contient alors les éléments ajoutés
        listeSiretAjoutes.removeAll(listeSiretInitiale);

        // Copie de la liste initiale.
        List<String> listeSiretSupprimes = new ArrayList<>(listeSiretInitiale);
        // Retrait des éléments finaux. La liste contient alors les éléments supprimés
        listeSiretSupprimes.removeAll(listeSiretSaisie);

        for (String siret : listeSiretSupprimes) {
            extensionEntrepriseAffilieeRepository.supprimeEntrepriseAffiliee(numContrat, StringUtils.substringBefore(siret, " "),
                    StringUtils.substringAfter(siret, " "));
        }
        for (String siret : listeSiretAjoutes) {
            extensionEntrepriseAffilieeRepository.create(creeEntrepriseAffiliee(siret, numContrat, user));
        }

        return "redirect:/ihm/extensionscontrats?" + details.getRechercheUrlQueryString();
    }

    /**
     * Valide le formulaire de saisie des détails du contrat.
     * 
     * @param details
     *            les valeurs du formulaire
     * @return true si le formulaire est correct, false sinon
     */
    private boolean valideDetails(ExtensionsContratsDetailsEnregistrement details) {
        boolean detailsValides = true;
        if (!valideChampsObligatoires(details)) {
            detailsValides = false;
        }
        if (!valideChampsMontants(details)) {
            detailsValides = false;
        }
        if (!valideChampsTaux(details)) {
            detailsValides = false;
        }
        if (!valideChampDateEffet(details)) {
            detailsValides = false;
        }
        LOGGER.info("Resultat validation formulaire : " + detailsValides);

        return detailsValides;
    }

    /**
     * Valide les champs obligatoires du formulaire, excepté la date d'effet (F02_RG_2_02).
     * 
     * @param details
     *            les valeurs du formulaire
     * @return true si les champs obligatoires (excepté la date d'effet) sont bien renseignés, false sinon
     */
    private boolean valideChampsObligatoires(ExtensionsContratsDetailsEnregistrement details) {
        String listeErreurs = StringUtils.defaultString(details.getListeErreurs());
        List<String> erreurs = new ArrayList<String>(Arrays.asList(StringUtils.split(listeErreurs, ";")));

        boolean champNonRenseigne = false;
        if (StringUtils.isBlank(details.getNbEtablissements())) {
            champNonRenseigne = true;
        }
        if (StringUtils.isBlank(details.getPcEtablissements())) {
            champNonRenseigne = true;
        }
        if (StringUtils.isBlank(details.getNbSalaries())) {
            champNonRenseigne = true;
        }
        if (StringUtils.isBlank(details.getPcSalaries())) {
            champNonRenseigne = true;
        }
        if (champNonRenseigne) {
            erreurs.add("Vous devez renseigner tous les champs obligatoires");
        }
        details.setListeErreurs(StringUtils.join(erreurs, ";"));
        return StringUtils.isBlank(details.getListeErreurs());
    }

    /**
     * Valide les champs de montants du formulaire (F02_RG_2_05).
     * 
     * @param details
     *            les valeurs du formulaire
     * @return true si les champs de montants sont corrects, false sinon
     */
    private boolean valideChampsMontants(ExtensionsContratsDetailsEnregistrement details) {
        List<String> erreurs = new ArrayList<String>(Arrays.asList(StringUtils.split(details.getListeErreurs(), ";")));
        boolean champMontantKO = false;
        if (!StringUtils.isNumeric(details.getNbEtablissements())) {
            champMontantKO = true;
        }
        if (!StringUtils.isNumeric(details.getNbSalaries())) {
            champMontantKO = true;
        }
        if (champMontantKO) {
            erreurs.add("Un nombre entier est attendu dans les champs seuils d'alerte en nombre");
        }
        details.setListeErreurs(StringUtils.join(erreurs, ";"));
        return StringUtils.isBlank(details.getListeErreurs());
    }

    /**
     * Valide les champs de taux du formulaire (F02_RG_2_06).
     * 
     * @param details
     *            les valeurs du formulaire
     * @return true si les champs de taux sont corrects, false sinon
     */
    private boolean valideChampsTaux(ExtensionsContratsDetailsEnregistrement details) {
        List<String> erreurs = new ArrayList<String>(Arrays.asList(StringUtils.split(details.getListeErreurs(), ";")));
        boolean champTauxKO = false;
        if (!StringUtils.isNumeric(details.getPcEtablissements())) {
            champTauxKO = true;
        }
        if (!StringUtils.isNumeric(details.getPcSalaries())) {
            champTauxKO = true;
        }
        if (champTauxKO) {
            erreurs.add("Un nombre entier est attendu dans les champs seuils d'alerte en pourcentage");
        }
        details.setListeErreurs(StringUtils.join(erreurs, ";"));
        return StringUtils.isBlank(details.getListeErreurs());
    }

    /**
     * Valide la date d'effet du formulaire (F02_RG_2_03).
     * 
     * @param details
     *            les valeurs du formulaire
     * @return true si la date d'effeat est correcte, false sinon
     */
    private boolean valideChampDateEffet(ExtensionsContratsDetailsEnregistrement details) {
        List<String> erreurs = new ArrayList<String>(Arrays.asList(StringUtils.split(details.getListeErreurs(), ";")));
        boolean champDateEffetKO = false;

        if (StringUtils.isBlank(details.getDateEffet())) {
            champDateEffetKO = true;
        } else {

            Contrat contrat = contratRepository.getDerniereSituationContrat(details.getNumeroContrat());
            String codePeriodicite = contrat != null ? contrat.getCodeFractionnement() : "";
            String periodeActuelleDateFrancaise = calculeDateDebutPeriodeActuelle(codePeriodicite);

            Integer dateDebutPeriodeActuelleDateRedac = DateRedac.convertitDateFrancaiseEnDateRedac(periodeActuelleDateFrancaise);

            boolean existeIndicateursDSNPeriodeActuelle = indicateursDSNPeriodeRepository
                    .existeIndicateursPourPeriodeActuelle(details.getNumeroContrat(), dateDebutPeriodeActuelleDateRedac);

            if (existeIndicateursDSNPeriodeActuelle) {
                if (!valideSiDateDebutPeriodeFuture(codePeriodicite, details.getDateEffet())) {
                    champDateEffetKO = true;
                }
            } else {
                if (!StringUtils.equals(details.getDateEffet(), periodeActuelleDateFrancaise)
                        && !valideSiDateDebutPeriodeFuture(codePeriodicite, details.getDateEffet())) {
                    champDateEffetKO = true;
                }
            }
        }

        if (champDateEffetKO) {
            erreurs.add("La date de début d'effet est obligatoire et doit être une date de début de période à venir");
        }
        details.setListeErreurs(StringUtils.join(erreurs, ";"));
        return StringUtils.isBlank(details.getListeErreurs());
    }

    /**
     * Calcule la date de début de la période acutelle au format JJMMAAA (F02_RG_2_07).
     * 
     * @param codePeriodicite
     *            périodicité du contrat
     * @return la date au format JJMMAAA
     */
    private String calculeDateDebutPeriodeActuelle(String codePeriodicite) {
        Integer dateActuelle = DateRedac.convertitEnDateRedac(Calendar.getInstance().getTime());
        Integer moisActuel = DateRedac.selectionnePartieDate(dateActuelle, DateRedac.MODIFIER_MOIS);
        Integer anneeActuelle = DateRedac.selectionnePartieDate(dateActuelle, DateRedac.MODIFIER_ANNEE);

        Integer moisDebutPeriode = null;
        String moisDebutPeriodeStr = "";

        switch (codePeriodicite) {
        case "M":
            moisDebutPeriode = moisActuel;
            break;
        case "S":
            moisDebutPeriode = (int) (Math.floor((double) (moisActuel - 1) / 6)) * 6 + 1;
            break;
        case "A":
            moisDebutPeriode = 1;
            break;
        default:
            moisDebutPeriode = (int) (Math.floor((double) (moisActuel - 1) / 3)) * 3 + 1;
        }

        moisDebutPeriodeStr = StringUtils.substring("0" + moisDebutPeriode.toString(), -2);

        return "01" + moisDebutPeriodeStr + anneeActuelle;
    }

    /**
     * Valide si la date d'effet saisie est une date de début de période future (F02_RG_2_07).
     * 
     */
    private boolean valideSiDateDebutPeriodeFuture(String codePeriodicite, String dateEffet) {
        boolean dateOK = true;
        Integer dateActuelle = DateRedac.convertitEnDateRedac(Calendar.getInstance().getTime());

        String jourDateEffet = StringUtils.substring(dateEffet, 0, 2);
        String moisDateEffet = StringUtils.substring(dateEffet, 2, 4);
        Integer dateEffetInt = DateRedac.convertitDateFrancaiseEnDateRedac(dateEffet);
        int moisDateEffetInt = Integer.parseInt(moisDateEffet);

        // Test du jour
        if (!StringUtils.equals(jourDateEffet, "01")) {
            dateOK = false;
            // Test de la date d'effet
        } else if (dateEffetInt <= dateActuelle) {
            dateOK = false;
            // Test du mois
        } else if (moisDateEffetInt < 1 || moisDateEffetInt > 12) {
            dateOK = false;
        } else {
            switch (codePeriodicite) {
            case "M":
                // Aucun autre contrôle à effectuer
                break;
            case "S":
                // Test du mois
                if (!StringUtils.equals(moisDateEffet, "01") && !StringUtils.equals(moisDateEffet, "07")) {
                    dateOK = false;
                }
                break;
            case "A":
                // Test du mois
                if (!StringUtils.equals(moisDateEffet, "01")) {
                    dateOK = false;
                }
                break;
            default:
                // Test du mois
                if (!valideMoisDateEffetTrimestrielle(moisDateEffet)) {
                    dateOK = false;
                }
                break;
            }
        }

        return dateOK;
    }

    /**
     * Valide la valeur du mois de la date d'effet saisie pour une périodicité trimestrielle.
     * 
     * @param moisDateEffet
     *            le mois à valider
     * @return true si le mois est valide, false sinon
     */
    private boolean valideMoisDateEffetTrimestrielle(String moisDateEffet) {
        boolean moisOK = false;
        if (StringUtils.equals(moisDateEffet, "01") || StringUtils.equals(moisDateEffet, "04")) {
            moisOK = true;
        }
        if (StringUtils.equals(moisDateEffet, "07") || StringUtils.equals(moisDateEffet, "10")) {
            moisOK = true;
        }
        return moisOK;
    }

    private ExtensionEntrepriseAffiliee creeEntrepriseAffiliee(String siret, String numContrat, String user) {
        ExtensionEntrepriseAffiliee entrepriseAffiliee = new ExtensionEntrepriseAffiliee();
        entrepriseAffiliee.setLigneEnCoursImportBatch(false);
        entrepriseAffiliee.setNumContrat(numContrat);
        entrepriseAffiliee.setSiren(StringUtils.substringBefore(siret, " "));
        entrepriseAffiliee.setNic(StringUtils.substringAfter(siret, " "));
        entrepriseAffiliee.setAuditUtilisateurCreation(user);
        return entrepriseAffiliee;
    }
}
