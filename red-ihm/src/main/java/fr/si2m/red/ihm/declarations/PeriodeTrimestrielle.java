package fr.si2m.red.ihm.declarations;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * Période trimestrielle.
 * 
 * @author nortaina
 *
 */
@Data
@AllArgsConstructor
public class PeriodeTrimestrielle implements Serializable {
    /**
     * UID de version.
     */
    private static final long serialVersionUID = -8654123619077477176L;

    /**
     * La date de début du trimestre.
     * 
     * @param dateDebutTrimestre
     *            la date de début du trimestre
     * @return la date de début du trimestre
     */
    private String dateDebutTrimestre;
    /**
     * Le libellé du trimestre.
     * 
     * @param libelleTrimestre
     *            le libellé du trimestre
     * @return le libellé du trimestre
     */
    private String libelleTrimestre;

}
