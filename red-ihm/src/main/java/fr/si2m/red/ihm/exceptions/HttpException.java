package fr.si2m.red.ihm.exceptions;

/**
 * Exception se traduisant par un code HTTP d'erreur.
 * 
 * @author nortaina
 *
 */
public class HttpException extends Exception {

    /**
     * UID de version.
     */
    private static final long serialVersionUID = -9164757098909714537L;

}
