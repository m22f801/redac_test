package fr.si2m.red.ihm;

import lombok.Getter;

/**
 * Pages de l'IHM.
 * 
 * @author nortaina
 *
 */
public enum Pages {
    /**
     * Page d'accueil.
     */
    ACCUEIL("Accueil REDAC-CDE", "/ihm/"),
    /**
     * Page de gestion de gestion des déclarations DSN.
     */
    DECLARATIONS("Déclaration DSN", "/ihm/declarations"),
    /**
     * Page de gestion de gestion des déclarations DSN.
     */
    PERIODE("Gestion des flux", "/ihm/declarations"),
    /**
     * Page de gestion de gestion des contrats.
     */
    EXTENSIONSCONTRATS("Gestion des contrats", "/ihm/extensionscontrats"),

    /**
     * Page de gestion de gestion des contrats.
     */
    DSNNONTRAITEES("Gestion des DSN non traitées", "/ihm/dsnnontraitees");

    /**
     * Le nom de la page.
     *
     * @return le nom de la page
     */
    @Getter
    private final String nom;
    /**
     * Le lien de la page.
     *
     * @return le lien de la page
     */
    @Getter
    private final String lien;

    private Pages(String nom, String lien) {
        this.nom = nom;
        this.lien = lien;
    }
}
