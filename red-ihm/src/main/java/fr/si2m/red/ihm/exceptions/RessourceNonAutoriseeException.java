package fr.si2m.red.ihm.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Exception levée pour l'accès à des ressources non autorisées.
 * 
 * @author nortaina
 *
 */
@ResponseStatus(value = HttpStatus.UNAUTHORIZED)
public final class RessourceNonAutoriseeException extends HttpException {
    /**
     * UID de version.
     */
    private static final long serialVersionUID = -5255221184025643013L;
}
