package fr.si2m.red.ihm;

import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import fr.si2m.red.DateRedac;
import fr.si2m.red.RedacUnexpectedException;
import fr.si2m.red.parametrage.ParamCodeLibelleRepository;
import lombok.Getter;
import lombok.Setter;

/**
 * Export Excel de contrats.
 * 
 * @author nortaina
 *
 */
public abstract class ExportExcel {
    protected static final Logger LOGGER = LoggerFactory.getLogger(ExportExcel.class);

    /**
     * Taille de la pagination par défaut utilisée pour requêter des entités à exporter.
     */
    public static final int TAILLE_PAGINATION_PAR_DEFAUT = 75000;

    /**
     * Le format des dates dans les exports.
     */
    public static final String FORMAT_DATE = "dd/MM/yyyy";

    /**
     * La police par défaut utilisée pour les exports Excel.
     */
    public static final String FONT_NOM = "Calibri";

    /**
     * La taille de police par défaut.
     */
    public static final short FONT_TAILLE_DEFAUT = 11;
    /**
     * La taille de police des titres.
     */
    public static final short FONT_TAILLE_TITRE = 18;
    /**
     * La taille de police des titres de second niveau.
     */
    public static final short FONT_TAILLE_SOUS_TITRE = 12;

    /**
     * Nombre maximal de caractères contenus dans une cellule Excel
     */
    public static final int MAX_CHAR_CELL = 32000;

    /**
     * La taille d'une page pour la récupération des données de l'export.
     * 
     * @param pageTaille
     *            la taille d'une page pour la récupération des données de l'export
     * @return la taille d'une page pour la récupération des données de l'export
     */
    @Getter
    @Setter
    private int pageTaille = TAILLE_PAGINATION_PAR_DEFAUT;

    /**
     * Le format des dates pour l'export.
     * 
     * @param formatDate
     *            le format des dates pour l'export
     * @return le format des dates pour l'export
     */
    @Getter
    @Setter
    private String formatDate = DateRedac.MESSAGES_FORMAT_PAR_DEFAUT;

    @Autowired
    private ParamCodeLibelleRepository paramCodeLibelleRepository;

    /**
     * Exporte le contenu Excel dans le flux sortant donné.
     * 
     * @param fluxSortant
     *            le flux sortant
     */
    protected void exporte(OutputStream fluxSortant) {
        SXSSFWorkbook workbook = new SXSSFWorkbook(pageTaille);

        creeContenuClasseur(workbook);

        try {
            workbook.write(fluxSortant);
        } catch (IOException e) {
            throw new RedacUnexpectedException("Erreur lors d'un export Excel", e);
        } finally {
            workbook.dispose();
            IOUtils.closeQuietly(workbook);
        }
    }

    /**
     * Exporte le contenu Excel dans le flux sortant donné.
     * 
     * @param fluxSortant
     *            le flux sortant
     */
    protected void exporteMontant(OutputStream fluxSortant) {
        SXSSFWorkbook workbook = new SXSSFWorkbook(pageTaille);

        creeContenuClasseurMontant(workbook);

        try {
            workbook.write(fluxSortant);
        } catch (IOException e) {
            throw new RedacUnexpectedException("Erreur lors d'un export Excel", e);
        } finally {
            workbook.dispose();
            IOUtils.closeQuietly(workbook);
        }
    }

    /**
     * Exporte le contenu Excel dans le flux sortant donné.
     * 
     * @param fluxSortant
     *            le flux sortant
     */
    protected void exporteMessage(OutputStream fluxSortant) {
        SXSSFWorkbook workbook = new SXSSFWorkbook(pageTaille);

        creeContenuClasseurMessage(workbook);

        try {
            workbook.write(fluxSortant);
        } catch (IOException e) {
            throw new RedacUnexpectedException("Erreur lors d'un export Excel", e);
        } finally {
            workbook.dispose();
            IOUtils.closeQuietly(workbook);
        }
    }

    /**
     * Crée le contenu du classeur Excel.
     * 
     * @param workbook
     *            le classeur à remplir
     */
    protected abstract void creeContenuClasseur(Workbook workbook);

    /**
     * Crée le contenu du classeur Excel.
     * 
     * @param workbook
     *            le classeur à remplir
     */
    protected abstract void creeContenuClasseurMontant(Workbook workbook);

    /**
     * Crée le contenu du classeur Excel.
     * 
     * @param workbook
     *            le classeur à remplir
     */
    protected abstract void creeContenuClasseurMessage(Workbook workbook);

    /**
     * Crée le style de cellule par défaut (mode gras ou non).
     * 
     * @param workbook
     *            le classeur
     * @param gras
     *            si le style doit inclure une police en gras
     * @return le style créé
     */
    protected CellStyle creeStyleCelluleParDefaut(Workbook workbook, boolean gras) {
        Font font = workbook.createFont();
        font.setFontHeightInPoints(FONT_TAILLE_DEFAUT);
        font.setFontName(FONT_NOM);
        font.setBold(gras);

        CellStyle style = workbook.createCellStyle();
        style.setFont(font);
        return style;
    }

    /**
     * Crée le style de cellule d'un sous-titre (mode gras ou non).
     * 
     * @param workbook
     *            le classeur
     * @param gras
     *            si le style doit inclure une police en gras
     * @return le style créé
     */
    protected CellStyle creeStyleCelluleSousTitre(Workbook workbook, boolean gras) {
        Font font = workbook.createFont();
        font.setFontHeightInPoints(FONT_TAILLE_SOUS_TITRE);
        font.setFontName(FONT_NOM);
        font.setBold(gras);

        CellStyle style = workbook.createCellStyle();
        style.setFont(font);
        return style;
    }

    /**
     * Crée une cellule pour un libellé dans un formulaire.
     * 
     * @param ligneFormulaire
     *            la ligne du formulaire à remplir
     * @param positionLibelle
     *            la position du libellé dans la ligne
     * @param styleLibelle
     *            le style du libellé
     * @param texteLibelle
     *            le texte du libellé
     */
    protected void creeCelluleFormulaireLibelle(Row ligneFormulaire, int positionLibelle, CellStyle styleLibelle, String texteLibelle) {
        Cell libelle = ligneFormulaire.createCell(positionLibelle);
        libelle.setCellStyle(styleLibelle);
        libelle.setCellValue(texteLibelle);
    }

    /**
     * Crée une cellule pour une valeur dans un formulaire.
     * 
     * @param ligneFormulaire
     *            la ligne du formulaire à remplir
     * @param positionValeur
     *            la position de la valeur dans la ligne
     * @param styleValeur
     *            le style du libellé
     * @param texteValeur
     *            la valeur du critère
     */
    protected void creeCelluleFormulaireValeur(Row ligneFormulaire, int positionValeur, CellStyle styleValeur, String texteValeur) {
        Cell valeur = ligneFormulaire.createCell(positionValeur);
        valeur.setCellStyle(styleValeur);
        valeur.setCellValue(StringUtils.defaultString(texteValeur));
    }

    /**
     * Récupère le libellé d'édition pour un code donné.
     * 
     * @param table
     *            la table ciblée
     * @param champ
     *            le champ ciblé
     * @param code
     *            la valeur de champ ciblé
     * @return le libellé pour l'édition
     */
    protected String getLibelleEdition(String table, String champ, String code) {
        String libelleEdition = paramCodeLibelleRepository.getLibelleEdition(table, champ, code);
        if (libelleEdition == null) {
            LOGGER.debug("Libellé introuvable pour {}-{}-{}", table, champ, code);
            return "LIBELLE INTROUVABLE";
        } else {
            return libelleEdition;
        }
    }

    /**
     * Récupère les libellés d'édition pour une liste de codes donnés.
     * 
     * @param codes
     *            les codes
     * @param table
     *            la table pour les libellés
     * @param champ
     *            le champ pour les libellés
     * @return les libellés pour les codes
     */
    protected List<String> getLibellesEdition(List<?> codes, String table, String champ) {
        List<String> libelles = new ArrayList<>(codes.size());
        for (Object code : codes) {
            libelles.add(getLibelleEdition(table, champ, code.toString()));
        }
        return libelles;
    }

    /**
     * Crée une cellule sur une ligne donnée.
     * 
     * @param ligne
     *            la ligne
     * @param position
     *            la position de la cellule
     * @param styleCellule
     *            le style de la cellule
     * @param valeur
     *            le contenu de la cellule
     */
    protected void creeCellule(Row ligne, int position, CellStyle styleCellule, String valeur) {
        Cell cell = ligne.createCell(position);
        cell.setCellStyle(styleCellule);
        cell.setCellValue(valeur);
    }

    /**
     * Crée une cellule sur une ligne donnée pour un contenu numérique décimale (montants).
     * 
     * @param ligne
     *            la ligne
     * @param position
     *            la position de la cellule
     * @param styleCellule
     *            le style de la cellule
     * @param valeur
     *            le contenu de la cellule
     */
    protected void creeCellule(Row ligne, int position, CellStyle styleCellule, Double valeurMontant) {
        Cell cell = ligne.createCell(position);
        cell.setCellStyle(styleCellule);

        // Pour que les sommes sur ces champs fonctionnent, le champ doit être
        // de type double pour être proprement set.

        // Math.round(valeurMontant * 100 ) / 100
        // arrondi les nombres aux centièmes, mais comme les montants ont tous 2
        // décimales, il n'y aura pas d'arrondi effectué.
        if (valeurMontant != null && Double.compare(valeurMontant.doubleValue(), 0d) != 0) {
            cell.setCellValue((double) Math.round(valeurMontant * 100) / 100);
        }

    }

    /**
     * Crée une cellule sur une ligne donnée pour un contenu numérique décimale (montants). Gère les valeurs à 0 pour afficher 0 dans la cellule ( et pas une
     * cellule vide )
     * 
     * @param ligne
     *            la ligne
     * @param position
     *            la position de la cellule
     * @param styleCellule
     *            le style de la cellule
     * @param valeur
     *            le contenu de la cellule
     * @param isInt
     *            le valeur est zéro
     */
    protected void creeCellule(Row ligne, int position, CellStyle styleCellule, Double valeurMontant, boolean isZero) {
        Cell cell = ligne.createCell(position);
        cell.setCellStyle(styleCellule);

        // Pour que les sommes sur ces champs fonctionnent, le champ doit être
        // de type double pour être proprement set.

        // Math.round(valeurMontant * 100 ) / 100
        // arrondi les nombres aux centièmes, mais comme les montants ont tous 2
        // décimales, il n'y aura pas d'arrondi effectué.
        if (valeurMontant != null && Double.compare(valeurMontant.doubleValue(), 0d) != 0) {
            cell.setCellValue((double) Math.round(valeurMontant * 100) / 100);
        }

        if (isZero && (valeurMontant == null || Double.compare(valeurMontant.doubleValue(), 0d) == 0)) {
            cell.setCellType(Cell.CELL_TYPE_NUMERIC);
            cell.setCellValue(0);
        }

    }

    /**
     * Crée une cellule sur une ligne donnée.
     * 
     * @param ligne
     *            la ligne
     * @param position
     *            la position de la cellule
     * @param styleCellule
     *            le style de la cellule
     * @param valeurs
     *            le contenu de la cellule
     */
    protected void creeCellule(Row ligne, int position, CellStyle styleCellule, List<String> valeurs) {
        Cell cell = ligne.createCell(position);
        cell.setCellStyle(styleCellule);
        cell.setCellValue(StringUtils.join(valeurs, ", "));
    }

    /**
     * Enrichit le style d'une cellule avec l'activation de toutes les bordures.
     * 
     * @param styleCellule
     *            le style de la cellule à enrichir
     */
    protected void enrichitStyleCelluleAvecToutesBordures(CellStyle styleCellule) {
        styleCellule.setBorderTop(CellStyle.BORDER_THIN);
        styleCellule.setBorderRight(CellStyle.BORDER_THIN);
        styleCellule.setBorderBottom(CellStyle.BORDER_THIN);
        styleCellule.setBorderLeft(CellStyle.BORDER_THIN);
        styleCellule.setTopBorderColor(IndexedColors.BLACK.getIndex());
        styleCellule.setRightBorderColor(IndexedColors.BLACK.getIndex());
        styleCellule.setBottomBorderColor(IndexedColors.BLACK.getIndex());
        styleCellule.setLeftBorderColor(IndexedColors.BLACK.getIndex());
    }

    /**
     * Safe toString à appliquer à n'importe quel élément.
     * 
     * @param element
     *            l'élément à convertir en chaîne de caractères
     * @return la chaîne de caractère de l'élément
     */
    protected String stringify(Object element) {
        return element != null ? element.toString() : StringUtils.EMPTY;
    }

    /**
     * Redimensionnement des colonnes, dépendant de la taille du tableau de dimension, 0-based.
     * 
     * @param extraction
     *            la feuille à redimensionner
     * @param widthArray
     *            les largeurs des colonnes en caractères à afficher
     */
    protected void redimensionnementColonnes(Sheet extraction, int[] widthArray) {
        for (int i = 0; i < widthArray.length; i++) {
            // on ajoute 2 au nombre de caractères voulu pour éviter que le dernier caractère soit tronqué
            extraction.setColumnWidth(i, (widthArray[i] + 2) * 256);
        }
    }
}
