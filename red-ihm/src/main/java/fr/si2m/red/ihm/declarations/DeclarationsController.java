package fr.si2m.red.ihm.declarations;

import java.io.IOException;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.ModelAndView;

import fr.si2m.red.DateRedac;
import fr.si2m.red.RedacUnexpectedException;
import fr.si2m.red.contrat.Contrat;
import fr.si2m.red.contrat.ContratRepository;
import fr.si2m.red.dsn.EtatPeriode;
import fr.si2m.red.ihm.FilAriane;
import fr.si2m.red.ihm.Pages;
import fr.si2m.red.ihm.RedacIhmController;
import fr.si2m.red.ihm.RoleRedac;
import fr.si2m.red.ihm.exceptions.RessourceNonTrouveeException;
import fr.si2m.red.ihm.session.UtilisateurIHM;
import fr.si2m.red.parametrage.ParamCodeLibelle;
import fr.si2m.red.parametrage.ParamCodeLibelleRepository;
import fr.si2m.red.parametrage.ParamControleSignalRejet;
import fr.si2m.red.parametrage.ParamControleSignalRejetRepository;
import fr.si2m.red.parametrage.ParamUtilisateurGestionnaireRepository;
import fr.si2m.red.reconciliation.CompteRenduIntegrationRepository;
import fr.si2m.red.reconciliation.HistoriqueAssignationPeriodeRepository;
import fr.si2m.red.reconciliation.HistoriqueAttenteRetourEtpPeriode;
import fr.si2m.red.reconciliation.HistoriqueAttenteRetourEtpPeriodeRepository;
import fr.si2m.red.reconciliation.HistoriqueCommentairePeriode;
import fr.si2m.red.reconciliation.HistoriqueCommentairePeriodeRepository;
import fr.si2m.red.reconciliation.HistoriqueEtatPeriode;
import fr.si2m.red.reconciliation.HistoriqueEtatPeriodeRepository;
import fr.si2m.red.reconciliation.MessageControle;
import fr.si2m.red.reconciliation.MessageControleRepository;
import fr.si2m.red.reconciliation.PeriodeRecue;
import fr.si2m.red.reconciliation.PeriodeRecueRepository;
import fr.si2m.red.reconciliation.PeriodesRecuesCriteresFusion;
import fr.si2m.red.reconciliation.PeriodesRecuesCriteresRecherche;
import fr.si2m.red.reconciliation.PopupChangementEnMasse;
import fr.si2m.red.reconciliation.RattachementDeclarationsRecuesRepository;
import fr.si2m.red.reconciliation.TypeActionPopupChangementEnMasse;

/**
 * Contrôleur des pages de gestion des DSN.
 * 
 * @author nortaina
 * 
 */
@Controller
@RequestMapping("/declarations")
public class DeclarationsController extends RedacIhmController {

    private static final Logger LOGGER = LoggerFactory.getLogger(DeclarationsController.class);

    private static final String CODE_VIP_FALSE = "N";

    private static final String IDENTIFIANT_CONTROLE_RED_FUSION = "RED_FUSION";
    private static final String TBL_PERIODE = "PERIODERECUE";
    private static final String REDIRECT_DECLARATION = "redirect:/ihm/declarations";

    private static final int TAILLE_PAGE = 1000;

    @Autowired
    private PeriodeRecueRepository periodeRecueRepository;
    @Autowired
    private HistoriqueEtatPeriodeRepository historiqueEtatPeriodeRepository;
    @Autowired
    private HistoriqueCommentairePeriodeRepository historiqueCommentairePeriodeRepository;
    @Autowired
    private HistoriqueAssignationPeriodeRepository historiqueAssignationPeriodeRepository;
    @Autowired
    private HistoriqueAttenteRetourEtpPeriodeRepository historiqueAttenteRetourEtpPeriodeRepository;
    @Autowired
    private RattachementDeclarationsRecuesRepository rattachementDeclarationsRecuesRepository;
    @Autowired
    private CompteRenduIntegrationRepository compteRenduIntegrationRepository;
    @Autowired
    private MessageControleRepository messageControleRepository;
    @Autowired
    private DeclarationsExportExcel declarationsExportExcel;
    @Autowired
    private ParamCodeLibelleRepository paramCodeLibelleRepository;
    @Autowired
    private ParamControleSignalRejetRepository paramControleSignalRejetRepository;
    @Autowired
    private ContratRepository contratRepository;
    @Autowired
    private ParamUtilisateurGestionnaireRepository paramUtilisateurGestionnaireRepository;

    /**
     * Récupère la page de recherche des DSN.
     * 
     * @param criteres
     *            les critères de recherche
     * @param httpRequest
     *            la requête HTTP de demande de la page
     * 
     * @return le modèle et la vue de la page
     * 
     */
    @Secured(RoleRedac.ROLE_REDCONSULT)
    @RequestMapping(value = "", method = RequestMethod.GET, produces = MediaType.TEXT_HTML_VALUE)
    @Transactional(propagation = Propagation.REQUIRES_NEW, rollbackFor = Throwable.class)
    public ModelAndView getRechercheDeclarations(@RequestParam MultiValueMap<String, Object> criteres, HttpServletRequest httpRequest) {

        // Récupération des critères de recherche
        PeriodesRecuesCriteresRecherche criteresRecherche = PeriodesRecuesCriteresRecherche.from(criteres);
        UtilisateurIHM utilisateur = getUtilisateurAuthentifie();
        if (utilisateur.isDroitVIP()) {
            criteresRecherche.getFiltreContratsVIP().addAll(utilisateur.getNumContratsVIP());
        }

        // Préparation des critères pour le bloc de synthèse
        if (criteres.isEmpty()) {
            initialisationGroupesGestion(utilisateur, criteresRecherche);
        }

        String libelleGroupeGestionUnique = criteresRecherche.getGroupesGestionAsList().size() == 1 ? paramCodeLibelleRepository
                .getLibelleCourt(ParamCodeLibelle.TABLE_CONTRAT, "NMGRPGES", criteresRecherche.getGroupesGestionAsList().get(0)) : null;

        List<String> listeFamilles = criteresRecherche.getFamillesAsList();
        String libelleFamilleUnique = listeFamilles.size() == 1
                ? paramCodeLibelleRepository.getLibelleCourt(ParamCodeLibelle.TABLE_CONTRAT, "NOFAM", listeFamilles.get(0)) : null;

        List<Integer> listeFamillesAsIntegers = new ArrayList<>(listeFamilles.size());
        for (String famille : listeFamilles) {
            listeFamillesAsIntegers.add(Integer.valueOf(famille));
        }

        // Initialisation du MAV
        ModelAndView mav = initialiseModeleVue("declarations/DeclarationsRecherche", httpRequest, criteresRecherche.getGroupesGestionAsList(),
                libelleGroupeGestionUnique, listeFamillesAsIntegers, libelleFamilleUnique);

        // Info fil d'Ariane
        FilAriane filAriane = new FilAriane();
        filAriane.setFil(Arrays.asList(Pages.ACCUEIL, Pages.DECLARATIONS));
        mav.addObject("filAriane", filAriane);

        // Infos résultats de recherche
        long total = 0;

        // On effectue une recherche avec les valeurs par defaut du bloc de
        // recherche lors de l'initialisation/reinitialisation
        // = on effectue une recherche quoi qu'il arrive
        if (criteres.isEmpty()) {
            // filtre grpe gestion déjà set
            criteresRecherche.setVip(new String[] { CODE_VIP_FALSE });
            criteresRecherche.setStatuts(new String[] { EtatPeriode.RCP.toString(), EtatPeriode.INS.toString(), EtatPeriode.NIN.toString() });
            criteresRecherche.setTriChamp("spec");
        }

        mav.addObject("resultats", periodeRecueRepository.recherchePeriodesRecues(criteresRecherche));
        total = periodeRecueRepository.compte(criteresRecherche);
        if (total == 0L) {
            mav.addObject("informations", "Aucune période pour cette sélection");
        }
        // nombre de périodes impactées avec séparatuer "espace" de milliers
        mav.addObject("nbPeriodesFiltrees", NumberFormat.getInstance(Locale.FRENCH).format(total).toString());

        mav.addObject("resultatsMeta", creeResultatsInfoPagination(total, criteresRecherche));

        // Données de référence pour la recherche
        Map<String, Object> reference = new HashMap<String, Object>();
        reference.put("listeStatuts", getEtatsPeriodesSelectionnables());
        reference.put("listeGroupesGestion", getGroupesGestionsSelectionnables());
        reference.put("listeFamilles", getFamillesSelectionnables());
        reference.put("listeMessagesControle", getControlesPourLibMessagesSelectionnables());
        reference.put("listePeriodes", getPeriodesTrimestriellesSelectionnables());
        reference.put("listeNatures", getNaturesPeriodesSelectionnables());
        reference.put("listeVIP", getVipSelectionnables());
        reference.put("listeUsersAffecteA", paramUtilisateurGestionnaireRepository
                .getListeNomsEtCodeUsers(PeriodesRecuesCriteresRecherche.NON_AFFECTE_LIBELLE, PeriodesRecuesCriteresRecherche.NON_AFFECTE_CODE));
        reference.put("listeUsersATraiterPar", paramUtilisateurGestionnaireRepository
                .getListeNomsEtCodeUsers(PeriodesRecuesCriteresRecherche.NON_ASSIGNE_LIBELLE, PeriodesRecuesCriteresRecherche.NON_ASSIGNE_CODE));
        reference.put("listeUsersTraitePar", paramUtilisateurGestionnaireRepository
                .getListeNomsEtCodeUsers(PeriodesRecuesCriteresRecherche.TRAITE_AUTO_LIBELLE, PeriodesRecuesCriteresRecherche.TRAITE_AUTO_CODE));
        reference.put("droitFusionerPeriode", (Boolean) getInfoUtilisateur(httpRequest).get(RoleRedac.ID_ROLE_GESTION_REDAC));
        mav.addObject("reference", reference);

        // Construction du contexte de la page
        Map<String, Object> contexte = new HashMap<String, Object>();
        contexte.put("criteres", criteresRecherche);
        contexte.put("reinitialisation", criteres.isEmpty());
        contexte.put("critereVipParDefaut", Arrays.asList(CODE_VIP_FALSE));
        contexte.put("critereStatutsParDefaut", Arrays.asList(EtatPeriode.RCP.toString(), EtatPeriode.INS.toString(), EtatPeriode.NIN.toString()));
        String criteresRechercheQueryString = criteresRecherche.toQueryString();
        contexte.put("urlQueryString", criteresRechercheQueryString);
        contexte.put("contratCriteresRecherche", StringUtils.replace(criteresRechercheQueryString, "&", ";"));
        contexte.put("droitGroupe2", (Boolean) getInfoUtilisateur(httpRequest).get(RoleRedac.ID_ROLE_GESTION_CDE));
        contexte.put("droitGroupe3", (Boolean) getInfoUtilisateur(httpRequest).get(RoleRedac.ID_ROLE_GESTION_REDAC));

        mav.addObject("contexte", contexte);

        // Construction des listes de la popup (sans autre gestionnaire)
        Map<String, Object> listesPopup = new HashMap<String, Object>();

        listesPopup.put("listeUsersAffecteA", paramUtilisateurGestionnaireRepository.getlistesAffecteAEtAtraiterParSansAutreGest(
                PeriodesRecuesCriteresRecherche.NON_AFFECTE_LIBELLE, PeriodesRecuesCriteresRecherche.NON_AFFECTE_CODE));
        listesPopup.put("listeUsersATraiterPar", paramUtilisateurGestionnaireRepository.getlistesAffecteAEtAtraiterParSansAutreGest(
                PeriodesRecuesCriteresRecherche.NON_ASSIGNE_LIBELLE, PeriodesRecuesCriteresRecherche.NON_ASSIGNE_CODE));

        // Construction de la liste de changement de statut en masse
        List<ParamCodeLibelle> listeStatutsInitiaux = paramCodeLibelleRepository.get(ParamCodeLibelle.TABLE_PERIODE_RECUE, "ETAT");
        HashMap<String, ParamCodeLibelle> mapStatuts = new HashMap<>(listeStatutsInitiaux.size());

        for (ParamCodeLibelle paramCodeLibelle : listeStatutsInitiaux) {
            mapStatuts.put(paramCodeLibelle.getCode(), paramCodeLibelle);
        }

        List<ParamCodeLibelle> statutsSelectionnables = Arrays.asList(mapStatuts.get(EtatPeriode.ARI.name()), mapStatuts.get(EtatPeriode.NIN.name()),
                mapStatuts.get(EtatPeriode.ING.name()), mapStatuts.get(EtatPeriode.INS.name()), mapStatuts.get(EtatPeriode.SSG.name()),
                mapStatuts.get(EtatPeriode.TPG.name()));

        // on conserve que les statuts selectionnables
        listeStatutsInitiaux.retainAll(statutsSelectionnables);

        ParamCodeLibelle nonRenseigneUser = new ParamCodeLibelle();
        listeStatutsInitiaux.add(0, nonRenseigneUser);
        listesPopup.put("listeStatuts", listeStatutsInitiaux);

        mav.addObject("listesPopup", listesPopup);

        return mav;
    }

    /**
     * 
     * Détermine la valeur par defaut du GroupesGestion - F09_RG_1_03 à l'initialisation/réinitialisation.
     * 
     * @param utilisateur
     *            l'utilisateur associé à la session
     * @param criteresRecherche
     *            les critères de la recherche
     */
    private void initialisationGroupesGestion(UtilisateurIHM utilisateur, PeriodesRecuesCriteresRecherche criteresRecherche) {
        String groupeGestUser = utilisateur.getGroupeGestion();
        if (StringUtils.isNotBlank(groupeGestUser)) {
            for (ParamCodeLibelle groupegestion : getGroupesGestionsSelectionnables()) {
                if (StringUtils.equals(groupeGestUser, groupegestion.getCode())) {
                    criteresRecherche.setGroupesGestion(new String[] { groupeGestUser });
                }
            }
        }
    }

    /**
     * Récupère la liste des états de période connus de l'application.
     * 
     * @return la liste des états de période connus de l'application
     */
    private List<ParamCodeLibelle> getEtatsPeriodesSelectionnables() {
        return paramCodeLibelleRepository.get(TBL_PERIODE, "ETAT");
    }

    /**
     * Récupère la liste des états de période connus de l'application.
     * 
     * @return la liste des états de période connus de l'application
     */
    private List<PeriodeTrimestrielle> getPeriodesTrimestriellesSelectionnables() {
        List<PeriodeTrimestrielle> periodes = new ArrayList<PeriodeTrimestrielle>(9);
        Calendar cal = Calendar.getInstance();
        int moisActuel = cal.get(Calendar.MONTH);
        int moisDebutTrimestreEnCours = moisActuel % 3 == 0 ? moisActuel : moisActuel - (moisActuel % 3);
        cal.set(Calendar.MONTH, moisDebutTrimestreEnCours);
        cal.set(Calendar.DAY_OF_MONTH, 1);
        Integer dateDebutTrimestreEnCours = DateRedac.convertitEnDateRedac(cal.getTime());
        PeriodeTrimestrielle enCours = new PeriodeTrimestrielle(String.valueOf(dateDebutTrimestreEnCours), "En cours");
        periodes.add(enCours);

        Map<Integer, Integer> numerosTrimestres = new HashMap<>();
        numerosTrimestres.put(0, 1);
        numerosTrimestres.put(3, 2);
        numerosTrimestres.put(6, 3);
        numerosTrimestres.put(9, 4);

        // Ajout des 8 trimestres précédant celui en cours
        for (int i = 0; i < 8; i++) {
            cal.add(Calendar.MONTH, -3);
            Integer dateDebutTrimestre = DateRedac.convertitEnDateRedac(cal.getTime());
            String libelleTrimestre = String.valueOf(cal.get(Calendar.YEAR)) + "T" + numerosTrimestres.get(cal.get(Calendar.MONTH));
            PeriodeTrimestrielle periode = new PeriodeTrimestrielle(String.valueOf(dateDebutTrimestre), libelleTrimestre);
            periodes.add(periode);
        }

        return periodes;
    }

    /**
     * Récupère la liste des controles ( par identifiantControle Distinct ) present dans ParamControlSignalRejet.
     * 
     * @return la liste des controles
     */
    private List<ParamControleSignalRejet> getControlesPourLibMessagesSelectionnables() {
        return paramControleSignalRejetRepository.getControlesOrdoneesDistinct();
    }

    /**
     * Récupère l'export Excel des contrats.
     * 
     * @param criteres
     *            les critères de la recherche des contrats
     * @param httpResponse
     *            la réponse à la requête de récupération
     * 
     * @throws IOException
     *             levée si une erreur d'écriture de flux est détectée
     */
    @Secured(RoleRedac.ROLE_REDCONSULT)
    @RequestMapping(value = "/exports/excel", method = RequestMethod.GET)
    @Transactional(propagation = Propagation.REQUIRES_NEW, rollbackFor = Throwable.class)
    @ResponseStatus(HttpStatus.OK)
    public void getExport(@RequestParam MultiValueMap<String, Object> criteres, HttpServletResponse httpResponse) throws IOException {
        // Récupération des critères de recherche
        PeriodesRecuesCriteresRecherche criteresRecherche = PeriodesRecuesCriteresRecherche.from(criteres);
        UtilisateurIHM utilisateur = getUtilisateurAuthentifie();
        if (utilisateur.isDroitVIP()) {
            criteresRecherche.getFiltreContratsVIP().addAll(utilisateur.getNumContratsVIP());
        } else {
            criteresRecherche.setVip(new String[] { CODE_VIP_FALSE });
        }
        try {
            httpResponse.setContentType("application/vnd.ms-excel");
            httpResponse.setHeader("Content-Disposition", "attachment; filename=Periodes.xlsx");
            declarationsExportExcel.exporte(criteresRecherche, httpResponse.getOutputStream());
            httpResponse.flushBuffer();
        } catch (Exception e) {
            httpResponse.reset();
            httpResponse.setHeader("Content-Type", "text/plain; charset=utf-8");
            throw e;
        }
    }

    /**
     * Fusion des périodes sélectionnées , puis redirection vers l'écran de recherche de déclarations
     * 
     * @return la commande de redirection vers la page de recherche de déclarations
     */
    @Secured(RoleRedac.ROLE_REDREDAC)
    @RequestMapping(value = "/fusion", method = RequestMethod.POST, produces = MediaType.TEXT_HTML_VALUE)
    @Transactional(propagation = Propagation.REQUIRES_NEW, rollbackFor = Throwable.class)
    @ResponseStatus(HttpStatus.SEE_OTHER)
    public String fusionPeriodes(@RequestParam MultiValueMap<String, Object> criteres) {

        // Récupération des critères de recherche
        PeriodesRecuesCriteresFusion criteresFusion = PeriodesRecuesCriteresFusion.from(criteres);

        // utilisateur effectuant la fusion
        UtilisateurIHM utilisateur = getUtilisateurAuthentifie();

        if (criteresFusion.getIdsPeriodesFusionAsLong().size() > 1) {
            // récupération des périodes candidates

            List<PeriodeRecue> periodesCandidates = periodeRecueRepository.getPeriodes(criteresFusion.getIdsPeriodesFusionAsLong());

            // Etape 1 : F09_RG_1_17
            PeriodeRecue periodeCible = identificationPeriodeCible(periodesCandidates, criteresFusion.getIdsPeriodesFusionAsLong());

            // on retire la periode cible de la liste, pour ne conserver que les periodes "autres"
            periodesCandidates.remove(periodeCible);

            criteresFusion.getIdsPeriodesFusionAsLong().remove(periodeCible.getIdPeriode());

            // Etape 2 : F09_RG_1_18
            rattachementDeclarationsRecuesRepository.rattachementVersPeriodeCible(periodeCible.getIdPeriode(),
                    criteresFusion.getIdsPeriodesFusionAsLong(), utilisateur.getIdentifiantActiveDirectory());

            // on récupère les historiques des commentaires et des retours ETP
            List<Long> idPeriodesCandidates = new ArrayList<Long>();
            for (PeriodeRecue periode : periodesCandidates) {
                idPeriodesCandidates.add(periode.getId());
            }

            List<HistoriqueCommentairePeriode> commentairesPeriodeCandidates = historiqueCommentairePeriodeRepository
                    .getHistoriqueCommentairesPeriodes(idPeriodesCandidates);
            List<HistoriqueAttenteRetourEtpPeriode> attenteRetourEtpPeriodeCandidates = historiqueAttenteRetourEtpPeriodeRepository
                    .getHistoriqueAttenteRetourEtpPeriodes(idPeriodesCandidates);

            // Etape 3 : F09_RG_1_19
            purgeElementDeclaratifEtPeriodeAutre(criteresFusion.getIdsPeriodesFusionAsLong());

            // Etape 4 : F09_RG_1_20
            periodeCible.setReconsolider(true);
            periodeCible.setEtatPeriode(EtatPeriode.RCP);
            periodeCible.setAuditUtilisateurDerniereModification(utilisateur.getIdentifiantActiveDirectory());
            periodeRecueRepository.modifieEntiteExistante(periodeCible);

            creerHistoriqueFusionEtatPeriode(periodeCible);
            creerHistoriqueCommentairePeriodes(periodeCible, commentairesPeriodeCandidates);
            creerHistoriqueAttenteRetourEtpPeriode(periodeCible, attenteRetourEtpPeriodeCandidates);
            messageControleRepository.supprimeMessagesSaufOriginePourPeriode(periodeCible.getIdPeriode(), MessageControle.ORIGINE_MESSAGE_GEST);

            // Etape 5 : F09_RG_1_21
            creationMessageControleFusion(periodeCible);
        }

        // fusion terminé, on redirige vers l'écran de recherche
        return REDIRECT_DECLARATION
                + (StringUtils.isNotBlank(criteresFusion.getParametresRecherche()) ? "?" + criteresFusion.getParametresRecherche() : "");

    }

    private void creerHistoriqueAttenteRetourEtpPeriode(PeriodeRecue periodeCible,
            List<HistoriqueAttenteRetourEtpPeriode> attenteRetourEtpPeriodeCandidates) {
        for (HistoriqueAttenteRetourEtpPeriode attenteRetour : attenteRetourEtpPeriodeCandidates) {
            HistoriqueAttenteRetourEtpPeriode nouvelHistoriqueAttenteRetourEtp = new HistoriqueAttenteRetourEtpPeriode();
            nouvelHistoriqueAttenteRetourEtp.setIdPeriode(periodeCible.getId());
            nouvelHistoriqueAttenteRetourEtp.setAuditUtilisateurCreation(getUtilisateurAuthentifie().getIdentifiantActiveDirectory());
            nouvelHistoriqueAttenteRetourEtp.setIdentifiantUtilisateur(getUtilisateurAuthentifie().getIdentifiantActiveDirectory());
            nouvelHistoriqueAttenteRetourEtp.setAction("Fusion : " + attenteRetour.getAction());
            nouvelHistoriqueAttenteRetourEtp.setDateHeureRetour(attenteRetour.getDateHeureRetour());
            historiqueAttenteRetourEtpPeriodeRepository.create(nouvelHistoriqueAttenteRetourEtp);
        }

    }

    private void creerHistoriqueCommentairePeriodes(PeriodeRecue periodeCible, List<HistoriqueCommentairePeriode> commentairesPeriodeCandidates) {

        for (HistoriqueCommentairePeriode commentaire : commentairesPeriodeCandidates) {
            HistoriqueCommentairePeriode nouvelHistoriqueCommentairePeriode = new HistoriqueCommentairePeriode();
            nouvelHistoriqueCommentairePeriode.setDateHeureSaisie(commentaire.getDateHeureSaisie());
            nouvelHistoriqueCommentairePeriode.setIdPeriode(periodeCible.getId());
            nouvelHistoriqueCommentairePeriode.setAuditUtilisateurCreation(getUtilisateurAuthentifie().getIdentifiantActiveDirectory());
            nouvelHistoriqueCommentairePeriode.setIdentifiantUtilisateur(getUtilisateurAuthentifie().getIdentifiantActiveDirectory());
            if (commentaire.getCommentaireUtilisateur().length() > 980) {
                nouvelHistoriqueCommentairePeriode
                        .setCommentaireUtilisateur("Période fusionnée : " + commentaire.getCommentaireUtilisateur().substring(0, 980));
            } else {
                nouvelHistoriqueCommentairePeriode.setCommentaireUtilisateur("Période fusionnée : " + commentaire.getCommentaireUtilisateur());
            }
            historiqueCommentairePeriodeRepository.create(nouvelHistoriqueCommentairePeriode);
        }
    }

    /**
     * Gestion des redirections vers la page de recherche de déclarations par défaut.
     * 
     * @return la commande de redirection vers la page de recherche de déclarations par défaut
     */
    @ResponseStatus(value = HttpStatus.MOVED_PERMANENTLY)
    @RequestMapping(value = "/*")
    public String redirect() {
        return REDIRECT_DECLARATION;
    }

    /**
     * Identification de la période cible pour la fusion (cf F09_RG_1_17 )
     * 
     * @param periodes
     *            liste des périodes candidates
     * @param idsPeriodesCandidates
     *            liste des identifiants des periodes candidates
     * @return la période cible
     */
    private PeriodeRecue identificationPeriodeCible(List<PeriodeRecue> periodes, List<Long> idsPeriodesCandidates) {

        // Type des périodes identique, on choisie la période la plus ancienne
        if (typePeriodeIdentique(periodes)) {
            return periodeRecueRepository.getPlusAnciennePeriodeCree(idsPeriodesCandidates);
        }

        if (typePeriodeExistant(PeriodeRecue.TYPE_PERIODE_DECLARATION, periodes)) {
            return periodeRecueRepository.getPlusAnciennePeriodeCreeAvecType(PeriodeRecue.TYPE_PERIODE_DECLARATION, idsPeriodesCandidates);
        }

        if (typePeriodeExistant(PeriodeRecue.TYPE_PERIODE_COMPLEMENT, periodes)) {
            return periodeRecueRepository.getPlusAnciennePeriodeCreeAvecType(PeriodeRecue.TYPE_PERIODE_COMPLEMENT, idsPeriodesCandidates);
        }

        if (typePeriodeExistant(PeriodeRecue.TYPE_PERIODE_REGULARISATION, periodes)) {
            return periodeRecueRepository.getPlusAnciennePeriodeCreeAvecType(PeriodeRecue.TYPE_PERIODE_REGULARISATION, idsPeriodesCandidates);
        }

        // ne devrait pas se produire, on match forcément un des 3 types de périodes
        return null;

    }

    /**
     * Indique si le type de période recherché est présent une unique fois dans la liste donnée
     * 
     * @param type
     *            type de période
     * @param periodes
     *            la liste des périodes
     * @return true si le type recherché apparait au moins une fois
     */
    private boolean typePeriodeExistant(String type, List<PeriodeRecue> periodes) {
        boolean existe = false;
        for (PeriodeRecue periode : periodes) {
            if (type.equals(periode.getTypePeriode())) {
                existe = true;
                break;
            }
        }

        return existe;
    }

    /**
     * Effectue la purge des éléments déclaratifs et des périodes "autres" cf F09_RG_1_19
     * 
     * @param idsPeriode
     *            liste des identifiants période
     */
    private void purgeElementDeclaratifEtPeriodeAutre(List<Long> idsPeriode) {

        // Purger en masse les éléments déclaratifs calculés pour ces périodes
        String sql = "call Purger_elements_declaratifs_en_masse(:listeIdsPeriodes)";
        Map<String, Object> params = new HashMap<>();
        params.put("listeIdsPeriodes", StringUtils.join(idsPeriode, ","));

        LOGGER.info("Appel de la procédure stockée Purger_elements_declaratifs_en_masse avec les paramètres {}", params);
        periodeRecueRepository.callMysqlProcedure(sql, params);
        LOGGER.info("Fin de la procédure stockée Purger_elements_declaratifs_en_masse");

        for (Long idPeriode : idsPeriode) {
            compteRenduIntegrationRepository.supprimeCompteRenduIntegrationPourPeriode(idPeriode);
            historiqueEtatPeriodeRepository.supprimeHistoriqueEtatPeriodePourPeriode(idPeriode);
            messageControleRepository.supprimeMessageControlePourPeriode(idPeriode);
            historiqueAssignationPeriodeRepository.supprimeHistoriqueAssignationPourPeriode(idPeriode);
            historiqueAttenteRetourEtpPeriodeRepository.supprimeHistoriqueAttenteRetourEtpPourPeriode(idPeriode);
            historiqueCommentairePeriodeRepository.supprimeHistoriqueCommentairePourPeriode(idPeriode);
            periodeRecueRepository.supprimePeriodeRecue(idPeriode);
        }

    }

    /**
     * Création d'un message controle pour la période , pour le controle RED_FUSION cf F09_RG_1_21
     * 
     * @param periode
     *            La période pour laquelle on va créer un message controle
     */
    private void creationMessageControleFusion(PeriodeRecue periode) {

        // F09_RG_1_27
        ParamControleSignalRejet paramControleChoisi = null;

        ParamControleSignalRejet paramControleSignalRejetGlobal = paramControleSignalRejetRepository
                .getParamControleSignalRejetPourControleEtNofam(IDENTIFIANT_CONTROLE_RED_FUSION, "ALL");
        if (paramControleSignalRejetGlobal == null) {

            // Contrat correspondant à la situation contrat valide à date de début de période
            Contrat contrat = contratRepository.getContratValidePourDate(periode.getNumeroContrat(), periode.getDateDebutPeriode());
            ParamControleSignalRejet paramControleSignalRejetPourNofam = paramControleSignalRejetRepository
                    .getParamControleSignalRejetPourControleEtNofam(IDENTIFIANT_CONTROLE_RED_FUSION,
                            Integer.toString(contrat.getNumFamilleProduit()));

            paramControleChoisi = paramControleSignalRejetPourNofam;
        } else {
            paramControleChoisi = paramControleSignalRejetGlobal;
        }

        String niveauAlerteGestRejet = paramControleChoisi != null ? paramControleChoisi.getNiveauAlerte() : StringUtils.EMPTY;
        String msgAlerte = paramControleChoisi != null ? paramControleChoisi.getMessageAlerte() : StringUtils.EMPTY;

        MessageControle msg = new MessageControle();
        msg.setIdPeriode(periode.getIdPeriode());
        msg.setIdentifiantControle(IDENTIFIANT_CONTROLE_RED_FUSION);
        msg.setOrigineMessage(MessageControle.ORIGINE_MESSAGE_REDAC);
        msg.setDateTraitement(DateRedac.maintenant());
        msg.setAuditUtilisateurCreation(getUtilisateurAuthentifie().getIdentifiantActiveDirectory());
        msg.setNiveauAlerte(niveauAlerteGestRejet);
        msg.setMessageUtilisateur(msgAlerte);

        messageControleRepository.create(msg);
    }

    /**
     * Indique si toute les périodes ont le même type
     * 
     * @param periodes
     *            liste des périodes
     * @return true si toute les périodes ont le même type
     */
    private boolean typePeriodeIdentique(List<PeriodeRecue> periodes) {
        String typePeriode = periodes.get(0).getTypePeriode();
        boolean flagTypeIdentique = true;

        for (int i = 1; i < periodes.size(); i++) {
            if (!periodes.get(i).getTypePeriode().equals(typePeriode)) {
                flagTypeIdentique = false;
                break;
            }
        }
        return flagTypeIdentique;
    }

    /**
     * Crée un historique pour la période periode cible de la fusion
     * 
     * @param periode
     *            la période cible de la fusion
     */
    private void creerHistoriqueFusionEtatPeriode(PeriodeRecue periode) {
        HistoriqueEtatPeriode nouvelHistoriqueEtatPeriode = new HistoriqueEtatPeriode();

        nouvelHistoriqueEtatPeriode.setIdPeriode(periode.getIdPeriode());
        nouvelHistoriqueEtatPeriode.setDateHeureChangement(DateRedac.convertitEnDateHeureRedac(Calendar.getInstance().getTime()));
        nouvelHistoriqueEtatPeriode.setEtatPeriode(periode.getEtatPeriode());
        nouvelHistoriqueEtatPeriode.setOrigineChangement("IHM");
        nouvelHistoriqueEtatPeriode.setAuditUtilisateurCreation(getUtilisateurAuthentifie().getIdentifiantActiveDirectory());
        nouvelHistoriqueEtatPeriode.setIdentifiantUtilisateur(getUtilisateurAuthentifie().getIdentifiantActiveDirectory());
        nouvelHistoriqueEtatPeriode.setCommentaireUtilisateur("Fusion réalisée");

        historiqueEtatPeriodeRepository.create(nouvelHistoriqueEtatPeriode);
    }

    /**
     * Blocage des périodes prise en compte dans la popup "Changement en masse des périodes"
     * 
     * @param criteres
     *            les critères de changement en masse
     * @return la commande de redirection vers la page de recherche de déclarations avec les derniers critères connus
     */
    @Secured({ RoleRedac.ROLE_REDCDE, RoleRedac.ROLE_REDREDAC })
    @RequestMapping(value = "/changementEnMasse/blocage", method = RequestMethod.POST, produces = MediaType.TEXT_HTML_VALUE)
    @Transactional(propagation = Propagation.REQUIRES_NEW, rollbackFor = Throwable.class)
    @ResponseStatus(HttpStatus.SEE_OTHER)
    public String blocagePeriodes(@RequestParam MultiValueMap<String, Object> criteres) {

        /**
         * Mapping des critères envoyés
         */

        // Récupération des changements sur les périodes ( action demandée + paramètres associés )
        PopupChangementEnMasse changementsPeriodes = PopupChangementEnMasse.from(criteres);

        // utilisateur effectuant l'action
        UtilisateurIHM utilisateur = getUtilisateurAuthentifie();

        /**
         * blocage en masse F09_RG_7_12
         */
        if ("bloquer".equals(changementsPeriodes.getBloquerPeriodesEnMasse())) {

            ParamControleSignalRejet paramControleSignalRejetGlobal = paramControleSignalRejetRepository
                    .getParamControleSignalRejetPourControleEtNofam(MessageControle.IDENTIFIANT_CONTROLE_GEST_REJET, "ALL");

            if (StringUtils.isEmpty(changementsPeriodes.getIdsPeriodes())) {
                // on utilise les critères de recherche
                // reconstruction d'un objet PeriodesRecuesCriteresRecherche à partir de la queryString
                PeriodesRecuesCriteresRecherche criteresRecherche = PeriodesRecuesCriteresRecherche.from(changementsPeriodes.getParametresRecherche(),
                        criteres);

                // pagination
                int maxPage = Long.valueOf(changementsPeriodes.getNbPeriodes().intValue() % TAILLE_PAGE == 0
                        ? changementsPeriodes.getNbPeriodes().intValue() / TAILLE_PAGE
                        : (changementsPeriodes.getNbPeriodes().intValue() / TAILLE_PAGE) + 1).intValue();

                List<Long> idsPeriodes = periodeRecueRepository.getIdPeriodesRecherche(criteresRecherche);

                for (int page = 0; page < maxPage; page++) {

                    List<Long> sousListePeriode = idsPeriodes.subList(page * TAILLE_PAGE,
                            (page + 1) == maxPage ? idsPeriodes.size() : (page + 1) * TAILLE_PAGE);

                    // On effectue la maj periode AVANT la création des messagesControles
                    // afin de pouvoir identifier les periodes déjà bloquées

                    periodeRecueRepository.modifieGestionnaireTraitePar(sousListePeriode, utilisateur.getIdentifiantActiveDirectory(), null,
                            TypeActionPopupChangementEnMasse.BLOCAGE);
                    periodeRecueRepository.modifieAuditUserEnMasse(sousListePeriode, utilisateur.getIdentifiantActiveDirectory(), null,
                            TypeActionPopupChangementEnMasse.BLOCAGE);

                    messageControleRepository.blocageEnMasse(sousListePeriode, paramControleSignalRejetGlobal,
                            utilisateur.getIdentifiantActiveDirectory());

                }
            } else {
                // On effectue la maj periode AVANT la création des messagesControles
                // afin de pouvoir identifier les periodes déjà bloquées
                periodeRecueRepository.modifieGestionnaireTraitePar(changementsPeriodes.getIdsPeriodesAsLong(),
                        utilisateur.getIdentifiantActiveDirectory(), null, TypeActionPopupChangementEnMasse.BLOCAGE);
                periodeRecueRepository.modifieAuditUserEnMasse(changementsPeriodes.getIdsPeriodesAsLong(),
                        utilisateur.getIdentifiantActiveDirectory(), null, TypeActionPopupChangementEnMasse.BLOCAGE);

                messageControleRepository.blocageEnMasse(changementsPeriodes.getIdsPeriodesAsLong(), paramControleSignalRejetGlobal,
                        utilisateur.getIdentifiantActiveDirectory());

            }

        } else {
            /**
             * deblocage en masse des périodes F09_RG_7_13
             */
            if (StringUtils.isEmpty(changementsPeriodes.getIdsPeriodes())) {
                // on utilise les critères de recherche
                // reconstruction d'un objet PeriodesRecuesCriteresRecherche à partir de la queryString
                PeriodesRecuesCriteresRecherche criteresRecherche = PeriodesRecuesCriteresRecherche.from(changementsPeriodes.getParametresRecherche(),
                        criteres);

                // pagination
                int maxPage = Long.valueOf(changementsPeriodes.getNbPeriodes().intValue() % TAILLE_PAGE == 0
                        ? changementsPeriodes.getNbPeriodes().intValue() / TAILLE_PAGE
                        : (changementsPeriodes.getNbPeriodes().intValue() / TAILLE_PAGE) + 1).intValue();

                List<Long> idsPeriodes = periodeRecueRepository.getIdPeriodesRecherche(criteresRecherche);

                for (int page = 0; page < maxPage; page++) {

                    List<Long> sousListePeriode = idsPeriodes.subList(page * TAILLE_PAGE,
                            (page + 1) == maxPage ? idsPeriodes.size() : (page + 1) * TAILLE_PAGE);

                    // On effectue la maj periode AVANT la suppression des messagesControles
                    // afin de pouvoir identifier les periodes déjà débloquées
                    periodeRecueRepository.modifieGestionnaireTraitePar(sousListePeriode, utilisateur.getIdentifiantActiveDirectory(), null,
                            TypeActionPopupChangementEnMasse.DEBLOCAGE);
                    periodeRecueRepository.modifieAuditUserEnMasse(sousListePeriode, utilisateur.getIdentifiantActiveDirectory(), null,
                            TypeActionPopupChangementEnMasse.DEBLOCAGE);

                    messageControleRepository.deblocageEnMasse(sousListePeriode);

                }
            } else {
                // On effectue la maj periode AVANT la suppression des messagesControles
                // afin de pouvoir identifier les periodes déjà débloquées
                periodeRecueRepository.modifieGestionnaireTraitePar(changementsPeriodes.getIdsPeriodesAsLong(),
                        utilisateur.getIdentifiantActiveDirectory(), null, TypeActionPopupChangementEnMasse.DEBLOCAGE);
                periodeRecueRepository.modifieAuditUserEnMasse(changementsPeriodes.getIdsPeriodesAsLong(),
                        utilisateur.getIdentifiantActiveDirectory(), null, TypeActionPopupChangementEnMasse.DEBLOCAGE);

                messageControleRepository.deblocageEnMasse(changementsPeriodes.getIdsPeriodesAsLong());

            }

        }

        return REDIRECT_DECLARATION
                + (StringUtils.isNotBlank(changementsPeriodes.getParametresRecherche()) ? "?" + changementsPeriodes.getParametresRecherche() : "");

    }

    /**
     * Effectue le controle sur les status des périodes données en paramètres pour changement d'état
     * 
     * @param criteres
     *            les critères de changement en masse
     * @return la liste des etats invalides parmi les périodes ciblées
     * @throws RessourceNonTrouveeException
     */
    @Secured({ RoleRedac.ROLE_REDCDE, RoleRedac.ROLE_REDREDAC })
    @RequestMapping(value = "/changementEnMasse/blocage/verificationStatus", method = RequestMethod.GET, produces = MediaType.TEXT_HTML_VALUE)
    @Transactional(propagation = Propagation.REQUIRES_NEW, rollbackFor = Throwable.class)
    public ResponseEntity<String> controleStatutPeriode(@RequestParam MultiValueMap<String, Object> criteres) {

        /**
         * Mapping des critères envoyés
         */
        PopupChangementEnMasse changement = PopupChangementEnMasse.from(criteres);

        /**
         * Récupération des etats invalides
         */

        // liste des statuts KO distinct
        Set<String> statutsKO = new HashSet<>();

        if (StringUtils.isEmpty(changement.getIdsPeriodes())) {

            // on utilise les critères de recherche
            // reconstruction d'un objet PeriodesRecuesCriteresRecherche à partir de la queryString
            PeriodesRecuesCriteresRecherche criteresRecherche = PeriodesRecuesCriteresRecherche.from(changement.getParametresRecherche(), criteres);

            // pagination
            int maxPage = Long.valueOf(changement.getNbPeriodes().intValue() % TAILLE_PAGE == 0 ? changement.getNbPeriodes().intValue() / TAILLE_PAGE
                    : (changement.getNbPeriodes().intValue() / TAILLE_PAGE) + 1).intValue();

            List<Long> idsPeriodes = periodeRecueRepository.getIdPeriodesRecherche(criteresRecherche);

            for (int page = 0; page < maxPage; page++) {

                List<Long> sousListePeriode = idsPeriodes.subList(page * TAILLE_PAGE,
                        (page + 1) == maxPage ? idsPeriodes.size() : (page + 1) * TAILLE_PAGE);

                statutsKO.addAll(periodeRecueRepository.getStatutsInvalidePourBlocage(sousListePeriode));
            }
        } else {
            // on utilise les id des cases cochées
            statutsKO.addAll(periodeRecueRepository.getStatutsInvalidePourBlocage(changement.getIdsPeriodesAsLong()));
        }

        /**
         * Génération de la réponse
         */
        if (statutsKO.isEmpty()) {
            return new ResponseEntity<String>("OK", HttpStatus.OK);
        } else {
            // récupération des libellés courts associés aux etats
            List<String> libelleStatutsKO = new ArrayList<>();
            for (String codeStatut : statutsKO) {
                libelleStatutsKO.add(paramCodeLibelleRepository.getLibelleEdition(TBL_PERIODE, "ETAT", codeStatut));
            }

            return new ResponseEntity<String>(org.springframework.util.StringUtils.collectionToDelimitedString(libelleStatutsKO, ", ", "« ", " »"),
                    HttpStatus.FORBIDDEN);
        }

    }

    /**
     * Mise en attente des périodes en masse dans la popup "Changement en masse des périodes" F09_RG_7_24
     * 
     * @param criteres
     *            les critères de changement en masse
     * @return la commande de redirection vers la page de recherche de déclarations avec les derniers critères connus
     */
    @Secured({ RoleRedac.ROLE_REDCDE, RoleRedac.ROLE_REDREDAC })
    @RequestMapping(value = "/changementEnMasse/miseEnAttente", method = RequestMethod.POST, produces = MediaType.TEXT_HTML_VALUE)
    @Transactional(propagation = Propagation.REQUIRES_NEW, rollbackFor = Throwable.class)
    @ResponseStatus(HttpStatus.SEE_OTHER)
    public String miseEnAttentePeriodes(@RequestParam MultiValueMap<String, Object> criteres) {

        // Récupération des changements sur les périodes ( action demandée + paramètres associés )
        PopupChangementEnMasse changementsPeriodes = PopupChangementEnMasse.from(criteres);

        // utilisateur effectuant l'action
        UtilisateurIHM utilisateur = getUtilisateurAuthentifie();

        /**
         * Attente en masse F09_RG_7_25
         */
        if ("attente".equals(changementsPeriodes.getAttenteRetourEntrepriseEnMasse())) {

            ParamControleSignalRejet paramControleSignalRejetGlobal = paramControleSignalRejetRepository
                    .getParamControleSignalRejetPourControleEtNofam(MessageControle.IDENTIFIANT_CONTROLE_GEST_RETOUR_ETP, "ALL");

            if (StringUtils.isEmpty(changementsPeriodes.getIdsPeriodes())) {
                // on utilise les critères de recherche
                // reconstruction d'un objet PeriodesRecuesCriteresRecherche à partir de la queryString
                PeriodesRecuesCriteresRecherche criteresRecherche = PeriodesRecuesCriteresRecherche.from(changementsPeriodes.getParametresRecherche(),
                        criteres);

                // pagination
                int maxPage = Long.valueOf(changementsPeriodes.getNbPeriodes().intValue() % TAILLE_PAGE == 0
                        ? changementsPeriodes.getNbPeriodes().intValue() / TAILLE_PAGE
                        : (changementsPeriodes.getNbPeriodes().intValue() / TAILLE_PAGE) + 1).intValue();

                List<Long> idsPeriodes = periodeRecueRepository.getIdPeriodesRecherche(criteresRecherche);

                for (int page = 0; page < maxPage; page++) {

                    List<Long> sousListePeriode = idsPeriodes.subList(page * TAILLE_PAGE,
                            (page + 1) == maxPage ? idsPeriodes.size() : (page + 1) * TAILLE_PAGE);

                    // On effectue la maj periode AVANT la creation des messagesControles
                    // afin de pouvoir identifier les periodes qui sont en attente

                    periodeRecueRepository.modifieGestionnaireTraitePar(sousListePeriode, utilisateur.getIdentifiantActiveDirectory(), null,
                            TypeActionPopupChangementEnMasse.ATTENTE);
                    periodeRecueRepository.modifieAuditUserEnMasse(sousListePeriode, utilisateur.getIdentifiantActiveDirectory(), null,
                            TypeActionPopupChangementEnMasse.ATTENTE);
                    messageControleRepository.miseEnAttenteEnMasse(sousListePeriode, paramControleSignalRejetGlobal,
                            utilisateur.getIdentifiantActiveDirectory());

                    // Ajout d'une ligne d'historique
                    historiqueAttenteRetourEtpPeriodeRepository.creationEnMasseOrigineIHM(sousListePeriode, TypeActionPopupChangementEnMasse.ATTENTE,
                            utilisateur.getIdentifiantActiveDirectory());

                }
            } else {
                // On effectue la maj periode AVANT la creation des messagesControles
                // afin de pouvoir identifier les periodes qui sont en attente
                periodeRecueRepository.modifieGestionnaireTraitePar(changementsPeriodes.getIdsPeriodesAsLong(),
                        utilisateur.getIdentifiantActiveDirectory(), null, TypeActionPopupChangementEnMasse.ATTENTE);
                periodeRecueRepository.modifieAuditUserEnMasse(changementsPeriodes.getIdsPeriodesAsLong(),
                        utilisateur.getIdentifiantActiveDirectory(), null, TypeActionPopupChangementEnMasse.ATTENTE);
                messageControleRepository.miseEnAttenteEnMasse(changementsPeriodes.getIdsPeriodesAsLong(), paramControleSignalRejetGlobal,
                        utilisateur.getIdentifiantActiveDirectory());

                // Ajout d'une ligne d'historique
                historiqueAttenteRetourEtpPeriodeRepository.creationEnMasseOrigineIHM(changementsPeriodes.getIdsPeriodesAsLong(),
                        TypeActionPopupChangementEnMasse.ATTENTE, utilisateur.getIdentifiantActiveDirectory());

            }

        } else {
            /**
             * levee attente en masse des périodes F09_RG_7_26
             */
            if (StringUtils.isEmpty(changementsPeriodes.getIdsPeriodes())) {
                // on utilise les critères de recherche
                // reconstruction d'un objet PeriodesRecuesCriteresRecherche à partir de la queryString
                PeriodesRecuesCriteresRecherche criteresRecherche = PeriodesRecuesCriteresRecherche.from(changementsPeriodes.getParametresRecherche(),
                        criteres);

                // pagination
                int maxPage = Long.valueOf(changementsPeriodes.getNbPeriodes().intValue() % TAILLE_PAGE == 0
                        ? changementsPeriodes.getNbPeriodes().intValue() / TAILLE_PAGE
                        : (changementsPeriodes.getNbPeriodes().intValue() / TAILLE_PAGE) + 1).intValue();

                List<Long> idsPeriodes = periodeRecueRepository.getIdPeriodesRecherche(criteresRecherche);

                for (int page = 0; page < maxPage; page++) {

                    List<Long> sousListePeriode = idsPeriodes.subList(page * TAILLE_PAGE,
                            (page + 1) == maxPage ? idsPeriodes.size() : (page + 1) * TAILLE_PAGE);

                    // On effectue la maj periode AVANT la suppression des messagesControles
                    // afin de pouvoir identifier les periodes qui ne sont pas en attente
                    periodeRecueRepository.modifieGestionnaireTraitePar(sousListePeriode, utilisateur.getIdentifiantActiveDirectory(), null,
                            TypeActionPopupChangementEnMasse.FIN_ATTENTE);
                    periodeRecueRepository.modifieAuditUserEnMasse(sousListePeriode, utilisateur.getIdentifiantActiveDirectory(), null,
                            TypeActionPopupChangementEnMasse.FIN_ATTENTE);

                    messageControleRepository.leveeMiseEnAttenteEnMasse(sousListePeriode);

                    // Ajout d'une ligne d'historique
                    historiqueAttenteRetourEtpPeriodeRepository.creationEnMasseOrigineIHM(sousListePeriode,
                            TypeActionPopupChangementEnMasse.FIN_ATTENTE, utilisateur.getIdentifiantActiveDirectory());

                }
            } else {
                // On effectue la maj periode AVANT la suppression des messagesControles
                // afin de pouvoir identifier les periodes qui ne sont pas en attente
                periodeRecueRepository.modifieGestionnaireTraitePar(changementsPeriodes.getIdsPeriodesAsLong(),
                        utilisateur.getIdentifiantActiveDirectory(), null, TypeActionPopupChangementEnMasse.FIN_ATTENTE);
                periodeRecueRepository.modifieAuditUserEnMasse(changementsPeriodes.getIdsPeriodesAsLong(),
                        utilisateur.getIdentifiantActiveDirectory(), null, TypeActionPopupChangementEnMasse.FIN_ATTENTE);

                messageControleRepository.leveeMiseEnAttenteEnMasse(changementsPeriodes.getIdsPeriodesAsLong());

                // Ajout d'une ligne d'historique
                historiqueAttenteRetourEtpPeriodeRepository.creationEnMasseOrigineIHM(changementsPeriodes.getIdsPeriodesAsLong(),
                        TypeActionPopupChangementEnMasse.FIN_ATTENTE, utilisateur.getIdentifiantActiveDirectory());
            }

        }

        return REDIRECT_DECLARATION
                + (StringUtils.isNotBlank(changementsPeriodes.getParametresRecherche()) ? "?" + changementsPeriodes.getParametresRecherche() : "");

    }

    /**
     * Affecte les périodes en masse dans la popup "Changement en masse des périodes" F09_RG_7_29
     * 
     * @param criteres
     *            les critères de changement en masse
     * @return la commande de redirection vers la page de recherche de déclarations avec les derniers critères connus
     */
    @Secured({ RoleRedac.ROLE_REDCDE, RoleRedac.ROLE_REDREDAC })
    @RequestMapping(value = "/changementEnMasse/affecterA", method = RequestMethod.POST, produces = MediaType.TEXT_HTML_VALUE)
    @Transactional(propagation = Propagation.REQUIRES_NEW, rollbackFor = Throwable.class)
    @ResponseStatus(HttpStatus.SEE_OTHER)
    public String affecterPeriodes(@RequestParam MultiValueMap<String, Object> criteres) {

        // Récupération des changements sur les périodes ( action demandée + paramètres associés )
        PopupChangementEnMasse changementsPeriodes = PopupChangementEnMasse.from(criteres);

        // utilisateur effectuant l'action
        UtilisateurIHM utilisateur = getUtilisateurAuthentifie();

        if (StringUtils.isEmpty(changementsPeriodes.getIdsPeriodes())) {
            // on utilise les critères de recherche
            // reconstruction d'un objet PeriodesRecuesCriteresRecherche à partir de la queryString
            PeriodesRecuesCriteresRecherche criteresRecherche = PeriodesRecuesCriteresRecherche.from(changementsPeriodes.getParametresRecherche(),
                    criteres);

            // pagination
            int maxPage = Long.valueOf(
                    changementsPeriodes.getNbPeriodes().intValue() % TAILLE_PAGE == 0 ? changementsPeriodes.getNbPeriodes().intValue() / TAILLE_PAGE
                            : (changementsPeriodes.getNbPeriodes().intValue() / TAILLE_PAGE) + 1)
                    .intValue();

            List<Long> idsPeriodes = periodeRecueRepository.getIdPeriodesRecherche(criteresRecherche);

            for (int page = 0; page < maxPage; page++) {

                List<Long> sousListePeriode = idsPeriodes.subList(page * TAILLE_PAGE,
                        (page + 1) == maxPage ? idsPeriodes.size() : (page + 1) * TAILLE_PAGE);
                periodeRecueRepository.modifieAuditUserEnMasse(sousListePeriode, utilisateur.getIdentifiantActiveDirectory(),
                        changementsPeriodes.getGestionnaireAffecteAEnMasse(), TypeActionPopupChangementEnMasse.AFFECTER);
                periodeRecueRepository.modifiegestionnaireAffecteA(sousListePeriode, changementsPeriodes.getGestionnaireAffecteAEnMasse());

            }

        } else {

            periodeRecueRepository.modifieAuditUserEnMasse(changementsPeriodes.getIdsPeriodesAsLong(), utilisateur.getIdentifiantActiveDirectory(),
                    changementsPeriodes.getGestionnaireAffecteAEnMasse(), TypeActionPopupChangementEnMasse.AFFECTER);
            periodeRecueRepository.modifiegestionnaireAffecteA(changementsPeriodes.getIdsPeriodesAsLong(),
                    changementsPeriodes.getGestionnaireAffecteAEnMasse());

        }

        return REDIRECT_DECLARATION
                + (StringUtils.isNotBlank(changementsPeriodes.getParametresRecherche()) ? "?" + changementsPeriodes.getParametresRecherche() : "");

    }

    /**
     * Assigner gestionnaire aux périodes en masse dans la popup "Changement en masse des périodes" champ ATraiterPar) F09_RG_7_32
     * 
     * @param criteres
     *            les critères de changement en masse
     * @return la commande de redirection vers la page de recherche de déclarations avec les derniers critères connus
     */
    @Secured({ RoleRedac.ROLE_REDCDE, RoleRedac.ROLE_REDREDAC })
    @RequestMapping(value = "/changementEnMasse/ATraiterPar", method = RequestMethod.POST, produces = MediaType.TEXT_HTML_VALUE)
    @Transactional(propagation = Propagation.REQUIRES_NEW, rollbackFor = Throwable.class)
    @ResponseStatus(HttpStatus.SEE_OTHER)
    public String assignerPeriodes(@RequestParam MultiValueMap<String, Object> criteres) {

        // Récupération des changements sur les périodes ( action demandée + paramètres associés )
        PopupChangementEnMasse changementsPeriodes = PopupChangementEnMasse.from(criteres);

        // utilisateur effectuant l'action
        UtilisateurIHM utilisateur = getUtilisateurAuthentifie();

        if (StringUtils.isEmpty(changementsPeriodes.getIdsPeriodes())) {
            // on utilise les critères de recherche
            // reconstruction d'un objet PeriodesRecuesCriteresRecherche à partir de la queryString
            PeriodesRecuesCriteresRecherche criteresRecherche = PeriodesRecuesCriteresRecherche.from(changementsPeriodes.getParametresRecherche(),
                    criteres);

            // pagination
            int maxPage = Long.valueOf(
                    changementsPeriodes.getNbPeriodes().intValue() % TAILLE_PAGE == 0 ? changementsPeriodes.getNbPeriodes().intValue() / TAILLE_PAGE
                            : (changementsPeriodes.getNbPeriodes().intValue() / TAILLE_PAGE) + 1)
                    .intValue();

            List<Long> idsPeriodes = periodeRecueRepository.getIdPeriodesRecherche(criteresRecherche);

            for (int page = 0; page < maxPage; page++) {

                List<Long> sousListePeriode = idsPeriodes.subList(page * TAILLE_PAGE,
                        (page + 1) == maxPage ? idsPeriodes.size() : (page + 1) * TAILLE_PAGE);

                periodeRecueRepository.modifieAuditUserEnMasse(sousListePeriode, utilisateur.getIdentifiantActiveDirectory(),
                        changementsPeriodes.getGestionnaireATraiterParEnMasse(), TypeActionPopupChangementEnMasse.ASSIGNER);
                periodeRecueRepository.modifieGestionnaireATraiterPar(sousListePeriode, changementsPeriodes.getGestionnaireATraiterParEnMasse());

                historiqueAssignationPeriodeRepository.creationEnMasseOrigineIHM(sousListePeriode,
                        changementsPeriodes.getGestionnaireATraiterParEnMasse(), utilisateur.getIdentifiantActiveDirectory());
            }

        } else {
            // Cas où une séléction de période à été effectuée
            periodeRecueRepository.modifieAuditUserEnMasse(changementsPeriodes.getIdsPeriodesAsLong(), utilisateur.getIdentifiantActiveDirectory(),
                    changementsPeriodes.getGestionnaireATraiterParEnMasse(), TypeActionPopupChangementEnMasse.ASSIGNER);
            periodeRecueRepository.modifieGestionnaireATraiterPar(changementsPeriodes.getIdsPeriodesAsLong(),
                    changementsPeriodes.getGestionnaireATraiterParEnMasse());

            historiqueAssignationPeriodeRepository.creationEnMasseOrigineIHM(changementsPeriodes.getIdsPeriodesAsLong(),
                    changementsPeriodes.getGestionnaireATraiterParEnMasse(), utilisateur.getIdentifiantActiveDirectory());

        }

        return REDIRECT_DECLARATION
                + (StringUtils.isNotBlank(changementsPeriodes.getParametresRecherche()) ? "?" + changementsPeriodes.getParametresRecherche() : "");

    }

    /**
     * Traitement ( Changer le statut des périodes) des périodes en masse dans la popup "Changement en masse des périodes"
     * 
     * @param criteres
     *            les critères de changement en masse
     * @return l'url de redirection vers la page de recherche de déclarations avec les derniers critères connus
     */
    @Secured({ RoleRedac.ROLE_REDCDE, RoleRedac.ROLE_REDREDAC })
    @RequestMapping(value = "/changementEnMasse/changementStatut", method = RequestMethod.POST, produces = MediaType.TEXT_HTML_VALUE)
    @Transactional(propagation = Propagation.REQUIRES_NEW, rollbackFor = Throwable.class)
    @ResponseStatus(HttpStatus.SEE_OTHER)
    public ResponseEntity<String> changementStatut(@RequestParam MultiValueMap<String, Object> criteres) {

        /**
         * Mapping des critères envoyés
         */
        PopupChangementEnMasse changementsPeriodes = PopupChangementEnMasse.from(criteres);

        // utilisateur effectuant l'action
        UtilisateurIHM utilisateur = getUtilisateurAuthentifie();

        // Selection de l'etat cible dans la liste
        String statutCible = changementsPeriodes.getCodeLibelleChangementDeStatutEnMasse();
        String messageUtilisateur = "";

        if (StringUtils.isEmpty(changementsPeriodes.getIdsPeriodes())) {

            // on utilise les critères de recherche
            // reconstruction d'un objet PeriodesRecuesCriteresRecherche à partir de la queryString
            PeriodesRecuesCriteresRecherche criteresRecherche = PeriodesRecuesCriteresRecherche.from(changementsPeriodes.getParametresRecherche(),
                    criteres);
            // pagination
            int maxPage = Long.valueOf(
                    changementsPeriodes.getNbPeriodes().intValue() % TAILLE_PAGE == 0 ? changementsPeriodes.getNbPeriodes().intValue() / TAILLE_PAGE
                            : (changementsPeriodes.getNbPeriodes().intValue() / TAILLE_PAGE) + 1)
                    .intValue();
            criteresRecherche.setPageTaille(TAILLE_PAGE);

            List<Long> idsPeriodes = periodeRecueRepository.getIdPeriodesRecherche(criteresRecherche);

            for (int page = 0; page < maxPage; page++) {

                List<Long> sousListePeriode = idsPeriodes.subList(page * TAILLE_PAGE,
                        (page + 1) == maxPage ? idsPeriodes.size() : (page + 1) * TAILLE_PAGE);

                periodeRecueRepository.modifieGestionnaireTraitePar(sousListePeriode, utilisateur.getIdentifiantActiveDirectory(), null,
                        TypeActionPopupChangementEnMasse.STATUT);
                periodeRecueRepository.modifieAuditUserEnMasse(sousListePeriode, utilisateur.getIdentifiantActiveDirectory(), null,
                        TypeActionPopupChangementEnMasse.STATUT);

                periodeRecueRepository.changementStatutPeriodeEnMasse(statutCible, sousListePeriode);
                historiqueEtatPeriodeRepository.creationEnMasseOrigineIHM(sousListePeriode, messageUtilisateur,
                        utilisateur.getIdentifiantActiveDirectory(), statutCible);

                if ("ARI".equals(statutCible)) {
                    messageControleRepository.supressionMessageControleEnMasse(sousListePeriode, MessageControle.ORIGINE_MESSAGE_GEST,
                            MessageControle.IDENTIFIANT_CONTROLE_GEST_REJET);
                }
            }
        } else {

            periodeRecueRepository.modifieGestionnaireTraitePar(changementsPeriodes.getIdsPeriodesAsLong(),
                    utilisateur.getIdentifiantActiveDirectory(), null, TypeActionPopupChangementEnMasse.STATUT);
            periodeRecueRepository.modifieAuditUserEnMasse(changementsPeriodes.getIdsPeriodesAsLong(), utilisateur.getIdentifiantActiveDirectory(),
                    null, TypeActionPopupChangementEnMasse.STATUT);

            periodeRecueRepository.changementStatutPeriodeEnMasse(statutCible, changementsPeriodes.getIdsPeriodesAsLong());
            historiqueEtatPeriodeRepository.creationEnMasseOrigineIHM(changementsPeriodes.getIdsPeriodesAsLong(), messageUtilisateur,
                    utilisateur.getIdentifiantActiveDirectory(), statutCible);

            if ("ARI".equals(statutCible)) {
                messageControleRepository.supressionMessageControleEnMasse(changementsPeriodes.getIdsPeriodesAsLong(),
                        MessageControle.ORIGINE_MESSAGE_GEST, MessageControle.IDENTIFIANT_CONTROLE_GEST_REJET);
            }
        }

        return new ResponseEntity<String>("/red-ihm/ihm/declarations/"
                + (StringUtils.isNotBlank(changementsPeriodes.getParametresRecherche()) ? "?" + changementsPeriodes.getParametresRecherche() : ""),
                HttpStatus.SEE_OTHER);

    }

    /**
     * Traitement (ajout de commentaire) des périodes en masse dans la popup "Changement en masse des périodes"
     * 
     * @param criteres
     *            les critères de changement en masse
     * @return l'url de redirection vers la page de recherche de déclarations avec les derniers critères connus
     */
    @Secured({ RoleRedac.ROLE_REDCDE, RoleRedac.ROLE_REDREDAC })
    @RequestMapping(value = "/changementEnMasse/commentaire", method = RequestMethod.POST, produces = MediaType.TEXT_HTML_VALUE)
    @Transactional(propagation = Propagation.REQUIRES_NEW, rollbackFor = Throwable.class)
    @ResponseStatus(HttpStatus.SEE_OTHER)
    public String ajoutCommentaire(@RequestParam MultiValueMap<String, Object> criteres) {

        /**
         * Mapping des critères envoyés
         */
        PopupChangementEnMasse changementsPeriodes = PopupChangementEnMasse.from(criteres);

        // utilisateur effectuant l'action
        UtilisateurIHM utilisateur = getUtilisateurAuthentifie();

        String messageUtilisateur = changementsPeriodes.getCommentaireEnMasse();

        if (StringUtils.isEmpty(changementsPeriodes.getIdsPeriodes())) {

            // on utilise les critères de recherche
            // reconstruction d'un objet PeriodesRecuesCriteresRecherche à partir de la queryString
            PeriodesRecuesCriteresRecherche criteresRecherche = PeriodesRecuesCriteresRecherche.from(changementsPeriodes.getParametresRecherche(),
                    criteres);
            // pagination
            int maxPage = Long.valueOf(
                    changementsPeriodes.getNbPeriodes().intValue() % TAILLE_PAGE == 0 ? changementsPeriodes.getNbPeriodes().intValue() / TAILLE_PAGE
                            : (changementsPeriodes.getNbPeriodes().intValue() / TAILLE_PAGE) + 1)
                    .intValue();
            criteresRecherche.setPageTaille(TAILLE_PAGE);

            List<Long> idsPeriodes = periodeRecueRepository.getIdPeriodesRecherche(criteresRecherche);

            for (int page = 0; page < maxPage; page++) {

                List<Long> sousListePeriode = idsPeriodes.subList(page * TAILLE_PAGE,
                        (page + 1) == maxPage ? idsPeriodes.size() : (page + 1) * TAILLE_PAGE);

                periodeRecueRepository.modifieGestionnaireTraitePar(sousListePeriode, utilisateur.getIdentifiantActiveDirectory(), null,
                        TypeActionPopupChangementEnMasse.COMMENTAIRE);
                periodeRecueRepository.modifieAuditUserEnMasse(sousListePeriode, utilisateur.getIdentifiantActiveDirectory(), null,
                        TypeActionPopupChangementEnMasse.COMMENTAIRE);

                historiqueCommentairePeriodeRepository.creationEnMasseOrigineIHM(sousListePeriode, messageUtilisateur,
                        utilisateur.getIdentifiantActiveDirectory());

            }
        } else {
            periodeRecueRepository.modifieGestionnaireTraitePar(changementsPeriodes.getIdsPeriodesAsLong(),
                    utilisateur.getIdentifiantActiveDirectory(), null, TypeActionPopupChangementEnMasse.COMMENTAIRE);
            periodeRecueRepository.modifieAuditUserEnMasse(changementsPeriodes.getIdsPeriodesAsLong(), utilisateur.getIdentifiantActiveDirectory(),
                    null, TypeActionPopupChangementEnMasse.COMMENTAIRE);

            historiqueCommentairePeriodeRepository.creationEnMasseOrigineIHM(changementsPeriodes.getIdsPeriodesAsLong(), messageUtilisateur,
                    utilisateur.getIdentifiantActiveDirectory());
        }

        return REDIRECT_DECLARATION
                + (StringUtils.isNotBlank(changementsPeriodes.getParametresRecherche()) ? "?" + changementsPeriodes.getParametresRecherche() : "");

    }

    /**
     * Effectue le controle sur les status des périodes données en paramètres pour changement d'état F09_RG_7_15
     * 
     * @param criteres
     *            multivalue map contenant les critères
     * @return la liste des etats invalides parmi les périodes ciblées
     * @throws RessourceNonTrouveeException
     */
    @Secured({ RoleRedac.ROLE_REDCDE, RoleRedac.ROLE_REDREDAC })
    @RequestMapping(value = "/changementStatutEnMasse/verificationStatuts", method = RequestMethod.GET, produces = MediaType.TEXT_HTML_VALUE)
    @Transactional(propagation = Propagation.REQUIRES_NEW, rollbackFor = Throwable.class)
    public ResponseEntity<String> controlesChangementStatutEnMasse(@RequestParam MultiValueMap<String, Object> criteres) {

        /**
         * Mapping des critères envoyés
         */
        PopupChangementEnMasse changementsPeriodes = PopupChangementEnMasse.from(criteres);

        List<String> statutOriginesDistinct = new ArrayList<>();

        List<String> statutsKO = new ArrayList<>();

        String statutCible = changementsPeriodes.getCodeLibelleChangementDeStatutEnMasse();

        if (StringUtils.isEmpty(changementsPeriodes.getIdsPeriodes())) {

            // on utilise les critères de recherche
            // reconstruction d'un objet PeriodesRecuesCriteresRecherche à partir de la queryString
            PeriodesRecuesCriteresRecherche criteresRecherche = PeriodesRecuesCriteresRecherche.from(changementsPeriodes.getParametresRecherche(),
                    criteres);

            // On récupère les etats d'origine distincts directement, sans récupérer les id_periodes
            // liste des etats réduite, pas de pagination necessaire

            statutOriginesDistinct.addAll(periodeRecueRepository.getStatutPeriodesRecherche(criteresRecherche));

            // verification validité statuts
            statutsKO.addAll(verificationStatutCible(statutCible, statutOriginesDistinct));

        } else {

            // Liste des statuts des périodes ciblées
            statutOriginesDistinct = periodeRecueRepository.getStatutPeriodesRecherche(changementsPeriodes.getIdsPeriodesAsLong());

            // verification validité statuts
            statutsKO.addAll(verificationStatutCible(statutCible, statutOriginesDistinct));
        }

        /**
         * Génération de la réponse
         */
        if (statutsKO.isEmpty()) {
            return new ResponseEntity<String>("OK", HttpStatus.OK);
        } else {

            StringBuilder message = new StringBuilder();
            message.append("Afin de pouvoir passer au statut «");
            message.append(paramCodeLibelleRepository.getLibelleEdition(TBL_PERIODE, "ETAT", statutCible));
            message.append("», toutes les périodes doivent être au statut ");

            // Etat obligatoire
            if (EtatPeriode.ARI.getCodeEtat().equals(statutCible) || EtatPeriode.TPG.getCodeEtat().equals(statutCible)
                    || EtatPeriode.SSG.getCodeEtat().equals(statutCible)) {
                message.append("« Non intégrée »");
            } else if (EtatPeriode.NIN.getCodeEtat().equals(statutCible)) {
                message.append("« A réintégrer », « Sans suite gestion » ou « Traitée par gestion »");
            } else if (EtatPeriode.ING.getCodeEtat().equals(statutCible)) {
                message.append("« Intégrée avec signalements »");
            } else if (EtatPeriode.INS.getCodeEtat().equals(statutCible)) {
                message.append("« Intégrée signalements acquittés gestion »");
            }

            // Etats non conformes
            message.append(", or certaines périodes impactées sont au statut ");
            message.append(org.springframework.util.StringUtils.collectionToDelimitedString(statutsKO, ", ", "« ", " »"));

            return new ResponseEntity<String>(message.toString(), HttpStatus.FORBIDDEN);
        }
    }

    /**
     * Vérification des etats initiaux des periodes, selon l'état ciblé F09_RG_7_15
     * 
     * @param etatCible
     *            l'état cible
     * @param etatsPeriodes
     *            la liste des etats distinct des periodes
     * @return le résultat des controles , non vide si controle KO
     */
    private List<String> verificationStatutCible(String etatCible, List<String> etatsPeriodes) {

        List<String> statutsKO = new ArrayList<>();

        switch (etatCible) {
        case "ARI":
            // F09_RG_7_16
        case "SSG":
            // F09_RG_7_20
        case "TPG":
            // F09_RG_7_21
            etatsPeriodes.removeAll(Arrays.asList("NIN"));
            break;
        case "NIN":
            // F09_RG_7_17
            etatsPeriodes.removeAll(Arrays.asList("ARI", "SSG", "TPG"));
            break;
        case "ING":
            // F09_RG_7_18
            etatsPeriodes.removeAll(Arrays.asList("INS"));
            break;
        case "INS":
            // F09_RG_7_19
            etatsPeriodes.removeAll(Arrays.asList("ING"));
            break;
        default:
            throw new RedacUnexpectedException("Etat cible inconnu : " + etatCible);
        }

        // la liste des états a été purgé des états valides, si non vides -> ne contient que des états invalides
        if (!etatsPeriodes.isEmpty()) {
            // récupération des libellés courts associés aux etats
            for (String codeEtat : etatsPeriodes) {
                statutsKO.add(paramCodeLibelleRepository.getLibelleCourt(TBL_PERIODE, "ETAT", codeEtat));
            }
        }

        return statutsKO;

    }

}
