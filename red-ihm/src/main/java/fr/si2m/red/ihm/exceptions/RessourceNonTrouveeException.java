package fr.si2m.red.ihm.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Exception levée pour une ressource non trouvée.
 * 
 * @author nortaina
 *
 */
@ResponseStatus(value = HttpStatus.NOT_FOUND)
public final class RessourceNonTrouveeException extends HttpException {
    /**
     * UID de version.
     */
    private static final long serialVersionUID = -2107373471937686739L;
}
