package fr.si2m.red.ihm;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;

import fr.si2m.red.DateRedac;
import fr.si2m.red.complement.ExtensionContrat;
import fr.si2m.red.complement.ExtensionContratRepository;
import fr.si2m.red.contrat.ContratRepository;
import fr.si2m.red.core.repository.CriteresRecherche;
import fr.si2m.red.core.utils.VersionReader;
import fr.si2m.red.ihm.session.UtilisateurIHM;
import fr.si2m.red.parametrage.ParamCodeLibelle;
import fr.si2m.red.parametrage.ParamCodeLibelleRepository;
import fr.si2m.red.reconciliation.PeriodeRecueRepository;

/**
 * Base des contrôleurs de l'IHM REDAC.
 * 
 * @author nortaina
 *
 */
public abstract class RedacIhmController {
    private static final Logger LOGGER = LoggerFactory.getLogger(RedacIhmController.class);

    private static final String PARAM_GROUPE_GESTION = "groupeGestion";

    /**
     * Le lecteur de version de l'application.
     */
    @Autowired
    private VersionReader versionReader;
    @Autowired
    private ConfigurationIHM configurationIHM;

    @Autowired
    private ParamCodeLibelleRepository paramCodeLibelleRepository;
    @Autowired
    private ContratRepository contratRepository;
    @Autowired
    private ExtensionContratRepository extensionContratRepository;
    @Autowired
    private PeriodeRecueRepository periodeRecueRepository;

    /**
     * Initialise un modèle/vue avec une page non focalisée sur un contexte précis et toute les données transverses à afficher sur cette page.
     * 
     * @param pageCiblee
     *            le nom de la page ciblée
     * @param httpRequest
     *            la requête nécessitant la récupération de ces données
     * @return le modèle/vue initialisé
     */
    protected ModelAndView initialiseModeleVue(String pageCiblee, HttpServletRequest httpRequest) {
        return initialiseModeleVue(pageCiblee, httpRequest, new ArrayList<String>(0), null, new ArrayList<Integer>(0), null);
    }

    /**
     * Initialise un modèle/vue avec une page ciblée et toute les données transverses à afficher sur cette page.
     * 
     * @param pageCiblee
     *            le nom de la page ciblée
     * @param httpRequest
     *            la requête nécessitant la récupération de ces données
     * @param groupesGestion
     *            les groupes de gestion pour les informations consolidées (null si tous)
     * @param libelleGroupeGestionUnique
     *            le libellé du groupe de gestion pour les informations consolidées
     * @param numsFamille
     *            les numéros de famille pour les informations consolidées (null si toutes)
     * @param libelleFamilleUnique
     *            le libellé du numéro de famille pour les informations consolidées
     * 
     * @return le modèle/vue initialisé
     */
    protected ModelAndView initialiseModeleVue(String pageCiblee, HttpServletRequest httpRequest, List<String> groupesGestion,
            String libelleGroupeGestionUnique, List<Integer> numsFamille, String libelleFamilleUnique) {
        ModelAndView mav = new ModelAndView(pageCiblee);
        ajouteDonneesGeneriques(mav, httpRequest, groupesGestion, libelleGroupeGestionUnique, numsFamille, libelleFamilleUnique);
        return mav;
    }

    /**
     * Récupère l'utilisateur authentifié.
     * 
     * @return l'utilisateur authentifié
     */
    protected UtilisateurIHM getUtilisateurAuthentifie() {
        UtilisateurIHM utilisateur = (UtilisateurIHM) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (utilisateur == null) {
            throw new IllegalStateException("Aucun utilisateur authentifié détecté");
        }
        return utilisateur;
    }

    /**
     * Récupère la liste des groupes de gestion connus de l'application.
     * 
     * @return la liste des groupes de gestion connus de l'application
     */
    protected List<ParamCodeLibelle> getGroupesGestionsSelectionnables() {
        return paramCodeLibelleRepository.get(ParamCodeLibelle.TABLE_CONTRAT, "NMGRPGES");
    }

    /**
     * Récupère la liste des natures de contrat connus de l'application.
     * 
     * @return la liste des natures de contrat connus de l'application
     */
    protected List<ParamCodeLibelle> getNaturesContratsSelectionnables() {
        return paramCodeLibelleRepository.get("TARIFS", "MOCALCOT");
    }

    /**
     * Récupère la liste des natures de période connus de l'application.
     * 
     * @return la liste des natures de période connus de l'application
     */
    protected List<ParamCodeLibelle> getNaturesPeriodesSelectionnables() {
        return paramCodeLibelleRepository.get("PERIODERECUE", "TYPE");
    }

    /**
     * Récupère la liste des familles de produit connues de l'application.
     * 
     * @return la liste des familles de produit connues de l'application
     */
    protected List<ParamCodeLibelle> getFamillesSelectionnables() {
        return paramCodeLibelleRepository.get(ParamCodeLibelle.TABLE_CONTRAT, "NOFAM");
    }

    /**
     * Récupère la liste des éligibilités connues de l'application.
     * 
     * @return la liste des éligibilités connues de l'application
     */
    protected List<ParamCodeLibelle> getEligibilitesSelectionnables() {
        return paramCodeLibelleRepository.get(ParamCodeLibelle.TABLE_CONTRAT, "ELIGDSN");
    }

    /**
     * Récupère la liste des natures de période connus de l'application.
     * 
     * @return la liste des natures de période connus de l'application
     */
    protected List<ParamCodeLibelle> getModesNatureContratSelectionnables() {
        return paramCodeLibelleRepository.get(ParamCodeLibelle.TABLE_EXTENSION_CONTRAT, "MODE_NAT_CONT");
    }

    /**
     * Récupère la liste des natures de période connus de l'application.
     * 
     * @return la liste des natures de période connus de l'application
     */
    protected List<ParamCodeLibelle> getVipSelectionnables() {
        return paramCodeLibelleRepository.get(ParamCodeLibelle.TABLE_EXTENSION_CONTRAT, "VIP");
    }

    /**
     * Récupère la liste des indicateurs d'exploitation connus de l'application.
     * 
     * @return la liste des indicateurs d'exploitation connus de l'application
     */
    protected List<ParamCodeLibelle> getIndicExploitationSelectionnables() {
        return paramCodeLibelleRepository.get(ParamCodeLibelle.TABLE_EXTENSION_CONTRAT, "EXPLOIT");
    }

    /**
     * Récupère la liste des indicateurs d'edition des consignes de paiement connus de l'application.
     * 
     * @return la liste des indicateurs d'edition des consignes de paiement connus de l'application
     */
    protected List<ParamCodeLibelle> getIndicConsignePaiementSelectionnables() {
        return paramCodeLibelleRepository.get(ParamCodeLibelle.TABLE_EXTENSION_CONTRAT, "EDIT_CONS_PAIMT");
    }

    /**
     * Récupère la liste des indicateurs de transfert connus de l'application.
     * 
     * @return la liste des indicateurs de transfert connus de l'application
     */
    protected List<ParamCodeLibelle> getIndicTransfertSelectionnables() {
        return paramCodeLibelleRepository.get(ParamCodeLibelle.TABLE_EXTENSION_CONTRAT, "TRANSFERT");
    }

    /**
     * Récupère la liste des modes de réaffectation connus de l'application.
     * 
     * @return la liste des modes de réaffectation connus de l'application
     */
    protected List<ParamCodeLibelle> getModesReaffectationSelectionnables() {
        return paramCodeLibelleRepository.get(ParamCodeLibelle.TABLE_EXTENSION_CONTRAT, "MODE_REAFF_CAT_EFFECTIF");
    }

    /**
     * Récupère la liste des bases assujetties connues de l'application.
     * 
     * @return la liste des bases assujetties connues de l'application
     */
    protected List<ParamCodeLibelle> getBasesAssujettiesSelectionnables() {
        return paramCodeLibelleRepository.get(ParamCodeLibelle.TABLE_EXTENSION_CONTRAT, "CTL_TYPE_BASE_ASSUJ");
    }

    /**
     * Ajoute les données transverses à toutes les pages à un modèle/vue donné.
     * 
     * @param mav
     *            le modèle/vue à mettre à jour avec les données transverses
     * @param httpRequest
     *            la requête nécessitant la récupération de ces données
     * @param groupesGestion
     *            les groupes de gestion pour les informations consolidées (null si tous)
     * @param libelleGroupeGestionUnique
     *            le libellé du groupe de gestion pour les informations consolidées
     * @param numsFamille
     *            les numéros de famille pour les informations consolidées (null si toutes)
     * @param libelleFamilleUnique
     *            le libellé du numéro de famille pour les informations consolidées
     */
    private void ajouteDonneesGeneriques(ModelAndView mav, HttpServletRequest httpRequest, List<String> groupesGestion,
            String libelleGroupeGestionUnique, List<Integer> numsFamille, String libelleFamilleUnique) {
        // Info utilisateur
        mav.addObject("utilisateur", getInfoUtilisateur(httpRequest));
        // Info résumé des éléments à traiter
        mav.addObject("digest", getDigestContratsEtPeriodes(groupesGestion, libelleGroupeGestionUnique, numsFamille, libelleFamilleUnique));
        // Info version
        mav.addObject("version", getApplicationVersion());
        // Info brique DSN
        mav.addObject("urlBriqueDSN", configurationIHM.getUrlBriqueDsn());
    }

    /**
     * Récupère les informations sur l'utilisateur de l'application.
     * 
     * @param httpRequest
     *            la requête nécessitant une récupération de ces informations
     * @return les informations sur l'utilisateur
     */
    protected Map<String, Object> getInfoUtilisateur(HttpServletRequest httpRequest) {
        @SuppressWarnings("unchecked")
        Map<String, Object> infoUtilisateur = (Map<String, Object>) httpRequest.getSession().getAttribute("INFO_UTIL");
        if (infoUtilisateur == null) {
            UtilisateurIHM utilisateur = getUtilisateurAuthentifie();
            infoUtilisateur = new HashMap<String, Object>();
            infoUtilisateur.put("nomComplet", utilisateur.getPrenom() + " " + utilisateur.getNom());
            infoUtilisateur.put(RoleRedac.ID_ROLE_GESTION_REDAC, false);
            infoUtilisateur.put(RoleRedac.ID_ROLE_GESTION_CDE, false);
            infoUtilisateur.put("droitVIP", utilisateur.isDroitVIP());
            infoUtilisateur.put("droitIndividu", utilisateur.isDroitIndividu());
            infoUtilisateur.put(PARAM_GROUPE_GESTION, utilisateur.getGroupeGestion());
            Authentication authentification = SecurityContextHolder.getContext().getAuthentication();
            Collection<? extends GrantedAuthority> droits = authentification.getAuthorities();
            for (GrantedAuthority droit : droits) {
                if (StringUtils.equals(droit.getAuthority(), RoleRedac.ROLE_REDREDAC)) {
                    infoUtilisateur.put(RoleRedac.ID_ROLE_GESTION_REDAC, true);
                } else if (StringUtils.equals(droit.getAuthority(), RoleRedac.ROLE_REDCDE)) {
                    infoUtilisateur.put(RoleRedac.ID_ROLE_GESTION_CDE, true);
                }
            }
            httpRequest.getSession().setAttribute("INFO_UTIL", infoUtilisateur);
        }
        return infoUtilisateur;
    }

    /**
     * Récupère les informations consolidées sur les contrats et périodes à traiter / en cours de traitement selon le contexte de la page en cours.
     * 
     * @param groupesGestion
     *            les groupes de gestion pour les informations consolidées (null si tous)
     * @param libelleGroupeGestionUnique
     *            le libellé du groupe de gestion pour les informations consolidées
     * @param numsFamille
     *            les numéros de famille pour les informations consolidées (null si toutes)
     * @param libelleFamilleUnique
     *            le libellé du numéro de famille pour les informations consolidées
     * 
     * @return les informations consolidées sur les contrats et périodes selon le contexte de la page en cours
     */
    protected Map<String, Object> getDigestContratsEtPeriodes(List<String> groupesGestion, String libelleGroupeGestionUnique,
            List<Integer> numsFamille, String libelleFamilleUnique) {
        Map<String, Object> digest = new HashMap<String, Object>();
        String libelleGroupe = "";
        if (groupesGestion == null || groupesGestion.isEmpty()) {
            libelleGroupe = "tous";
        } else if (groupesGestion.size() == 1) {
            libelleGroupe = libelleGroupeGestionUnique;
        } else {
            libelleGroupe = "multiples";
        }

        String libelleFamille = "";
        if (numsFamille == null || numsFamille.isEmpty()) {
            libelleFamille = "toutes";
        } else if (numsFamille.size() == 1) {
            libelleFamille = libelleFamilleUnique;
        } else {
            libelleFamille = "multiples";
        }

        Integer dateDuJour = DateRedac.convertitEnDateRedac(Calendar.getInstance().getTime());

        digest.put(PARAM_GROUPE_GESTION, libelleGroupe);
        digest.put("famille", libelleFamille);
        digest.put("nbContratsEligiblesDSN", contratRepository.compteContratsEligiblesDSN(groupesGestion, numsFamille, dateDuJour));
        digest.put("nbPeriodesEnCoursReception", periodeRecueRepository.comptePeriodesEnCoursDeReception(groupesGestion, numsFamille, dateDuJour));
        digest.put("nbPeriodesATraiter", periodeRecueRepository.comptePeriodesATraiter(groupesGestion, numsFamille, dateDuJour));
        return digest;
    }

    /**
     * Crée des informations de pagination pour l'IHM.
     * 
     * @param nbTotalResultats
     *            le nombre total de résultats sans pagination
     * @param criteresRecherche
     *            les critères de recherche
     * @return les informations de pagination créées
     */
    protected Map<String, Object> creeResultatsInfoPagination(long nbTotalResultats, CriteresRecherche criteresRecherche) {
        Map<String, Object> resultatsMeta = new HashMap<String, Object>();
        resultatsMeta.put("total", nbTotalResultats);
        resultatsMeta.put("pageNumero", criteresRecherche.getPageNumero());
        int pageTaille = criteresRecherche.getPageTaille();
        resultatsMeta.put("pageTaille", pageTaille);
        resultatsMeta.put("maxPage", nbTotalResultats % pageTaille == 0 ? nbTotalResultats / pageTaille : (nbTotalResultats / pageTaille) + 1);
        return resultatsMeta;
    }

    /**
     * Vérifie si l'utilisateur a accès à un contrat donné.
     * 
     * @param numContrat
     *            le numéro de contrat
     * @return true si l'utilisateur à accès à ce contrat, false sinon
     */
    protected boolean verifieAccesContrat(String numContrat) {
        UtilisateurIHM utilisateur = getUtilisateurAuthentifie();
        ExtensionContrat extensionContrat = extensionContratRepository.getExtensionContrat(numContrat);
        boolean contratNonVIP = extensionContrat == null || !extensionContrat.isVip();
        return contratNonVIP
                || utilisateur.isDroitVIP() && (utilisateur.getNumContratsVIP().isEmpty() || utilisateur.getNumContratsVIP().contains(numContrat));
    }

    /**
     * Gère les exceptions non prévues.
     * 
     * @param exception
     *            l'exception non prévue
     * 
     * @return les informations sur l'erreur détectée
     */
    @ExceptionHandler(RuntimeException.class)
    public ResponseEntity<Object> traiteErreurExecution(RuntimeException exception) {
        String codeErreur = UUID.randomUUID().toString();
        LOGGER.error("Une erreur inattendue a été détectée avec le code erreur " + codeErreur, exception);
        return new ResponseEntity<Object>(
                "Une erreur inattendue a été détectée, veuillez contacter l'administrateur de l'application en précisant ce code d'erreur : "
                        + codeErreur,
                HttpStatus.INTERNAL_SERVER_ERROR);
    }

    /**
     * Gère les exceptions d'accès interdit.
     * 
     * @param exception
     *            l'exception d'accès
     * 
     * @return les informations sur l'erreur détectée
     */
    @ExceptionHandler(AccessDeniedException.class)
    public ResponseEntity<Object> traiteAccesInterdit(AccessDeniedException exception) {
        LOGGER.warn("Tentative d'accès interdit à une méthode sécurisée : {}", exception.getMessage());
        return new ResponseEntity<Object>("Vous n'avez pas accès à cette ressource", HttpStatus.UNAUTHORIZED);
    }

    /**
     * Récupère la version de l'application.
     * 
     * @return la version de l'application
     */
    private String getApplicationVersion() {
        return versionReader.readApplicationVersion();
    }

}
