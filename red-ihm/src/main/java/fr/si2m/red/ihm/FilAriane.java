package fr.si2m.red.ihm;

import java.util.List;

import lombok.Data;

/**
 * Réprésentation d'un fil d'Ariane dans l'IHM REDAC.
 * 
 * @author nortaina
 *
 */
@Data
public class FilAriane {
    /**
     * Le fil des pages.
     * 
     * @param fil
     *            le fil des pages
     * @return le fil des pages
     */
    private List<Pages> fil;
}
