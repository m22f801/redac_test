package fr.si2m.red.ihm;

/**
 * Rôle connu de l'application REDAC.
 * 
 * @author nortaina - eudesr
 *
 */
public final class RoleRedac {
    /**
     * Rôle de consultation des données de toute l'application.// groupe 1
     */
    public static final String ROLE_REDCONSULT = "ROLE_REDCONSULT";
    /**
     * Rôle de gestion des données REDAC.// groupe 3
     */
    public static final String ROLE_REDREDAC = "ROLE_REDREDAC";
    /**
     * Rôle de gestion des données CDE. //groupe 2
     */
    public static final String ROLE_REDCDE = "ROLE_REDCDE";

    /**
     * cle associe au role (groupe 3 ) dans la map des droits de l'user
     */
    public static final String ID_ROLE_GESTION_REDAC = "roleGestionRedac";

    /**
     * cle associe au role (groupe 2 ) dans la map des droits de l'user
     */
    public static final String ID_ROLE_GESTION_CDE = "roleGestionCde";

    private RoleRedac() {
    }
}
