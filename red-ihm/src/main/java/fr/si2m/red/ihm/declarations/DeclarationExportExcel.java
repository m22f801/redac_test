package fr.si2m.red.ihm.declarations;

import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;

import fr.si2m.red.DateRedac;
import fr.si2m.red.contrat.Contrat;
import fr.si2m.red.ihm.ExportExcel;
import fr.si2m.red.parametrage.ParamCodeLibelle;
import fr.si2m.red.reconciliation.PeriodeRecue;

/**
 * Base pour les exports Excel autour d'une déclaration.
 * 
 * @author nortaina
 *
 */
public abstract class DeclarationExportExcel extends ExportExcel {
    /**
     * Le conteneur de la période aux individus à exporter.
     */
    protected static final ThreadLocal<PeriodeRecue> PERIODE = new ThreadLocal<>();
    /**
     * Le conteneur des informations du contrat de la période à exporter.
     */
    protected static final ThreadLocal<Contrat> CONTRAT = new ThreadLocal<>();

    private static final int SYNTHESE_TITRE_POSITION_LIBELLE_1 = 0;
    private static final int SYNTHESE_TITRE_POSITION_VALEUR_1 = 1;
    private static final int SYNTHESE_TITRE_POSITION_LIBELLE_2 = 4;
    private static final int SYNTHESE_TITRE_POSITION_VALEUR_2 = 5;

    private static final int SYNTHESE_POSITION_LIBELLE_1 = 0;
    private static final int SYNTHESE_POSITION_VALEUR_1 = 1;
    private static final int SYNTHESE_POSITION_LIBELLE_2 = 3;
    private static final int SYNTHESE_POSITION_VALEUR_2 = 4;

    /**
     * Exporte le contenu Excel dans le flux sortant donné.
     * 
     * @param periodeRecue
     *            la période dont les individus sont à exporter
     * @param contrat
     *            les données du contrat à prendre en compte pour cette période
     * @param fluxSortant
     *            le flux sortant
     */
    public void exporte(PeriodeRecue periodeRecue, Contrat contrat, OutputStream fluxSortant) {
        PERIODE.set(periodeRecue);
        CONTRAT.set(contrat);
        super.exporte(fluxSortant);
    }

    public void exporteMontant(PeriodeRecue periodeRecue, Contrat contrat, OutputStream fluxSortant) {
        PERIODE.set(periodeRecue);
        CONTRAT.set(contrat);
        super.exporteMontant(fluxSortant);
    }

    public void exporteMessage(PeriodeRecue periodeRecue, Contrat contrat, OutputStream fluxSortant) {
        PERIODE.set(periodeRecue);
        CONTRAT.set(contrat);
        super.exporteMessage(fluxSortant);
    }

    /**
     * Crée l'entête de l'onglet de synthèse pour la déclaration.
     * 
     * @param workbook
     *            le classeur
     * @param synthese
     *            l'onglet de synthèse
     * @return l'entête de l'onglet
     */
    protected int creeEnteteOngletSynthese(Workbook workbook, Sheet synthese) {
        PeriodeRecue periode = PERIODE.get();

        int rowNum = 0;

        // Titre sur 7 colonnes
        Row ligneTitre = synthese.createRow(rowNum++);
        Cell celluleTitre = ligneTitre.createCell(0);
        CellStyle styleCelluleTitre = creeStyleCelluleTitreSynthese(workbook);
        celluleTitre.setCellStyle(styleCelluleTitre);
        celluleTitre.setCellValue("Synthèse de période");
        for (int i = 1; i < 7; i++) {
            ligneTitre.createCell(i).setCellStyle(styleCelluleTitre);
        }
        synthese.addMergedRegion(new CellRangeAddress(0, 0, 0, 6));

        // Styles
        CellStyle styleSousTitreLibelle = creeStyleCelluleSousTitre(workbook, false);
        styleSousTitreLibelle.setAlignment(CellStyle.ALIGN_RIGHT);
        styleSousTitreLibelle.setFillForegroundColor(IndexedColors.WHITE.getIndex());
        styleSousTitreLibelle.setFillPattern(CellStyle.SOLID_FOREGROUND);

        CellStyle styleSousTitreValeur = creeStyleCelluleSousTitre(workbook, true);
        styleSousTitreValeur.setFillForegroundColor(IndexedColors.WHITE.getIndex());
        styleSousTitreValeur.setFillPattern(CellStyle.SOLID_FOREGROUND);

        CellStyle bordureADroite = workbook.createCellStyle();
        bordureADroite.setBorderRight(CellStyle.BORDER_THIN);
        bordureADroite.setRightBorderColor(IndexedColors.BLACK.getIndex());
        bordureADroite.setFillForegroundColor(IndexedColors.WHITE.getIndex());
        bordureADroite.setFillPattern(CellStyle.SOLID_FOREGROUND);

        CellStyle bordureEnBas = workbook.createCellStyle();
        bordureEnBas.setBorderBottom(CellStyle.BORDER_THIN);
        bordureEnBas.setBottomBorderColor(IndexedColors.BLACK.getIndex());
        bordureEnBas.setFillForegroundColor(IndexedColors.WHITE.getIndex());
        bordureEnBas.setFillPattern(CellStyle.SOLID_FOREGROUND);

        CellStyle styleSousTitreLibelleBordureEnBas = creeStyleCelluleSousTitre(workbook, false);
        styleSousTitreLibelleBordureEnBas.setAlignment(CellStyle.ALIGN_RIGHT);
        styleSousTitreLibelleBordureEnBas.setBorderBottom(CellStyle.BORDER_THIN);
        styleSousTitreLibelleBordureEnBas.setBottomBorderColor(IndexedColors.BLACK.getIndex());
        styleSousTitreLibelleBordureEnBas.setFillForegroundColor(IndexedColors.WHITE.getIndex());
        styleSousTitreLibelleBordureEnBas.setFillPattern(CellStyle.SOLID_FOREGROUND);

        CellStyle styleSousTitreValeurBordureEnBas = creeStyleCelluleSousTitre(workbook, true);
        styleSousTitreValeurBordureEnBas.setBorderBottom(CellStyle.BORDER_THIN);
        styleSousTitreValeurBordureEnBas.setBottomBorderColor(IndexedColors.BLACK.getIndex());
        styleSousTitreValeurBordureEnBas.setFillForegroundColor(IndexedColors.WHITE.getIndex());
        styleSousTitreValeurBordureEnBas.setFillPattern(CellStyle.SOLID_FOREGROUND);

        CellStyle bordureADroiteEtEnBas = workbook.createCellStyle();
        bordureADroiteEtEnBas.setBorderRight(CellStyle.BORDER_THIN);
        bordureADroiteEtEnBas.setRightBorderColor(IndexedColors.BLACK.getIndex());
        bordureADroiteEtEnBas.setBorderBottom(CellStyle.BORDER_THIN);
        bordureADroiteEtEnBas.setBottomBorderColor(IndexedColors.BLACK.getIndex());
        bordureADroiteEtEnBas.setFillForegroundColor(IndexedColors.WHITE.getIndex());
        bordureADroiteEtEnBas.setFillPattern(CellStyle.SOLID_FOREGROUND);

        // Numéro de contrat et début de période
        Row ligneSynthese1 = synthese.createRow(rowNum++);

        creeCelluleFormulaireLibelle(ligneSynthese1, SYNTHESE_TITRE_POSITION_LIBELLE_1, styleSousTitreLibelle, "N° Contrat :");
        creeCelluleFormulaireValeur(ligneSynthese1, SYNTHESE_TITRE_POSITION_VALEUR_1, styleSousTitreValeur, periode.getNumeroContrat());

        ligneSynthese1.createCell(SYNTHESE_TITRE_POSITION_VALEUR_1 + 1).setCellStyle(styleSousTitreLibelle);
        ligneSynthese1.createCell(SYNTHESE_TITRE_POSITION_VALEUR_1 + 2).setCellStyle(styleSousTitreLibelle);

        creeCelluleFormulaireLibelle(ligneSynthese1, SYNTHESE_TITRE_POSITION_LIBELLE_2, styleSousTitreLibelle, "Début période :");
        creeCelluleFormulaireValeur(ligneSynthese1, SYNTHESE_TITRE_POSITION_VALEUR_2, styleSousTitreValeur,
                DateRedac.formate(FORMAT_DATE, periode.getDateDebutPeriode()));

        ligneSynthese1.createCell(6).setCellStyle(bordureADroite);

        // Nature et fin de période
        Row ligneSynthese2 = synthese.createRow(rowNum++);

        creeCelluleFormulaireLibelle(ligneSynthese2, SYNTHESE_TITRE_POSITION_LIBELLE_1, styleSousTitreLibelle, "Nature :");
        creeCelluleFormulaireValeur(ligneSynthese2, SYNTHESE_TITRE_POSITION_VALEUR_1, styleSousTitreValeur,
                getLibelleEdition(ParamCodeLibelle.TABLE_PERIODE_RECUE, "TYPE", periode.getTypePeriode()));

        ligneSynthese2.createCell(SYNTHESE_TITRE_POSITION_VALEUR_1 + 1).setCellStyle(styleSousTitreLibelle);
        ligneSynthese2.createCell(SYNTHESE_TITRE_POSITION_VALEUR_1 + 2).setCellStyle(styleSousTitreLibelle);

        creeCelluleFormulaireLibelle(ligneSynthese2, SYNTHESE_TITRE_POSITION_LIBELLE_2, styleSousTitreLibelle, "Fin période :");
        creeCelluleFormulaireValeur(ligneSynthese2, SYNTHESE_TITRE_POSITION_VALEUR_2, styleSousTitreValeur,
                DateRedac.formate(FORMAT_DATE, periode.getDateFinPeriode()));

        ligneSynthese2.createCell(6).setCellStyle(bordureADroite);

        // Date de création et délimitation du bloc
        Row ligneSynthese3 = synthese.createRow(rowNum++);

        creeCelluleFormulaireLibelle(ligneSynthese3, SYNTHESE_TITRE_POSITION_LIBELLE_1, styleSousTitreLibelleBordureEnBas, "Date création :");
        creeCelluleFormulaireValeur(ligneSynthese3, SYNTHESE_TITRE_POSITION_VALEUR_1, styleSousTitreValeurBordureEnBas,
                DateRedac.formate(FORMAT_DATE, periode.getDateCreation()));

        for (int i = 2; i < 7; i++) {
            ligneSynthese3.createCell(i).setCellStyle(bordureEnBas);
        }
        ligneSynthese3.createCell(6).setCellStyle(bordureADroiteEtEnBas);

        return rowNum;
    }

    /**
     * Crée les détails sur la période dans l'onglet de synthèse de la déclaration.
     * 
     * @param workbook
     *            le classeur
     * @param synthese
     *            l'onglet de synthèse
     * @param premiereLigne
     *            la ligne sur laquelle commencer l'écriture des détails
     * @return l'entête de l'onglet
     */
    protected int creeDetailsPeriodeOngletSynthese(Workbook workbook, Sheet synthese, int premiereLigne) {
        int rowNum = premiereLigne;
        Contrat contrat = CONTRAT.get();

        // Styles
        CellStyle styleLibelle = creeStyleCelluleParDefaut(workbook, false);
        styleLibelle.setAlignment(CellStyle.ALIGN_RIGHT);

        CellStyle styleValeur = creeStyleCelluleParDefaut(workbook, true);

        // Date de génération
        Row ligneSynthese1 = synthese.createRow(rowNum++);

        creeCelluleFormulaireLibelle(ligneSynthese1, SYNTHESE_POSITION_LIBELLE_1, styleLibelle, "générée le :");
        creeCelluleFormulaireValeur(ligneSynthese1, SYNTHESE_POSITION_VALEUR_1, styleValeur,
                new SimpleDateFormat(FORMAT_DATE).format(Calendar.getInstance().getTime()));

        // Ligne vide
        rowNum++;

        // Compte producteur
        Row ligneSynthese2 = synthese.createRow(rowNum++);

        creeCelluleFormulaireLibelle(ligneSynthese2, SYNTHESE_POSITION_LIBELLE_1, styleLibelle, "Cpte prod :");
        creeCelluleFormulaireValeur(ligneSynthese2, SYNTHESE_POSITION_VALEUR_1, styleValeur,
                contrat == null ? StringUtils.EMPTY : contrat.getNumCompteProducteur());

        // Compte encaissement et raison sociale du client
        Row ligneSynthese3 = synthese.createRow(rowNum++);

        creeCelluleFormulaireLibelle(ligneSynthese3, SYNTHESE_POSITION_LIBELLE_1, styleLibelle, "Cpte encaiss :");
        creeCelluleFormulaireValeur(ligneSynthese3, SYNTHESE_POSITION_VALEUR_1, styleValeur,
                contrat == null ? StringUtils.EMPTY : contrat.getCompteEncaissement());

        creeCelluleFormulaireLibelle(ligneSynthese3, SYNTHESE_POSITION_LIBELLE_2, styleLibelle, "Client :");
        creeCelluleFormulaireValeur(ligneSynthese3, SYNTHESE_POSITION_VALEUR_2, styleValeur,
                contrat == null ? StringUtils.EMPTY : contrat.getClient().getRaisonSociale());

        // Groupe de gestion et numéro de SIREN
        Row ligneSynthese4 = synthese.createRow(rowNum++);

        creeCelluleFormulaireLibelle(ligneSynthese4, SYNTHESE_POSITION_LIBELLE_1, styleLibelle, "Grpe gestion :");
        creeCelluleFormulaireValeur(ligneSynthese4, SYNTHESE_POSITION_VALEUR_1, styleValeur,
                getLibelleEdition(ParamCodeLibelle.TABLE_CONTRAT, "NMGRPGES", contrat == null ? StringUtils.EMPTY : contrat.getGroupeGestion()));

        creeCelluleFormulaireLibelle(ligneSynthese4, SYNTHESE_POSITION_LIBELLE_2, styleLibelle, "SIREN :");
        creeCelluleFormulaireValeur(ligneSynthese4, SYNTHESE_POSITION_VALEUR_2, styleValeur,
                contrat == null ? StringUtils.EMPTY : contrat.getClient().getNumSiren());

        // Famille et NIC
        Row ligneSynthese5 = synthese.createRow(rowNum++);

        creeCelluleFormulaireLibelle(ligneSynthese5, SYNTHESE_POSITION_LIBELLE_1, styleLibelle, "Famille :");
        creeCelluleFormulaireValeur(ligneSynthese5, SYNTHESE_POSITION_VALEUR_1, styleValeur, getLibelleEdition(ParamCodeLibelle.TABLE_CONTRAT,
                "NOFAM", contrat == null ? StringUtils.EMPTY : Integer.toString(contrat.getNumFamilleProduit())));

        creeCelluleFormulaireLibelle(ligneSynthese5, SYNTHESE_POSITION_LIBELLE_2, styleLibelle, "NIC :");
        creeCelluleFormulaireValeur(ligneSynthese5, SYNTHESE_POSITION_VALEUR_2, styleValeur,
                contrat == null ? StringUtils.EMPTY : contrat.getClient().getNumSiret());

        // Ligne vide
        rowNum++;

        return rowNum;
    }

    /**
     * Crée le style de la cellule de titre pour l'onglet de synthèse.
     * 
     * @param workbook
     *            le classeur
     * @return le style de la cellule de titre
     */
    private CellStyle creeStyleCelluleTitreSynthese(Workbook workbook) {
        Font fontTitre = workbook.createFont();
        fontTitre.setFontHeightInPoints(FONT_TAILLE_TITRE);
        fontTitre.setFontName(FONT_NOM);

        CellStyle styleTitre = workbook.createCellStyle();
        styleTitre.setFont(fontTitre);
        styleTitre.setBorderRight(CellStyle.BORDER_THIN);
        styleTitre.setRightBorderColor(IndexedColors.BLACK.getIndex());
        styleTitre.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
        styleTitre.setAlignment(CellStyle.ALIGN_CENTER);
        styleTitre.setFillForegroundColor(IndexedColors.WHITE.getIndex());
        styleTitre.setFillPattern(CellStyle.SOLID_FOREGROUND);

        return styleTitre;
    }

    /**
     * Crée le style de la cellule de titre pour les onglets de liste de données.
     * 
     * @param workbook
     *            le classeur
     * @return le style de la cellule de titre
     */
    protected CellStyle creeStyleCelluleTitreListe(Workbook workbook) {
        Font fontTitre = workbook.createFont();
        fontTitre.setFontHeightInPoints((short) 16);
        fontTitre.setFontName(FONT_NOM);
        CellStyle styleTitre = workbook.createCellStyle();
        styleTitre.setFont(fontTitre);
        return styleTitre;
    }

    /**
     * Crée le style des cellules d'entête de liste de données.
     * 
     * @param workbook
     *            le classeur
     * @param gras
     *            si la police doit être en gras
     * @return le style des cellules d'entête
     */
    protected CellStyle creeStyleCelluleEnteteListe(Workbook workbook, boolean gras) {
        CellStyle styleCellule = creeStyleCelluleParDefaut(workbook, gras);
        styleCellule.setAlignment(CellStyle.ALIGN_CENTER);
        styleCellule.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
        enrichitStyleCelluleAvecToutesBordures(styleCellule);
        styleCellule.setWrapText(true);
        return styleCellule;
    }

    /**
     * Crée une cellule d'entête de liste de données, pouvant s'étaler sur plusieurs colonnes.
     * 
     * @param feuille
     *            la feuille de classeur
     * @param ligne
     *            la ligne d'entête
     * @param idxDebut
     *            l'index de la colonne de début de cellule
     * @param idxFin
     *            l'index de la colonne de fin de cellule
     * @param styleCelluleEntete
     *            le style de la celulle
     * @param texte
     *            le texte de la celulle
     */
    protected void creeCelluleEntete(Sheet feuille, Row ligne, int idxDebut, int idxFin, CellStyle styleCelluleEntete, String texte) {
        creeCellule(ligne, idxDebut, styleCelluleEntete, texte);
        for (int i = idxDebut + 1; i <= idxFin; i++) {
            ligne.createCell(i).setCellStyle(styleCelluleEntete);
        }
        int idxLigne = ligne.getRowNum();
        feuille.addMergedRegion(new CellRangeAddress(idxLigne, idxLigne, idxDebut, idxFin));
    }

}
