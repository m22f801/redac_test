package fr.si2m.red.ihm.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Exception levée pour les requêtes mal formées.
 * 
 * @author nortaina
 *
 */
@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public final class MauvaiseRequeteException extends HttpException {
    /**
     * UID de version.
     */
    private static final long serialVersionUID = 3722714018234263561L;
}
