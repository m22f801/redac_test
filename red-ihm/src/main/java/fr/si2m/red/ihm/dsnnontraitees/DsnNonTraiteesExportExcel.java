package fr.si2m.red.ihm.dsnnontraitees;

import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import fr.si2m.red.reconciliation.*;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import fr.si2m.red.ihm.ExportExcel;
import fr.si2m.red.parametrage.ParamCodeLibelle;
import fr.si2m.red.parametrage.ParamCodeLibelleRepository;
import fr.si2m.red.parametrage.ParamControleSignalRejetRepository;
import fr.si2m.red.parametrage.ParamUtilisateurGestionnaireRepository;
import fr.si2m.red.parametrage.ParamValeurDefautRepository;

/**
 * Export Excel de déclarations.
 * 
 * @author nortaina
 *
 */
@Component
public class DsnNonTraiteesExportExcel extends ExportExcel {
    private static final int CRITERE_POSITION_LIBELLE_1 = 0;
    private static final int CRITERE_POSITION_VALEUR_1 = 1;
    private static final int CRITERE_POSITION_LIBELLE_2 = 3;
    private static final int CRITERE_POSITION_VALEUR_2 = 4;
    private static final int CRITERE_POSITION_LIBELLE_3 = 6;
    private static final int CRITERE_POSITION_VALEUR_3 = 7;

    private static final String CRITERES_JOIN_CHAR = ", ";

    /**
     * Le conteneur de critères de recherche pour le thread courant.
     */
    private static final ThreadLocal<DsnNonTraiteesCriteresRecherche> CRITERES_RECHERCHES = new ThreadLocal<>();

    @Autowired
    private IDsnNonTraiteesRepository iDsnNonTraiteesRepository;
    @Autowired
    private HistoriqueAssignationPeriodeRepository historiqueAssignationPeriodeRepository;
    @Autowired
    private ParamValeurDefautRepository paramValeurDefautRepository;
    @Autowired
    private ParamCodeLibelleRepository paramCodeLibelleRepository;
    @Autowired
    private ParamControleSignalRejetRepository paramControleSignalRejetRepository;
    @Autowired
    private ParamUtilisateurGestionnaireRepository paramUtilisateurGestionnaireRepository;

    /**
     * Map des libelles edition GroupeGestion
     */
    private Map<String, String> mapGroupeGestion = new HashMap<>();

    /**
     * Map des libelles edition NumFamilleProduit
     */
    private Map<String, String> mapNumFamilleProduit = new HashMap<>();

    /**
     * Map des libellés edition VIP
     */
    private Map<String, String> mapVip = new HashMap<>();

    /**
     * Map des libellés type Periode
     */
    private Map<String, String> mapType = new HashMap<>();

    /**
     * Map des libellés mode Paiement
     */
    private Map<String, String> mapModePaiement = new HashMap<>();

    /**
     * Map des libellés état d'une période
     */
    private Map<String, String> mapEtatPeriode = new HashMap<>();

    /**
     * Map des libellés édition Mode Gestion (indicateur DSN)
     */
    private Map<String, String> mapModeGestDSN = new HashMap<>();

    /**
     * Map des libellés édition indicateur Transfert
     */
    private Map<String, String> mapTransfert = new HashMap<>();

    /**
     * Libellé à afficher si aucun résultat
     */
    private static String LIBELLE_INTROUVABLE = "LIBELLE INTROUVABLE";

    /**
     * Libellé à afficher si aucun résultat
     */
    private static String DONNEE_INTROUVABLE = "DONNEE INTROUVABLE";

    /**
     * Libellé à afficher si aucun résultat
     */
    private static String DONNEE_VIDE = "";

    /**
     * Libellé à afficher si aucun résultat numérique
     */
    private static String DONNEE_ZERO = "0 \u20ac";

    /**
     * Libellé à afficher si aucun résultat
     */
    private static String DONNEE_NEANT = "DSN Néant";

    /**
     * Exporte le contenu Excel dans le flux sortant donné.
     * 
     * @param criteresRecherche
     *            les critères de recherche pour les déclarations à exporter
     * @param fluxSortant
     *            le flux sortant
     */
    public void exporte(DsnNonTraiteesCriteresRecherche criteresRecherche, OutputStream fluxSortant) {
        CRITERES_RECHERCHES.set(criteresRecherche);
        super.exporte(fluxSortant);
    }

    @Override
    protected void creeContenuClasseur(Workbook workbook) {
        Assert.notNull(CRITERES_RECHERCHES.get(),
                "Les critères de recherche pour l'export n'ont pas été définis - appeler la méthode #exporte(DsnNonTraiteesCriteresRecherche, OutputStream)");

        // Styles transverses
        CellStyle styleCelluleParDefaut = creeStyleCelluleParDefaut(workbook, false);

        // Onglets
        creeOngletCriteres(workbook, styleCelluleParDefaut);
        creeOngletExtraction(workbook);
    }

    @Override
    protected void creeContenuClasseurMontant(Workbook workbook) {
    }

    @Override
    protected void creeContenuClasseurMessage(Workbook workbook) {
    }

    /**
     * Prépare l'onglet des critères.
     * 
     * @param workbook
     *            le classeur à exporter
     * @param styleCelluleParDefaut
     *            le style par défaut à utiliser pour les cellules
     */
    private void creeOngletCriteres(Workbook workbook, CellStyle styleCelluleParDefaut) {
        Sheet criteres = workbook.createSheet("Critères");

        // Styles
        CellStyle styleLibelle = creeStyleCelluleParDefaut(workbook, false);
        styleLibelle.setAlignment(CellStyle.ALIGN_RIGHT);

        CellStyle styleValeur = creeStyleCelluleParDefaut(workbook, true);

        int rowNum = 0;

        // Titre sur 7 colonnes
        Row ligneTitre = criteres.createRow(rowNum++);
        Cell celluleTitre = ligneTitre.createCell(0);
        CellStyle styleCelluleTitre = creeStyleCelluleTitre(workbook);
        celluleTitre.setCellStyle(styleCelluleTitre);
        celluleTitre.setCellValue("Sélection des DSN non traitées");
        for (int i = 1; i < 8; i++) {
            ligneTitre.createCell(i).setCellStyle(styleCelluleTitre);
        }
        criteres.addMergedRegion(new CellRangeAddress(0, 0, 0, 7));

        // Ligne date de génération
        Row ligneDateGeneration = criteres.createRow(rowNum++);
        Cell celluleLibelleDateGeneration = ligneDateGeneration.createCell(0);
        celluleLibelleDateGeneration.setCellStyle(styleCelluleParDefaut);
        celluleLibelleDateGeneration.setCellValue("générée le :");
        Cell celluleValeurDateGeneration = ligneDateGeneration.createCell(1);
        celluleValeurDateGeneration.setCellStyle(styleValeur);
        celluleValeurDateGeneration.setCellValue(new SimpleDateFormat("dd/MM/yyyy").format(Calendar.getInstance().getTime()));

        // Saut de ligne
        rowNum++;

        // Ligne critères de sélection
        Cell celluleCriteres = criteres.createRow(rowNum++).createCell(0);
        celluleCriteres.setCellStyle(creeStyleCelluleSousTitre(workbook));
        celluleCriteres.setCellValue("Critères de sélection :");

        // Critères
        DsnNonTraiteesCriteresRecherche criteresRecherche = CRITERES_RECHERCHES.get();

        Row ligneCriteres1 = criteres.createRow(rowNum++);
        creeCelluleFormulaireLibelle(ligneCriteres1, CRITERE_POSITION_LIBELLE_1, styleLibelle, "Etat :");
        creeCelluleFormulaireValeur(ligneCriteres1, CRITERE_POSITION_VALEUR_1, styleValeur,
                getLibellesEdition(criteresRecherche.getStatutsAsList(), ParamCodeLibelle.TABLE_ADHESION_ETAB_MOIS, "ETAT"));

        creeCelluleFormulaireLibelle(ligneCriteres1, CRITERE_POSITION_LIBELLE_2, styleLibelle, "Grpe gestion :");
        creeCelluleFormulaireValeur(ligneCriteres1, CRITERE_POSITION_VALEUR_2, styleValeur,
                getLibellesEdition(criteresRecherche.getGroupesGestionAsList(), ParamCodeLibelle.TABLE_CONTRAT, "NMGRPGES"));

        creeCelluleFormulaireLibelle(ligneCriteres1, CRITERE_POSITION_LIBELLE_3, styleLibelle, "Siren :");
        creeCelluleFormulaireValeur(ligneCriteres1, CRITERE_POSITION_VALEUR_3, styleValeur, criteresRecherche.getSirenEntreprise());

        Row ligneCriteres2 = criteres.createRow(rowNum++);
        creeCelluleFormulaireLibelle(ligneCriteres2, CRITERE_POSITION_LIBELLE_1, styleLibelle, "Contrat :");
        creeCelluleFormulaireValeur(ligneCriteres2, CRITERE_POSITION_VALEUR_1, styleValeur, criteresRecherche.getNumContrat());

        creeCelluleFormulaireLibelle(ligneCriteres2, CRITERE_POSITION_LIBELLE_2, styleLibelle, "Famille :");
        creeCelluleFormulaireValeur(ligneCriteres2, CRITERE_POSITION_VALEUR_2, styleValeur,
                getLibellesEdition(criteresRecherche.getFamillesAsList(), ParamCodeLibelle.TABLE_CONTRAT, "NOFAM"));

        creeCelluleFormulaireLibelle(ligneCriteres2, CRITERE_POSITION_LIBELLE_3, styleLibelle, "Nic :");
        creeCelluleFormulaireValeur(ligneCriteres2, CRITERE_POSITION_VALEUR_3, styleValeur, criteresRecherche.getNicEtablissement());

        Row ligneCriteres3 = criteres.createRow(rowNum++);
        creeCelluleFormulaireLibelle(ligneCriteres3, CRITERE_POSITION_LIBELLE_1, styleLibelle, "Mois rattachement :");

        if (criteresRecherche.getMoisRattachement() != null && !criteresRecherche.getMoisRattachement().isEmpty()) {
            creeCelluleFormulaireValeur(ligneCriteres3, CRITERE_POSITION_VALEUR_1, styleValeur, criteresRecherche.getMoisRattachement());
        } else {
            creeCelluleFormulaireValeur(ligneCriteres3, CRITERE_POSITION_VALEUR_1, styleValeur, "");
        }

        creeCelluleFormulaireLibelle(ligneCriteres3, CRITERE_POSITION_LIBELLE_2, styleLibelle, "Cpt enc :");
        creeCelluleFormulaireValeur(ligneCriteres3, CRITERE_POSITION_VALEUR_2, styleValeur, criteresRecherche.getCptEnc());

        creeCelluleFormulaireLibelle(ligneCriteres3, CRITERE_POSITION_LIBELLE_3, styleLibelle, "Raison soc. :");
        creeCelluleFormulaireValeur(ligneCriteres3, CRITERE_POSITION_VALEUR_3, styleValeur, criteresRecherche.getRaisonSociale());

        Row ligneCriteres4 = criteres.createRow(rowNum++);
        creeCelluleFormulaireLibelle(ligneCriteres4, CRITERE_POSITION_LIBELLE_1, styleLibelle, "Versement :");
        List<Boolean> versements = criteresRecherche.getVersementsAsList();
        creeCelluleFormulaireValeur(ligneCriteres4, CRITERE_POSITION_VALEUR_1, styleValeur, getLibellesVersements(versements));

        creeCelluleFormulaireLibelle(ligneCriteres4, CRITERE_POSITION_LIBELLE_2, styleLibelle, "Cpt prod :");
        creeCelluleFormulaireValeur(ligneCriteres4, CRITERE_POSITION_VALEUR_2, styleValeur, criteresRecherche.getCptProd());

        for (int col = 0; col < 8; col++) {
            criteres.autoSizeColumn(col);
        }
    }

    /**
     * Récupère les libellés d'édition pour les critères sur versement.
     * 
     * @param versements
     *            les codes de versement
     * @return les libellés correspondant
     */
    private List<String> getLibellesVersements(List<Boolean> versements) {
        List<String> libellesVersements = new ArrayList<>(versements.size());
        if (versements.contains(Boolean.FALSE)) {
            libellesVersements.add("Aucun");
        }
        if (versements.contains(Boolean.TRUE)) {
            libellesVersements.add("Au moins 1");
        }
        return libellesVersements;
    }

    /**
     * Crée une cellule pour une multi-valeur de formulaire.
     * 
     * @param ligneFormulaire
     *            la ligne du formulaire à remplir
     * @param positionValeur
     *            la position de la valeur dans la ligne
     * @param styleValeur
     *            le style du libellé
     * @param multiValeur
     *            les valeurs du critère
     */
    private void creeCelluleFormulaireValeur(Row ligneFormulaire, int positionValeur, CellStyle styleValeur, List<?> multiValeur) {
        creeCelluleFormulaireValeur(ligneFormulaire, positionValeur, styleValeur, StringUtils.join(multiValeur, CRITERES_JOIN_CHAR));
    }

    private CellStyle creeStyleCelluleTitre(Workbook workbook) {
        Font fontTitre = workbook.createFont();
        fontTitre.setFontHeightInPoints(FONT_TAILLE_TITRE);
        fontTitre.setFontName(FONT_NOM);

        CellStyle styleTitre = workbook.createCellStyle();
        styleTitre.setFont(fontTitre);
        styleTitre.setBorderBottom(CellStyle.BORDER_THIN);
        styleTitre.setBottomBorderColor(IndexedColors.BLACK.getIndex());
        styleTitre.setBorderRight(CellStyle.BORDER_THIN);
        styleTitre.setRightBorderColor(IndexedColors.BLACK.getIndex());
        styleTitre.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
        styleTitre.setAlignment(CellStyle.ALIGN_CENTER);
        return styleTitre;
    }

    private CellStyle creeStyleCelluleSousTitre(Workbook workbook) {
        Font fontSousTitre = workbook.createFont();
        fontSousTitre.setFontHeightInPoints(FONT_TAILLE_SOUS_TITRE);
        fontSousTitre.setFontName(FONT_NOM);
        fontSousTitre.setUnderline(Font.U_SINGLE);

        CellStyle styleSousTitre = workbook.createCellStyle();
        styleSousTitre.setFont(fontSousTitre);
        return styleSousTitre;
    }

    private void creeOngletExtraction(Workbook workbook) {
        Sheet extraction = workbook.createSheet("Extraction");

        // Styles
        CellStyle styleCellule = creeStyleCelluleParDefaut(workbook, false);
        enrichitStyleCelluleAvecToutesBordures(styleCellule);
        styleCellule.setWrapText(true);

        DsnNonTraiteesCriteresRecherche criteresRecherche = CRITERES_RECHERCHES.get();
        int rowNum = 0;

        //Entête
        Row ligneEntete = extraction.createRow(rowNum++);
        remplitLigneEntete(ligneEntete, styleCellule);

        for (DsnNonTraitees dsn : iDsnNonTraiteesRepository.rechercheDsnNonTraiteesExportExcel(criteresRecherche)) {
            Row ligneDsn = extraction.createRow(rowNum++);
            remplitLigneDsnNonTraitee(ligneDsn, dsn, styleCellule);
        }

        // nombre de caractères à afficher par colonnes
        int[] widthArray = { 40, 40, 15, 16, 8, 7, 12, 12, 40, 12, 12, 40, 12, 40, 13, 13, 13, 18, 12, 13, 13, 40, 30 };

        redimensionnementColonnes(extraction, widthArray);

        extraction.setAutoFilter(new CellRangeAddress(0, 0, 0, 26));
    }

    /**
     * Remplit les cases d'une ligne entête.
     * @param ligneEntete La ligne d'entête
     * @param styleCellule Le style à appliquer à chaque case
     */
    private void remplitLigneEntete(Row ligneEntete, CellStyle styleCellule) {
        int cellNum = 0;
        creeCellule(ligneEntete, cellNum++, styleCellule, "Grpe Gest.");
        creeCellule(ligneEntete, cellNum++, styleCellule, "Famille");
        creeCellule(ligneEntete, cellNum++, styleCellule, "Contrat");
        creeCellule(ligneEntete, cellNum++, styleCellule, "Date situation contrat");
        creeCellule(ligneEntete, cellNum++, styleCellule, "Cpt prod");
        creeCellule(ligneEntete, cellNum++, styleCellule, "Cpt enc");
        creeCellule(ligneEntete, cellNum++, styleCellule, "SIREN Emetteur");
        creeCellule(ligneEntete, cellNum++, styleCellule, "NIC Emetteur");
        creeCellule(ligneEntete, cellNum++, styleCellule, "Nom Emetteur");
        creeCellule(ligneEntete, cellNum++, styleCellule, "SIREN Entreprise");
        creeCellule(ligneEntete, cellNum++, styleCellule, "NIC Entreprise");
        creeCellule(ligneEntete, cellNum++, styleCellule, "Raison sociale Entreprise");
        creeCellule(ligneEntete, cellNum++, styleCellule, "NIC Etab");
        creeCellule(ligneEntete, cellNum++, styleCellule, "Enseigne Etab");
        creeCellule(ligneEntete, cellNum++, styleCellule, "Mois déclaré");
        creeCellule(ligneEntete, cellNum++, styleCellule, "Mois rattachement");
        creeCellule(ligneEntete, cellNum++, styleCellule, "Date fichier");
        creeCellule(ligneEntete, cellNum++, styleCellule, "Date intégration REDAC");
        creeCellule(ligneEntete, cellNum++, styleCellule, "Nb individus");
        creeCellule(ligneEntete, cellNum++, styleCellule, "Mt cotisation");
        creeCellule(ligneEntete, cellNum++, styleCellule, "Montant verst");
        creeCellule(ligneEntete, cellNum++, styleCellule, "Mode paiemt");
        creeCellule(ligneEntete, cellNum, styleCellule, "Etat");
    }

    /**
     * Remplit les cases d'une ligne de DSN non traitée.
     * @param lignePeriode La ligne a remplir
     * @param dsnNonTraitee La DSN non traitée correspondante a la ligne
     * @param styleCellule Le style de cellule a appliquer pour la ligne
     */
    private void remplitLigneDsnNonTraitee(Row lignePeriode, DsnNonTraitees dsnNonTraitee, CellStyle styleCellule) {
        int cellNum = 0;

        if (dsnNonTraitee.getGroupeGestion() != null) {
            creeCellule(lignePeriode, cellNum++, styleCellule, dsnNonTraitee.getGroupeGestion());
        } else {
            creeCellule(lignePeriode, cellNum++, styleCellule, DONNEE_VIDE);
        }

        if (dsnNonTraitee.getFamille() != null) {
            creeCellule(lignePeriode, cellNum++, styleCellule, dsnNonTraitee.getFamille());
        } else {
            creeCellule(lignePeriode, cellNum++, styleCellule, DONNEE_VIDE);
        }

        if (dsnNonTraitee.getReferenceContrat() != null) {
            creeCellule(lignePeriode, cellNum++, styleCellule, dsnNonTraitee.getReferenceContrat());
        } else {
            creeCellule(lignePeriode, cellNum++, styleCellule, DONNEE_INTROUVABLE);
        }

        if (dsnNonTraitee.getDateSituation() != null) {
            creeCellule(lignePeriode, cellNum++, styleCellule, dsnNonTraitee.getDateSituation());
        } else {
            creeCellule(lignePeriode, cellNum++, styleCellule, DONNEE_VIDE);
        }

        if (dsnNonTraitee.getNcprod() != null) {
            creeCellule(lignePeriode, cellNum++, styleCellule, dsnNonTraitee.getNcprod());
        } else {
            creeCellule(lignePeriode, cellNum++, styleCellule, DONNEE_VIDE);
        }

        if (dsnNonTraitee.getNcenc() != null) {
            creeCellule(lignePeriode, cellNum++, styleCellule, dsnNonTraitee.getNcenc());
        } else {
            creeCellule(lignePeriode, cellNum++, styleCellule, DONNEE_VIDE);
        }

        if (dsnNonTraitee.getSirenEmetteur() != null) {
            creeCellule(lignePeriode, cellNum++, styleCellule, dsnNonTraitee.getSirenEmetteur());
        } else {
            creeCellule(lignePeriode, cellNum++, styleCellule, DONNEE_INTROUVABLE);
        }

        if (dsnNonTraitee.getNicEmetteur() != null) {
            creeCellule(lignePeriode, cellNum++, styleCellule, dsnNonTraitee.getNicEmetteur());
        } else {
            creeCellule(lignePeriode, cellNum++, styleCellule, DONNEE_INTROUVABLE);
        }

        if (dsnNonTraitee.getNomEmetteur() != null) {
            creeCellule(lignePeriode, cellNum++, styleCellule, dsnNonTraitee.getNomEmetteur());
        } else {
            creeCellule(lignePeriode, cellNum++, styleCellule, DONNEE_INTROUVABLE);
        }

        if (dsnNonTraitee.getSirenEntreprise() != null) {
            creeCellule(lignePeriode, cellNum++, styleCellule, dsnNonTraitee.getSirenEntreprise());
        } else {
            creeCellule(lignePeriode, cellNum++, styleCellule, DONNEE_INTROUVABLE);
        }

        if (dsnNonTraitee.getNicEntreprise() != null) {
            creeCellule(lignePeriode, cellNum++, styleCellule, dsnNonTraitee.getNicEntreprise());
        } else {
            creeCellule(lignePeriode, cellNum++, styleCellule, DONNEE_INTROUVABLE);
        }

        if (dsnNonTraitee.getRaisonSocialeEntreprise() != null) {
            creeCellule(lignePeriode, cellNum++, styleCellule, dsnNonTraitee.getRaisonSocialeEntreprise());
        } else {
            creeCellule(lignePeriode, cellNum++, styleCellule, DONNEE_INTROUVABLE);
        }

        if (dsnNonTraitee.getNicEtablissement() != null) {
            creeCellule(lignePeriode, cellNum++, styleCellule, dsnNonTraitee.getNicEtablissement());
        } else {
            creeCellule(lignePeriode, cellNum++, styleCellule, DONNEE_INTROUVABLE);
        }

        if (dsnNonTraitee.getEnseigneEtablissement() != null) {
            creeCellule(lignePeriode, cellNum++, styleCellule, dsnNonTraitee.getEnseigneEtablissement());
        } else {
            creeCellule(lignePeriode, cellNum++, styleCellule, DONNEE_INTROUVABLE);
        }

        if (dsnNonTraitee.getMoisDeclaration() != null) {
            creeCellule(lignePeriode, cellNum++, styleCellule, dsnNonTraitee.getMoisDeclaration());
        } else {
            creeCellule(lignePeriode, cellNum++, styleCellule, DONNEE_INTROUVABLE);
        }

        if (dsnNonTraitee.getMoisRattachement() != null) {
            creeCellule(lignePeriode, cellNum++, styleCellule, dsnNonTraitee.getMoisRattachement());
        } else {
            creeCellule(lignePeriode, cellNum++, styleCellule, DONNEE_INTROUVABLE);
        }

        if (dsnNonTraitee.getDateFichier() != null) {
            creeCellule(lignePeriode, cellNum++, styleCellule, dsnNonTraitee.getDateFichier());
        } else {
            creeCellule(lignePeriode, cellNum++, styleCellule, DONNEE_INTROUVABLE);
        }

        if (dsnNonTraitee.getDateCreation() != null) {
            creeCellule(lignePeriode, cellNum++, styleCellule, dsnNonTraitee.getDateCreation());
        } else {
            creeCellule(lignePeriode, cellNum++, styleCellule, DONNEE_INTROUVABLE);
        }

        if (dsnNonTraitee.getNombreIndividu() != null && !DsnNonTraitees.DSN_NEANT.equals(dsnNonTraitee.getNombreIndividu())) {
            creeCellule(lignePeriode, cellNum++, styleCellule, Double.parseDouble(dsnNonTraitee.getNombreIndividu()));
        } else {
            creeCellule(lignePeriode, cellNum++, styleCellule, DONNEE_NEANT);
        }

        if (dsnNonTraitee.getMontantCotisation() != null && !DsnNonTraitees.DSN_NEANT.equals(dsnNonTraitee.getMontantCotisation())) {
            if (dsnNonTraitee.getMontantCotisation().equals("0.00")) { // Important, car pour excel une valeur à 0 est équivalent à "" et l'extraction n'affiche par 0 mais ""
                creeCellule(lignePeriode, cellNum++, styleCellule, DONNEE_ZERO);
            } else {
                creeCellule(lignePeriode, cellNum++, styleCellule, Double.parseDouble(dsnNonTraitee.getMontantCotisation()));
            }
        } else {
            if (dsnNonTraitee.getMontantCotisation() != null && dsnNonTraitee.getNombreIndividu() != null && !DsnNonTraitees.DSN_NEANT.equals(dsnNonTraitee.getNombreIndividu())) {
                creeCellule(lignePeriode, cellNum++, styleCellule, DONNEE_ZERO);
            } else {
                creeCellule(lignePeriode, cellNum++, styleCellule, DONNEE_NEANT);
            }
        }

        if (dsnNonTraitee.getMontantVersement() != null && dsnNonTraitee.getMontantVersement() != 0) {
            creeCellule(lignePeriode, cellNum++, styleCellule, dsnNonTraitee.getMontantVersement());
        } else {
            creeCellule(lignePeriode, cellNum++, styleCellule, DONNEE_ZERO);
        }

        if (dsnNonTraitee.getModePaiement() != null) {
            creeCellule(lignePeriode, cellNum++, styleCellule, dsnNonTraitee.getModePaiement());
        } else {
            creeCellule(lignePeriode, cellNum++, styleCellule, DONNEE_VIDE);
        }

        if (dsnNonTraitee.getEtat() != null) {
            creeCellule(lignePeriode, cellNum++, styleCellule, dsnNonTraitee.getEtat());
        } else {
            creeCellule(lignePeriode, cellNum++, styleCellule, DONNEE_INTROUVABLE);
        }
    }
}
