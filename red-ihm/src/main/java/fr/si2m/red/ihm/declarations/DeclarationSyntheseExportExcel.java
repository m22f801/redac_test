package fr.si2m.red.ihm.declarations;

import java.awt.Color;
import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.DataFormat;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFColor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import fr.si2m.red.DateRedac;
import fr.si2m.red.contrat.TarifRepository;
import fr.si2m.red.dsn.BaseAssujettieRepository;
import fr.si2m.red.parametrage.Tranche;
import fr.si2m.red.reconciliation.CategorieQuittancementIndividuRepository;
import fr.si2m.red.reconciliation.EffectifCategorieContratTravailRepository;
import fr.si2m.red.reconciliation.EffectifCategorieMouvementRepository;
import fr.si2m.red.reconciliation.PeriodeRecue;
import fr.si2m.red.reconciliation.RattachementDeclarationsRecuesRepository;
import fr.si2m.red.reconciliation.ResumeDeclarationIndividuExcel;
import fr.si2m.red.reconciliation.ResumeDecompteEffectifExcel;
import fr.si2m.red.reconciliation.TrancheCategorieBaseAssujettieRepository;
import fr.si2m.red.reconciliation.TrancheCategorieRepository;

/**
 * Export Excel de la synthèse d'une déclaration.
 * 
 * @author nortaina, eudesr
 *
 */
@Component
public class DeclarationSyntheseExportExcel extends DeclarationExportExcel {

    private static final String MONTANTS_FORMAT = "# ### ##0.00 €;-# ### ##0.00 €";

    @Autowired
    private TarifRepository tarifRepository;
    @Autowired
    private RattachementDeclarationsRecuesRepository rattachementDeclarationsRecuesRepository;
    @Autowired
    private CategorieQuittancementIndividuRepository categorieQuittancementIndividuRepository;
    @Autowired
    private EffectifCategorieMouvementRepository effectifCategorieMouvementRepository;
    @Autowired
    private TrancheCategorieRepository trancheCategorieRepository;
    @Autowired
    private TrancheCategorieBaseAssujettieRepository trancheCategorieBaseAssujettieRepository;
    @Autowired
    private EffectifCategorieContratTravailRepository effectifCategorieContratTravailRepository;
    @Autowired
    private BaseAssujettieRepository baseAssujettieRepository;

    @Override
    protected void creeContenuClasseur(Workbook workbook) {
        // Paramètres période
        PeriodeRecue periode = PERIODE.get();
        Assert.notNull(periode, "La période pour l'export n'a pas été définie - appeler la méthode #exporte(PeriodeRecue, OutputStream)");

        Integer natureContrat = tarifRepository.getNatureContratPourPeriode(periode.getNumeroContrat(), periode.getDateDebutPeriode(),
                periode.getDateFinPeriode());

        // Styles
        CellStyle styleTitre = creeStyleCelluleTitreListe(workbook);

        CellStyle styleCellulesEntete = creeStyleCelluleEnteteListe(workbook, false);

        CellStyle styleCelluleParDefaut = creeStyleCelluleParDefaut(workbook, false);
        enrichitStyleCelluleAvecToutesBordures(styleCelluleParDefaut);
        styleCelluleParDefaut.setWrapText(true);

        // Onglets
        creeOngletSynthese(workbook, natureContrat);

        if (natureContrat != null && natureContrat == 1) {
            creeOngletDecompteEffectifs(workbook, styleTitre, styleCellulesEntete, styleCelluleParDefaut);
        }

        creeOngletDetailsBaseParIndividu(workbook, styleTitre, styleCellulesEntete, styleCelluleParDefaut);
    }

    @Override
    protected void creeContenuClasseurMontant(Workbook workbook) {
    }

    @Override
    protected void creeContenuClasseurMessage(Workbook workbook) {
    }

    /**
     * Crée l'onglet de synthèse de la période.
     * 
     * @param workbook
     *            le classeur
     * @param natureContrat
     *            le mode de calcul de cotisation pour la période à exporter
     */
    private void creeOngletSynthese(Workbook workbook, Integer natureContrat) {
        Sheet synthese = workbook.createSheet("Synthèse");
        Long idPeriode = PERIODE.get().getIdPeriode();

        int rowNum = creeEnteteOngletSynthese(workbook, synthese);
        rowNum = creeDetailsPeriodeOngletSynthese(workbook, synthese, rowNum);

        // Saut de ligne
        rowNum++;

        // Styles
        CellStyle styleSousTitre = creeStyleCelluleSousTitre(workbook, false);

        CellStyle styleNumCategorie = creeStyleCelluleTableauConsolidation(workbook, false, new Color(203, 203, 203), CellStyle.ALIGN_CENTER);
        CellStyle styleTitreNumCategorie = creeStyleCelluleTableauConsolidation(workbook, false, new Color(203, 203, 203), CellStyle.ALIGN_RIGHT);

        CellStyle styleMontants = creeStyleCelluleTableauConsolidation(workbook, false, new Color(231, 231, 231), CellStyle.ALIGN_RIGHT);
        DataFormat cf = workbook.createDataFormat();
        styleMontants.setDataFormat(cf.getFormat(MONTANTS_FORMAT));

        CellStyle styleMontantsDetails = creeStyleCelluleTableauConsolidation(workbook, true, new Color(231, 231, 231), CellStyle.ALIGN_RIGHT);
        styleMontantsDetails.setDataFormat(cf.getFormat(MONTANTS_FORMAT));

        CellStyle styleValeurs = creeStyleCelluleTableauConsolidation(workbook, false, new Color(221, 217, 195), CellStyle.ALIGN_RIGHT);
        CellStyle styleValeursDetails = creeStyleCelluleTableauConsolidation(workbook, true, new Color(221, 217, 195), CellStyle.ALIGN_RIGHT);

        // Tableaux
        List<String> numerosCategorieQuittancement = categorieQuittancementIndividuRepository.getNumerosCategorieDistinctsPourPeriode(idPeriode);
        boolean existeNoCat = baseAssujettieRepository.existeBaseAssujettiePourPeriode(idPeriode);

        if (existeNoCat) {

            if (natureContrat != null && natureContrat == 1) {
                // Sur effectif
                creeCellule(synthese.createRow(rowNum++), 0, styleSousTitre, "Consolidation des déclarations sur effectif :");
            } else if (natureContrat != null && natureContrat == 2) {
                // Sur salaire
                creeCellule(synthese.createRow(rowNum++), 0, styleSousTitre, "Consolidation des déclarations sur salaire :");
            }
            synthese.addMergedRegion(new CellRangeAddress(rowNum - 1, rowNum - 1, 0, 1));

            Row ligneEntete = synthese.createRow(rowNum++);
            int idxEntete = 0;
            creeCellule(ligneEntete, idxEntete++, styleTitreNumCategorie, "Population :");
            for (String numCategorie : numerosCategorieQuittancement) {
                creeCellule(ligneEntete, idxEntete++, styleNumCategorie, numCategorie);
            }
            creeCellule(ligneEntete, idxEntete++, styleNumCategorie, "Total");

            // Cotisations
            List<Double> montantsCotisations = categorieQuittancementIndividuRepository
                    .getSommesMontantsCotisationPourCategoriesQuittancement(idPeriode, numerosCategorieQuittancement);
            Double montantTotalCotisations = categorieQuittancementIndividuRepository.getSommeMontantsCotisationDeclare(idPeriode);

            Row ligneMontantCalcule = synthese.createRow(rowNum++);
            int idxMontantCalcule = 0;
            creeCellule(ligneMontantCalcule, idxMontantCalcule++, styleMontants, "Montant déclaré cotisation :");

            for (Double montantCotisation : montantsCotisations) {
                creeCellule(ligneMontantCalcule, idxMontantCalcule++, styleMontantsDetails, montantCotisation != null ? montantCotisation : 0, true);
            }
            creeCellule(ligneMontantCalcule, idxMontantCalcule++, styleMontants, montantTotalCotisations != null ? montantTotalCotisations : 0, true);

            if (natureContrat != null && natureContrat == 1) {
                // Sur effectif : effectifs et mouvements d'effectifs
                creeConsolidationSurEffectif(workbook, synthese, rowNum, idPeriode, numerosCategorieQuittancement, styleValeurs, styleValeursDetails);
            } else if (natureContrat != null && natureContrat == 2) {
                // Sur salaire : montants par tranche
                creeConsolidationSurSalaire(synthese, rowNum, idPeriode, numerosCategorieQuittancement, styleMontants, styleMontantsDetails);
            }

        }
        synthese.autoSizeColumn(0);
        for (int col = 1; col < 20; col++) {
            synthese.setColumnWidth(col, 4000);
        }

    }

    /**
     * Crée la partie consolidation sur effectifs dans l'onglet de synthèse.
     * 
     * @param workbook
     *            le classeur Excel
     * @param synthese
     *            la feuille de synthèse
     * @param premiereLigne
     *            le numéro de ligne de départ de la partie
     * @param idPeriode
     *            l'identifiant de période
     * @param numerosCategorieQuittancement
     *            les numéros de catégorie de quittancement
     * @param styleValeurs
     *            le style pour l'affichage des valeurs
     * @param styleValeursDetails
     *            les style pour l'affichage des détails de valeurs
     */
    private void creeConsolidationSurEffectif(Workbook workbook, Sheet synthese, int premiereLigne, Long idPeriode,
            List<String> numerosCategorieQuittancement, CellStyle styleValeurs, CellStyle styleValeursDetails) {
        int rowNum = premiereLigne;

        // Styles
        CellStyle styleMouvements = creeStyleCelluleTableauConsolidation(workbook, false, new Color(220, 230, 242), CellStyle.ALIGN_RIGHT);
        CellStyle styleMouvementsDetails = creeStyleCelluleTableauConsolidation(workbook, true, new Color(220, 230, 242), CellStyle.ALIGN_RIGHT);

        // Sur effectif : effectifs et mouvements d'effectifs
        List<Long> effectifsDebut = categorieQuittancementIndividuRepository.getEffectifsDebutPourPeriodeEtCategoriesQuittancement(idPeriode,
                numerosCategorieQuittancement);
        Long effectifTotalDebut = categorieQuittancementIndividuRepository.getEffectifTotalDebutPeriode(idPeriode);

        Row ligneEffectifsDebut = synthese.createRow(rowNum++);
        int idxEffectifsDebut = 0;
        creeCellule(ligneEffectifsDebut, idxEffectifsDebut++, styleValeurs, "Effectifs calculés Début :");
        for (Long effectifDebut : effectifsDebut) {
            creeCellule(ligneEffectifsDebut, idxEffectifsDebut++, styleValeursDetails, (double) effectifDebut, true);
        }
        creeCellule(ligneEffectifsDebut, idxEffectifsDebut++, styleValeurs, (double) effectifTotalDebut, true);

        List<Integer> datesEffetDistinctes = effectifCategorieMouvementRepository.getDatesEffetDistinctesPourPeriode(idPeriode);
        for (Integer dateEffet : datesEffetDistinctes) {
            Row ligneMouvement = synthese.createRow(rowNum++);
            int idxMouvement = 0;
            creeCellule(ligneMouvement, idxMouvement++, styleMouvements,
                    "Mouvts calculés au " + DateRedac.convertionJourMoisDateRedacSansValidation(dateEffet));
            Map<String, Integer> mouvementsParNumCategorie = effectifCategorieMouvementRepository
                    .getNombresMouvementsPourDateEffetEtCategoriesQuittancement(idPeriode, dateEffet, numerosCategorieQuittancement);
            for (String numCategorie : numerosCategorieQuittancement) {
                creeCellule(ligneMouvement, idxMouvement++, styleMouvementsDetails, (double) mouvementsParNumCategorie.get(numCategorie), true);
            }
            creeCellule(ligneMouvement, idxMouvement++, styleMouvements,
                    (double) effectifCategorieMouvementRepository.getNombreTotalMouvementsPourDateEffet(idPeriode, dateEffet), true);
        }

        List<Long> effectifsFin = categorieQuittancementIndividuRepository.getEffectifsFinPourPeriodeEtCategoriesQuittancement(idPeriode,
                numerosCategorieQuittancement);
        Long effectifTotalFin = categorieQuittancementIndividuRepository.getEffectifTotalFinPeriode(idPeriode);

        Row ligneEffectifsFin = synthese.createRow(rowNum++);
        int idxEffectifsFin = 0;
        creeCellule(ligneEffectifsFin, idxEffectifsFin++, styleValeurs, "Effectifs calculés Fin :");
        for (Long effectifFin : effectifsFin) {
            creeCellule(ligneEffectifsFin, idxEffectifsFin++, styleValeursDetails, (double) effectifFin, true);
        }
        creeCellule(ligneEffectifsFin, idxEffectifsFin++, styleValeurs, (double) effectifTotalFin, true);
    }

    /**
     * Crée la partie consolidation sur salaires dans l'onglet de synthèse.
     * 
     * @param synthese
     *            la feuille de synthèse
     * @param premiereLigne
     *            le numéro de ligne de départ de la partie
     * @param idPeriode
     *            l'identifiant de période
     * @param numerosCategorieQuittancement
     *            les numéros de catégorie de quittancement
     * @param styleValeurs
     *            le style pour l'affichage des valeurs
     * @param styleValeursDetails
     *            les style pour l'affichage des détails de valeurs
     */
    private void creeConsolidationSurSalaire(Sheet synthese, int premiereLigne, Long idPeriode, List<String> numerosCategorieQuittancement,
            CellStyle styleValeurs, CellStyle styleValeursDetails) {
        int rowNum = premiereLigne;

        // Sur salaire : montants par tranche
        List<Tranche> tranchesDistinctes = trancheCategorieRepository.getTranchesDistinctesPourPeriodeTrieesParNumero(idPeriode);
        for (Tranche infoTrancheDistincte : tranchesDistinctes) {
            Row ligneTranche = synthese.createRow(rowNum++);
            int idxTranche = 0;
            Integer numTranche = infoTrancheDistincte.getNumTranche();
            String libelleTranche = infoTrancheDistincte.getLibelleTranche();
            creeCellule(ligneTranche, idxTranche++, styleValeurs, numTranche + ". " + libelleTranche);
            Map<String, Double> montantsTranchesParNumCategorie = trancheCategorieRepository
                    .getSommesMontantsTranchePourCategoriesQuittancement(idPeriode, numTranche, libelleTranche, numerosCategorieQuittancement);
            for (String numCategorie : numerosCategorieQuittancement) {
                Double money = montantsTranchesParNumCategorie.get(numCategorie);
                money = money != null ? money : Double.valueOf(0d);
                creeCellule(ligneTranche, idxTranche++, styleValeursDetails, money, true);
            }
            Double montantTot = trancheCategorieRepository.getSommesMontantsTranche(idPeriode, numTranche, libelleTranche);
            if (montantTot != null) {
                creeCellule(ligneTranche, idxTranche++, styleValeursDetails, montantTot, true);
            }

        }
    }

    /**
     * Crée un style spécifique au tableau de consolidation.
     * 
     * @param workbook
     *            le classeur Excel
     * @param gras
     *            si le style est en gras ou non
     * @param couleurDeFond
     *            la couleur de fond
     * @param alignement
     *            l'alignement du texte
     * 
     * @return le style créé pour le classeur
     */
    private CellStyle creeStyleCelluleTableauConsolidation(Workbook workbook, boolean gras, Color couleurDeFond, short alignement) {
        XSSFCellStyle style = (XSSFCellStyle) creeStyleCelluleParDefaut(workbook, gras);
        style.setFillForegroundColor(new XSSFColor(couleurDeFond));
        style.setFillPattern(CellStyle.SOLID_FOREGROUND);
        enrichitStyleCelluleAvecToutesBordures(style);
        style.setAlignment(alignement);
        return style;
    }

    private void creeOngletDecompteEffectifs(Workbook workbook, CellStyle styleCelluleTitre, CellStyle styleCelluleEntete,
            CellStyle styleCelluleParDefaut) {
        Sheet decompteEffectifs = workbook.createSheet("Décompte Effectifs");

        int rowNum = 0;

        // Titre
        Cell celluleTitre = decompteEffectifs.createRow(rowNum++).createCell(0);
        celluleTitre.setCellStyle(styleCelluleTitre);
        celluleTitre.setCellValue("Détail des Effectifs décomptés par catégorie");
        decompteEffectifs.addMergedRegion(new CellRangeAddress(0, 0, 0, 10));

        // Saut de ligne
        rowNum++;

        // Première ligne entête
        Row premiereLigneEntete = decompteEffectifs.createRow(rowNum++);
        creeCelluleEntete(decompteEffectifs, premiereLigneEntete, 0, 4, styleCelluleEntete, "Adhésion");
        creeCelluleEntete(decompteEffectifs, premiereLigneEntete, 5, 12, styleCelluleEntete, "Individu");
        creeCelluleEntete(decompteEffectifs, premiereLigneEntete, 13, 16, styleCelluleEntete, "Contrat travail");
        creeCelluleEntete(decompteEffectifs, premiereLigneEntete, 17, 24, styleCelluleEntete, "Décompte des effectifs");

        // Seconde ligne entête
        Row secondeLigneEntete = decompteEffectifs.createRow(rowNum++);
        int idxColEntete = 0;
        creeCellule(secondeLigneEntete, idxColEntete++, styleCelluleEntete, "Date constit.\n05.007");
        creeCellule(secondeLigneEntete, idxColEntete++, styleCelluleEntete, "Mois rattach.");
        creeCellule(secondeLigneEntete, idxColEntete++, styleCelluleEntete, "Mois déclar.\n05.005");
        creeCellule(secondeLigneEntete, idxColEntete++, styleCelluleEntete, "SIREN\n06.001");
        creeCellule(secondeLigneEntete, idxColEntete++, styleCelluleEntete, "NIC\n11.001");
        creeCellule(secondeLigneEntete, idxColEntete++, styleCelluleEntete, "NIR\n30.001");
        creeCellule(secondeLigneEntete, idxColEntete++, styleCelluleEntete, "NTT\n30.020");
        creeCellule(secondeLigneEntete, idxColEntete++, styleCelluleEntete, "num.adhér.\n30.019");
        creeCellule(secondeLigneEntete, idxColEntete++, styleCelluleEntete, "sexe\n30.005");
        creeCellule(secondeLigneEntete, idxColEntete++, styleCelluleEntete, "nom famille\n30.002");
        creeCellule(secondeLigneEntete, idxColEntete++, styleCelluleEntete, "nom usage\n30.003");
        creeCellule(secondeLigneEntete, idxColEntete++, styleCelluleEntete, "prénom\n30.004");
        creeCellule(secondeLigneEntete, idxColEntete++, styleCelluleEntete, "date nais.\n30.006");
        creeCellule(secondeLigneEntete, idxColEntete++, styleCelluleEntete, "Début cont.\n40.001");
        creeCellule(secondeLigneEntete, idxColEntete++, styleCelluleEntete, "Fin cont.\n40.010");
        creeCellule(secondeLigneEntete, idxColEntete++, styleCelluleEntete, "Libellé empl.\n40.006");
        creeCellule(secondeLigneEntete, idxColEntete++, styleCelluleEntete, "Num.cont.\n40.009");
        creeCellule(secondeLigneEntete, idxColEntete++, styleCelluleEntete, "Num.Catég.");
        creeCellule(secondeLigneEntete, idxColEntete++, styleCelluleEntete, "Début rattach.");
        creeCellule(secondeLigneEntete, idxColEntete++, styleCelluleEntete, "Fin rattach.");
        creeCellule(secondeLigneEntete, idxColEntete++, styleCelluleEntete, "Nb affilié");
        creeCellule(secondeLigneEntete, idxColEntete++, styleCelluleEntete, "A.D. Adultes");
        creeCellule(secondeLigneEntete, idxColEntete++, styleCelluleEntete, "A.D. Autres");
        creeCellule(secondeLigneEntete, idxColEntete++, styleCelluleEntete, "A.D. Enfants");
        creeCellule(secondeLigneEntete, idxColEntete++, styleCelluleEntete, "Effectif ttal");

        Long idPeriode = PERIODE.get().getIdPeriode();
        int pageTaille = getPageTaille();
        int page = 0;
        List<ResumeDecompteEffectifExcel> effectifCategorieContratsTravail = effectifCategorieContratTravailRepository
                .getDecomptesEffectifPourPeriodePagines(idPeriode, page * pageTaille, pageTaille);
        while (!effectifCategorieContratsTravail.isEmpty()) {
            for (ResumeDecompteEffectifExcel contratTravailSurEffectifCategorie : effectifCategorieContratsTravail) {
                Row ligneDecompteEffectif = decompteEffectifs.createRow(rowNum++);
                remplitLigneDecompteEffectif(ligneDecompteEffectif, styleCelluleParDefaut, contratTravailSurEffectifCategorie);
            }
            // On récupère la page suivante
            page++;
            effectifCategorieContratsTravail = effectifCategorieContratTravailRepository.getDecomptesEffectifPourPeriodePagines(idPeriode,
                    page * pageTaille, pageTaille);
        }

        // nombre de caractères à afficher par colonnes
        int[] widthArray = { 13, 13, 12, 9, 6, 13, 40, 30, 6, 40, 40, 40, 10, 18, 16, 40, 20, 11, 14, 12, 10, 12, 11, 12, 13 };
        redimensionnementColonnes(decompteEffectifs, widthArray);

        decompteEffectifs.setAutoFilter(new CellRangeAddress(3, 3, 0, 24));
    }

    /**
     * Remplit une ligne de décompte d'effectifs.
     * 
     * @param ligneDecompteEffectif
     *            la ligne à remplir
     * @param styleCellule
     *            le style des cellules de la ligne
     * @param contratTravailSurEffectifCategorie
     *            le contrat de travail sur catégorie d'effectif
     */
    private void remplitLigneDecompteEffectif(Row ligneDecompteEffectif, CellStyle styleCellule,
            ResumeDecompteEffectifExcel contratTravailSurEffectifCategorie) {
        int idxCol = 0;

        // Adhésion
        creeCellule(ligneDecompteEffectif, idxCol++, styleCellule,
                DateRedac.convertionDateRedacSansValidation(contratTravailSurEffectifCategorie.getDateConstitution()));
        creeCellule(ligneDecompteEffectif, idxCol++, styleCellule,
                DateRedac.convertionDateRedacSansValidation(contratTravailSurEffectifCategorie.getMoisRattachement()));
        creeCellule(ligneDecompteEffectif, idxCol++, styleCellule,
                DateRedac.convertionDateRedacSansValidation(contratTravailSurEffectifCategorie.getMoisDeclare()));
        creeCellule(ligneDecompteEffectif, idxCol++, styleCellule, contratTravailSurEffectifCategorie.getSiren());
        creeCellule(ligneDecompteEffectif, idxCol++, styleCellule, contratTravailSurEffectifCategorie.getNic());

        // Individu
        creeCellule(ligneDecompteEffectif, idxCol++, styleCellule, contratTravailSurEffectifCategorie.getNir());
        creeCellule(ligneDecompteEffectif, idxCol++, styleCellule, contratTravailSurEffectifCategorie.getNtt());
        creeCellule(ligneDecompteEffectif, idxCol++, styleCellule, contratTravailSurEffectifCategorie.getNumAdherent());
        creeCellule(ligneDecompteEffectif, idxCol++, styleCellule, contratTravailSurEffectifCategorie.getSexe());
        creeCellule(ligneDecompteEffectif, idxCol++, styleCellule, contratTravailSurEffectifCategorie.getNomFamille());
        creeCellule(ligneDecompteEffectif, idxCol++, styleCellule, contratTravailSurEffectifCategorie.getNomUsage());
        creeCellule(ligneDecompteEffectif, idxCol++, styleCellule, contratTravailSurEffectifCategorie.getPrenom());
        creeCellule(ligneDecompteEffectif, idxCol++, styleCellule,
                DateRedac.convertionDateRedacSansValidation(contratTravailSurEffectifCategorie.getDateNaissance()));

        // Contrat travail
        creeCellule(ligneDecompteEffectif, idxCol++, styleCellule,
                DateRedac.convertionDateRedacSansValidation(contratTravailSurEffectifCategorie.getDateDebutContrat()));
        creeCellule(ligneDecompteEffectif, idxCol++, styleCellule,
                DateRedac.convertionDateRedacSansValidation(contratTravailSurEffectifCategorie.getDateFinPrevisionnelle()));
        creeCellule(ligneDecompteEffectif, idxCol++, styleCellule, contratTravailSurEffectifCategorie.getLibelleEmploi());
        creeCellule(ligneDecompteEffectif, idxCol++, styleCellule, contratTravailSurEffectifCategorie.getNumeroContrat());

        // Décompte des effectifs
        creeCellule(ligneDecompteEffectif, idxCol++, styleCellule, contratTravailSurEffectifCategorie.getNumCategorieQuittancement());
        creeCellule(ligneDecompteEffectif, idxCol++, styleCellule,
                DateRedac.convertionDateRedacSansValidation(contratTravailSurEffectifCategorie.getDateDebutBase()));
        creeCellule(ligneDecompteEffectif, idxCol++, styleCellule,
                DateRedac.convertionDateRedacSansValidation(contratTravailSurEffectifCategorie.getDateFinBase()));
        creeCellule(ligneDecompteEffectif, idxCol++, styleCellule, (double) contratTravailSurEffectifCategorie.getNombreAffilies());
        creeCellule(ligneDecompteEffectif, idxCol++, styleCellule, (double) contratTravailSurEffectifCategorie.getAyantsDroitAdulte());
        creeCellule(ligneDecompteEffectif, idxCol++, styleCellule, (double) contratTravailSurEffectifCategorie.getAyantsDroitAutre());
        creeCellule(ligneDecompteEffectif, idxCol++, styleCellule, (double) contratTravailSurEffectifCategorie.getAyantsDroitEnfant());
        creeCellule(ligneDecompteEffectif, idxCol++, styleCellule, (double) contratTravailSurEffectifCategorie.getTotalEffectif());
    }

    private void creeOngletDetailsBaseParIndividu(Workbook workbook, CellStyle styleCelluleTitre, CellStyle styleCelluleEntete,
            CellStyle styleCelluleParDefaut) {
        Long idPeriode = PERIODE.get().getIdPeriode();

        Sheet detailBases = workbook.createSheet("Détail Bases par Individu");

        int rowNum = 0;

        // Titre
        Cell celluleTitre = detailBases.createRow(rowNum++).createCell(0);
        celluleTitre.setCellStyle(styleCelluleTitre);
        celluleTitre.setCellValue("Détail des Bases assujetties rattachées à cette période");
        detailBases.addMergedRegion(new CellRangeAddress(0, 0, 0, 10));

        // Saut de ligne
        rowNum++;

        // Première ligne entête
        Row premiereLigneEntete = detailBases.createRow(rowNum++);
        creeCelluleEntete(detailBases, premiereLigneEntete, 0, 4, styleCelluleEntete, "Adhésion");
        creeCelluleEntete(detailBases, premiereLigneEntete, 5, 12, styleCelluleEntete, "Individu");
        creeCelluleEntete(detailBases, premiereLigneEntete, 13, 16, styleCelluleEntete, "Contrat travail");
        creeCelluleEntete(detailBases, premiereLigneEntete, 17, 24, styleCelluleEntete, "Affiliation");
        creeCelluleEntete(detailBases, premiereLigneEntete, 25, 28, styleCelluleEntete, "Base assujettie");
        creeCelluleEntete(detailBases, premiereLigneEntete, 29, 41, styleCelluleEntete, "Montant du type de composant de base assujettie");
        creeCelluleEntete(detailBases, premiereLigneEntete, 42, 45, styleCelluleEntete, "Montant de la tranche correspondante");

        // Seconde ligne entête
        Row secondeLigneEntete = detailBases.createRow(rowNum++);
        int idxColEntete = 0;
        // Adhésion
        creeCellule(secondeLigneEntete, idxColEntete++, styleCelluleEntete, "Date constit.\n05.007");
        creeCellule(secondeLigneEntete, idxColEntete++, styleCelluleEntete, "Mois rattach.");
        creeCellule(secondeLigneEntete, idxColEntete++, styleCelluleEntete, "Mois déclar.\n05.005");
        creeCellule(secondeLigneEntete, idxColEntete++, styleCelluleEntete, "SIREN\n06.001");
        creeCellule(secondeLigneEntete, idxColEntete++, styleCelluleEntete, "NIC\n11.001");
        // Individu
        creeCellule(secondeLigneEntete, idxColEntete++, styleCelluleEntete, "NIR\n30.001");
        creeCellule(secondeLigneEntete, idxColEntete++, styleCelluleEntete, "NTT\n30.020");
        creeCellule(secondeLigneEntete, idxColEntete++, styleCelluleEntete, "num.adhér.\n30.019");
        creeCellule(secondeLigneEntete, idxColEntete++, styleCelluleEntete, "sexe\n30.005");
        creeCellule(secondeLigneEntete, idxColEntete++, styleCelluleEntete, "nom famille\n30.002");
        creeCellule(secondeLigneEntete, idxColEntete++, styleCelluleEntete, "nom usage\n30.003");
        creeCellule(secondeLigneEntete, idxColEntete++, styleCelluleEntete, "prénom\n30.004");
        creeCellule(secondeLigneEntete, idxColEntete++, styleCelluleEntete, "date nais.\n30.006");
        // Contrat travail
        creeCellule(secondeLigneEntete, idxColEntete++, styleCelluleEntete, "Début cont.\n40.001");
        creeCellule(secondeLigneEntete, idxColEntete++, styleCelluleEntete, "Fin cont.\n40.010");
        creeCellule(secondeLigneEntete, idxColEntete++, styleCelluleEntete, "Libellé empl.\n40.006");
        creeCellule(secondeLigneEntete, idxColEntete++, styleCelluleEntete, "Num.cont.\n40.009");
        // Affiliation
        creeCellule(secondeLigneEntete, idxColEntete++, styleCelluleEntete, "Code opt.\n70.004");
        creeCellule(secondeLigneEntete, idxColEntete++, styleCelluleEntete, "Code pop.\n70.005");
        creeCellule(secondeLigneEntete, idxColEntete++, styleCelluleEntete, "A.D. Adultes\n70.008");
        creeCellule(secondeLigneEntete, idxColEntete++, styleCelluleEntete, "Ayants Droits\n70.009");
        creeCellule(secondeLigneEntete, idxColEntete++, styleCelluleEntete, "A.D. Autres\n70.010");
        creeCellule(secondeLigneEntete, idxColEntete++, styleCelluleEntete, "A.D. Enfants\n70.011");
        creeCellule(secondeLigneEntete, idxColEntete++, styleCelluleEntete, "Debut affil.\n70.014");
        creeCellule(secondeLigneEntete, idxColEntete++, styleCelluleEntete, "Fin affil.\n70.015");

        // Base assujettie
        creeCellule(secondeLigneEntete, idxColEntete++, styleCelluleEntete, "Début rattacht\n78.002");
        creeCellule(secondeLigneEntete, idxColEntete++, styleCelluleEntete, "Fin rattacht\n78.003");
        creeCellule(secondeLigneEntete, idxColEntete++, styleCelluleEntete, "Mt cotisat.\n81.004");
        creeCellule(secondeLigneEntete, idxColEntete++, styleCelluleEntete, "Estimation\ncotisation");

        // Montant du type de composant de base assujettie
        creeCellule(secondeLigneEntete, idxColEntete++, styleCelluleEntete, "Sal.brut\n10");
        creeCellule(secondeLigneEntete, idxColEntete++, styleCelluleEntete, "TA\n11");
        creeCellule(secondeLigneEntete, idxColEntete++, styleCelluleEntete, "T2\n12");
        creeCellule(secondeLigneEntete, idxColEntete++, styleCelluleEntete, "TB\n13");
        creeCellule(secondeLigneEntete, idxColEntete++, styleCelluleEntete, "TC\n14");
        creeCellule(secondeLigneEntete, idxColEntete++, styleCelluleEntete, "TD\n15");
        creeCellule(secondeLigneEntete, idxColEntete++, styleCelluleEntete, "TD1\n16");
        creeCellule(secondeLigneEntete, idxColEntete++, styleCelluleEntete, "Spécif.\n17");
        creeCellule(secondeLigneEntete, idxColEntete++, styleCelluleEntete, "Forfait.\n18");
        creeCellule(secondeLigneEntete, idxColEntete++, styleCelluleEntete, "Fictive\n19");
        creeCellule(secondeLigneEntete, idxColEntete++, styleCelluleEntete, "Mt forfait.\n20");
        creeCellule(secondeLigneEntete, idxColEntete++, styleCelluleEntete, "Mt libre\n21");
        creeCellule(secondeLigneEntete, idxColEntete++, styleCelluleEntete, "T2U\n24");
        // Montant de tranche correspondante
        for (int numTranche = 1; numTranche <= 4; numTranche++) {
            String libelleTranche = trancheCategorieBaseAssujettieRepository.getLibelleTranchePourPeriodeEtNumTranche(idPeriode, numTranche);
            libelleTranche = libelleTranche != null ? libelleTranche : "T" + numTranche;
            creeCellule(secondeLigneEntete, idxColEntete++, styleCelluleEntete, libelleTranche);
        }

        int pageTaille = getPageTaille();
        int page = 0;

        boolean existeBaseAssujetiePourPeriode = baseAssujettieRepository.existeBaseAssujettiePourPeriode(idPeriode);

        if (existeBaseAssujetiePourPeriode) {
            List<ResumeDeclarationIndividuExcel> declarationsIndividu = rattachementDeclarationsRecuesRepository
                    .getBasesAssujettiesRattacheesPagines(idPeriode, page * pageTaille, pageTaille);

            boolean inferieurPageTaille = false;
            while (!declarationsIndividu.isEmpty() && !inferieurPageTaille) {
                for (ResumeDeclarationIndividuExcel declarationIndividu : declarationsIndividu) {
                    Row ligneBaseAssujettie = detailBases.createRow(rowNum++);
                    remplitLigneBaseAssujettie(ligneBaseAssujettie, styleCelluleParDefaut, declarationIndividu);
                }

                // mise à jour du flag
                inferieurPageTaille = declarationsIndividu.size() < pageTaille;

                if (!inferieurPageTaille) {
                    // On récupère la page suivante
                    page++;
                    declarationsIndividu = rattachementDeclarationsRecuesRepository.getBasesAssujettiesRattacheesPagines(idPeriode, page * pageTaille,
                            pageTaille);
                }
            }
        }

        // nombre de caractères à afficher par colonnes
        int[] widthArray = { 13, 13, 12, 9, 6, // Adhésion
                13, 40, 30, 6, 40, 40, 40, 10, // individu
                18, 16, 40, 20, // Contrat travail
                9, 9, 12, 13, 11, 12, 14, 14, // Affiliation
                14, 12, 11, 12, // Base assujettie
                10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, // Montant du type de composant de base assujettie
                10, 10, 10, 10 };// Montant de la tranche correspondante

        redimensionnementColonnes(detailBases, widthArray);
        detailBases.setAutoFilter(new CellRangeAddress(3, 3, 0, 41));

    }

    private void remplitLigneBaseAssujettie(Row ligneBaseAssujettie, CellStyle styleCellule, ResumeDeclarationIndividuExcel declarationIndividu) {
        int idxCol = 0;

        // Adhésion
        creeCellule(ligneBaseAssujettie, idxCol++, styleCellule,
                DateRedac.convertionDateRedacSansValidation(declarationIndividu.getDateConstitution()));
        creeCellule(ligneBaseAssujettie, idxCol++, styleCellule,
                DateRedac.convertionDateRedacSansValidation(declarationIndividu.getMoisRattachement()));
        creeCellule(ligneBaseAssujettie, idxCol++, styleCellule, DateRedac.convertionDateRedacSansValidation(declarationIndividu.getMoisDeclare()));
        creeCellule(ligneBaseAssujettie, idxCol++, styleCellule, declarationIndividu.getSiren());
        creeCellule(ligneBaseAssujettie, idxCol++, styleCellule, declarationIndividu.getNic());

        // Individu
        creeCellule(ligneBaseAssujettie, idxCol++, styleCellule, declarationIndividu.getNir());
        creeCellule(ligneBaseAssujettie, idxCol++, styleCellule, declarationIndividu.getNtt());
        creeCellule(ligneBaseAssujettie, idxCol++, styleCellule, declarationIndividu.getNumAdherent());
        creeCellule(ligneBaseAssujettie, idxCol++, styleCellule, declarationIndividu.getSexe());
        creeCellule(ligneBaseAssujettie, idxCol++, styleCellule, declarationIndividu.getNomFamille());
        creeCellule(ligneBaseAssujettie, idxCol++, styleCellule, declarationIndividu.getNomUsage());
        creeCellule(ligneBaseAssujettie, idxCol++, styleCellule, declarationIndividu.getPrenom());
        creeCellule(ligneBaseAssujettie, idxCol++, styleCellule, DateRedac.convertionDateRedacSansValidation(declarationIndividu.getDateNaissance()));

        // Contrat travail
        creeCellule(ligneBaseAssujettie, idxCol++, styleCellule,
                DateRedac.convertionDateRedacSansValidation(declarationIndividu.getDateDebutContrat()));
        creeCellule(ligneBaseAssujettie, idxCol++, styleCellule,
                DateRedac.convertionDateRedacSansValidation(declarationIndividu.getDateFinPrevisionnelle()));
        creeCellule(ligneBaseAssujettie, idxCol++, styleCellule, declarationIndividu.getLibelleEmploi());
        creeCellule(ligneBaseAssujettie, idxCol++, styleCellule, declarationIndividu.getNumeroContrat());

        // Affiliation
        creeCellule(ligneBaseAssujettie, idxCol++, styleCellule, declarationIndividu.getCodeOption());
        creeCellule(ligneBaseAssujettie, idxCol++, styleCellule, declarationIndividu.getCodePopulation());

        creeCellule(ligneBaseAssujettie, idxCol++, styleCellule,
                declarationIndividu.getAyantDroitAdultes() != null ? (double) declarationIndividu.getAyantDroitAdultes() : null);
        creeCellule(ligneBaseAssujettie, idxCol++, styleCellule,
                declarationIndividu.getAyantDroit() != null ? (double) declarationIndividu.getAyantDroit() : null);
        creeCellule(ligneBaseAssujettie, idxCol++, styleCellule,
                declarationIndividu.getAyantDroitAutres() != null ? (double) declarationIndividu.getAyantDroitAutres() : null);
        creeCellule(ligneBaseAssujettie, idxCol++, styleCellule,
                declarationIndividu.getAyantDroitEnfants() != null ? (double) declarationIndividu.getAyantDroitEnfants() : null);
        creeCellule(ligneBaseAssujettie, idxCol++, styleCellule, declarationIndividu.getDateDebutAffiliationSansValidation());
        creeCellule(ligneBaseAssujettie, idxCol++, styleCellule, declarationIndividu.getDateFinAffiliationSansValidation());

        // Base assujettie
        creeCellule(ligneBaseAssujettie, idxCol++, styleCellule,
                DateRedac.convertionDateRedacSansValidation(declarationIndividu.getDateDebRattachement()));
        creeCellule(ligneBaseAssujettie, idxCol++, styleCellule,
                DateRedac.convertionDateRedacSansValidation(declarationIndividu.getDateFinRattachement()));
        creeCellule(ligneBaseAssujettie, idxCol++, styleCellule, declarationIndividu.getMontantCotisation());
        creeCellule(ligneBaseAssujettie, idxCol++, styleCellule, declarationIndividu.getEstimationCotisation());

        // Montant du type de composant base assujettie
        creeCellule(ligneBaseAssujettie, idxCol++, styleCellule, declarationIndividu.getMontantCBA10());
        creeCellule(ligneBaseAssujettie, idxCol++, styleCellule, declarationIndividu.getMontantCBA11());
        creeCellule(ligneBaseAssujettie, idxCol++, styleCellule, declarationIndividu.getMontantCBA12());
        creeCellule(ligneBaseAssujettie, idxCol++, styleCellule, declarationIndividu.getMontantCBA13());
        creeCellule(ligneBaseAssujettie, idxCol++, styleCellule, declarationIndividu.getMontantCBA14());
        creeCellule(ligneBaseAssujettie, idxCol++, styleCellule, declarationIndividu.getMontantCBA15());
        creeCellule(ligneBaseAssujettie, idxCol++, styleCellule, declarationIndividu.getMontantCBA16());
        creeCellule(ligneBaseAssujettie, idxCol++, styleCellule, declarationIndividu.getMontantCBA17());
        creeCellule(ligneBaseAssujettie, idxCol++, styleCellule, declarationIndividu.getMontantCBA18());
        creeCellule(ligneBaseAssujettie, idxCol++, styleCellule, declarationIndividu.getMontantCBA19());
        creeCellule(ligneBaseAssujettie, idxCol++, styleCellule, declarationIndividu.getMontantCBA20());
        creeCellule(ligneBaseAssujettie, idxCol++, styleCellule, declarationIndividu.getMontantCBA21());
        creeCellule(ligneBaseAssujettie, idxCol++, styleCellule, declarationIndividu.getMontantCBA24());

        // Montant de la tranche correspondante
        creeCellule(ligneBaseAssujettie, idxCol++, styleCellule, declarationIndividu.getMontantTranche1());
        creeCellule(ligneBaseAssujettie, idxCol++, styleCellule, declarationIndividu.getMontantTranche2());
        creeCellule(ligneBaseAssujettie, idxCol++, styleCellule, declarationIndividu.getMontantTranche3());
        creeCellule(ligneBaseAssujettie, idxCol++, styleCellule, declarationIndividu.getMontantTranche4());

    }
}
