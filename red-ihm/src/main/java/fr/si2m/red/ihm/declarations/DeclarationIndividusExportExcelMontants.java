package fr.si2m.red.ihm.declarations;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import fr.si2m.red.DateRedac;
import fr.si2m.red.contrat.TarifRepository;
import fr.si2m.red.dsn.AdhesionEtablissementMois;
import fr.si2m.red.dsn.ResumeAffiliationExportExcelAllegesMontant;
import fr.si2m.red.reconciliation.PeriodeRecue;
import fr.si2m.red.reconciliation.RattachementDeclarationsRecuesRepository;

/**
 * Export Excel des individus d'une déclaration.
 * 
 * @author nortaina
 *
 */
@Component
public class DeclarationIndividusExportExcelMontants extends DeclarationExportExcel {

    @Autowired
    private RattachementDeclarationsRecuesRepository rattachementDeclarationsRecuesRepository;

    @Autowired
    private TarifRepository tarifRepository;

    private static final int SYNTHESE_POSITION_LIBELLE_2 = 3;
    private static final int SYNTHESE_POSITION_VALEUR_2 = 4;

    @Override
    protected void creeContenuClasseurMontant(Workbook workbook) {
        Assert.notNull(PERIODE.get(), "La période pour l'export n'a pas été définie - appeler la méthode #exporte(PeriodeRecue, OutputStream)");

        // Onglets
        creeOngletSynthese(workbook);
        creeOngletExtraction(workbook);
    }

    /**
     * Crée l'onglet de synthèse de la période.
     * 
     * @param workbook
     *            le classeur
     * @param styleCelluleParDefaut
     *            le style par défaut, réutilisable
     */
    private void creeOngletSynthese(Workbook workbook) {
        Sheet synthese = workbook.createSheet("Synthèse");

        int rowNum = creeEnteteOngletSynthese(workbook, synthese);
        rowNum = creeDetailsPeriodeOngletSynthese(workbook, synthese, rowNum);
        rowNum = creeDetailsPeriodeOngletSyntheseIndividus(workbook, synthese, rowNum);

        // nombre de caractères à afficher par colonnes
        int[] widthArray = { 20, 20, 20, 20, 20, 20, 20 };
        redimensionnementColonnes(synthese, widthArray);

    }

    private int creeDetailsPeriodeOngletSyntheseIndividus(Workbook workbook, Sheet synthese, int premiereLigne) {
        PeriodeRecue periode = PERIODE.get();

        // Styles
        CellStyle styleLibelle = creeStyleCelluleParDefaut(workbook, false);
        styleLibelle.setAlignment(CellStyle.ALIGN_RIGHT);

        CellStyle styleValeur = creeStyleCelluleParDefaut(workbook, true);

        // Modification de libelle: SIREN déclaré
        Cell cellSiren = null;
        int rowNum = premiereLigne;

        // Retrieve the row and check for null
        Row syntheseLigne = synthese.getRow(8);
        if (syntheseLigne != null) {
            cellSiren = syntheseLigne.getCell(3);
        }

        if (cellSiren != null) {
            cellSiren.setCellValue("SIREN déclaré :");
        }

        // Modification de libelle: NIC déclaré
        Cell cellNIC = null;

        syntheseLigne = synthese.getRow(9);
        if (syntheseLigne != null) {
            cellNIC = syntheseLigne.getCell(3);
        }

        if (cellNIC != null) {
            cellNIC.setCellValue("NIC déclaré :");
        }

        // Ajout du champ SIREN émetteur
        int createNewRowAt = 10;

        Row ligneSyntheseSirenEmetteur = synthese.createRow(createNewRowAt);
        ligneSyntheseSirenEmetteur = synthese.getRow(createNewRowAt);

        List<AdhesionEtablissementMois> adhesionsEtablissements = rattachementDeclarationsRecuesRepository
                .getAdhesionsEtablissementsMoisRattaches(periode.getIdPeriode());

        // Considerer la premiere adhesion
        creeCelluleFormulaireLibelle(ligneSyntheseSirenEmetteur, SYNTHESE_POSITION_LIBELLE_2, styleLibelle, "SIREN émetteur :");
        String sirenEmetteur = StringUtils.EMPTY;
        if (!adhesionsEtablissements.isEmpty()) {
            sirenEmetteur = adhesionsEtablissements.get(0).getSirenEmetteur();
        }
        creeCelluleFormulaireValeur(ligneSyntheseSirenEmetteur, SYNTHESE_POSITION_VALEUR_2, styleValeur, sirenEmetteur);
        rowNum++;
        return rowNum;

    }

    private void creeOngletExtraction(Workbook workbook) {
        Sheet extraction = workbook.createSheet("Individus");

        PeriodeRecue periode = PERIODE.get();
        Long idPeriode = periode.getIdPeriode();

        int rowNum = remplitLigneEntete(workbook, extraction);

        // Styles
        CellStyle styleCelluleParDefaut = creeStyleCelluleParDefaut(workbook, false);
        enrichitStyleCelluleAvecToutesBordures(styleCelluleParDefaut);
        styleCelluleParDefaut.setWrapText(true);

        List<ResumeAffiliationExportExcelAllegesMontant> affiliations = rattachementDeclarationsRecuesRepository.getIndividusMontants(idPeriode);

        for (ResumeAffiliationExportExcelAllegesMontant affiliation : affiliations) {
            Row ligneIndividu = extraction.createRow(rowNum++);

            remplitLigneIndividu(ligneIndividu, affiliation, styleCelluleParDefaut);
        }

        // nombre de caractères à afficher par colonnes
        int[] widthArray = { 13, 40, 30, 6, 40, 40, 40, 10, // Individus
                10, 10, // Contrat travail
                15, 40, 11 // Affiliation
        };
        redimensionnementColonnes(extraction, widthArray);

        extraction.setAutoFilter(new CellRangeAddress(3, 3, 0, 12));
    }

    /**
     * Remplit les cases d'une ligne entête.
     * 
     * @param workbook
     *            le classeur Excel
     * @param extraction
     *            la feuille de l'extraction
     * 
     * @return le numéro de ligne suivant l'entête
     */
    private int remplitLigneEntete(Workbook workbook, Sheet extraction) {
        // PeriodeRecue periode = PERIODE.get();

        int rowNum = 0;
        int idxColEntete = 0;

        // Style
        CellStyle styleCellule = creeStyleCelluleEnteteListe(workbook, false);

        // Titre
        Cell celluleTitre = extraction.createRow(rowNum++).createCell(0);
        CellStyle styleTitre = creeStyleCelluleTitreListe(workbook);
        celluleTitre.setCellStyle(styleTitre);
        celluleTitre.setCellValue("Liste des individus allégée avec montant de cotisation");
        extraction.addMergedRegion(new CellRangeAddress(0, 0, 0, 5));

        // Saut de ligne
        rowNum++;

        // Première ligne entête
        Row premiereLigneEntete = extraction.createRow(rowNum++);
        creeCelluleEntete(extraction, premiereLigneEntete, 0, 7, styleCellule, "Individu");
        creeCelluleEntete(extraction, premiereLigneEntete, 8, 9, styleCellule, "Contrat Travail");
        creeCelluleEntete(extraction, premiereLigneEntete, 10, 12, styleCellule, "Affiliation");

        // Seconde ligne entête
        Row secondeLigneEntete = extraction.createRow(rowNum++);
        creeCellule(secondeLigneEntete, idxColEntete++, styleCellule, "NIR\n30.001");
        creeCellule(secondeLigneEntete, idxColEntete++, styleCellule, "NTT\n30.020");
        creeCellule(secondeLigneEntete, idxColEntete++, styleCellule, "num.adhér.\n30.019");
        creeCellule(secondeLigneEntete, idxColEntete++, styleCellule, "sexe\n30.005");
        creeCellule(secondeLigneEntete, idxColEntete++, styleCellule, "nom famille\n30.002");
        creeCellule(secondeLigneEntete, idxColEntete++, styleCellule, "nom usage\n30.003");
        creeCellule(secondeLigneEntete, idxColEntete++, styleCellule, "prénom\n30.004");
        creeCellule(secondeLigneEntete, idxColEntete++, styleCellule, "date nais.\n30.006");
        creeCellule(secondeLigneEntete, idxColEntete++, styleCellule, "Date entrée\n40.001");
        creeCellule(secondeLigneEntete, idxColEntete++, styleCellule, "Date sortie\n40.010");
        creeCellule(secondeLigneEntete, idxColEntete++, styleCellule, "Code Population");
        creeCellule(secondeLigneEntete, idxColEntete++, styleCellule, "Libellé Population\nSI source");
        creeCellule(secondeLigneEntete, idxColEntete++, styleCellule, "Mt Cotis.");

        return rowNum;
    }

    /**
     * Remplit les cases d'une ligne individu.
     * 
     * @param ligneIndividu
     *            la ligne individu
     * @param affiliation
     *            l'affiliation de l'individu à exporter
     */
    private void remplitLigneIndividu(Row ligneIndividu, ResumeAffiliationExportExcelAllegesMontant affiliation, CellStyle styleCellule) {
        int idxcolonne = 0;
        creeCellule(ligneIndividu, idxcolonne++, styleCellule, affiliation.getIdentifiantRepertoire());
        creeCellule(ligneIndividu, idxcolonne++, styleCellule, affiliation.getNumeroTechniqueTemporaire());
        creeCellule(ligneIndividu, idxcolonne++, styleCellule, affiliation.getMatricule());
        creeCellule(ligneIndividu, idxcolonne++, styleCellule, affiliation.getSexe());
        creeCellule(ligneIndividu, idxcolonne++, styleCellule, affiliation.getNomFamille());
        creeCellule(ligneIndividu, idxcolonne++, styleCellule, affiliation.getNomUsage());
        creeCellule(ligneIndividu, idxcolonne++, styleCellule, affiliation.getPrenom());
        creeCellule(ligneIndividu, idxcolonne++, styleCellule, DateRedac.convertionDateRedacSansValidation(affiliation.getDateNaissance()));
        creeCellule(ligneIndividu, idxcolonne++, styleCellule, DateRedac.convertionDateRedacSansValidation(affiliation.getDateDebutContrat()));
        creeCellule(ligneIndividu, idxcolonne++, styleCellule, DateRedac.convertionDateRedacSansValidation(affiliation.getDateFinPrevisionnelle()));
        creeCellule(ligneIndividu, idxcolonne++, styleCellule, affiliation.getCodePopulation());

        String libPopulation = affiliation.getLibPopulation() != null ? affiliation.getLibPopulation() : StringUtils.EMPTY;
        creeCellule(ligneIndividu, idxcolonne++, styleCellule, libPopulation);

        // On veut pouvoir afficher la valeur 0 si valeur présente dans CATEGORIE_QUITTANCEMENT_INDIVIDU
        // ou 'vide' si valeur null

        // F09_RG_5_06

        if (affiliation.isMontantCotisationExistant() && Double.compare(affiliation.getMontantCot().doubleValue(), 0d) == 0) {
            creeCellule(ligneIndividu, idxcolonne++, styleCellule, affiliation.getMontantCot(), true);
        } else {
            creeCellule(ligneIndividu, idxcolonne++, styleCellule, affiliation.getMontantCot());
        }
    }

    @Override
    protected void creeContenuClasseur(Workbook workbook) {
        // TODO Auto-generated method stub

    }

    @Override
    protected void creeContenuClasseurMessage(Workbook workbook) {
        // TODO Auto-generated method stub

    }

}
