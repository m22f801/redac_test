-- Utilisateurs de test

INSERT INTO PARAM_UTILISATEUR_GESTIONNAIRE (CODE_USER, NOM, PRENOM, DROIT_VIP, DROIT_INDIV, GRP_GEST, USER_CREATION) VALUES 
('4M_0', 'Noether', 'Emmy', 'O', 'O', 'N', '_INIT'),
('4M_1', 'Lovelace', 'Ada', 'O', 'O', 'N', '_INIT'),
('4M_2', 'Voisin', 'Claire', 'N', 'O', 'N', '_INIT'),
('4M_3', 'Mirzakhani', 'Maryam', 'N', 'N', 'N', '_INIT'),
('SI2M_0', 'Germain', 'Sophie', 'O', 'N', 'N', '_INIT'),
('SI2M_1', 'Serfaty', 'Sylvia', 'N', 'O', 'N', '_INIT'),
('SI2M_2', 'Choquet-Bruhat', 'Yvonne', 'O', 'O', 'N', '_INIT'),
('SI2M_3', 'd''Alexandrie', 'Hypatie', 'N', 'O', 'N', '_INIT');