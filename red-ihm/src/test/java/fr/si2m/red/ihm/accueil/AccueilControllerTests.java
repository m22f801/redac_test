package fr.si2m.red.ihm.accueil;

import org.junit.Assert;
import org.testng.annotations.Test;

/**
 * Tests sur le contrôleur de l'accueil.
 * 
 * @author nortaina
 *
 */
public class AccueilControllerTests {
    /**
     * Teste la récupération du modèle de la page d'accueil.
     */
    @Test
    public void testGetAccueilModel() {
        String redirectionString = new AccueilController().getAccueil();
        Assert.assertNotNull(redirectionString);
        Assert.assertEquals(redirectionString, "redirect:/ihm/declarations");
    }
}
