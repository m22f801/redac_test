package fr.si2m.red.batch.flux10.execution;

import java.util.List;

import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobInstance;
import org.springframework.batch.core.explore.JobExplorer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;
import org.testng.Assert;

import fr.si2m.red.EntiteImportableBatch;
import fr.si2m.red.batch.BatchTests;
import fr.si2m.red.batch.moteur.CodeRetour;
import fr.si2m.red.batch.moteur.execution.ExecuteurBatch;
import fr.si2m.red.core.repository.EntiteImportableRepository;

public abstract class Flux10BatchTests extends BatchTests {

    protected abstract String getNomEntitesTestees();

    protected abstract EntiteImportableRepository<? extends EntiteImportableBatch> getReferentielEntitesImportees();

    protected abstract int getNombreEntitesTestees();

    @Autowired
    private PlatformTransactionManager txManager;
    @Autowired
    private JobExplorer jobExplorer;

    /**
     * Test du job.
     * 
     * @throws Exception
     *             si une erreur inattendue survient lors du test
     */
    public void lancerTestJobCasNormal(ExecuteurBatch executeur) throws Exception {
        // Préparation de l'exécution sur le profil de test des batchs

        executeur.setProfilsActifsJob(new String[] { "testBatch" });

        // Exécution
        int codeRetour = executeur.executeJob();

        // Teste le code retour
        Assert.assertEquals(codeRetour, CodeRetour.TERMINE.getCode(), "Le batch devait se terminer avec le code 0 (terminé sans erreurs).");

        // Début des tests sur la base
        TransactionStatus txStatus = txManager.getTransaction(new DefaultTransactionDefinition());
        try {
            // Teste la bonne insertion des entités dans la base de test
            List<? extends EntiteImportableBatch> paramEntite = getReferentielEntitesImportees().liste();
            Assert.assertEquals(paramEntite.size(), getNombreEntitesTestees(),
                    "Le batch devait insérer " + getNombreEntitesTestees() + " " + getNomEntitesTestees() + "s en base.");

            // / A mettre dans le parent
            // Teste la sauvegarde de l'exécution du job en base
            List<JobInstance> jobFlux10Instances = jobExplorer.findJobInstancesByJobName("jobFlux10_" + getNomEntitesTestees(), 0, 1);
            Assert.assertEquals(jobFlux10Instances.size(), 1,
                    "Il ne devrait y avoir qu'une seule instance créée pour le job flux 10 " + getNomEntitesTestees());
            List<JobExecution> executionsJob = jobExplorer.getJobExecutions(jobFlux10Instances.get(0));
            Assert.assertEquals(executionsJob.size(), 1, "Il ne devrait y avoir qu'une seule exécution du job flux 10 " + getNomEntitesTestees());
            JobExecution executionJobFlux10 = executionsJob.get(0);

            Assert.assertEquals(executionJobFlux10.getExitStatus().getExitCode(), "0", "Le code de retour en base devrait être 0");
            Assert.assertEquals(executionJobFlux10.getExitStatus().getExitDescription(), "Terminé",
                    "Le messsage de retour aurait dû correspondre au code 0");
        } finally {
            txManager.commit(txStatus);
        }
    }

    protected String[] getParametrageBatch() {
        String[] parametrageBatch = new String[] {
                System.getProperty("user.dir") + "/target/test-classes/fr/si2m/red/batch/flux10/inputs/valides/" + getNomEntitesTestees() + ".csv",
                System.getProperty("user.dir") + "/target/test-classes/fr/si2m/red/batch/flux10/outputs/erreurs_" + getNomEntitesTestees() + ".log" };

        // RED-107 le path vers le fichier de config est paramétrable depuis les scripts, et donné à la JVM
        System.setProperty("configBatch", System.getProperty("user.dir") + "/target/test-classes/RR000.properties");

        return parametrageBatch;
    }

}
