/**
 * 
 */
package fr.si2m.red.batch.flux10.execution;

import java.io.File;

import org.springframework.beans.factory.annotation.Autowired;
import org.testng.Assert;
import org.testng.annotations.Test;

import fr.si2m.red.EntiteImportableBatch;
import fr.si2m.red.batch.moteur.CodeRetour;
import fr.si2m.red.core.repository.EntiteImportableRepository;
import fr.si2m.red.parametrage.ParamSirenFauxRepository;

/**
 * @author eudesr
 *
 */
@Test
public class Flux10ParamSirenFauxExecuteurBatchTests extends Flux10BatchTests {

    @Autowired
    private ParamSirenFauxRepository paramSirenFauxRepository;

    /**
     * Test du job.
     * 
     * @throws Exception
     *             si une erreur inattendue survient lors du test
     */
    @Test
    public void testJobCasNormal() throws Exception {
        // Préparation de l'exécution sur le profil de test des batchs
        Flux10ParamSirenFauxExecuteurBatch executeur = new Flux10ParamSirenFauxExecuteurBatch(getParametrageBatch());

        // Lancement du batch
        lancerTestJobCasNormal(executeur);
    }

    /*
     * (non-Javadoc)
     * 
     * @see fr.si2m.red.batch.flux10.execution.Flux10BatchTests#getNomEntitesTestees()
     */
    @Override
    protected String getNomEntitesTestees() {
        return "ParamSirenFaux";
    }

    /*
     * (non-Javadoc)
     * 
     * @see fr.si2m.red.batch.flux10.execution.Flux10BatchTests#getReferentielEntitesImportees()
     */
    @Override
    protected EntiteImportableRepository<? extends EntiteImportableBatch> getReferentielEntitesImportees() {
        return paramSirenFauxRepository;
    }

    /*
     * (non-Javadoc)
     * 
     * @see fr.si2m.red.batch.flux10.execution.Flux10BatchTests#getNombreEntitesTestees()
     */
    @Override
    protected int getNombreEntitesTestees() {
        return 3;
    }

    /**
     * Test du job.
     * 
     * @throws Exception
     *             si une erreur inattendue survient lors du test
     */
    @Test
    public void testCasNormalSurFichierReduitDeTI() throws Exception {
        String[] parametrageBatch = new String[] {
                System.getProperty("user.dir") + "/target/test-classes/fr/si2m/red/batch/flux10/inputs/valides/" + getNomEntitesTestees() + ".csv",
                System.getProperty("user.dir") + "/target/test-classes/fr/si2m/red/batch/flux10/outputs/erreurs_" + getNomEntitesTestees() + ".log" };

        File errorLog = new File(
                System.getProperty("user.dir") + "/target/test-classes/fr/si2m/red/batch/flux10/outputs/erreurs_" + getNomEntitesTestees() + ".log");
        if (errorLog.exists()) {
            // On part sur des logs frais
            errorLog.delete();
        }

        // RED-107 le path vers le fichier de config est paramétrable depuis les scripts, et donné à la JVM
        System.setProperty("configBatch", System.getProperty("user.dir") + "/target/test-classes/RR000.properties");

        // Préparation de l'exécution sur le profil de test des batchs
        Flux10ParamSirenFauxExecuteurBatch executeur = new Flux10ParamSirenFauxExecuteurBatch(parametrageBatch);
        executeur.setProfilsActifsJob(new String[] { "testBatch" });

        // Exécution
        int codeRetour = executeur.executeJob();

        // Teste le code retour
        Assert.assertEquals(codeRetour, CodeRetour.TERMINE.getCode(), "Le batch devait se terminer avec le code 0.");

    }

    /**
     * Test du job.
     * 
     * @throws Exception
     *             si une erreur inattendue survient lors du test
     */
    @Test
    public void testFormatSurFichierReduitDeTI() throws Exception {
        String[] parametrageBatch = new String[] {
                System.getProperty("user.dir") + "/target/test-classes/fr/si2m/red/batch/flux10/inputs/erreurformat/" + getNomEntitesTestees()
                        + ".csv",
                System.getProperty("user.dir") + "/target/test-classes/fr/si2m/red/batch/flux10/outputs/erreurs_" + getNomEntitesTestees() + ".log" };

        // RED-107 le path vers le fichier de config est paramétrable depuis les scripts, et donné à la JVM
        System.setProperty("configBatch", System.getProperty("user.dir") + "/target/test-classes/RR000.properties");

        // Préparation de l'exécution sur le profil de test des batchs
        Flux10ParamSirenFauxExecuteurBatch executeur = new Flux10ParamSirenFauxExecuteurBatch(parametrageBatch);
        executeur.setProfilsActifsJob(new String[] { "testBatch" });

        // Exécution
        int codeRetour = executeur.executeJob();

        // Teste le code retour
        Assert.assertEquals(codeRetour, CodeRetour.ERREUR_FONCTIONNELLE_BLOQUANTE.getCode(), "Le batch devait se terminer avec le code 40.");

    }
}
