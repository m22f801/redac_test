package fr.si2m.red.batch.flux4.execution;

import org.testng.Assert;
import org.testng.annotations.Test;

import fr.si2m.red.batch.BatchTests;
import fr.si2m.red.batch.moteur.CodeRetour;

/**
 * Tests du batch de controle du flux 4. ( 401 )
 * 
 * @author benitahy
 *
 */
@Test
public class Flux4ControleFichiersBatchTests extends BatchTests {

    /**
     * Test du job.
     * 
     * @throws Exception
     *             si une erreur inattendue survient lors du test
     */
    @Test(enabled = true)
    public void testJobCasNormal() throws Exception {
        String[] parametrageBatch = new String[] {
                System.getProperty("user.dir") + "/target/test-classes/fr/si2m/red/batch/flux4/inputs/phase1/valides", "20151123_114433", "123",
                "SI QUATREM", "4M", "WF", "4", "RR401",
                System.getProperty("user.dir") + "\\target\\test-classes\\fr\\si2m\\red\\batch\\flux4\\outputs\\erreurs_flux4_ControleFichiers.log" };

        // RED-107 le path vers le fichier de config est paramétrable depuis les scripts, et donné à la JVM
        System.setProperty("configBatch", System.getProperty("user.dir") + "/target/test-classes/RR401.properties");

        // Préparation de l'exécution sur le profil de test des batchs
        Flux4ControleFichiersExecuteurBatch executeur = new Flux4ControleFichiersExecuteurBatch(parametrageBatch);
        executeur.setProfilsActifsJob(new String[] { "testBatch" });

        // Exécution
        int codeRetour = executeur.executeJob();

        // Teste le code retour
        Assert.assertEquals(codeRetour, CodeRetour.TERMINE.getCode(), "Le batch devait se terminer avec le code 0 (terminé sans erreurs).");

    }

    @Test(enabled = false)
    public void testJobCasEnteteKo() throws Exception {
        String[] parametrageBatch = new String[] {
                System.getProperty("user.dir") + "\\target\\test-classes\\fr\\si2m\\red\\batch\\flux4\\inputs\\phase1\\P1_KO_enTete",
                "20151123_114433", "123", "SI QUATREM", "4M", "WF", "4", "RR401",
                System.getProperty("user.dir") + "\\target\\test-classes\\fr\\si2m\\red\\batch\\flux4\\outputs\\erreurs_flux4_ControleFichiers.log" };
        // Préparation de l'exécution sur le profil de test des batchs
        Flux4ControleFichiersExecuteurBatch executeur = new Flux4ControleFichiersExecuteurBatch(parametrageBatch);
        executeur.setProfilsActifsJob(new String[] { "testBatch" });

        // Exécution
        int codeRetour = executeur.executeJob();

        // Teste le code retour
        Assert.assertEquals(codeRetour, CodeRetour.ERREUR_FONCTIONNELLE_BLOQUANTE.getCode(),
                "Le batch devait se terminer avec le code 40 (erreur fonctionnelle bloquante).");

    }

    @Test(enabled = false)
    public void testJobCasHorodatageKo() throws Exception {
        String[] parametrageBatch = new String[] {
                System.getProperty("user.dir") + "\\target\\test-classes\\fr\\si2m\\red\\batch\\flux4\\inputs\\phase1\\P1_KO_horodatage",
                "20151123_114433", "123", "SI QUATREM", "4M", "WF", "4", "RR401",
                System.getProperty("user.dir") + "\\target\\test-classes\\fr\\si2m\\red\\batch\\flux4\\outputs\\erreurs_flux4_ControleFichiers.log" };
        // Préparation de l'exécution sur le profil de test des batchs
        Flux4ControleFichiersExecuteurBatch executeur = new Flux4ControleFichiersExecuteurBatch(parametrageBatch);
        executeur.setProfilsActifsJob(new String[] { "testBatch" });

        // Exécution
        int codeRetour = executeur.executeJob();

        // Teste le code retour
        Assert.assertEquals(codeRetour, CodeRetour.ERREUR_FONCTIONNELLE_BLOQUANTE.getCode(),
                "Le batch devait se terminer avec le code 40 (erreur fonctionnelle bloquante).");

    }

    @Test(enabled = false)
    public void testJobCasPiedPageKo() throws Exception {
        String[] parametrageBatch = new String[] {
                System.getProperty("user.dir") + "\\target\\test-classes\\fr\\si2m\\red\\batch\\flux4\\inputs\\phase1\\P1_KO_piedDePage",
                "20151123_114433", "123", "SI QUATREM", "4M", "WF", "4", "RR401",
                System.getProperty("user.dir") + "\\target\\test-classes\\fr\\si2m\\red\\batch\\flux4\\outputs\\erreurs_flux4_ControleFichiers.log" };
        // Préparation de l'exécution sur le profil de test des batchs
        Flux4ControleFichiersExecuteurBatch executeur = new Flux4ControleFichiersExecuteurBatch(parametrageBatch);
        executeur.setProfilsActifsJob(new String[] { "testBatch" });

        // Exécution
        int codeRetour = executeur.executeJob();

        // Teste le code retour
        Assert.assertEquals(codeRetour, CodeRetour.ERREUR_FONCTIONNELLE_BLOQUANTE.getCode(),
                "Le batch devait se terminer avec le code 40 (erreur fonctionnelle bloquante).");

    }

}
