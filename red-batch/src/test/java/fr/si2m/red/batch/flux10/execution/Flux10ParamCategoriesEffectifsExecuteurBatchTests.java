package fr.si2m.red.batch.flux10.execution;

import java.io.File;
import java.io.FileInputStream;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;
import org.testng.Assert;
import org.testng.annotations.Test;

import fr.si2m.red.EntiteImportableBatch;
import fr.si2m.red.batch.moteur.CodeRetour;
import fr.si2m.red.core.repository.EntiteImportableRepository;
import fr.si2m.red.parametrage.ParamCategorieEffectifs;
import fr.si2m.red.parametrage.ParamCategorieEffectifsId;
import fr.si2m.red.parametrage.ParamCategorieEffectifsRepository;

/**
 * Tests des batchs du flux 10.
 * 
 * @author poidij
 *
 */
@Test
public class Flux10ParamCategoriesEffectifsExecuteurBatchTests extends Flux10BatchTests {

    @Autowired
    private PlatformTransactionManager txManager;

    @Autowired
    private ParamCategorieEffectifsRepository paramCategorieEffectifsRepository;

    /**
     * Test du job.
     * 
     * @throws Exception
     *             si une erreur inattendue survient lors du test
     */
    @Test
    public void testJobCasNormal() throws Exception {
        // Préparation de l'exécution sur le profil de test des batchs
        Flux10ParamCategoriesEffectifsExecuteurBatch executeur = new Flux10ParamCategoriesEffectifsExecuteurBatch(getParametrageBatch());

        // Lancement du batch
        lancerTestJobCasNormal(executeur);

        // Début des tests sur la base
        TransactionStatus txStatus = txManager.getTransaction(new DefaultTransactionDefinition());
        try {
            ParamCategorieEffectifs param = paramCategorieEffectifsRepository.get(new ParamCategorieEffectifsId(false, "1", "MISS€ETRAN"));
            Assert.assertNotNull(param);
            Assert.assertEquals(param.getLibelleCategorie(), "MISS€ETRAN");

            ParamCategorieEffectifs paramPopEtendue = paramCategorieEffectifsRepository
                    .get(new ParamCategorieEffectifsId(false, "0123456789", "TEST LICAT POPULATION ETENDUE"));
            Assert.assertNotNull(paramPopEtendue);
            Assert.assertEquals(paramPopEtendue.getLibelleCategorie(), "TEST LICAT POPULATION ETENDUE");

        } finally {
            txManager.rollback(txStatus);
        }
    }

    protected String getNomEntitesTestees() {
        return "ParamCategoriesEffectifs";
    }

    @Override
    protected int getNombreEntitesTestees() {
        return 3854;
    }

    protected EntiteImportableRepository<? extends EntiteImportableBatch> getReferentielEntitesImportees() {
        return paramCategorieEffectifsRepository;
    }

    /**
     * Test du job.
     * 
     * @throws Exception
     *             si une erreur inattendue survient lors du test
     */
    @Test
    public void testJobCasLimiteChampsInvalides() throws Exception {
        String[] parametrageBatch = new String[] {
                System.getProperty("user.dir") + "/target/test-classes/fr/si2m/red/batch/flux10/inputs/erreurformat/" + getNomEntitesTestees()
                        + "-champs.csv",
                System.getProperty("user.dir") + "/target/test-classes/fr/si2m/red/batch/flux10/outputs/erreurs_" + getNomEntitesTestees() + ".log" };
        File errorLog = new File(
                System.getProperty("user.dir") + "/target/test-classes/fr/si2m/red/batch/flux10/outputs/erreurs_" + getNomEntitesTestees() + ".log");
        if (errorLog.exists()) {
            // On part sur des logs frais
            errorLog.delete();
        }

        // RED-107 le path vers le fichier de config est paramétrable depuis les scripts, et donné à la JVM
        System.setProperty("configBatch", System.getProperty("user.dir") + "/target/test-classes/RR000.properties");

        // Préparation de l'exécution sur le profil de test des batchs
        Flux10ParamCategoriesEffectifsExecuteurBatch executeur = new Flux10ParamCategoriesEffectifsExecuteurBatch(parametrageBatch);
        executeur.setProfilsActifsJob(new String[] { "testBatch" });

        // Exécution
        int codeRetour = executeur.executeJob();

        // Teste le code retour
        Assert.assertEquals(codeRetour, CodeRetour.ERREUR_FONCTIONNELLE_BLOQUANTE.getCode(), "Le batch devait se terminer avec le code 40.");

        // Teste l'absence de message d'unicité dans les logs d'erreur
        FileInputStream errorLogStream = new FileInputStream(errorLog);
        try {
            String errorLogContent = IOUtils.toString(errorLogStream);
            Assert.assertFalse(StringUtils.contains(errorLogContent, "ne respecte pas la règle d’unicité"));
        } finally {
            IOUtils.closeQuietly(errorLogStream);
        }

    }

}
