package fr.si2m.red.batch.test.item;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.si2m.red.batch.moteur.item.RedacteurBaseEntiteImportable;
import fr.si2m.red.batch.test.Test;
import fr.si2m.red.core.repository.EntiteImportableRepository;
import fr.si2m.red.core.repository.jpa.JpaEntiteImportableRepository;

/**
 * Simulation d'insertion des entités de tests récupérées et validées.
 * 
 * @author nortaina
 *
 */
public class TestRedacteurBase extends RedacteurBaseEntiteImportable<Test> {
    private static final Logger LOGGER = LoggerFactory.getLogger(TestRedacteurBase.class);

    @Override
    protected EntiteImportableRepository<Test> getRedacRepository() {
        return new JpaEntiteImportableRepository<Test>() {
        };
    }

    @Override
    public void write(List<? extends Test> items) {
        LOGGER.info("Début insertion de " + items.size() + " tests...");
        LOGGER.info(items.toString());
        LOGGER.info("Insertion de " + items.size() + " tests terminée !");
    }

}
