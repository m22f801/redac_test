package fr.si2m.red.batch.test.support;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import org.apache.commons.lang3.StringUtils;
import org.springframework.batch.item.file.transform.FieldSet;
import org.springframework.validation.BindException;

import fr.si2m.red.batch.moteur.support.ConvertisseurLigne;
import fr.si2m.red.batch.test.Test;

/**
 * Convertisseur de lignes CSV vers l'entité test.
 * 
 * @author nortaina
 *
 */
public class TestConvertisseurLigne extends ConvertisseurLigne<Test> {

    private final DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");

    @Override
    public Test mapFieldSet(FieldSet fieldSet) throws BindException {
        Test test = new Test();
        test.setNom(fieldSet.readString("nom"));
        test.setPrenom(fieldSet.readString("prenom"));
        String date = fieldSet.readString("dateNaissance");
        if (StringUtils.isNotBlank(date)) {
            try {
                test.setDateNaissance(dateFormat.parse(date));
            } catch (ParseException e) {
                traceBloquante(e, "La date de naissance " + date + " n'est pas au bon format yyyyMMdd");
            }
        }
        String salaire = fieldSet.readString("salaire");
        salaire = StringUtils.replace(salaire, ",", ".");
        test.setSalaire(Double.valueOf(salaire));
        return test;

    }
}
