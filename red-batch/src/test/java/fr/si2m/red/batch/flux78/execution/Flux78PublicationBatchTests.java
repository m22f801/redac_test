package fr.si2m.red.batch.flux78.execution;

import java.io.File;
import java.util.List;

import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobInstance;
import org.springframework.batch.core.explore.JobExplorer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;
import org.testng.Assert;
import org.testng.annotations.Test;

import fr.si2m.red.batch.BatchTests;
import fr.si2m.red.batch.moteur.CodeRetour;
import fr.si2m.red.reconciliation.CompteRenduIntegration;
import fr.si2m.red.reconciliation.CompteRenduIntegrationRepository;
import fr.si2m.red.reconciliation.HistoriqueEtatPeriode;
import fr.si2m.red.reconciliation.HistoriqueEtatPeriodeRepository;
import fr.si2m.red.reconciliation.MessageControle;
import fr.si2m.red.reconciliation.MessageControleRepository;

/**
 * Tests des batchs du flux 78.
 * 
 * @author poidij
 *
 */
@Test
public class Flux78PublicationBatchTests extends BatchTests {

    @Autowired
    private PlatformTransactionManager txManager;
    @Autowired
    private JobExplorer jobExplorer;
    @Autowired
    private HistoriqueEtatPeriodeRepository historiqueEtatPeriodeRepository;
    @Autowired
    private MessageControleRepository messageControleRepository;
    @Autowired
    private CompteRenduIntegrationRepository compteRenduIntegrationRepository;

    /**
     * Test du job pour le SI AVAL = WQUI
     * 
     * @throws Exception
     *             si une erreur inattendue survient lors du test
     */
    @Test(enabled = true)
    public void testJobCasWQUINormal() throws Exception {
        String repertoireSortie = System.getProperty("user.dir") + "/target/test-classes/fr/si2m/red/batch/flux78/outputs/";
        File fichierRepertoireSortie = new File(repertoireSortie);
        if (!fichierRepertoireSortie.exists()) {
            fichierRepertoireSortie.mkdirs();
        }
        String[] parametrageBatch = new String[] { repertoireSortie, "20160118", "WQUI", "20151229_170757",
                System.getProperty("user.dir") + "\\target\\test-classes\\fr\\si2m\\red\\batch\\flux78\\outputs\\erreurs_flux78_Publication.log" };

        // RED-107 le path vers le fichier de config est paramétrable depuis les scripts, et donné à la JVM
        System.setProperty("configBatch", System.getProperty("user.dir") + "/target/test-classes/RR701.properties");

        // Préparation de l'exécution sur le profil de test des batchs
        Flux78PublicationExecuteurBatch executeur = new Flux78PublicationExecuteurBatch(parametrageBatch);
        executeur.setProfilsActifsJob(new String[] { "testBatch" });

        // Exécution
        int codeRetour = executeur.executeJob();

        // Teste le code retour
        Assert.assertEquals(codeRetour, CodeRetour.TERMINE.getCode(), "Le batch devait se terminer avec le code 0 (terminé sans erreurs).");

        // Début des tests sur la base
        TransactionStatus txStatus = txManager.getTransaction(new DefaultTransactionDefinition());
        try {
            List<HistoriqueEtatPeriode> listeHistoriqueEtatPeriode = historiqueEtatPeriodeRepository.liste();
            List<MessageControle> listeMessageControle = messageControleRepository.liste();
            List<CompteRenduIntegration> listeCompteRenduIntegration = compteRenduIntegrationRepository.liste();

            Assert.assertEquals(listeHistoriqueEtatPeriode.size(), 41, "Le batch devait créer 41 HistoriqueEtatPeriodes.");

            Assert.assertEquals(listeMessageControle.size(), 2, "Le batch devait conserver 2 MessageControle.");

            Assert.assertEquals(listeCompteRenduIntegration.size(), 2, "Le batch devait conserver 2 CompteRenduIntegration.");

            // Teste la sauvegarde de l'exécution du job en base
            List<JobInstance> jobFlux78Instances = jobExplorer.findJobInstancesByJobName("jobFlux78_Publication", 0, 1);
            Assert.assertEquals(jobFlux78Instances.size(), 1, "Il ne devrait y avoir qu'une seule instance créée pour le job flux78");
            List<JobExecution> executionsJob = jobExplorer.getJobExecutions(jobFlux78Instances.get(0));
            Assert.assertEquals(executionsJob.size(), 1, "Il ne devrait y avoir qu'une seule exécution du job flux78");
            JobExecution executionJobFlux78 = executionsJob.get(0);

            Assert.assertEquals(executionJobFlux78.getExitStatus().getExitCode(), "0", "Le code de retour en base devrait être 0");
            Assert.assertEquals(executionJobFlux78.getExitStatus().getExitDescription(), "Terminé",
                    "Le messsage de retour aurait dû correspondre au code 0");
        } finally {
            txManager.commit(txStatus);
        }

    }

    /**
     * Test du job pour le SI AVAL = GERD
     * 
     * @throws Exception
     *             si une erreur inattendue survient lors du test
     */
    @Test(enabled = true)
    public void testJobCasGERDNormal() throws Exception {
        String repertoireSortie = System.getProperty("user.dir") + "/target/test-classes/fr/si2m/red/batch/flux78/outputs/";
        File fichierRepertoireSortie = new File(repertoireSortie);
        if (!fichierRepertoireSortie.exists()) {
            fichierRepertoireSortie.mkdirs();
        }
        String[] parametrageBatch = new String[] { repertoireSortie, "20160118", "GERD", "20151229_170757",
                System.getProperty("user.dir") + "\\target\\test-classes\\fr\\si2m\\red\\batch\\flux78\\outputs\\erreurs_flux78_Publication.log" };

        // RED-107 le path vers le fichier de config est paramétrable depuis les scripts, et donné à la JVM
        System.setProperty("configBatch", System.getProperty("user.dir") + "/target/test-classes/RR701.properties");

        // Préparation de l'exécution sur le profil de test des batchs
        Flux78PublicationExecuteurBatch executeur = new Flux78PublicationExecuteurBatch(parametrageBatch);
        executeur.setProfilsActifsJob(new String[] { "testBatch" });

        // Exécution
        int codeRetour = executeur.executeJob();

        // Teste le code retour
        Assert.assertEquals(codeRetour, CodeRetour.TERMINE.getCode(), "Le batch devait se terminer avec le code 0 (terminé sans erreurs).");

        // Début des tests sur la base
        TransactionStatus txStatus = txManager.getTransaction(new DefaultTransactionDefinition());
        try {
            List<HistoriqueEtatPeriode> listeHistoriqueEtatPeriode = historiqueEtatPeriodeRepository.liste();
            List<MessageControle> listeMessageControle = messageControleRepository.liste();
            List<CompteRenduIntegration> listeCompteRenduIntegration = compteRenduIntegrationRepository.liste();

            Assert.assertEquals(listeHistoriqueEtatPeriode.size(), 6, "Le batch devait créer 6 HistoriqueEtatPeriodes.");

            Assert.assertEquals(listeMessageControle.size(), 2, "Le batch devait conserver 2 MessageControle.");

            Assert.assertEquals(listeCompteRenduIntegration.size(), 2, "Le batch devait conserver 2 CompteRenduIntegration.");

            // Teste la sauvegarde de l'exécution du job en base
            List<JobInstance> jobFlux78Instances = jobExplorer.findJobInstancesByJobName("jobFlux78_Publication", 0, 1);
            Assert.assertEquals(jobFlux78Instances.size(), 1, "Il ne devrait y avoir qu'une seule instance créée pour le job flux78");
            List<JobExecution> executionsJob = jobExplorer.getJobExecutions(jobFlux78Instances.get(0));
            Assert.assertEquals(executionsJob.size(), 1, "Il ne devrait y avoir qu'une seule exécution du job flux78");
            JobExecution executionJobFlux78 = executionsJob.get(0);

            Assert.assertEquals(executionJobFlux78.getExitStatus().getExitCode(), "0", "Le code de retour en base devrait être 0");
            Assert.assertEquals(executionJobFlux78.getExitStatus().getExitDescription(), "Terminé",
                    "Le messsage de retour aurait dû correspondre au code 0");
        } finally {
            txManager.commit(txStatus);
        }

    }

    /**
     * Test du job pour le SI AVAL = EGER
     * 
     * @throws Exception
     *             si une erreur inattendue survient lors du test
     */
    @Test(enabled = true)
    public void testJobCasEGERNormal() throws Exception {
        String repertoireSortie = System.getProperty("user.dir") + "/target/test-classes/fr/si2m/red/batch/flux78/outputs/";
        File fichierRepertoireSortie = new File(repertoireSortie);
        if (!fichierRepertoireSortie.exists()) {
            fichierRepertoireSortie.mkdirs();
        }
        String[] parametrageBatch = new String[] { repertoireSortie, "20160118", "EGER", "20151229_170757",
                System.getProperty("user.dir") + "\\target\\test-classes\\fr\\si2m\\red\\batch\\flux78\\outputs\\erreurs_flux78_Publication.log" };

        // RED-107 le path vers le fichier de config est paramétrable depuis les scripts, et donné à la JVM
        System.setProperty("configBatch", System.getProperty("user.dir") + "/target/test-classes/RR701.properties");

        // Préparation de l'exécution sur le profil de test des batchs
        Flux78PublicationExecuteurBatch executeur = new Flux78PublicationExecuteurBatch(parametrageBatch);
        executeur.setProfilsActifsJob(new String[] { "testBatch" });

        // Exécution
        int codeRetour = executeur.executeJob();

        // Teste le code retour
        Assert.assertEquals(codeRetour, CodeRetour.TERMINE.getCode(), "Le batch devait se terminer avec le code 0 (terminé sans erreurs).");

        // Début des tests sur la base
        TransactionStatus txStatus = txManager.getTransaction(new DefaultTransactionDefinition());
        try {

            // Teste la sauvegarde de l'exécution du job en base
            List<JobInstance> jobFlux78Instances = jobExplorer.findJobInstancesByJobName("jobFlux78_Publication", 0, 1);
            Assert.assertEquals(jobFlux78Instances.size(), 1, "Il ne devrait y avoir qu'une seule instance créée pour le job flux78");
            List<JobExecution> executionsJob = jobExplorer.getJobExecutions(jobFlux78Instances.get(0));
            Assert.assertEquals(executionsJob.size(), 1, "Il ne devrait y avoir qu'une seule exécution du job flux78");
            JobExecution executionJobFlux78 = executionsJob.get(0);

            Assert.assertEquals(executionJobFlux78.getExitStatus().getExitCode(), "0", "Le code de retour en base devrait être 0");
            Assert.assertEquals(executionJobFlux78.getExitStatus().getExitDescription(), "Terminé",
                    "Le messsage de retour aurait dû correspondre au code 0");
        } finally {
            txManager.commit(txStatus);
        }

    }

}
