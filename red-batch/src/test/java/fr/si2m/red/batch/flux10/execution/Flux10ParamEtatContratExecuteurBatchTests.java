package fr.si2m.red.batch.flux10.execution;

import org.springframework.beans.factory.annotation.Autowired;
import org.testng.Assert;
import org.testng.annotations.Test;

import fr.si2m.red.EntiteImportableBatch;
import fr.si2m.red.batch.moteur.CodeRetour;
import fr.si2m.red.core.repository.EntiteImportableRepository;
import fr.si2m.red.parametrage.ParamEtatContratRepository;

/**
 * Tests des batchs du flux 10.
 * 
 * @author poidij
 *
 */
@Test
public class Flux10ParamEtatContratExecuteurBatchTests extends Flux10BatchTests {

    @Autowired
    private ParamEtatContratRepository ParamEtatContratRepository;

    /**
     * Test du job.
     * 
     * @throws Exception
     *             si une erreur inattendue survient lors du test
     */
    @Test
    public void testJobCasNormal() throws Exception {
        // Préparation de l'exécution sur le profil de test des batchs
        Flux10ParamEtatContratExecuteurBatch executeur = new Flux10ParamEtatContratExecuteurBatch(getParametrageBatch());

        // Lancement du batch
        lancerTestJobCasNormal(executeur);
    }

    protected String getNomEntitesTestees() {
        return "ParamEtatContrat";
    }

    @Override
    protected int getNombreEntitesTestees() {
        return 11;
    }

    protected EntiteImportableRepository<? extends EntiteImportableBatch> getReferentielEntitesImportees() {
        return ParamEtatContratRepository;
    }

    /**
     * Test du job.
     * 
     * @throws Exception
     *             si une erreur inattendue survient lors du test
     */
    @Test
    public void testCasLimiteTI() throws Exception {
        String[] parametrageBatch = new String[] {
                System.getProperty("user.dir") + "/target/test-classes/fr/si2m/red/batch/flux10/inputs/erreurformat/" + getNomEntitesTestees()
                        + "-TI.csv",
                System.getProperty("user.dir") + "/target/test-classes/fr/si2m/red/batch/flux10/outputs/erreurs_" + getNomEntitesTestees() + ".log" };

        // RED-107 le path vers le fichier de config est paramétrable depuis les scripts, et donné à la JVM
        System.setProperty("configBatch", System.getProperty("user.dir") + "/target/test-classes/RR000.properties");

        // Préparation de l'exécution sur le profil de test des batchs
        Flux10ParamEtatContratExecuteurBatch executeur = new Flux10ParamEtatContratExecuteurBatch(parametrageBatch);
        executeur.setProfilsActifsJob(new String[] { "testBatch" });

        // Exécution
        int codeRetour = executeur.executeJob();

        // Teste le code retour
        Assert.assertEquals(codeRetour, CodeRetour.ERREUR_FONCTIONNELLE_BLOQUANTE.getCode(), "Le batch devait se terminer avec le code 0.");

    }
}
