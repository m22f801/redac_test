package fr.si2m.red.batch.flux3.execution;

import java.util.Calendar;

import org.testng.Assert;
import org.testng.annotations.Test;

import fr.si2m.red.batch.BatchTests;
import fr.si2m.red.batch.moteur.CodeRetour;

/**
 * Tests des batchs du flux 3.
 * 
 * @author poidij
 *
 */
@Test
public class Flux3BatchTests extends BatchTests {

    /**
     * Test du job.
     * 
     * @throws Exception
     *             si une erreur inattendue survient lors du test
     */
    @Test(enabled = true)
    public void testJobCasNormal() throws Exception {
        String[] parametrageBatch = new String[] { System.getProperty("user.dir") + "/target/test-classes/fr/si2m/red/batch/flux3/outputs",
                "4M-DSN-TU", "C-TU", "151019_10243412",
                System.getProperty("user.dir") + "\\target\\test-classes\\fr\\si2m\\red\\batch\\flux3\\outputs\\erreurs_flux3.log" };

        // RED-107 le path vers le fichier de config est paramétrable depuis les scripts, et donné à la JVM
        System.setProperty("configBatch", System.getProperty("user.dir") + "/target/test-classes/RR300.properties");

        // Préparation de l'exécution sur le profil de test des batchs
        Flux3ExecuteurBatch executeur = new Flux3ExecuteurBatch(parametrageBatch);
        executeur.setProfilsActifsJob(new String[] { "testBatch" });

        // Exécution
        int codeRetour = executeur.executeJob();

        // Teste le code retour
        Assert.assertEquals(codeRetour, CodeRetour.TERMINE.getCode(), "Le batch devait se terminer avec le code 0 (terminé sans erreurs).");
    }

    /**
     * Teste de retrait d'un an sur le 29/02 d'une année bisextile.
     * 
     * @throws Exception
     *             si une erreur inattendue survient lors du test
     */
    @Test
    public void testAnneeBisextile() throws Exception {
        Calendar cal = Calendar.getInstance();
        cal.set(2016, Calendar.FEBRUARY, 29);
        Assert.assertEquals(cal.get(Calendar.YEAR), 2016);
        Assert.assertEquals(cal.get(Calendar.MONTH), Calendar.FEBRUARY);
        Assert.assertEquals(cal.get(Calendar.DAY_OF_MONTH), 29);

        cal.add(Calendar.YEAR, -1);

        Assert.assertEquals(cal.get(Calendar.YEAR), 2015);
        Assert.assertEquals(cal.get(Calendar.MONTH), Calendar.FEBRUARY);
        Assert.assertEquals(cal.get(Calendar.DAY_OF_MONTH), 28);
    }
}
