package fr.si2m.red.batch.test;

import java.util.Date;

import lombok.Data;
import lombok.EqualsAndHashCode;
import fr.si2m.red.EntiteImportableBatch;

/**
 * Classe de test.
 * 
 * @author nortaina
 *
 */
@Data
@EqualsAndHashCode(callSuper = false, of = { "nom", "prenom" })
public class Test extends EntiteImportableBatch {
    /**
     * UID de version.
     */
    private static final long serialVersionUID = 1L;

    private boolean ligneEnCoursImportBatch;
    private String nom;
    private String prenom;
    private Date dateNaissance;
    private Double salaire;

    @Override
    public Object getId() {
        return getNom() + ", " + getPrenom();
    }

}
