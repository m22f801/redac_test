package fr.si2m.red.batch.flux10.execution;

import java.io.File;
import java.io.FileInputStream;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.Assert;
import org.testng.annotations.Test;

import fr.si2m.red.EntiteImportableBatch;
import fr.si2m.red.batch.moteur.CodeRetour;
import fr.si2m.red.core.repository.EntiteImportableRepository;
import fr.si2m.red.parametrage.ParamNatureBaseComposantsRepository;

/**
 * Tests des batchs du flux 10.
 * 
 * @author poidij
 *
 */
@Test
public class Flux10ParamNatureBaseComposantsExecuteurBatchTests extends Flux10BatchTests {

    @Autowired
    private ParamNatureBaseComposantsRepository paramNatureBaseComposantsRepository;

    /**
     * Test du job.
     * 
     * @throws Exception
     *             si une erreur inattendue survient lors du test
     */
    @Test
    public void testJobCasNormal() throws Exception {
        // Préparation de l'exécution sur le profil de test des batchs
        Flux10ParamNatureBaseComposantsExecuteurBatch executeur = new Flux10ParamNatureBaseComposantsExecuteurBatch(getParametrageBatch());

        // Lancement du batch
        lancerTestJobCasNormal(executeur);
    }

    protected String getNomEntitesTestees() {
        return "ParamNatureBaseComposants";
    }

    @Override
    protected int getNombreEntitesTestees() {
        return 46;
    }

    protected EntiteImportableRepository<? extends EntiteImportableBatch> getReferentielEntitesImportees() {
        return paramNatureBaseComposantsRepository;
    }

    /**
     * Test du job.
     * 
     * @throws Exception
     *             si une erreur inattendue survient lors du test
     */
    @Test
    public void testCasNormalSurFichierReduitDeTI() throws Exception {
        String[] parametrageBatch = new String[] {
                System.getProperty("user.dir") + "/target/test-classes/fr/si2m/red/batch/flux10/inputs/valides/" + getNomEntitesTestees() + "-TI.csv",
                System.getProperty("user.dir") + "/target/test-classes/fr/si2m/red/batch/flux10/outputs/erreurs_" + getNomEntitesTestees() + ".log" };

        // RED-107 le path vers le fichier de config est paramétrable depuis les scripts, et donné à la JVM
        System.setProperty("configBatch", System.getProperty("user.dir") + "/target/test-classes/RR000.properties");

        // Préparation de l'exécution sur le profil de test des batchs
        Flux10ParamNatureBaseComposantsExecuteurBatch executeur = new Flux10ParamNatureBaseComposantsExecuteurBatch(parametrageBatch);
        executeur.setProfilsActifsJob(new String[] { "testBatch" });

        // Exécution
        int codeRetour = executeur.executeJob();

        // Teste le code retour
        Assert.assertEquals(codeRetour, CodeRetour.TERMINE.getCode(), "Le batch devait se terminer avec le code 0.");

    }

    /**
     * Test du job.
     * 
     * @throws Exception
     *             si une erreur inattendue survient lors du test
     */
    @Test
    public void testCasLimiteDuplicationLignes() throws Exception {
        String[] parametrageBatch = new String[] {
                System.getProperty("user.dir") + "/target/test-classes/fr/si2m/red/batch/flux10/inputs/erreurformat/" + getNomEntitesTestees()
                        + "-unicite.csv",
                System.getProperty("user.dir") + "/target/test-classes/fr/si2m/red/batch/flux10/outputs/erreurs_" + getNomEntitesTestees() + ".log" };

        File errorLog = new File(
                System.getProperty("user.dir") + "/target/test-classes/fr/si2m/red/batch/flux10/outputs/erreurs_" + getNomEntitesTestees() + ".log");
        if (errorLog.exists()) {
            // On part sur des logs frais
            errorLog.delete();
        }

        // RED-107 le path vers le fichier de config est paramétrable depuis les scripts, et donné à la JVM
        System.setProperty("configBatch", System.getProperty("user.dir") + "/target/test-classes/RR000.properties");

        // Préparation de l'exécution sur le profil de test des batchs
        Flux10ParamNatureBaseComposantsExecuteurBatch executeur = new Flux10ParamNatureBaseComposantsExecuteurBatch(parametrageBatch);
        executeur.setProfilsActifsJob(new String[] { "testBatch" });

        // Exécution
        int codeRetour = executeur.executeJob();

        // Teste le code retour
        Assert.assertEquals(codeRetour, CodeRetour.ERREUR_FONCTIONNELLE_BLOQUANTE.getCode(), "Le batch devait se terminer avec le code 40.");

        // Teste la présence de message d'unicité dans les logs d'erreur
        FileInputStream errorLogStream = new FileInputStream(errorLog);
        try {
            String errorLogContent = IOUtils.toString(errorLogStream);
            Assert.assertTrue(StringUtils.contains(errorLogContent, "ne respecte pas la règle d'unicité"));
        } finally {
            IOUtils.closeQuietly(errorLogStream);
        }

    }

    /**
     * Test du job.
     * 
     * @throws Exception
     *             si une erreur inattendue survient lors du test
     */
    @Test
    public void testCasLimitesTI() throws Exception {
        String[] parametrageBatch = new String[] {
                System.getProperty("user.dir") + "/target/test-classes/fr/si2m/red/batch/flux10/inputs/erreurformat/" + getNomEntitesTestees()
                        + "-TI.csv",
                System.getProperty("user.dir") + "/target/test-classes/fr/si2m/red/batch/flux10/outputs/erreurs_" + getNomEntitesTestees() + ".log" };

        // RED-107 le path vers le fichier de config est paramétrable depuis les scripts, et donné à la JVM
        System.setProperty("configBatch", System.getProperty("user.dir") + "/target/test-classes/RR000.properties");

        // Préparation de l'exécution sur le profil de test des batchs
        Flux10ParamNatureBaseComposantsExecuteurBatch executeur = new Flux10ParamNatureBaseComposantsExecuteurBatch(parametrageBatch);
        executeur.setProfilsActifsJob(new String[] { "testBatch" });

        // Exécution
        int codeRetour = executeur.executeJob();

        // Teste le code retour
        Assert.assertEquals(codeRetour, CodeRetour.ERREUR_FONCTIONNELLE_BLOQUANTE.getCode(), "Le batch devait se terminer avec le code 40.");

    }

}
