package fr.si2m.red.batch.flux1.execution;

import java.util.List;

import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobInstance;
import org.springframework.batch.core.explore.JobExplorer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;
import org.testng.Assert;
import org.testng.annotations.Test;

import fr.si2m.red.batch.BatchTests;
import fr.si2m.red.batch.moteur.CodeRetour;
import fr.si2m.red.contrat.Client;
import fr.si2m.red.contrat.ClientRepository;
import fr.si2m.red.contrat.Contrat;
import fr.si2m.red.contrat.ContratRepository;
import fr.si2m.red.contrat.Intermediaire;
import fr.si2m.red.contrat.IntermediaireRepository;
import fr.si2m.red.contrat.Tarif;
import fr.si2m.red.contrat.TarifRepository;

/**
 * Tests des batchs du flux 1.
 * 
 * @author nortaina
 *
 */
@Test
public class Flux1BatchTests extends BatchTests {

    @Autowired
    private PlatformTransactionManager txManager;
    @Autowired
    private JobExplorer jobExplorer;
    @Autowired
    private IntermediaireRepository intermediaireRepository;
    @Autowired
    private ClientRepository clientRepository;
    @Autowired
    private ContratRepository contratRepository;
    @Autowired
    private TarifRepository tarifRepository;

    /**
     * Test du job , mode de chargement normal
     * 
     * @throws Exception
     *             si une erreur inattendue survient lors du test
     */
    @Test
    public void testJobCasNormal() throws Exception {
        String[] parametrageBatch = new String[] { System.getProperty("user.dir") + "/target/test-classes/fr/si2m/red/batch/flux1/inputs/valides",
                "D06102015A", "normal",
                System.getProperty("user.dir") + "\\target\\test-classes\\fr\\si2m\\red\\batch\\flux1\\outputs\\erreurs.log" };
        // Préparation de l'exécution sur le profil de test des batchs
        Flux1ExecuteurBatch executeur = new Flux1ExecuteurBatch(parametrageBatch);
        executeur.setProfilsActifsJob(new String[] { "testBatch" });

        // RED-107 le path vers le fichier de config est paramétrable depuis les scripts, et donné à la JVM
        System.setProperty("configBatch", System.getProperty("user.dir") + "/target/test-classes/RR100.properties");

        // Exécution
        int codeRetour = executeur.executeJob();

        // Teste le code retour
        Assert.assertEquals(codeRetour, CodeRetour.TERMINE.getCode(), "Le batch devait se terminer avec le code 0 (terminé sans erreurs).");

        // Début des tests sur la base
        TransactionStatus txStatus = txManager.getTransaction(new DefaultTransactionDefinition());
        try {
            // Teste la bonne insertion des 49 intermediaires dans la base de test
            List<Intermediaire> intermediaires = intermediaireRepository.liste();
            List<Client> clients = clientRepository.liste();
            List<Contrat> contrats = contratRepository.liste();
            List<Tarif> tarifs = tarifRepository.liste();
            Assert.assertEquals(intermediaires.size(), 4, "Le batch devait insérer 4 intermediaires en base.");
            Assert.assertEquals(clients.size(), 6, "Le batch devait insérer 6 clients en base.");
            Assert.assertEquals(contrats.size(), 3, "Le batch devait insérer 3 contrats en base.");
            Assert.assertEquals(tarifs.size(), 3, "Le batch devait insérer 3 tarifs en base.");

            Contrat contrat1 = contratRepository.getContrat("000893600528002", 20130101);
            Contrat contrat2 = contratRepository.getContrat("000893600528002", 20140501);

            Assert.assertNotNull(contrat1);
            Assert.assertNotNull(contrat1.getDateFinSituationLigne(), "La date de fin de situation 1 devrait être remplie");
            Assert.assertNotNull(contrat2);
            Assert.assertNull(contrat2.getDateFinSituationLigne(), "La date de fin de situation devrait être vide");

            Assert.assertEquals(contrat1.getDateFinSituationLigne().intValue(), 20140430, "La date de fin de situation devrait être '20140531'");

            Assert.assertEquals(intermediaires.get(0).getAuditUtilisateurCreation(), "RR100",
                    "Les contrats insérés doivent comporter la notion d'utilisateur de création.");
            Assert.assertNotNull(intermediaires.get(0).getAuditDateCreation(),
                    "Les contrats insérés doivent comporter la notion de date de création.");

            // Teste la sauvegarde de l'exécution du job en base
            List<JobInstance> jobFlux1Instances = jobExplorer.findJobInstancesByJobName("jobFlux1", 0, 1);
            Assert.assertEquals(jobFlux1Instances.size(), 1, "Il ne devrait y avoir qu'une seule instance créée pour le job flux 1");
            List<JobExecution> executionsJob = jobExplorer.getJobExecutions(jobFlux1Instances.get(0));
            Assert.assertEquals(executionsJob.size(), 1, "Il ne devrait y avoir qu'une seule exécution du job flux 1");
            JobExecution executionJobFlux1 = executionsJob.get(0);

            Assert.assertEquals(executionJobFlux1.getExitStatus().getExitCode(), "0", "Le code de retour en base devrait être 0");
            Assert.assertEquals(executionJobFlux1.getExitStatus().getExitDescription(), "Terminé",
                    "Le messsage de retour aurait dû correspondre au code 0");
        } finally {
            txManager.commit(txStatus);
        }

    }

    /**
     * Test du job , mode de chargement normal
     * 
     * @throws Exception
     *             si une erreur inattendue survient lors du test
     */
    @Test
    public void testJobCasNormalModePopulationEtendue() throws Exception {
        String[] parametrageBatch = new String[] { System.getProperty("user.dir") + "/target/test-classes/fr/si2m/red/batch/flux1/inputs/valides",
                "D06102015P", "populationEtendue",
                System.getProperty("user.dir") + "\\target\\test-classes\\fr\\si2m\\red\\batch\\flux1\\outputs\\erreurs.log" };

        // RED-107 le path vers le fichier de config est paramétrable depuis les scripts, et donné à la JVM
        System.setProperty("configBatch", System.getProperty("user.dir") + "/target/test-classes/RR100.properties");

        // Préparation de l'exécution sur le profil de test des batchs
        Flux1ExecuteurBatch executeur = new Flux1ExecuteurBatch(parametrageBatch);
        executeur.setProfilsActifsJob(new String[] { "testBatch" });

        // Exécution
        int codeRetour = executeur.executeJob();

        // Teste le code retour
        Assert.assertEquals(codeRetour, CodeRetour.TERMINE.getCode(), "Le batch devait se terminer avec le code 0 (terminé sans erreurs).");

        // Début des tests sur la base
        TransactionStatus txStatus = txManager.getTransaction(new DefaultTransactionDefinition());
        try {
            // Teste la bonne insertion des 49 intermediaires dans la base de test
            List<Intermediaire> intermediaires = intermediaireRepository.liste();
            List<Client> clients = clientRepository.liste();
            List<Contrat> contrats = contratRepository.liste();
            List<Tarif> tarifs = tarifRepository.liste();
            Assert.assertEquals(intermediaires.size(), 4, "Le batch devait insérer 4 intermediaires en base.");
            Assert.assertEquals(clients.size(), 6, "Le batch devait insérer 6 clients en base.");
            Assert.assertEquals(contrats.size(), 3, "Le batch devait insérer 3 contrats en base.");
            Assert.assertEquals(tarifs.size(), 3, "Le batch devait insérer 3 tarifs en base.");

            Contrat contrat1 = contratRepository.getContrat("000893600528002", 20130101);
            Contrat contrat2 = contratRepository.getContrat("000893600528002", 20140501);

            Assert.assertNotNull(contrat1);
            Assert.assertNotNull(contrat1.getDateFinSituationLigne(), "La date de fin de situation 1 devrait être remplie");
            Assert.assertNotNull(contrat2);
            Assert.assertNull(contrat2.getDateFinSituationLigne(), "La date de fin de situation devrait être vide");

            Assert.assertEquals(contrat1.getDateFinSituationLigne().intValue(), 20140430, "La date de fin de situation devrait être '20140531'");

            Assert.assertEquals(intermediaires.get(0).getAuditUtilisateurCreation(), "RR100",
                    "Les contrats insérés doivent comporter la notion d'utilisateur de création.");
            Assert.assertNotNull(intermediaires.get(0).getAuditDateCreation(),
                    "Les contrats insérés doivent comporter la notion de date de création.");

            // Teste la sauvegarde de l'exécution du job en base
            List<JobInstance> jobFlux1Instances = jobExplorer.findJobInstancesByJobName("jobFlux1", 0, 1);
            Assert.assertEquals(jobFlux1Instances.size(), 1, "Il ne devrait y avoir qu'une seule instance créée pour le job flux 1");
            List<JobExecution> executionsJob = jobExplorer.getJobExecutions(jobFlux1Instances.get(0));
            Assert.assertEquals(executionsJob.size(), 1, "Il ne devrait y avoir qu'une seule exécution du job flux 1");
            JobExecution executionJobFlux1 = executionsJob.get(0);

            Assert.assertEquals(executionJobFlux1.getExitStatus().getExitCode(), "0", "Le code de retour en base devrait être 0");
            Assert.assertEquals(executionJobFlux1.getExitStatus().getExitDescription(), "Terminé",
                    "Le messsage de retour aurait dû correspondre au code 0");
        } finally {
            txManager.commit(txStatus);
        }

    }

}
