package fr.si2m.red.batch.facade;

import java.util.Set;

import org.testng.Assert;
import org.testng.annotations.Test;

import fr.si2m.red.batch.BatchTests;
import fr.si2m.red.batch.flux1.execution.Flux1ExecuteurBatch;
import fr.si2m.red.batch.moteur.CodeRetour;
import fr.si2m.red.batch.moteur.execution.ExecuteurBatch;
import fr.si2m.red.batch.test.execution.TestExecuteurImport;

/**
 * Tests de la façade de batchs.
 * 
 * @author nortaina
 *
 */
@Test
public class BatchFacadeTests extends BatchTests {

    /**
     * Test du scan des exécuteurs par le registre.
     * 
     * @throws Exception
     *             si une erreur inattendue survient lors du test
     */
    @Test
    public void testScanDuRegistre() throws Exception {
        Set<String> executeursDisponibles = RegistreExecuteursBatchs.getBatchsExecutables();

        // Test au moins 1 car d'autres vont venir s'ajouter
        Assert.assertTrue(executeursDisponibles.size() > 0, "Au moins un exécuteur batch devrait être connu de la façade");

        // Test disponibilité du batch flux 1
        Assert.assertTrue(executeursDisponibles.contains("flux1"), "Le batch flux 1 devrait être enregistré par la façade");
    }

    /**
     * Test de la récupération de l'exécuteur du job flux 1 via la façade.
     * 
     * @throws Exception
     *             si une erreur inattendue survient lors du test
     */
    @Test
    public void testCreationExecuteurJobFlux1() throws Exception {
        String[] parametrageBatch = new String[] { "classpath:fr/si2m/red/batch/flux1/inputs/valides", "D150722A", "normal", "C:\\tmp\\errors.log" };
        ExecuteurBatch executeur = RegistreExecuteursBatchs.getExecuteurPourBatch("flux1", parametrageBatch);

        Assert.assertEquals(executeur.getClass(), Flux1ExecuteurBatch.class, "L'exécuteur du batch flux 1 enregistré n'est pas le bon");
    }

    /**
     * Test de l'exécution d'un batch non connu de la façade.
     * 
     * @throws Exception
     *             si une erreur inattendue survient lors du test
     */
    @Test
    public void testExecuteBatchInconnu() throws Exception {
        int codeRetour = BatchFacade.executeBatch(new String[] { "testInconnu", "123" });
        Assert.assertEquals(codeRetour, CodeRetour.ERREUR_TECHNIQUE_BLOQUANTE.getCode());
    }

    /**
     * Test de l'exécution du job de test via la façade.
     * 
     * @throws Exception
     *             si une erreur inattendue survient lors du test
     */
    @Test
    public void testExecuteTestImport() throws Exception {
        int codeRetour = BatchFacade.executeBatch(new String[] { "testImport", TestExecuteurImport.FICHIER_VALIDE });
        Assert.assertEquals(codeRetour, CodeRetour.TERMINE.getCode());
    }

    /**
     * Test sur import de fichier avec erreur fonctionnelle non bloquante.
     */
    @org.testng.annotations.Test
    public void testTestImportErreurFonctionnelleNonBloquante() {
        int codeRetour = BatchFacade.executeBatch(new String[] { "testImport", TestExecuteurImport.FICHIER_VALIDE_ERR_NON_BLOQ });
        Assert.assertEquals(codeRetour, CodeRetour.ERREUR_FONCTIONNELLE_NON_BLOQUANTE.getCode());
    }

    /**
     * Test sur import de fichier avec erreur fonctionnelle bloquante.
     */
    @org.testng.annotations.Test
    public void testTestImportErreurFonctionnelleBloquante() {
        int codeRetour = BatchFacade.executeBatch(new String[] { "testImport", TestExecuteurImport.FICHIER_VALIDE_ERR_BLOQ });
        Assert.assertEquals(codeRetour, CodeRetour.ERREUR_FONCTIONNELLE_BLOQUANTE.getCode());
    }

    /**
     * Test sur import de fichier non existant (pas de lancement de batch).
     */
    @org.testng.annotations.Test
    public void testTestImportNonExecute() {
        int codeRetour = BatchFacade.executeBatch(new String[] { "testImport", TestExecuteurImport.FICHIER_INEXISTANT });
        Assert.assertEquals(codeRetour, CodeRetour.ERREUR_FONCTIONNELLE_BLOQUANTE.getCode());
    }

    /**
     * Test sur import de fichier avec erreur technique (bloquante).
     */
    @org.testng.annotations.Test
    public void testTestImportErreurTechnique() {
        int codeRetour = BatchFacade.executeBatch(new String[] { "testImport", TestExecuteurImport.FICHIER_MAL_FORME });
        Assert.assertEquals(codeRetour, CodeRetour.ERREUR_TECHNIQUE_BLOQUANTE.getCode());
    }

}
