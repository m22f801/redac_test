package fr.si2m.red.batch.flux4.execution;

import java.util.List;

import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobInstance;
import org.springframework.batch.core.explore.JobExplorer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;
import org.testng.Assert;
import org.testng.annotations.Test;

import fr.si2m.red.batch.BatchTests;
import fr.si2m.red.batch.moteur.CodeRetour;
import fr.si2m.red.dsn.AdhesionEtablissementMois;
import fr.si2m.red.dsn.AdhesionEtablissementMoisRepository;
import fr.si2m.red.dsn.Affiliation;
import fr.si2m.red.dsn.AffiliationRepository;
import fr.si2m.red.dsn.ArretTravail;
import fr.si2m.red.dsn.ArretTravailRepository;
import fr.si2m.red.dsn.AyantDroit;
import fr.si2m.red.dsn.AyantDroitRepository;
import fr.si2m.red.dsn.BaseAssujettie;
import fr.si2m.red.dsn.BaseAssujettieRepository;
import fr.si2m.red.dsn.ChangementIndividu;
import fr.si2m.red.dsn.ChangementIndividuRepository;
import fr.si2m.red.dsn.ComposantBaseAssujettie;
import fr.si2m.red.dsn.ComposantBaseAssujettieRepository;
import fr.si2m.red.dsn.ComposantVersement;
import fr.si2m.red.dsn.ComposantVersementRepository;
import fr.si2m.red.dsn.Contact;
import fr.si2m.red.dsn.ContactRepository;
import fr.si2m.red.dsn.ContratTravail;
import fr.si2m.red.dsn.ContratTravailRepository;
import fr.si2m.red.dsn.CotisationEtablissement;
import fr.si2m.red.dsn.CotisationEtablissementRepository;
import fr.si2m.red.dsn.Individu;
import fr.si2m.red.dsn.IndividuRepository;
import fr.si2m.red.dsn.Remuneration;
import fr.si2m.red.dsn.RemunerationRepository;
import fr.si2m.red.dsn.Versement;
import fr.si2m.red.dsn.VersementIndividu;
import fr.si2m.red.dsn.VersementIndividuRepository;
import fr.si2m.red.dsn.VersementRepository;

/**
 * Tests du batch de promotion du flux 4. ( 403 )
 * 
 * @author poidij
 *
 */
@Test
public class Flux4PromotionBatchTests extends BatchTests {

    @Autowired
    private PlatformTransactionManager txManager;
    @Autowired
    private JobExplorer jobExplorer;
    @Autowired
    private AdhesionEtablissementMoisRepository adhesionEtablissementMoisRepository;
    @Autowired
    private ContactRepository contactRepository;
    @Autowired
    private CotisationEtablissementRepository cotisationEtablissementRepository;
    @Autowired
    private VersementRepository versementRepository;
    @Autowired
    private ComposantVersementRepository composantVersementRepository;
    @Autowired
    private IndividuRepository individuRepository;
    @Autowired
    private ChangementIndividuRepository changementIndividuRepository;
    @Autowired
    private VersementIndividuRepository versementIndividuRepository;
    @Autowired
    private RemunerationRepository remunerationRepository;
    @Autowired
    private ContratTravailRepository contratTravailRepository;
    @Autowired
    private ArretTravailRepository arretTravailRepository;
    @Autowired
    private AffiliationRepository affiliationRepository;
    @Autowired
    private AyantDroitRepository ayantDroitRepository;
    @Autowired
    private BaseAssujettieRepository baseAssujettieRepository;
    @Autowired
    private ComposantBaseAssujettieRepository composantBaseAssujettieRepository;

    /**
     * Test du job.
     * 
     * @throws Exception
     *             si une erreur inattendue survient lors du test
     */
    @Test(enabled = true)
    public void testJobCasNormal() throws Exception {
        String[] parametrageBatch = new String[] {
                System.getProperty("user.dir") + "\\target\\test-classes\\fr\\si2m\\red\\batch\\flux3\\outputs\\erreurs_flux4_Promotion.log" };

        // RED-107 le path vers le fichier de config est paramétrable depuis les scripts, et donné à la JVM
        System.setProperty("configBatch", System.getProperty("user.dir") + "/target/test-classes/RR403.properties");

        // Préparation de l'exécution sur le profil de test des batchs
        Flux4PromotionExecuteurBatch executeur = new Flux4PromotionExecuteurBatch(parametrageBatch);
        executeur.setProfilsActifsJob(new String[] { "testBatch" });

        // Exécution
        int codeRetour = executeur.executeJob();

        // Teste le code retour
        Assert.assertEquals(codeRetour, CodeRetour.TERMINE.getCode(), "Le batch devait se terminer avec le code 0 (terminé sans erreurs).");

        TransactionStatus txStatus = txManager.getTransaction(new DefaultTransactionDefinition());
        try {
            // Teste la bonne insertion des 49 intermediaires dans la base de test
            List<AdhesionEtablissementMois> listeAdhesionEtablissementMois = adhesionEtablissementMoisRepository.liste();
            List<Contact> listeContact = contactRepository.liste();
            List<CotisationEtablissement> listeCotisationEtablissement = cotisationEtablissementRepository.liste();
            List<Versement> listeVersement = versementRepository.liste();
            List<ComposantVersement> listeComposantVersement = composantVersementRepository.liste();
            List<Individu> listeIndividu = individuRepository.liste();
            List<ChangementIndividu> listeChangementIndividu = changementIndividuRepository.liste();
            List<VersementIndividu> listeVersementIndividu = versementIndividuRepository.liste();
            List<Remuneration> listeRemuneration = remunerationRepository.liste();
            List<ContratTravail> listeContratTravail = contratTravailRepository.liste();
            List<ArretTravail> listeArretTravail = arretTravailRepository.liste();
            List<Affiliation> listeAffiliation = affiliationRepository.liste();
            List<AyantDroit> listeAyantDroit = ayantDroitRepository.liste();
            List<BaseAssujettie> listeBaseAssujettie = baseAssujettieRepository.liste();
            List<ComposantBaseAssujettie> listeComposantBaseAssujettie = composantBaseAssujettieRepository.liste();

            Assert.assertEquals(listeAdhesionEtablissementMois.size(), 4, "Le batch devait conserver 4 AdhesionEtablissementMois.");
            for (AdhesionEtablissementMois entite : listeAdhesionEtablissementMois) {
                if ("TU4_KO".equals(entite.getAuditUtilisateurCreation())) {
                    Assert.fail("L'entité " + entite.getClass() + " (" + entite.toString() + ") devrait être supprimé ");
                }
            }
            Assert.assertEquals(listeContact.size(), 4, "Le batch devait conserver 4 Contact.");
            for (Contact entite : listeContact) {
                if ("TU4_KO".equals(entite.getAuditUtilisateurCreation())) {
                    Assert.fail("L'entité " + entite.getClass() + " (" + entite.toString() + ") devrait être supprimé ");
                }
            }
            Assert.assertEquals(listeCotisationEtablissement.size(), 4, "Le batch devait conserver 4 CotisationEtablissement.");
            for (CotisationEtablissement entite : listeCotisationEtablissement) {
                if ("TU4_KO".equals(entite.getAuditUtilisateurCreation())) {
                    Assert.fail("L'entité " + entite.getClass() + " (" + entite.toString() + ") devrait être supprimé ");
                }
            }
            Assert.assertEquals(listeVersement.size(), 4, "Le batch devait conserver 4 Versement.");
            for (Versement entite : listeVersement) {
                if ("TU4_KO".equals(entite.getAuditUtilisateurCreation())) {
                    Assert.fail("L'entité " + entite.getClass() + " (" + entite.toString() + ") devrait être supprimé ");
                }
            }
            Assert.assertEquals(listeComposantVersement.size(), 4, "Le batch devait conserver 4 ComposantVersement.");
            for (ComposantVersement entite : listeComposantVersement) {
                if ("TU4_KO".equals(entite.getAuditUtilisateurCreation())) {
                    Assert.fail("L'entité " + entite.getClass() + " (" + entite.toString() + ") devrait être supprimé ");
                }
            }
            Assert.assertEquals(listeIndividu.size(), 4, "Le batch devait conserver 4 Individu.");
            for (Individu entite : listeIndividu) {
                if ("TU4_KO".equals(entite.getAuditUtilisateurCreation())) {
                    Assert.fail("L'entité " + entite.getClass() + " (" + entite.toString() + ") devrait être supprimé ");
                }
            }
            Assert.assertEquals(listeChangementIndividu.size(), 4, "Le batch devait conserver 4 ChangementIndividu.");
            for (ChangementIndividu entite : listeChangementIndividu) {
                if ("TU4_KO".equals(entite.getAuditUtilisateurCreation())) {
                    Assert.fail("L'entité " + entite.getClass() + " (" + entite.toString() + ") devrait être supprimé ");
                }
            }
            Assert.assertEquals(listeVersementIndividu.size(), 4, "Le batch devait conserver 4 VersementIndividu.");
            for (VersementIndividu entite : listeVersementIndividu) {
                if ("TU4_KO".equals(entite.getAuditUtilisateurCreation())) {
                    Assert.fail("L'entité " + entite.getClass() + " (" + entite.toString() + ") devrait être supprimé ");
                }
            }
            Assert.assertEquals(listeRemuneration.size(), 4, "Le batch devait conserver 4 Remuneration.");
            for (Remuneration entite : listeRemuneration) {
                if ("TU4_KO".equals(entite.getAuditUtilisateurCreation())) {
                    Assert.fail("L'entité " + entite.getClass() + " (" + entite.toString() + ") devrait être supprimé ");
                }
            }
            Assert.assertEquals(listeContratTravail.size(), 4, "Le batch devait conserver 4 ContratTravail.");
            for (ContratTravail entite : listeContratTravail) {
                if ("TU4_KO".equals(entite.getAuditUtilisateurCreation())) {
                    Assert.fail("L'entité " + entite.getClass() + " (" + entite.toString() + ") devrait être supprimé ");
                }
            }
            Assert.assertEquals(listeArretTravail.size(), 4, "Le batch devait conserver 4 ArretTravail.");
            for (ArretTravail entite : listeArretTravail) {
                if ("TU4_KO".equals(entite.getAuditUtilisateurCreation())) {
                    Assert.fail("L'entité " + entite.getClass() + " (" + entite.toString() + ") devrait être supprimé ");
                }
            }
            Assert.assertEquals(listeAffiliation.size(), 4, "Le batch devait conserver 4 Affiliation.");
            for (Affiliation entite : listeAffiliation) {
                if ("TU4_KO".equals(entite.getAuditUtilisateurCreation())) {
                    Assert.fail("L'entité " + entite.getClass() + " (" + entite.toString() + ") devrait être supprimé ");
                }
            }
            Assert.assertEquals(listeAyantDroit.size(), 4, "Le batch devait conserver 4 AyantDroit.");
            for (AyantDroit entite : listeAyantDroit) {
                if ("TU4_KO".equals(entite.getAuditUtilisateurCreation())) {
                    Assert.fail("L'entité " + entite.getClass() + " (" + entite.toString() + ") devrait être supprimé ");
                }
            }
            Assert.assertEquals(listeBaseAssujettie.size(), 4, "Le batch devait conserver 4 BaseAssujettie.");
            for (BaseAssujettie entite : listeBaseAssujettie) {
                if ("TU4_KO".equals(entite.getAuditUtilisateurCreation())) {
                    Assert.fail("L'entité " + entite.getClass() + " (" + entite.toString() + ") devrait être supprimé ");
                }
            }
            Assert.assertEquals(listeComposantBaseAssujettie.size(), 4, "Le batch devait conserver 4 ComposantBaseAssujettie.");
            for (ComposantBaseAssujettie entite : listeComposantBaseAssujettie) {
                if ("TU4_KO".equals(entite.getAuditUtilisateurCreation())) {
                    Assert.fail("L'entité " + entite.getClass() + " (" + entite.toString() + ") devrait être supprimé ");
                }
            }

            // Teste la sauvegarde de l'exécution du job en base
            List<JobInstance> jobFlux4Instances = jobExplorer.findJobInstancesByJobName("jobFlux4_Promotion", 0, 1);
            Assert.assertEquals(jobFlux4Instances.size(), 1, "Il ne devrait y avoir qu'une seule instance créée pour le job flux 4");
            List<JobExecution> executionsJob = jobExplorer.getJobExecutions(jobFlux4Instances.get(0));
            Assert.assertEquals(executionsJob.size(), 1, "Il ne devrait y avoir qu'une seule exécution du job flux 4");
            JobExecution executionJobFlux4 = executionsJob.get(0);

            Assert.assertEquals(executionJobFlux4.getExitStatus().getExitCode(), "0", "Le code de retour en base devrait être 0");
            Assert.assertEquals(executionJobFlux4.getExitStatus().getExitDescription(), "Terminé",
                    "Le messsage de retour aurait dû correspondre au code 0");
        } finally {
            txManager.commit(txStatus);
        }

    }

}
