package fr.si2m.red.batch.flux56.execution;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

import org.apache.commons.io.IOUtils;
import org.testng.Assert;
import org.testng.annotations.Test;

import fr.si2m.red.batch.BatchTests;
import fr.si2m.red.batch.moteur.CodeRetour;

/**
 * Tests des batchs du flux 56.
 * 
 * @author delortj
 *
 */
@Test
public class Flux56BatchTests extends BatchTests {

    /**
     * Test du job pour le SI_AVAL WQUI
     * 
     * @throws Exception
     *             si une erreur inattendue survient lors du test
     */
    @Test
    public void testJobCasNormalWQUI() throws Exception {
        String repertoireSortie = System.getProperty("user.dir") + "/target/test-classes/fr/si2m/red/batch/flux56/outputs/";
        File fichierRepertoireSortie = new File(repertoireSortie);
        if (!fichierRepertoireSortie.exists()) {
            fichierRepertoireSortie.mkdirs();
        }
        String[] parametrageBatch = new String[] { repertoireSortie, "20151222", "WQUI", "20160116_125526",
                System.getProperty("user.dir") + "\\target\\test-classes\\fr\\si2m\\red\\batch\\flux56\\outputs\\erreurs_flux56.log" };

        // RED-107 le path vers le fichier de config est paramétrable depuis les scripts, et donné à la JVM
        System.setProperty("configBatch", System.getProperty("user.dir") + "/target/test-classes/RR500.properties");

        // Préparation de l'exécution sur le profil de test des batchs
        Flux56ExecuteurBatch executeur = new Flux56ExecuteurBatch(parametrageBatch);
        executeur.setProfilsActifsJob(new String[] { "testBatch" });

        // Exécution
        int codeRetour = executeur.executeJob();

        // Teste le code retour
        Assert.assertEquals(codeRetour, CodeRetour.TERMINE.getCode(), "Le batch devait se terminer avec le code 0 (terminé sans erreurs).");

        // Validation WQUI
        FileReader lecteurFichierWQUIValide = new FileReader(
                System.getProperty("user.dir") + "/target/test-classes/fr/si2m/red/batch/flux56/inputs/valides/WF000_E3_MDG_WQUI.sor");
        String contenuWQUIValide = IOUtils.toString(lecteurFichierWQUIValide);
        lecteurFichierWQUIValide.close();

        FileReader lecteurFichierWQUI = new FileReader(
                System.getProperty("user.dir") + "/target/test-classes/fr/si2m/red/batch/flux56/outputs/20160116_125526_RR500_E4_MDG_WQUI.sor");
        BufferedReader lecteurContenuWQUI = new BufferedReader(lecteurFichierWQUI);
        String ligneWQUI;
        while ((ligneWQUI = lecteurContenuWQUI.readLine()) != null) {
            Assert.assertTrue(contenuWQUIValide.contains(ligneWQUI),
                    "La ligne " + ligneWQUI + " n'apparaît pas dans le fichier WQUI attendu :\n" + contenuWQUIValide);
        }
        lecteurContenuWQUI.close();

    }

    /**
     * Test du job pour le SI_AVAL GERD
     * 
     * @throws Exception
     *             si une erreur inattendue survient lors du test
     */
    @Test
    public void testJobCasNormalGERD() throws Exception {
        String repertoireSortie = System.getProperty("user.dir") + "/target/test-classes/fr/si2m/red/batch/flux56/outputs/";
        File fichierRepertoireSortie = new File(repertoireSortie);
        if (!fichierRepertoireSortie.exists()) {
            fichierRepertoireSortie.mkdirs();
        }
        String[] parametrageBatch = new String[] { repertoireSortie, "20151222", "GERD", "20160116_125526",
                System.getProperty("user.dir") + "\\target\\test-classes\\fr\\si2m\\red\\batch\\flux56\\outputs\\erreurs_flux56.log" };

        // RED-107 le path vers le fichier de config est paramétrable depuis les scripts, et donné à la JVM
        System.setProperty("configBatch", System.getProperty("user.dir") + "/target/test-classes/RR500.properties");

        // Préparation de l'exécution sur le profil de test des batchs
        Flux56ExecuteurBatch executeur = new Flux56ExecuteurBatch(parametrageBatch);
        executeur.setProfilsActifsJob(new String[] { "testBatch" });

        // Exécution
        int codeRetour = executeur.executeJob();

        // Teste le code retour
        Assert.assertEquals(codeRetour, CodeRetour.TERMINE.getCode(), "Le batch devait se terminer avec le code 0 (terminé sans erreurs).");

        // Validation GERD
        FileReader lecteurFichierGERDValide = new FileReader(
                System.getProperty("user.dir") + "/target/test-classes/fr/si2m/red/batch/flux56/inputs/valides/WF000_E3_MDG_GERD.sor");
        String contenuGERDValide = IOUtils.toString(lecteurFichierGERDValide);
        lecteurFichierGERDValide.close();

        FileReader lecteurFichierGERD = new FileReader(
                System.getProperty("user.dir") + "/target/test-classes/fr/si2m/red/batch/flux56/outputs/20160116_125526_RR500_E4_MDG_GERD.sor");
        BufferedReader lecteurContenuGERD = new BufferedReader(lecteurFichierGERD);
        String ligneGERD;
        while ((ligneGERD = lecteurContenuGERD.readLine()) != null) {
            Assert.assertTrue(contenuGERDValide.contains(ligneGERD),
                    "La ligne " + ligneGERD + " n'apparaît pas dans le fichier GERD attendu :\n" + contenuGERDValide);
        }
        lecteurContenuGERD.close();
    }

    /**
     * Test du job pour le SI_AVAL EGER
     * 
     * @throws Exception
     *             si une erreur inattendue survient lors du test
     */
    @Test
    public void testJobCasNormalEGER() throws Exception {
        String repertoireSortie = System.getProperty("user.dir") + "/target/test-classes/fr/si2m/red/batch/flux56/outputs/";
        File fichierRepertoireSortie = new File(repertoireSortie);
        if (!fichierRepertoireSortie.exists()) {
            fichierRepertoireSortie.mkdirs();
        }
        String[] parametrageBatch = new String[] { repertoireSortie, "20151222", "EGER", "20160116_125526",
                System.getProperty("user.dir") + "\\target\\test-classes\\fr\\si2m\\red\\batch\\flux56\\outputs\\erreurs_flux56.log" };

        // RED-107 le path vers le fichier de config est paramétrable depuis les scripts, et donné à la JVM
        System.setProperty("configBatch", System.getProperty("user.dir") + "/target/test-classes/RR500.properties");

        // Préparation de l'exécution sur le profil de test des batchs
        Flux56ExecuteurBatch executeur = new Flux56ExecuteurBatch(parametrageBatch);
        executeur.setProfilsActifsJob(new String[] { "testBatch" });

        // Exécution
        int codeRetour = executeur.executeJob();

        // Teste le code retour
        Assert.assertEquals(codeRetour, CodeRetour.TERMINE.getCode(), "Le batch devait se terminer avec le code 0 (terminé sans erreurs).");

        // Validation EGER
        FileReader lecteurFichierEGERValide = new FileReader(
                System.getProperty("user.dir") + "/target/test-classes/fr/si2m/red/batch/flux56/inputs/valides/WF000_E3_MDG_EGER.sor");
        String contenuEGERValide = IOUtils.toString(lecteurFichierEGERValide);
        lecteurFichierEGERValide.close();

        FileReader lecteurFichierEGER = new FileReader(
                System.getProperty("user.dir") + "/target/test-classes/fr/si2m/red/batch/flux56/outputs/20160116_125526_RR500_E4_MDG_EGER.sor");
        BufferedReader lecteurContenuEGER = new BufferedReader(lecteurFichierEGER);
        String ligneEGER;
        while ((ligneEGER = lecteurContenuEGER.readLine()) != null) {
            Assert.assertTrue(contenuEGERValide.contains(ligneEGER),
                    "La ligne " + ligneEGER + " n'apparaît pas dans le fichier EGER attendu :\n" + contenuEGERValide);
        }
        lecteurContenuEGER.close();
    }
}
