package fr.si2m.red.batch;

import java.sql.SQLException;

import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;

import fr.si2m.red.internal.test.db.TestDatabase;

@DirtiesContext
@ActiveProfiles("testBatch")
@ContextConfiguration(locations = { "classpath:fr/si2m/red/batch/moteur/config.xml" })
public abstract class BatchTests extends AbstractTestNGSpringContextTests {
    /**
     * Le port d'écoute de la base de test.
     * 
     * @return le port d'écoute de la base de test
     */
    protected int getTestDatabasePortUnique() {
        return TestDatabase.PORT_PAR_DEFAUT;
    }

    /**
     * Démarre la base de test.
     *
     * @throws SQLException
     *             si une erreur technique survient lors du démarrage de la base
     */
    @BeforeTest
    public void startDB() throws SQLException {
        TestDatabase.start(getTestDatabasePortUnique());
    }

    /**
     * Eteint la base de test.
     *
     * @throws SQLException
     *             si une erreur technique survient lors de l'extinction de la base
     */
    @AfterTest
    public void shutdownDB() throws SQLException {
        TestDatabase.shutdown(getTestDatabasePortUnique());
    }

    /**
     * Réinitialise les données en base pour partir du bon pied.
     *
     * @throws SQLException
     *             si une erreur technique survient lors du rafraîchissement de la base
     */
    @BeforeMethod
    public void refreshDB() throws SQLException {
        TestDatabase.refreshData(getTestDatabasePortUnique());
    }

}
