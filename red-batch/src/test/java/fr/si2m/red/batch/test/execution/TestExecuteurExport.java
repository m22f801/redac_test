package fr.si2m.red.batch.test.execution;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParameter;
import org.springframework.batch.core.JobParameters;
import org.springframework.context.support.GenericXmlApplicationContext;

import fr.si2m.red.batch.moteur.CodeRetour;
import fr.si2m.red.batch.moteur.execution.ExecuteurBatch;

/**
 * Lanceur de batchs de test.
 * 
 * @author nortaina
 *
 */
public class TestExecuteurExport extends ExecuteurBatch {
    private static final Logger LOGGER = LoggerFactory.getLogger(TestExecuteurExport.class);

    private final String emplacementRessourceFichierSortie;

    /**
     * Constructeur.
     * 
     * @param cheminCompletFichierSortie
     *            l'emplacement du fichier de sortie du batch
     */
    public TestExecuteurExport(String cheminCompletFichierSortie) {
        super(new String[] { cheminCompletFichierSortie });
        this.emplacementRessourceFichierSortie = tranformeEmplacementRessourceFichier(cheminCompletFichierSortie);
        this.setProfilsActifsJob(new String[] { "test" });
    }

    @Override
    protected String getEmplacementFichierConfigurationJob() {
        return "fr/si2m/red/batch/test/jobs.xml";
    }

    @Override
    protected String getIdJobCible() {
        return "jobTestExport";
    }

    @Override
    protected boolean lanceTraitementAvantJob(GenericXmlApplicationContext contexteExecutionJob) {
        // Aucune validation nécessaire
        return true;
    }

    @Override
    protected JobParameters initParametrageJob(GenericXmlApplicationContext contexteExecutionJob) {
        Map<String, JobParameter> parametresJob = new HashMap<String, JobParameter>();
        // parametresJob.put("fichierSortie", new JobParameter("file:C:\\temp\\test.csv"));
        parametresJob.put("fichierSortie", new JobParameter(emplacementRessourceFichierSortie));
        return new JobParameters(parametresJob);
    }

    @Override
    protected void lanceTraitementFinJob(GenericXmlApplicationContext configurationJob, CodeRetour codeRetour, JobExecution executionJob) {
        LOGGER.info("Fin du job de test d'export");
    }
    
    protected boolean testeFichierVide(String emplacementRessourceFichier) {
        return false;
    }

}
