package fr.si2m.red.batch.test;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * Test transformé (nom en majuscules et date / salaire au format français).
 * 
 * @author nortaina
 *
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class TestTransforme extends Test {
    /**
     * UID de version.
     */
    private static final long serialVersionUID = 1L;

    private String nomTransforme;
    private String dateNaissanceTransforme;
    private String salaireTransforme;
}
