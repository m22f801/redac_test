package fr.si2m.red.batch.test.support;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import fr.si2m.red.batch.test.Test;

/**
 * Mapper de données de test lues dans une base de données.
 * 
 * @author nortaina
 *
 */
public class TestRowMapper implements RowMapper<Test> {
    @Override
    public Test mapRow(ResultSet rs, int rowNum) throws SQLException {
        Test test = new Test();
        test.setNom(rs.getString("NOM"));
        test.setPrenom(rs.getString("PRENOM"));
        test.setDateNaissance(rs.getDate("DATE_NAISSANCE"));
        test.setSalaire(rs.getDouble("SALAIRE"));
        return test;
    }

}
