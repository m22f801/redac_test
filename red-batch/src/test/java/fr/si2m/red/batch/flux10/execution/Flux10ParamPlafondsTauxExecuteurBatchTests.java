package fr.si2m.red.batch.flux10.execution;

import org.springframework.beans.factory.annotation.Autowired;
import org.testng.annotations.Test;

import fr.si2m.red.EntiteImportableBatch;
import fr.si2m.red.core.repository.EntiteImportableRepository;
import fr.si2m.red.parametrage.ParamPlafondTauxRepository;

/**
 * Tests des batchs du flux 10.
 * 
 * @author poidij
 *
 */
@Test
public class Flux10ParamPlafondsTauxExecuteurBatchTests extends Flux10BatchTests {

    @Autowired
    private ParamPlafondTauxRepository paramPlafondTauxRepository;

    /**
     * Test du job.
     * 
     * @throws Exception
     *             si une erreur inattendue survient lors du test
     */
    @Test
    public void testJobCasNormal() throws Exception {
        // Préparation de l'exécution sur le profil de test des batchs
        Flux10ParamPlafondsTauxExecuteurBatch executeur = new Flux10ParamPlafondsTauxExecuteurBatch(getParametrageBatch());

        // Lancement du batch
        lancerTestJobCasNormal(executeur);
    }

    protected String getNomEntitesTestees() {
        return "ParamPlafondsTaux";
    }

    @Override
    protected int getNombreEntitesTestees() {
        return 1;
    }

    protected EntiteImportableRepository<? extends EntiteImportableBatch> getReferentielEntitesImportees() {
        return paramPlafondTauxRepository;
    }

}
