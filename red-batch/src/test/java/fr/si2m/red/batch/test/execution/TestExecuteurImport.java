package fr.si2m.red.batch.test.execution;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParameter;
import org.springframework.batch.core.JobParameters;
import org.springframework.context.support.GenericXmlApplicationContext;
import org.testng.Assert;

import fr.si2m.red.batch.moteur.CodeRetour;
import fr.si2m.red.batch.moteur.execution.ExecuteurBatch;
import fr.si2m.red.batch.moteur.execution.FacadeExecuteurBatch;

/**
 * Lanceur de batchs de test.
 * 
 * @author nortaina
 *
 */
@FacadeExecuteurBatch(nomBatch = "testImport")
public class TestExecuteurImport extends ExecuteurBatch {
    /**
     * Emplacement du fichier valide de test.
     */
    public static final String FICHIER_VALIDE = "classpath:fr/si2m/red/batch/test/inputs/test.csv";
    /**
     * Emplacement du fichier de test avec une erreur fonctionnelle non bloquante.
     */
    public static final String FICHIER_VALIDE_ERR_NON_BLOQ = "classpath:fr/si2m/red/batch/test/inputs/test-err-non-bloq.csv";
    /**
     * Emplacement du fichier de test avec une erreur fonctionnelle bloquante.
     */
    public static final String FICHIER_VALIDE_ERR_BLOQ = "classpath:fr/si2m/red/batch/test/inputs/test-err-bloq.csv";
    /**
     * Emplacement du fichier de test techniquement mal formé.
     */
    public static final String FICHIER_MAL_FORME = "classpath:fr/si2m/red/batch/test/inputs/test-err-mal-formé.csv";
    /**
     * Emplacement non valide pour un fichier de test.
     */
    public static final String FICHIER_INEXISTANT = "classpath:fr/si2m/red/batch/test/inputs/test--pala--mais-tes-pala-mais-tes-ou--pala.csv";

    private static final Logger LOGGER = LoggerFactory.getLogger(TestExecuteurImport.class);

    private final String emplacementRessourceFichierEntree;

    /**
     * Constructeur.
     * 
     * @param args
     *            un seul paramètre attendu : l'emplacement du fichier d'entrée du batch
     */
    public TestExecuteurImport(String[] args) {
        super(args);
        this.emplacementRessourceFichierEntree = tranformeEmplacementRessourceFichier(args[0]);
        this.setProfilsActifsJob(new String[] { "test" });
    }

    @Override
    protected String getEmplacementFichierConfigurationJob() {
        return "fr/si2m/red/batch/test/jobs.xml";
    }

    @Override
    protected String getIdJobCible() {
        return "jobTestImport";
    }

    @Override
    protected JobParameters initParametrageJob(GenericXmlApplicationContext contexteExecutionJob) {
        Map<String, JobParameter> parametresJob = new HashMap<String, JobParameter>();
        parametresJob.put("fichierEntree", new JobParameter(emplacementRessourceFichierEntree));
        return new JobParameters(parametresJob);
    }

    @Override
    protected boolean lanceTraitementAvantJob(GenericXmlApplicationContext contexteExecutionJob) {
        if (!verifieExistencePhysiqueRessource(contexteExecutionJob, emplacementRessourceFichierEntree)) {
            LOGGER.error("Fichier test manquant pour démarrer le batch : {}", emplacementRessourceFichierEntree);
            return false;
        }
        return true;
    }

    @Override
    protected void lanceTraitementFinJob(GenericXmlApplicationContext configurationJob, CodeRetour codeRetour, JobExecution executionJob) {
        File fichierImporte = sauvegardeFichierImporte(configurationJob, executionJob, emplacementRessourceFichierEntree);

        // Test en interne du fichier importé en base mémoire puisqu'on n'a plus accès au contexte après l'exécution
        List<String> nomsFichiers = getFichierBatchRepository(configurationJob).listeFichiersImportes(executionJob.getId());
        Assert.assertEquals(nomsFichiers.size(), 1, "Un fichier devrait avoir été importé en base");
        Assert.assertTrue(nomsFichiers.contains(fichierImporte.getName()));
        InputStream contenuFichierSauvegarde = getFichierBatchRepository(configurationJob).getContenuFichierImporte(executionJob.getId(),
                fichierImporte.getName());
        try {
            String contenuSauvegarde = IOUtils.toString(contenuFichierSauvegarde);
            LOGGER.debug("Contenu fichier sauvegardé :");
            LOGGER.debug("{}", contenuSauvegarde);
            String contenuOriginal = IOUtils.toString(configurationJob.getResource(emplacementRessourceFichierEntree).getInputStream());
            LOGGER.debug("Contenu fichier original :");
            LOGGER.debug("{}", contenuOriginal);
            Assert.assertEquals(contenuSauvegarde, contenuOriginal, "Les contenus du fichier sauvegardé doit être identique à l'original");
        } catch (IOException e) {
            Assert.fail("Erreur de lecture des contenus des fichiers tests", e);
        } finally {
            IOUtils.closeQuietly(contenuFichierSauvegarde);
        }

        LOGGER.info("Fichier {} bien sauvegardé en base", fichierImporte.getName());
    }
    
    protected boolean testeFichierVide(String emplacementRessourceFichier) {
        return false;
    }

}
