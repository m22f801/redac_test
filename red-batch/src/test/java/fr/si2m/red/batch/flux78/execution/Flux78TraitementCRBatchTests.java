package fr.si2m.red.batch.flux78.execution;

import java.util.List;

import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobInstance;
import org.springframework.batch.core.explore.JobExplorer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;
import org.testng.Assert;
import org.testng.annotations.Test;

import fr.si2m.red.batch.BatchTests;
import fr.si2m.red.batch.moteur.CodeRetour;
import fr.si2m.red.reconciliation.CompteRenduIntegration;
import fr.si2m.red.reconciliation.CompteRenduIntegrationRepository;
import fr.si2m.red.reconciliation.HistoriqueEtatPeriode;
import fr.si2m.red.reconciliation.HistoriqueEtatPeriodeRepository;
import fr.si2m.red.reconciliation.MessageControle;
import fr.si2m.red.reconciliation.MessageControleRepository;

/**
 * Tests des batchs du flux 78.
 * 
 * @author poidij
 *
 */
@Test
public class Flux78TraitementCRBatchTests extends BatchTests {

    @Autowired
    private PlatformTransactionManager txManager;
    @Autowired
    private JobExplorer jobExplorer;
    @Autowired
    private MessageControleRepository messageControleRepository;
    @Autowired
    private HistoriqueEtatPeriodeRepository historiqueEtatPeriodeRepository;
    @Autowired
    private CompteRenduIntegrationRepository compteRenduIntegrationRepository;

    /**
     * Test du job.
     * 
     * @throws Exception
     *             si une erreur inattendue survient lors du test
     */
    @Test(enabled = true)
    public void testJobCasNormal() throws Exception {
        String[] parametrageBatch = new String[] {
                System.getProperty("user.dir") + "/target/test-classes/fr/si2m/red/batch/flux78/inputs/F78_RG_P4_Valide.tri",
                System.getProperty("user.dir") + "\\target\\test-classes\\fr\\si2m\\red\\batch\\flux78\\outputs\\erreurs_flux78_TraitementCR.log" };

        // RED-107 le path vers le fichier de config est paramétrable depuis les scripts, et donné à la JVM
        System.setProperty("configBatch", System.getProperty("user.dir") + "/target/test-classes/RR702.properties");

        // Préparation de l'exécution sur le profil de test des batchs
        Flux78TraitementCRExecuteurBatch executeur = new Flux78TraitementCRExecuteurBatch(parametrageBatch);
        executeur.setProfilsActifsJob(new String[] { "testBatch" });

        // Exécution
        int codeRetour = executeur.executeJob();

        // Teste le code retour
        Assert.assertEquals(codeRetour, CodeRetour.TERMINE.getCode(), "Le batch devait se terminer avec le code 0 (terminé sans erreurs).");

        // Début des tests sur la base
        TransactionStatus txStatus = txManager.getTransaction(new DefaultTransactionDefinition());
        try {

            for (HistoriqueEtatPeriode hist : historiqueEtatPeriodeRepository.liste()) {

                // On extrait l'id de la période de l'objet en cours
                Long idHisto = hist.getIdPeriode();
                // On récupère le compte rendu associé à la période
                CompteRenduIntegration compteRenduConcerne = compteRenduIntegrationRepository.getPourPeriode(idHisto);
                // On récupère l'origine du changement
                String origine = hist.getOrigineChangement();
                // On récupère l'emetteur numérique
                Integer emetteur = compteRenduConcerne.getEmetteurCr();

                if (emetteur == 0) {
                    Assert.assertEquals("WQUI", origine, "L'emetteur 0 devrait être associé à WQUI");
                } else if (emetteur == 1) {
                    Assert.assertEquals("GERD", origine, "L'emetteur 1 devrait être associé à GERD");
                } else if (emetteur == 2) {
                    Assert.assertEquals("EGER", origine, "L'emetteur 2 devrait être associé à EGER");
                }

            }

            List<MessageControle> listeMessageControle = messageControleRepository.liste();

            // F78_RG_P4_08 et F78_RG_P4_12
            for (MessageControle entite : listeMessageControle) {
                if (entite.getParamMessage1() != null || entite.getParamMessage2() != null || entite.getParamMessage3() != null
                        || entite.getParamMessage4() != null) {
                    Assert.fail("Les champs PARAM1,PARAM2,PARAM3,PARAM1 devraient être vides");
                }
            }

            // Teste la sauvegarde de l'exécution du job en base
            List<JobInstance> jobFlux78Instances = jobExplorer.findJobInstancesByJobName("jobFlux78_TraitementCR", 0, 1);
            Assert.assertEquals(jobFlux78Instances.size(), 1, "Il ne devrait y avoir qu'une seule instance créée pour le job flux78");
            List<JobExecution> executionsJob = jobExplorer.getJobExecutions(jobFlux78Instances.get(0));
            Assert.assertEquals(executionsJob.size(), 1, "Il ne devrait y avoir qu'une seule exécution du job flux78");
            JobExecution executionJobFlux78 = executionsJob.get(0);

            Assert.assertEquals(executionJobFlux78.getExitStatus().getExitCode(), "0", "Le code de retour en base devrait être 0");
        } finally {
            txManager.commit(txStatus);
        }

    }

}
