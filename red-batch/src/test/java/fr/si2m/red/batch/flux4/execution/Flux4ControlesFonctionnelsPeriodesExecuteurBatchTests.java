package fr.si2m.red.batch.flux4.execution;

import java.io.File;
import java.util.List;

import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobInstance;
import org.springframework.batch.core.explore.JobExplorer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;
import org.testng.Assert;
import org.testng.annotations.Test;

import fr.si2m.red.DateRedac;
import fr.si2m.red.batch.BatchTests;
import fr.si2m.red.batch.moteur.CodeRetour;
import fr.si2m.red.parametrage.ParamControleSignalRejet;
import fr.si2m.red.reconciliation.MessageControle;
import fr.si2m.red.reconciliation.MessageControleRepository;
import fr.si2m.red.reconciliation.PeriodeRecueRepository;

/**
 * Tests sur la phase 6 du flux 4. ( 406 )
 * 
 * @author nortaina
 *
 */
public class Flux4ControlesFonctionnelsPeriodesExecuteurBatchTests extends BatchTests {

    @Autowired
    private PlatformTransactionManager txManager;
    @Autowired
    private JobExplorer jobExplorer;

    @Autowired
    private PeriodeRecueRepository periodeRecueRepository;

    @Autowired
    private MessageControleRepository messageControleRepository;

    /**
     * Test du cas normal.
     */
    @Test
    public void testCasNormal() {
        String[] parametrageBatch = new String[] {
                System.getProperty("user.dir") + "/target/test-classes/fr/si2m/red/batch/flux4/outputs/erreurs-calculs.log" };

        // RED-107 le path vers le fichier de config est paramétrable depuis les scripts, et donné à la JVM
        System.setProperty("configBatch", System.getProperty("user.dir") + "/target/test-classes/RR406.properties");

        File errorLog = new File(parametrageBatch[0]);
        if (errorLog.exists()) {
            // On part sur des logs frais
            errorLog.delete();
        }

        // Préparation de l'exécution sur le profil de test des batchs
        Flux4ControlesFonctionnelsPeriodesExecuteurBatch executeur = new Flux4ControlesFonctionnelsPeriodesExecuteurBatch(parametrageBatch);
        executeur.setProfilsActifsJob(new String[] { "testBatch" });

        // Exécution
        int codeRetour = executeur.executeJob();

        // Teste le code retour
        Assert.assertEquals(codeRetour, CodeRetour.TERMINE.getCode(), "Le batch devait se terminer avec le code 0.");

        // Début des tests sur la base
        TransactionStatus txStatus = txManager.getTransaction(new DefaultTransactionDefinition());
        try {
            // Teste la sauvegarde de l'exécution du job en base
            List<JobInstance> jobFlux1Instances = jobExplorer.findJobInstancesByJobName("jobFlux4_ControlesFonctionnelsPeriodes", 0, 1);
            Assert.assertEquals(jobFlux1Instances.size(), 1, "Il ne devrait y avoir qu'une seule instance créée pour le job");
            List<JobExecution> executionsJob = jobExplorer.getJobExecutions(jobFlux1Instances.get(0));
            Assert.assertEquals(executionsJob.size(), 1, "Il ne devrait y avoir qu'une seule exécution du job");
            JobExecution executionJobFlux1 = executionsJob.get(0);

            Assert.assertEquals(executionJobFlux1.getExitStatus().getExitCode(), "0", "Le code de retour en base devrait être 0");
            Assert.assertEquals(executionJobFlux1.getExitStatus().getExitDescription(), "Terminé",
                    "Le messsage de retour aurait dû correspondre au code 0");

            // Vérification suppression des messages de contrôle initiaux pour la période 34
            List<MessageControle> messages = messageControleRepository.getPourPeriode(34L);
            for (MessageControle message : messages) {
                Assert.assertNotSame(message.getAuditUtilisateurCreation(), "TEST_INIT");
            }

            // Vérification pour résiliation contrat
            MessageControle messageResiliation = messageControleRepository.getPourPeriodeEtIdentifiantControle(34L,
                    ParamControleSignalRejet.ID_CONTRATS_RESILIES);
            Assert.assertNotNull(messageResiliation);
            Assert.assertEquals(messageResiliation.getOrigineMessage(), "REDAC");
            Assert.assertEquals(messageResiliation.getDateTraitement(), DateRedac.maintenant());
            Assert.assertEquals(messageResiliation.getNiveauAlerte(), ParamControleSignalRejet.NIVEAU_ALERTE_SIGNAL);
            Assert.assertNull(messageResiliation.getIndividu());
            Assert.assertEquals(messageResiliation.getParamMessage1(), "01/09/2015");
            Assert.assertNull(messageResiliation.getParamMessage2());
            Assert.assertNull(messageResiliation.getParamMessage3());
            Assert.assertNull(messageResiliation.getParamMessage4());
            Assert.assertEquals(messageResiliation.getMessageUtilisateur(), "Contrat résilié au 01/09/2015");

            // Vérification absence pour résiliation contrat
            MessageControle messageResiliationAbsent = messageControleRepository.getPourPeriodeEtIdentifiantControle(35L,
                    ParamControleSignalRejet.ID_CONTRATS_RESILIES);
            Assert.assertNull(messageResiliationAbsent);

            // Vérification pour cotisation établissement
            MessageControle messageCotisationEtablissement = messageControleRepository.getPourPeriodeEtIdentifiantControle(36L,
                    ParamControleSignalRejet.ID_PRESENCE_COTISATION_ETABLISSEMENT);
            Assert.assertNotNull(messageCotisationEtablissement);
            Assert.assertEquals(messageCotisationEtablissement.getOrigineMessage(), "REDAC");
            Assert.assertEquals(messageCotisationEtablissement.getDateTraitement(), DateRedac.maintenant());
            Assert.assertEquals(messageCotisationEtablissement.getNiveauAlerte(), ParamControleSignalRejet.NIVEAU_ALERTE_SIGNAL);
            Assert.assertNull(messageCotisationEtablissement.getIndividu());
            Assert.assertNull(messageCotisationEtablissement.getParamMessage1());
            Assert.assertNull(messageCotisationEtablissement.getParamMessage2());
            Assert.assertNull(messageCotisationEtablissement.getParamMessage3());
            Assert.assertNull(messageCotisationEtablissement.getParamMessage4());
            Assert.assertEquals(messageCotisationEtablissement.getMessageUtilisateur(), "Contrat avec cotisation établissement");

            // Vérification pour varation nb établissements
            MessageControle messageVarNbEtablissements = messageControleRepository.getPourPeriodeEtIdentifiantControle(43L,
                    ParamControleSignalRejet.ID_VARIATION_NB_ETABLISSEMENTS);
            Assert.assertNotNull(messageVarNbEtablissements);
            Assert.assertEquals(messageVarNbEtablissements.getOrigineMessage(), "REDAC");
            Assert.assertEquals(messageVarNbEtablissements.getDateTraitement(), DateRedac.maintenant());
            Assert.assertEquals(messageVarNbEtablissements.getNiveauAlerte(), ParamControleSignalRejet.NIVEAU_ALERTE_REJET);
            Assert.assertNull(messageVarNbEtablissements.getIndividu());
            Assert.assertEquals(messageVarNbEtablissements.getParamMessage1(), "1");
            Assert.assertEquals(messageVarNbEtablissements.getParamMessage2(), "1");
            Assert.assertEquals(messageVarNbEtablissements.getParamMessage3(), "10");
            Assert.assertNull(messageVarNbEtablissements.getParamMessage4());
            Assert.assertEquals(messageVarNbEtablissements.getMessageUtilisateur(), "Variation significative du nombre d'établissements");

            messageVarNbEtablissements = messageControleRepository.getPourPeriodeEtIdentifiantControle(39L,
                    ParamControleSignalRejet.ID_VARIATION_NB_ETABLISSEMENTS);
            Assert.assertNotNull(messageVarNbEtablissements);
            Assert.assertEquals(messageVarNbEtablissements.getOrigineMessage(), "REDAC");
            Assert.assertEquals(messageVarNbEtablissements.getDateTraitement(), DateRedac.maintenant());
            Assert.assertEquals(messageVarNbEtablissements.getNiveauAlerte(), ParamControleSignalRejet.NIVEAU_ALERTE_REJET);
            Assert.assertNull(messageVarNbEtablissements.getIndividu());
            Assert.assertEquals(messageVarNbEtablissements.getParamMessage1(), "5");
            Assert.assertEquals(messageVarNbEtablissements.getParamMessage2(), "5");
            Assert.assertEquals(messageVarNbEtablissements.getParamMessage3(), "10");
            Assert.assertNull(messageVarNbEtablissements.getParamMessage4());
            Assert.assertEquals(messageVarNbEtablissements.getMessageUtilisateur(), "Variation significative du nombre d'établissements");

            // Vérification pour variation nb salariés
            MessageControle messageVarNbSalaries = messageControleRepository.getPourPeriodeEtIdentifiantControle(49L,
                    ParamControleSignalRejet.ID_VARIATION_NB_SALARIES);
            Assert.assertNotNull(messageVarNbSalaries);
            Assert.assertEquals(messageVarNbSalaries.getOrigineMessage(), "REDAC");
            Assert.assertEquals(messageVarNbSalaries.getDateTraitement(), DateRedac.maintenant());
            Assert.assertEquals(messageVarNbSalaries.getNiveauAlerte(), ParamControleSignalRejet.NIVEAU_ALERTE_REJET);
            Assert.assertNull(messageVarNbSalaries.getIndividu());
            Assert.assertEquals(messageVarNbSalaries.getParamMessage1(), "1");
            Assert.assertEquals(messageVarNbSalaries.getParamMessage2(), "1");
            Assert.assertEquals(messageVarNbSalaries.getParamMessage3(), "10");
            Assert.assertNull(messageVarNbSalaries.getParamMessage4());
            Assert.assertEquals(messageVarNbSalaries.getMessageUtilisateur(), "Variation significative du nombre de salariés");

            messageVarNbSalaries = messageControleRepository.getPourPeriodeEtIdentifiantControle(45L,
                    ParamControleSignalRejet.ID_VARIATION_NB_SALARIES);
            Assert.assertNotNull(messageVarNbSalaries);
            Assert.assertEquals(messageVarNbSalaries.getOrigineMessage(), "REDAC");
            Assert.assertEquals(messageVarNbSalaries.getDateTraitement(), DateRedac.maintenant());
            Assert.assertEquals(messageVarNbSalaries.getNiveauAlerte(), ParamControleSignalRejet.NIVEAU_ALERTE_REJET);
            Assert.assertNull(messageVarNbSalaries.getIndividu());
            Assert.assertEquals(messageVarNbSalaries.getParamMessage1(), "5");
            Assert.assertEquals(messageVarNbSalaries.getParamMessage2(), "5");
            Assert.assertEquals(messageVarNbSalaries.getParamMessage3(), "10");
            Assert.assertNull(messageVarNbSalaries.getParamMessage4());
            Assert.assertEquals(messageVarNbSalaries.getMessageUtilisateur(), "Variation significative du nombre de salariés");

            // Vérification pour variation nb salariés
            MessageControle messageModePaiement = messageControleRepository.getPourPeriodeEtIdentifiantControle(50L,
                    ParamControleSignalRejet.ID_MODE_PAIEMENT);
            Assert.assertNotNull(messageModePaiement);
            Assert.assertEquals(messageModePaiement.getOrigineMessage(), "REDAC");
            Assert.assertEquals(messageModePaiement.getDateTraitement(), DateRedac.maintenant());
            Assert.assertEquals(messageModePaiement.getNiveauAlerte(), ParamControleSignalRejet.NIVEAU_ALERTE_REJET);
            Assert.assertNull(messageModePaiement.getIndividu());
            Assert.assertNull(messageModePaiement.getParamMessage1());
            Assert.assertNull(messageModePaiement.getParamMessage2());
            Assert.assertNull(messageModePaiement.getParamMessage3());
            Assert.assertNull(messageModePaiement.getParamMessage4());
            Assert.assertEquals(messageModePaiement.getMessageUtilisateur(), "Mode de paiement particulier");

            // Vérification pour cohérence des périodes de versement
            MessageControle messageVersement = messageControleRepository.getPourPeriodeEtIdentifiantControle(52L,
                    ParamControleSignalRejet.ID_PERIODE_VERSEMENT);
            Assert.assertNotNull(messageVersement);
            Assert.assertEquals(messageVersement.getOrigineMessage(), "REDAC");
            Assert.assertEquals(messageVersement.getDateTraitement(), DateRedac.maintenant());
            Assert.assertEquals(messageVersement.getNiveauAlerte(), ParamControleSignalRejet.NIVEAU_ALERTE_REJET);
            Assert.assertNull(messageVersement.getIndividu());
            Assert.assertEquals(messageVersement.getParamMessage1(), "2015M07");
            Assert.assertNull(messageVersement.getParamMessage2());
            Assert.assertNull(messageVersement.getParamMessage3());
            Assert.assertNull(messageVersement.getParamMessage4());
            Assert.assertEquals(messageVersement.getMessageUtilisateur(), "Période de versement non conforme à la périodicité du contrat");

            // Vérification pour trou de déclaration
            MessageControle messageTrouDeclaration = messageControleRepository.getPourPeriodeEtIdentifiantControle(54L,
                    ParamControleSignalRejet.ID_TROU_DECLARATION);
            Assert.assertNotNull(messageTrouDeclaration);
            Assert.assertEquals(messageTrouDeclaration.getOrigineMessage(), "REDAC");
            Assert.assertEquals(messageTrouDeclaration.getDateTraitement(), DateRedac.maintenant());
            Assert.assertEquals(messageTrouDeclaration.getNiveauAlerte(), ParamControleSignalRejet.NIVEAU_ALERTE_REJET);
            Assert.assertNull(messageTrouDeclaration.getIndividu());
            Assert.assertEquals(messageTrouDeclaration.getParamMessage1(), "12/2015");
            Assert.assertNull(messageTrouDeclaration.getParamMessage2());
            Assert.assertNull(messageTrouDeclaration.getParamMessage3());
            Assert.assertNull(messageTrouDeclaration.getParamMessage4());
            Assert.assertEquals(messageTrouDeclaration.getMessageUtilisateur(), "Mois absent dans la période");

            // Vérification pour types des bases assujetties
            MessageControle messageTypesComposantsBasesAssujetties = messageControleRepository.getPourPeriodeEtIdentifiantControle(56L,
                    ParamControleSignalRejet.ID_TYPES_BASES_ASSUJETTIES);
            Assert.assertNotNull(messageTypesComposantsBasesAssujetties);
            Assert.assertEquals(messageTypesComposantsBasesAssujetties.getOrigineMessage(), "REDAC");
            Assert.assertEquals(messageTypesComposantsBasesAssujetties.getDateTraitement(), DateRedac.maintenant());
            Assert.assertEquals(messageTypesComposantsBasesAssujetties.getNiveauAlerte(), ParamControleSignalRejet.NIVEAU_ALERTE_REJET);
            Assert.assertNull(messageTypesComposantsBasesAssujetties.getIndividu());
            Assert.assertEquals(messageTypesComposantsBasesAssujetties.getParamMessage1(), "44");
            Assert.assertNull(messageTypesComposantsBasesAssujetties.getParamMessage2());
            Assert.assertNull(messageTypesComposantsBasesAssujetties.getParamMessage3());
            Assert.assertNull(messageTypesComposantsBasesAssujetties.getParamMessage4());
            Assert.assertEquals(messageTypesComposantsBasesAssujetties.getMessageUtilisateur(),
                    "Type composant base assujettie déclaré non conforme : 44");

            // Vérification pour changement de date de naissance d'un salarié
            MessageControle messageChangementDateNaissance = messageControleRepository.getPourPeriodeEtIdentifiantControle(58L,
                    ParamControleSignalRejet.ID_CHANGEMENT_DATE_NAISSANCE_ASSURE);
            Assert.assertNotNull(messageChangementDateNaissance);
            Assert.assertEquals(messageChangementDateNaissance.getOrigineMessage(), "REDAC");
            Assert.assertEquals(messageChangementDateNaissance.getDateTraitement(), DateRedac.maintenant());
            Assert.assertEquals(messageChangementDateNaissance.getNiveauAlerte(), ParamControleSignalRejet.NIVEAU_ALERTE_SIGNAL);
            Assert.assertNull(messageChangementDateNaissance.getIndividu());
            Assert.assertNull(messageChangementDateNaissance.getParamMessage1());
            Assert.assertNull(messageChangementDateNaissance.getParamMessage2());
            Assert.assertNull(messageChangementDateNaissance.getParamMessage3());
            Assert.assertNull(messageChangementDateNaissance.getParamMessage4());
            Assert.assertEquals(messageChangementDateNaissance.getMessageUtilisateur(), "Changement de date de naissance d'un assuré");

            Assert.assertTrue(periodeRecueRepository.getPeriodesAReconsolider().isEmpty());
        } finally {
            txManager.rollback(txStatus);
        }
    }

}
