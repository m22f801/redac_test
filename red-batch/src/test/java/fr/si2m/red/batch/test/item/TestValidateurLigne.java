package fr.si2m.red.batch.test.item;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.si2m.red.RedacUnexpectedException;
import fr.si2m.red.batch.moteur.item.ValidateurLigne;
import fr.si2m.red.batch.test.Test;

/**
 * Validateur d'éléments test.
 * 
 * @author nortaina
 *
 */
public class TestValidateurLigne extends ValidateurLigne<Test, Test> {
    private static final Logger LOGGER = LoggerFactory.getLogger(TestValidateurLigne.class);

    @Override
    public Test process(Test test) throws Exception {
        LOGGER.info("Début de la validation de {} à la ligne {}...", test, test.getLigneImport());

        // Bloquantes
        valideBloquante(test != null, "La ligne de test n'existe pas.");
        if (!test.isLigneSyntaxiquementValide()) {
            throw new RedacUnexpectedException("La ligne de test est syntaxiquement incorrecte.");
        }
        valideBloquante(StringUtils.isNotBlank(test.getNom()), "Le nom de " + test.toString() + " n'est pas déclaré.");
        valideBloquante(StringUtils.isNotBlank(test.getPrenom()), "Le prénom de " + test.toString() + " n'est pas déclaré.");
        valideBloquante(test.getSalaire() != null, "Le salaire de " + test.toString() + " n'est pas déclaré.");

        // Non bloquante
        valideNonBloquante(test.getDateNaissance() != null, "L'élément {} n'est fonctionnellement pas valide (erreur non bloquante).", test);

        LOGGER.info("Validation de {} à la ligne {} terminée", test, test.getLigneImport());
        return test;
    }
}