package fr.si2m.red.batch.test.execution;

import java.io.File;
import java.io.IOException;

import org.testng.Assert;
import org.testng.reporters.Files;

import fr.si2m.red.batch.moteur.CodeRetour;

/**
 * Teste le test (haha... on s'amuse comme on peut...).
 * 
 * @author nortaina
 *
 */
@org.testng.annotations.Test
public class TestTests {

    /**
     * Test sur import de fichier valide.
     */
    @org.testng.annotations.Test
    public void testTestImportValide() {
        TestExecuteurImport executeur = new TestExecuteurImport(new String[] { TestExecuteurImport.FICHIER_VALIDE });
        int codeRetour = executeur.executeJob();
        Assert.assertEquals(codeRetour, CodeRetour.TERMINE.getCode());
    }

    /**
     * Test sur import de fichier avec erreur fonctionnelle non bloquante.
     */
    @org.testng.annotations.Test
    public void testTestImportErreurFonctionnelleNonBloquante() {
        TestExecuteurImport executeur = new TestExecuteurImport(new String[] { TestExecuteurImport.FICHIER_VALIDE_ERR_NON_BLOQ });
        int codeRetour = executeur.executeJob();
        Assert.assertEquals(codeRetour, CodeRetour.ERREUR_FONCTIONNELLE_NON_BLOQUANTE.getCode());
    }

    /**
     * Test sur import de fichier avec erreur fonctionnelle bloquante.
     */
    @org.testng.annotations.Test
    public void testTestImportErreurFonctionnelleBloquante() {
        TestExecuteurImport executeur = new TestExecuteurImport(new String[] { TestExecuteurImport.FICHIER_VALIDE_ERR_BLOQ });
        int codeRetour = executeur.executeJob();
        Assert.assertEquals(codeRetour, CodeRetour.ERREUR_FONCTIONNELLE_BLOQUANTE.getCode());
    }

    /**
     * Test sur import de fichier non existant (pas de lancement de batch).
     */
    @org.testng.annotations.Test
    public void testTestImportNonExecute() {
        TestExecuteurImport executeur = new TestExecuteurImport(new String[] { TestExecuteurImport.FICHIER_INEXISTANT });
        int codeRetour = executeur.executeJob();
        Assert.assertEquals(codeRetour, CodeRetour.ERREUR_FONCTIONNELLE_BLOQUANTE.getCode());
    }

    /**
     * Test sur import de fichier avec erreur technique (bloquante).
     */
    @org.testng.annotations.Test
    public void testTestImportErreurTechnique() {
        TestExecuteurImport executeur = new TestExecuteurImport(new String[] { TestExecuteurImport.FICHIER_MAL_FORME });
        int codeRetour = executeur.executeJob();
        Assert.assertEquals(codeRetour, CodeRetour.ERREUR_TECHNIQUE_BLOQUANTE.getCode());
    }

    /**
     * Test export d'un fichier.
     * 
     * @throws IOException
     *             si une erreur de lecture du fichier de sortie survient
     */
    @org.testng.annotations.Test
    public void testTestExport() throws IOException {
        ClassLoader classLoader = getClass().getClassLoader();
        File classpathRoot = new File(classLoader.getResource("").getPath());

        String cheminCompletFichierSortie = classpathRoot.getPath() + "/fr/si2m/red/batch/test/outputs/test.csv";

        TestExecuteurExport executeur = new TestExecuteurExport(cheminCompletFichierSortie);
        int codeRetour = executeur.executeJob();
        Assert.assertEquals(codeRetour, CodeRetour.TERMINE.getCode());

        File fichierSortie = new File(cheminCompletFichierSortie);
        Assert.assertTrue(fichierSortie.exists());
        String contenuFichierSortie = Files.readFile(fichierSortie);

        // Pour les logs
        System.out.println(contenuFichierSortie);

        Assert.assertTrue(contenuFichierSortie.contains("MAY|Melinda|24/12/1975|2751,4"));
        Assert.assertTrue(contenuFichierSortie.contains("JOHNSON|Daisy|05/05/1988|1856,21"));
        Assert.assertTrue(contenuFichierSortie.contains("SIMMONS|Jemma|23/02/1986|2100,0"));
        Assert.assertTrue(contenuFichierSortie.contains("MORSE|Bobbi|14/04/1981|2685,03"));
        Assert.assertTrue(contenuFichierSortie.contains("HAND|Victoria|11/06/1974|3025,87"));
    }
}
