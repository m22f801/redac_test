package fr.si2m.red.batch.test.item;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import org.springframework.util.StringUtils;

import fr.si2m.red.batch.moteur.item.TransformateurDonnee;
import fr.si2m.red.batch.test.Test;
import fr.si2m.red.batch.test.TestTransforme;

/**
 * Transformateur de donnée de test.
 * 
 * @author nortaina
 *
 */
public class TestTransformateurDonnee extends TransformateurDonnee<Test, TestTransforme> {

    private final DateFormat dateFormatFrancais = new SimpleDateFormat("dd/MM/yyyy");

    @Override
    public TestTransforme process(Test item) throws Exception {
        TestTransforme testTransforme = new TestTransforme();

        testTransforme.setNom(item.getNom());
        testTransforme.setPrenom(item.getPrenom());
        testTransforme.setDateNaissance(item.getDateNaissance());
        testTransforme.setSalaire(item.getSalaire());

        testTransforme.setDateNaissanceTransforme(dateFormatFrancais.format(testTransforme.getDateNaissance()));
        testTransforme.setNomTransforme(testTransforme.getNom().toUpperCase());
        testTransforme.setSalaireTransforme(StringUtils.replace(String.valueOf(testTransforme.getSalaire()), ".", ","));

        return testTransforme;
    }
}
