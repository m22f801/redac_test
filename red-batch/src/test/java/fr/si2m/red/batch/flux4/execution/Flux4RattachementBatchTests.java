package fr.si2m.red.batch.flux4.execution;

import java.util.List;

import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobInstance;
import org.springframework.batch.core.explore.JobExplorer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;
import org.testng.Assert;
import org.testng.annotations.Test;

import fr.si2m.red.batch.BatchTests;
import fr.si2m.red.batch.moteur.CodeRetour;
import fr.si2m.red.dsn.AdhesionEtablissementMois;
import fr.si2m.red.dsn.AdhesionEtablissementMoisRepository;
import fr.si2m.red.reconciliation.HistoriqueEtatPeriode;
import fr.si2m.red.reconciliation.HistoriqueEtatPeriodeRepository;
import fr.si2m.red.reconciliation.PeriodeRecue;
import fr.si2m.red.reconciliation.PeriodeRecueRepository;
import fr.si2m.red.reconciliation.RattachementDeclarationsRecues;
import fr.si2m.red.reconciliation.RattachementDeclarationsRecuesRepository;

/**
 * Tests du batch de Rattachement du flux 4. ( 404 )
 * 
 * @author poidij
 *
 */
@Test
public class Flux4RattachementBatchTests extends BatchTests {

    @Autowired
    private PlatformTransactionManager txManager;
    @Autowired
    private JobExplorer jobExplorer;
    @Autowired
    private AdhesionEtablissementMoisRepository adhesionEtablissementMoisRepository;
    @Autowired
    private PeriodeRecueRepository periodeRecueRepository;
    @Autowired
    private HistoriqueEtatPeriodeRepository historiqueEtatPeriodeRepository;
    @Autowired
    private RattachementDeclarationsRecuesRepository rattachementDeclarationsRecuesRepository;

    /**
     * Test du job.
     * 
     * @throws Exception
     *             si une erreur inattendue survient lors du test
     */
    @Test(enabled = true)
    public void testJobCasNormal() throws Exception {
        String[] parametrageBatch = new String[] {
                System.getProperty("user.dir") + "\\target\\test-classes\\fr\\si2m\\red\\batch\\flux3\\outputs\\erreurs_flux4_Rattachement.log" };

        // RED-107 le path vers le fichier de config est paramétrable depuis les scripts, et donné à la JVM
        System.setProperty("configBatch", System.getProperty("user.dir") + "/target/test-classes/RR404.properties");

        // Préparation de l'exécution sur le profil de test des batchs
        Flux4RattachementExecuteurBatch executeur = new Flux4RattachementExecuteurBatch(parametrageBatch);
        executeur.setProfilsActifsJob(new String[] { "testBatch" });

        // Exécution
        int codeRetour = executeur.executeJob();

        // Teste le code retour
        Assert.assertEquals(codeRetour, CodeRetour.TERMINE.getCode(), "Le batch devait se terminer avec le code 0 (terminé sans erreurs).");

        TransactionStatus txStatus = txManager.getTransaction(new DefaultTransactionDefinition());
        try {
            // Teste la bonne insertion des 49 intermediaires dans la base de test
            List<AdhesionEtablissementMois> listeAdhesionEtablissementMois = adhesionEtablissementMoisRepository.liste();
            List<PeriodeRecue> listePeriode = periodeRecueRepository.liste();
            List<HistoriqueEtatPeriode> listeHistorique = historiqueEtatPeriodeRepository.liste();
            List<RattachementDeclarationsRecues> listeRattachement = rattachementDeclarationsRecuesRepository.liste();

            Assert.assertEquals(listeAdhesionEtablissementMois.size(), 9, "Le batch devait conserver 9 AdhesionEtablissementMois.");
            for (AdhesionEtablissementMois entite : listeAdhesionEtablissementMois) {
                if ("TU4_KO".equals(entite.getAuditUtilisateurCreation())) {
                    Assert.fail("L'entité " + entite.getClass() + " (" + entite.toString() + ") devrait être supprimée ");
                }
            }
            Assert.assertEquals(listePeriode.size(), 10 + 9, "Le batch devait créer 9 PeriodeRecue.");
            Assert.assertEquals(listeHistorique.size(), 9, "Le batch devait créer 9 HistoriqueEtatPeriode.");
            Assert.assertEquals(listeRattachement.size(), 9, "Le batch devait créer 9 RattachementDeclarationsRecues.");

            // Teste la sauvegarde de l'exécution du job en base
            List<JobInstance> jobFlux4Instances = jobExplorer.findJobInstancesByJobName("jobFlux4_Rattachement", 0, 1);
            Assert.assertEquals(jobFlux4Instances.size(), 1, "Il ne devrait y avoir qu'une seule instance créée pour le job flux 4");
            List<JobExecution> executionsJob = jobExplorer.getJobExecutions(jobFlux4Instances.get(0));
            Assert.assertEquals(executionsJob.size(), 1, "Il ne devrait y avoir qu'une seule exécution du job flux 4");
            JobExecution executionJobFlux4 = executionsJob.get(0);

            Assert.assertEquals(executionJobFlux4.getExitStatus().getExitCode(), "0", "Le code de retour en base devrait être 0");
            Assert.assertEquals(executionJobFlux4.getExitStatus().getExitDescription(), "Terminé",
                    "Le messsage de retour aurait dû correspondre au code 0");
        } finally {
            txManager.commit(txStatus);
        }

    }

}
