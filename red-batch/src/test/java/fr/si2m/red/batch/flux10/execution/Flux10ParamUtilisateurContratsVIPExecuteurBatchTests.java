package fr.si2m.red.batch.flux10.execution;

import org.springframework.beans.factory.annotation.Autowired;
import org.testng.annotations.Test;

import fr.si2m.red.EntiteImportableBatch;
import fr.si2m.red.core.repository.EntiteImportableRepository;
import fr.si2m.red.parametrage.ParamUtilisateurContratVIPRepository;

/**
 * Tests des batchs du flux 10.
 * 
 * @author poidij
 *
 */
@Test
public class Flux10ParamUtilisateurContratsVIPExecuteurBatchTests extends Flux10BatchTests {

    @Autowired
    private ParamUtilisateurContratVIPRepository paramUtilisateurContratVIPRepository;

    /**
     * Test du job.
     * 
     * @throws Exception
     *             si une erreur inattendue survient lors du test
     */
    @Test
    public void testJobCasNormal() throws Exception {
        // Préparation de l'exécution sur le profil de test des batchs
        Flux10ParamUtilisateurContratsVIPExecuteurBatch executeur = new Flux10ParamUtilisateurContratsVIPExecuteurBatch(getParametrageBatch());

        // Lancement du batch
        lancerTestJobCasNormal(executeur);
    }

    protected String getNomEntitesTestees() {
        return "ParamUtilisateurContratsVIP";
    }

    @Override
    protected int getNombreEntitesTestees() {
        return 3;
    }

    protected EntiteImportableRepository<? extends EntiteImportableBatch> getReferentielEntitesImportees() {
        return paramUtilisateurContratVIPRepository;
    }

}
