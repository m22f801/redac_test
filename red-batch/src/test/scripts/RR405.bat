@echo off

java -Xms1024m -Xmx2048m -XX:PermSize=128m -XX:MaxPermSize=256m -cp ../conf;../bin/red-batch.jar fr.si2m.red.batch.facade.BatchFacade flux4_CalculElementsDeclaratifs "C:\temp\RR405.err"
exit /B %ERRORLEVEL%
