@echo off

set REPERTOIRE_FICHIERS=%1
set ID_FLUX=%2

java -Xms1024m -Xmx2048m -XX:PermSize=128m -XX:MaxPermSize=256m -cp ../conf;../bin/red-batch.jar fr.si2m.red.batch.facade.BatchFacade flux1 %REPERTOIRE_FICHIERS% %ID_FLUX% "C:\temp\RR100.err"
exit /B %ERRORLEVEL%