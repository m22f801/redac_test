@echo off

# Paramètres entrants
set NOM_DU_BATCH=%1
set CHEMIN_ABSOLU_FICHIER=%2
set NOM_COMPLET_DU_BATCH=flux10_%NOM_DU_BATCH%

# Préparation du fichier de récapitulatif des erreurs
set FICHIER_ERREURS="C:\temp\RR000.err"

java -Xms1024m -Xmx2048m -XX:PermSize=128m -XX:MaxPermSize=256m -cp ../conf;../bin/red-batch.jar fr.si2m.red.batch.facade.BatchFacade %NOM_COMPLET_DU_BATCH% %CHEMIN_ABSOLU_FICHIER% %FICHIER_ERREURS%
exit /B %ERRORLEVEL%

