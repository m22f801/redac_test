@echo off

set REPERTOIRE_SORTIE=%1
set DATE_CALCUL_INDICATEURS=%2
set SI_AVAL=%3
set TIMESTAMP_EXPORT=%4
set LOG=%5
set FICHIER_PROPERTIES=%6

java -Xms1024m -Xmx2048m -XX:PermSize=128m -XX:MaxPermSize=256m -DconfigBatch=${FICHIER_PROPERTIES} -cp ../conf;../bin/red-batch.jar fr.si2m.red.batch.facade.BatchFacade flux56 %REPERTOIRE_SORTIE% %DATE_CALCUL_INDICATEURS% %SI_AVAL%  %TIMESTAMP_EXPORT% %LOG%
exit /B %ERRORLEVEL%