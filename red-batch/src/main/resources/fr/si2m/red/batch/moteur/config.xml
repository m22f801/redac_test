<beans xmlns="http://www.springframework.org/schema/beans"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xmlns:context="http://www.springframework.org/schema/context"
	xmlns:jdbc="http://www.springframework.org/schema/jdbc"
	xmlns:aop="http://www.springframework.org/schema/aop"
	xmlns:batch="http://www.springframework.org/schema/batch" 
	xsi:schemaLocation="http://www.springframework.org/schema/beans 
		http://www.springframework.org/schema/beans/spring-beans-4.0.xsd
	    http://www.springframework.org/schema/jdbc
		http://www.springframework.org/schema/jdbc/spring-jdbc-4.0.xsd
	    http://www.springframework.org/schema/aop
		http://www.springframework.org/schema/aop/spring-aop-4.0.xsd
	    http://www.springframework.org/schema/batch
	    http://www.springframework.org/schema/batch/spring-batch-3.0.xsd
	    http://www.springframework.org/schema/context
		http://www.springframework.org/schema/context/spring-context-4.0.xsd">
	    
	<context:property-placeholder properties-ref="moteurConfig" order="100" />
	
	<!-- Configuration par défaut du moteur à inclure dans les surcharges de configuration -->
	<bean id="moteurConfig" class="org.springframework.beans.factory.config.PropertiesFactoryBean">
		<property name="locations">
			<array>
				<value>classpath:moteur.properties</value>
			</array>
		</property>
	</bean>
		
	<import resource="classpath:fr/si2m/red/config.xml"/>

	<!-- Le lanceur de job par défaut -->
	<bean id="jobLauncher" class="org.springframework.batch.core.launch.support.SimpleJobLauncher">
		<property name="jobRepository" ref="jobRepository" />
	</bean>
	
	<!-- Permet le transport d'informations entre contextes d'exécution -->
	<bean id="etapeCodeRetourModificateur" class="fr.si2m.red.batch.moteur.EtapeCodeRetourModificateur" />
	<bean id="jobMessageRetourModificateur" class="fr.si2m.red.batch.moteur.JobMessageRetourModificateur" />
	
	
	<!-- Permet la trace des performances sur les jobs batchs -->
	<bean id="morceauSynchronePerformanceTraceur" class="fr.si2m.red.batch.moteur.trace.MorceauSynchronePerformanceTraceur" scope="prototype">
		<property name="traceTraitementMorceau" value="${trace.performance.traitement_morceau:true}"></property>
		<property name="traceLectureLigne" value="${trace.performance.lecture_ligne:false}"></property> 
		<property name="traceProcessEtValidationLigne" value="${trace.performance.process_validation_ligne:false}"></property> 
		<property name="traceEcritureLignes" value="${trace.performance.ecriture_lignes:false}"></property>
	</bean>
	<bean id="etapePerformanceTraceur" class="fr.si2m.red.batch.moteur.trace.EtapePerformanceTraceur" scope="prototype">
		<property name="traceTraitementEtape" value="${trace.performance.traitement_etape:true}"></property>
	</bean>
	<bean id="jobPerformanceTraceur" class="fr.si2m.red.batch.moteur.trace.JobPerformanceTraceur" scope="prototype">
		<property name="traceTraitementJob" value="${trace.performance.traitement_job:true}"></property>
	</bean>
	
	<!-- Base de configuration des composants de batchs REDAC -->
	<batch:job id="jobRedac" restartable="true" abstract="true">
		<batch:listeners>
        	<batch:listener ref="jobMessageRetourModificateur" />
            <batch:listener ref="jobPerformanceTraceur" />
    	</batch:listeners>
	</batch:job>
	<batch:step id="etapeRedac" abstract="true">
		<batch:tasklet transaction-manager="txManager" allow-start-if-complete="true">
			<batch:chunk commit-interval="${batch.intervalle_commit:800}">
				<batch:listeners>
            		<batch:listener ref="morceauSynchronePerformanceTraceur" />
        			<batch:listener ref="listenerCollecteErreursValidation" />
				</batch:listeners>
			</batch:chunk>
		</batch:tasklet>
		<batch:listeners>
        	<batch:listener ref="etapeCodeRetourModificateur" />
        	<batch:listener ref="listenerCollecteErreursValidation" />
            <batch:listener ref="etapePerformanceTraceur" />
    	</batch:listeners>
	</batch:step>
    <batch:step id="etapeRedacExtraction" abstract="true">
        <batch:tasklet transaction-manager="txManager" allow-start-if-complete="true">
            <batch:chunk commit-interval="${batch.intervalle_export:500}">
                <batch:listeners>
                    <batch:listener ref="morceauSynchronePerformanceTraceur" />
                </batch:listeners>
            </batch:chunk>
        </batch:tasklet>
        <batch:listeners>
            <batch:listener ref="etapeCodeRetourModificateur" />
            <batch:listener ref="etapePerformanceTraceur" />
        </batch:listeners>
    </batch:step>
	<batch:step id="tacheRedac" abstract="true">
		<batch:tasklet transaction-manager="txManager" allow-start-if-complete="true">
			<batch:listeners>
           		<batch:listener ref="morceauSynchronePerformanceTraceur" />
			</batch:listeners>
		</batch:tasklet>
		<batch:listeners>
        	<batch:listener ref="etapeCodeRetourModificateur" />
            <batch:listener ref="etapePerformanceTraceur" />
    	</batch:listeners>
	</batch:step>
		
	<!-- Permet la gestion des collectes d'erreurs sur un ensemble de fichiers importés avant d'arrêter un traitement batch -->
	<bean id="listenerCollecteErreursValidation" class="fr.si2m.red.batch.moteur.erreur.ListenerCollecteErreursValidation" />
    
	<bean id="erreursCollecteesDecideur" class="fr.si2m.red.batch.moteur.erreur.ErreursCollecteesDecideur" />
	<bean id="tacheLancementExceptionErreursCollectees" class="fr.si2m.red.batch.moteur.erreur.TacheLancementExceptionErreursCollectees" />
	<batch:step id="etapeArretSurErreurBloquante">
		<batch:tasklet transaction-manager="txManager" allow-start-if-complete="true" ref="tacheLancementExceptionErreursCollectees" />
		<batch:listeners>
        	<batch:listener ref="etapeCodeRetourModificateur" />
            <batch:listener ref="etapePerformanceTraceur" />
    	</batch:listeners>
	</batch:step>
	
    <!-- Explorateur des executions des batchs en base -->
    <bean id="jobExplorer" class="org.springframework.batch.core.explore.support.JobExplorerFactoryBean">
        <property name="dataSource" ref="dataSource" />
    </bean>
    
    <!-- Référentiel des fichiers sauvegardés en base -->
    <bean id="fichierBatchRepository" class="fr.si2m.red.batch.moteur.execution.JdbcFichierBatchRepository" />
	
	<!-- Utilitaires (si besoin pour la suite) -->
	<bean id="editeurDecimalesFrancaises" class="fr.si2m.red.batch.moteur.support.DecimalesFrancaisesProprieteEditeur" />
	
	<beans profile="test">
		<!-- Référentiel des jobs in-memory pour les tests -->
		<bean id="jobRepository" class="org.springframework.batch.core.repository.support.MapJobRepositoryFactoryBean">
			<property name="transactionManager" ref="txManager" />
		</bean>
		
		<!-- Initialisation de la base in-memory pour la sauvegarde des fichiers importés -->
		<jdbc:initialize-database data-source="dataSource">
			<jdbc:script location="classpath:/fr/si2m/red/batch/internal/test/db/01_batch_schema.sql" />
		</jdbc:initialize-database>
	</beans>
 	 
	<!-- QA batch configuration -->
	<beans profile="testBatch">
		<!-- Référentiel des jobs persisté sur la base de test TestDatabase -->
		<bean id="jobRepository" class="org.springframework.batch.core.repository.support.JobRepositoryFactoryBean">
			<property name="dataSource" ref="dataSource" />
			<property name="transactionManager" ref="txManager" />
			<property name="databaseType" value="h2" />
		</bean>
		
		<!-- Référence à la classe TestDatabase qui lance une base en mémoire utilisable à l'intérieur et à l'extérieur du contexte Spring -->
		<bean id="dataSource" class="org.springframework.jdbc.datasource.DriverManagerDataSource">
			<property name="driverClassName" value="org.h2.Driver" />
			<property name="url" value="jdbc:h2:tcp://localhost:9092/~/test" />
			<property name="username" value="sa" />
			<property name="password" value="" />
		</bean>
		
		<!-- Initialisation de la base TestDatabase pour supporter les batchs -->
		<jdbc:initialize-database data-source="dataSource">
			<jdbc:script location="classpath:/org/springframework/batch/core/schema-drop-h2.sql" />
			<jdbc:script location="classpath:/org/springframework/batch/core/schema-h2.sql" />
			<jdbc:script location="classpath:/fr/si2m/red/batch/internal/test/db/01_batch_schema.sql" />
		</jdbc:initialize-database>
		
		<!-- Configuration JPA H2 -->
		<bean id="entityManagerFactory" parent="entityManagerFactoryBase">
			<property name="jpaVendorAdapter">
				<bean class="org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter">
					<property name="showSql" value="true" />
					<property name="database" value="H2" />
				</bean>
			</property>
		</bean>
		
	</beans>
	
	<!-- Production configuration pour batch -->
	<beans profile="prod">
	
		<!-- Référentiel des jobs persisté en base -->
		<bean id="jobRepository" class="org.springframework.batch.core.repository.support.JobRepositoryFactoryBean">
			<property name="dataSource" ref="dataSource" />
			<property name="transactionManager" ref="txManager" />
			<property name="databaseType" value="mysql" />
		</bean>
		
	</beans>

</beans>