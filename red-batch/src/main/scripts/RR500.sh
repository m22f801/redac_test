#!/bin/sh

if [ $# -ne 3 ]
then
    echo "La commande prend en arguments :"
	echo "1 - Le répertoire de sortie des fichiers."
	echo "2 - La date de calcul des indicateurs."
    echo "3 - Le SI AVAL."
    exit 80
fi

# Répertoire du script
CHEMIN_SCRIPT=$(readlink -f "$0")
CHEMIN_REPERTOIRE_SCRIPT=$(dirname "$CHEMIN_SCRIPT")

# Récupération nom batch pour nommage fichier de logs
FICHIER_PROPERTIES=${CHEMIN_REPERTOIRE_SCRIPT}/../conf/RR500.properties

while IFS=$' \t\n\r' read line 
do
	if [[ $line == audit.nom_batch=* ]]
	then
		NOM_BATCH="${line#*=}"  
	fi
done < $FICHIER_PROPERTIES

# Paramètres entrants
REPERTOIRE_SORTIE=$1
DATE_CALCUL_INDICATEURS=$2
SI_AVAL=$3

# Répertoire d'exécution du script
REPERTOIRE_EXEC=`pwd`
# Préparation du fichier de récapitulatif des erreurs
mkdir -p ${REPERTOIRE_EXEC}/logs
FICHIER_ERREURS=${REPERTOIRE_EXEC}/logs/${NOM_BATCH}.err
# Préparation du timestamp pour l'export
TIMESTAMP_EXPORT=`date +%y%m%d_%H%M%S`

java -Xms1024m -Xmx2048m -XX:PermSize=128m -XX:MaxPermSize=256m -DconfigBatch=${FICHIER_PROPERTIES} -cp ${CHEMIN_REPERTOIRE_SCRIPT}/../conf:${CHEMIN_REPERTOIRE_SCRIPT}/../bin/red-batch.jar fr.si2m.red.batch.facade.BatchFacade flux56 ${REPERTOIRE_SORTIE} ${DATE_CALCUL_INDICATEURS} ${SI_AVAL} ${TIMESTAMP_EXPORT} ${FICHIER_ERREURS}
exit $?