#!/bin/sh

if [ $# -ne 2 ]
then
    echo "La commande prend en argument :"
	echo "1 - le répertoire des fichiers à importer;"
	echo "2 - HORODATAGE_FICHIERS."
	echo ""
	echo "Les valeurs du fichier de configuration doivent être mises \"entre double-quotes\" si elles comportent un espace"
    exit 80
fi

# Répertoire du script
CHEMIN_SCRIPT=$(readlink -f "$0")
CHEMIN_REPERTOIRE_SCRIPT=$(dirname "$CHEMIN_SCRIPT")
FICHIER_PROPERTIES=${CHEMIN_REPERTOIRE_SCRIPT}/../conf/RR401.properties
echo "$FICHIER_PROPERTIES"

# Paramètres entrants
REPERTOIRE_FICHIERS=$1
HORODATAGE_FICHIERS=$2

if [ ! -d "$REPERTOIRE_FICHIERS" ]
then
	echo "Le répertoire renseigné n'existe pas"
	exit 80
fi

# Lecture du fichier de configuration
while IFS=$' \t\n\r' read line 
do
    if [[ $line == batch.code_brique_dsn=* ]]
	then
		 CODE_BRIQUE_DSN="${line#*=}"  
	else 
		if [[ $line == batch.systeme_information=* ]]
		then
			SYSTEME_INFORMATION="${line#*=}"  
		else 
			if [[ $line == batch.champ_contexte=* ]]
			then
				CONTEXTE="${line#*=}"  
			else
				if [[ $line == batch.code_application=* ]]
				then
					CODE_APPLICATION="${line#*=}"  
				else 
					if [[ $line == audit.environnement=* ]]
					then
						NUMERO_ENVIRONNEMENT="${line#*=}"  
					else 
						if [[ $line == audit.nom_batch=* ]]
						then
							NOM_BATCH="${line#*=}"  
						fi
					fi
				fi
			fi
		fi
	fi
done < $FICHIER_PROPERTIES


# Répertoire d'exécution du script
REPERTOIRE_EXEC=`pwd`
# Préparation du fichier de récapitulatif des erreurs
mkdir -p ${REPERTOIRE_EXEC}/logs
FICHIER_ERREURS=${REPERTOIRE_EXEC}/logs/${NOM_BATCH}.err

java -Xms1024m -Xmx2048m -XX:PermSize=128m -XX:MaxPermSize=256m  -DconfigBatch=${FICHIER_PROPERTIES} -cp ${CHEMIN_REPERTOIRE_SCRIPT}/../conf:${CHEMIN_REPERTOIRE_SCRIPT}/../bin/red-batch.jar fr.si2m.red.batch.facade.BatchFacade flux4_ControleFichiers "${REPERTOIRE_FICHIERS}" "${HORODATAGE_FICHIERS}" "${CODE_BRIQUE_DSN//[$'\t\r\n']}" "${SYSTEME_INFORMATION//[$'\t\r\n']}" "${CONTEXTE//[$'\t\r\n']}" "${CODE_APPLICATION//[$'\t\r\n']}" "${NUMERO_ENVIRONNEMENT//[$'\t\r\n']}" "${NOM_BATCH//[$'\t\r\n']}" ${FICHIER_ERREURS}
exit $?