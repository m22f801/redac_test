#!/bin/sh

if [ $# -ne 3 ]
then
    echo "La commande prend en argument :"
    echo "1 - le répertoire des fichiers à importer."
    echo "2 - l'identifiant d'importation."
    echo "3 - le mode de chargement."
    exit 80
fi

# Répertoire du script
CHEMIN_SCRIPT=$(readlink -f "$0")
CHEMIN_REPERTOIRE_SCRIPT=$(dirname "$CHEMIN_SCRIPT")

# Récupération nom batch pour nommage fichier de logs
FICHIER_PROPERTIES=${CHEMIN_REPERTOIRE_SCRIPT}/../conf/RR100.properties

while IFS=$' \t\n\r' read line 
do
	if [[ $line == audit.nom_batch=* ]]
	then
		NOM_BATCH="${line#*=}"  
	fi
done < $FICHIER_PROPERTIES

# Paramètres entrants
REPERTOIRE_FICHIERS=$1
ID_FLUX=$2
MODE_CHARGEMENT=$3

# Répertoire d'exécution du script
REPERTOIRE_EXEC=`pwd`
# Préparation du fichier de récapitulatif des erreurs
mkdir -p ${REPERTOIRE_EXEC}/logs
FICHIER_ERREURS=${REPERTOIRE_EXEC}/logs/${NOM_BATCH}.err

java -Xms1024m -Xmx2048m -XX:PermSize=128m -XX:MaxPermSize=256m -DconfigBatch=${FICHIER_PROPERTIES} -cp ${CHEMIN_REPERTOIRE_SCRIPT}/../conf:${CHEMIN_REPERTOIRE_SCRIPT}/../bin/red-batch.jar fr.si2m.red.batch.facade.BatchFacade flux1 ${REPERTOIRE_FICHIERS} ${ID_FLUX} ${MODE_CHARGEMENT} ${FICHIER_ERREURS}
exit $?