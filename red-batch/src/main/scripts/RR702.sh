#!/bin/sh

if [ $# -ne 1 ]
then
    echo "La commande prend en argument :"
	echo "1 - le chemin complet du fichier à traiter."
    exit 80
fi

# Répertoire du script
CHEMIN_SCRIPT=$(readlink -f "$0")
CHEMIN_REPERTOIRE_SCRIPT=$(dirname "$CHEMIN_SCRIPT")

# Récupération nom batch pour nommage fichier de logs
FICHIER_PROPERTIES=${CHEMIN_REPERTOIRE_SCRIPT}/../conf/RR702.properties

while IFS=$' \t\n\r' read line 
do
	if [[ $line == audit.nom_batch=* ]]
	then
		NOM_BATCH="${line#*=}"  
	fi
done < $FICHIER_PROPERTIES

# Paramètres entrants
CHEMIN_FICHIER_ENTREE=$1

# Test du paramètre entrant
if [ -f "$CHEMIN_FICHIER_ENTREE" ]
then 
	DIR=$(dirname "${CHEMIN_FICHIER_ENTREE}")
	FILENAME=$(basename "$CHEMIN_FICHIER_ENTREE")
	EXTENSION="${FILENAME##*.}"
	FILENAME="${FILENAME%.*}"
	if [ -z "$EXTENSION" -o "$EXTENSION" = "$FILENAME" -o "$EXTENSION" = "tri" ]
	then 
    	echo "L'extension '$EXTENSION' du fichier n'est pas correcte"
	    exit 80
	fi
else
    echo "Le fichier renseigné n'existe pas"
    exit 80
fi

# tri du fichier
CHEMIN_FICHIER_TRIE=$DIR"/"$FILENAME".tri"
sort ${CHEMIN_FICHIER_ENTREE} > ${CHEMIN_FICHIER_TRIE}

# Répertoire d'exécution du script
REPERTOIRE_EXEC=`pwd`
# Préparation du fichier de récapitulatif des erreurs
mkdir -p ${REPERTOIRE_EXEC}/logs
FICHIER_ERREURS=${REPERTOIRE_EXEC}/logs/${NOM_BATCH}.err

java -Xms1024m -Xmx2048m -XX:PermSize=128m -XX:MaxPermSize=256m -DconfigBatch=${FICHIER_PROPERTIES} -cp ${CHEMIN_REPERTOIRE_SCRIPT}/../conf:${CHEMIN_REPERTOIRE_SCRIPT}/../bin/red-batch.jar fr.si2m.red.batch.facade.BatchFacade flux78_TraitementCR ${CHEMIN_FICHIER_TRIE} ${FICHIER_ERREURS}
exit $?