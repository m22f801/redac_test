#!/bin/sh

if [ $# -ne 2 ]
then
    echo "La commande prend en argument :"
	echo "1 - le nom du paramétrage à importer;"
	echo "2 - le chemin complet vers le fichier de paramétrage à importer."
	echo ""
    echo "Les noms des paramétrages importables sont :"
    echo "* CategoriesEffectifs"
    echo "* CodeLibelle"
    echo "* Contact"
    echo "* ControleSignalRejet"
    echo "* EtatContrat"
    echo "* FamilleContrat"
    echo "* FamilleModeCalculContrat"
    echo "* GestionPeriodes"
    echo "* GroupeGestion"
    echo "* NatureBase"
    echo "* NatureBaseComposants"
    echo "* PlafondsTaux"
    echo "* ValeurDefaut"
    echo "* UtilisateurGestionnaire"
    echo "* UtilisateurContratsVIP"
    echo "* SirenFaux"
	echo "* CodePays"
	echo ""
    exit 80
fi

# Répertoire du script
CHEMIN_SCRIPT=$(readlink -f "$0")
CHEMIN_REPERTOIRE_SCRIPT=$(dirname "$CHEMIN_SCRIPT")

# Paramètres entrants
NOM_DU_BATCH=$1
CHEMIN_ABSOLU_FICHIER=$2
NOM_COMPLET_DU_BATCH="flux10_"$NOM_DU_BATCH

# Récupération nom batch pour nommage fichier de logs
FICHIER_PROPERTIES=${CHEMIN_REPERTOIRE_SCRIPT}/../conf/RR000.properties

while IFS=$' \t\n\r' read line 
do
	if [[ $line == audit.nom_batch=* ]]
	then
		NOM_BATCH="${line#*=}"  
	fi
done < $FICHIER_PROPERTIES

# Répertoire d'exécution du script
REPERTOIRE_EXEC=`pwd`
# Préparation du fichier de récapitulatif des erreurs
mkdir -p ${REPERTOIRE_EXEC}/logs
FICHIER_ERREURS=${REPERTOIRE_EXEC}/logs/${NOM_BATCH}.err

java -Xms1024m -Xmx2048m -XX:PermSize=128m -XX:MaxPermSize=256m -DconfigBatch=${FICHIER_PROPERTIES} -cp ${CHEMIN_REPERTOIRE_SCRIPT}/../conf:${CHEMIN_REPERTOIRE_SCRIPT}/../bin/red-batch.jar fr.si2m.red.batch.facade.BatchFacade $NOM_COMPLET_DU_BATCH $CHEMIN_ABSOLU_FICHIER $FICHIER_ERREURS
exit $?