#!/bin/sh

if [ $# -ne 3 ]
then
    echo "La commande prend en argument :"
	echo "1 - le répertoire des fichiers à importer;"
	echo "2 - HORODATAGE_FICHIERS;"
	echo "3 - le type du fichier (ex : \"ADHESION_ETAB_MOIS\")."
	echo ""
    echo "Les types de fichiers importables sont :"
	echo "* ADHESION_ETAB_MOIS"
	echo "* CONTACT"
	echo "* COTIS_ETAB"
	echo "* VERSEMENT"
	echo "* COMPO_VERST"
	echo "* INDIVIDU"
	echo "* CHGT_INDIV"
	echo "* VERS_INDIV"
	echo "* REMUNERATION"
	echo "* CONTR_TRAV"
	echo "* ARRET_TRAV"
	echo "* AFFILIATION"
	echo "* AYANT_DROIT"
	echo "* BASE_ASSUJ"
	echo "* COMPO_BASE_ASSUJ"
	echo ""
	echo "Les valeurs du fichier de configuration doivent être mises \"entre double-quotes\" si elles comportent un espace"
    exit 80
fi

# Répertoire du script
CHEMIN_SCRIPT=$(readlink -f "$0")
CHEMIN_REPERTOIRE_SCRIPT=$(dirname "$CHEMIN_SCRIPT")
FICHIER_PROPERTIES=${CHEMIN_REPERTOIRE_SCRIPT}/../conf/RR402.properties

# Paramètres entrants
REPERTOIRE_FICHIERS=$1
HORODATAGE_FICHIERS=$2
TYPE_FICHIER=$3

if [ ! -d "$REPERTOIRE_FICHIERS" ]
then
	echo "Le répertoire renseigné n'existe pas"
	exit 80
fi

# Lecture du fichier de configuration
while IFS=$' \t\n\r' read line 
do
    if [[ $line == batch.code_brique_dsn=* ]]
	then
		 CODE_BRIQUE_DSN="${line#*=}"  
	else 
		if [[ $line == batch.systeme_information=* ]]
		then
			SYSTEME_INFORMATION="${line#*=}"  
		else 
			if [[ $line == batch.champ_contexte=* ]]
			then
				CONTEXTE="${line#*=}"
			else
				if [[ $line == batch.code_application=* ]]
				then
					CODE_APPLICATION="${line#*=}"  
				else 
					if [[ $line == audit.environnement=* ]]
					then
						NUMERO_ENVIRONNEMENT="${line#*=}"  
					else
						if [[ $line == audit.nom_batch=* ]]
						then
							NOM_BATCH="${line#*=}"  
						fi
					fi
				fi
			fi
		fi
	fi
done < $FICHIER_PROPERTIES

# Répertoire d'exécution du script
REPERTOIRE_EXEC=`pwd`
# Préparation du fichier de récapitulatif des erreurs
mkdir -p ${REPERTOIRE_EXEC}/logs
FICHIER_ERREURS=${REPERTOIRE_EXEC}/logs/${NOM_BATCH}.err

java -Xms1024m -Xmx2048m -XX:PermSize=128m -XX:MaxPermSize=256m -DconfigBatch=${FICHIER_PROPERTIES}  -cp ${CHEMIN_REPERTOIRE_SCRIPT}/../conf:${CHEMIN_REPERTOIRE_SCRIPT}/../bin/red-batch.jar fr.si2m.red.batch.facade.BatchFacade flux4_"${TYPE_FICHIER}" "${REPERTOIRE_FICHIERS}" "${HORODATAGE_FICHIERS}" "${TYPE_FICHIER}" "${CODE_BRIQUE_DSN//[$'\t\r\n']}" "${SYSTEME_INFORMATION//[$'\t\r\n']}" "${CONTEXTE//[$'\t\r\n']}" "${CODE_APPLICATION//[$'\t\r\n']}" "${NUMERO_ENVIRONNEMENT//[$'\t\r\n']}" ${FICHIER_ERREURS}
exit $?