#!/bin/sh

if [ $# -ne 3 ]
then
    echo "La commande prend en argument :"
	echo "1 - le répertoire de sortie des fichiers exportés."
	echo "2 - le Système d'Information Source."
	echo "3 - le Contexte."
    exit 80
fi


# Répertoire du script
CHEMIN_SCRIPT=$(readlink -f "$0")
CHEMIN_REPERTOIRE_SCRIPT=$(dirname "$CHEMIN_SCRIPT")
FICHIER_PROPERTIES=${CHEMIN_REPERTOIRE_SCRIPT}/../conf/RR300.properties

# Lecture du fichier de configuration
while IFS=$' \t\n\r' read line 
do
	if [[ $line == audit.environnement=* ]]
	then
		NUMERO_ENVIRONNEMENT="${line#*=}"  
	else 
		if [[ $line == audit.nom_batch=* ]]
		then
			NOM_BATCH="${line#*=}"  
		fi
	fi
done < $FICHIER_PROPERTIES


# Paramètres entrants
REPERTOIRE_SORTIE=$1
SI_SOURCE=$2
CONTEXTE=$3

# Répertoire d'exécution du script
REPERTOIRE_EXEC=`pwd`
# Préparation du fichier de récapitulatif des erreurs
mkdir -p ${REPERTOIRE_EXEC}/logs
FICHIER_ERREURS=${REPERTOIRE_EXEC}/logs/${NOM_BATCH}.err
# Préparation du timestamp pour l'export
TIMESTAMP_EXPORT=`date +%y%m%d_%H%M%S`

java -Xms1024m -Xmx2048m -XX:PermSize=128m -XX:MaxPermSize=256m -DconfigBatch=${FICHIER_PROPERTIES} -cp ${CHEMIN_REPERTOIRE_SCRIPT}/../conf:${CHEMIN_REPERTOIRE_SCRIPT}/../bin/red-batch.jar fr.si2m.red.batch.facade.BatchFacade flux3 ${REPERTOIRE_SORTIE} ${SI_SOURCE} ${CONTEXTE} ${TIMESTAMP_EXPORT} ${FICHIER_ERREURS} 

if [ ! -d "$REPERTOIRE_SORTIE" ]
then
	echo "Le répertoire renseigné n'existe pas"
	exit 80
fi

##########################################
# Génération fichier OPTIONS
# chemin complet fichier OPTIONS
FICHIER_OPTIONS="${REPERTOIRE_SORTIE}/${TIMESTAMP_EXPORT}_${NOM_BATCH//[$'\t\r\n']}_E${NUMERO_ENVIRONNEMENT//[$'\t\r\n']}.opti.sor"

# ecriture ligne entête
printf "%-15s|OPTIONS                  |`date +%d.%m.%Y`|%-5s\\r\\n" $(echo ${SI_SOURCE}${line} | cut -c1-16) $(echo ${CONTEXTE}${line} | cut -c1-6) > ${FICHIER_OPTIONS}
# ecriture ligne de fin
echo "9999999999|00000000002" >> ${FICHIER_OPTIONS}

##########################################

exit $?