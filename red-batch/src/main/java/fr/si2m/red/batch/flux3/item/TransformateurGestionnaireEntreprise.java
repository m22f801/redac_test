package fr.si2m.red.batch.flux3.item;

import fr.si2m.red.batch.flux3.ligne.Gestionnaire;
import fr.si2m.red.batch.moteur.item.TransformateurDonnee;
import fr.si2m.red.parametrage.ResumeGestionnaire;
import fr.si2m.red.reconciliation.ParamGestionnaireUtilisateurRepository;
import lombok.Setter;

/**
 * Mapper des entités Gestionnaire à partir du siren fourni
 * 
 * cf F03_RG_S13
 * 
 * @author eudesr
 *
 */
public class TransformateurGestionnaireEntreprise extends TransformateurDonnee<ResumeGestionnaire, Gestionnaire> {

    @Setter
    private ParamGestionnaireUtilisateurRepository paramUtilisateurGestionnaireRepository;

    @Override
    public Gestionnaire process(ResumeGestionnaire resume) throws Exception {

        // F03_RG_S13
        Gestionnaire gestionnaire = new Gestionnaire();
        gestionnaire.setCodeMiseAJour(resume.getCodeMiseAJour());
        gestionnaire.setIdentifiantTechniqueEntreprise(resume.getIdentifiantTechniqueEntreprise());
        gestionnaire.setTypeMaille(1);
        gestionnaire.setReferenceMaille(resume.getReferenceMaille());
        gestionnaire.setTypeIntervenant("1");
        gestionnaire.setReferenceGestionnaire(resume.getReferenceGestionnaire());
        gestionnaire.setLibelle(resume.getLibelle());
        gestionnaire.setPrenomGestionnaireCompte(resume.getPrenomGestionnaireCompte());
        gestionnaire.setNomGestionnaireCompte(resume.getNomGestionnaireCompte());
        gestionnaire.setUniteGestion(resume.getUniteGestion());
        gestionnaire.setService(resume.getService());
        gestionnaire.setCentreGestion(resume.getCentreGestion());
        gestionnaire.setIdentifiantTechniqueContrat("");
        return gestionnaire;
    }
}
