package fr.si2m.red.batch.moteur.item;

import lombok.Getter;
import lombok.Setter;

import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.core.io.Resource;

import fr.si2m.red.EntiteImportableBatch;
import fr.si2m.red.batch.moteur.BatchConfiguration;
import fr.si2m.red.batch.moteur.support.MapperLigne;
import fr.si2m.red.batch.moteur.support.RedacFieldSetFactory;

/**
 * Base de lecteur de fichiers plats pour les batchs d'import REDAC.
 * 
 * @author nortaina
 *
 * @param <T>
 *            le type des lignes lues
 */
public abstract class LecteurFichierPlat<T extends EntiteImportableBatch> extends FlatFileItemReader<T> {
    /**
     * Le caractère permettant de délimiter des champs dans le fichier plat par défaut.
     */
    public static final String DELIMITEUR_PAR_DEFAUT = ";";

    /**
     * Le mapper de ligne.
     * 
     * @return le mapper de ligne
     */
    @Getter
    private MapperLigne<T> mapperLigne;

    /**
     * Si les champs doivent toujours être élagués ou non.
     * 
     * @param alwaysTrim
     *            si les champs doivent toujours être élagués ou non
     */
    @Setter
    private boolean alwaysTrim = false;

    /**
     * Le caractère permettant de délimiter des champs dans le fichier plat.
     * 
     * 
     * @param delimiteurSpecifique
     *            le délimiteur spécifique utilisé dans le fichier importé
     */
    @Setter
    private String delimiteurChamps = DELIMITEUR_PAR_DEFAUT;

    /**
     * Constructeur de lecteur de fichier plat.
     * 
     * @param nomsChamps
     *            les noms des champs à lire / mapper
     * @param nomPrototypeLigne
     *            le nom du prototype d'une ligne lue dans le contexte Spring
     * @param beanFactory
     *            l'usine de beans Spring où est référencé le prototype de ligne
     */
    protected LecteurFichierPlat(String[] nomsChamps, String nomPrototypeLigne, BeanFactory beanFactory) {
        super();
        initialise(nomsChamps, nomPrototypeLigne, beanFactory);
    }

    /**
     * Constructeur de lecteur de fichier plat. (Surcharge)
     * 
     * @param nomsChamps
     *            les noms des champs à lire / mapper
     * @param nomPrototypeLigne
     *            le nom du prototype d'une ligne lue dans le contexte Spring
     * @param beanFactory
     *            l'usine de beans Spring où est référencé le prototype de ligne
     * @param delimiteurSpecifique
     *            le délimiteur de caractères dans le fichier importé
     */
    protected LecteurFichierPlat(String[] nomsChamps, String nomPrototypeLigne, BeanFactory beanFactory, String delimiteurSpecifique) {
        super();
        // Définition du délimiteur
        setDelimiteurChamps(delimiteurSpecifique);

        initialise(nomsChamps, nomPrototypeLigne, beanFactory);
    }

    /**
     * Initialise des attributs pour le Constructeur de lecteur de fichier plat.
     * 
     * @param nomsChamps
     *            les noms des champs à lire / mapper
     * @param nomPrototypeLigne
     *            le nom du prototype d'une ligne lue dans le contexte Spring
     * @param beanFactory
     *            l'usine de beans Spring où est référencé le prototype de ligne
     */
    private void initialise(String[] nomsChamps, String nomPrototypeLigne, BeanFactory beanFactory) {
        // Initialisation du mapper de ligne
        mapperLigne = initBaseMapperLigne(nomsChamps, nomPrototypeLigne, beanFactory);

        // Enregistrement auprès du parent
        setLineMapper(mapperLigne);

        // Encodage par défaut
        setEncoding(BatchConfiguration.ENCODAGE_FICHIERS_IMPORTES.name());

        // Pas de caractère de début de ligne identifié pour les commentaires
        setComments(new String[] {});
    }

    /**
     * Initialise la base du mapper de ligne utilisé par le lecteur.
     * 
     * @param nomsChamps
     *            les noms ordonnés des champs d'une ligne
     * @param nomPrototypeLigne
     *            le nom du prototype à utiliser pour une entité ligne mappée
     * @param beanFactory
     *            l'usine de bean pour la récupération du prototype de ligne
     * @return le mapper de ligne initialisé dans le lecteur (sans la stratégie de lecture / conversion des champs)
     */
    protected MapperLigne<T> initBaseMapperLigne(String[] nomsChamps, String nomPrototypeLigne, BeanFactory beanFactory) {
        MapperLigne<T> mapperLigneInitialise = new MapperLigne<T>();
        DelimitedLineTokenizer delimiteur = new DelimitedLineTokenizer(delimiteurChamps) {
            @Override
            protected boolean isQuoteCharacter(char c) {
                return false;
            }
        };
        delimiteur.setNames(nomsChamps);
        RedacFieldSetFactory usineFieldSet = new RedacFieldSetFactory();
        usineFieldSet.setAlwaysTrim(alwaysTrim);
        delimiteur.setFieldSetFactory(usineFieldSet);
        mapperLigneInitialise.setLineTokenizer(delimiteur);
        mapperLigneInitialise.setBeanFactory(beanFactory);
        mapperLigneInitialise.setNomPrototypeLigne(nomPrototypeLigne);
        return mapperLigneInitialise;
    }

    /**
     * Met à jour le nom du batch exécutant la lecture.
     * 
     * @param nomBatch
     *            le nom du batch exécutant la lecture
     */
    public void setNomBatch(String nomBatch) {
        mapperLigne.setNomBatch(nomBatch);
    }

    @Override
    public void setResource(Resource resource) {
        // Mise à jour du nom du fichier en cours de traitement dans le mapper de ligne
        mapperLigne.setNomFichierTraite(resource.getFilename());
        super.setResource(resource);
    }
}
