package fr.si2m.red.batch.flux3.item;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import fr.si2m.red.batch.flux3.ligne.SituationContrat;
import fr.si2m.red.batch.moteur.item.JpaTempRepository;
import fr.si2m.red.core.repository.jdbc.JdbcRepository;

/**
 * Rédacteur de situations contrats exportées dans une table de données temporaires pour consultation ultérieure.
 * 
 * @author poidij
 *
 */
@Repository
public class JpaSituationContratTempRepository extends JpaTempRepository<SituationContrat> {

    @Override
    public Class<SituationContrat> getClassePrototypeEntite() {
        return SituationContrat.class;
    }

    /**
     * Retourne la date de fin de la derniere situation contrat, pour le taux d'appel donné
     * 
     * @param noco
     *            le numéro contrat
     * @return la date de fin
     */
    Integer getDateFinDerniereSituation(String noco) {
        String sql = "SELECT DATE_FIN_SIT FROM TMP_RR300_SITUATIONS_CONTRATS WHERE NOCO= :numContrat ORDER BY DATE_DEB_SIT DESC LIMIT 1";
        Map<String, Object> params = new HashMap<>();
        params.put("numContrat", noco);

        return getPremierResultatSiExiste(getJdbcTemplate().query(sql, params, new RowMapper<Integer>() {

            @Override
            public Integer mapRow(ResultSet rs, int rowNum) throws SQLException {
                return JdbcRepository.getInteger(rs, "DATE_FIN_SIT");
            }
        }));
    }
}
