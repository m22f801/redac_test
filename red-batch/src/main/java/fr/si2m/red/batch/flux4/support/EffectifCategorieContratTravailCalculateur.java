package fr.si2m.red.batch.flux4.support;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import fr.si2m.red.RedacUnexpectedException;
import fr.si2m.red.complement.ExtensionContrat;
import fr.si2m.red.complement.ExtensionContratRepository;
import fr.si2m.red.contrat.ContratRepository;
import fr.si2m.red.contrat.NumCategorieLibelleCategorie;
import fr.si2m.red.contrat.Tarif;
import fr.si2m.red.contrat.TarifRepository;
import fr.si2m.red.dsn.AdhesionEtablissementMois;
import fr.si2m.red.dsn.Affiliation;
import fr.si2m.red.dsn.AffiliationRepository;
import fr.si2m.red.dsn.BaseAssujettie;
import fr.si2m.red.dsn.BaseAssujettieRepository;
import fr.si2m.red.dsn.ContratTravail;
import fr.si2m.red.dsn.PlageRattachement;
import fr.si2m.red.parametrage.ParamCategorieEffectifs;
import fr.si2m.red.parametrage.ParamCategorieEffectifsId;
import fr.si2m.red.parametrage.ParamCategorieEffectifsRepository;
import fr.si2m.red.parametrage.ParamPlafondTauxRepository;
import fr.si2m.red.parametrage.ParamValeurDefautRepository;
import fr.si2m.red.reconciliation.EffectifCategorieContratTravail;
import fr.si2m.red.reconciliation.IndicateursDSNPeriode;
import fr.si2m.red.reconciliation.IndicateursDSNPeriodeRepository;
import fr.si2m.red.reconciliation.PeriodeRecue;
import fr.si2m.red.reconciliation.PeriodeRecueRepository;
import fr.si2m.red.reconciliation.RattachementDeclarationsRecuesRepository;

/**
 * Constructeur d'entité {@link EffectifCategorieContratTravail} basées sur les RDG de la phase 5 du flux 4.
 * 
 * @author nortaina
 *
 */
@Component
public class EffectifCategorieContratTravailCalculateur {
    private static final Logger LOGGER = LoggerFactory.getLogger(EffectifCategorieContratTravailCalculateur.class);

    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    private RattachementDeclarationsRecuesRepository rattachementDeclarationsRecuesRepository;
    @Autowired
    private BaseAssujettieRepository baseAssujettieRepository;
    @Autowired
    private PeriodeRecueRepository periodeRecueRepository;
    @Autowired
    private IndicateursDSNPeriodeRepository indicateursDSNPeriodeRepository;
    @Autowired
    private ExtensionContratRepository extensionContratRepository;
    @Autowired
    private ParamValeurDefautRepository paramValeurDefautRepository;
    @Autowired
    private TarifRepository tarifRepository;
    @Autowired
    private AffiliationRepository affiliationRepository;
    @Autowired
    private ParamCategorieEffectifsRepository paramCategorieEffectifsRepository;
    @Autowired
    private ParamPlafondTauxRepository paramPlafondTauxRepository;
    @Autowired
    private ContratRepository contratRepository;

    private final String auditNomBatch;

    private static final String ERREUR_PARAM_CATEGORIE_EFFECTIF = "Détection d''un mode de réaffectation de catégorie effectif REACA sans ParamCategorieEffectif associé à {0} pour la période [idPeriode={1}; numContrat={2}; typePeriode={3}; dateDebutPeriode={4}; dateCreation={5}] et le contrat de travail [idContratTravail={6}]";

    private static final String ERREUR_MODE_REAFFECTATION_INCONNU = "Mode de réaffectation inconnu de l''application : {0}. Période traitée : [idPeriode={1}; numContrat={2}; typePeriode={3}; dateDebutPeriode={4}; dateCreation={5}]. Origine : {6}";

    private static final String ERREUR_TARIF = "La situation tarifaire [numContrat={0}, numCategorieQuittancement={1}, date={2}] n''existe pas. Période traitée : [idPeriode={3}; numContrat={4}; typePeriode={5}; dateDebutPeriode={6}; dateCreation={7}]";

    private static final String ERREUR_PLAFOND_TAUX = "Il n''existe pas de ParamPlafondTaux en vigueur à la date {0}. Période traitée : [idPeriode={1}; numContrat={2}; typePeriode={3}; dateDebutPeriode={4}; dateCreation={5}]";

    private static final String ERREUR_CONTRAT = "La situation contrat [numContrat={0}, date=({1})] n''existe pas. Période traitée : [idPeriode={2}; numContrat={3}; typePeriode={4}; dateDebutPeriode={5}; dateCreation={6}]";

    private String origineModeReaffectation = "PARAM_VALEUR_DEFAUT";

    /**
     * Constructeur par défaut du calculateur.
     * 
     */
    public EffectifCategorieContratTravailCalculateur() {
        this.auditNomBatch = "RR405";
    }

    /**
     * Constructeur du calculateur.
     * 
     * @param auditNomBatch
     *            le nom du batch appelant le calculateur
     */
    public EffectifCategorieContratTravailCalculateur(String auditNomBatch) {
        this.auditNomBatch = auditNomBatch;
    }

    /**
     * Calcule et persiste les entités pour la période reçue.
     * 
     * @param periode
     *            la période reçue
     * 
     * @return le nombre d'entités persistées
     */
    public long calculeEtPersiste(PeriodeRecue periode) {
        long nbEntitesPersistees = 0L;

        Long idPeriode = periode.getIdPeriode();
        String modeReaffectationCategorieEffectif = resoudModeReaffectationCategorieEffectif(periode);
        Set<NumCategorieLibelleCategorie> couplesNumCatLiCatDistincts = tarifRepository
                .getDistinctNumCategorieLibelleCategoriePourContratEtDate(periode.getNumeroContrat(), periode.getDateDebutPeriode());

        // Avec des hypothèses pessimistes, on peut potentiellement avoir de l'ordre de 100 000 contrats de travails à parcourir par entité PeriodeRecue
        // => On pagine les traitements par 5000 contrats
        final int taillePage = 5000;
        int numeroPage = 0;

        // Itération sur chaque contrat de travail rattaché à la période
        List<ContratTravail> contratsTravail = rattachementDeclarationsRecuesRepository.getContratsTravailRattachesPagines(idPeriode,
                numeroPage * taillePage, taillePage);
        while (!contratsTravail.isEmpty()) {
            LOGGER.debug("Traitement de la page {} de {} contrats de travail en cours...", numeroPage, taillePage);
            for (ContratTravail contratTravail : contratsTravail) {
                String idContratTravail = contratTravail.getIdContratTravail();
                List<Affiliation> affiliations = affiliationRepository.getPourContratTravail(idContratTravail);
                List<PlageRattachement> listePlages = baseAssujettieRepository
                        .getPlagesRattachementsPourContratTravailAvecMontantCotisationNonNul(idContratTravail);

                // Récupération AdhesionEtablisementMois ratachée
                AdhesionEtablissementMois adhesion = contratTravail.getIndividu().getAdhesionEtablissementMois();

                // flag récupération taux d'appel -- F04_RG_P5_68
                boolean tauxAppelRecupere = false;
                Double tauxAppel = null;

                for (int j = 0; j < listePlages.size(); j++) {
                    PlageRattachement plage = listePlages.get(j);
                    // Récupération des bases traitées sur la plage
                    List<BaseAssujettie> listeBasesTraitees = baseAssujettieRepository
                            .getBasesAssujettiesAvecMontantCotisationNonNul(idContratTravail, plage.getDateDebutPlage());

                    // Création des effectifs temporaires sur la plage pour les calculs ultérieurs
                    List<EffectifCategorieContratTemp> effectifsTemp = creeEffectifsCategorieContratTemp(couplesNumCatLiCatDistincts, affiliations,
                            listeBasesTraitees, modeReaffectationCategorieEffectif, periode);

                    // Initialisation des effectifs sur la plage, par numéro de catégorie de quittancement
                    Map<String, EffectifCategorieContratTravail> effectifsPlage = initialiseEffectifsCategorieContratTravail(
                            couplesNumCatLiCatDistincts, idPeriode, contratTravail, plage, adhesion.getMoisRattachement());

                    // Alimentation nombre affiliés
                    String numCategorieQuittancementPourNombreAffilies = getNumCategorieQuittancementPourNombreAffilies(effectifsTemp);
                    effectifsPlage.get(numCategorieQuittancementPourNombreAffilies).setNombreAffilies(1);

                    // Réaffectation des ayants droit adultes
                    reaffecteAyantsDroitAdultes(effectifsPlage, effectifsTemp);
                    // Réaffectation des ayants droit autres
                    reaffecteAyantsDroitAutres(effectifsPlage, effectifsTemp);
                    // Réaffectation des ayants droit enfants
                    reaffecteAyantsDroitEnfants(effectifsPlage, effectifsTemp);

                    if (!tauxAppelRecupere) {
                        tauxAppel = contratRepository.getTauxAppelContratValidePourDate(periode.getNumeroContrat(), periode.getDateDebutPeriode());
                        if (tauxAppel == null) {
                            String logErreur = MessageFormat.format(ERREUR_CONTRAT, periode.getNumeroContrat(),
                                    periode.getDateDebutPeriode().toString(), periode.getIdPeriode(), periode.getNumeroContrat(),
                                    periode.getTypePeriode(), periode.getDateDebutPeriode().toString(), periode.getDateCreation().toString());
                            throw new RedacUnexpectedException(logErreur);
                        }
                        tauxAppelRecupere = true;
                    }

                    // Calculs de total effectif, montant forfait et estimation cotisation
                    calculeTotalEffectifEtMontantForfaitEtEstimationCotisation(effectifsPlage.values(), periode, adhesion.getMoisDeclare(),
                            adhesion.getMoisRattachement(), tauxAppel);

                    // Calcul des prorata de cotisation
                    calculeProrataCotisations(effectifsPlage.values(), listeBasesTraitees);

                    // On enregistre tous les effectifs calculés
                    nbEntitesPersistees += enregistreContratsTravailSurEffectifEtCategorie(effectifsPlage.values());
                }
                // On libère progressivement la mémoire
                entityManager.detach(contratTravail);
                for (Affiliation affiliation : affiliations) {
                    entityManager.detach(affiliation);
                }
            }
            entityManager.flush();
            entityManager.clear();
            LOGGER.debug("Fin du traitement de la page {} des contrats de travail !", numeroPage);
            // On récupère la page suivante
            numeroPage++;
            contratsTravail = rattachementDeclarationsRecuesRepository.getContratsTravailRattachesPagines(idPeriode, numeroPage * taillePage,
                    taillePage);
        }
        return nbEntitesPersistees;
    }

    /**
     * Effectue un enregistrement de plusieurs contrats de travail à la fois.
     * 
     * @param contratsTravail
     *            les contrats à enregistrer
     * @return le nombre de contrats enregistrés
     */
    private long enregistreContratsTravailSurEffectifEtCategorie(Collection<EffectifCategorieContratTravail> contratsTravail) {
        for (EffectifCategorieContratTravail contratTravailSurEffectifEtCategorie : contratsTravail) {
            entityManager.persist(contratTravailSurEffectifEtCategorie);
        }
        return contratsTravail.size();
    }

    /**
     * Récupère le numéro de catégorie de quittancement pour lequel une affiliation sera comptabilisée pour une plage donnée.
     * 
     * @param effectifsTemp
     *            les effectifs temporaires sur la plage
     * @return le numéro de catégorie de quittancement pour lequel l'affiliation sera comptabilisée
     */
    private String getNumCategorieQuittancementPourNombreAffilies(List<EffectifCategorieContratTemp> effectifsTemp) {
        // Recette : trier les lignes de EffectifCategorieContratTemp par prioriteAffilies descendante
        // puis par indicateurAffiliation descendant
        // puis par numCategorieQuittancement descendant.
        // Récupérer le numCategorieQuittancement de la première ligne.
        // => Equivalent : on prend le max sur un tri ascendant
        return Collections.max(effectifsTemp, new Comparator<EffectifCategorieContratTemp>() {
            @Override
            public int compare(EffectifCategorieContratTemp effectif1, EffectifCategorieContratTemp effectif2) {
                Integer diffPrio = effectif1.getPrioriteAffilies() - effectif2.getPrioriteAffilies();
                if (diffPrio != 0) {
                    return diffPrio;
                }
                Integer diffIndicateur = effectif1.getIndicateurAffiliation() - effectif2.getIndicateurAffiliation();
                if (diffIndicateur != 0) {
                    return diffIndicateur;
                }
                return effectif1.getNumCategorieQuittancement().compareTo(effectif2.getNumCategorieQuittancement());
            }
        }).getNumCategorieQuittancement();
    }

    private List<EffectifCategorieContratTemp> creeEffectifsCategorieContratTemp(Set<NumCategorieLibelleCategorie> couplesNumCatLiCatDistincts,
            List<Affiliation> affiliations, List<BaseAssujettie> listeBasesTraitees, String modeReaffectationCategorieEffectif,
            PeriodeRecue periode) {
        List<EffectifCategorieContratTemp> effectifsTemp = new ArrayList<>(couplesNumCatLiCatDistincts.size());
        boolean isNoCatUnique = couplesNumCatLiCatDistincts.size() == 1;
        for (NumCategorieLibelleCategorie coupleNumCatLiCat : couplesNumCatLiCatDistincts) {
            EffectifCategorieContratTemp effectifTemp = creeEffectifCategorieContratTemp(affiliations, coupleNumCatLiCat, listeBasesTraitees,
                    modeReaffectationCategorieEffectif, periode, isNoCatUnique);
            effectifsTemp.add(effectifTemp);
        }
        return effectifsTemp;
    }

    /**
     * Initialise les effectifs de couples "NOCAT/LICAT" pour une plage donnée.
     * 
     * @param couplesNumCatLiCat
     *            les couples "NOCAT/LICAT" de la plage
     * @param idPeriode
     *            l'identifiant de la période de la plage
     * @param contratTravail
     *            le contrat de travail de la plage
     * @param plage
     *            la plage de temps
     * @param moisRattachement
     *            le mois de Rattachement de l'adhesion etablissement mois
     * @return les effectifs initialisés, indexés par numéro de catégorie
     */
    private Map<String, EffectifCategorieContratTravail> initialiseEffectifsCategorieContratTravail(
            Set<NumCategorieLibelleCategorie> couplesNumCatLiCat, Long idPeriode, ContratTravail contratTravail, PlageRattachement plage,
            Integer moisRattachement) {
        Map<String, EffectifCategorieContratTravail> effectifsPlage = new HashMap<>(couplesNumCatLiCat.size());
        for (NumCategorieLibelleCategorie coupleNumCatLiCat : couplesNumCatLiCat) {
            EffectifCategorieContratTravail effectif = new EffectifCategorieContratTravail();
            effectif.setIdPeriode(idPeriode);
            effectif.setMoisRattachement(moisRattachement);
            effectif.setIdContratTravail(contratTravail.getIdContratTravail());
            effectif.setNumCategorieQuittancement(coupleNumCatLiCat.getNumCategorie());
            effectif.setDateDebutBase(plage.getDateDebutPlage());
            effectif.setDateFinBase(plage.getDateFinPlage());
            effectif.setNombreAffilies(0);
            effectif.setAyantsDroitAdulte(0);
            effectif.setAyantsDroitAutre(0);
            effectif.setAyantsDroitEnfant(0);
            effectif.setTotalEffectif(0L);
            effectif.setMontantForfait(0d);
            effectif.setEstimationCotisation(0d);
            effectif.setProrataCotisation(0d);
            effectif.setAuditUtilisateurCreation(this.auditNomBatch);
            effectifsPlage.put(coupleNumCatLiCat.getNumCategorie(), effectif);
        }
        return effectifsPlage;
    }

    /**
     * Résoud le mode de réaffectation pour une période correspondant par priorité :
     * <ol>
     * <li>à l'indicateur DSN,</li>
     * <li>à la valeur d'une extension contrat,</li>
     * <li>à la valeur par défaut.</li>
     * </ol>
     * 
     * @param periode
     *            la période à traiter
     * @return le mode de réaffectation pour cette période
     */
    private String resoudModeReaffectationCategorieEffectif(PeriodeRecue periode) {
        IndicateursDSNPeriode indicateurs = indicateursDSNPeriodeRepository.getIndicateursPourPeriode(periode.getNumeroContrat(),
                periode.getDateDebutPeriode(), periode.getDateFinPeriode());
        if (indicateurs != null) {
            origineModeReaffectation = "INDICATEURS_DSN_PERIODES";
            return indicateurs.getModeReaffectationCategorieEffectif();
        }

        ExtensionContrat extensionContrat = extensionContratRepository.getExtensionContrat(periode.getNumeroContrat());
        if (extensionContrat != null && extensionContrat.getDateEffetIndicateurs() != null
                && extensionContrat.getDateEffetIndicateurs() <= periode.getDateDebutPeriode()) {
            origineModeReaffectation = "EXTENSIONS_CONTRATS";
            return extensionContrat.getModeReaffectationCategorieEffectifs();
        }

        return paramValeurDefautRepository.getModeReaffectationCategorieEffectifsParDefaut();
    }

    /**
     * Recherche parmi une liste d'affiliations connues si l'une d'entre elles est en cours pour un numéro de catégorie donné. ( identification Affiliation pour
     * F04_RG_P5_12 )
     * 
     * @param affiliations
     *            la liste d'affiliations connues
     * @param numCategorie
     *            le numéro de catégorie
     * @param listeBasesTraitees
     *            la liste des bases assujetties traitées
     * @param isNoCatUnique
     *            flag indiquant si on est dans un contexte de noCatUnique dans Tarifs
     * @return l'affiliation en cours si elle existe
     */
    private Affiliation rechercheAffiliationEnCoursPourNumCat(List<Affiliation> affiliations, String numCategorie,
            List<BaseAssujettie> listeBasesTraitees, boolean isNoCatUnique) {
        Affiliation affiliationEnCours = null;

        if (isNoCatUnique) {
            for (Affiliation affiliation : affiliations) {
                if (StringUtils.equals(affiliation.getCodePopulation(), StringUtils.EMPTY)
                        || StringUtils.equals(affiliation.getCodePopulation(), null)
                        || StringUtils.equals(affiliation.getCodePopulation(), numCategorie)) {
                    boolean affiliationTraitee = verifieExistenceBaseTraiteePourAffiliation(affiliation, listeBasesTraitees);
                    if (affiliationTraitee) {
                        affiliationEnCours = affiliation;
                        break;
                    }
                }
            }
        } else {
            for (Affiliation affiliation : affiliations) {
                if (StringUtils.equals(affiliation.getCodePopulation(), numCategorie)) {
                    boolean affiliationTraitee = verifieExistenceBaseTraiteePourAffiliation(affiliation, listeBasesTraitees);
                    if (affiliationTraitee) {
                        affiliationEnCours = affiliation;
                        break;
                    }
                }
            }
        }

        return affiliationEnCours;
    }

    /**
     * Vérifie qu'une base assujettie traitée existe pour une affiliation donnée.
     * 
     * @param affiliation
     *            l'affiliation
     * @param listeBasesTraitees
     *            la liste des bases traitées
     * @return true s'il existe une base traitée relative à l'affiliation, false sinon
     */
    private boolean verifieExistenceBaseTraiteePourAffiliation(Affiliation affiliation, List<BaseAssujettie> listeBasesTraitees) {
        for (BaseAssujettie baseTraitee : listeBasesTraitees) {
            if (StringUtils.equals(baseTraitee.getIdAffiliation(), affiliation.getIdAffiliation())) {
                return true;
            }
        }
        return false;
    }

    /**
     * Crée un effectif temporaire pour un couple NOCAT/LICAT donné.
     * 
     * @param affiliations
     *            la liste des affiliations concernées par le contrat de travail
     * @param coupleNumCatLiCat
     *            le couple ciblé
     * @param listeBasesTraitees
     *            la liste des bases traitées
     * @param modeReaffectationCategorieEffectif
     *            le mode de réaffectation
     * @param isNoCatUnique
     *            flag indiquant si on est dans un contexte de noCatUnique dans Tarifs
     * @return l'effectif temporaire créé
     */
    private EffectifCategorieContratTemp creeEffectifCategorieContratTemp(List<Affiliation> affiliations,
            NumCategorieLibelleCategorie coupleNumCatLiCat, List<BaseAssujettie> listeBasesTraitees, String modeReaffectationCategorieEffectif,
            PeriodeRecue periode, boolean isNoCatUnique) {
        Affiliation affiliationEnCours = rechercheAffiliationEnCoursPourNumCat(affiliations, coupleNumCatLiCat.getNumCategorie(), listeBasesTraitees,
                isNoCatUnique);
        EffectifCategorieContratTemp effectifTemp;
        if (StringUtils.equals(modeReaffectationCategorieEffectif, "DIREC")) {
            effectifTemp = creeEffectifCategorieContratTempDIREC(coupleNumCatLiCat, affiliationEnCours);
        } else if (StringUtils.equals(modeReaffectationCategorieEffectif, "REACA")) {
            ParamCategorieEffectifs paramCategorieEffectif = paramCategorieEffectifsRepository
                    .get(new ParamCategorieEffectifsId(false, coupleNumCatLiCat.getNumCategorie(), coupleNumCatLiCat.getLibelleCategorie()));
            if (paramCategorieEffectif == null) {
                String reconsolider = "N";
                periodeRecueRepository.modifieReconsoliderPeriodeRecue(periode.getIdPeriode(), reconsolider);
                String logErreur = MessageFormat.format(ERREUR_PARAM_CATEGORIE_EFFECTIF, coupleNumCatLiCat, periode.getIdPeriode(),
                        periode.getNumeroContrat(), periode.getTypePeriode(), periode.getDateDebutPeriode().toString(),

                        periode.getDateCreation().toString(), affiliationEnCours.getIdContratTravail());
                throw new RedacUnexpectedException(logErreur);
            }
            effectifTemp = creeEffectifCategorieContratTempREACA(coupleNumCatLiCat, affiliationEnCours, paramCategorieEffectif);
        } else {
            String reconsolider = "N";
            periodeRecueRepository.modifieReconsoliderPeriodeRecue(periode.getIdPeriode(), reconsolider);
            String logErreur = MessageFormat.format(ERREUR_MODE_REAFFECTATION_INCONNU, modeReaffectationCategorieEffectif, periode.getIdPeriode(),
                    periode.getNumeroContrat(), periode.getTypePeriode(), periode.getDateDebutPeriode().toString(),
                    periode.getDateCreation().toString(), origineModeReaffectation);
            throw new RedacUnexpectedException(logErreur);
        }
        return effectifTemp;
    }

    /**
     * Crée un effectif temporaire en mode de réaffectation DIREC.
     * 
     * @param coupleNumCatLiCat
     *            le couple NOCAT / LICAT
     * @param affiliationEnCours
     *            l'affiliation en cours pour ce couple (optionnel)
     * @return l'effectif temporaire créé
     */
    private EffectifCategorieContratTemp creeEffectifCategorieContratTempDIREC(NumCategorieLibelleCategorie coupleNumCatLiCat,
            Affiliation affiliationEnCours) {
        EffectifCategorieContratTemp effectifTemp = new EffectifCategorieContratTemp();
        effectifTemp.setNumCategorieQuittancement(coupleNumCatLiCat.getNumCategorie());
        effectifTemp.setLibelleCategorieQuittancement(coupleNumCatLiCat.getLibelleCategorie());
        effectifTemp.setPrioriteAffilies(0);
        effectifTemp.setPrioriteAyantsDroitAdultes(0);
        effectifTemp.setSeuilAyantsDroitAdultes(0);
        effectifTemp.setPrioriteAyantsDroitAutres(0);
        effectifTemp.setSeuilAyantsDroitAutres(0);
        effectifTemp.setPrioriteAyantsDroitEnfants(0);
        effectifTemp.setSeuilAyantsDroitEnfants(0);
        effectifTemp.setIndicateurAffiliation(affiliationEnCours != null ? 1 : 0);

        Integer nbAyantDroitAdultes = null;
        Integer nbAyantDroitAutres = null;
        Integer nbAyantDroitEnfants = null;

        if (affiliationEnCours != null) {
            if (affiliationEnCours.getNombreAyantDroitAdultes() != null) {
                nbAyantDroitAdultes = affiliationEnCours.getNombreAyantDroitAdultes();
            }
            if (affiliationEnCours.getNombreAyantDroitAutres() != null) {
                nbAyantDroitAutres = affiliationEnCours.getNombreAyantDroitAutres();
            }
            if (affiliationEnCours.getNombreAyantDroitEnfants() != null) {
                nbAyantDroitEnfants = affiliationEnCours.getNombreAyantDroitEnfants();
            }
        }
        effectifTemp.setNombreAyantDroitAdultes(nbAyantDroitAdultes != null ? nbAyantDroitAdultes : 0);
        effectifTemp.setNombreAyantDroitAutres(nbAyantDroitAutres != null ? nbAyantDroitAutres : 0);
        effectifTemp.setNombreAyantDroitEnfants(nbAyantDroitEnfants != null ? nbAyantDroitEnfants : 0);
        return effectifTemp;
    }

    /**
     * Crée un effectif temporaire en mode de réaffectation REACA.
     * 
     * @param coupleNumCatLiCat
     *            le couple NOCAT / LICAT
     * @param affiliationEnCours
     *            l'affiliation en cours pour ce couple (optionnel)
     * @param paramCategorieEffectif
     *            le paramétrage de référence
     * @return l'effectif temporaire créé
     */
    private EffectifCategorieContratTemp creeEffectifCategorieContratTempREACA(NumCategorieLibelleCategorie coupleNumCatLiCat,
            Affiliation affiliationEnCours, ParamCategorieEffectifs paramCategorieEffectif) {
        EffectifCategorieContratTemp effectifTemp = new EffectifCategorieContratTemp();
        effectifTemp.setNumCategorieQuittancement(coupleNumCatLiCat.getNumCategorie());
        effectifTemp.setLibelleCategorieQuittancement(coupleNumCatLiCat.getLibelleCategorie());
        effectifTemp.setPrioriteAffilies(paramCategorieEffectif.getPrioriteAffilies());
        effectifTemp.setPrioriteAyantsDroitAdultes(paramCategorieEffectif.getPrioriteAyantsDroitAdultes());
        effectifTemp.setSeuilAyantsDroitAdultes(paramCategorieEffectif.getSeuilAyantsDroitAdultes());
        effectifTemp.setPrioriteAyantsDroitAutres(paramCategorieEffectif.getPrioriteAyantsDroitAutres());
        effectifTemp.setSeuilAyantsDroitAutres(paramCategorieEffectif.getSeuilAyantsDroitAutres());
        effectifTemp.setPrioriteAyantsDroitEnfants(paramCategorieEffectif.getPrioriteAyantsDroitEnfants());
        effectifTemp.setSeuilAyantsDroitEnfants(paramCategorieEffectif.getSeuilAyantsDroitEnfants());
        effectifTemp.setIndicateurAffiliation(affiliationEnCours != null ? 1 : 0);

        if (affiliationEnCours != null) {
            determineNombresAyantsDroitPourREACA(effectifTemp, affiliationEnCours);
        } else {
            effectifTemp.setNombreAyantDroitAdultes(0);
            effectifTemp.setNombreAyantDroitAutres(0);
            effectifTemp.setNombreAyantDroitEnfants(0);
        }
        return effectifTemp;
    }

    /**
     * Détermine les 3 nombres d'ayants droit pour une entité Effectif temporaire.
     * 
     * @param effectifTemp
     *            l'effectif considéré
     * @param affiliationEnCours
     *            l'affiliation en cours de traitement
     */
    private void determineNombresAyantsDroitPourREACA(EffectifCategorieContratTemp effectifTemp, Affiliation affiliationEnCours) {
        Integer nbAyantDroitAdultes = null;
        Integer nbAyantDroitAutres = null;
        Integer nbAyantDroitEnfants = null;

        if (effectifTemp.getPrioriteAyantsDroitAdultes() != null && effectifTemp.getPrioriteAyantsDroitAdultes() != -1) {
            nbAyantDroitAdultes = affiliationEnCours.getNombreAyantDroitAdultes();
        }
        if (effectifTemp.getPrioriteAyantsDroitAutres() != null && effectifTemp.getPrioriteAyantsDroitAutres() != -1) {
            nbAyantDroitAutres = affiliationEnCours.getNombreAyantDroitAutres();
        }
        if (effectifTemp.getPrioriteAyantsDroitEnfants() != null && effectifTemp.getPrioriteAyantsDroitEnfants() != -1) {
            nbAyantDroitEnfants = affiliationEnCours.getNombreAyantDroitEnfants();
        }
        effectifTemp.setNombreAyantDroitAdultes(nbAyantDroitAdultes != null ? nbAyantDroitAdultes : 0);
        effectifTemp.setNombreAyantDroitAutres(nbAyantDroitAutres != null ? nbAyantDroitAutres : 0);
        effectifTemp.setNombreAyantDroitEnfants(nbAyantDroitEnfants != null ? nbAyantDroitEnfants : 0);
    }

    /**
     * Algorithme de réaffectation des ayants droits adultes.
     * 
     * @param effectifsPlage
     *            les effectifs à mettre à jour
     * @param effectifsTemp
     *            les effectifs temporaires liés
     */
    private void reaffecteAyantsDroitAdultes(Map<String, EffectifCategorieContratTravail> effectifsPlage,
            List<EffectifCategorieContratTemp> effectifsTemp) {
        // Pour chaque ligne de EffectifCategorieContratTemp triées par prioriteAyantsDroitAdultes descendante puis par seuilAyantsDroitAdultes
        // descendant puis par indicateurAffiliation descendant puis par numCategorieQuittancement descendant :
        List<EffectifCategorieContratTemp> effectifsTempTriesPourAyantsDroitAdultes = new ArrayList<>(effectifsTemp);
        Collections.sort(effectifsTempTriesPourAyantsDroitAdultes, new ComparateurTriDescendantPourReaffectationAyantsDroitAdultes());
        for (EffectifCategorieContratTemp effectifTempEnCours : effectifsTempTriesPourAyantsDroitAdultes) {
            // Calculer sommeAyantDroit = EffectifCategorieContratTemp.nombreAyantDroitAdultes + somme des
            // EffectifCategorieContratTemp.nombreAyantDroitAdultes dont prioriteAyantsDroitAdultes est strictement inférieure à celle de la ligne en
            // cours
            Integer sommeAyantDroit = calculeSommeAyantDroitAdultesAvecPrioriteInferieure(effectifTempEnCours,
                    effectifsTempTriesPourAyantsDroitAdultes);
            EffectifCategorieContratTravail effectifContratTravail = effectifsPlage.get(effectifTempEnCours.getNumCategorieQuittancement());
            // Si sommeAyantDroit > EffectifCategorieContratTemp.seuilAyantsDroitAdultes
            if (sommeAyantDroit > effectifTempEnCours.getSeuilAyantsDroitAdultes()) {
                // Alimenter EffectifCategorieContratTravail.ayantsDroitAdulte avec sommeAyantDroit -
                // EffectifCategorieContratTemp.seuilAyantsDroitAdultes
                effectifContratTravail.setAyantsDroitAdulte(sommeAyantDroit - effectifTempEnCours.getSeuilAyantsDroitAdultes());
                // Déduire cette valeur de EffectifCategorieContratTemp.nombreAyantDroitAdultes
                effectifTempEnCours
                        .setNombreAyantDroitAdultes(effectifTempEnCours.getNombreAyantDroitAdultes() - effectifContratTravail.getAyantsDroitAdulte());
            } else {
                // Sinon
                // Alimenter EffectifCategorieContratTravail.ayantsDroitAdulte à 0
                effectifContratTravail.setAyantsDroitAdulte(0);
            }
            // Alimenter EffectifCategorieContratTemp.prioriteAyantsDroitAdultes à -1
            effectifTempEnCours.setPrioriteAyantsDroitAdultes(-1);
        }
        // A la fin de la réaffectation des ayants droit adultes, la somme des EffectifCategorieContratTemp.nombreAyantDroitAdultes doit être égale à 0.
        if (LOGGER.isDebugEnabled()) {
            Integer sommeTotale = 0;
            for (EffectifCategorieContratTemp effectifTemp : effectifsTempTriesPourAyantsDroitAdultes) {
                LOGGER.debug("Calcul somme totale des EffectifCategorieContratTemp.nombreAyantDroitAdultes : += {}...",
                        effectifTemp.getNombreAyantDroitAdultes());
                sommeTotale += effectifTemp.getNombreAyantDroitAdultes();
            }
            LOGGER.debug("Somme totale des EffectifCategorieContratTemp.nombreAyantDroitAdultes (devant être égale à 0) : {}", sommeTotale);
        }
    }

    /**
     * Calcule la somme des ayants droit adultes pour un effectif temporaire donné, en incluant tous les effectifs temporaires d'une liste donnée ayant une
     * priorité strictement inférieure.
     * 
     * @param effectifTemp
     *            l'effectif temporaire
     * @param effectifsTempTriesPourAyantsDroitAdultes
     *            la liste de tous les effectifs temporaires à parcourir
     * @return la somme des ayants droit enfants pour l'effectif temporaire
     */
    private Integer calculeSommeAyantDroitAdultesAvecPrioriteInferieure(EffectifCategorieContratTemp effectifTemp,
            List<EffectifCategorieContratTemp> effectifsTempTriesPourAyantsDroitAdultes) {
        Integer sommeAyantDroit = effectifTemp.getNombreAyantDroitAdultes();
        for (EffectifCategorieContratTemp effectifTempAutre : effectifsTempTriesPourAyantsDroitAdultes) {
            if (effectifTemp.getPrioriteAyantsDroitAdultes() > effectifTempAutre.getPrioriteAyantsDroitAdultes()) {
                sommeAyantDroit += effectifTempAutre.getNombreAyantDroitAdultes();
            }
        }
        return sommeAyantDroit;
    }

    /**
     * Algorithme de réaffectation des ayants droits autres.
     * 
     * @param effectifsPlage
     *            les effectifs à mettre à jour
     * @param effectifsTemp
     *            les effectifs temporaires liés
     */
    private void reaffecteAyantsDroitAutres(Map<String, EffectifCategorieContratTravail> effectifsPlage,
            List<EffectifCategorieContratTemp> effectifsTemp) {
        // Pour chaque ligne de EffectifCategorieContratTemp triées par prioriteAyantsDroitAutres descendante puis par seuilAyantsDroitAutres
        // descendant puis par indicateurAffiliation descendant puis par numCategorieQuittancement descendant :
        List<EffectifCategorieContratTemp> effectifsTempTriesPourAyantsDroitAutres = new ArrayList<>(effectifsTemp);
        Collections.sort(effectifsTempTriesPourAyantsDroitAutres, new ComparateurTriDescendantPourReaffectationAyantsDroitAutres());
        for (EffectifCategorieContratTemp effectifTempEnCours : effectifsTempTriesPourAyantsDroitAutres) {
            // Calculer sommeAyantDroit = EffectifCategorieContratTemp.nombreAyantDroitAutres + somme des
            // EffectifCategorieContratTemp.nombreAyantDroitAutres dont prioriteAyantsDroitAutres est strictement inférieure à celle de la ligne en
            // cours
            Integer sommeAyantDroit = calculeSommeAyantDroitAutresAvecPrioriteInferieure(effectifTempEnCours,
                    effectifsTempTriesPourAyantsDroitAutres);
            EffectifCategorieContratTravail effectifContratTravail = effectifsPlage.get(effectifTempEnCours.getNumCategorieQuittancement());
            // Si sommeAyantDroit > EffectifCategorieContratTemp.seuilAyantsDroitAutres
            if (sommeAyantDroit > effectifTempEnCours.getSeuilAyantsDroitAutres()) {
                // Alimenter EffectifCategorieContratTravail.ayantsDroitAutre avec sommeAyantDroit -
                // EffectifCategorieContratTemp.seuilAyantsDroitAutres
                effectifContratTravail.setAyantsDroitAutre(sommeAyantDroit - effectifTempEnCours.getSeuilAyantsDroitAutres());
                // Déduire cette valeur de EffectifCategorieContratTemp.nombreAyantDroitAutres
                effectifTempEnCours
                        .setNombreAyantDroitAutres(effectifTempEnCours.getNombreAyantDroitAutres() - effectifContratTravail.getAyantsDroitAutre());
            } else {
                // Sinon
                // Alimenter EffectifCategorieContratTravail.ayantsDroitAutre à 0
                effectifContratTravail.setAyantsDroitAutre(0);
            }
            // Alimenter EffectifCategorieContratTemp.prioriteAyantsDroitAutres à -1
            effectifTempEnCours.setPrioriteAyantsDroitAutres(-1);
        }
        // A la fin de la réaffectation des ayants droit autres, la somme des EffectifCategorieContratTemp.nombreAyantDroitAutres doit être égale à 0.
        if (LOGGER.isDebugEnabled()) {
            Integer sommeTotale = 0;
            for (EffectifCategorieContratTemp effectifTemp : effectifsTempTriesPourAyantsDroitAutres) {
                LOGGER.debug("Calcul somme totale des EffectifCategorieContratTemp.nombreAyantDroitAutres : += {}...",
                        effectifTemp.getNombreAyantDroitAutres());
                sommeTotale += effectifTemp.getNombreAyantDroitAutres();
            }
            LOGGER.debug("Somme totale des EffectifCategorieContratTemp.nombreAyantDroitAutres (devant être égale à 0) : {}", sommeTotale);
        }
    }

    /**
     * Calcule la somme des ayants droit autres pour un effectif temporaire donné, en incluant tous les effectifs temporaires d'une liste donnée ayant une
     * priorité strictement inférieure.
     * 
     * @param effectifTemp
     *            l'effectif temporaire
     * @param effectifsTempTriesPourAyantsDroitAutres
     *            la liste de tous les effectifs temporaires à parcourir
     * @return la somme des ayants droit enfants pour l'effectif temporaire
     */
    private Integer calculeSommeAyantDroitAutresAvecPrioriteInferieure(EffectifCategorieContratTemp effectifTemp,
            List<EffectifCategorieContratTemp> effectifsTempTriesPourAyantsDroitAutres) {
        Integer sommeAyantDroit = effectifTemp.getNombreAyantDroitAutres();
        for (EffectifCategorieContratTemp effectifTempAutre : effectifsTempTriesPourAyantsDroitAutres) {
            if (effectifTemp.getPrioriteAyantsDroitAutres() > effectifTempAutre.getPrioriteAyantsDroitAutres()) {
                sommeAyantDroit += effectifTempAutre.getNombreAyantDroitAutres();
            }
        }
        return sommeAyantDroit;
    }

    /**
     * Algorithme de réaffectation des ayants droits enfants.
     * 
     * @param effectifsPlage
     *            les effectifs à mettre à jour
     * @param effectifsTemp
     *            les effectifs temporaires liés
     */
    private void reaffecteAyantsDroitEnfants(Map<String, EffectifCategorieContratTravail> effectifsPlage,
            List<EffectifCategorieContratTemp> effectifsTemp) {
        // Pour chaque ligne de EffectifCategorieContratTemp triées par prioriteAyantsDroitEnfants descendante puis par seuilAyantsDroitEnfants
        // descendant puis par indicateurAffiliation descendant puis par numCategorieQuittancement descendant :
        List<EffectifCategorieContratTemp> effectifsTempTriesPourAyantsDroitEnfants = new ArrayList<>(effectifsTemp);
        Collections.sort(effectifsTempTriesPourAyantsDroitEnfants, new ComparateurTriDescendantPourReaffectationAyantsDroitEnfants());
        for (EffectifCategorieContratTemp effectifTempEnCours : effectifsTempTriesPourAyantsDroitEnfants) {
            // Calculer sommeAyantDroit = EffectifCategorieContratTemp.nombreAyantDroitEnfants + somme des
            // EffectifCategorieContratTemp.nombreAyantDroitEnfants dont prioriteAyantsDroitEnfants est strictement inférieure à celle de la ligne en
            // cours
            Integer sommeAyantDroit = calculeSommeAyantDroitEnfantsAvecPrioriteInferieure(effectifTempEnCours,
                    effectifsTempTriesPourAyantsDroitEnfants);
            EffectifCategorieContratTravail effectifContratTravail = effectifsPlage.get(effectifTempEnCours.getNumCategorieQuittancement());
            // Si sommeAyantDroit > EffectifCategorieContratTemp.seuilAyantsDroitEnfants
            if (sommeAyantDroit > effectifTempEnCours.getSeuilAyantsDroitEnfants()) {
                // Alimenter EffectifCategorieContratTravail.ayantsDroitEnfant avec sommeAyantDroit -
                // EffectifCategorieContratTemp.seuilAyantsDroitEnfants
                effectifContratTravail.setAyantsDroitEnfant(sommeAyantDroit - effectifTempEnCours.getSeuilAyantsDroitEnfants());
                // Déduire cette valeur de EffectifCategorieContratTemp.nombreAyantDroitEnfants
                effectifTempEnCours
                        .setNombreAyantDroitEnfants(effectifTempEnCours.getNombreAyantDroitEnfants() - effectifContratTravail.getAyantsDroitEnfant());
            } else {
                // Sinon
                // Alimenter EffectifCategorieContratTravail.ayantsDroitEnfant à 0
                effectifContratTravail.setAyantsDroitEnfant(0);
            }
            // Alimenter EffectifCategorieContratTemp.prioriteAyantsDroitEnfants à -1
            effectifTempEnCours.setPrioriteAyantsDroitEnfants(-1);
        }
        // A la fin de la réaffectation des ayants droit enfants, la somme des EffectifCategorieContratTemp.nombreAyantDroitEnfants doit être égale à 0.
        if (LOGGER.isDebugEnabled()) {
            Integer sommeTotale = 0;
            for (EffectifCategorieContratTemp effectifTemp : effectifsTempTriesPourAyantsDroitEnfants) {
                LOGGER.debug("Calcul somme totale des EffectifCategorieContratTemp.nombreAyantDroitEnfants : += {}...",
                        effectifTemp.getNombreAyantDroitEnfants());
                sommeTotale += effectifTemp.getNombreAyantDroitEnfants();
            }
            LOGGER.debug("Somme totale des EffectifCategorieContratTemp.nombreAyantDroitEnfants (devant être égale à 0) : {}", sommeTotale);
        }
    }

    /**
     * Calcule la somme des ayants droit enfants pour un effectif temporaire donné, en incluant tous les effectifs temporaires d'une liste donnée ayant une
     * priorité strictement inférieure.
     * 
     * @param effectifTemp
     *            l'effectif temporaire
     * @param effectifsTempTriesPourAyantsDroitEnfants
     *            la liste de tous les effectifs temporaires à parcourir
     * @return la somme des ayants droit enfants pour l'effectif temporaire
     */
    private Integer calculeSommeAyantDroitEnfantsAvecPrioriteInferieure(EffectifCategorieContratTemp effectifTemp,
            List<EffectifCategorieContratTemp> effectifsTempTriesPourAyantsDroitEnfants) {
        Integer sommeAyantDroit = effectifTemp.getNombreAyantDroitEnfants();
        for (EffectifCategorieContratTemp effectifTempAutre : effectifsTempTriesPourAyantsDroitEnfants) {
            if (effectifTemp.getPrioriteAyantsDroitEnfants() > effectifTempAutre.getPrioriteAyantsDroitEnfants()) {
                sommeAyantDroit += effectifTempAutre.getNombreAyantDroitEnfants();
            }
        }
        return sommeAyantDroit;
    }

    /**
     * Calcule tout ce qui est dans le nom de la méthode pour des effectifs sur une plage donnée...
     * 
     * @param effectifsPlage
     *            les effectifs concerné pour le recalcul
     * @param periode
     *            la période pour ces effectifs
     * @param moisRattachement
     *            le mois de Rattachement de l'adhesion etablissement mois
     * @param moisDeclare
     *            le mois de déclaration de l'adhesion etablissement mois
     * @param tauxAppel
     *            le taux d'appel de la situation contrat valide
     */
    private void calculeTotalEffectifEtMontantForfaitEtEstimationCotisation(Collection<EffectifCategorieContratTravail> effectifsPlage,
            PeriodeRecue periode, Integer moisRattachement, Integer moisDeclare, Double tauxAppel) {
        Double plafondMensuelSecuriteSociale = paramPlafondTauxRepository.getPlafondMensuelSecuriteSociale(periode.getDateDebutPeriode());
        if (plafondMensuelSecuriteSociale == null) {
            String reconsolider = "N";
            periodeRecueRepository.modifieReconsoliderPeriodeRecue(periode.getIdPeriode(), reconsolider);
            String logErreur = MessageFormat.format(ERREUR_PLAFOND_TAUX, periode.getDateDebutPeriode().toString(), periode.getIdPeriode(),
                    periode.getNumeroContrat(), periode.getTypePeriode(), periode.getDateDebutPeriode().toString(),
                    periode.getDateCreation().toString());
            throw new RedacUnexpectedException(logErreur);
        }
        for (EffectifCategorieContratTravail effectif : effectifsPlage) {
            // Calcul total effectif

            // Pour les périodes de type régulation
            if (PeriodeRecue.TYPE_PERIODE_REGULARISATION.equals(periode.getTypePeriode())) {
                effectif.setTotalEffectif(Long.valueOf(effectif.getNombreAffilies()) + Long.valueOf(effectif.getAyantsDroitAdulte())
                        + Long.valueOf(effectif.getAyantsDroitAutre()) + Long.valueOf(effectif.getAyantsDroitEnfant()));
            }

            // Pour les périodes de type Déclaration ou Complément
            if (PeriodeRecue.TYPE_PERIODE_DECLARATION.equals(periode.getTypePeriode())
                    || PeriodeRecue.TYPE_PERIODE_COMPLEMENT.equals(periode.getTypePeriode())) {
                if (moisDeclare.intValue() == moisRattachement.intValue()) {
                    effectif.setTotalEffectif(Long.valueOf(effectif.getNombreAffilies()) + Long.valueOf(effectif.getAyantsDroitAdulte())
                            + Long.valueOf(effectif.getAyantsDroitAutre()) + Long.valueOf(effectif.getAyantsDroitEnfant()));
                } else {
                    effectif.setTotalEffectif(0L);
                }
            }

            Tarif tarif = tarifRepository.getPourNumContratEtNumCategorieEtDateEffet(periode.getNumeroContrat(),
                    effectif.getNumCategorieQuittancement(), periode.getDateDebutPeriode());

            if (tarif == null) {
                String reconsolider = "N";
                periodeRecueRepository.modifieReconsoliderPeriodeRecue(periode.getIdPeriode(), reconsolider);
                String logErreur = MessageFormat.format(ERREUR_TARIF, periode.getNumeroContrat(), effectif.getNumCategorieQuittancement(),
                        periode.getDateDebutPeriode().toString(), periode.getIdPeriode(), periode.getNumeroContrat(), periode.getTypePeriode(),
                        periode.getDateDebutPeriode().toString(), periode.getDateCreation().toString());
                throw new RedacUnexpectedException(logErreur);
            }
            Long tauxCalculCotisationsUniques = tarif.getTauxCalculCotisationsUniques();

            // Si tauxCalculCotisationsUniques renseigné (non null et > 0)
            if (tauxCalculCotisationsUniques != null && tauxCalculCotisationsUniques > 0) {
                // tauxCalculCotisationsUniques = tauxCalculCotisationsUniques / 1000000 (car le taux est en 10000èmes de %)
                // montantForfait = tauxCalculCotisationsUniques * plafondMensuelSecuriteSociale
                Double tauxCalculCotisationsUniquesAsDouble = Double.valueOf(tauxCalculCotisationsUniques) / 1000000d;
                effectif.setMontantForfait(tauxCalculCotisationsUniquesAsDouble * plafondMensuelSecuriteSociale);
            } else {
                // Sinon montantForfait = montantCotisationsUniques / 1200 (car ce montant est en centimes pour 1 an)
                long montantCotisationsUniques = tarif.getMontantCotisationsUniques() != null ? tarif.getMontantCotisationsUniques() : 0L;
                effectif.setMontantForfait(Double.valueOf(montantCotisationsUniques) / 1200d);
            }

            // estimationCotisation = totalEffectif * montantForfait * (tauxAppel / 10000 ) car taux en 100ème de % )
            effectif.setEstimationCotisation((Long.valueOf(effectif.getNombreAffilies()) + Long.valueOf(effectif.getAyantsDroitAdulte())
                    + Long.valueOf(effectif.getAyantsDroitAutre()) + Long.valueOf(effectif.getAyantsDroitEnfant())) * effectif.getMontantForfait()
                    * (tauxAppel.doubleValue() / 10000d));
        }
    }

    /**
     * Calcule les prorata de cotisations pour des effectifs sur une plage donnée.
     * 
     * @param effectifsPlage
     *            les effectifs concerné pour le recalcul
     * @param listeBasesTraitees
     *            la liste des bases traitées pour la plage
     */
    private void calculeProrataCotisations(Collection<EffectifCategorieContratTravail> effectifsPlage, List<BaseAssujettie> listeBasesTraitees) {
        // Calculer totalCotisationContratPlage = somme des montantCotisation de listeBasesTraitees
        Double totalCotisationContratPlage = 0d;
        for (BaseAssujettie baseTraitee : listeBasesTraitees) {
            totalCotisationContratPlage += baseTraitee.getMontantCotisation();
        }
        // Calculer totalEstimationCotisationPlage = somme des estimationCotisation de l’entité EffectifCategorieContratTravail pour le ContratTravail
        // et la plage en cours
        Double totalEstimationCotisationPlage = 0d;
        for (EffectifCategorieContratTravail effectif : effectifsPlage) {
            totalEstimationCotisationPlage += effectif.getEstimationCotisation();
        }
        if (totalEstimationCotisationPlage.compareTo(Double.valueOf(0d)) != 0) {
            for (EffectifCategorieContratTravail effectif : effectifsPlage) {
                // Pour chaque ligne de l’entité EffectifCategorieContratTravail pour le ContratTravail et la plage en cours, alimenter :
                // EffectifCategorieContratTravail.prorataCotisation = EffectifCategorieContratTravail.estimationCotisation * totalCotisationContratPlage /
                // totalEstimationCotisationPlage
                effectif.setProrataCotisation(effectif.getEstimationCotisation() * totalCotisationContratPlage / totalEstimationCotisationPlage);
            }
        }
        // A la fin de ce traitement la somme des EffectifCategorieContratTravail.prorataCotisation doit être égale à totalCotisationContratPlage
        if (LOGGER.isDebugEnabled()) {
            Double totalProrataCotisation = 0d;
            for (EffectifCategorieContratTravail effectif : effectifsPlage) {
                totalProrataCotisation += effectif.getProrataCotisation();
            }
            LOGGER.debug("La somme des EffectifCategorieContratTravail.prorataCotisation vaut {} et devrait être égale à {}", totalProrataCotisation,
                    totalCotisationContratPlage);
        }
    }
}
