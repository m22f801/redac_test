package fr.si2m.red.batch.flux3.ligne;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * couple noco/nocat pour une situation tarifaire
 * 
 * @author eudesr
 *
 */

@Data
@EqualsAndHashCode(callSuper = false, of = { "numContrat", "numCategorie" })
@ToString(callSuper = false, of = { "numContrat", "numCategorie" })
public class SituationTarifNoco {

    private String numContrat;

    private String numCategorie;

}
