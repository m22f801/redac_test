package fr.si2m.red.batch.moteur.trace;

import java.util.List;
import java.util.concurrent.TimeUnit;

import lombok.Setter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.ChunkListener;
import org.springframework.batch.core.annotation.AfterChunk;
import org.springframework.batch.core.annotation.AfterProcess;
import org.springframework.batch.core.annotation.AfterRead;
import org.springframework.batch.core.annotation.AfterWrite;
import org.springframework.batch.core.annotation.BeforeChunk;
import org.springframework.batch.core.annotation.BeforeProcess;
import org.springframework.batch.core.annotation.BeforeRead;
import org.springframework.batch.core.annotation.BeforeWrite;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.scope.context.StepContext;

/**
 * Traceur des performances des morceaux d'étapes de jobs batchs. Ne fonctionne que pour les morceaux exécutés de façon séquentielle (synchrone).
 * 
 * @author nortaina
 *
 */
public class MorceauSynchronePerformanceTraceur implements ChunkListener {
    private static final Logger LOGGER = LoggerFactory.getLogger(MorceauSynchronePerformanceTraceur.class);

    /**
     * Activation / désactivation des traces de performance pour le traitement d'un morceau d'une étape.
     * 
     * @param traceTraitementMorceau
     *            true pour activer les traces de performance pour les traitements de morceaux
     */
    @Setter
    private boolean traceTraitementMorceau;
    /**
     * Activation / désactivation des traces de performance pour la lecture d'une ligne.
     * 
     * @param traceLectureLigne
     *            true pour activer les traces de performance pour les lectures de lignes
     */
    @Setter
    private boolean traceLectureLigne;
    /**
     * Activation / désactivation des traces de performance pour le process et la validation d'une ligne.
     * 
     * @param traceProcessEtValidationLigne
     *            true pour activer les traces de performance pour les process et validations de lignes
     */
    @Setter
    private boolean traceProcessEtValidationLigne;
    /**
     * Activation / désactivation des traces de performance pour l'écriture de plusieurs lignes.
     * 
     * @param traceEcritureLignes
     *            true pour activer les traces de performance pour les écritures de lignes
     */
    @Setter
    private boolean traceEcritureLignes;

    private int numeroMorceauSeq = 1;

    private int numeroMorceau;
    private String nomEtape;
    private String nomJob;

    private long debutMorceau;
    private long debutLecture;
    private long debutProcessEtValidation;
    private long debutEcriture;

    /**
     * Gestion des traces de performance en début de morceau d'étape.
     * 
     * @param chunkContext
     *            le contexte du morceau
     */
    @BeforeChunk
    @Override
    public void beforeChunk(ChunkContext chunkContext) {
        StepContext stepContext = chunkContext.getStepContext();
        this.nomEtape = stepContext.getStepName();
        this.nomJob = stepContext.getJobName();
        this.numeroMorceau = numeroMorceauSeq++;

        if (!traceTraitementMorceau) {
            return;
        }
        this.debutMorceau = System.nanoTime();
        LOGGER.info("Début du morceau {} de l'étape {} pour le job {}", numeroMorceau, nomEtape, nomJob);
    }

    /**
     * Gestion des traces de performance en fin de morceau d'étape.
     * 
     * @param chunkContext
     *            le contexte du morceau
     */
    @AfterChunk
    @Override
    public void afterChunk(ChunkContext chunkContext) {
        if (!traceTraitementMorceau) {
            return;
        }
        LOGGER.info("Morceau {} de l'étape {} réalisé en {} ms pour le job {}", numeroMorceau, nomEtape,
                TimeUnit.NANOSECONDS.toMillis(System.nanoTime() - debutMorceau), nomJob);
    }

    /**
     * Aucun traitement défini.
     * 
     * @param context
     *            le contexte du morceau
     */
    @Override
    public void afterChunkError(ChunkContext context) {
        // Rien
    }

    /**
     * Gestion des traces de performance en début de lecture d'une ligne.
     */
    @BeforeRead
    public void avantLecture() {
        if (!traceLectureLigne) {
            return;
        }
        LOGGER.info("Début lecture pour le morceau {} de l'étape {} du job {}", numeroMorceau, nomEtape, nomJob);
        this.debutLecture = System.nanoTime();
    }

    /**
     * Gestion des traces de performance en fin de lecture d'une ligne.
     */
    @AfterRead
    public void apresLecture() {
        if (!traceLectureLigne) {
            return;
        }
        LOGGER.info("Lecture de fichier réalisée en {} ms pour le morceau {} de l'étape {} du job {}",
                TimeUnit.NANOSECONDS.toMillis(System.nanoTime() - debutLecture), numeroMorceau, nomEtape, nomJob);
    }

    /**
     * Gestion des traces de performance en début de process et validation d'une ligne.
     * 
     * @param element
     *            la ligne traité
     */
    @BeforeProcess
    public void avantProcessEtValidation(Object element) {
        if (!traceProcessEtValidationLigne) {
            return;
        }
        LOGGER.info("Début process et validation de {} pour le morceau {} de l'étape {} du job {}", String.valueOf(element), numeroMorceau, nomEtape,
                nomJob);
        this.debutProcessEtValidation = System.nanoTime();
    }

    /**
     * Gestion des traces de performance en fin de process et validation d'une ligne.
     * 
     * @param avant
     *            la ligne traité avant process
     * @param apres
     *            la ligne traité après process
     */
    @AfterProcess
    public void apresProcessEtValidation(Object avant, Object apres) {
        if (!traceProcessEtValidationLigne) {
            return;
        }
        LOGGER.info("Process et validation de {}/{} réalisés en {} ms pour le morceau {} de l'étape {} du job {}", String.valueOf(avant),
                String.valueOf(apres), TimeUnit.NANOSECONDS.toMillis(System.nanoTime() - debutProcessEtValidation), numeroMorceau, nomEtape, nomJob);
    }

    /**
     * Gestion des traces de performance en début d'écriture de lignes.
     * 
     * @param elements
     *            les éléments écrits
     */
    @BeforeWrite
    public void avantEcriture(List<Object> elements) {
        if (!traceEcritureLignes) {
            return;
        }
        LOGGER.info("Début écriture de {} éléments pour le morceau {} de l'étape {} du job {}", elements.size(), numeroMorceau, nomEtape, nomJob);
        this.debutEcriture = System.nanoTime();
    }

    /**
     * Gestion des traces de performance en fin d'écriture de lignes.
     * 
     * @param elements
     *            les éléments écrits
     */
    @AfterWrite
    public void apresEcriture(List<Object> elements) {
        if (!traceEcritureLignes) {
            return;
        }
        LOGGER.info("Ecriture de {} éléments réalisée en {} ms pour le morceau {} de l'étape {} du job {}", elements.size(),
                TimeUnit.NANOSECONDS.toMillis(System.nanoTime() - debutEcriture), numeroMorceau, nomEtape, nomJob);
    }

}
