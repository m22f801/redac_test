package fr.si2m.red.batch.flux4.execution;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import fr.si2m.red.EntiteImportableBatch;

/**
 * Modèle des categories de quittancement des individus.
 * 
 * @author poidij
 *
 */
@Entity
@Table(name = "TMP_RR400_HORODATAGE_TRAITEMENTS")
@Data
@EqualsAndHashCode(callSuper = false, of = { "horodatage" })
@ToString(callSuper = false, of = { "horodatage" })
public class HorodatageTraitement extends EntiteImportableBatch {

    /**
     * UID de version.
     */
    private static final long serialVersionUID = -6297715664444506671L;

    /**
     * Indicateur de ligne temporaire en cours d'import via Batch.
     * 
     * @param ligneEnCoursImportBatch
     *            le nom du SI d'où provient l'information
     * @return le nom du SI d'où provient l'information
     */
    @Id
    @Column(name = "TMP_BATCH")
    private boolean ligneEnCoursImportBatch;

    /**
     * Horodatage du traitement.
     * 
     * @param horodatage
     *            L'horodatage du traitement
     * @return L'horodatage du traitement
     */
    @Id
    @Column(name = "HORODATAGE_TRAITEMENT")
    private String horodatage;

    @Override
    public String getId() {
        return horodatage;
    }

}
