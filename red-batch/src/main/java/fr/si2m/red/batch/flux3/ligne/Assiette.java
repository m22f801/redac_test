package fr.si2m.red.batch.flux3.ligne;

import lombok.Data;
import fr.si2m.red.DateRedac;

/**
 * Description d'une ligne d'un fichier ASSIETTES à exporter.
 * 
 * @author nortaina
 *
 */
@Data
public class Assiette {
    /**
     * Code Mise à jour.
     * 
     * @param codeMiseAJour
     *            Code Mise à jour
     * @return Code Mise à jour
     */
    private String codeMiseAJour;

    /**
     * Identifiant de l’entreprise G3C.
     * 
     * @param identifiantEntrepriseG3C
     *            Identifiant de l’entreprise G3C
     * @return Identifiant de l’entreprise G3C
     */
    private String identifiantEntrepriseG3C;

    /**
     * N° de séquence contrat G3C.
     * 
     * @param numSequenceContratG3C
     *            N° de séquence contrat G3C
     * @return N° de séquence contrat G3C
     */
    private String numSequenceContratG3C;

    /**
     * Code population G3C.
     * 
     * @param codePopulationG3C
     *            Code population G3C
     * @return Code population G3C
     */
    private String codePopulationG3C;

    /**
     * Code option G3C.
     * 
     * @param codeOptionG3C
     *            Code option G3C
     * @return Code option G3C
     */
    private String codeOptionG3C;

    /**
     * Type Assiette G3C.
     * 
     * @param typeAssietteG3C
     *            Type Assiette G3C
     * @return Type Assiette G3C
     */
    private String typeAssietteG3C;

    /**
     * Taux base standard ou spécifique.
     * 
     * @param tauxBaseStandardOuSpecifique
     *            Taux base standard ou spécifique
     * @return Taux base standard ou spécifique
     */
    private Long tauxBaseStandardOuSpecifique;

    /**
     * Montant base spécifique.
     * 
     * @param montantBaseSpecifique
     *            Montant base spécifique
     * @return Montant base spécifique
     */
    private Long montantBaseSpecifique;

    /**
     * Date de début d’application.
     * 
     * @param dateDebutApplication
     *            Date de début d’application
     * @return Date de début d’application
     */
    private Integer dateDebutApplication;

    /**
     * Date de fin d’application.
     * 
     * @param dateFinApplication
     *            Date de fin d’application
     * @return Date de fin d’application
     */
    private Integer dateFinApplication;

    /**
     * Type de base.
     * 
     * @param typeBase
     *            Type de base
     * @return Type de base
     */
    private Integer typeBase;

    /**
     * Libellé base spécifique.
     * 
     * @param libelleBaseSpecifique
     *            Libellé base spécifique
     * @return Libellé base spécifique
     */
    private String libelleBaseSpecifique;

    /**
     * Référence à l’identifiant technique contrat.
     * 
     * @param identifiantTechniqueContrat
     *            Référence à l’identifiant technique contrat
     * @return Référence à l’identifiant technique contrat
     */
    private String identifiantTechniqueContrat;

    /**
     * Référence à l’identifiant technique population.
     * 
     * @param identifiantTechniquePopulation
     *            Référence à l’identifiant technique population
     * @return Référence à l’identifiant technique population
     */
    private String identifiantTechniquePopulation;

    /**
     * Référence à l’identifiant technique Option.
     * 
     * @param identifiantTechniqueOption
     *            Référence à l’identifiant technique Option
     * @return Référence à l’identifiant technique Option
     */
    private String identifiantTechniqueOption;

    /**
     * Date de début d’application.
     * 
     * @return Date de début d’application
     */
    public String getDateDebutApplicationFormatee() {
        return DateRedac.formate(DateRedac.EXPORT_FORMAT_PAR_DEFAUT, dateDebutApplication);
    }

    /**
     * Date de fin d’application formatée pour export.
     * 
     * @return Date de fin d’application formatée pour export
     */
    public String getDateFinApplicationFormatee() {
        return DateRedac.formate(DateRedac.EXPORT_FORMAT_PAR_DEFAUT, dateFinApplication);
    }

}
