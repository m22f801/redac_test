package fr.si2m.red.batch.flux78.execution;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParameter;
import org.springframework.batch.core.JobParameters;
import org.springframework.context.support.GenericXmlApplicationContext;

import fr.si2m.red.DateRedac;
import fr.si2m.red.batch.moteur.CodeRetour;
import fr.si2m.red.batch.moteur.execution.ExecuteurBatch;
import fr.si2m.red.batch.moteur.execution.FacadeExecuteurBatch;

/**
 * Exécuteur du batch du flux 78.
 * 
 * @author poidij
 *
 */
@FacadeExecuteurBatch(nomBatch = "flux78_Publication")
public class Flux78PublicationExecuteurBatch extends ExecuteurBatch {
    /**
     * Ordre du paramètre de lancement "répertoire de sortie des fichiers".
     */
    public static final int ORDRE_PARAM_REPERTOIRE_SORTIE = 0;

    /**
     * Ordre du paramètre de lancement "date du traitement".
     */
    public static final int ORDRE_PARAM_DATE_TRAITEMENT = 1;

    /**
     * Ordre du paramètre de lancement "Si Aval".
     */
    public static final int ORDRE_PARAM_SI_AVAL = 2;

    /**
     * Ordre du paramètre de lancement "timestamp de l'export".
     */
    public static final int ORDRE_PARAM_TIMESTAMP_EXPORT = 3;

    /**
     * Ordre du paramètre de lancement "emplacement du fichier de log d'erreurs".
     */
    public static final int ORDRE_PARAM_EMPLACEMENT_FICHIER_ERREURS = 4;

    private static final Logger LOGGER = LoggerFactory.getLogger(Flux78PublicationExecuteurBatch.class);

    private final String emplacementRessourceRepertoireSortie;
    private final String siAval;
    private final String timestampExport;
    private final String dateDuJour;

    /**
     * Exécuteur de batch du flux 1 prenant en compte les fichiers contrats, tarifs, Clients et intermédiaires centralisés dans un répertoire donnés.
     * 
     * @param parametrageBatch
     *            les paramètres d'exécution du batch - deux paramètres sont attendus en entrée :
     *            <ol>
     *            <li>le chemin complet du répertoire où sont regroupés les fichiers à réceptionner pour le flux 1</li>
     *            <li>l'identifiant partagé par les fichiers (exemple : D150722A pour CONTRAT.D150722A.txt)</li>
     *            </ol>
     */
    public Flux78PublicationExecuteurBatch(String[] parametrageBatch) {
        super(parametrageBatch);

        String repertoireFichiers = parametrageBatch[ORDRE_PARAM_REPERTOIRE_SORTIE];
        this.emplacementRessourceRepertoireSortie = tranformeEmplacementRessourceFichier(repertoireFichiers);

        this.dateDuJour = parametrageBatch[ORDRE_PARAM_DATE_TRAITEMENT];
        this.siAval = parametrageBatch[ORDRE_PARAM_SI_AVAL];

        this.timestampExport = parametrageBatch[ORDRE_PARAM_TIMESTAMP_EXPORT];

        String cheminAbsoluFichierLogErreurs = parametrageBatch[ORDRE_PARAM_EMPLACEMENT_FICHIER_ERREURS];
        setCheminAbsoluFichierLogErreurs(cheminAbsoluFichierLogErreurs);
    }

    @Override
    protected String getEmplacementFichierConfigurationJob() {
        return "classpath:fr/si2m/red/batch/flux78/job_Publication.xml";
    }

    @Override
    protected String getIdJobCible() {
        return "jobFlux78_Publication";
    }

    @Override
    protected boolean lanceTraitementAvantJob(GenericXmlApplicationContext contexteExecutionJob) {
        // Validation de l'existence du répertoire de sortie
        if (!verifieExistencePhysiqueRessource(contexteExecutionJob, emplacementRessourceRepertoireSortie)) {
            LOGGER.error("Répertoire manquant pour démarrer le batch : {}", emplacementRessourceRepertoireSortie);
            return false;
        }

        // Validation de la date
        if (!DateRedac.verifieDateRedac(this.dateDuJour)) {
            LOGGER.error("Date du jour de publication invalide : {}", this.dateDuJour);
            return false;
        }

        return true;
    }

    @Override
    protected JobParameters initParametrageJob(GenericXmlApplicationContext contexteExecutionJob) {
        Map<String, JobParameter> parametresJob = new HashMap<String, JobParameter>();

        // Configuration repertoire de sortie
        LOGGER.debug("Répertoire de sortie pris en compte : {}", emplacementRessourceRepertoireSortie);
        parametresJob.put("repertoireSortie", new JobParameter(emplacementRessourceRepertoireSortie));

        LOGGER.debug("Timestamp pris en compte pour les traitements (date du jour) : {}", dateDuJour);
        parametresJob.put("dateDuJour", new JobParameter(dateDuJour));

        LOGGER.debug("Si Aval pris en compte pour les traitements : {}", siAval);
        parametresJob.put("siAval", new JobParameter(siAval));

        LOGGER.debug("Timestamp à inclure dans les noms de fichier exportés : {}", timestampExport);
        parametresJob.put("racineDateNomFichiers", new JobParameter(timestampExport));

        // Configuration de l'emplacement du fichier de log
        LOGGER.debug("Fichier erreurs pris en compte : {}", getCheminAbsoluFichierLogErreurs());
        parametresJob.put("fichierLogErreurs", new JobParameter(getCheminAbsoluFichierLogErreurs()));

        return new JobParameters(parametresJob);
    }

    @Override
    protected void lanceTraitementFinJob(GenericXmlApplicationContext contexteExecutionJob, CodeRetour codeRetour, JobExecution executionJob) {
        // Logge le message de réussite du traitement batch dans le fichier de log
        if (codeRetour == CodeRetour.TERMINE) {
            traceTraitementOKDansFichierLog("Traitement du flux 78 - Publication - réussi - SI_AVAL=" + siAval);
        } else if (codeRetour == CodeRetour.ERREUR_FONCTIONNELLE_NON_BLOQUANTE) {
            traceTraitementOKDansFichierLog("Traitement du flux 78 - Publication - réussi, avec erreur(s) non bloquante(s) - SI_AVAL=" + siAval);
        }

    }

}
