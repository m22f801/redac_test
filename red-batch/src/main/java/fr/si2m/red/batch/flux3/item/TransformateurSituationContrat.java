package fr.si2m.red.batch.flux3.item;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import fr.si2m.red.DateRedac;
import fr.si2m.red.batch.flux3.ligne.SituationContrat;
import fr.si2m.red.batch.moteur.item.TransformateurDonnee;
import fr.si2m.red.complement.ExtensionContrat;
import fr.si2m.red.complement.ExtensionContratRepository;
import fr.si2m.red.contrat.Contrat;
import fr.si2m.red.contrat.TarifRepository;
import fr.si2m.red.parametrage.ParamFamilleModeCalculContratRepository;
import fr.si2m.red.parametrage.ParamValeurDefautRepository;
import fr.si2m.red.reconciliation.IndicateursDSNPeriode;
import fr.si2m.red.reconciliation.IndicateursDSNPeriodeRepository;
import lombok.Setter;

/**
 * Résolution des entités {@link SituationContrat} et leurs indicateurs à exporter à partir d'un contrat.<br/>
 * <br/>
 * c.f. F03_RG_S01
 * 
 * @author delortjouvesf
 *
 */
public class TransformateurSituationContrat extends TransformateurDonnee<Contrat, List<SituationContrat>> {

    @Setter
    private IndicateursDSNPeriodeRepository indicateursDSNPeriodeRepository;

    @Setter
    private ExtensionContratRepository extensionContratRepository;

    @Setter
    private ParamValeurDefautRepository paramValeurDefautRepository;

    @Setter
    private TarifRepository tarifRepository;

    @Setter
    private ParamFamilleModeCalculContratRepository paramFamilleModeCalculContratRepository;

    private static final String VIP_N = "N";

    @Override
    public List<SituationContrat> process(Contrat contrat) {

        List<SituationContrat> situationsContrat = new ArrayList<SituationContrat>();

        // On recupère les données d'une extension éventuelle de contrat
        ExtensionContrat extensionContrat = extensionContratRepository.getExtensionContrat(contrat.getNumContrat());
        IndicateursDSNPeriode indicateursDSNPeriode = indicateursDSNPeriodeRepository.getIndicateursDernierePeriode(contrat.getNumContrat());

        // Indicateur si une extension contrat entraîne la définition d'une nouvelle situation contrat
        boolean extensionEffectivePleinement = false;
        boolean extensionEffectivePartiellement = false;

        if (extensionContrat != null) {
            // Pour les comparaisons de dates, on transforme les dates de fin absentes en valeur maxi (99999999)
            int dateFinSituationContrat = DateRedac.getDateComparable(contrat.getDateFinSituationLigne());
            int dateEffetsIndicateursExtension = extensionContrat.getDateEffetIndicateurs();

            // Vérification de la primeur des indicateurs à prendre en compte
            boolean primeurIndicateursDSNPeriode = indicateursDSNPeriode != null
                    && dateEffetsIndicateursExtension <= DateRedac.getDateComparable(indicateursDSNPeriode.getFinPeriode());

            // L'extension entraîne une nouvelle situation de contrat seulement si les indicateurs DSN Période n'ont pas la priorité et si la date d'effet de
            // l'extension est comprise entre la date de début et la date de fin de situation.
            extensionEffectivePleinement = !primeurIndicateursDSNPeriode && dateEffetsIndicateursExtension <= contrat.getDateDebutSituationLigne();
            extensionEffectivePartiellement = !primeurIndicateursDSNPeriode && dateEffetsIndicateursExtension > contrat.getDateDebutSituationLigne()
                    && dateEffetsIndicateursExtension <= dateFinSituationContrat;
        }

        // Création de la situation contrat de base
        SituationContrat situationContratNominale = creeSituationContrat(contrat);
        situationContratNominale.setDateDebutSituation(contrat.getDateDebutSituationLigne());
        if (extensionEffectivePartiellement) {
            // La date de fin de situation à prendre en compte dans ce cas est celle du "début d'effet de l'extension - 1 jour"
            Integer dateFinSituationAjustee = DateRedac.retirerALaDate(extensionContrat.getDateEffetIndicateurs(), 1, DateRedac.MODIFIER_JOUR,
                    "TransformateurSituationDate");
            situationContratNominale.setDateFinSituation(dateFinSituationAjustee);
        } else {
            situationContratNominale.setDateFinSituation(contrat.getDateFinSituationLigne());
        }
        if (!extensionEffectivePleinement) {
            // VIP systématiquement mis à N pour la Brique
            situationContratNominale.setContratVIP(VIP_N);
            appliqueIndicateursSituationAPartirIndicateursDSNPeriodes(situationContratNominale, indicateursDSNPeriode, contrat);
        } else {
            appliqueIndicateursSituationAPartirExtensionContrat(situationContratNominale, extensionContrat, contrat);
        }
        situationsContrat.add(situationContratNominale);

        if (extensionEffectivePartiellement) {
            // Création de la situation contrat avec l'extension effective sur une partie de la situation
            SituationContrat situationExtensionContrat = creeSituationContrat(contrat);
            situationExtensionContrat.setDateDebutSituation(extensionContrat.getDateEffetIndicateurs());
            situationExtensionContrat.setDateFinSituation(contrat.getDateFinSituationLigne());
            appliqueIndicateursSituationAPartirExtensionContrat(situationExtensionContrat, extensionContrat, contrat);

            situationsContrat.add(situationExtensionContrat);
        }

        return situationsContrat;

    }

    /**
     * Crée une ligne SituationContrat completée avec avec les données du contrat passé en parametre. <br/>
     * Ne gere pas les dates de debut et fin de situation.<br/>
     * <br/>
     * c.f. F03_RG_S01
     * 
     * @param contrat
     *            le contrat à l'origine de la situation
     * @return la situation de contrat créée
     * 
     */
    private SituationContrat creeSituationContrat(Contrat contrat) {
        SituationContrat situationContrat = new SituationContrat();

        situationContrat.setNumContrat(contrat.getNumContrat());

        situationContrat.setCodeMiseAJour("R");

        situationContrat.setTypeContrat(contrat.getDesignationFamille());

        situationContrat.setDateDebutEffetContrat(contrat.getDateEffetContrat());

        situationContrat.setCodeOrganismePorteurRisque(paramValeurDefautRepository.getCodeOrganismeParDefaut());

        situationContrat.setNumCompteEncaisseur(contrat.getCompteEncaissement());

        String codePeriodicite = contrat.getCodeFractionnement();
        String periodicite = "Trimestriel";
        if (codePeriodicite != null) {
            switch (codePeriodicite) {
            case "M":
                periodicite = "Mensuel";
                break;
            case "T":
                // On précise quand même ce cas
                periodicite = "Trimestriel";
                break;
            case "S":
                periodicite = "Semestriel";
                break;
            case "A":
                periodicite = "Annuel";
                break;
            default:
            }
        }

        situationContrat.setPeriodicitePaiement(periodicite);

        situationContrat.setIdentifiantTechniqueContrat(contrat.getNumContrat());

        situationContrat.setDesignationContrat(contrat.getDesignationFamille());

        // valeurs utilisées pour les fichier contrat-entreprise et contrat-Etablissement
        situationContrat.setNumClient(contrat.getNumSouscripteur());
        situationContrat.setEligibiliteDSN(contrat.getEligibiliteContratDsn());
        situationContrat.setCodeIntermediaire(contrat.getCompteSinistres());

        // NumFamilleGarantie, CodeOrganismeAssureur1, CodeOrganismeAssureur2 sont des int
        // valeur par defaut = 0

        situationContrat.setTauxAppel(contrat.getTauxAppel());

        situationContrat.setNmGrpges(contrat.getGroupeGestion());

        return situationContrat;

    }

    /**
     * Modifie la ligne de situationContrat en appliquant les indicateurs DSN Période. F03_RG_S01 F03_RG_S02
     * 
     * @param situationContrat
     *            la situation de contrat dont les indicateurs doivent être résolus
     * @param indicateursDSNPeriode
     *            les indicateurs à appliquer à la situation
     * @param contrat
     *            le contrat de référence
     */
    private void appliqueIndicateursSituationAPartirIndicateursDSNPeriodes(SituationContrat situationContrat,
            IndicateursDSNPeriode indicateursDSNPeriode, Contrat contrat) {
        if (indicateursDSNPeriode != null) {
            // Application des indicateurs pour la dernière période, sauf VIP
            situationContrat.setIndicateurControleAssiette(transcodeControleAssiette(indicateursDSNPeriode.getControleTypeBaseAssujettie()));
            situationContrat.setIdentifiantDelegataireCotisation(
                    getNumCompteEncaisseurSiModeDelegue(indicateursDSNPeriode.getModeNatureContrat(), contrat.getCompteEncaissement()));
            // /!\ Important pour la suite de l'export
            situationContrat.setModeDelegue(isContratEnModeDelegue(indicateursDSNPeriode.getModeNatureContrat(), contrat.getCompteEncaissement()));
        } else {
            // Application des valeurs par défaut, sauf VIP
            Integer modeCalculCotisation = tarifRepository.getModeCalculCotisation(contrat.getNumContrat());
            String controleTypeParDefaut = paramFamilleModeCalculContratRepository
                    .getModeControleTypesBasesAssujetties(contrat.getNumFamilleProduit(), contrat.getGroupeGestion(), modeCalculCotisation);

            String modeNatureContratDefaut = paramValeurDefautRepository.getModeNatureContratParDefaut();

            situationContrat.setIndicateurControleAssiette(transcodeControleAssiette(controleTypeParDefaut));
            situationContrat.setIdentifiantDelegataireCotisation(
                    getNumCompteEncaisseurSiModeDelegue(modeNatureContratDefaut, contrat.getCompteEncaissement()));
            // /!\ Important pour la suite de l'export
            situationContrat.setModeDelegue(isContratEnModeDelegue(modeNatureContratDefaut, contrat.getCompteEncaissement()));
        }
    }

    /**
     * Modifie la ligne de situationContrat en appliquant les indicateurs de l'extension de contrat. F03_RG_S01 F03_RG_S02
     * 
     * @param contrat
     *            le contrat de référence
     * @param extensionContratSituation
     *            l'extension de contrat
     * @param situationContrat
     *            la situation de contrat dont les indicateurs doivent être résolus
     */
    private void appliqueIndicateursSituationAPartirExtensionContrat(SituationContrat situationContrat, ExtensionContrat extensionContratSituation,
            Contrat contrat) {
        // VIP systématiquement mis à N pour la Brique
        situationContrat.setContratVIP(VIP_N);
        situationContrat.setIndicateurControleAssiette(transcodeControleAssiette(extensionContratSituation.getControleTypeBaseAssujettie()));
        situationContrat.setIdentifiantDelegataireCotisation(
                getNumCompteEncaisseurSiModeDelegue(extensionContratSituation.getModeNatureContrat(), contrat.getCompteEncaissement()));
        // /!\ Important pour la suite de l'export
        situationContrat.setModeDelegue(isContratEnModeDelegue(extensionContratSituation.getModeNatureContrat(), contrat.getCompteEncaissement()));
    }

    /**
     * Transcode un contrôle de type de base assujettie en un code de contrôle d'assiette.
     * 
     * @param codeControleTypeBaseAssujettie
     *            le contrôle de type de base assujettie
     * @return le code de contrôle d'assiette correspondant
     */
    private String transcodeControleAssiette(String codeControleTypeBaseAssujettie) {
        String code = "";
        if (codeControleTypeBaseAssujettie != null) {
            switch (codeControleTypeBaseAssujettie) {
            case "N":
                code = "N";
                break;
            case "RED":
                code = "N";
                break;
            case "BRR":
                code = "R";
                break;
            case "BRB":
                code = "J";
                break;
            default:
                throw new IllegalArgumentException("Champ indicateur contrôle assiette inconnu : " + codeControleTypeBaseAssujettie);
            }
        } else {
            throw new IllegalArgumentException("Champ indicateur contrôle assiette inconnu");
        }
        return code;

    }

    /**
     * Détermine si un contrat est en mode délégué.
     * 
     * c.f. f03_RG_S16
     * 
     * @param modeNatureContrat
     *            le mode du contrat à prendre en compte
     * @return true si le contrat est en mode délégué, false sinon
     */
    private boolean isContratEnModeDelegue(String modeNatureContrat, String numComteEncaisseur) {
        return StringUtils.equals(modeNatureContrat, "L") || StringUtils.equals(modeNatureContrat, "C") && !StringUtils.isNumeric(numComteEncaisseur)
                && !StringUtils.startsWith(numComteEncaisseur, "R1");
    }

    /**
     * Récupère le numéro de compte d'encaisseur d'une situation contrat que si le contrat est en mode délégué.
     * 
     * c.f. f03_RG_S16
     * 
     * @param modeNatureContrat
     *            le mode du contrat à prendre en compte
     * @param numComteEncaisseur
     *            le numéro du compte d'encaissement du contrat
     * @return le numéro de compte d'encaissement si le contrat est en mode délégué, un espace sinon
     */
    private String getNumCompteEncaisseurSiModeDelegue(String modeNatureContrat, String numComteEncaisseur) {
        String numCompteEncaisseurSituation = " ";
        if (isContratEnModeDelegue(modeNatureContrat, numComteEncaisseur)) {
            numCompteEncaisseurSituation = numComteEncaisseur;
        }
        return numCompteEncaisseurSituation;
    }

}
