package fr.si2m.red.batch.flux78.item;

import java.text.MessageFormat;

import lombok.Setter;
import fr.si2m.red.EntiteImportableBatch;
import fr.si2m.red.batch.RedacMessages;
import fr.si2m.red.batch.moteur.erreur.ValidateurLigneAvecCollecte;

/**
 * Vérification de la validité du nombre d'enregistrement indiqué dans la ligne de synthèse.
 * 
 * @author poidij
 *
 */
public class ControleNbEnregistrementsProcesseur extends ValidateurLigneAvecCollecte<EntiteImportableBatch> {

    @Setter
    private String fichierEntree;

    @Override
    public void valide(EntiteImportableBatch item) {
        item.setNomFichierImport(fichierEntree);

        Object contextValue = getStepExecution().getJobExecution().getExecutionContext().get(ControleLigneSyntheseProcesseur.LIGNE_SYNTHESE_KO);

        if (!(contextValue == null ? true : (boolean) contextValue)) {
            // Entorse à l'architechture en vigueur, faute de temps pour reprendre proprement ;-) : on met ligneSyntaxiquementValide à false, afin de faire
            // passer item.isImportValide() à false.
            // De ce fait, ListenerCollecteErreursValidation.afterWrite détectera qu'il y a une erreur 40 et ErreursCollecteesDecideur reverra KO.
            // Par suite, RedacteurErreurs n'écrira rien pour cet item car on n'aura pas rempli la liste d'erreur de l'item lui même. Gégé.
            item.setLigneSyntaxiquementValide(false);
            valideChampAvecCollecte(false, item, "NbreEnregistrements1", RedacMessages.PAS_DE_SYNTHESE);
        }

        contextValue = getStepExecution().getJobExecution().getExecutionContext().get(RedacMessages.CHAMP_NBRE_ENREGISTREMENTS2);

        if (!(contextValue == null ? true : (boolean) contextValue)) {
            item.setLigneSyntaxiquementValide(false);
            String messageErreur = MessageFormat.format(RedacMessages.ERREUR_CHAMP_VALEUR_DIFFERENTE, RedacMessages.CHAMP_NBRE_ENREGISTREMENTS2,
                    "du nombre d'enregistrements 2 présents");
            valideChampAvecCollecte(false, item, RedacMessages.CHAMP_NBRE_ENREGISTREMENTS2, messageErreur);
        }

        contextValue = getStepExecution().getJobExecution().getExecutionContext().get(RedacMessages.CHAMP_NBRE_ENREGISTREMENTS3);
        if (!(contextValue == null ? true : (boolean) contextValue)) {
            item.setLigneSyntaxiquementValide(false);
            String messageErreur = MessageFormat.format(RedacMessages.ERREUR_CHAMP_VALEUR_DIFFERENTE, RedacMessages.CHAMP_NBRE_ENREGISTREMENTS3,
                    "du nombre d'enregistrements 3 présents");
            valideChampAvecCollecte(false, item, RedacMessages.CHAMP_NBRE_ENREGISTREMENTS3, messageErreur);
        }
    }
}
