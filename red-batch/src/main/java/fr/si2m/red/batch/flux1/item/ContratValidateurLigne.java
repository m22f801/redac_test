package fr.si2m.red.batch.flux1.item;

import java.text.MessageFormat;

import org.apache.commons.lang3.StringUtils;

import fr.si2m.red.DateRedac;
import fr.si2m.red.batch.RedacMessages;
import fr.si2m.red.batch.moteur.erreur.ValidateurLigneAvecCollecte;
import fr.si2m.red.contrat.ClientRepository;
import fr.si2m.red.contrat.Contrat;
import fr.si2m.red.contrat.IntermediaireRepository;
import fr.si2m.red.parametrage.ParamEtatContratRepository;
import fr.si2m.red.parametrage.ParamFamilleContratRepository;
import lombok.Setter;

/**
 * Validateur de ligne contrat.
 * 
 * @author delortj
 *
 */
public class ContratValidateurLigne extends ValidateurLigneAvecCollecte<Contrat> {

    @Setter
    private ClientRepository clientRepository;
    @Setter
    private IntermediaireRepository intermediaireRepository;
    @Setter
    private ParamEtatContratRepository paramEtatContratRepository;
    @Setter
    private ParamFamilleContratRepository paramFamilleContratRepository;

    @Override
    protected void valide(final Contrat contrat) {

        valideChampsObligatoires(contrat);

        valideLongueurChampsTexte(contrat);

        valideLongueurChampsDate(contrat);

        // Champs particuliers
        valideChampEligibiliteContratDsn(contrat);

        valideChampDelegationPaiement(contrat);

        // F01_RG_C16
        valideChampNumeriqueDecimaleAvecCollecte(contrat.getTauxAppel(), 5, 2, contrat, "TXAPPCOT",
                "le champ TXAPPCOT dépasse la taille maximale prévue");

        validationCorrespondancesVersAutresTables(contrat);

        valideCoupleNofamGrpGest(contrat);
    }

    /**
     * Validation des champs obligatoires du contrat.
     * 
     * @param contrat
     *            Le contrat dont les champs obligatoires doivent être validés.
     */
    private void valideChampsObligatoires(final Contrat contrat) {
        // Validation des champs obligatoires
        // F01_RG_C02
        valideChampAvecCollecte(StringUtils.isNotBlank(contrat.getNumContrat()), contrat, "NOCO", "Le champ NOCO obligatoire n'est pas renseigné");
    }

    /**
     * Validation des champs texte du contrat.
     * 
     * @param contrat
     *            Le contrat dont les textes doivent être validés.
     */
    private void valideLongueurChampsTexte(final Contrat contrat) {
        // Longueur des champs

        // F01_RG_C02
        valideTailleMaximaleChampTexteAvecCollecte(contrat.getNumContrat(), 15, contrat, "NOCO");

        // F01_RG_C03
        valideTailleMaximaleChampTexteAvecCollecte(contrat.getNumProduit(), 10, contrat, "NMPDT");

        // F01_RG_C04
        valideTailleMaximaleChampTexteAvecCollecte(contrat.getNumCompteProducteur(), 6, contrat, RedacMessages.CHAMP_NCPROD);

        // F01_RG_C06
        valideTailleMaximaleChampTexteAvecCollecte(contrat.getEligibiliteContratDsn(), 3, contrat, RedacMessages.CHAMP_ELIGDSN);

        // F01_RG_C07
        valideTailleMaximaleChampAvecCollecte(contrat.getNumSouscripteur(), 6, contrat, RedacMessages.CHAMP_NOCLI);

        // F01_RG_C09
        valideTailleMaximaleChampAvecCollecte(contrat.getTypeSouscripteur(), 2, contrat, "TYSOUS");

        // F01_RG_C10
        valideTailleMaximaleChampTexteAvecCollecte(contrat.getNomSouscripteur(), 38, contrat, "NMSOUS");

        // F01_RG_C11
        valideTailleMaximaleChampAvecCollecte(contrat.getEtatContrat(), 2, contrat, RedacMessages.CHAMP_COETACO);

        // F01_RG_C12
        valideTailleMaximaleChampTexteAvecCollecte(contrat.getModeQuittancementContrat(), 1, contrat, "COMOQUIT");

        // F01_RG_C13
        valideTailleMaximaleChampAvecCollecte(contrat.getTypeContrat(), 1, contrat, "TYCO");

        // F01_RG_C14
        valideTailleMaximaleChampTexteAvecCollecte(contrat.getModeEmission(), 1, contrat, "TYEMI");

        // F01_RG_C15
        valideTailleMaximaleChampTexteAvecCollecte(contrat.getCodeFractionnement(), 1, contrat, RedacMessages.CHAMP_COFRQUIT);

        // F01_RG_C17
        valideTailleMaximaleChampTexteAvecCollecte(contrat.getGroupeGestion(), 2, contrat, "NMGRPGES");

        // F01_RG_C18
        valideTailleMaximaleChampTexteAvecCollecte(contrat.getGroupeProduction(), 2, contrat, "NMGRPPRO");

        // F01_RG_C19
        valideTailleMaximaleChampTexteAvecCollecte(contrat.getGroupeSinistre(), 2, contrat, "NMGRPSIN");

        // F01_RG_C20
        valideTailleMaximaleChampTexteAvecCollecte(contrat.getGroupeCommissions(), 2, contrat, "NMGRPCOM");

        // F01_RG_C21
        valideTailleMaximaleChampTexteAvecCollecte(contrat.getCompteEncaissement(), 6, contrat, RedacMessages.CHAMP_NCENC);

        // F01_RG_C24
        valideTailleMaximaleChampAvecCollecte(contrat.getNumFamilleProduit(), 3, contrat, "NOFAM");

        // F01_RG_C26
        valideTailleMaximaleChampTexteAvecCollecte(contrat.getCompteSinistres(), 6, contrat, RedacMessages.CHAMP_NCSIN);

        // F01_RG_C27
        valideTailleMaximaleChampTexteAvecCollecte(contrat.getRibComplet(), 23, contrat, "NORIB");

        // F01_RG_C28
        valideTailleMaximaleChampTexteAvecCollecte(contrat.getCodeAni(), 1, contrat, "COANI");

        // F01_RG_C29
        valideTailleMaximaleChampTexteAvecCollecte(contrat.getCategorieObjective(), 3, contrat, "CATOBJ");

        // F01_RG_C30
        valideTailleMaximaleChampTexteAvecCollecte(contrat.getDelegationPaiement(), 1, contrat, "CDLP");

        // F01_RG_C33
        valideTailleMaximaleChampAvecCollecte(contrat.getNumClientDelegation(), 6, contrat, RedacMessages.CHAMP_NUCLIDLP);

        // F01_RG_C34
        valideTailleMaximaleChampAvecCollecte(contrat.getEffectifs(), 6, contrat, "NBEFF");

    }

    /**
     * Validation des champs Date du contrat.
     * 
     * @param contrat
     *            Le contrat dont les dates doivent être validées.
     */
    private void valideLongueurChampsDate(final Contrat contrat) {
        // F01_RG_C31 - JIRA 119
        contrat.setDebutDelegation(valideChampDateAvecCollecte(contrat.getDebutDelegation(), DateRedac.FORMAT_DATES, contrat, "DTDEBDLP", false));

        // F01_RG_C32 - JIRA 119
        contrat.setFinDelegation(valideChampDateAvecCollecte(contrat.getFinDelegation(), DateRedac.FORMAT_DATES, contrat, "DTFINDLP", false));

        // Verification des dates
        // F01_RG_C05
        contrat.setDateDebutSituationLigne(
                valideChampDateAvecCollecte(contrat.getDateDebutSituationLigne(), DateRedac.FORMAT_DATES, contrat, "DT_DEBUT_SIT", true));

        // F01_RG_C08
        contrat.setDateEffetContrat(valideChampDateAvecCollecte(contrat.getDateEffetContrat(), DateRedac.FORMAT_DATES, contrat, "DTEFFCO", false));

        // F01_RG_C22
        contrat.setDateModifCodeEtat(valideChampDateAvecCollecte(contrat.getDateModifCodeEtat(), DateRedac.FORMAT_DATES, contrat, "DTCREA", false));

        // F01_RG_C23
        contrat.setDateCreationContrat(
                valideChampDateAvecCollecte(contrat.getDateCreationContrat(), DateRedac.FORMAT_DATES, contrat, "DTCREACO", false));

        // F01_RG_C25
        contrat.setDateDerniereModifSituationLigne(
                valideChampDateAvecCollecte(contrat.getDateDerniereModifSituationLigne(), DateRedac.FORMAT_DATES, contrat, "DT_DERN_MODIF", false));

        // F01_RG_C35
        contrat.setDateFinSituationLigne(
                valideChampDateAvecCollecte(contrat.getDateFinSituationLigne(), DateRedac.FORMAT_DATES, contrat, "DT_FIN_SIT", false));

    }

    /**
     * Validation des correspondances vers les autres tables.
     * 
     * @param contrat
     */
    private void validationCorrespondancesVersAutresTables(final Contrat contrat) {
        boolean existeUnNocli = clientRepository.existeUnClientTemporaire(contrat.getNumSouscripteur());
        String messageErreurNocli = MessageFormat.format(RedacMessages.ERREUR_REFERENCE, "Le client référencé", RedacMessages.CHAMP_NOCLI,
                contrat.getNumSouscripteur());
        valideChampAvecCollecte(existeUnNocli, contrat, RedacMessages.CHAMP_NOCLI, messageErreurNocli);

        if (contrat.getNumClientDelegation() != null && contrat.getNumClientDelegation() < Math.pow(10, 6)) {
            boolean existeUnNuclidlp = clientRepository.existeUnClientTemporaire(contrat.getNumClientDelegation());
            String messageErreurNuclidlp = MessageFormat.format(RedacMessages.ERREUR_REFERENCE, "Le client référencé", RedacMessages.CHAMP_NUCLIDLP,
                    contrat.getNumClientDelegation());
            valideChampAvecCollecte(existeUnNuclidlp, contrat, RedacMessages.CHAMP_NUCLIDLP, messageErreurNuclidlp);
        }

        boolean existeUnNcprod = intermediaireRepository.existeUnNcprod(contrat.getNumCompteProducteur());
        String messageErreurNcprod = MessageFormat.format(RedacMessages.ERREUR_REFERENCE, "L'intérmédiaire apporteur référencé",
                RedacMessages.CHAMP_NCPROD, contrat.getNumCompteProducteur());
        valideChampAvecCollecte(existeUnNcprod, contrat, RedacMessages.CHAMP_NCPROD, messageErreurNcprod);

        boolean existeUnNcenc = intermediaireRepository.existeUnNcprod(contrat.getCompteEncaissement());
        String messageErreurNcenc = MessageFormat.format(RedacMessages.ERREUR_REFERENCE,
                "L'intérmédiaire délégataire de gestion des cotisations référencé", RedacMessages.CHAMP_NCENC, contrat.getCompteEncaissement());
        valideChampAvecCollecte(existeUnNcenc, contrat, RedacMessages.CHAMP_NCENC, messageErreurNcenc);

        boolean existeUnNcsin = intermediaireRepository.existeUnNcprod(contrat.getCompteSinistres());
        String messageErreurNcsin = MessageFormat.format(RedacMessages.ERREUR_REFERENCE,
                "L'intérmédiaire délégataire de gestion des sinistres référencé", RedacMessages.CHAMP_NCSIN, contrat.getCompteSinistres());
        valideChampAvecCollecte(existeUnNcsin, contrat, RedacMessages.CHAMP_NCSIN, messageErreurNcsin);

        // F01_RG_NBC01
        boolean etatContratCorrespondantExiste = paramEtatContratRepository.existeParamEtatContrat(contrat.getEtatContrat());
        String messageErreurCoetaco = "Le COETACO (" + contrat.getEtatContrat() + ") n'existe pas dans la table ParamEtatContrat";
        valideChampAvecCollecteSansBlocage(etatContratCorrespondantExiste, contrat, RedacMessages.CHAMP_COETACO, messageErreurCoetaco);
    }

    /**
     * Validation et mapping du champ d'éligibilité du contrat en cours de traitement.
     * 
     * @param contrat
     */
    private void valideChampEligibiliteContratDsn(final Contrat contrat) {
        // F01_RG_BC06

        // Validation du champ.
        if (StringUtils.isNotBlank(contrat.getEligibiliteContratDsn()) && contrat.getEligibiliteContratDsn().length() <= 3) {
            boolean valeurEligdsnValide = StringUtils.equals(contrat.getEligibiliteContratDsn(), "SRT")
                    || StringUtils.equals(contrat.getEligibiliteContratDsn(), "SRN") || StringUtils.equals(contrat.getEligibiliteContratDsn(), "NON");

            valideChampAvecCollecte(valeurEligdsnValide, contrat, RedacMessages.CHAMP_ELIGDSN, MessageFormat
                    .format(RedacMessages.ERREUR_CHAMP_VALEUR_INCORRECTE, RedacMessages.CHAMP_ELIGDSN, contrat.getEligibiliteContratDsn()));
        }

        // Mapping du champ.
        if (StringUtils.isBlank(contrat.getEligibiliteContratDsn())) {
            contrat.setEligibiliteContratDsn("NON");
        }

    }

    /**
     * Validation du champ délégation de paiement du contrat en cours de traitement.
     * 
     * @param contrat
     */
    private void valideChampDelegationPaiement(final Contrat contrat) {
        // F01_RG_BC08
        if (StringUtils.isNotBlank(contrat.getDelegationPaiement()) && contrat.getDelegationPaiement().length() <= 1) {
            boolean valeurCdlpValide = StringUtils.equals(contrat.getDelegationPaiement(), "O")
                    || StringUtils.equals(contrat.getDelegationPaiement(), "N");

            valideChampAvecCollecte(valeurCdlpValide, contrat, "CDLP",
                    MessageFormat.format(RedacMessages.ERREUR_CHAMP_VALEUR_INCORRECTE, "CDLP", contrat.getDelegationPaiement()));
        }
    }

    private void valideCoupleNofamGrpGest(final Contrat contrat) {
        // F01_RG_BC10

        String messageErreurNocatLicat = "Couple famille/groupe de gestion inconnu sur le contrat n° " + contrat.getNumContrat() + " (famille : "
                + contrat.getNumFamilleProduit() + " / groupe gestion : " + contrat.getGroupeGestion() + ")";

        boolean existeCoupleNofamGrpGest = paramFamilleContratRepository.existeCoupleNofamNmgrpges(contrat.getNumFamilleProduit(),
                contrat.getGroupeGestion());

        valideChampAvecCollecte(existeCoupleNofamGrpGest, contrat, "NOCAT/LICAT", messageErreurNocatLicat);
    }
}
