package fr.si2m.red.batch.moteur.item;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.si2m.red.EntiteImportableBatch;

/**
 * Gestionnaire d'insertion de données en base.
 * 
 * @author nortaina
 *
 * @param <T>
 *            le type de données
 */
public class ModificateurEntitesReferentiel<T extends EntiteImportableBatch> extends RedacteurBaseEntiteImportable<T> {
    private static final Logger LOGGER = LoggerFactory.getLogger(ModificateurEntitesReferentiel.class);

    @Override
    public void write(List<? extends T> donnees) {
        if (donnees == null || donnees.isEmpty()) {
            LOGGER.info("Aucune modification de données effectuée !");
            return;
        }
        getRedacRepository().modifieEntitesExistantes(donnees);
    }

}
