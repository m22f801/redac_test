package fr.si2m.red.batch.flux4.item;

import lombok.Setter;

import org.apache.commons.lang3.StringUtils;

import fr.si2m.red.DateRedac;
import fr.si2m.red.batch.RedacMessages;
import fr.si2m.red.batch.moteur.erreur.ValidateurLigneAvecCollecte;
import fr.si2m.red.dsn.AffiliationRepository;
import fr.si2m.red.dsn.BaseAssujettie;
import fr.si2m.red.dsn.BaseAssujettieRepository;

/**
 * Validateur de ligne de paramétrage code libellé.
 * 
 * @author benitahy
 *
 */
public class BaseAssujettieValidateurLigne extends ValidateurLigneAvecCollecte<BaseAssujettie> {

    @Setter
    private AffiliationRepository affiliationRepository;

    @Setter
    private BaseAssujettieRepository baseAssujettieRepository;

    @Override
    protected void valide(final BaseAssujettie baseAssujettie) {

        // unicité
        valideChampAvecCollecte(!baseAssujettieRepository.existeUneBaseAssujettie(baseAssujettie.getIdBaseAssujettie()), baseAssujettie, "ID",
                "La ligne ne respecte pas la règle d'unicité");

        // Champs obligatoires
        valideChampAvecCollecte(StringUtils.isNotBlank(baseAssujettie.getIdBaseAssujettie()), baseAssujettie, "ID_BASE_ASSUJETTIE",
                "Le champ ID_BASE_ASSUJETTIE obligatoire n'est pas renseigné");
        valideChampAvecCollecte(StringUtils.isNotBlank(baseAssujettie.getIdAffiliation()), baseAssujettie, "ID_AFFILIATION",
                "Le champ ID_AFFILIATION obligatoire n'est pas renseigné");
        valideChampAvecCollecte(baseAssujettie.getMontantCotisation() != null, baseAssujettie, "MONTANT_COTISATION", "Le champ MONTANT_COTISATION obligatoire n'est pas renseigné");

        // Longueur des champs
        valideTailleFixeChampAvecCollecte(baseAssujettie.getIdBaseAssujettie(), 30, baseAssujettie, "ID_BASE_ASSUJETTIE");
        valideTailleFixeChampAvecCollecte(baseAssujettie.getIdAffiliation(), 30, baseAssujettie, "ID_AFFILIATION");
        valideChampDateTailleFixeAvecCollecte(baseAssujettie.getDateDebutRattachementAsText(), DateRedac.FORMAT_DATES_DDMMYYYY, baseAssujettie, "DATE_DEB_RATTACHEMENT", true);
        valideChampDateTailleFixeAvecCollecte(baseAssujettie.getDateFinRattachementAsText(), DateRedac.FORMAT_DATES_DDMMYYYY, baseAssujettie, "DATE_FIN_RATTACHEMENT", true);
        valideChampNumeriqueDecimaleTailleFixeAvecCollecte(baseAssujettie.getMontantCotisationAsText(), 16, 2, baseAssujettie, "MONTANT_COTISATION");

        validationCorrespondanceReferentielle(baseAssujettie);
    }

    /**
     * Validation des liens entre tables.
     * 
     * @param baseAssujettie
     *            La baseAssujettie dont l'Affiliation doit être validée.
     */
    private void validationCorrespondanceReferentielle(final BaseAssujettie baseAssujettie) {
        if (StringUtils.isNotBlank(baseAssujettie.getIdAffiliation())) {
            boolean existeUneAffiliation = affiliationRepository.existeUneAffiliation(baseAssujettie.getIdAffiliation());
            valideChampAvecCollecte(existeUneAffiliation, baseAssujettie, "IdAffiliation", RedacMessages.ERREUR_REFERENCE_FLUX4);
        }
    }
}
