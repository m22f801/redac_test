package fr.si2m.red.batch.flux4.item;

import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;

import fr.si2m.red.batch.flux4.execution.HorodatageTraitement;
import fr.si2m.red.batch.flux4.execution.HorodatageTraitementRepository;
import fr.si2m.red.batch.moteur.EtapeCodeRetourModificateur;
import lombok.Setter;

/**
 * Tâche de promotion des domaines temporaires de référentiels lorsque toutes les entités importées ont été validées.
 * 
 * @author nortaina
 *
 */
public class TacheAjoutCodeTraitementValide extends EtapeCodeRetourModificateur implements Tasklet {

    @Setter
    private String horodatage;

    @Setter
    private HorodatageTraitementRepository horodatageTraitementRepository;

    @Override
    public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) throws Exception {

        HorodatageTraitement horodatageTraitement = new HorodatageTraitement();
        horodatageTraitement.setHorodatage(horodatage);

        horodatageTraitementRepository.create(horodatageTraitement);
        return RepeatStatus.FINISHED;
    }
}
