package fr.si2m.red.batch.moteur.item;

import org.springframework.batch.item.ItemProcessor;

import fr.si2m.red.batch.moteur.EtapeCodeRetourModificateur;

/**
 * Gestionnaire de transformation d'une donnée REDAC.
 * 
 * @author nortaina
 *
 * @param <I>
 *            type de la ligne en entrée
 * @param <O>
 *            type de la ligne en sortie
 * 
 */
public abstract class TransformateurDonnee<I, O> extends EtapeCodeRetourModificateur implements ItemProcessor<I, O> {
}
