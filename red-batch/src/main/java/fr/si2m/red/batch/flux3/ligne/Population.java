package fr.si2m.red.batch.flux3.ligne;

import lombok.Data;
import fr.si2m.red.DateRedac;

/**
 * Description d'une ligne d'un fichier POPULATIONS à exporter.
 * 
 * @author nortaina
 *
 */
@Data
public class Population {
    /**
     * Code Mise à jour.
     * 
     * @param codeMiseAJour
     *            Code Mise à jour
     * @return Code Mise à jour
     */
    private String codeMiseAJour;

    /**
     * Identifiant de l’entreprise.
     * 
     * @param identifiantEntrepriseG3C
     *            Identifiant de l’entreprise
     * @return Identifiant de l’entreprise
     */
    private String identifiantEntrepriseG3C;

    /**
     * N° de séquence contrat G3C.
     * 
     * @param numSequenceContratG3C
     *            N° de séquence contrat G3C
     * @return N° de séquence contrat G3C
     */
    private String numSequenceContratG3C;

    /**
     * Code population G3C.
     * 
     * @param codePopulationG3C
     *            Code population G3C
     * @return Code population G3C
     */
    private String codePopulationG3C;

    /**
     * Libellé population court G3C.
     * 
     * @param libellePopulationCourtG3C
     *            Libellé population court G3C
     * @return Libellé population court G3C
     */
    private String libellePopulationCourtG3C;

    /**
     * Libellé population long G3C.
     * 
     * @param libellePopulationLongG3C
     *            Libellé population long G3C
     * @return Libellé population long G3C
     */
    private String libellePopulationLongG3C;

    /**
     * Date de début d’application.
     * 
     * @param dateDebutApplication
     *            Date de début d’application
     * @return Date de début d’application
     */
    private Integer dateDebutApplication;

    /**
     * Date de fin d’application.
     * 
     * @param dateFinApplication
     *            Date de fin d’application
     * @return Date de fin d’application
     */
    private Integer dateFinApplication;

    /**
     * Libellé population à afficher.
     * 
     * @param libellePopulation
     *            Libellé population à afficher
     * @return Libellé population à afficher
     */
    private String libellePopulation;

    /**
     * Ancien code population.
     * 
     * @param ancienCodePopulation
     *            Ancien code population
     * @return Ancien code population
     */
    private String ancienCodePopulation;

    /**
     * Identifiant technique population (N° de séquence local à une instance de cette interface).
     * 
     * @param identifiantTechniquePopulation
     *            Identifiant technique population (N° de séquence local à une instance de cette interface)
     * @return Identifiant technique population (N° de séquence local à une instance de cette interface)
     */
    private String identifiantTechniquePopulation;

    /**
     * Référence à l’identifiant technique de l’entreprise (n° de séquence local à une instance de cette interface).
     * 
     * @param identifiantTechniqueEntreprise
     *            Référence à l’identifiant technique de l’entreprise (n° de séquence local à une instance de cette interface)
     * @return Référence à l’identifiant technique de l’entreprise (n° de séquence local à une instance de cette interface)
     */
    private String identifiantTechniqueEntreprise;

    /**
     * Référence à l’identifiant technique contrat (N° de séquence local à une instance de cette interface).
     * 
     * @param identifiantTechniqueContrat
     *            Référence à l’identifiant technique contrat (N° de séquence local à une instance de cette interface)
     * @return Référence à l’identifiant technique contrat (N° de séquence local à une instance de cette interface)
     */
    private String identifiantTechniqueContrat;

    /**
     * Code population à afficher.
     * 
     * @param codePopulation
     *            Code population à afficher
     * @return Code population à afficher
     */
    private String codePopulation;

    /**
     * Date de début d’application.
     * 
     * @return Date de début d’application
     */
    public String getDateDebutApplicationFormatee() {
        return DateRedac.formate(DateRedac.EXPORT_FORMAT_PAR_DEFAUT, dateDebutApplication);
    }

    /**
     * Date de fin d’application.
     * 
     * @return Date de fin d’application
     */
    public String getDateFinApplicationFormatee() {
        return DateRedac.formate(DateRedac.EXPORT_FORMAT_PAR_DEFAUT, dateFinApplication);
    }

}
