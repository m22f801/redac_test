package fr.si2m.red.batch.moteur.item;

import java.io.IOException;
import java.io.Writer;
import java.util.Collection;
import java.util.List;

import lombok.Setter;

import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.file.FlatFileFooterCallback;

/**
 * Ce rédacteur d'en-tête est un FlatFileFooterCallback car la ligne d'en-ête doit contenir le nombre de ligne de données : on écrit donc l'en-ête dans un
 * fichier à part, puis on concatène les données dans une étape supplémentaire (cf le wiki REDAC).
 * 
 * @author poidij
 *
 * @param <T>
 */
public class RedacteurEnTeteIndicateurs<T> implements ItemWriter<T>, FlatFileFooterCallback {

    @Setter
    private ItemWriter<T> delegate;
    @Setter
    private String dateCalculIndicateurs;

    private int nbDeLignes = 0;

    @Override
    public void writeFooter(Writer writer) throws IOException {
        String nbDeLignesTailleFixe = ("00000000" + nbDeLignes).substring(String.valueOf(nbDeLignes).length());
        writer.write("1" + nbDeLignesTailleFixe + dateCalculIndicateurs + "" + "\r\n");
    }

    @Override
    public void write(List<? extends T> items) throws Exception {
        for (Object item : items) {
            if (item instanceof Collection<?>) {
                nbDeLignes += ((Collection<?>) item).size();
            } else {
                nbDeLignes += 1;
            }
        }
        delegate.write(items);
    }

}
