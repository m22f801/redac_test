package fr.si2m.red.batch.flux78.item;

import java.io.IOException;
import java.io.Writer;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.file.FlatFileFooterCallback;

import lombok.Setter;

/**
 * Editeur de pied de page pour les exports du flux 8E.<br/>
 * <br/>
 * Ce rédacteur garde à jour un compteur pour le nombre de lignes de certaines entités à exporter pour le résumé de bas de page puis invoque le véritable
 * rédacteur du corps de fichier.
 * 
 * @author eudesr
 *
 * @param <T>
 *            le type de ligne exportée
 */
public class RedacteurPiedDePageEGER<T> implements ItemWriter<T>, FlatFileFooterCallback {

    private static final Logger LOGGER = LoggerFactory.getLogger(RedacteurPiedDePageEGER.class);

    /**
     * Délégué pour l'écriture effective des lignes dans le corps de fichier.
     */
    @Setter
    private ItemWriter<T> delegate;

    private int nbDeLignes = 1;

    @Override
    public void writeFooter(final Writer writer) throws IOException {

        long time = System.currentTimeMillis();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
        Date resultdate = new Date(time);

        writer.write("EGER;" + StringUtils.substring(String.valueOf(nbDeLignes), 0, 10) + ";" + sdf.format(resultdate) + "\r\n");

        LOGGER.debug("{} lignes totales affichées en pied-de-page", nbDeLignes);
    }

    @Override
    public void write(List<? extends T> items) throws Exception {
        for (Object item : items) {
            if (item instanceof Collection<?>) {
                nbDeLignes += ((Collection<?>) item).size();
            } else {
                nbDeLignes += 1;
            }
        }

        LOGGER.debug("{} lignes écrites...", nbDeLignes);
        delegate.write(items);
    }

}
