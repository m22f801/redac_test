/**
 * Module des composants de gestion d'erreurs pour les étapes de validations pures avec collectes globales dans le moteur REDAC.
 * 
 * @author nortaina
 *
 */
package fr.si2m.red.batch.moteur.erreur;

