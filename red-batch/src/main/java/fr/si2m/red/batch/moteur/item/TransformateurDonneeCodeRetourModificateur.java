package fr.si2m.red.batch.moteur.item;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.listener.ExecutionContextPromotionListener;
import org.springframework.batch.item.ItemProcessor;

import fr.si2m.red.batch.moteur.CodeRetour;
import fr.si2m.red.batch.moteur.support.LogUtils;
import lombok.AccessLevel;
import lombok.Getter;

/**
 * Gestionnaire de transformation d'une donnée REDAC.
 * 
 * @author poidij
 *
 * @param <I>
 *            type de la ligne en entrée
 * @param <O>
 *            type de la ligne en sortie
 * 
 */
public abstract class TransformateurDonneeCodeRetourModificateur<I, O> extends ExecutionContextPromotionListener implements ItemProcessor<I, O> {
    private static final Logger LOGGER = LoggerFactory.getLogger(TransformateurDonneeCodeRetourModificateur.class);

    public static final String CLE_FORCE_STATUT_EXECUTION = "CLE_FORCE_STATUT_EXECUTION";

    /**
     * L'exécution de l'étape.
     *
     * @return l'exécution de l'étape
     */
    @Getter(AccessLevel.PROTECTED)
    public StepExecution stepExecution;

    /**
     * Constructeur.
     * 
     */
    public TransformateurDonneeCodeRetourModificateur() {
        super();
        // Permet le passage du statut d'exécution forcé entre contextes d'exécution
        setKeys(new String[] { CLE_FORCE_STATUT_EXECUTION });
    }

    protected void traceErreurSansBlocage(String messageAvertissement, String cheminAbsoluFichierLogErreurs) {
        LOGGER.warn(messageAvertissement);
        LogUtils.traceErreurDansFichierLog(messageAvertissement, cheminAbsoluFichierLogErreurs);
        getStepExecution().getExecutionContext().put(CLE_FORCE_STATUT_EXECUTION, CodeRetour.ERREUR_FONCTIONNELLE_NON_BLOQUANTE);
    }

    protected void traceErreurSansBlocage(String messageAvertissement, String cheminAbsoluFichierLogErreurs, Exception e) {
        LOGGER.warn(messageAvertissement, e);
        LogUtils.traceErreurDansFichierLog(messageAvertissement, cheminAbsoluFichierLogErreurs);
        getStepExecution().getExecutionContext().put(CLE_FORCE_STATUT_EXECUTION, CodeRetour.ERREUR_FONCTIONNELLE_NON_BLOQUANTE);
    }

    /**
     * Valide un champ date.
     * 
     * @param dateNumerique
     *            la date au format numérique à valider
     * @param formatDate
     *            le format de date pour validation
     * @return la valeur de date valide (ou l'alternative par défaut) à prendre en compte pour l'import
     */
    protected Integer valideChampDateAvecCollecte(Integer dateNumerique, String formatDate) {

        // Validation taille
        int tailleFormatAttendu = StringUtils.replace(formatDate, "'", StringUtils.EMPTY).length();
        if (dateNumerique.toString().length() != tailleFormatAttendu) {
            return null;
        }

        // Validation format
        SimpleDateFormat format = new SimpleDateFormat(formatDate);
        format.setLenient(false);
        try {
            format.parse(dateNumerique.toString());
        } catch (ParseException e) {
            return null;
        }

        return dateNumerique;
    }

}
