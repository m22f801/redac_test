package fr.si2m.red.batch.moteur.execution;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParameter;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.context.support.GenericXmlApplicationContext;
import org.springframework.core.io.Resource;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;
import org.springframework.util.Assert;

import fr.si2m.red.Entite;
import fr.si2m.red.EntiteImportableBatch;
import fr.si2m.red.RedacUnexpectedException;
import fr.si2m.red.batch.RedacMessages;
import fr.si2m.red.batch.moteur.BatchConfiguration;
import fr.si2m.red.batch.moteur.CodeRetour;
import fr.si2m.red.batch.moteur.item.JpaTempRepository;
import fr.si2m.red.core.repository.EntiteImportableRepository;
import lombok.Getter;
import lombok.Setter;

/**
 * Exécuteur de batch REDAC.<br/>
 * <br/>
 * Propose une base à surcharger pour l'exécution standardisée d'un job batch REDAC préconfiguré.
 * 
 * @author nortaina
 *
 */
public abstract class ExecuteurBatch {
    private static final Logger LOGGER = LoggerFactory.getLogger(ExecuteurBatch.class);

    /**
     * Emplacement de la configuration du moteur des batchs REDAC.
     */
    public static final String EMPLACEMENT_CONFIGURATION_MOTEUR_BATCH = "classpath:fr/si2m/red/batch/moteur/config.xml";

    public static final String ERREUR_NON_PREVUE = "Erreur non prévue";

    private String[] profilsActifsJob = new String[] { "prod" };

    private List<String> parametrageBatch;

    /**
     * Le chemin absolu vers un fichier de logs d'erreurs spécifique.
     * 
     * @param cheminAbsoluFichierLogErreurs
     *            le chemin absolu vers le fichier de logs d'erreurs
     */
    @Setter
    @Getter
    private String cheminAbsoluFichierLogErreurs;

    /**
     * Constructeur d'exécuteur de batch.
     * 
     * @param parametrageBatch
     *            les paramètres de lancement du batch sous forme de données textuelles brutes
     */
    protected ExecuteurBatch(String[] parametrageBatch) {
        this.parametrageBatch = Arrays.asList(parametrageBatch);
    }

    /**
     * Récupère les profils actifs du job à exécuter ("prod" par défaut).
     *
     * @return les profils actifs du job à exécuter ("prod" par défaut)
     */
    public String[] getProfilsActifsJob() {
        return Arrays.copyOf(profilsActifsJob, profilsActifsJob.length);
    }

    /**
     * Met à jour les profils actifs du job à exécuter.
     *
     * @param profilsActifsJob
     *            les profils actifs du job à exécuter à mettre à jour
     */
    public void setProfilsActifsJob(String[] profilsActifsJob) {
        Assert.notNull(profilsActifsJob, "La liste des profils actifs ne peut être null");
        this.profilsActifsJob = Arrays.copyOf(profilsActifsJob, profilsActifsJob.length);
    }

    /**
     * Récupère les paramètres de lancement du batch sous forme de données brutes.
     *
     * @return les paramètres de lancement du batch sous forme de données brutes
     */
    protected List<String> getRawBatchParameters() {
        return parametrageBatch;
    }

    /**
     * Lance le job REDAC pré-configuré par cet exécuteur.
     * 
     * @return le code de sortie REDAC en fin de job
     */
    public int executeJob() {
        LOGGER.info("Execution du job batch à partir des paramètres suivants : {}", getRawBatchParameters());

        GenericXmlApplicationContext contexteExecutionJob;
        JobLauncher lanceurJob;
        Job job;
        JobParameters parametrageJob;
        try {
            contexteExecutionJob = getContexteExecutionJob();
            if (!lanceTraitementAvantJob(contexteExecutionJob)) {
                LOGGER.error(MessageFormat.format(RedacMessages.ERREUR_JOB_CODE_RETOUR_40, "L'exécution du job a été annulée."));
                return CodeRetour.ERREUR_FONCTIONNELLE_BLOQUANTE.getCode();
            }

            lanceurJob = contexteExecutionJob.getBean(JobLauncher.class);
            job = getJobCible(contexteExecutionJob);

            parametrageJob = initParametrageJob(contexteExecutionJob);

            // Indication temporelle d'exécution afin de permettre à Spring de
            // relancer job
            // car Spring exige l'unicité des paramètres pour chaque appel d'un
            // job donné
            Map<String, JobParameter> parametresJob = parametrageJob.getParameters();
            parametresJob.put("Timestamp", new JobParameter(String.valueOf(System.currentTimeMillis())));

            parametrageJob = new JobParameters(parametresJob);
            LOGGER.debug("Timestamp d'exécution  : {}", String.valueOf(System.currentTimeMillis()));
        } catch (Exception e) {
            LOGGER.error(MessageFormat.format(RedacMessages.ERREUR_JOB_CODE_RETOUR_80,
                    "Une erreur technique lors de l'initialisation du job a été détectée."), e);
            // Logge le message d'erreur technique non prévue (à l'initialisation Spring) dans le fichier de log fonctionnelle
            traceErreurDansFichierLog(ERREUR_NON_PREVUE);

            return CodeRetour.ERREUR_TECHNIQUE_BLOQUANTE.getCode();
        }

        try {
            LOGGER.info("Exécution du job {} en cours...", job.getName());
            JobExecution executionJob = lanceurJob.run(job, parametrageJob);
            LOGGER.info("Exécution du job {} terminée avec l'identifiant d'exécution {}", job.getName(), executionJob.getId());

            LOGGER.debug("Exécution du traitement post-job {} en cours...", job.getName());
            CodeRetour codeRetourBatch = getCodeRetourExecutionJob(executionJob);
            lanceTraitementFinJob(contexteExecutionJob, codeRetourBatch, executionJob);
            LOGGER.debug("Exécution du traitement post-job {} terminé", job.getName());

            LOGGER.info("Statut du job batch en sortie : code {} - {}", codeRetourBatch.getCode(), executionJob.getExitStatus().getExitDescription());

            // Logge le message d'erreur technique non prévue (SQL, Hibernate) dans le fichier de log fonctionnelle
            if (codeRetourBatch == CodeRetour.ERREUR_TECHNIQUE_BLOQUANTE) {
                traceErreurDansFichierLog(ERREUR_NON_PREVUE);
            }

            return codeRetourBatch.getCode();
        } catch (Exception e) {
            LOGGER.error("Erreur de traitement batch", e);
            // Logge le message d'erreur technique non prévue (pendant le traitement Spring) dans le fichier de log fonctionnelle
            traceErreurDansFichierLog(ERREUR_NON_PREVUE);

            return CodeRetour.ERREUR_TECHNIQUE_BLOQUANTE.getCode();
        } finally {
            if (contexteExecutionJob != null) {
                contexteExecutionJob.close();
            }
        }
    }

    /**
     * Récupère l'emplacement du fichier de configuration Spring du job à exécuter.
     * 
     * @return l'emplacement du fichier de configuration du job à exécuter
     */
    protected abstract String getEmplacementFichierConfigurationJob();

    /**
     * Récupère l'ID du job batch à exécuter dans le fichier de configuration du job.<br/>
     * Peut renvoyer <code>null</code> en toute sécurité si le fichier ne déclare qu'un seul job.<br/>
     * 
     * @return l'ID du job batch dans le fichier de configuration du job, ou null si un unique job est déclaré dans la configuration
     */
    protected abstract String getIdJobCible();

    /**
     * Définit un traitement à lancer avant l'exécution du job.
     * 
     * @param contexteExecutionJob
     *            le contexte d'exécution du job
     * @return true si le job est toujours prêt à être lancé, false pour annuler son lancement
     */
    protected abstract boolean lanceTraitementAvantJob(GenericXmlApplicationContext contexteExecutionJob);

    /**
     * Initialise le paramétrage du job batch à exécuter.
     * 
     * @param contexteExecutionJob
     *            le contexte d'exécution du job
     * 
     * @return le paramétrage initialisé du job à exécuter
     */
    protected abstract JobParameters initParametrageJob(GenericXmlApplicationContext contexteExecutionJob);

    /**
     * Définit un traitement à lancer après l'exécution du job, uniquement s'il a été éxécuté (et non annulé).
     * 
     * @param contexteExecutionJob
     *            le contexte d'exécution du job
     * @param codeRetour
     *            le code de retour du job
     * @param executionJob
     *            l'exécution du job
     */
    protected abstract void lanceTraitementFinJob(GenericXmlApplicationContext contexteExecutionJob, CodeRetour codeRetour,
            JobExecution executionJob);

    /**
     * Vérifie l'existence physique d'une ressource fichier.
     * 
     * @param contexteExecutionJob
     *            le contexte d'exécution du job
     * @param emplacementRessourceFichier
     *            l'emplacement de la ressource à vérifier
     * @return true si la ressource existe, false sinon
     */
    protected boolean verifieExistencePhysiqueRessource(GenericXmlApplicationContext contexteExecutionJob, String emplacementRessourceFichier) {
        Resource ressourceFichier = contexteExecutionJob.getResource(emplacementRessourceFichier);
        return ressourceFichier.exists();
    }

    /**
     * Teste si un fichier est vide.
     * 
     * @param contexteExecutionJob
     *            le contexte d'exécution du job
     * @param fichierEnEntree
     *            l'emplacement du fichier à vérifier
     * @param nbLignesDansFichierLorsqueVide
     *            le nombre de lignes maximum présents dans le fichier pour qu'il soit considéré comme vide
     * @return true si le fichier est vide (ne contient que la ligne d'en-tete, false sinon
     */
    protected boolean testeSiFichierContientDonneesUtiles(GenericXmlApplicationContext contexteExecutionJob, String fichierEnEntree,
            int nbLignesDansFichierLorsqueVide) {
        long nbLignesDansFichier = 0;
        Resource ressourceFichier = contexteExecutionJob.getResource(fichierEnEntree);

        FileInputStream fileInputStreamReader = null;
        InputStreamReader inputStreamReader = null;
        LineNumberReader lineNumberReader = null;
        try {
            fileInputStreamReader = new FileInputStream(ressourceFichier.getFile());
            inputStreamReader = new InputStreamReader(fileInputStreamReader, BatchConfiguration.ENCODAGE_FICHIERS_IMPORTES);
            lineNumberReader = new LineNumberReader(inputStreamReader);
            while (lineNumberReader.getLineNumber() <= nbLignesDansFichierLorsqueVide) {
                String ligneLue = lineNumberReader.readLine();
                if (ligneLue == null) {
                    break;
                } else {
                    nbLignesDansFichier = lineNumberReader.getLineNumber();
                }
            }
        } catch (FileNotFoundException e) {
            throw new RedacUnexpectedException("Erreur lors de la lecture du fichier en entrée - Le fichier n'a pas été trouvé", e);
        } catch (IOException e) {
            throw new RedacUnexpectedException("Erreur lors de la lecture du fichier en entrée", e);
        } finally {
            IOUtils.closeQuietly(fileInputStreamReader);
            IOUtils.closeQuietly(inputStreamReader);
            IOUtils.closeQuietly(lineNumberReader);
        }

        LOGGER.debug("Nombre de lignes comptées dans le fichier : {}", nbLignesDansFichier);
        return nbLignesDansFichier > nbLignesDansFichierLorsqueVide;
    }

    /**
     * Vérifie une extension de fichier.
     * 
     * @param emplacementRessourceFichier
     *            l'emplacement de la ressource du fichier
     * @param extensionsValides
     *            la liste des extensions valides
     * @return true si l'extension de la ressource est valide, false sinon
     */
    protected boolean verifieValiditeExtensionRessource(String emplacementRessourceFichier, List<String> extensionsValides) {
        if (extensionsValides.contains("all")) {
            return true;
        }
        String extension = StringUtils.substringAfterLast(emplacementRessourceFichier, ".");
        return extensionsValides.contains(extension);
    }

    /**
     * Transforme l'emplacement donné pour un fichier en tant qu'emplacement de ressource valide.<br/>
     * Si l'emplacement initial donné ne commence pas par un des mots clés définissant un emplacement de ressource (file:, classpath: ou url:), l'emplacement
     * donné sera considéré comme étant un chemin absolu vers un fichier système.
     * 
     * @param emplacementFichier
     *            l'emplacement du fichier
     * @return l'emplacement de la ressource associée au fichier
     */
    protected String tranformeEmplacementRessourceFichier(String emplacementFichier) {
        if (!StringUtils.startsWithIgnoreCase(emplacementFichier, "classpath:") && !StringUtils.startsWithIgnoreCase(emplacementFichier, "file:")
                && !StringUtils.startsWithIgnoreCase(emplacementFichier, "url:")) {
            // Chemin système par défaut
            return "file:" + emplacementFichier;
        } else {
            // Emplacement valide pour une ressource
            return emplacementFichier;
        }
    }

    /**
     * Sauvegarde un fichier dans le référentiel des fichiers batchs.
     * 
     * @param contexteExecutionJob
     *            le contexte d'exécution du job
     * @param executionJob
     *            l'exécution du job
     * @param cheminRessourceFichierBatch
     *            le chemin (ressource préfixée file:, classpath: ou url:) du fichier batch
     * @return le fichier batch importé
     */
    protected File sauvegardeFichierImporte(GenericXmlApplicationContext contexteExecutionJob, JobExecution executionJob,
            String cheminRessourceFichierBatch) {
        InputStream fluxContenuFichier = null;
        try {
            File fichierBatch = contexteExecutionJob.getResource(cheminRessourceFichierBatch).getFile();
            String nomFichier = fichierBatch.getName();
            int tailleFichier = (int) fichierBatch.length();
            fluxContenuFichier = new FileInputStream(fichierBatch);
            getFichierBatchRepository(contexteExecutionJob).sauvegardeFichierImporte(executionJob.getId(), nomFichier, fluxContenuFichier,
                    tailleFichier);
            return fichierBatch;
        } catch (IOException e) {
            throw new RedacUnexpectedException(e);
        } finally {
            IOUtils.closeQuietly(fluxContenuFichier);
        }
    }

    /**
     * Récupère le référentiel des fichiers batchs sauvegardés pour les jobs d'import.
     * 
     * @param contexteExecutionJob
     *            le contexte d'exécution du job
     * @return le référentiel des fichiers batchs
     */
    protected FichierBatchRepository getFichierBatchRepository(GenericXmlApplicationContext contexteExecutionJob) {
        return contexteExecutionJob.getBean("fichierBatchRepository", FichierBatchRepository.class);
    }

    /**
     * Récupère le contexte d'exécution du job à exécuter, incluant au moins un moteur de batch REDAC configuré et la définition du job.
     * 
     * @return le contexte d'exécution du job à exécuter
     */
    protected GenericXmlApplicationContext getContexteExecutionJob() {
        GenericXmlApplicationContext contexteExecutionJob = new GenericXmlApplicationContext();
        contexteExecutionJob.getEnvironment().setActiveProfiles(getProfilsActifsJob());
        contexteExecutionJob.load(EMPLACEMENT_CONFIGURATION_MOTEUR_BATCH, getEmplacementFichierConfigurationJob());
        contexteExecutionJob.refresh();
        return contexteExecutionJob;
    }

    /**
     * Récupération du job ciblé par cet exécuteur.
     * 
     * @param contexteExecutionJob
     *            le contexte d'exécution du job
     * @return le job ciblé par cet exécuteur
     */
    private Job getJobCible(GenericXmlApplicationContext contexteExecutionJob) {
        String idJobCible = getIdJobCible();
        if (idJobCible != null) {
            LOGGER.debug("Résolution du job [id={}] défini dans la configuration donnée", idJobCible);
            return contexteExecutionJob.getBean(idJobCible, Job.class);
        } else {
            LOGGER.debug("Résolution du job unique défini dans la configuration donnée");
            return contexteExecutionJob.getBean(Job.class);
        }
    }

    /**
     * Récupère le code retour contenu dans l'exécution d'un job.
     * 
     * @param executionJob
     *            l'exécution du job
     * @return le code retour de l'exécution
     */
    private CodeRetour getCodeRetourExecutionJob(JobExecution executionJob) {
        String codeRetourDansStatut = executionJob.getExitStatus().getExitCode();
        if (!NumberUtils.isNumber(codeRetourDansStatut)) {
            throw new IllegalStateException("Le job ne semble pas bien configuré - il doit impérativement étendre le job REDAC par défaut");
        }
        return CodeRetour.fromCode(Integer.parseInt(codeRetourDansStatut));
    }

    /**
     * Vide l'ensemble des référentiels ciblés par l'import.
     * 
     * @param contexteExecutionJob
     *            le contexte d'exécution du job
     * @param classesRepositories
     *            interfaces des référentiels aux domaines temporaires à nettoyer
     */
    protected void videEspacesTemporaires(GenericXmlApplicationContext contexteExecutionJob,
            List<Class<? extends EntiteImportableRepository<? extends EntiteImportableBatch>>> classesRepositories) {
        // Vidage des référentiels à réimporter
        TransactionStatus txStatus = commenceTransaction(contexteExecutionJob);
        try {
            for (Class<? extends EntiteImportableRepository<?>> classeRepository : classesRepositories) {
                EntiteImportableRepository<?> repository = contexteExecutionJob.getBean(classeRepository);
                repository.nettoieEntitesTemporaires();
            }
            termineTransaction(contexteExecutionJob, txStatus);
        } catch (Exception e) {
            LOGGER.error("Une erreur est survenue pendant le vidage des référentiels", e);
            rollbackTransaction(contexteExecutionJob, txStatus);
        }
    }

    /**
     * Vide un référentiel temporaire ciblé par l'import.
     * 
     * @param contexteExecutionJob
     *            le contexte d'exécution du job
     * @param classeRepository
     *            interface du référentiel du domaine temporaire à nettoyer
     */
    protected void videEspacesTemporaires(GenericXmlApplicationContext contexteExecutionJob,
            Class<? extends EntiteImportableRepository<? extends EntiteImportableBatch>> classeRepository) {
        // Vidage du référentiel à réimporter
        TransactionStatus txStatus = commenceTransaction(contexteExecutionJob);
        try {
            EntiteImportableRepository<?> repository = contexteExecutionJob.getBean(classeRepository);
            repository.nettoieEntitesTemporaires();
            termineTransaction(contexteExecutionJob, txStatus);
        } catch (Exception e) {
            LOGGER.error("Une erreur est survenue pendant le vidage du référentiel", e);
            rollbackTransaction(contexteExecutionJob, txStatus);
        }
    }

    /**
     * Nettoire une liste de tables de travail.
     * 
     * @param contexteExecutionJob
     *            le contexte d'exécution du job
     * @param classesRepositories
     *            les classes des référentiels donnant accès aux tables de travail
     */
    protected void videTablesDeTravail(GenericXmlApplicationContext contexteExecutionJob,
            List<Class<? extends JpaTempRepository<? extends Entite>>> classesRepositories) {
        TransactionStatus txStatus = commenceTransaction(contexteExecutionJob);
        for (Class<? extends JpaTempRepository<?>> classeRepository : classesRepositories) {
            try {
                JpaTempRepository<?> repository = contexteExecutionJob.getBean(classeRepository);
                repository.nettoieEntites();
            } catch (Exception e) {
                LOGGER.error("Une erreur est survenue pendant le nettoyage d'un table de travail", e);
                rollbackTransaction(contexteExecutionJob, txStatus);
            }
        }
        termineTransaction(contexteExecutionJob, txStatus);
    }

    /**
     * Initialise une transaction à partir du contexte d'exécution d'un job.
     * 
     * @param contexteExecutionJob
     *            le contexte d'exécution de job
     * @return le statut de la transaction initialisée
     */
    protected TransactionStatus commenceTransaction(GenericXmlApplicationContext contexteExecutionJob) {
        PlatformTransactionManager txManager = contexteExecutionJob.getBean(PlatformTransactionManager.class);
        return txManager.getTransaction(new DefaultTransactionDefinition());
    }

    /**
     * Termine une transaction initialisée à partir du contexte d'exécution d'un job.
     * 
     * @param contexteExecutionJob
     *            le contexte d'exécution de job
     * @param txStatus
     *            le statut de la transaction à terminer
     */
    protected void termineTransaction(GenericXmlApplicationContext contexteExecutionJob, TransactionStatus txStatus) {
        PlatformTransactionManager txManager = contexteExecutionJob.getBean(PlatformTransactionManager.class);
        txManager.commit(txStatus);
    }

    /**
     * Annule une transaction initialisée à partir du contexte d'exécution d'un job.
     * 
     * @param contexteExecutionJob
     *            le contexte d'exécution de job
     * @param txStatus
     *            le statut de la transaction à annuler
     */
    protected void rollbackTransaction(GenericXmlApplicationContext contexteExecutionJob, TransactionStatus txStatus) {
        PlatformTransactionManager txManager = contexteExecutionJob.getBean(PlatformTransactionManager.class);
        txManager.rollback(txStatus);
    }

    /**
     * Logge une ligne d'erreur dans un fichier de log d'erreurs.
     * 
     * @param messageErreur
     *            le message d'erreur à logger
     */
    protected void traceErreurDansFichierLog(String messageErreur) {
        traceDansFichierLog(messageErreur, true);
    }

    /**
     * Logge une ligne d'erreur dans un fichier de log d'erreurs pour chaque élément de la liste.
     * 
     * @param listeErreurs
     *            une liste d'erreurs à logger
     */
    protected void traceErreurDansFichierLog(List<String> listeErreurs) {
        for (String erreur : listeErreurs) {
            traceErreurDansFichierLog(erreur);
        }

    }

    /**
     * Logge une ligne d'information dans un fichier de log d'erreurs en cas de traitement réussi.
     * 
     * @param messageOK
     *            le message de traitement OK
     */
    protected void traceTraitementOKDansFichierLog(String messageOK) {
        traceDansFichierLog(messageOK, false);
    }

    /**
     * Logge une ligne d'information dans un fichier de log d'erreur.
     * 
     * @param message
     *            le message à logger
     * @param erreur
     *            si la ligne correspond à une erreur
     */
    private void traceDansFichierLog(String message, boolean erreur) {
        if (erreur) {
            LOGGER.error(message);
        } else {
            LOGGER.info(message);
        }

        SimpleDateFormat formateurDate = new SimpleDateFormat("yyyyMMdd HH:mm:ss");
        String date = formateurDate.format(Calendar.getInstance().getTime());

        if (StringUtils.isBlank(this.cheminAbsoluFichierLogErreurs)) {
            LOGGER.warn("Aucun chemin de fichier de logs d'erreur spécifié");
            return;
        }

        PrintWriter redacteurFichier = null;
        try {
            File fichierLogErreurs = new File(this.cheminAbsoluFichierLogErreurs);

            if (fichierLogErreurs.getParentFile() != null && !fichierLogErreurs.getParentFile().exists()) {
                boolean repertoireParentCree = fichierLogErreurs.getParentFile().mkdirs();
                if (!repertoireParentCree) {
                    throw new RedacUnexpectedException("Impossible de créer le répertoire des logs d'erreurs");
                }
            }

            if (!fichierLogErreurs.exists()) {
                boolean fichierCree = fichierLogErreurs.createNewFile();
                if (!fichierCree) {
                    LOGGER.error("Impossible de créer le fichier de log d'erreurs");
                    return;
                }
            }
            FileOutputStream fluxSortantFichier = new FileOutputStream(fichierLogErreurs, true);
            OutputStreamWriter redacteurFluxSortantFichier = new OutputStreamWriter(fluxSortantFichier, BatchConfiguration.ENCODAGE_FICHIERS_LOGS);
            redacteurFichier = new PrintWriter(redacteurFluxSortantFichier, false);
            redacteurFichier.write(date + " " + message);
            redacteurFichier.println();
        } catch (Exception e) {
            LOGGER.error("Erreur lors du log d'une erreur", e);
            return;
        } finally {
            if (redacteurFichier != null) {
                redacteurFichier.flush();
            }
            IOUtils.closeQuietly(redacteurFichier);
        }
    }
}
