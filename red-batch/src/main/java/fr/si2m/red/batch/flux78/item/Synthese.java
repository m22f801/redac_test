package fr.si2m.red.batch.flux78.item;

import java.text.MessageFormat;

import lombok.Data;
import lombok.EqualsAndHashCode;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.si2m.red.batch.EntiteImportableBatchNonPersistee;
import fr.si2m.red.batch.RedacMessages;

/**
 * Ligne de synthèse présente dans un compte-rendu de traitement par le back-office.
 * 
 * @author poidij
 *
 */
@Data
@EqualsAndHashCode(callSuper = false, of = { "typeEnregistrement", "typeFlux", "emetteurCR", "dateTraitement" })
public class Synthese extends EntiteImportableBatchNonPersistee {
    /**
     * UID de version.
     */
    private static final long serialVersionUID = 1L;

    private static final Logger LOGGER = LoggerFactory.getLogger(Synthese.class);

    /**
     * Le type d'enregistrement.
     * 
     * @param typeEnregistrement
     *            le type d'enregistrement
     * @return le type d'enregistrement
     * 
     */
    private String typeEnregistrement;
    private String emetteurCR;
    private String typeFlux;
    private String dateTraitement;
    private String compteRenduGlobal;
    private String nbEnregistrements2;
    private String nbEnregistrements3;
    private String messageSynthese;
    private String filler;
    private String applicationConcernee;

    /**
     * EmetteurCR.
     * 
     * @return l'emetteur de compte rendu
     */
    public Integer getEmetteurCRAsInteger() {
        Integer intValue = null;
        try {
            intValue = Integer.valueOf(getEmetteurCR());
        } catch (Exception e) {
            LOGGER.warn(MessageFormat.format(RedacMessages.ERREUR_VALEUR_CONVERSION_ENTIER, "EmetteurCR", getEmetteurCR()), e);
        }
        return intValue;
    }

    /**
     * Type de flux.
     * 
     * @return le type de flux
     */
    public Integer getTypeFluxAsInteger() {
        Integer intValue = null;
        try {
            intValue = Integer.valueOf(getTypeFlux());
        } catch (Exception e) {
            LOGGER.warn(MessageFormat.format(RedacMessages.ERREUR_VALEUR_CONVERSION_ENTIER, "TypeFlux", getTypeFlux()), e);
        }
        return intValue;
    }

    /**
     * Date de traitement.
     * 
     * @return la date de traitement
     */
    public Integer getDateTraitementAsInteger() {
        Integer intValue = null;
        try {
            intValue = Integer.valueOf(getDateTraitement());
        } catch (Exception e) {
            LOGGER.warn(MessageFormat.format(RedacMessages.ERREUR_VALEUR_CONVERSION_ENTIER, "DateTraitement", getDateTraitement()), e);
        }
        return intValue;
    }

    /**
     * Compte rendu global.
     * 
     * @return le compte rendu global
     */
    public Integer getCompteRenduGlobalAsInteger() {
        Integer intValue = null;
        try {
            intValue = Integer.valueOf(getCompteRenduGlobal());
        } catch (Exception e) {
            LOGGER.warn(MessageFormat.format(RedacMessages.ERREUR_VALEUR_CONVERSION_ENTIER, "CompteRenduGlobal", getCompteRenduGlobal()), e);
        }
        return intValue;
    }

    /**
     * Nombre d'enregistrements de type 2.
     * 
     * @return le nombre d'enregistrements de type 2
     */
    public Integer getNbEnregistrements2AsInteger() {
        Integer intValue = null;
        try {
            intValue = Integer.valueOf(getNbEnregistrements2().trim());
        } catch (Exception e) {
            LOGGER.warn(MessageFormat.format(RedacMessages.ERREUR_VALEUR_CONVERSION_ENTIER, "NbEnregistrements2", getNbEnregistrements2()), e);
        }
        return intValue;
    }

    /**
     * Nombre d'enregistrements de type 3.
     * 
     * @return le nombre d'enregistrements de type 3
     */
    public Integer getNbEnregistrements3AsInteger() {
        Integer intValue = null;
        try {
            intValue = Integer.valueOf(getNbEnregistrements3().trim());
        } catch (Exception e) {
            LOGGER.warn(MessageFormat.format(RedacMessages.ERREUR_VALEUR_CONVERSION_ENTIER, "NbEnregistrements3", getNbEnregistrements3()), e);
        }
        return intValue;
    }

}
