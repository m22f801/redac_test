package fr.si2m.red.batch.flux3.item;

import fr.si2m.red.batch.flux3.ligne.ContratDelegatairePrestation;
import fr.si2m.red.batch.flux3.ligne.SituationContrat;
import fr.si2m.red.batch.moteur.item.TransformateurDonnee;

/**
 * Mapper des entités ContratDelegatairePrestation à partir de la SituationContrat fournie.
 * 
 * cf F03_RG_S10
 * 
 * @author poidij
 *
 */
public class TransformateurContratDelegatairePrestation extends TransformateurDonnee<SituationContrat, ContratDelegatairePrestation> {

    @Override
    public ContratDelegatairePrestation process(SituationContrat situationContrat) throws Exception {
        ContratDelegatairePrestation contratDelegatairePrestation = new ContratDelegatairePrestation();

        contratDelegatairePrestation.setCodeMiseAJour("R");
        contratDelegatairePrestation.setIdentifiantEntrepriseG3C("");
        contratDelegatairePrestation.setNumSequenceContratG3C("");
        contratDelegatairePrestation.setCodeRisque("TR");
        contratDelegatairePrestation.setCodeOrganismeDelegatairePrestationG3C("");
        contratDelegatairePrestation.setDateDebutValidite(situationContrat.getDateDebutSituation());
        contratDelegatairePrestation.setDateFinValidite(situationContrat.getDateFinSituation());
        contratDelegatairePrestation.setIdentifiantTechniqueEntreprise("");
        contratDelegatairePrestation.setIdentifiantTechniqueContrat(situationContrat.getNumContrat());
        contratDelegatairePrestation.setIdentifiantTechniqueDelegataire(situationContrat.getCodeIntermediaire());

        return contratDelegatairePrestation;
    }

}
