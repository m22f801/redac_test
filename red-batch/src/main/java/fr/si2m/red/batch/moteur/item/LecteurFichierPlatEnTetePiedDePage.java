package fr.si2m.red.batch.moteur.item;

import java.io.BufferedReader;
import java.lang.reflect.Field;

import org.apache.commons.lang3.StringUtils;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.annotation.Autowired;

import fr.si2m.red.EntiteImportableBatch;
import fr.si2m.red.RedacUnexpectedException;
import fr.si2m.red.batch.moteur.support.LecteurLigne;

/**
 * Lecteur de fichiers plats pour les batchs d'import REDAC permettant un mapping direct des champs vers les propriétés d'une entité "prototype". <br/>
 * <br/>
 * Une ligne de pied de page pour les fichiers importés commence systématiquement par le
 * 
 * @author poidij
 *
 * @param <T>
 *            le type des lignes lues
 */
public class LecteurFichierPlatEnTetePiedDePage<T extends EntiteImportableBatch> extends LecteurFichierPlat<T> {

    /**
     * Le préfixe de ligne permettant la détection d'un pied de page.
     */
    public static final String PREFIXE_PIED_DE_PAGE = "9999999999|";

    /**
     * Le nombre maximum de caractères sur une ligne des fichiers du flux 4 (pour le fichier des Adhésions : 1405).
     */
    public static final int LONGUEUR_MAX_LIGNES_FLUX4 = 1500;

    /**
     * Constructeur de lecteur de fichier plat avec entête et pied de page.
     * 
     * @param nomsChamps
     *            les noms des champs à lire
     * @param nomPrototypeLigne
     *            le nom du prototype d'une ligne dans l'usine de beans
     * @param beanFactory
     *            l'usine de bean
     * @param delimiteurSpecifique
     *            le délimiteur de champs spécifique
     */
    @Autowired
    public LecteurFichierPlatEnTetePiedDePage(String[] nomsChamps, String nomPrototypeLigne, BeanFactory beanFactory, String delimiteurSpecifique) {
        super(nomsChamps, nomPrototypeLigne, beanFactory, delimiteurSpecifique);
        getMapperLigne().setFieldSetMapper(new LecteurLigne<T>(beanFactory, nomPrototypeLigne));
        setLinesToSkip(1);
    }

    @Override
    protected T doRead() throws Exception {

        BufferedReader reader = getReader();
        // Pose une marque pour retour ultérieur
        reader.mark(LONGUEUR_MAX_LIGNES_FLUX4);
        String line = reader.readLine();

        if (!StringUtils.startsWith(line, PREFIXE_PIED_DE_PAGE)) {
            // Cette ligne n'est pas le pied de page
            // Retour à la marque posée précédemment
            reader.reset();
            return super.doRead();
        }
        return null;
    }

    /**
     * Récupère le lecteur de ligne.
     * 
     * @return le lecteur de ligne
     */
    protected BufferedReader getReader() {
        try {
            Field readerField = FlatFileItemReader.class.getDeclaredField("reader");
            readerField.setAccessible(true);
            return (BufferedReader) readerField.get(this);
        } catch (NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException e) {
            throw new RedacUnexpectedException(e);
        }
    }

}
