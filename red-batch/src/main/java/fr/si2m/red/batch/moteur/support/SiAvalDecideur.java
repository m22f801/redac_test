/**
 * 
 */
package fr.si2m.red.batch.moteur.support;

import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.job.flow.FlowExecutionStatus;
import org.springframework.batch.core.job.flow.JobExecutionDecider;

/**
 * Choix de la prochaine étape à exécuter selon le SI_AVAL
 * 
 * @author eudesr
 *
 */
public class SiAvalDecideur implements JobExecutionDecider {

    /**
     * Le SI AVAL cible
     */
    private String siAval;

    @Override
    public FlowExecutionStatus decide(JobExecution jobExecution, StepExecution stepExecution) {

        this.siAval = jobExecution.getJobParameters().getString("siAval");

        if ("GERD".equals(siAval)) {
            return new FlowExecutionStatus("SI_GERD");
        } else if ("WQUI".equals(siAval)) {
            return new FlowExecutionStatus("SI_WQUI");
        } else if ("EGER".equals(siAval)) {
            return new FlowExecutionStatus("SI_EGER");
        } else {
            // si le SI_AVAL est différents des SI connus, alors on termine le batch
            return new FlowExecutionStatus("SI_INCONNU");
        }

    }

}
