package fr.si2m.red.batch.flux4.item;

import lombok.Setter;

import org.apache.commons.lang3.StringUtils;

import fr.si2m.red.DateRedac;
import fr.si2m.red.batch.RedacMessages;
import fr.si2m.red.batch.moteur.erreur.ValidateurLigneAvecCollecte;
import fr.si2m.red.dsn.AdhesionEtablissementMoisRepository;
import fr.si2m.red.dsn.Versement;
import fr.si2m.red.dsn.VersementRepository;

/**
 * Validateur de ligne de paramétrage code libellé.
 * 
 * @author benitahy
 *
 */
public class VersementValidateurLigne extends ValidateurLigneAvecCollecte<Versement> {

    @Setter
    private AdhesionEtablissementMoisRepository adhesionEtablissementMoisRepository;

    @Setter
    private VersementRepository versementRepository;

    @Override
    protected void valide(final Versement versement) {

        // unicité
        valideChampAvecCollecte(!versementRepository.existeUnVersement(versement.getIdVersement()), versement, "ID", "La ligne ne respecte pas la règle d'unicité");

        // Champs obligatoires
        valideChampAvecCollecte(StringUtils.isNotBlank(versement.getIdVersement()), versement, "ID_VERSEMENT", "Le champ ID_VERSEMENT obligatoire n'est pas renseigné");
        valideChampAvecCollecte(StringUtils.isNotBlank(versement.getIdAdhEtabMois()), versement, "ID_ADH_ETAB_MOIS", "Le champ ID_ADH_ETAB_MOIS obligatoire n'est pas renseigné");
        valideChampAvecCollecte(StringUtils.isNotBlank(versement.getIdentifiantOrganismeProtectionSociale()), versement, "ID_OPS",
                "Le champ ID_OPS obligatoire n'est pas renseigné");

        // Longueur des champs
        valideTailleFixeChampAvecCollecte(versement.getIdVersement(), 30, versement, "ID_VERSEMENT");
        valideTailleFixeChampAvecCollecte(versement.getIdAdhEtabMois(), 30, versement, "ID_ADH_ETAB_MOIS");
        valideTailleFixeChampAvecCollecte(versement.getIdentifiantOrganismeProtectionSociale(), 14, versement, "ID_OPS");
        valideTailleFixeChampAvecCollecte(versement.getEntiteAffectation(), 14, versement, "ENTITE_AFFECTATION");
        valideTailleFixeChampAvecCollecte(versement.getBic(), 11, versement, "BIC");
        valideTailleFixeChampAvecCollecte(versement.getIban(), 34, versement, "IBAN");
        valideChampNumeriqueDecimaleTailleFixeAvecCollecte(versement.getMontantVersementAsText(), 16, 2, versement, "MONTANT_VERSEMENT");
        valideTailleFixeChampAvecCollecte(versement.getCodeDelegataireGestion(), 6, versement, "COT_DELEG_COT_VRS");
        valideTailleFixeChampAvecCollecte(versement.getModePaiement(), 2, versement, "MODE_PAIEMENT");
        valideChampDateTailleFixeAvecCollecte(versement.getDatePaiementAsText(), DateRedac.FORMAT_DATES_DDMMYYYY, versement, "DATE_PAIEMENT", false);
        valideTailleFixeChampAvecCollecte(versement.getSiretPayeur(), 14, versement, "SIRET_PAYEUR");
        valideTailleFixeChampAvecCollecte(versement.getCodeIdentifiantFonds(), 50, versement, "CODE_ID_FONDS");
        valideTailleFixeChampAvecCollecte(versement.getReferencePaiement(), 80, versement, "REF_PAIEMENT");

        validationCorrespondanceReferentielle(versement);
    }

    /**
     * Validation des liens entre tables.
     * 
     * @param versement
     *            Le versement dont l'Adhésion doit être validée.
     */
    private void validationCorrespondanceReferentielle(final Versement versement) {
        if (StringUtils.isNotBlank(versement.getIdAdhEtabMois())) {
            boolean existeUneAdhesion = adhesionEtablissementMoisRepository.existeUneAdhesion(versement.getIdAdhEtabMois());
            valideChampAvecCollecte(existeUneAdhesion, versement, "IdAdhEtabMois", RedacMessages.ERREUR_REFERENCE_FLUX4);
        }
    }
}
