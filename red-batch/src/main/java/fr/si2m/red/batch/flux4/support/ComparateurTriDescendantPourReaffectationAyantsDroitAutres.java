package fr.si2m.red.batch.flux4.support;

import java.io.Serializable;
import java.util.Comparator;

/**
 * Comparateur spécifique pour les tris descendants nécessaire à l'application de l'algorithme de réaffectation des ayants droit autres.
 * 
 * @author nortaina
 *
 */
public class ComparateurTriDescendantPourReaffectationAyantsDroitAutres implements Comparator<EffectifCategorieContratTemp>, Serializable {

    /**
     * UID de version.
     */
    private static final long serialVersionUID = 6688277177181688710L;

    @Override
    public int compare(EffectifCategorieContratTemp effectif1, EffectifCategorieContratTemp effectif2) {
        // Tri descendant : on inverse 2 et 1 dans les comparaisons
        Integer diffProrite = effectif2.getPrioriteAyantsDroitAutres() - effectif1.getPrioriteAyantsDroitAutres();
        if (diffProrite != 0) {
            return diffProrite;
        }
        Integer diffSeuil = effectif2.getSeuilAyantsDroitAutres() - effectif1.getSeuilAyantsDroitAutres();
        if (diffSeuil != 0) {
            return diffSeuil;
        }
        Integer diffIndicateur = effectif2.getIndicateurAffiliation() - effectif1.getIndicateurAffiliation();
        if (diffIndicateur != 0) {
            return diffIndicateur;
        }
        return effectif2.getNumCategorieQuittancement().compareTo(effectif1.getNumCategorieQuittancement());
    }

}
