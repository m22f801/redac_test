package fr.si2m.red.batch.flux78.item;

import java.text.MessageFormat;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.si2m.red.batch.EntiteImportableBatchNonPersistee;
import fr.si2m.red.batch.RedacMessages;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * Message reçu dans un compte-rendu de traitement.
 * 
 * @author poidij
 *
 */
@Data
@EqualsAndHashCode(callSuper = false, of = { "typeEnregistrement", "contratZoneA", "contratZoneB", "contratZoneC", "dateDebutPeriode",
        "codeNatureMessage" })
public class MessageAgrandies extends EntiteImportableBatchNonPersistee {
    /**
     * 
     */
    private static final long serialVersionUID = 6630402790332748025L;

    /**
     * UID de version.
     */

    private static final Logger LOGGER = LoggerFactory.getLogger(MessageAgrandies.class);

    /**
     * Le type d'enregistrement.
     * 
     * @param typeEnregistrement
     *            le type d'enregistrement
     * @return le type d'enregistrement
     * 
     */
    private String typeEnregistrement;
    private String contratZoneA;
    private String contratZoneB;
    private String contratZoneC;
    private String dateDebutPeriode;
    private String dateFinPeriode;
    private String codeNatureMessage;
    private String individu;
    private String codeMessage;
    private String libelleMessage;
    private String filler;
    private String application;
    private Integer dateTraitementSynthese;

    /**
     * Date de début de période.
     * 
     * @return la date de début de période
     */
    public Integer getDateDebutPeriodeAsInteger() {
        Integer intValue = null;
        try {
            intValue = Integer.valueOf(getDateDebutPeriode());
        } catch (Exception e) {
            LOGGER.warn(MessageFormat.format(RedacMessages.ERREUR_VALEUR_CONVERSION_ENTIER, "DateDebutPeriode", getDateDebutPeriode()), e);
        }
        return intValue;
    }

    /**
     * Date de fin de période.
     * 
     * @return la date de fin de période
     */
    public Integer getDateFinPeriodeAsInteger() {
        Integer intValue = null;
        try {
            intValue = Integer.valueOf(getDateFinPeriode());
        } catch (Exception e) {
            LOGGER.warn(MessageFormat.format(RedacMessages.ERREUR_VALEUR_CONVERSION_ENTIER, "DateFinPeriode", getDateFinPeriode()), e);
        }
        return intValue;
    }

    /**
     * Code nature du message.
     * 
     * @return le code nature du message
     */
    public Integer getCodeNatureMessageAsInteger() {
        Integer intValue = null;
        try {
            intValue = Integer.valueOf(getCodeNatureMessage());
        } catch (Exception e) {
            LOGGER.warn(MessageFormat.format(RedacMessages.ERREUR_VALEUR_CONVERSION_ENTIER, "CodeNatureMessage", getCodeNatureMessage()), e);
        }
        return intValue;
    }

    /**
     * Code message.
     * 
     * @return le code message
     */
    public Integer getCodeMessageAsInteger() {
        Integer intValue = null;
        try {
            intValue = Integer.valueOf(getCodeMessage());
        } catch (Exception e) {
            LOGGER.warn(MessageFormat.format(RedacMessages.ERREUR_VALEUR_CONVERSION_ENTIER, "CodeMessage", getCodeMessage()), e);
        }
        return intValue;
    }

}
