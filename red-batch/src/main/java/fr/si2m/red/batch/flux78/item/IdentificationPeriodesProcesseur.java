package fr.si2m.red.batch.flux78.item;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

import fr.si2m.red.DateRedac;
import fr.si2m.red.batch.moteur.item.TransformateurDonnee;
import fr.si2m.red.dsn.BaseAssujettieRepository;
import fr.si2m.red.dsn.EtatPeriode;
import fr.si2m.red.reconciliation.CategorieQuittancementIndividuRepository;
import fr.si2m.red.reconciliation.CompteRenduIntegrationRepository;
import fr.si2m.red.reconciliation.HistoriqueEtatPeriode;
import fr.si2m.red.reconciliation.IndicateursDSNPeriode;
import fr.si2m.red.reconciliation.IndicateursDSNPeriodeRepository;
import fr.si2m.red.reconciliation.MessageControleRepository;
import fr.si2m.red.reconciliation.PeriodeRecue;
import fr.si2m.red.reconciliation.PeriodeRecueRepository;
import lombok.Setter;

/**
 * Création d'indicateurs DSN de période à partir de contrats.
 * 
 * @author poidij
 *
 */
public class IdentificationPeriodesProcesseur extends TransformateurDonnee<PeriodeRecue, List<HistoriqueEtatPeriode>> {

    private static final String LIBELLE_IDENTIFIANT_BATCH_701 = "Batch d’envoi vers Back-Office";
    private static final String PREFIXE_COMMENTAIRE_UTILISATEUR_BATCH_701 = "Critères d’envoi de la période au Back-Office non respectés : ";

    @Autowired
    private PeriodeRecueRepository periodeRecueRepository;

    @Autowired
    private IndicateursDSNPeriodeRepository indicateursDSNPeriodeRepository;

    @Autowired
    private CompteRenduIntegrationRepository compteRenduIntegrationRepository;

    @Autowired
    private MessageControleRepository messageControleRepository;

    @Autowired
    private BaseAssujettieRepository baseAssujettieRepository;

    @Autowired
    private CategorieQuittancementIndividuRepository categorieQuittancementIndividuRepository;

    @Setter
    private String nomBatch;

    @Override
    public List<HistoriqueEtatPeriode> process(PeriodeRecue periode) throws Exception {

        // sauvegarde de l'état initial
        EtatPeriode etatInitial = periode.getEtatPeriode();

        // F78_RG_P1_02

        // Modification de la période
        periode.setEtatPeriode(EtatPeriode.ENC);
        periodeRecueRepository.modifieEntiteExistante(periode);

        List<HistoriqueEtatPeriode> listeHistoriqueEtatPeriodes = new ArrayList<HistoriqueEtatPeriode>();

        // Création de l'historique période
        HistoriqueEtatPeriode historiqueENC = creerHistorique(periode, null, null, null);
        listeHistoriqueEtatPeriodes.add(historiqueENC);

        // Recherche et test de l'indicateur
        IndicateursDSNPeriode indicateursDSNPeriode = indicateursDSNPeriodeRepository.getIndicateursPourPeriode(periode.getNumeroContrat(),
                periode.getDateDebutPeriode(), periode.getDateFinPeriode());

        // F78_RG_P1_04
        List<String> listeModeGestion = Arrays.asList("O", "C");

        boolean existeIndicateurDSNPeriode = indicateursDSNPeriode != null;
        boolean indicateurDSNTransfert = existeIndicateurDSNPeriode ? indicateursDSNPeriode.isTransfertDSN() : false;
        boolean indicateurDSNModeGestion = existeIndicateurDSNPeriode ? listeModeGestion.contains(indicateursDSNPeriode.getModeGestionDSN()) : false;
        boolean existeMessagesControle = messageControleRepository.existeMessageNiveauRejet(periode.getIdPeriode());

        if (!(existeIndicateurDSNPeriode && indicateurDSNTransfert
                && ((indicateurDSNModeGestion && !existeMessagesControle) || "ARI".equals(etatInitial.getCodeEtat())))) {
            // Modification de la période
            periode.setEtatPeriode(EtatPeriode.NIN);
            periodeRecueRepository.modifieEntiteExistante(periode);

            // F78_RG_P1_05 , F78_RG_P1_06 , F78_RG_P1_07
            identificationConditionEnErreur(existeIndicateurDSNPeriode, indicateurDSNTransfert, indicateurDSNModeGestion, existeMessagesControle,
                    periode, listeHistoriqueEtatPeriodes);

        }

        if (!baseAssujettieRepository.existeBaseAssujettiePourPeriode(periode.getIdPeriode())) {
            // Modification de la période
            periode.setEtatPeriode(EtatPeriode.NIN);
            periodeRecueRepository.modifieEntiteExistante(periode);

            // Création de l'historique période
            HistoriqueEtatPeriode histo = creerHistorique(periode, LIBELLE_IDENTIFIANT_BATCH_701,
                    PREFIXE_COMMENTAIRE_UTILISATEUR_BATCH_701 + "Aucune base assujettie ni cotisations dans la période.", EtatPeriode.NIN);
            // on décalle de 4 secondes pour éviter doublon si la F78_RG_P1_04 génère un enregistrement
            histo.setDateHeureChangement(histo.getDateHeureChangement() + 4);
            listeHistoriqueEtatPeriodes.add(histo);
        } else if (!categorieQuittancementIndividuRepository.existeCategorieQuittancementIndividuPourPeriode(periode.getIdPeriode())) {
            // Modification de la période
            periode.setEtatPeriode(EtatPeriode.NIN);
            periodeRecueRepository.modifieEntiteExistante(periode);

            // Création de l'historique période
            HistoriqueEtatPeriode histo = creerHistorique(periode, LIBELLE_IDENTIFIANT_BATCH_701,
                    PREFIXE_COMMENTAIRE_UTILISATEUR_BATCH_701 + "Consolidation de la période en erreur !", EtatPeriode.NIN);

            // on décalle de 5 secondes pour éviter doublon si la F78_RG_P1_04 génère un enregistrement
            histo.setDateHeureChangement(histo.getDateHeureChangement() + 5);
            listeHistoriqueEtatPeriodes.add(histo);
        }

        if (periode.getEtatPeriode() == EtatPeriode.ENC) {
            // F78_RG_P1_03

            // Suppression des compte rendus
            compteRenduIntegrationRepository.supprimeCompteRenduIntegrationPourPeriode(periode.getIdPeriode());

            // Suppression
            messageControleRepository.supprimeMessagesSIAvalPourPeriode(periode.getIdPeriode());
        }

        return listeHistoriqueEtatPeriodes;
    }

    /**
     * Crée une entité HistoriqueEtatPeriode pour une période reçue donnée.
     * 
     * @param periode
     *            la période reçue
     * @param identifiantUtilisateur
     *            l'identifiant utilisateur
     * @param commentaireUtilisateur
     *            le commentaire utilisateur
     * @return la ligne HistoriqueEtatPeriode
     */
    private HistoriqueEtatPeriode creerHistorique(PeriodeRecue periode, String identifiantUtilisateur, String commentaireUtilisateur,
            EtatPeriode etatPeriode) {
        HistoriqueEtatPeriode historique = new HistoriqueEtatPeriode();
        historique.setIdPeriode(periode.getIdPeriode());
        historique.setDateHeureChangement(DateRedac.convertitEnDateHeureRedac(Calendar.getInstance().getTime()));
        historique.setEtatPeriode(periode.getEtatPeriode());
        historique.setOrigineChangement("RDC");
        historique.setAuditUtilisateurCreation(nomBatch);
        if (StringUtils.isNotBlank(identifiantUtilisateur)) {
            historique.setIdentifiantUtilisateur(identifiantUtilisateur);
        }
        if (StringUtils.isNotBlank(commentaireUtilisateur)) {
            historique.setCommentaireUtilisateur(commentaireUtilisateur);
        }
        if (etatPeriode != null) {
            historique.setEtatPeriode(etatPeriode);
        }

        return historique;
    }

    /**
     * Identifie la condition non respectéen et ajoute l'information à l'historique de la période
     * 
     * @param existeIndicateurDSNPeriode
     *            indique la présence d'un indicateur DSN pour la période
     * @param indicateurDSNTransfert
     *            indique la valeur du flag TransfertDSN, si indicateurDSN existant
     * @param indicateurDSNModeGestion
     *            true si ModeGestion IN { 'O' , 'C' } , false sinon , si indicateurDSN existant
     * @param existeMessagesControle
     *            indique la présence de messages controle, pour la période
     * @param periode
     *            la période testée
     */
    private void identificationConditionEnErreur(boolean existeIndicateurDSNPeriode, boolean indicateurDSNTransfert, boolean indicateurDSNModeGestion,
            boolean existeMessagesControle, PeriodeRecue periode, List<HistoriqueEtatPeriode> listeHistoriqueEtatPeriodes) {
        // F78_RG_P1_05
        if (!existeIndicateurDSNPeriode) {
            HistoriqueEtatPeriode histo = creerHistorique(periode, LIBELLE_IDENTIFIANT_BATCH_701,
                    PREFIXE_COMMENTAIRE_UTILISATEUR_BATCH_701 + "Indicateurs non trouvés sur la période", EtatPeriode.NIN);
            histo.setDateHeureChangement(histo.getDateHeureChangement() + 1);
            listeHistoriqueEtatPeriodes.add(histo);
        }

        // F78_RG_P1_06
        if (existeIndicateurDSNPeriode && !indicateurDSNTransfert) {
            HistoriqueEtatPeriode histo = creerHistorique(periode, LIBELLE_IDENTIFIANT_BATCH_701,
                    PREFIXE_COMMENTAIRE_UTILISATEUR_BATCH_701 + "TransfertDSN = NON", EtatPeriode.NIN);
            histo.setDateHeureChangement(histo.getDateHeureChangement() + 1);
            listeHistoriqueEtatPeriodes.add(histo);
        }
        // F78_RG_P1_06
        if (existeIndicateurDSNPeriode && !indicateurDSNModeGestion) {
            HistoriqueEtatPeriode histo = creerHistorique(periode, LIBELLE_IDENTIFIANT_BATCH_701,
                    PREFIXE_COMMENTAIRE_UTILISATEUR_BATCH_701 + "modeGestionDSN = N", EtatPeriode.NIN);
            histo.setDateHeureChangement(histo.getDateHeureChangement() + 2);
            listeHistoriqueEtatPeriodes.add(histo);
        }

        // F78_RG_P1_07
        if (existeMessagesControle) {
            HistoriqueEtatPeriode histo = creerHistorique(periode, LIBELLE_IDENTIFIANT_BATCH_701,
                    PREFIXE_COMMENTAIRE_UTILISATEUR_BATCH_701 + "Présence de messages de rejet identifiés par REDAC", EtatPeriode.NIN);
            histo.setDateHeureChangement(histo.getDateHeureChangement() + 3);
            listeHistoriqueEtatPeriodes.add(histo);
        }
    }

}
