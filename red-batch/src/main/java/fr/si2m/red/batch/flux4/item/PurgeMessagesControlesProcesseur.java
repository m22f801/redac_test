package fr.si2m.red.batch.flux4.item;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import fr.si2m.red.batch.moteur.item.TransformateurDonneeAvecCollecte;
import fr.si2m.red.reconciliation.MessageControleRepository;
import fr.si2m.red.reconciliation.PeriodeRecue;

/**
 * Purge des messages de contrôles d'une période à reconsolider.<br/>
 * <br/>
 * c.f. RDG F04_RG_P6_01
 * 
 * @author eudesr
 *
 */
public class PurgeMessagesControlesProcesseur extends TransformateurDonneeAvecCollecte<PeriodeRecue, PeriodeRecue> {
    private static final Logger LOGGER = LoggerFactory.getLogger(PurgeMessagesControlesProcesseur.class);

    @Autowired
    private MessageControleRepository messageControleRepository;

    /**
     * Exécution des purges avant de reconsolider une période donnée.
     *
     * @param periode
     *            la période dont les messages controles sont à purger
     */
    @Override
    public final PeriodeRecue process(PeriodeRecue periode) throws Exception {
        int supprime = messageControleRepository.supprimeMessagesRedacPourPeriode(periode.getIdPeriode());
        LOGGER.debug("Suppression réussie de {} messages de controle pour la période {}", supprime, periode.getIdPeriode());
        return periode;
    }

}
