package fr.si2m.red.batch.flux10.execution;

import java.text.MessageFormat;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParameter;
import org.springframework.batch.core.JobParameters;
import org.springframework.context.support.GenericXmlApplicationContext;

import fr.si2m.red.EntiteImportableBatch;
import fr.si2m.red.batch.RedacMessages;
import fr.si2m.red.batch.moteur.CodeRetour;
import fr.si2m.red.batch.moteur.execution.ExecuteurBatch;
import fr.si2m.red.core.repository.EntiteImportableRepository;

/**
 * Base des exécuteurs des batchs du flux 10.
 * 
 * @author poidij
 *
 */
public abstract class Flux10ExecuteurBatch extends ExecuteurBatch {
    /**
     * Ordre du paramètre de lancement "emplacement du répertoire des fichiers".
     */
    public static final int ORDRE_PARAM_CHEMIN_ABSOLU_FICHIER = 0;
    /**
     * Ordre du paramètre de lancement "emplacement du fichier de log d'erreurs" .
     */
    public static final int ORDRE_PARAM_EMPLACEMENT_FICHIER_ERREURS = 1;

    private static final Logger LOGGER = LoggerFactory.getLogger(Flux10ExecuteurBatch.class);

    private final String cheminAbsoluFichierImport;
    private final String emplacementRessourceFichierImport;

    private static final List<String> EXTENTIONS_VALIDES = Arrays.asList("csv", "txt");

    /**
     * Exécuteur de batch du flux 10.
     * 
     * @param parametrageBatch
     *            les paramètres d'exécution du batch - deux paramètres sont attendus en entrée :
     *            <ol>
     *            <li>le chemin absolu vers le fichier à importer</li>
     *            <li>le chemin absolu vers le fichier de logs à éventuellement renseigner</li>
     *            </ol>
     */
    protected Flux10ExecuteurBatch(String[] parametrageBatch) {
        super(parametrageBatch);
        this.cheminAbsoluFichierImport = parametrageBatch[ORDRE_PARAM_CHEMIN_ABSOLU_FICHIER];
        this.emplacementRessourceFichierImport = tranformeEmplacementRessourceFichier(cheminAbsoluFichierImport);

        String cheminAbsoluFichierLogErreurs = parametrageBatch[ORDRE_PARAM_EMPLACEMENT_FICHIER_ERREURS];
        setCheminAbsoluFichierLogErreurs(cheminAbsoluFichierLogErreurs);
    }

    /**
     * Récupère le nom des entités importées.
     * 
     * @return le nom des entités importées
     */
    protected abstract String getNomEntitesImportees();

    /**
     * Récupère le nom de l'entité importée.
     * 
     * @return le nom de l'entité importée
     */
    protected abstract Class<? extends EntiteImportableRepository<? extends EntiteImportableBatch>> getReferentielEntitesImportees();

    @Override
    protected String getEmplacementFichierConfigurationJob() {
        return "classpath:fr/si2m/red/batch/flux10/job_" + getNomEntitesImportees() + ".xml";
    }

    @Override
    protected String getIdJobCible() {
        return "jobFlux10_" + getNomEntitesImportees();
    }

    @Override
    protected boolean lanceTraitementAvantJob(GenericXmlApplicationContext contexteExecutionJob) {
        // Validation de l'extension des fichiers
        if (!verifieValiditeExtensionRessource(this.emplacementRessourceFichierImport, EXTENTIONS_VALIDES)) {
            LOGGER.error("Fichier à extention incorrecte : {}", this.emplacementRessourceFichierImport);
            String messageErreur = MessageFormat.format(RedacMessages.ERREUR_FICHIER_EXTENSION_INCORRECTE, this.cheminAbsoluFichierImport);
            traceErreurDansFichierLog(messageErreur);
            return false;
        }

        // Validation de l'existence des fichiers
        if (!verifieExistencePhysiqueRessource(contexteExecutionJob, this.emplacementRessourceFichierImport)) {
            LOGGER.error("Fichier manquant pour démarrer le batch : {}", this.emplacementRessourceFichierImport);
            String messageErreur = MessageFormat.format(RedacMessages.ERREUR_FICHIER_INEXISTANT, this.cheminAbsoluFichierImport);
            traceErreurDansFichierLog(messageErreur);
            return false;
        }

        if (!testeSiFichierContientDonneesUtiles(contexteExecutionJob, this.emplacementRessourceFichierImport, 1)) {
            LOGGER.error("Le fichier ne peut pas être vide : {}", this.emplacementRessourceFichierImport);
            String mesageErreur = MessageFormat.format(RedacMessages.ERREUR_FICHIER_VIDE, this.cheminAbsoluFichierImport);
            traceErreurDansFichierLog(mesageErreur);
            return false;
        }

        videEspacesTemporaires(contexteExecutionJob, getReferentielEntitesImportees());

        return true;
    }

    @Override
    protected JobParameters initParametrageJob(GenericXmlApplicationContext contexteExecutionJob) {
        Map<String, JobParameter> parametresJob = new HashMap<String, JobParameter>();

        // Configuration lecteur du fichier à importer
        LOGGER.debug("Fichier entrée pris en compte : {}", this.emplacementRessourceFichierImport);
        parametresJob.put("fichierEntree", new JobParameter(this.emplacementRessourceFichierImport));

        // Configuration de l'emplacement du fichier de log
        LOGGER.debug("Fichier erreurs pris en compte : {}", getCheminAbsoluFichierLogErreurs());
        parametresJob.put("fichierLogErreurs", new JobParameter(getCheminAbsoluFichierLogErreurs()));

        return new JobParameters(parametresJob);
    }

    @Override
    protected void lanceTraitementFinJob(GenericXmlApplicationContext contexteExecutionJob, CodeRetour codeRetour, JobExecution executionJob) {
        if (codeRetour != CodeRetour.TERMINE && codeRetour != CodeRetour.ERREUR_FONCTIONNELLE_NON_BLOQUANTE) {
            videEspacesTemporaires(contexteExecutionJob, getReferentielEntitesImportees());
        }

        // Logge le message de réussite du traitement batch dans le fichier de log
        if (codeRetour == CodeRetour.TERMINE) {
            traceTraitementOKDansFichierLog("Traitement réussi du fichier " + this.cheminAbsoluFichierImport);
        } else if (codeRetour == CodeRetour.ERREUR_FONCTIONNELLE_NON_BLOQUANTE) {
            traceTraitementOKDansFichierLog("Traitement réussi du fichier, avec erreur(s) non bloquante(s) : " + this.cheminAbsoluFichierImport);
        }
        LOGGER.info("Fin d'exécution du batch flux 10 - import des données {}", getNomEntitesImportees());
    }
}
