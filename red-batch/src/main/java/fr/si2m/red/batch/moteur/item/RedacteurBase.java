package fr.si2m.red.batch.moteur.item;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;

import lombok.Setter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Repository;
import org.springframework.util.Assert;

import fr.si2m.red.Entite;
import fr.si2m.red.batch.moteur.EtapeCodeRetourModificateur;
import fr.si2m.red.core.repository.RedacRepository;

/**
 * Rédacteur de situations contrats exportées dans une table de données temporaires pour consultation ultérieure.
 * 
 * @author poidij
 * 
 * @param <T>
 *            le type de données à insérer dans le référentiel REDAC
 *
 */
@Repository
public class RedacteurBase<T extends Entite> extends EtapeCodeRetourModificateur implements ItemWriter<List<T>>, InitializingBean {
    private static final Logger LOGGER = LoggerFactory.getLogger(RedacteurBase.class);

    /**
     * Le référentiel du type de données ciblées.
     *
     * @param referentiel
     *            le référentiel du type de données ciblées à mettre à jour
     */
    @Setter
    private RedacRepository<T> referentiel;

    /**
     * Flag pour vérifier ou non les doublons (peut radicalement impacter les performances si positionné à true).
     * 
     * @param verifieDoublons
     *            true pour vérifier les doublons, false sinon
     */
    @Setter
    private boolean verifieDoublons = false;

    @Override
    public void afterPropertiesSet() throws Exception {
        Assert.notNull(referentiel, "Le référentiel doit obligatoirement être paramétré");
    }

    @Override
    public void write(List<? extends List<T>> listeItems) throws Exception {
        long totalInsertions = 0L;
        if (verifieDoublons) {
            // Applatissement
            List<T> donnees = new LinkedList<T>();
            for (List<T> liste : listeItems) {
                donnees.addAll(liste);
            }

            // Gestion des doublons au préalable
            List<T> sansDoublons = new ArrayList<T>(new LinkedHashSet<T>(donnees));
            if (!sansDoublons.isEmpty()) {
                List<T> listeDoublonsEnBase = referentiel.getEntiteExistantes(sansDoublons);
                sansDoublons.removeAll(listeDoublonsEnBase);
                referentiel.creeEnMasse(sansDoublons);
                totalInsertions = sansDoublons.size();
            }
        } else {
            for (List<T> liste : listeItems) {
                referentiel.creeEnMasse(liste);
                totalInsertions += liste.size();
            }
        }
        LOGGER.debug("Nombre d'entités insérées dans le référentiel : {}", totalInsertions);
    }
}
