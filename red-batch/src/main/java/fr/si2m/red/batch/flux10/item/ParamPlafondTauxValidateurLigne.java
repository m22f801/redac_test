package fr.si2m.red.batch.flux10.item;

import fr.si2m.red.DateRedac;
import fr.si2m.red.batch.moteur.erreur.ValidateurLigneAvecCollecte;
import fr.si2m.red.parametrage.ParamPlafondTaux;

/***
 * Validateur de ligne.
 * 
 * @author delortj
 *
 */
public class ParamPlafondTauxValidateurLigne extends ValidateurLigneAvecCollecte<ParamPlafondTaux> {

    @Override
    protected void valide(final ParamPlafondTaux plafondsTaux) {
        // Validation de date obligatoire et du format de date
        plafondsTaux.setDebut(valideChampDateAvecCollecte(plafondsTaux.getDebut(), DateRedac.FORMAT_DATES, plafondsTaux, "DEBUT", true));

        // Validation du format de date
        plafondsTaux.setFin(valideChampDateAvecCollecte(plafondsTaux.getFin(), DateRedac.FORMAT_DATES, plafondsTaux, "FIN", false));

        // Longueur des champs
        if (plafondsTaux.getPlafondMensuelSecuriteSociale() != null) {
            valideChampNumeriqueDecimaleAvecCollecte(plafondsTaux.getPlafondMensuelSecuriteSociale(), 13, 2, plafondsTaux, "PMSS",
                    "La taille du champ PMSS est incorrect");
        }

        // Ajustement de l'entité si validation OK
        if (verifieDateVide(plafondsTaux.getFin())) {
            plafondsTaux.setFin(null);
        }
    }
}
