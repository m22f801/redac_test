package fr.si2m.red.batch.moteur.support;

import org.springframework.batch.item.file.mapping.FieldSetMapper;

import fr.si2m.red.batch.moteur.EtapeCodeRetourModificateur;

/**
 * Gestionnaire de conversion de ligne réceptionnée pour un traitement batch REDAC.<br/>
 * <br/>
 * S'il n'y a pas besoin de conversion (mapping des champs de types primitifs uniquement), privilégier {@link LecteurLigne}.
 * 
 * @author nortaina
 *
 * @param <T>
 *            type de la ligne à convertir
 * 
 * @see LecteurLigne
 */
public abstract class ConvertisseurLigne<T> extends EtapeCodeRetourModificateur implements FieldSetMapper<T> {

}
