package fr.si2m.red.batch.flux3.ligne;

import lombok.Data;

/**
 * Description d'une ligne d'un fichier CONTACTS à exporter.
 * 
 * @author nortaina
 *
 */
@Data
public class Contact {
    /**
     * Code Mise à jour.
     * 
     * @param codeMiseAJour
     *            Code Mise à jour
     * @return Code Mise à jour
     */
    private String codeMiseAJour;

    /**
     * Identifiant du contact.
     * 
     * @param identifiantContact
     *            Identifiant du contact
     * @return Identifiant du contact
     */
    private String identifiantContact;

    /**
     * Date de début d’application.
     * 
     * @param dateDebutApplication
     *            Date de début d’application
     * @return Date de début d’application
     */
    private String dateDebutApplication;

    /**
     * Date de fin d’application.
     * 
     * @param dateFinApplication
     *            Date de fin d’application
     * @return Date de fin d’application
     */
    private String dateFinApplication;

    /**
     * Type d’enregistrement.
     * 
     * @param typeEnregistrement
     *            Type d’enregistrement
     * @return Type d’enregistrement
     */
    private String typeEnregistrement;

    /**
     * Référence à l’identifiant technique de l’Entreprise (N° de séquence local à une instance de cette interface).
     * 
     * @param identifiantTechniqueEntreprise
     *            Référence à l’identifiant technique de l’Entreprise (N° de séquence local à une instance de cette interface)
     * @return Référence à l’identifiant technique de l’Entreprise (N° de séquence local à une instance de cette interface)
     */
    private String identifiantTechniqueEntreprise;

    /**
     * Référence à l’identifiant technique de l’Etablissement (N° de séquence local à une instance de cette interface).
     * 
     * @param identifiantTechniqueEtablissement
     *            Référence à l’identifiant technique de l’Etablissement (N° de séquence local à une instance de cette interface)
     * @return Référence à l’identifiant technique de l’Etablissement (N° de séquence local à une instance de cette interface)
     */
    private String identifiantTechniqueEtablissement;

    /**
     * Rôle gestionnaire.
     * 
     * @param roleGestionnaire
     *            Rôle gestionnaire
     * @return Rôle gestionnaire
     */
    private String roleGestionnaire;

    /**
     * Rôle technique.
     * 
     * @param roleTechnique
     *            Rôle technique
     * @return Rôle technique
     */
    private String roleTechnique;

    /**
     * Nom du contact.
     * 
     * @param nom
     *            Nom du contact
     * @return Nom du contact
     */
    private String nom;

    /**
     * Numéro de téléphone.
     * 
     * @param telephone
     *            Numéro de téléphone
     * @return Numéro de téléphone
     */
    private String telephone;

    /**
     * Complément numéro de téléphone.
     * 
     * @param complementTelephone
     *            Complément numéro de téléphone
     * @return Complément numéro de téléphone
     */
    private String complementTelephone;

    /**
     * Numéro de fax.
     * 
     * @param fax
     *            Numéro de fax
     * @return Numéro de fax
     */
    private String fax;

    /**
     * Adresse email.
     * 
     * @param email
     *            Adresse email
     * @return Adresse email
     */
    private String email;

}
