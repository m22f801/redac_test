package fr.si2m.red.batch.flux3.item;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import fr.si2m.red.batch.flux3.ligne.SituationTarifNoco;
import fr.si2m.red.contrat.SituationTarif;

/**
 * Un mapper de ligne {@link SituationTarif} pour supporter la récupération de données via JDBC.
 * 
 * @author eudesr
 *
 */
public class SituationTarifNocoRowMapper implements RowMapper<SituationTarifNoco> {

    @Override
    public SituationTarifNoco mapRow(ResultSet rs, int rowNum) throws SQLException {

        SituationTarifNoco situationTarifNoco = new SituationTarifNoco();
        // Mapping des champs de la table TMP_RR300_TARIFS_IDS
        situationTarifNoco.setNumContrat(rs.getString("NOCO"));
        return situationTarifNoco;
    }
}
