package fr.si2m.red.batch.flux10.item;

import org.apache.commons.lang3.StringUtils;

import fr.si2m.red.batch.moteur.erreur.ValidateurLigneAvecCollecte;
import fr.si2m.red.parametrage.ParamUtilisateurGestionnaire;

/***
 * Validateur de ligne.
 * 
 * @author poidij
 *
 */
public class ParamUtilisateurGestionnaireValidateurLigne extends ValidateurLigneAvecCollecte<ParamUtilisateurGestionnaire> {

    @Override
    protected void valide(final ParamUtilisateurGestionnaire utilisateurGestionnaire) {
        // Champ obligatoire
        valideChampAvecCollecte(StringUtils.isNotBlank(utilisateurGestionnaire.getIdentifiantActiveDirectory()), utilisateurGestionnaire, "CODE_USER",
                "Le champ CODE_USER obligatoire n'est pas renseigné");

        // Longueur des champs

        // F01_RG_???
        valideTailleMaximaleChampTexteAvecCollecte(utilisateurGestionnaire.getIdentifiantActiveDirectory(), 50, utilisateurGestionnaire, "CODE_USER");

        valideTailleMaximaleChampTexteAvecCollecte(utilisateurGestionnaire.getNom(), 50, utilisateurGestionnaire, "NOM");

        valideTailleMaximaleChampTexteAvecCollecte(utilisateurGestionnaire.getPrenom(), 50, utilisateurGestionnaire, "PRENOM");

        valideTailleMaximaleChampTexteAvecCollecte(utilisateurGestionnaire.getDroitVIPAsText(), 1, utilisateurGestionnaire, "DROIT_VIP");

        valideTailleMaximaleChampTexteAvecCollecte(utilisateurGestionnaire.getDroitIndividuAsText(), 1, utilisateurGestionnaire, "DROIT_INDIV");

        valideTailleMaximaleChampTexteAvecCollecte(utilisateurGestionnaire.getGroupeGestion(), 1, utilisateurGestionnaire, "GRP_GEST");

        // ajout des nouvelles zones du fichier flux 10 gestionnaire
        valideTailleMaximaleChampTexteAvecCollecte(utilisateurGestionnaire.getAfficheIhm(), 3, utilisateurGestionnaire, "AFFICHE_IHM");
        valideTailleMaximaleChampTexteAvecCollecte(utilisateurGestionnaire.getType(), 50, utilisateurGestionnaire, "TYPE");
        valideTailleMaximaleChampTexteAvecCollecte(utilisateurGestionnaire.getGgEgal(), 50, utilisateurGestionnaire, "GG_EGAL");
        valideTailleMaximaleChampTexteAvecCollecte(utilisateurGestionnaire.getGgInclus1(), 50, utilisateurGestionnaire, "GG_INCLUS_1");
        valideTailleMaximaleChampTexteAvecCollecte(utilisateurGestionnaire.getGgInclus2(), 50, utilisateurGestionnaire, "GG_INCLUS_2");
        valideTailleMaximaleChampTexteAvecCollecte(utilisateurGestionnaire.getGgExclus(), 50, utilisateurGestionnaire, "GG_EXCLUS");
        valideTailleMaximaleChampTexteAvecCollecte(utilisateurGestionnaire.getNiveau1(), 30, utilisateurGestionnaire, "NIVEAU_1");
        valideTailleMaximaleChampTexteAvecCollecte(utilisateurGestionnaire.getNiveau2(), 30, utilisateurGestionnaire, "NIVEAU_2");
        valideTailleMaximaleChampTexteAvecCollecte(utilisateurGestionnaire.getNiveau3(), 30, utilisateurGestionnaire, "NIVEAU_3");

    }
}
