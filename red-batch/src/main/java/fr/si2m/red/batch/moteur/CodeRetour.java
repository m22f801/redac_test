package fr.si2m.red.batch.moteur;

/**
 * Codes retours des batchs REDAC.
 * 
 * @author nortaina
 *
 */
public enum CodeRetour {
    /**
     * Batch terminé sans erreur.
     */
    TERMINE(0),
    /**
     * Batch terminé avec au moins une erreur fonctionnelle non-bloquante.
     */
    ERREUR_FONCTIONNELLE_NON_BLOQUANTE(20),
    /**
     * Batch terminé avec une erreur fonctionnelle bloquante.
     */
    ERREUR_FONCTIONNELLE_BLOQUANTE(40),
    /**
     * Batch terminé avec une erreur fonctionnelle bloquante.
     */
    ERREUR_TECHNIQUE_BLOQUANTE(80);

    private final int code;

    private CodeRetour(int code) {
        this.code = code;
    }

    /**
     * Récupère le code de sortie correspondant.
     *
     * @return le code de sortie correspondant
     */
    public int getCode() {
        return code;
    }

    /**
     * Récupère le code de sortie correspondant en chaîne de caractères.
     *
     * @return le code de sortie correspondant en chaîne de caractères
     */
    public String getCodeAsString() {
        return String.valueOf(code);
    }

    /**
     * Résoud un retour à partir de son code.
     * 
     * @param code
     *            le code
     * @return le retour correspondant au code
     */
    public static CodeRetour fromCode(int code) {
        for (CodeRetour retour : values()) {
            if (retour.code == code) {
                return retour;
            }
        }
        return null;
    }

}
