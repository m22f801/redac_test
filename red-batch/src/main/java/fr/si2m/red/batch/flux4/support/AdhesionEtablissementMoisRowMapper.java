package fr.si2m.red.batch.flux4.support;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import fr.si2m.red.dsn.AdhesionEtablissementMois;
import fr.si2m.red.reconciliation.PeriodeRecue;

/**
 * Un mapper de ligne {@link PeriodeRecue} pour supporter la récupération de données Périodes Reçues via JDBC.
 * 
 * @author nortaina
 *
 */
public class AdhesionEtablissementMoisRowMapper implements RowMapper<AdhesionEtablissementMois> {

    @Override
    public AdhesionEtablissementMois mapRow(ResultSet rs, int rowNum) throws SQLException {
        AdhesionEtablissementMois adhesion = new AdhesionEtablissementMois();
        adhesion.setIdAdhEtabMois(rs.getString("ID_ADH_ETAB_MOIS"));
        adhesion.setCodeEssaiReel(rs.getString("CODE_ESSAI_REEL"));
        adhesion.setTypeEnvoi(rs.getString("TYPE_ENVOI"));
        adhesion.setSirenEmetteur(rs.getString("SIREN_EMETTEUR"));
        adhesion.setNicEmetteur(rs.getString("NIC_EMETEUR"));
        adhesion.setNomEmetteur(rs.getString("NOM_EMETTEUR"));
        adhesion.setVoieEmetteur(rs.getString("VOIE_EMETTEUR"));
        adhesion.setCodePostalEmetteur(rs.getString("CODE_POSTAL_EMETTEUR"));
        adhesion.setLocaliteEmetteur(rs.getString("LOCALITE_EMETTEUR"));
        adhesion.setPaysEmetteur(rs.getString("PAYS_EMETTEUR"));
        adhesion.setCodeDistributionEmetteur(rs.getString("CODE_DISTRIB_EMETTEUR"));
        adhesion.setComplementConstructionEmetteur(rs.getString("COMPLEMENT_CONSTRUCTION_EMETTEUR"));
        adhesion.setComplementVoieEmetteur(rs.getString("COMPLEMENT_VOIE_EMETTEUR"));
        adhesion.setCiviliteContact(rs.getString("CIVILITE_CONTACT"));
        adhesion.setNomContact(rs.getString("NOM_CONTACT"));
        adhesion.setMailContact(rs.getString("MAIL_CONTACT"));
        adhesion.setTelContact(rs.getString("TEL_CONTACT"));
        adhesion.setFaxContact(rs.getString("FAX_CONTACT"));
        adhesion.setTypeDeclaration(rs.getString("TYPE_DECLARATION"));

        adhesion.setNumeroOrdreDeclaration(rs.getLong("NUMERO_ORDRE_DECLARATION"));

        adhesion.setMoisDeclare(rs.getInt("MOIS_DECLARE"));
        adhesion.setMoisRattachement(rs.getInt("MOIS_RATTACHEMENT"));
        adhesion.setDateFichier(rs.getInt("DATE_FICHIER"));
        adhesion.setChampDeclaration(rs.getString("CHAMP_DECLARATION"));
        adhesion.setIdentifiantMetier(rs.getString("IDENTIFIANT_METIER"));
        adhesion.setDeviseDeclaration(rs.getString("DEVISE_DECLARATION"));
        adhesion.setSirenEntreprise(rs.getString("SIREN_ENTREPRISE"));
        adhesion.setNicEntreprise(rs.getString("NIC_ENTREPRISE"));
        adhesion.setCodeAPEN(rs.getString("CODE_APEN"));
        adhesion.setVoieEntreprise(rs.getString("VOIE_ENTREPRISE"));
        adhesion.setCodePostalEntreprise(rs.getString("CODE_POSTAL_ENTREPRISE"));
        adhesion.setLocaliteEntreprise(rs.getString("LOCALITE_ENTREPRISE"));
        adhesion.setComplementConstructionEntreprise(rs.getString("COMPLEMENT_CONSTRUCTION_ENTREPRISE"));
        adhesion.setComplementVoieEntreprise(rs.getString("COMPLEMENT_VOIE_ENTREPRISE"));

        adhesion.setEffectifMoyen(rs.getInt("EFFECTIF_MOYEN_ENTREPRISE"));

        adhesion.setPaysEntreprise(rs.getString("PAYS_ENTREPRISE"));
        adhesion.setCodeDistributionEntreprise(rs.getString("CODE_DISTRIB_ENTREPRISE"));
        adhesion.setImplantationEntreprise(rs.getString("IMPLANTATION_ENTREPRISE"));
        adhesion.setRaisonSociale(rs.getString("RAISON_SOCIALE_ENTREPRISE"));
        adhesion.setNicEtablissement(rs.getString("NIC_ETABLISSEMENT"));
        adhesion.setCodeAPET(rs.getString("CODE_APET"));
        adhesion.setNicEtablissement(rs.getString("NIC_ETABLISSEMENT"));

        adhesion.setVoieEtablissement(rs.getString("VOIE_ETABLISSEMENT"));
        adhesion.setCodePostalEtablissement(rs.getString("CODE_POSTAL_ETABLISSEMENT"));
        adhesion.setLocaliteEtablissement(rs.getString("LOCALITE_ETABLISSEMENT"));
        adhesion.setComplementConstructionEtablissement(rs.getString("COMPLEMENT_CONSTRUCTION_ETABLISSEMENT"));
        adhesion.setComplementVoieEtablissement(rs.getString("COMPLEMENT_VOIE_ETABLISSEMENT"));

        adhesion.setEffectifFinPeriode(rs.getInt("EFFECTIF_FIN_PERIODE_ETABLISSEMENT"));
        adhesion.setPaysEtablissement(rs.getString("PAYS_ETABLISSEMENT"));
        adhesion.setCodeDistributionEtablissement(rs.getString("CODE_DISTRIB_ETABLISSEMENT"));
        adhesion.setNatureJuridique(rs.getString("NATURE_JURIDIQUE_ETABLISSEMENT"));
        adhesion.setCodeINSEE(rs.getString("CODE_INSEE"));

        adhesion.setDateEcheance(rs.getInt("DATE_ECHEANCE"));

        adhesion.setCategorieJuridique(rs.getString("CATEGORIE_JURIDIQUE"));
        adhesion.setEnseigneEtablissement(rs.getString("ENSEIGNE_ETABLISSEMENT"));
        adhesion.setReferenceContrat(rs.getString("REFERENCE_CONTRAT"));
        adhesion.setCodeOrganisme(rs.getString("CODE_ORGANISME"));
        adhesion.setCodeDelegataireGestion(rs.getString("CODE_DELEG_COT"));
        adhesion.setPersonnelCouvert(rs.getString("PERSONNEL_COUVERT"));

        return adhesion;
    }
}
