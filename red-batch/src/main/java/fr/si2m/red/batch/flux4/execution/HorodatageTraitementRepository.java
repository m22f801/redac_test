package fr.si2m.red.batch.flux4.execution;

import fr.si2m.red.core.repository.EntiteImportableRepository;

/**
 * Référentiel des horodatages traitements.
 * 
 * @author poidij
 *
 */
public interface HorodatageTraitementRepository extends EntiteImportableRepository<HorodatageTraitement> {

    /**
     * Vérifie qu'un horodatage donné existe dans le référentiel.
     * 
     * @param horodatage
     *            l'horodatage à vérifier
     * @return true si l'horodatage existe, false sinon
     */
    boolean existeUnHorodatageTraitement(String horodatage);

}
