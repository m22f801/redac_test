package fr.si2m.red.batch;

/**
 * Messages présents dans les traitements batchs.
 * 
 * @author poidij
 *
 */
public final class RedacMessages {

    /**
     * Message d'erreur pour le code retour 40 après exécution d'un batch en erreur fonctionnelle.
     */
    public static final String ERREUR_JOB_CODE_RETOUR_40 = "Statut du job batch en sortie : code 40 - {0}";

    /**
     * Message d'erreur pour le code retour 80 après exécution d'un batch en erreur technique.
     */
    public static final String ERREUR_JOB_CODE_RETOUR_80 = "Statut du job batch en sortie : code 80 - {0}";

    /**
     * Message d'erreur d'un fichier vide.
     */
    public static final String ERREUR_FICHIER_VIDE = "Le fichier {0} est vide.";
    /**
     * Message d'erreur d'un fichier inexistant.
     */
    public static final String ERREUR_FICHIER_INEXISTANT = "Le fichier {0} n''existe pas.";
    /**
     * Message d'erreur d'un fichier avec une extension incorrecte.
     */
    public static final String ERREUR_FICHIER_EXTENSION_INCORRECTE = "Le fichier {0} a une extension incorrecte.";
    /**
     * Message d'erreur d'un fichier sans ligne d'entête.
     */
    public static final String ERREUR_FICHIER_SANS_ENTETE = "Le fichier {0} ne contient pas d''entête.";
    /**
     * Message d'erreur d'un fichier sans marqueur de fin de fichier (dernière ligne spécifique).
     */
    public static final String ERREUR_FICHIER_SANS_FIN = "Le fichier {0} ne contient pas de fin de fichier.";

    /**
     * Message d'erreur d'un champ non numérique.
     */
    public static final String ERREUR_CHAMP_NON_NUMERIQUE = "Le champ {0} n''est pas numérique.";
    /**
     * Message d'erreur d'un champ obligatoire non renseigné.
     */
    public static final String ERREUR_CHAMP_OBLIGATOIRE = "Le champ {0} obligatoire n''est pas renseigné.";
    /**
     * Message d'erreur d'un champ numérique avec une taille non conforme.
     */
    public static final String ERREUR_CHAMP_TAILLE_NON_CONFORME = "La taille du champ {0} n''est pas conforme.";
    /**
     * Message d'erreur d'un champ avec une taille dépassée.
     */
    public static final String ERREUR_CHAMP_TAILLE_DEPASSEE = "Le champ {0} dépasse la taille maximale prévue.";
    /**
     * Message d'erreur d'un champ erroné.
     */
    public static final String ERREUR_CHAMP_ERRONE = "Le champ {0} est erroné.";
    /**
     * Message d'erreur d'un champ à la valeur incorrecte.
     */
    public static final String ERREUR_CHAMP_VALEUR_INCORRECTE = "La valeur du champ {0} ({1}) n''est pas correcte.";
    /**
     * Message d'erreur d'un champ à la valeur différente d'une valeur attendue.
     */
    public static final String ERREUR_CHAMP_VALEUR_DIFFERENTE = "Le champ {0} est différent {1}.";
    /**
     * Message d'erreur d'un champ à la valeur erronée, inférieure à une valeur attendue.
     */
    public static final String ERREUR_CHAMP_ERRONE_VALEUR_INFERIEURE = "Le champ {0} est erroné (inférieur à {1}).";
    /**
     * Message d'erreur d'un champ à la valeur erronée, différente d'une valeur attendue.
     */
    public static final String ERREUR_CHAMP_ERRONE_VALEUR_INATTENDUE = "Le champ {0} est erroné (différent de {1}).";
    /**
     * Message d'erreur d'un champ au format incorrect.
     */
    public static final String ERREUR_CHAMP_FORMAT_INCORRECT = "Le format du champ {0} est incorrect.";

    /**
     * Message d'erreur sur conversion d'une valeur.
     */
    public static final String ERREUR_VALEUR_CONVERSION_ENTIER = "La conversion de la valeur {0}={1} en nombre entier a échoué";

    /**
     * Message d'erreur pour une période inconnue lors de la réception de données correspondant à une période reçue.
     */
    public static final String ERREUR_AUCUNE_PERIODE = "Aucune période ne correspond à cette ligne.";

    /**
     * Message d'erreur en cas de valeur de clé étrangère n'existant pas.
     */
    public static final String ERREUR_REFERENCE = "{0} ({1} : {2}) n''existe pas.";

    /**
     * Message d'erreur en cas de valeur de clé étrangère n'existant pas.
     */
    public static final String ERREUR_REFERENCE_FLUX4 = "La ligne ne respecte pas une contrainte d'intégrité.";

    /**
     * Libellé du champ COETACO.
     */
    public static final String CHAMP_COETACO = "COETACO";
    /**
     * Libellé du champ MTCUNCU.
     */
    public static final String CHAMP_MTCUNCU = "MTCUNCU";
    /**
     * Libellé du champ TXCALCU.
     */
    public static final String CHAMP_TXCALCU = "TXCALCU";
    /**
     * Libellé du champ TXBASE1.
     */
    public static final String CHAMP_TXBASE1 = "TXBASE1";
    /**
     * Libellé du champ TXBASE2.
     */
    public static final String CHAMP_TXBASE2 = "TXBASE2";
    /**
     * Libellé du champ TXBASE3.
     */
    public static final String CHAMP_TXBASE3 = "TXBASE3";
    /**
     * Libellé du champ TXBASE4.
     */
    public static final String CHAMP_TXBASE4 = "TXBASE4";
    /**
     * Libellé du champ NCPROD.
     */
    public static final String CHAMP_NCPROD = "NCPROD";
    /**
     * Libellé du champ ELIGDSN.
     */
    public static final String CHAMP_ELIGDSN = "ELIGDSN";
    /**
     * Libellé du champ NOCLI.
     */
    public static final String CHAMP_NOCLI = "NOCLI";
    /**
     * Libellé du champ COFRQUIT.
     */
    public static final String CHAMP_COFRQUIT = "COFRQUIT";
    /**
     * Libellé du champ NCENC.
     */
    public static final String CHAMP_NCENC = "NCENC";
    /**
     * Libellé du champ NCSIN.
     */
    public static final String CHAMP_NCSIN = "NCSIN";
    /**
     * Libellé du champ NUCLIDLP.
     */
    public static final String CHAMP_NUCLIDLP = "NUCLIDLP";

    /**
     * Message d'erreur pour une ligne de synthèse KO.
     */
    public static final String PAS_DE_SYNTHESE = "Le fichier ne contient pas de ligne synthèse.";
    
    /**
     * Libellé du champ nombre d'enregistrement de type 2 (Flux 78).
     */
    public static final String CHAMP_NBRE_ENREGISTREMENTS2 = "nombre d'enregistrements de type 2";
    /**
     * Libellé du champ nombre d'enregistrement de type 3 (Flux 78).
     */
    public static final String CHAMP_NBRE_ENREGISTREMENTS3 = "nombre d'enregistrements de type 3";
    /**
     * Libellé du champ dateDebutPeriode pour validations.
     */
    public static final String CHAMP_DATE_DEBUT_PERIODE = "dateDebutPeriode";
    /**
     * Libellé du champ dateFinPeriode pour validations.
     */
    public static final String CHAMP_DATE_FIN_PERIODE = "dateFinPeriode";
    /**
     * Libellé du champ codeNature pour validations.
     */
    public static final String CHAMP_CODE_NATURE = "codeNature";
    /**
     * Libellé du champ codeNatureMessage pour validations.
     */
    public static final String CHAMP_CODE_NATURE_MESSAGE = "CodeNatureMessage";    
    /**
     * Libellé du champ CodeRejet pour validations.
     */
    public static final String CHAMP_CODE_REJET = "CodeRejet";    
    /**
     * Libellé du champ LibelleRejet pour validations.
     */
    public static final String CHAMP_LIBELLE_REJET = "LibelleRejet";    
    

    private RedacMessages() {
    }

}
