package fr.si2m.red.batch.flux3.ligne;

import lombok.Data;
import fr.si2m.red.DateRedac;

/**
 * Description d'une ligne d'un fichier ENTREPRISE à exporter.
 * 
 * @author nortaina
 *
 */
@Data
public class Entreprise {
    /**
     * Code Mise à jour.
     * 
     * @param codeMiseAJour
     *            Code Mise à jour
     * @return Code Mise à jour
     */
    private String codeMiseAJour;

    /**
     * Identifiant de l’entreprise G3C.
     * 
     * @param identifiantEntrepriseG3C
     *            Identifiant de l’entreprise G3C
     * @return Identifiant de l’entreprise G3C
     */
    private String identifiantEntrepriseG3C;

    /**
     * Raison Sociale longue G3C.
     * 
     * @param raisonSocialeLongueG3C
     *            Raison Sociale longue G3C
     * @return Raison Sociale longue G3C
     */
    private String raisonSocialeLongueG3C;

    /**
     * Sigle de l'Entreprise.
     * 
     * @param sigleEntreprise
     *            Sigle de l'Entreprise
     * @return Sigle de l'Entreprise
     */
    private String sigleEntreprise;

    /**
     * Date de création de l’Entreprise.
     * 
     * @param dateCreationEntreprise
     *            Date de création de l’Entreprise
     * @return Date de création de l’Entreprise
     */
    private Integer dateCreationEntreprise;

    /**
     * Numéro d’Identification de l’Entreprise dans l’Organisme.
     * 
     * @param numIdentificationEntrepriseDansOrganisme
     *            Numéro d’Identification de l’Entreprise dans l’Organisme
     * @return Numéro d’Identification de l’Entreprise dans l’Organisme
     */
    private Integer numIdentificationEntrepriseDansOrganisme;

    /**
     * Numéro de SIREN.
     * 
     * @param numSiren
     *            Numéro de SIREN
     * @return Numéro de SIREN
     */
    private String numSiren;

    /**
     * Raison Sociale abrégée G3C.
     * 
     * @param raisonSocialeAbregeeG3C
     *            Raison Sociale abrégée G3C
     * @return Raison Sociale abrégée G3C
     */
    private String raisonSocialeAbregeeG3C;

    /**
     * Date de première exploitation.
     * 
     * @param datePremiereExploitation
     *            Date de première exploitation
     * @return Date de première exploitation
     */
    private Integer datePremiereExploitation;

    /**
     * Date de début de mise en sommeil.
     * 
     * @param dateDebutMiseEnSommeil
     *            Date de début de mise en sommeil
     * @return Date de début de mise en sommeil
     */
    private Integer dateDebutMiseEnSommeil;

    /**
     * Date de fin de mise en sommeil.
     * 
     * @param dateFinMiseEnSommeil
     *            Date de fin de mise en sommeil
     * @return Date de fin de mise en sommeil
     */
    private Integer dateFinMiseEnSommeil;

    /**
     * Code indicateur de cession partielle ou totale.
     * 
     * @param dateFinMiseEnSommeil
     *            Code indicateur de cession partielle ou totale
     * @return Code indicateur de cession partielle ou totale
     */
    private String codeIndicateurCession;

    /**
     * Date de fin de cession partielle.
     * 
     * @param dateFinCessionPartielle
     *            Date de fin de cession partielle
     * @return Date de fin de cession partielle
     */
    private Integer dateFinCessionPartielle;

    /**
     * Date de fin de plan de continuation.
     * 
     * @param dateFinPlanContinuation
     *            Date de fin de plan de continuation
     * @return Date de fin de plan de continuation
     */
    private Integer dateFinPlanContinuation;

    /**
     * Date de dissolution.
     * 
     * @param dateDissolution
     *            Date de dissolution
     * @return Date de dissolution
     */
    private Integer dateDissolution;

    /**
     * Date de radiation.
     * 
     * @param dateRadiation
     *            Date de radiation
     * @return Date de radiation
     */
    private Integer dateRadiation;

    /**
     * Code motif de la radiation.
     * 
     * @param codeMotifRadiation
     *            Code motif de la radiation
     * @return Code motif de la radiation
     */
    private String codeMotifRadiation;

    /**
     * Date de cessation de l’activité.
     * 
     * @param dateCessationActivite
     *            Date de cessation de l’activité
     * @return Date de cessation de l’activité
     */
    private Integer dateCessationActivite;

    /**
     * Date première embauche.
     * 
     * @param datePremiereEmbauche
     *            Date première embauche
     * @return Date première embauche
     */
    private Integer datePremiereEmbauche;

    /**
     * Date de connaissance de l’entreprise par le Groupe Médéric.
     * 
     * @param dateConnaissance
     *            Date de connaissance de l’entreprise par le Groupe Médéric
     * @return Date de connaissance de l’entreprise par le Groupe Médéric
     */
    private Integer dateConnaissance;

    /**
     * Code APE.
     * 
     * @param codeAPE
     *            Code APE
     * @return Code APE
     */
    private String codeAPE;

    /**
     * Date de début d’application du code APE.
     * 
     * @param dateDebutApplicationCodeAPE
     *            Date de début d’application du code APE
     * @return Date de début d’application du code APE
     */
    private Integer dateDebutApplicationCodeAPE;

    /**
     * Date de fin d’application du code APE.
     * 
     * @param dateFinApplicationCodeAPE
     *            Date de fin d’application du code APE
     * @return Date de fin d’application du code APE
     */
    private Integer dateFinApplicationCodeAPE;

    /**
     * Date effet Raison sociale.
     * 
     * @param dateEffetRaisonSociale
     *            Date effet Raison sociale
     * @return Date effet Raison sociale
     */
    private Integer dateEffetRaisonSociale;

    /**
     * Option décalage de paie de l’entreprise.
     * 
     * @param optionDecalagePaieEntreprise
     *            Option décalage de paie de l’entreprise
     * @return Option décalage de paie de l’entreprise
     */
    private String optionDecalagePaieEntreprise;

    /**
     * Date de début d’application du décalage de paie.
     * 
     * @param dateDebutApplicationDecalagePaie
     *            Date de début d’application du décalage de paie
     * @return Date de début d’application du décalage de paie
     */
    private Integer dateDebutApplicationDecalagePaie;

    /**
     * Date de fin d’application du décalage de paie.
     * 
     * @param dateFinApplicationDecalagePaie
     *            Date de fin d’application du décalage de paie
     * @return Date de fin d’application du décalage de paie
     */
    private Integer dateFinApplicationDecalagePaie;

    /**
     * Type de décalage pratiqué.
     * 
     * @param typeDecalagePratique
     *            Type de décalage pratiqué
     * @return Type de décalage pratiqué
     */
    private String typeDecalagePratique;

    /**
     * Code entreprise VIP.
     * 
     * @param codeEntrepriseVIP
     *            Code entreprise VIP
     * @return Code entreprise VIP
     */
    private String codeEntrepriseVIP;

    /**
     * Message à destination du déclarant.
     * 
     * @param messageDestinationDeclarant
     *            Message à destination du déclarant
     * @return Message à destination du déclarant
     */
    private String messageDestinationDeclarant;

    /**
     * Raison Sociale de l’Entreprise.
     * 
     * @param raisonSocialeEntreprise
     *            Raison Sociale de l’Entreprise
     * @return Raison Sociale de l’Entreprise
     */
    private String raisonSocialeEntreprise;

    /**
     * Identifiant technique de l’Entreprise (N° de séquence local à une instance de cette interface).
     * 
     * @param identifiantTechniqueEntreprise
     *            Identifiant technique de l’Entreprise (N° de séquence local à une instance de cette interface)
     * @return Identifiant technique de l’Entreprise (N° de séquence local à une instance de cette interface)
     */
    private String identifiantTechniqueEntreprise;

    /**
     * Date de création de l'entreprise.
     * 
     * @return Date de création de l'entreprise
     */
    public String getDateCreationEntrepriseFormatee() {
        return DateRedac.formate(DateRedac.EXPORT_FORMAT_PAR_DEFAUT, dateCreationEntreprise);
    }

    /**
     * Date de première exploitation.
     * 
     * @return Date de première exploitation
     */
    public String getDatePremiereExploitationFormatee() {
        return DateRedac.formate(DateRedac.EXPORT_FORMAT_PAR_DEFAUT, datePremiereExploitation);
    }

    /**
     * Date de début de mise en sommeil.
     * 
     * @return Date de début de mise en sommeil
     */
    public String getDateDebutMiseEnSommeilFormatee() {
        return DateRedac.formate(DateRedac.EXPORT_FORMAT_PAR_DEFAUT, dateDebutMiseEnSommeil);
    }

    /**
     * Date de fin de mise en sommeil.
     * 
     * @return Date de fin de mise en sommeil
     */
    public String getDateFinMiseEnSommeilFormatee() {
        return DateRedac.formate(DateRedac.EXPORT_FORMAT_PAR_DEFAUT, dateFinMiseEnSommeil);
    }

    /**
     * Date de fin de cession partielle.
     * 
     * @return Date de fin de cession partielle
     */
    public String getDateFinCessionPartielleFormatee() {
        return DateRedac.formate(DateRedac.EXPORT_FORMAT_PAR_DEFAUT, dateFinCessionPartielle);
    }

    /**
     * Date de fin de plan de continuation.
     * 
     * @return Date de fin de plan de continuation
     */
    public String getDateFinPlanContinuationFormatee() {
        return DateRedac.formate(DateRedac.EXPORT_FORMAT_PAR_DEFAUT, dateFinPlanContinuation);
    }

    /**
     * Date de dissolution.
     * 
     * @return Date de dissolution
     */
    public String getDateDissolutionFormatee() {
        return DateRedac.formate(DateRedac.EXPORT_FORMAT_PAR_DEFAUT, dateDissolution);
    }

    /**
     * Date de radiation.
     * 
     * @return Date de radiation
     */
    public String getDateRadiationFormatee() {
        return DateRedac.formate(DateRedac.EXPORT_FORMAT_PAR_DEFAUT, dateRadiation);
    }

    /**
     * Date de cessation de l’activité.
     * 
     * @return Date de cessation de l’activité
     */
    public String getDateCessationActiviteFormatee() {
        return DateRedac.formate(DateRedac.EXPORT_FORMAT_PAR_DEFAUT, dateCessationActivite);
    }

    /**
     * Date première embauche.
     * 
     * @return Date première embauche
     */
    public String getDatePremiereEmbaucheFormatee() {
        return DateRedac.formate(DateRedac.EXPORT_FORMAT_PAR_DEFAUT, datePremiereEmbauche);
    }

    /**
     * Date de connaissance de l’entreprise par le Groupe Médéric.
     * 
     * @return Date de connaissance de l’entreprise par le Groupe Médéric
     */
    public String getDateConnaissanceFormatee() {
        return DateRedac.formate(DateRedac.EXPORT_FORMAT_PAR_DEFAUT, dateConnaissance);
    }

    /**
     * Date de début d’application du code APE.
     * 
     * @return Date de début d’application du code APE
     */
    public String getDateDebutApplicationCodeAPEFormatee() {
        return DateRedac.formate(DateRedac.EXPORT_FORMAT_PAR_DEFAUT, dateDebutApplicationCodeAPE);
    }

    /**
     * Date de fin d’application du code APE.
     * 
     * @return Date de fin d’application du code APE
     */
    public String getDateFinApplicationCodeAPEFormatee() {
        return DateRedac.formate(DateRedac.EXPORT_FORMAT_PAR_DEFAUT, dateFinApplicationCodeAPE);
    }

    /**
     * Date effet Raison sociale.
     * 
     * @return Date effet Raison sociale
     */
    public String getDateEffetRaisonSocialeFormatee() {
        return DateRedac.formate(DateRedac.EXPORT_FORMAT_PAR_DEFAUT, dateEffetRaisonSociale);
    }

    /**
     * Date de début d’application du décalage de paie.
     * 
     * @return Date de début d’application du décalage de paie
     */
    public String getDateDebutApplicationDecalagePaieFormatee() {
        return DateRedac.formate(DateRedac.EXPORT_FORMAT_PAR_DEFAUT, dateDebutApplicationDecalagePaie);
    }

    /**
     * Date de fin d’application du décalage de paie.
     * 
     * @return Date de fin d’application du décalage de paie
     */
    public String getDateFinApplicationDecalagePaieFormatee() {
        return DateRedac.formate(DateRedac.EXPORT_FORMAT_PAR_DEFAUT, dateFinApplicationDecalagePaie);
    }

}
