package fr.si2m.red.batch.moteur.trace;

import java.util.concurrent.TimeUnit;

import lombok.Setter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobExecutionListener;
import org.springframework.batch.core.annotation.AfterJob;
import org.springframework.batch.core.annotation.BeforeJob;

/**
 * Traceur des performances des jobs batchs.
 * 
 * @author nortaina
 *
 */
public class JobPerformanceTraceur implements JobExecutionListener {
    private static final Logger LOGGER = LoggerFactory.getLogger(JobPerformanceTraceur.class);

    /**
     * Activation / désactivation des traces de performance pour le traitement d'un job.
     * 
     * @param traceTraitementEtape
     *            true pour activer les traces de performance pour les traitements de jobs
     */
    @Setter
    private boolean traceTraitementJob;

    private String nomJob;

    private long debutJob;

    /**
     * Gestion des traces de performance en début de job.
     * 
     * @param jobExecution
     *            le contexte du job
     */
    @BeforeJob
    @Override
    public void beforeJob(JobExecution jobExecution) {
        if (!traceTraitementJob) {
            return;
        }
        this.nomJob = jobExecution.getJobInstance().getJobName();
        this.debutJob = System.nanoTime();
        LOGGER.info("Début du job {}", nomJob);

    }

    /**
     * Gestion des traces de performance en fin de job.
     * 
     * @param jobExecution
     *            le contexte du job
     */
    @AfterJob
    @Override
    public void afterJob(JobExecution jobExecution) {
        if (!traceTraitementJob) {
            return;
        }
        LOGGER.info("Job {} réalisé en {} ms", nomJob, TimeUnit.NANOSECONDS.toMillis(System.nanoTime() - debutJob));
    }
}
