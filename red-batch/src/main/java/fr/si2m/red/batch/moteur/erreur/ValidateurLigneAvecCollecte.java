package fr.si2m.red.batch.moteur.erreur;

import java.math.BigDecimal;
import java.text.MessageFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.si2m.red.EntiteImportableBatch;
import fr.si2m.red.batch.RedacMessages;
import fr.si2m.red.batch.moteur.CodeRetour;
import fr.si2m.red.batch.moteur.item.ValidateurLigne;

/**
 * Validateur spécifique avec collecte d'erreurs dans une liste.
 * 
 * @author nortaina
 *
 * @param <I>
 *            le type d'entité validé
 */
public abstract class ValidateurLigneAvecCollecte<I extends EntiteImportableBatch> extends ValidateurLigne<I, I> {
    private static final Logger LOGGER = LoggerFactory.getLogger(ValidateurLigneAvecCollecte.class);

    private static final String MESSAGE_TRACE_ERREUR = "Une erreur a été détectée sur le champ {}.{} : {}";

    @Override
    public final I process(I item) throws Exception {
        if (item.isLigneSyntaxiquementValide()) {
            valide(item);
        }
        return item;
    }

    /**
     * Valide et collecte les erreurs de validation éventuelles via des appels à
     * {@link ValidateurLigneAvecCollecte#valideChampAvecCollecte(boolean, EntiteImportableBatch, String, String)}.
     * 
     * @param item
     *            l'élément à valider
     */
    protected abstract void valide(I item);

    /**
     * Valide une ligne importé.
     * 
     * @param affirmation
     *            l'affirmation à valider
     * @param entiteImportee
     *            l'entité à valider
     * @param nomChamp
     *            le nom du champ potentiellement en erreur
     * @param messageErreur
     *            le message d'erreur si l'affirmation est fausse
     */
    protected void valideChampAvecCollecte(boolean affirmation, EntiteImportableBatch entiteImportee, String nomChamp, String messageErreur) {
        if (!affirmation) {
            LOGGER.error(MESSAGE_TRACE_ERREUR, entiteImportee.getClass().getSimpleName(), nomChamp, messageErreur);
            entiteImportee.enregistreErreurSurImport(messageErreur);
        }
    }

    /**
     * Valide une ligne importé en collectant des avertissements en cas d'invalidité.
     * 
     * @param affirmation
     *            l'affirmation à valider
     * @param entiteImportee
     *            l'entité à valider
     * @param nomChamp
     *            le nom du champ potentiellement en erreur
     * @param messageAvertissement
     *            le message d'avertissement si l'affirmation est fausse
     */
    protected void valideChampAvecCollecteSansBlocage(boolean affirmation, EntiteImportableBatch entiteImportee, String nomChamp,
            String messageAvertissement) {
        if (!affirmation) {
            LOGGER.warn(MESSAGE_TRACE_ERREUR, entiteImportee.getClass().getSimpleName(), nomChamp, messageAvertissement);
            entiteImportee.enregistreAvertissementSurImport(messageAvertissement);
            getStepExecution().getExecutionContext().put(CLE_FORCE_STATUT_EXECUTION, CodeRetour.ERREUR_FONCTIONNELLE_NON_BLOQUANTE);
        }
    }

    /**
     * Valide la taille d'un champ texte d'une ligne importé.
     * 
     * @param valeurChamp
     *            l'affirmation à valider
     * @param tailleMaximale
     *            la taille maximale du champ à vérifier
     * @param entiteImportee
     *            l'entité à valider
     * @param nomChamp
     *            le nom du champ potentiellement en erreur
     */
    protected void valideTailleMaximaleChampTexteAvecCollecte(String valeurChamp, int tailleMaximale, EntiteImportableBatch entiteImportee,
            String nomChamp) {
        String messageErreur = MessageFormat.format(RedacMessages.ERREUR_CHAMP_TAILLE_DEPASSEE, nomChamp);
        valideTailleMaximaleChampTexteAvecCollecte(valeurChamp, tailleMaximale, entiteImportee, nomChamp, messageErreur);
    }

    /**
     * Valide la taille d'un champ texte d'une ligne importé, avec précision du message d'erreur.
     * 
     * @param valeurChamp
     *            l'affirmation à valider
     * @param tailleMaximale
     *            la taille maximale du champ à vérifier
     * @param entiteImportee
     *            l'entité à valider
     * @param nomChamp
     *            le nom du champ potentiellement en erreur
     * @param messageErreur
     *            le message d'erreur si l'affirmation est fausse
     */
    protected void valideTailleMaximaleChampTexteAvecCollecte(String valeurChamp, int tailleMaximale, EntiteImportableBatch entiteImportee,
            String nomChamp, String messageErreur) {
        if (StringUtils.isBlank(valeurChamp)) {
            return;
        }
        if (valeurChamp.length() > tailleMaximale) {
            LOGGER.error(MESSAGE_TRACE_ERREUR, entiteImportee.getClass().getSimpleName(), nomChamp, messageErreur);
            entiteImportee.enregistreErreurSurImport(messageErreur);
        }
    }

    /**
     * Valide la taille fixe d'un champ texte d'une ligne importée.
     * 
     * @param valeurChamp
     *            l'affirmation à valider
     * @param tailleFixe
     *            la taille fixe attendue du champ à vérifier
     * @param entiteImportee
     *            l'entité à valider
     * @param nomChamp
     *            le nom du champ potentiellement en erreur
     */
    protected void valideTailleFixeChampAvecCollecte(String valeurChamp, int tailleFixe, EntiteImportableBatch entiteImportee, String nomChamp) {
        if (valeurChamp.length() != tailleFixe) {
            String messageErreur = MessageFormat.format(RedacMessages.ERREUR_CHAMP_TAILLE_NON_CONFORME, nomChamp);
            LOGGER.error(MESSAGE_TRACE_ERREUR, entiteImportee.getClass().getSimpleName(), nomChamp, messageErreur);
            entiteImportee.enregistreErreurSurImport(messageErreur);
        }
    }

    /**
     * Valide la taille fixe d'un champ numérique entier d'une ligne importé.
     * 
     * @param valeurChamp
     *            l'affirmation à valider
     * @param precision
     *            la précision maximale supportée pour ce champ
     * @param entiteImportee
     *            l'entité à valider
     * @param nomChamp
     *            le nom du champ potentiellement en erreur
     */
    protected void valideChampIntegerTailleFixeAvecCollecte(String valeurChamp, int precision, EntiteImportableBatch entiteImportee,
            String nomChamp) {
        // Validation format numérique
        boolean formatNumerique = true;
        if (StringUtils.isNotBlank(valeurChamp)) {
            try {
                Integer.parseInt(valeurChamp.trim());
            } catch (Exception e) {
                String messageChampMauvaisFormat = MessageFormat.format(RedacMessages.ERREUR_CHAMP_FORMAT_INCORRECT, nomChamp);
                LOGGER.error(MESSAGE_TRACE_ERREUR, entiteImportee.getClass().getSimpleName(), nomChamp, messageChampMauvaisFormat, e);
                entiteImportee.enregistreErreurSurImport(messageChampMauvaisFormat);
                formatNumerique = false;
            }
        }

        if (formatNumerique && valeurChamp.length() != precision) {
            String messageErreur = MessageFormat.format(RedacMessages.ERREUR_CHAMP_TAILLE_NON_CONFORME, nomChamp);
            LOGGER.error(MESSAGE_TRACE_ERREUR, entiteImportee.getClass().getSimpleName(), nomChamp, messageErreur);
            entiteImportee.enregistreErreurSurImport(messageErreur);
        }
    }

    /**
     * Valide la taille fixe d'un champ numérique entier (Long) d'une ligne importé.
     * 
     * @param valeurChamp
     *            l'affirmation à valider
     * @param precision
     *            la précision maximale supportée pour ce champ
     * @param entiteImportee
     *            l'entité à valider
     * @param nomChamp
     *            le nom du champ potentiellement en erreur
     */
    protected void valideChampLongTailleFixeAvecCollecte(String valeurChamp, int precision, EntiteImportableBatch entiteImportee, String nomChamp) {
        // Validation format numérique
        boolean formatNumerique = true;
        if (StringUtils.isNotBlank(valeurChamp)) {
            try {
                Long.parseLong(valeurChamp.trim());
            } catch (Exception e) {
                String messageChampMauvaisFormat = MessageFormat.format(RedacMessages.ERREUR_CHAMP_FORMAT_INCORRECT, nomChamp);
                LOGGER.error(MESSAGE_TRACE_ERREUR, entiteImportee.getClass().getSimpleName(), nomChamp, messageChampMauvaisFormat, e);
                entiteImportee.enregistreErreurSurImport(messageChampMauvaisFormat);
                formatNumerique = false;
            }
        }

        if (formatNumerique && valeurChamp.length() != precision) {
            String messageErreur = MessageFormat.format(RedacMessages.ERREUR_CHAMP_TAILLE_NON_CONFORME, nomChamp);
            LOGGER.error(MESSAGE_TRACE_ERREUR, entiteImportee.getClass().getSimpleName(), nomChamp, messageErreur);
            entiteImportee.enregistreErreurSurImport(messageErreur);
        }
    }

    /**
     * Vérifie si le nombre de chiffres de la partie entière et des décimales d'un parametre numérique (Double) sont egales respectivement aux parametres
     * nbPartieEntiere et nbPartieDecimale.
     * 
     * @param nombreATesterAsText
     *            objet nombre au format texte
     * @param nbPartieEntier
     *            longueur exacte attendue de la partie entiere
     * @param nbPartieDecimale
     *            longueur exacte attendue de la partie decimale
     * @param entiteImportee
     *            l'entité à valider
     * @param nomChamp
     *            le nom du champ décimal à valider
     */
    protected void valideChampNumeriqueDecimaleTailleFixeAvecCollecte(String nombreATesterAsText, Integer nbPartieEntier, Integer nbPartieDecimale,
            EntiteImportableBatch entiteImportee, String nomChamp) {
        if (StringUtils.isBlank(nombreATesterAsText)) {
            return;
        }

        // Validation format numérique
        boolean formatNumerique = true;
        if (StringUtils.isNotBlank(nombreATesterAsText)) {
            try {
                Double.parseDouble(nombreATesterAsText.trim());
            } catch (Exception e) {
                String messageChampMauvaisFormat = MessageFormat.format(RedacMessages.ERREUR_CHAMP_FORMAT_INCORRECT, nomChamp);
                LOGGER.error(MESSAGE_TRACE_ERREUR, entiteImportee.getClass().getSimpleName(), nomChamp, messageChampMauvaisFormat, e);
                entiteImportee.enregistreErreurSurImport(messageChampMauvaisFormat);
                formatNumerique = false;
            }
        }

        if (formatNumerique) {
            if (nombreATesterAsText.length() != (nbPartieEntier + nbPartieDecimale + 1)) {
                String messageErreur = MessageFormat.format(RedacMessages.ERREUR_CHAMP_TAILLE_NON_CONFORME, nomChamp);
                LOGGER.error(MESSAGE_TRACE_ERREUR, entiteImportee.getClass().getSimpleName(), nomChamp, messageErreur);
                entiteImportee.enregistreErreurSurImport(messageErreur);
            } else if (!valideCadrageEtFormatNumeriqueDecimal(nombreATesterAsText, nbPartieDecimale)) {
                String messageChampMauvaisFormat = MessageFormat.format(RedacMessages.ERREUR_CHAMP_FORMAT_INCORRECT, nomChamp);
                LOGGER.error(MESSAGE_TRACE_ERREUR, entiteImportee.getClass().getSimpleName(), nomChamp, messageChampMauvaisFormat);
                entiteImportee.enregistreErreurSurImport(messageChampMauvaisFormat);
            }
        }
    }

    /**
     * Indique si le cadrage et le format d'un champ donné est valide
     * 
     * @param nombreATesterAsText
     *            la valeur textuelle du champ à valider
     * @param nbPartieDecimale
     *            la longueur nominale de la partie décimale du champ
     * @return true si le champ est valide, false sinon
     */
    private boolean valideCadrageEtFormatNumeriqueDecimal(String nombreATesterAsText, int nbPartieDecimale) {
        boolean nombreOK = true;
        int partieEntiere = nombreATesterAsText.indexOf('.');
        int partieDecimale = StringUtils.substringAfter(nombreATesterAsText, ".").trim().length();

        if (partieEntiere < 1 || partieDecimale != nbPartieDecimale) {
            // le séparateur décimal (point) est en première position ou n'est pas présent,
            // la partie décimale n'a pas 2 chiffres de longueur
            nombreOK = false;
        } else if (nombreATesterAsText.startsWith(" ") && nombreATesterAsText.endsWith(" ")) {
            // nombre cadré ni à gauche ni à droite
            nombreOK = false;
        } else if (nombreATesterAsText.trim().startsWith("+.") || nombreATesterAsText.trim().startsWith("-.")) {
            // nombre cadré à gauche ou à droite, mais commencant par un signe directement suivi du point
            nombreOK = false;
        }

        return nombreOK;
    }

    /**
     * Valide le champ date d'une ligne importé.
     * 
     * @param dateAsText
     *            la date sous forme de chaîne de caractères à valider
     * @param formatDate
     *            le format de date pour validation
     * @param entiteImportee
     *            l'entité à valider
     * @param nomChamp
     *            le nom du champ date à valider
     * @param champObligatoire
     *            si le champ date est obligatoire
     */
    protected void valideChampDateTailleFixeAvecCollecte(String dateAsText, String formatDate, EntiteImportableBatch entiteImportee, String nomChamp,
            boolean champObligatoire) {
        // Validation format numérique
        int dateAsInt = 0;
        boolean formatNumerique = true;
        if (StringUtils.isNotBlank(dateAsText)) {
            try {
                dateAsInt = Integer.parseInt(dateAsText);
            } catch (Exception e) {
                String messageChampMauvaisFormat = MessageFormat.format(RedacMessages.ERREUR_CHAMP_FORMAT_INCORRECT, nomChamp);
                LOGGER.error(MESSAGE_TRACE_ERREUR, entiteImportee.getClass().getSimpleName(), nomChamp, messageChampMauvaisFormat, e);
                entiteImportee.enregistreErreurSurImport(messageChampMauvaisFormat);
                formatNumerique = false;
            }
        }

        // Validation présence
        if (StringUtils.isBlank(dateAsText) || (dateAsInt == 0 && formatNumerique)) {
            if (champObligatoire) {
                String messageChampObligatoire = MessageFormat.format(RedacMessages.ERREUR_CHAMP_OBLIGATOIRE, nomChamp);
                LOGGER.error(MESSAGE_TRACE_ERREUR, entiteImportee.getClass().getSimpleName(), nomChamp, messageChampObligatoire);
                entiteImportee.enregistreErreurSurImport(messageChampObligatoire);
            }
            return;
        }

        if (formatNumerique) {
            valideTailleEtValeurDate(dateAsText, formatDate, entiteImportee, nomChamp);
        }
    }

    /**
     * Indique si la taille et la valeur d'un champ date donné est valide.
     * 
     * @param dateAsText
     *            la valeur textuelle du champ
     * @param formatDate
     *            le format de date attendu
     * @param entiteImportee
     *            l'entité
     * @param nomChamp
     *            le nom du champ
     */
    private void valideTailleEtValeurDate(String dateAsText, String formatDate, EntiteImportableBatch entiteImportee, String nomChamp) {
        // Validation taille
        int tailleFormatAttendu = StringUtils.replace(formatDate, "'", StringUtils.EMPTY).length();
        if (dateAsText.length() != tailleFormatAttendu) {
            String messageChampMauvaiseTaille = MessageFormat.format(RedacMessages.ERREUR_CHAMP_TAILLE_NON_CONFORME, nomChamp);
            LOGGER.error(MESSAGE_TRACE_ERREUR, entiteImportee.getClass().getSimpleName(), nomChamp, messageChampMauvaiseTaille);
            entiteImportee.enregistreErreurSurImport(messageChampMauvaiseTaille);
            return;
        }

        // Validation format de date
        SimpleDateFormat format = new SimpleDateFormat(formatDate);
        format.setLenient(false);
        try {
            format.parse(dateAsText);
        } catch (ParseException e) {
            String messageChampMauvaisFormat = MessageFormat.format(RedacMessages.ERREUR_CHAMP_FORMAT_INCORRECT, nomChamp);
            LOGGER.error(MESSAGE_TRACE_ERREUR, entiteImportee.getClass().getSimpleName(), nomChamp, messageChampMauvaisFormat, e);
            entiteImportee.enregistreErreurSurImport(messageChampMauvaisFormat);
        }
    }

    /**
     * Valide le champ date d'une ligne importé.
     * 
     * @param dateNumerique
     *            la date au format numérique à valider
     * @param formatDate
     *            le format de date pour validation
     * @param entiteImportee
     *            l'entité à valider
     * @param nomChamp
     *            le nom du champ date à valider
     * @param champObligatoire
     *            si le champ date est obligatoire
     */
    protected void valideChampDateTailleFixeAvecCollecte(Integer dateNumerique, String formatDate, EntiteImportableBatch entiteImportee,
            String nomChamp, boolean champObligatoire) {
        valideChampDateTailleFixeAvecCollecte(dateNumerique.toString(), formatDate, entiteImportee, nomChamp, champObligatoire);
    }

    /**
     * Valide la taille d'un champ numérique entier d'une ligne importé.
     * 
     * @param valeurChamp
     *            l'affirmation à valider
     * @param precision
     *            la précision maximale supportée pour ce champ
     * @param entiteImportee
     *            l'entité à valider
     * @param nomChamp
     *            le nom du champ potentiellement en erreur
     */
    protected void valideTailleMaximaleChampAvecCollecte(Integer valeurChamp, int precision, EntiteImportableBatch entiteImportee, String nomChamp) {
        String messageErreur = MessageFormat.format(RedacMessages.ERREUR_CHAMP_TAILLE_DEPASSEE, nomChamp);
        valideTailleMaximaleChampAvecCollecte(valeurChamp, precision, entiteImportee, nomChamp, messageErreur);
    }

    /**
     * Valide la taille d'un champ numérique entier d'une ligne importé, avec précision du message d'erreur.
     * 
     * @param valeurChamp
     *            l'affirmation à valider
     * @param precision
     *            la précision maximale supportée pour ce champ
     * @param entiteImportee
     *            l'entité à valider
     * @param nomChamp
     *            le nom du champ potentiellement en erreur
     * @param messageErreur
     *            le message d'erreur si l'affirmation est fausse
     */
    protected void valideTailleMaximaleChampAvecCollecte(Integer valeurChamp, int precision, EntiteImportableBatch entiteImportee, String nomChamp,
            String messageErreur) {
        if (valeurChamp == null) {
            return;
        }
        if (valeurChamp >= Math.pow(10, precision)) {
            LOGGER.error(MESSAGE_TRACE_ERREUR, entiteImportee.getClass().getSimpleName(), nomChamp, messageErreur);
            entiteImportee.enregistreErreurSurImport(messageErreur);
        }
    }

    /**
     * Valide la taille d'un champ numérique entier d'une ligne importé.
     * 
     * @param valeurChamp
     *            l'affirmation à valider
     * @param precision
     *            la précision maximale supportée pour ce champ
     * @param entiteImportee
     *            l'entité à valider
     * @param nomChamp
     *            le nom du champ potentiellement en erreur
     */
    protected void valideTailleMaximaleChampAvecCollecte(Long valeurChamp, int precision, EntiteImportableBatch entiteImportee, String nomChamp) {
        String messageErreur = MessageFormat.format(RedacMessages.ERREUR_CHAMP_TAILLE_DEPASSEE, nomChamp);
        valideTailleMaximaleChampAvecCollecte(valeurChamp, precision, entiteImportee, nomChamp, messageErreur);
    }

    /**
     * Valide la taille d'un champ numérique entier d'une ligne importé, avec précision du message d'erreur.
     * 
     * @param valeurChamp
     *            l'affirmation à valider
     * @param precision
     *            la précision maximale supportée pour ce champ
     * @param entiteImportee
     *            l'entité à valider
     * @param nomChamp
     *            le nom du champ potentiellement en erreur
     * @param messageErreur
     *            le message d'erreur si l'affirmation est fausse
     */
    protected void valideTailleMaximaleChampAvecCollecte(Long valeurChamp, int precision, EntiteImportableBatch entiteImportee, String nomChamp,
            String messageErreur) {
        if (valeurChamp == null) {
            return;
        }
        if (valeurChamp >= Math.pow(10, precision)) {
            LOGGER.error(MESSAGE_TRACE_ERREUR, entiteImportee.getClass().getSimpleName(), nomChamp, messageErreur);
            entiteImportee.enregistreErreurSurImport(messageErreur);
        }
    }

    /**
     * Valide le champ date d'une ligne importé.
     * 
     * @param dateNumerique
     *            la date au format numérique à valider
     * @param formatDate
     *            le format de date pour validation
     * @param entiteImportee
     *            l'entité à valider
     * @param nomChamp
     *            le nom du champ date à valider
     * @param champObligatoire
     *            si le champ date est obligatoire
     * @return la valeur de date valide (ou l'alternative par défaut) à prendre en compte pour l'import
     */
    protected Integer valideChampDateAvecCollecte(Integer dateNumerique, String formatDate, EntiteImportableBatch entiteImportee, String nomChamp,
            boolean champObligatoire) {
        // Validation présence
        if (verifieDateVide(dateNumerique)) {
            if (champObligatoire) {
                String messageChampObligatoire = MessageFormat.format(RedacMessages.ERREUR_CHAMP_OBLIGATOIRE, nomChamp);
                LOGGER.error(MESSAGE_TRACE_ERREUR, entiteImportee.getClass().getSimpleName(), nomChamp, messageChampObligatoire);
                entiteImportee.enregistreErreurSurImport(messageChampObligatoire);
            }
            return null;
        }

        // Validation taille supérieure
        int tailleFormatAttendu = StringUtils.replace(formatDate, "'", StringUtils.EMPTY).length();
        if (dateNumerique.toString().length() > tailleFormatAttendu) {
            String messageTailleMax = MessageFormat.format(RedacMessages.ERREUR_CHAMP_TAILLE_DEPASSEE, nomChamp);
            LOGGER.error(MESSAGE_TRACE_ERREUR, entiteImportee.getClass().getSimpleName(), nomChamp, messageTailleMax);
            entiteImportee.enregistreErreurSurImport(messageTailleMax);
            return null;
        }
        // Message d'erreur validations format et taille inférieure
        String messageChampMauvaisFormat = MessageFormat.format(RedacMessages.ERREUR_CHAMP_FORMAT_INCORRECT, nomChamp);
        // Validation taille inférieure
        if (dateNumerique.toString().length() < tailleFormatAttendu) {
            LOGGER.error(MESSAGE_TRACE_ERREUR, entiteImportee.getClass().getSimpleName(), nomChamp, messageChampMauvaisFormat);
            entiteImportee.enregistreErreurSurImport(messageChampMauvaisFormat);
            return null;
        }
        // Validation format
        SimpleDateFormat format = new SimpleDateFormat(formatDate);
        format.setLenient(false);
        try {
            format.parse(dateNumerique.toString());
        } catch (ParseException e) {
            LOGGER.error(MESSAGE_TRACE_ERREUR, entiteImportee.getClass().getSimpleName(), nomChamp, messageChampMauvaisFormat);
            entiteImportee.enregistreErreurSurImport(messageChampMauvaisFormat);
            return null;
        }

        return dateNumerique;
    }

    /**
     * Teste si le champ date est vide.
     * 
     * @param dateNumerique
     *            la date au format numérique à tester
     * @return true si la date est vide, false sinon
     */
    protected boolean verifieDateVide(Integer dateNumerique) {
        return dateNumerique == null || dateNumerique == 0;
    }

    /**
     * Teste si le champ booléen est au format O/N.
     * 
     * @param champBooleen
     *            la valeur du champ au format booléen à tester
     * @return true si la valeur est correcte, false sinon
     */
    protected boolean verifieBooleenON(String champBooleen) {
        return "O".equalsIgnoreCase(champBooleen) || "N".equalsIgnoreCase(champBooleen);
    }

    /**
     * Teste si le champ booléen est au format Oui/Non.
     * 
     * @param champBooleen
     *            la valeur du champ au format booléen à tester
     * @return true si la valeur est correcte, false sinon
     */
    protected boolean verifieBooleenOuiNon(String champBooleen) {
        return "OUI".equalsIgnoreCase(champBooleen) || "NON".equalsIgnoreCase(champBooleen);
    }

    /**
     * Vérifie si le nombre de chiffres de la partie entière et des décimales d'un parametre numérique (Double) sont egales respectivement aux parametres
     * nbPartieEntiere et nbPartieDecimale.
     * 
     * @param nombreATester
     *            objet de type integer
     * @param nbPartieEntier
     *            longueur maxi de la partie entiere
     * @param nbPartieDecimale
     *            longueur maxi de la partie decimale
     * @param entiteImportee
     *            l'entité à valider
     * @param nomChamp
     *            le nom du champ décimal à valider
     * @param messageErreur
     *            le message d'erreur si l'affirmation est fausse
     */
    protected void valideChampNumeriqueDecimaleAvecCollecte(Double nombreATester, Integer nbPartieEntier, Integer nbPartieDecimale,
            EntiteImportableBatch entiteImportee, String nomChamp, String messageErreur) {
        if (nombreATester == null) {
            return;
        }
        String laValeur = "";
        try {
            laValeur = BigDecimal.valueOf(nombreATester).toPlainString();
        } catch (Exception e) {
            String messageMauvaisFormat = MessageFormat.format(RedacMessages.ERREUR_CHAMP_FORMAT_INCORRECT, nomChamp);
            LOGGER.error(MESSAGE_TRACE_ERREUR, entiteImportee.getClass().getSimpleName(), nomChamp, messageMauvaisFormat, e);
            entiteImportee.enregistreErreurSurImport(messageMauvaisFormat);
        }

        int partieEntiere = laValeur.indexOf('.');
        int partieDecimale = laValeur.length() - partieEntiere - 1;
        if (!(partieEntiere <= nbPartieEntier && partieDecimale <= nbPartieDecimale)) {
            LOGGER.error(MESSAGE_TRACE_ERREUR, entiteImportee.getClass().getSimpleName(), nomChamp, messageErreur);
            entiteImportee.enregistreErreurSurImport(messageErreur);
        }
    }

}
