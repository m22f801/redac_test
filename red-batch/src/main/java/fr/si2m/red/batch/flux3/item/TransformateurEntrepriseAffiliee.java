package fr.si2m.red.batch.flux3.item;

import fr.si2m.red.batch.flux3.ligne.Entreprise;
import fr.si2m.red.batch.moteur.item.TransformateurDonnee;
import fr.si2m.red.complement.ExtensionEntrepriseAffiliee;
import fr.si2m.red.complement.ExtensionEntrepriseAffilieeRepository;
import fr.si2m.red.contrat.ContratRepository;
import lombok.Setter;

/**
 * Mapper des entités Entreprise (Affiliée) à partir du numéro de Siren fourni.
 * 
 * @author benitahy
 *
 */
public class TransformateurEntrepriseAffiliee extends TransformateurDonnee<String, Entreprise> {

    /**
     * Référentiel des contrats
     */
    @Setter
    private ContratRepository contratRepository;

    /**
     * Référentiel des entreprises complémentaires.
     * 
     * @param extensionEntrepriseAffilieeRepository
     *            le référentiel des entreprises affiliées
     */
    @Setter
    private ExtensionEntrepriseAffilieeRepository extensionEntrepriseAffilieeRepository;

    @Override
    public Entreprise process(String numSiren) throws Exception {
        ExtensionEntrepriseAffiliee extensionEntrepriseAffiliee = extensionEntrepriseAffilieeRepository
                .getExtensionEntrepriseAffilieeComplementaire(numSiren);
        if (extensionEntrepriseAffiliee == null) {
            return null;
        }
        Entreprise entreprise = new Entreprise();
        entreprise.setNumSiren(extensionEntrepriseAffiliee.getSiren());
        entreprise.setCodeMiseAJour("R");
        entreprise.setCodeEntrepriseVIP("N");
        entreprise.setIdentifiantTechniqueEntreprise(extensionEntrepriseAffiliee.getSiren());
        entreprise.setNumIdentificationEntrepriseDansOrganisme(0);
        entreprise.setRaisonSocialeEntreprise(contratRepository.getRaisonSocialeClientContrat(extensionEntrepriseAffiliee.getNumContrat()));
        return entreprise;
    }

}
