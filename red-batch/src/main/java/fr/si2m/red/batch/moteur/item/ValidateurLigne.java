package fr.si2m.red.batch.moteur.item;

import org.springframework.batch.item.ItemProcessor;

import fr.si2m.red.batch.moteur.EtapeCodeRetourModificateur;

/**
 * Gestionnaire de validation d'une ligne d'information REDAC.<br/>
 * <br/>
 * Permet également de traiter la collecte d'erreurs si traitement de type "annuler / remplacer".
 * 
 * @author nortaina
 *
 * @param <I>
 *            type de la ligne à valider
 * @param <O>
 *            type du retour de la validation
 */
public abstract class ValidateurLigne<I, O> extends EtapeCodeRetourModificateur implements ItemProcessor<I, O> {

}
