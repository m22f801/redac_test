package fr.si2m.red.batch.flux10.item;

import org.apache.commons.lang3.StringUtils;

import fr.si2m.red.DateRedac;
import fr.si2m.red.batch.moteur.erreur.ValidateurLigneAvecCollecte;
import fr.si2m.red.parametrage.ParamContact;

/**
 * Validateur de ligne .
 * 
 * @author delortj
 *
 */
public class ParamContactValidateurLigne extends ValidateurLigneAvecCollecte<ParamContact> {

    @Override
    protected void valide(final ParamContact contact) {
        // Champs obligatoires

        valideChampAvecCollecte(StringUtils.isNotBlank(contact.getIdentifiantContact()), contact, "CONTACT",
                "le champ CONTACT obligatoire, n’est pas renseigné");

        // Format de la date de début
        contact.setDebut(valideChampDateAvecCollecte(contact.getDebut(), DateRedac.FORMAT_DATES, contact, "DEBUT", true));

        contact.setFin(valideChampDateAvecCollecte(contact.getFin(), DateRedac.FORMAT_DATES, contact, "FIN", false));

        // Longueur des champs

        valideChampAvecCollecte(contact.getCodeMaj().length() <= 1, contact, "CODE_MAJ", "le champ CODE_MAJ dépasse la taille maximale prévue");

        if (StringUtils.isNotBlank(contact.getIdentifiantContact())) {
            valideChampAvecCollecte(contact.getIdentifiantContact().length() <= 30, contact, "CONTACT",
                    "le champ CONTACT dépasse la taille maximale prévue");
        }

        valideChampAvecCollecte(contact.getTypeEnregistrement().length() <= 1, contact, "TYPE_ENREG",
                "le champ TYPE_ENREG dépasse la taille maximale prévue");

        valideChampAvecCollecte(contact.getIdentifiantEntreprise().length() <= 14, contact, "ENTREPRISE",
                "le champ ENTREPRISE dépasse la taille maximale prévue");

        valideChampAvecCollecte(contact.getIdentifiantEtablissement().length() <= 14, contact, "ETABLISSEMENT",
                "le champ ETABLISSEMENT dépasse la taille maximale prévue");

        valideChampAvecCollecte(contact.getRoleGestionnaire().length() <= 1, contact, "ROLE_GEST",
                "le champ ROLE_GEST dépasse la taille maximale prévue");

        valideChampAvecCollecte(contact.getRoleTechnique().length() <= 1, contact, "ROLE_TECH",
                "le champ ROLE_TECH dépasse la taille maximale prévue");

        valideChampAvecCollecte(contact.getNomContact().length() <= 80, contact, "NOM_CONTACT",
                "le champ NOM_CONTACT dépasse la taille maximale prévue");

        valideChampAvecCollecte(contact.getTelephone().length() <= 20, contact, "TELEPHONE", "le champ TELEPHONE dépasse la taille maximale prévue");

        valideChampAvecCollecte(contact.getComplementTelephone().length() <= 80, contact, "COMPL_TEL",
                "le champ COMPL_TEL dépasse la taille maximale prévue");

        valideChampAvecCollecte(contact.getFax().length() <= 20, contact, "FAX ", "le champ FAX dépasse la taille maximale prévue");

        valideChampAvecCollecte(contact.getEmail().length() <= 100, contact, "EMAIL", "le champ EMAIL dépasse la taille maximale prévue");

        // Ajustement de l'entité si validation OK
        if (verifieDateVide(contact.getFin())) {
            contact.setFin(null);
        }
    }
}
