package fr.si2m.red.batch.moteur.support;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.si2m.red.batch.moteur.BatchConfiguration;

/**
 * Classe utilitaire des logs
 *
 */
public class LogUtils {

    private static final Logger LOGGER = LoggerFactory.getLogger(LogUtils.class);

    /**
     * Constructeur par défaut
     */
    private LogUtils() {
        super();
    }

    /**
     * Trace les logs dans le fichier avec niveau log erreur
     * 
     * @param messageAvertissement
     *            message à tracer
     * @param cheminAbsoluFichierLogErreurs
     *            chemin absolu vers le fichier de log
     */
    public static void traceErreurDansFichierLog(String messageAvertissement, String cheminAbsoluFichierLogErreurs) {
        traceDansFichierLog(messageAvertissement, true, cheminAbsoluFichierLogErreurs);
    }

    /**
     * Trace les logs dans le fichier avec niveau de log erreur ou info
     * 
     * @param message
     *            le message à tracer
     * @param erreur
     *            si niveau de log = erreur si true, info sinon
     * @param cheminAbsoluFichierLogErreurs
     *            chemin absolu vers le fichier de log
     */
    private static void traceDansFichierLog(String message, boolean erreur, String cheminAbsoluFichierLogErreurs) {
        if (erreur) {
            LOGGER.error(message);
        } else {
            LOGGER.info(message);
        }

        SimpleDateFormat formateurDate = new SimpleDateFormat("yyyyMMdd HH:mm:ss");
        String date = formateurDate.format(Calendar.getInstance().getTime());

        if (StringUtils.isBlank(cheminAbsoluFichierLogErreurs)) {
            LOGGER.warn("Aucun chemin de fichier de logs d'erreur spécifié");
            return;
        }

        PrintWriter redacteurFichier = null;
        try {
            File fichierLogErreurs = new File(cheminAbsoluFichierLogErreurs);
            if (!fichierLogErreurs.exists()) {
                boolean fichierCree = fichierLogErreurs.createNewFile();
                if (!fichierCree) {
                    LOGGER.error("Impossible de créer le fichier de log d'erreurs");
                    return;
                }
            }
            FileOutputStream fluxSortantFichier = new FileOutputStream(fichierLogErreurs, true);
            OutputStreamWriter redacteurFluxSortantFichier = new OutputStreamWriter(fluxSortantFichier, BatchConfiguration.ENCODAGE_FICHIERS_LOGS);
            redacteurFichier = new PrintWriter(redacteurFluxSortantFichier, false);
            redacteurFichier.write(date + " " + message);
            redacteurFichier.println();
        } catch (Exception e) {
            LOGGER.error("Erreur lors du log d'une erreur", e);
            return;
        } finally {
            if (redacteurFichier != null) {
                redacteurFichier.flush();
            }
            IOUtils.closeQuietly(redacteurFichier);
        }

    }
}