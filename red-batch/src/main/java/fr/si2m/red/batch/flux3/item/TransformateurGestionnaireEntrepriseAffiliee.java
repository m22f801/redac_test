package fr.si2m.red.batch.flux3.item;

import java.util.List;

import fr.si2m.red.batch.flux3.ligne.Gestionnaire;
import fr.si2m.red.batch.moteur.item.TransformateurDonnee;
import fr.si2m.red.parametrage.ParamUtilisateurGestionnaire;
import fr.si2m.red.reconciliation.ParamGestionnaireUtilisateurRepository;
import lombok.Setter;

/**
 * Mapper des entités Gestionnaire à partir du siren fourni
 * 
 * cf F03_RG_S13
 * 
 * @author eudesr
 *
 */
public class TransformateurGestionnaireEntrepriseAffiliee extends TransformateurDonnee<String, Gestionnaire> {

    @Setter
    private ParamGestionnaireUtilisateurRepository paramUtilisateurGestionnaireRepository;

    @Override
    public Gestionnaire process(String numSiren) throws Exception {

        // F03_RG_S13
        Gestionnaire gestionnaire = new Gestionnaire();
        gestionnaire.setCodeMiseAJour("R");
        gestionnaire.setIdentifiantTechniqueEntreprise(numSiren);
        gestionnaire.setTypeMaille(1);
        gestionnaire.setReferenceMaille(numSiren);
        gestionnaire.setTypeIntervenant("1");
        List<ParamUtilisateurGestionnaire> param_gestionnaire = null;
        param_gestionnaire = paramUtilisateurGestionnaireRepository.rechercheGestionnaireSiren(numSiren);
        if (param_gestionnaire.get(0).getIdentifiantActiveDirectory() != null) {
            gestionnaire.setReferenceGestionnaire(param_gestionnaire.get(0).getIdentifiantActiveDirectory());
            gestionnaire.setLibelle(param_gestionnaire.get(0).getIdentifiantActiveDirectory());
            gestionnaire.setPrenomGestionnaireCompte(param_gestionnaire.get(0).getPrenom());
            gestionnaire.setNomGestionnaireCompte(param_gestionnaire.get(0).getNom());
            gestionnaire.setUniteGestion(param_gestionnaire.get(0).getNiveau1());
            gestionnaire.setService(param_gestionnaire.get(0).getNiveau2());
            gestionnaire.setCentreGestion(param_gestionnaire.get(0).getNiveau3());
            gestionnaire.setIdentifiantTechniqueContrat("");
            return gestionnaire;
        } else {
            return null;
        }

    }

}
