package fr.si2m.red.batch.flux1.execution;

import java.io.File;
import java.text.MessageFormat;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParameter;
import org.springframework.batch.core.JobParameters;
import org.springframework.context.support.GenericXmlApplicationContext;

import fr.si2m.red.batch.RedacMessages;
import fr.si2m.red.batch.moteur.CodeRetour;
import fr.si2m.red.batch.moteur.execution.ExecuteurBatch;
import fr.si2m.red.batch.moteur.execution.FacadeExecuteurBatch;
import fr.si2m.red.contrat.ClientRepository;
import fr.si2m.red.contrat.ContratRepository;
import fr.si2m.red.contrat.IntermediaireRepository;
import fr.si2m.red.contrat.TarifRepository;

/**
 * Exécuteur du batch du flux 1.
 * 
 * @author nortaina
 *
 */
@FacadeExecuteurBatch(nomBatch = "flux1")
public class Flux1ExecuteurBatch extends ExecuteurBatch {
    /**
     * Ordre du paramètre de lancement "emplacement du répertoire des fichiers".
     */
    public static final int ORDRE_PARAM_EMPLACEMENT_REPERTOIRE_FICHIERS = 0;
    /**
     * Ordre du paramètre de lancement "ID du flux à déclencher".
     */
    public static final int ORDRE_PARAM_ID_FLUX = 1;

    /**
     * Ordre du paramètre de mode de chargement
     */
    public static final int ORDRE_PARAM_MODE_CHARGEMENT = 2;

    /**
     * Ordre du paramètre de lancement "emplacement du fichier de log d'erreurs".
     */
    public static final int ORDRE_PARAM_EMPLACEMENT_FICHIER_ERREURS = 3;

    private static final Logger LOGGER = LoggerFactory.getLogger(Flux1ExecuteurBatch.class);

    private static final String ID_FLUX_TPL = "{ID_FLUX}";
    private static final String EXTENSION_FICHIERS = "ent";
    private static final String FICHIER_CONTRATS_TPL = "CONTRAT." + ID_FLUX_TPL + "." + EXTENSION_FICHIERS;
    private static final String FICHIER_CLIENTS_TPL = "EXTCLI." + ID_FLUX_TPL + "." + EXTENSION_FICHIERS;
    private static final String FICHIER_TARIFS_TPL = "FICTARIF." + ID_FLUX_TPL + "." + EXTENSION_FICHIERS;
    private static final String FICHIER_INTERMEDIAIRES_TPL = "INTER." + ID_FLUX_TPL + "." + EXTENSION_FICHIERS;

    private final String repertoireFichiers;
    private final String emplacementRessourceRepertoireFichiers;
    private final String nomFichierContrats;
    private final String nomFichierTarifs;
    private final String nomFichierClients;
    private final String nomFichierIntermediaires;
    private final String modeChargement;

    /**
     * Exécuteur de batch du flux 1 prenant en compte les fichiers contrats, tarifs, Clients et intermédiaires centralisés dans un répertoire donnés.
     * 
     * @param parametrageBatch
     *            les paramètres d'exécution du batch - deux paramètres sont attendus en entrée :
     *            <ol>
     *            <li>le chemin complet du répertoire où sont regroupés les fichiers à réceptionner pour le flux 1</li>
     *            <li>l'identifiant partagé par les fichiers (exemple : D150722A pour CONTRAT.D150722A.txt)</li>
     *            </ol>
     */
    public Flux1ExecuteurBatch(String[] parametrageBatch) {
        super(parametrageBatch);

        this.repertoireFichiers = parametrageBatch[ORDRE_PARAM_EMPLACEMENT_REPERTOIRE_FICHIERS];
        String idFlux = parametrageBatch[ORDRE_PARAM_ID_FLUX];
        this.emplacementRessourceRepertoireFichiers = tranformeEmplacementRessourceFichier(repertoireFichiers) + File.separator;

        this.nomFichierContrats = StringUtils.replace(FICHIER_CONTRATS_TPL, ID_FLUX_TPL, idFlux);
        this.nomFichierTarifs = StringUtils.replace(FICHIER_TARIFS_TPL, ID_FLUX_TPL, idFlux);
        this.nomFichierClients = StringUtils.replace(FICHIER_CLIENTS_TPL, ID_FLUX_TPL, idFlux);
        this.nomFichierIntermediaires = StringUtils.replace(FICHIER_INTERMEDIAIRES_TPL, ID_FLUX_TPL, idFlux);

        this.modeChargement = parametrageBatch[ORDRE_PARAM_MODE_CHARGEMENT];

        String cheminAbsoluFichierLogErreurs = parametrageBatch[ORDRE_PARAM_EMPLACEMENT_FICHIER_ERREURS];
        setCheminAbsoluFichierLogErreurs(cheminAbsoluFichierLogErreurs);
    }

    @Override
    protected String getEmplacementFichierConfigurationJob() {
        return "classpath:fr/si2m/red/batch/flux1/jobs.xml";
    }

    @Override
    protected String getIdJobCible() {
        return "jobFlux1";
    }

    @Override
    protected boolean lanceTraitementAvantJob(GenericXmlApplicationContext contexteExecutionJob) {

        if (!validationFichiers(contexteExecutionJob)) {
            return false;
        }

        // F01_RG_G4
        List<String> modesChargementValides = Arrays.asList("normal", "populationEtendue");
        if (!modesChargementValides.contains(this.modeChargement)) {
            String messageErreur = "Le paramètre mode de chargement est incorrect";
            LOGGER.error(messageErreur);
            traceErreurDansFichierLog(messageErreur);
            return false;
        }

        videEspacesTemporaires(contexteExecutionJob,
                Arrays.asList(ContratRepository.class, TarifRepository.class, IntermediaireRepository.class, ClientRepository.class));

        return true;
    }

    private boolean verifieExtensionsFichiers(File[] fichiers, List<String> nomFichiersAttendus) {
        boolean toutesExtensionsCorrectes = true;
        for (File fichier : fichiers) {
            if (fichier.isFile()) {
                toutesExtensionsCorrectes &= verifieExtensionFichier(fichier, nomFichiersAttendus);
            }
        }
        return toutesExtensionsCorrectes;
    }

    /**
     * Vérifie que le fichier donné possède la bonne extension si son nom matche un nom de fichier attendu.
     * 
     * @param fichier
     *            le fichier à valider
     * @param nomsFichiersAttendus
     *            les noms de fichiers attendus
     * @return true si l'extension du fichier est valide, false sinon
     */
    private boolean verifieExtensionFichier(File fichier, List<String> nomsFichiersAttendus) {
        String nomFichier = StringUtils.substringBeforeLast(fichier.getName(), ".");
        String extensionFichier = StringUtils.substringAfterLast(fichier.getName(), ".");
        for (String nomFichierAttendu : nomsFichiersAttendus) {
            if (nomFichierAttendu.startsWith(nomFichier) && !EXTENSION_FICHIERS.equals(extensionFichier)) {
                String emplacementFichier = repertoireFichiers + File.separator + fichier.getName();
                String messageErreur = MessageFormat.format(RedacMessages.ERREUR_FICHIER_EXTENSION_INCORRECTE, emplacementFichier);
                traceErreurDansFichierLog(messageErreur);
                return false;
            }
        }
        return true;
    }

    /**
     * Effectue les tests sur les fichiers entrants
     * 
     * @param contexteExecutionJob
     *            le contexte d'exécution du job
     * 
     * @return true si les fichiers sont ok, false sinon.
     * 
     */
    private boolean validationFichiers(GenericXmlApplicationContext contexteExecutionJob) {
        List<String> nomFichiersAttendus = Arrays.asList(nomFichierContrats, nomFichierTarifs, nomFichierClients, nomFichierIntermediaires);

        // Test de l'existence du répertoire
        if (!verifieExistencePhysiqueRessource(contexteExecutionJob, emplacementRessourceRepertoireFichiers)) {
            LOGGER.error("Répertoire manquant pour démarrer le batch : {}", emplacementRessourceRepertoireFichiers);
            return false;
        }

        // Récupération des fichiers du répertoire
        File repertoire = new File(this.repertoireFichiers);
        File[] fichiers = repertoire.listFiles();
        if (fichiers == null) {
            LOGGER.error("Pas de fichiers dans le répertoire " + repertoire.getAbsolutePath());
            return false;
        }

        // Validation des extensions
        if (!verifieExtensionsFichiers(fichiers, nomFichiersAttendus)) {
            return false;
        }

        // Validation de la taille des fichiers
        for (String nomFichierAttendu : nomFichiersAttendus) {
            String emplacementFichier = emplacementRessourceRepertoireFichiers + nomFichierAttendu;
            if (!testeSiFichierContientDonneesUtiles(contexteExecutionJob, emplacementFichier, 0)) {
                LOGGER.error("Le fichier ne peut pas être vide : {}", emplacementFichier);
                String messageErreur = MessageFormat.format(RedacMessages.ERREUR_FICHIER_VIDE, emplacementFichier);
                traceErreurDansFichierLog(messageErreur);
                return false;
            }
        }

        return true;
    }

    @Override
    protected JobParameters initParametrageJob(GenericXmlApplicationContext contexteExecutionJob) {
        Map<String, JobParameter> parametresJob = new HashMap<String, JobParameter>();
        String emplacementRepertoire = emplacementRessourceRepertoireFichiers + File.separator;

        // Configuration lecteur du fichier contrats
        LOGGER.debug("Fichier entrée contrats pris en compte : {}", emplacementRepertoire + nomFichierContrats);
        parametresJob.put("fichierEntreeContrats", new JobParameter(emplacementRepertoire + nomFichierContrats));

        // Configuration lecteur du fichier tarifs
        LOGGER.debug("Fichier entrée tarifs pris en compte : {}", emplacementRepertoire + nomFichierTarifs);
        parametresJob.put("fichierEntreeTarifs", new JobParameter(emplacementRepertoire + nomFichierTarifs));

        // Configuration lecteur du fichier Clients
        LOGGER.debug("Fichier entrée Clients pris en compte : {}", emplacementRepertoire + nomFichierClients);
        parametresJob.put("fichierEntreeClients", new JobParameter(emplacementRepertoire + nomFichierClients));

        // Configuration lecteur du fichier intermédiaires
        LOGGER.debug("Fichier entrée intermédiaires pris en compte : {}", emplacementRepertoire + nomFichierIntermediaires);
        parametresJob.put("fichierEntreeIntermediaires", new JobParameter(emplacementRepertoire + nomFichierIntermediaires));

        // Configuration du mode de chargement
        LOGGER.debug("Mode de chargement pris en compte : {}", this.modeChargement);
        parametresJob.put("modeChargement", new JobParameter(this.modeChargement));

        // Configuration de l'emplacement du fichier de log
        LOGGER.debug("Fichier erreurs pris en compte : {}", getCheminAbsoluFichierLogErreurs());
        parametresJob.put("fichierLogErreurs", new JobParameter(getCheminAbsoluFichierLogErreurs()));

        return new JobParameters(parametresJob);
    }

    @Override
    protected void lanceTraitementFinJob(GenericXmlApplicationContext contexteExecutionJob, CodeRetour codeRetour, JobExecution executionJob) {
        if (codeRetour != CodeRetour.TERMINE && codeRetour != CodeRetour.ERREUR_FONCTIONNELLE_NON_BLOQUANTE) {
            videEspacesTemporaires(contexteExecutionJob,
                    Arrays.asList(TarifRepository.class, ContratRepository.class, IntermediaireRepository.class, ClientRepository.class));
        }

        // Logge le message de réussite du traitement batch dans le fichier de log
        if (codeRetour == CodeRetour.TERMINE) {
            traceTraitementOKDansFichierLog("Traitement réussi des fichiers");
        } else if (codeRetour == CodeRetour.ERREUR_FONCTIONNELLE_NON_BLOQUANTE) {
            traceTraitementOKDansFichierLog("Traitement réussi des fichiers, avec erreur(s) non bloquante(s)");
        }
    }

}
