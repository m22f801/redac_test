package fr.si2m.red.batch.flux4.execution;

import fr.si2m.red.EntiteImportableBatch;
import fr.si2m.red.batch.moteur.execution.FacadeExecuteurBatch;
import fr.si2m.red.core.repository.EntiteImportableRepository;
import fr.si2m.red.dsn.Remuneration;
import fr.si2m.red.dsn.RemunerationRepository;

/**
 * Exécuteur du batch du flux 4 pour la table Remuneration.
 * 
 * @author benitahy
 *
 */
@FacadeExecuteurBatch(nomBatch = "flux4_REMUNERATION")
public class Flux4RemunerationExecuteurBatch extends Flux4ChargementExecuteurBatch {

    /**
     * Exécuteur de batch du flux 4 pour l'import de {@link Remuneration}.
     * 
     * @param parametrageBatch
     *            les paramètres d'exécution du batch - deux paramètres sont attendus en entrée :
     *            <ol>
     *            <li>le chemin absolu vers le fichier à importer</li>
     *            <li>le chemin absolu vers le fichier de logs à éventuellement renseigner</li>
     *            </ol>
     */
    public Flux4RemunerationExecuteurBatch(String[] parametrageBatch) {
        super(parametrageBatch);
    }

    @Override
    protected String getNomEntitesImportees() {
        return "Remuneration";
    }

    @Override
    protected Class<? extends EntiteImportableRepository<? extends EntiteImportableBatch>> getReferentielEntitesImportees() {
        return RemunerationRepository.class;
    }

}
