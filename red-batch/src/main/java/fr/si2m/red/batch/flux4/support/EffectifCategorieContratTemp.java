package fr.si2m.red.batch.flux4.support;

import lombok.Data;

/**
 * Donnée temporaire support du flux 4, phase 5 (RR405).
 * 
 * @author nortaina
 *
 */
@Data
public class EffectifCategorieContratTemp {

    private String numCategorieQuittancement;
    private String libelleCategorieQuittancement;
    private Integer prioriteAffilies;
    private Integer prioriteAyantsDroitAdultes;
    private Integer seuilAyantsDroitAdultes;
    private Integer prioriteAyantsDroitAutres;
    private Integer seuilAyantsDroitAutres;
    private Integer prioriteAyantsDroitEnfants;
    private Integer seuilAyantsDroitEnfants;
    private Integer indicateurAffiliation;
    private Integer nombreAyantDroitAdultes;
    private Integer nombreAyantDroitAutres;
    private Integer nombreAyantDroitEnfants;

}
