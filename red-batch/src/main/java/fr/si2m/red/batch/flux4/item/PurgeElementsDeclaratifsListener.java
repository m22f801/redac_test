package fr.si2m.red.batch.flux4.item;

import org.springframework.batch.core.annotation.BeforeProcess;
import org.springframework.beans.factory.annotation.Autowired;

import fr.si2m.red.reconciliation.CategorieQuittancementIndividuRepository;
import fr.si2m.red.reconciliation.EffectifCategorieContratTravailRepository;
import fr.si2m.red.reconciliation.EffectifCategorieMouvementRepository;
import fr.si2m.red.reconciliation.PeriodeRecue;
import fr.si2m.red.reconciliation.TrancheCategorieBaseAssujettieRepository;
import fr.si2m.red.reconciliation.TrancheCategorieRepository;

/**
 * Purge des éléments déclaratifs avant un calcul d'éléments déclaratifs d'une période à reconsolider.
 * 
 * c.f. RDG F04_RG_P5_01
 * 
 * @author nortaina
 *
 */
public class PurgeElementsDeclaratifsListener {
    @Autowired
    private CategorieQuittancementIndividuRepository categorieQuittancementIndividuRepository;
    @Autowired
    private EffectifCategorieMouvementRepository effectifCategorieMouvementRepository;
    @Autowired
    private TrancheCategorieRepository trancheCategorieRepository;
    @Autowired
    private EffectifCategorieContratTravailRepository effectifCategorieContratTravailRepository;
    @Autowired
    private TrancheCategorieBaseAssujettieRepository trancheCategorieBaseAssujettieRepository;

    /**
     * Exécution des purges avant de reconsolider une période donnée.
     * 
     * @param periode
     *            la période dont les éléments déclaratifs sont à purger
     */
    @BeforeProcess
    public void beforeProcess(PeriodeRecue periode) {
        categorieQuittancementIndividuRepository.supprimePourPeriode(periode.getId());
        effectifCategorieMouvementRepository.supprimePourPeriode(periode.getId());
        trancheCategorieRepository.supprimePourPeriode(periode.getId());
        effectifCategorieContratTravailRepository.supprimePourPeriode(periode.getId());
        trancheCategorieBaseAssujettieRepository.supprimePourPeriode(periode.getId());
    }

}
