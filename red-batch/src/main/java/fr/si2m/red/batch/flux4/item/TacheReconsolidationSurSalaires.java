package fr.si2m.red.batch.flux4.item;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;

import fr.si2m.red.batch.moteur.EtapeCodeRetourModificateur;
import fr.si2m.red.batch.moteur.support.LogUtils;
import fr.si2m.red.dsn.Trace;
import fr.si2m.red.reconciliation.CompteRenduIntegrationRepository;
import fr.si2m.red.reconciliation.HistoriqueAssignationPeriodeRepository;
import fr.si2m.red.reconciliation.HistoriqueAttenteRetourEtpPeriodeRepository;
import fr.si2m.red.reconciliation.HistoriqueCommentairePeriodeRepository;
import fr.si2m.red.reconciliation.HistoriqueEtatPeriodeRepository;
import fr.si2m.red.reconciliation.MessageControleRepository;
import fr.si2m.red.reconciliation.PeriodeRecueRepository;
import fr.si2m.red.reconciliation.RattachementDeclarationsRecuesRepository;
import lombok.Setter;

/**
 * Tâche de reconsolidation des éléments déclaratifs sur salaires.
 * 
 * @author poidij
 *
 */
public class TacheReconsolidationSurSalaires extends EtapeCodeRetourModificateur implements Tasklet {
    private static final Logger LOGGER = LoggerFactory.getLogger(TacheReconsolidationSurSalaires.class);

    private static final String PARAM_LISTE_ID_PERIODES = "listeIdsPeriodes";
    private static final String PARAM_AUDIT_NOM_BATCH = "auditNomBatch";

    @Setter
    private RattachementDeclarationsRecuesRepository rattachementDeclarationsRecuesRepository;

    @Setter
    private PeriodeRecueRepository periodeRecueRepository;

    @Setter
    private String auditNomBatch;

    @Setter
    private String cheminAbsoluFichierLogErreurs;

    @Autowired
    private CompteRenduIntegrationRepository compteRenduIntegrationRepository;
    @Autowired
    private HistoriqueEtatPeriodeRepository historiqueEtatPeriodeRepository;
    @Autowired
    private MessageControleRepository messageControleRepository;
    @Autowired
    private HistoriqueAssignationPeriodeRepository historiqueAssignationPeriodeRepository;
    @Autowired
    private HistoriqueAttenteRetourEtpPeriodeRepository historiqueAttenteRetourEtpPeriodeRepository;
    @Autowired
    private HistoriqueCommentairePeriodeRepository historiqueCommentairePeriodeRepository;

    @Override
    public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) throws Exception {
        this.saveStepExecution(chunkContext.getStepContext().getStepExecution());

        // identifications des périodes à calculer
        Set<Long> idsPeriodes = periodeRecueRepository.identifiantsPeriodeContratSurSalaireACalculer();

        // F04_RG_P5_03
        List<Trace> listeTracesNocat = periodeRecueRepository.recupereTracesVerificationNocatSurSalaires();

        if (!listeTracesNocat.isEmpty()) {
            traiteErreurImprevue(listeTracesNocat, "sur salaires");

            // retrait de la période en erreurs des périodes à calculer
            for (Trace tracePeriode : listeTracesNocat) {
                idsPeriodes.remove(tracePeriode.getIdPeriode());
            }
        }

        // La F04_RG_P5_03 vérifiée, on initie les calculs pour les contrats sur salaires des periodes valides

        if (!idsPeriodes.isEmpty()) {

            calculElementsDeclaratifsPourListePeriodes(idsPeriodes);

            // Traitement des erreurs lors des calculs
            List<Trace> listeTraces = periodeRecueRepository.recupereTracesSurSalairesBatch405();

            if (!listeTraces.isEmpty()) {
                traiteErreurImprevue(listeTraces, "sur salaires");
            }
        }

        return RepeatStatus.FINISHED;
    }

    /**
     * Traite les erreurs rencontrées lors des calculs
     * 
     * @param listeTraces
     *            liste des erreurs
     * @param typeCalcul
     *            le type de calcul
     */
    private void traiteErreurImprevue(List<Trace> listeTraces, String typeCalcul) {
        try {
            // Récupérer la liste des traces et celle des identifiants de périodes
            List<String> listeErreurs = new ArrayList<String>();
            Set<Long> idsPeriodes = new HashSet<Long>();

            for (Trace trace : listeTraces) {
                listeErreurs.add(trace.getMessage());
                idsPeriodes.add(trace.getIdPeriode());
            }

            // Logger dans la log technique qu'il y a des erreurs détectées
            LOGGER.error("Erreur(s) détectée(s) lors du calcul d'éléments déclaratifs {}", typeCalcul);
            // Ecrire les traces dans le fichier de log
            for (String log : listeErreurs) {
                LogUtils.traceErreurDansFichierLog(log, cheminAbsoluFichierLogErreurs);
                valideNonBloquante(false, log);
            }

            // Détacher en masse les périodes en erreur
            int nbDetachements = rattachementDeclarationsRecuesRepository.detacheAdhesionsDernierLotRattachementEnMasse(idsPeriodes);

            // Logger le détachement
            String message = StringUtils.EMPTY;
            if (listeTraces.size() == 1) {
                message = MessageFormat.format("Détachement de {0} adhésions pour la période {1}", nbDetachements, idsPeriodes.iterator().next());
            } else {
                message = MessageFormat.format("Détachement de {0} adhésions pour les périodes {1}", nbDetachements,
                        StringUtils.join(idsPeriodes, ","));
            }

            LOGGER.info(message);
            LogUtils.traceErreurDansFichierLog(message, cheminAbsoluFichierLogErreurs);

            // Purger en masse les éléments déclaratifs calculés pour ces périodes
            purgeElementsDeclaratifs(idsPeriodes);

            // On recherche les périodes orphelines après détachement
            List<Long> idsPeriodesOrphelines = rattachementDeclarationsRecuesRepository.getPeriodesOrphelines(idsPeriodes);

            if (idsPeriodesOrphelines != null && !idsPeriodesOrphelines.isEmpty()) {
                for (Long idPeriode : idsPeriodesOrphelines) {
                    compteRenduIntegrationRepository.supprimeCompteRenduIntegrationPourPeriode(idPeriode);
                    historiqueEtatPeriodeRepository.supprimeHistoriqueEtatPeriodePourPeriode(idPeriode);
                    messageControleRepository.supprimeMessageControlePourPeriode(idPeriode);
                    historiqueAssignationPeriodeRepository.supprimeHistoriqueAssignationPourPeriode(idPeriode);
                    historiqueAttenteRetourEtpPeriodeRepository.supprimeHistoriqueAttenteRetourEtpPourPeriode(idPeriode);
                    historiqueCommentairePeriodeRepository.supprimeHistoriqueCommentairePourPeriode(idPeriode);
                    periodeRecueRepository.supprimePeriodeRecue(idPeriode);
                }

                // On ne recalcule pas pour les périodes orphelines supprimés, les id sont exclus du recalcul

                Iterator<Long> itr = idsPeriodes.iterator();
                while (itr.hasNext()) {
                    Long id = itr.next();
                    if (idsPeriodesOrphelines.contains(id)) {
                        itr.remove();
                    }
                }
            }

            // potentielement vide, après détachement + suppression des périodes orphelines
            if (!idsPeriodes.isEmpty()) {

                // Recalculer en masse les éléments déclaratifs calculés pour ces périodes ( cf SFD 5.6.3 F04_RG_P5_01 )
                calculElementsDeclaratifsPourListePeriodes(idsPeriodes);

                // Traitement des erreurs éventuel lors du recalcul
                List<Trace> listeTracesSurSalaires = periodeRecueRepository.recupereTracesSurSalairesBatch405();
                if (!listeTracesSurSalaires.isEmpty()) {
                    traiteErreurSurRecalcul(listeTracesSurSalaires, typeCalcul);
                }

            }

        } catch (Exception e) {
            LOGGER.info("Erreur lors du traitement des erreurs", e);
        }

    }

    /**
     * Traite les erreurs survenu lors du recalcul dans la foulée, sur première erreur lors du 405.
     * 
     * @param listeTraces
     *            la trace contenant les périodes en erreurs
     */
    private void traiteErreurSurRecalcul(List<Trace> listeTraces, String typeCalcul) {

        // Logger dans la log technique qu'il y a des erreurs détectées
        LOGGER.error("Erreur(s) détectée(s) lors du recalcul d'éléments déclaratifs {}", typeCalcul);

        // Récupérer la liste des traces et celle des identifiants de périodes
        List<String> listeErreurs = new ArrayList<String>();
        Set<Long> idsPeriodes = new HashSet<Long>();

        for (Trace trace : listeTraces) {
            listeErreurs.add(trace.getMessage());
            idsPeriodes.add(trace.getIdPeriode());
        }

        // Ecrire les traces dans le fichier de log
        for (String log : listeErreurs) {
            LogUtils.traceErreurDansFichierLog(log, cheminAbsoluFichierLogErreurs);
            valideNonBloquante(false, log);
        }

        // Purger en masse les éléments déclaratifs recalculés pour ces périodes
        purgeElementsDeclaratifs(idsPeriodes);

        // On passe les périodes en erreur à reconsolider='N'
        // pour le que 406 ne tienne pas compte de la période en erreur
        String reconsolider = "N";
        for (Long idPeriode : idsPeriodes) {
            periodeRecueRepository.modifieReconsoliderPeriodeRecue(idPeriode, reconsolider);
        }

    }

    /**
     * Calcul des éléments déclaratifs pour les contrats sur salaires pour toutes les périodes Inutilisé
     */
    private void calculElementsDeclaratifs() {

        Map<String, Object> params = new HashMap<>();
        params.put(PARAM_AUDIT_NOM_BATCH, auditNomBatch);

        // Calcul des TrancheCategorieBaseAssujettie
        String sql = "call Inserer_TRANCHE_CATEGORIE_BASE_ASSUJETTIE(:auditNomBatch)";

        LOGGER.info("Appel de la procédure stockée Inserer_TRANCHE_CATEGORIE_BASE_ASSUJETTIE avec les paramètres {}", params);
        periodeRecueRepository.callMysqlProcedure(sql, params);
        LOGGER.info("Fin de la procédure stockée Inserer_TRANCHE_CATEGORIE_BASE_ASSUJETTIE");

        // Calcul des CategorieQuittancementIndividu
        sql = "call Inserer_CATEGORIE_QUITTANCEMENT_INDIVIDU_SALAIRE(:auditNomBatch)";
        LOGGER.info("Appel de la procédure stockée Inserer_CATEGORIE_QUITTANCEMENT_INDIVIDU_SALAIRE avec les paramètres {}", params);
        periodeRecueRepository.callMysqlProcedure(sql, params);
        LOGGER.info("Fin de la procédure stockée Inserer_CATEGORIE_QUITTANCEMENT_INDIVIDU_SALAIRE");

        // Calcul des effectifs des début et de fin
        sql = "call Calcul_effectif_indiv_salaire(:auditNomBatch)";
        LOGGER.info("Appel de la procédure stockée Calcul_effectif_indiv_salaire avec les paramètres {}", params);
        periodeRecueRepository.callMysqlProcedure(sql, params);
        LOGGER.info("Fin de la procédure stockée Calcul_effectif_indiv_salaire");

        // Calcul des TrancheCategorie
        sql = "call Inserer_TRANCHE_CATEGORIE(:auditNomBatch)";
        LOGGER.info("Appel de la procédure stockée Inserer_TRANCHE_CATEGORIE avec les paramètres {}", params);
        periodeRecueRepository.callMysqlProcedure(sql, params);
        LOGGER.info("Fin de la procédure stockée Inserer_TRANCHE_CATEGORIE");
    }

    /**
     * Calcul des éléments déclaratifs pour les contrats sur salaires pour un périmètre définis de périodes renseignées dans les params
     * 
     * @param params
     *            map de paramètres
     * @param idsPeriodes
     *            Set d'idPeriodes à recalculer
     */
    private void calculElementsDeclaratifsPourListePeriodes(Set<Long> idsPeriodes) {

        // Recalculer en masse les éléments déclaratifs calculés pour ces périodes ( cf SFD 5.6.3 F04_RG_P5_01 )
        Map<String, Object> params = new HashMap<>();
        params.put(PARAM_AUDIT_NOM_BATCH, auditNomBatch);

        // on ajoute la nouvelle liste d'id à recalculer
        params.put(PARAM_LISTE_ID_PERIODES, StringUtils.join(idsPeriodes, ","));

        String sql = "call Inserer_TRANCHE_CATEGORIE_BASE_ASSUJETTIE_pour_periodes(:auditNomBatch, :listeIdsPeriodes)";
        LOGGER.info("Appel de la procédure stockée Inserer_TRANCHE_CATEGORIE_BASE_ASSUJETTIE_pour_periodes avec les paramètres {}", params);
        periodeRecueRepository.callMysqlProcedure(sql, params);
        LOGGER.info("Fin de la procédure stockée Inserer_TRANCHE_CATEGORIE_BASE_ASSUJETTIE_pour_periodes");

        sql = "call Inserer_CATEGORIE_QUITTANCEMENT_INDIVIDU_SALAIRE_pour_periodes(:auditNomBatch, :listeIdsPeriodes)";
        LOGGER.info("Appel de la procédure stockée Inserer_CATEGORIE_QUITTANCEMENT_INDIVIDU_SALAIRE_pour_periodes avec les paramètres {}", params);
        periodeRecueRepository.callMysqlProcedure(sql, params);
        LOGGER.info("Fin de la procédure stockée Inserer_CATEGORIE_QUITTANCEMENT_INDIVIDU_SALAIRE_pour_periodes");

        // Calcul des effectifs des début et de fin
        sql = "call Calcul_effectif_indiv_salaire_pour_periodes(:auditNomBatch, :listeIdsPeriodes)";
        LOGGER.info("Appel de la procédure stockée Calcul_effectif_indiv_salaire_pour_periodes avec les paramètres {}", params);
        periodeRecueRepository.callMysqlProcedure(sql, params);
        LOGGER.info("Fin de la procédure stockée Calcul_effectif_indiv_salaire_pour_periodes");

        sql = "call Inserer_TRANCHE_CATEGORIE_pour_periodes(:auditNomBatch, :listeIdsPeriodes)";
        LOGGER.info("Appel de la procédure stockée Inserer_TRANCHE_CATEGORIE_pour_periodes avec les paramètres {}", params);
        periodeRecueRepository.callMysqlProcedure(sql, params);
        LOGGER.info("Fin de la procédure stockée Inserer_TRANCHE_CATEGORIE_pour_periodes");

    }

    /**
     * Purge des éléments déclaratifs pour les périodes données
     * 
     * @param idsPeriodes
     *            Set d'IdPeriodes
     */
    private void purgeElementsDeclaratifs(Set<Long> idsPeriodes) {
        String sql = "call Purger_elements_declaratifs_en_masse(:listeIdsPeriodes)";

        Map<String, Object> params = new HashMap<>();
        params.put(PARAM_AUDIT_NOM_BATCH, auditNomBatch);
        params.put(PARAM_LISTE_ID_PERIODES, StringUtils.join(idsPeriodes, ","));

        LOGGER.info("Appel de la procédure stockée Purger_elements_declaratifs_en_masse avec les paramètres {}", params);
        periodeRecueRepository.callMysqlProcedure(sql, params);
        LOGGER.info("Fin de la procédure stockée Purger_elements_declaratifs_en_masse");
    }

}
