package fr.si2m.red.batch.moteur.item;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.annotation.Autowired;

import fr.si2m.red.EntiteImportableBatch;
import fr.si2m.red.batch.moteur.support.LecteurLigne;

/**
 * Lecteur de fichiers plats pour les batchs d'import REDAC permettant un mapping direct des champs vers les propriétés d'une entité "prototype".
 * 
 * @author nortaina
 *
 * @param <T>
 *            le type des lignes lues
 */
public class LecteurFichierPlatSansConversion<T extends EntiteImportableBatch> extends LecteurFichierPlat<T> {

    /**
     * Constructeur pour le mapping direct (par noms) de propriétés dans une entité ligne.
     * 
     * @param nomsChamps
     *            les noms ordonnés des champs d'une ligne
     * @param nomPrototypeLigne
     *            le nom du prototype à utiliser pour une entité ligne
     * @param beanFactory
     *            l'usine de bean pour la récupération du prototype de ligne
     */
    @Autowired
    public LecteurFichierPlatSansConversion(String[] nomsChamps, String nomPrototypeLigne, BeanFactory beanFactory) {
        super(nomsChamps, nomPrototypeLigne, beanFactory);
        getMapperLigne().setFieldSetMapper(new LecteurLigne<T>(beanFactory, nomPrototypeLigne));
    }

}
