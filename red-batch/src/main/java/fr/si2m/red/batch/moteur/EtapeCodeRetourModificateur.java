package fr.si2m.red.batch.moteur;

import lombok.AccessLevel;
import lombok.Getter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.annotation.BeforeStep;
import org.springframework.batch.core.listener.ExecutionContextPromotionListener;

/**
 * Composant pouvant modifier le code retour de l'étape d'un traitement batch REDAC dans le contexte d'exécution du job.<br/>
 * <br/>
 * Ce composant doit IMPERATIVEMENT être placé en tant que listener d'étape batch.
 * 
 * @author nortaina
 * 
 * @see CodeRetour
 *
 */
public class EtapeCodeRetourModificateur extends ExecutionContextPromotionListener {
    /**
     * La clé technique pour forcer le statut de retour dans le contexte d'exécution d'un batch.
     */
    public static final String CLE_FORCE_STATUT_EXECUTION = "CLE_FORCE_STATUT_EXECUTION";

    private static final Logger LOGGER = LoggerFactory.getLogger(EtapeCodeRetourModificateur.class);

    /**
     * L'exécution de l'étape.
     *
     * @return l'exécution de l'étape
     */
    @Getter(AccessLevel.PROTECTED)
    private StepExecution stepExecution;

    /**
     * Constructeur.
     * 
     */
    public EtapeCodeRetourModificateur() {
        super();
        // Permet le passage du statut d'exécution forcé entre contextes d'exécution
        setKeys(new String[] { CLE_FORCE_STATUT_EXECUTION });
    }

    /**
     * Sauvegarde de l'exécution de l'étape pour le rendre disponible lors des traitements.
     * 
     * @param stepExecution
     *            l'exécution de l'étape
     */
    @BeforeStep
    public void saveStepExecution(StepExecution stepExecution) {
        this.stepExecution = stepExecution;
    }

    /**
     * Lance une nouvelle erreur fonctionnelle bloquante si une affirmation donnée se révèle fausse.
     * 
     * @param affirmation
     *            l'affirmation à tester
     * @param message
     *            le message d'erreur
     */
    protected void valideBloquante(boolean affirmation, String message) {
        if (affirmation) {
            return;
        }
        LOGGER.error(message);
        throw new ErreurFonctionnelleBloquanteException(message);
    }

    /**
     * Trace une erreur fonctionnelle bloquante à partir d'une erreur quelconque.
     * 
     * @param erreur
     *            l'erreur entraînant un blocage fonctionnel
     * @param message
     *            le message d'erreur
     */
    protected void traceBloquante(Throwable erreur, String message) {
        LOGGER.error(message);
        throw new ErreurFonctionnelleBloquanteException(message, erreur);
    }

    /**
     * Trace une erreur fonctionnelle non bloquante si une affirmation donnée se révèle fausse.
     * 
     * @param affirmation
     *            l'affirmation à tester
     * @param templateMessage
     *            le template de message d'erreur
     * @param messageArgs
     *            les arguments pour le template
     */
    protected void valideNonBloquante(boolean affirmation, String templateMessage, Object... messageArgs) {
        if (affirmation) {
            return;
        }
        LOGGER.warn(templateMessage, messageArgs);
        stepExecution.getExecutionContext().put(CLE_FORCE_STATUT_EXECUTION, CodeRetour.ERREUR_FONCTIONNELLE_NON_BLOQUANTE);
    }

}
