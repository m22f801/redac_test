package fr.si2m.red.batch.flux4.execution;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParameter;
import org.springframework.batch.core.JobParameters;
import org.springframework.context.support.GenericXmlApplicationContext;

import fr.si2m.red.batch.moteur.CodeRetour;
import fr.si2m.red.batch.moteur.execution.FacadeExecuteurBatch;

/**
 * Exécuteur de la phase 5 - calcul et stockage des éléments déclaratifs - du flux 4.
 * 
 * @author nortaina
 *
 */
@FacadeExecuteurBatch(nomBatch = "flux4_CalculElementsDeclaratifs")
public class Flux4CalculElementsDeclaratifsExecuteurBatch extends Flux4ExecuteurBatch {

    private static final Logger LOGGER = LoggerFactory.getLogger(Flux4CalculElementsDeclaratifsExecuteurBatch.class);

    /**
     * Ordre du paramètre de lancement "emplacement du fichier de log d'erreurs".
     */
    public static final int ORDRE_PARAM_EMPLACEMENT_FICHIER_ERREURS = 0;

    private String idJob = "CalculElementsDeclaratifs";

    /**
     * Exécuteur de batch du flux 4 pour le calcul et stockage des éléments déclaratifs.
     * 
     * @param parametrageBatch
     *            les paramètres d'exécution du batch - un paramètre est attendu en entrée :
     *            <ol>
     *            <li>le chemin absolu vers le fichier de logs à éventuellement renseigner</li>
     *            </ol>
     */
    public Flux4CalculElementsDeclaratifsExecuteurBatch(String[] parametrageBatch) {
        super(parametrageBatch);
        String cheminAbsoluFichierLogErreurs = parametrageBatch[ORDRE_PARAM_EMPLACEMENT_FICHIER_ERREURS];
        setCheminAbsoluFichierLogErreurs(cheminAbsoluFichierLogErreurs);
    }

    /**
     * Exécuteur de batch du flux 4 pour le calcul et stockage des éléments déclaratifs.
     * 
     * @param parametrageBatch
     *            les paramètres d'exécution du batch - un paramètre est attendu en entrée :
     *            <ol>
     *            <li>le chemin absolu vers le fichier de logs à éventuellement renseigner</li>
     *            </ol>
     * @param idJob
     *            l'id Job
     */
    public Flux4CalculElementsDeclaratifsExecuteurBatch(String[] parametrageBatch, String idJob) {
        super(parametrageBatch);
        String cheminAbsoluFichierLogErreurs = parametrageBatch[ORDRE_PARAM_EMPLACEMENT_FICHIER_ERREURS];
        setCheminAbsoluFichierLogErreurs(cheminAbsoluFichierLogErreurs);
        this.idJob = idJob;
    }

    @Override
    protected String getIdJobFlux4() {
        return idJob;
    }

    @Override
    protected JobParameters initParametrageJob(GenericXmlApplicationContext contexteExecutionJob) {
        Map<String, JobParameter> parametresJob = new HashMap<String, JobParameter>();

        // Configuration de l'emplacement du fichier de log
        LOGGER.debug("Fichier erreurs pris en compte : {}", getCheminAbsoluFichierLogErreurs());
        parametresJob.put("fichierLogErreurs", new JobParameter(getCheminAbsoluFichierLogErreurs()));

        return new JobParameters(parametresJob);
    }

    @Override
    protected void lanceTraitementFinJob(GenericXmlApplicationContext contexteExecutionJob, CodeRetour codeRetour, JobExecution executionJob) {
        // Logge le message de réussite du traitement batch dans le fichier de log
        if (codeRetour == CodeRetour.TERMINE) {
            traceTraitementOKDansFichierLog("Traitement du flux 4 - Calcul et stockage des éléments déclaratifs - réussi");
        } else if (codeRetour == CodeRetour.ERREUR_FONCTIONNELLE_NON_BLOQUANTE) {
            traceTraitementOKDansFichierLog(
                    "Traitement du flux 4 - Calcul et stockage des éléments déclaratifs - réussi, avec erreur(s) non bloquante(s)");
        }

        LOGGER.info("Fin d'exécution du batch flux 4 - Calcul et stockage des éléments déclaratifs");
    }

}
