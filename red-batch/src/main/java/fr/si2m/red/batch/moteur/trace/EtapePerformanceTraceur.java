package fr.si2m.red.batch.moteur.trace;

import java.util.concurrent.TimeUnit;

import lombok.Setter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.StepExecutionListener;
import org.springframework.batch.core.annotation.AfterStep;
import org.springframework.batch.core.annotation.BeforeStep;

/**
 * Traceur des performances des étapes de jobs batchs.
 * 
 * @author nortaina
 *
 */
public class EtapePerformanceTraceur implements StepExecutionListener {
    private static final Logger LOGGER = LoggerFactory.getLogger(EtapePerformanceTraceur.class);

    /**
     * Activation / désactivation des traces de performance pour le traitement d'une étape.
     * 
     * @param traceTraitementEtape
     *            true pour activer les traces de performance pour les traitements d'étapes
     */
    @Setter
    private boolean traceTraitementEtape;

    private String nomEtape;
    private String nomJob;

    private long debutEtape;

    /**
     * Gestion des traces de performance en début d'étape.
     * 
     * @param stepExecution
     *            le contexte de l'étape
     */
    @BeforeStep
    @Override
    public void beforeStep(StepExecution stepExecution) {
        if (!traceTraitementEtape) {
            return;
        }
        this.nomEtape = stepExecution.getStepName();
        this.nomJob = stepExecution.getJobExecution().getJobInstance().getJobName();
        this.debutEtape = System.nanoTime();
        LOGGER.info("Début de l'étape {} pour le job {}", nomEtape, nomJob);
    }

    /**
     * Gestion des traces de performance en fin d'étape.
     * 
     * @param stepExecution
     *            le contexte de l'étape
     * @return le statut de sortie d'étape
     */
    @AfterStep
    @Override
    public ExitStatus afterStep(StepExecution stepExecution) {
        if (traceTraitementEtape) {
            LOGGER.info("Etape {} réalisée en {} ms pour le job {}", nomEtape, TimeUnit.NANOSECONDS.toMillis(System.nanoTime() - debutEtape), nomJob);
            LOGGER.info("Etape {} a réalisé {} lectures et {} écritures", nomEtape, stepExecution.getReadCount(), stepExecution.getWriteCount());
        }
        return stepExecution.getExitStatus();
    }

}
