package fr.si2m.red.batch.flux78.item;

import java.text.MessageFormat;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.annotation.AfterStep;

import fr.si2m.red.EntiteImportableBatch;
import fr.si2m.red.RedacUnexpectedException;
import fr.si2m.red.batch.RedacMessages;
import fr.si2m.red.batch.moteur.erreur.ValidateurLigneAvecCollecte;
import fr.si2m.red.dsn.EtatPeriode;
import fr.si2m.red.reconciliation.CompteRenduIntegration;
import fr.si2m.red.reconciliation.CompteRenduIntegrationRepository;
import fr.si2m.red.reconciliation.HistoriqueEtatPeriode;
import fr.si2m.red.reconciliation.HistoriqueEtatPeriodeRepository;
import fr.si2m.red.reconciliation.MessageControle;
import fr.si2m.red.reconciliation.MessageControleRepository;
import fr.si2m.red.reconciliation.PeriodeRecue;
import fr.si2m.red.reconciliation.PeriodeRecueRepository;
import fr.si2m.red.reconciliation.PeriodeRecueTraitee;
import fr.si2m.red.reconciliation.PeriodeRecueTraiteeRepository;
import lombok.Setter;

/**
 * Traitement du fichier compte-rendu. WARNING : ce processeur doit être impérativement step-scoped !
 * 
 * @author benitahy
 *
 */
public class TraitementCRProcesseur extends ValidateurLigneAvecCollecte<EntiteImportableBatch> {
    private static final Logger LOGGER = LoggerFactory.getLogger(ControleLigneSyntheseProcesseur.class);

    private static final String APPLICATION_WQUI = "WQUI";
    private static final String APPLICATION_GERD = "GERD";
    private static final String APPLICATION_EGER = "EGER";

    private static final String ORIGINE_MESSAGE = "SIAVAL";

    private static final String NIVEAU_ALERTE = "REJET";

    private static final String LIBELLE_IDENTIFIANT_BATCH_702 = "Réception CR du Back-Office";

    @Setter
    private PeriodeRecueRepository periodeRecueRepository;
    @Setter
    private CompteRenduIntegrationRepository compteRenduIntegrationRepository;
    @Setter
    private MessageControleRepository messageControleRepository;
    @Setter
    private HistoriqueEtatPeriodeRepository historiqueEtatPeriodeRepository;
    @Setter
    private PeriodeRecueTraiteeRepository periodeRecueTraiteeRepository;

    @Setter
    private String fichierEntree;
    @Setter
    private String nomBatch;
    @Setter
    private Long timeStampExecution;

    private Synthese synthese;
    private int numeroLigneCourant;
    private boolean arretTraitement = false;

    @Override
    public void valide(EntiteImportableBatch item) {

        // Si le step a été arrêté (CompteRenduGlobal = 1), on ne traite plus d'item
        if (arretTraitement) {
            return;
        }

        // On comptabilise TOUTES les lignes, même celles à ignorer
        numeroLigneCourant++;

        if (item instanceof ContratPeriode) {
            traiteLigneContratPeriode((ContratPeriode) item);
        } else if (item instanceof Message) {
            traiteLigneMessage((Message) item);
        } else if (item instanceof MessageAgrandies) {
            traiteLigneMessageAgrandies((MessageAgrandies) item);
        } else if (item instanceof Synthese) {
            // On fait cette vérification en dernier car il n'y a qu'une seule ligne de synthèse !
            if (synthese != null) {
                throw new RedacUnexpectedException("Il ne devrait exister qu'une ligne de synthèse");
            }
            // Affectation de la ligne synthèse unique à ce processeur
            synthese = (Synthese) item;
            traiteLigneSynthese();
        }

    }

    /**
     * Traitement de l'unique ligne de synthèse issue du fichier en entrée.
     * 
     */
    private void traiteLigneSynthese() {
        synthese.setNomFichierImport(fichierEntree);
        synthese.setLigneImport(numeroLigneCourant);

        // F78_RG_P4_05 Récupération de l'application concernée
        // F78_RG_P4_22 Identifiaction de l'application concernée
        if (synthese.getEmetteurCRAsInteger() == 0) {
            synthese.setApplicationConcernee(APPLICATION_WQUI);
        } else if (synthese.getEmetteurCRAsInteger() == 1) {
            synthese.setApplicationConcernee(APPLICATION_GERD);
        } else if (synthese.getEmetteurCRAsInteger() == 2) {
            synthese.setApplicationConcernee(APPLICATION_EGER);
        }

        // Ne pas tester la nullité de compteRenduGlobalAsInteger car pour ce cas, le programme se termine en exception (code 80)
        // En effet, pas de RDG spécifiée pour ce cas
        if (synthese.getCompteRenduGlobalAsInteger() == 1) {

            List<PeriodeRecue> periodesEncours = periodeRecueRepository.getPeriodesRecuesEncoursPourApplication(synthese.getApplicationConcernee());

            for (PeriodeRecue periodeRecue : periodesEncours) {
                // F78_RG_P4_07
                creeCompteRenduIntegration(periodeRecue, synthese);

                // F78_RG_P4_08
                creeMessageControle(periodeRecue, synthese.getApplicationConcernee() + "_" + NIVEAU_ALERTE, synthese.getDateTraitementAsInteger(),
                        NIVEAU_ALERTE, null, synthese.getMessageSynthese());
                // F78_RG_P4_09
                modifieEtatPeriode(periodeRecue, EtatPeriode.NIN);
                creeHistoriqueEtatPeriode(periodeRecue, EtatPeriode.NIN, synthese.getApplicationConcernee(), LIBELLE_IDENTIFIANT_BATCH_702,
                        "Fichier rejeté intégralement (dateTraitement : " + synthese.getDateTraitement() + ")");
            }

            LOGGER.info("Le compte rendu global est égal à 1, arrêt du traitement.");
            // Ceci fonctionne car le processeur est également un listener sur l'étape. Après ce Step, le Job s'arrêtera.
            getStepExecution().setExitStatus(ExitStatus.STOPPED);
            // Nécessaire pour ne plus traitement les autres item du Chunk.
            arretTraitement = true;
        }
    }

    /**
     * Traitement d'une ligne contrat/période issue du fichier en entrée.
     * 
     * @param contratPeriode
     *            la ligne contrat/période à traiter
     */
    private void traiteLigneContratPeriode(ContratPeriode contratPeriode) {

        contratPeriode.setNomFichierImport(fichierEntree);
        contratPeriode.setLigneImport(numeroLigneCourant);
        contratPeriode.setApplication(synthese.getApplicationConcernee());
        contratPeriode.setDateTraitementSynthese(synthese.getDateTraitementAsInteger());
        contratPeriode.setEmetteurCRSynthese(synthese.getEmetteurCRAsInteger());
        contratPeriode.setTypeFluxSynthese(synthese.getTypeFluxAsInteger());

        // F78_RG_P4_20
        Integer dateDebutPeriode = contratPeriode.getDateDebutPeriodeAsInteger();
        valideChampAvecCollecteSansBlocage(dateDebutPeriode != null, contratPeriode, RedacMessages.CHAMP_DATE_DEBUT_PERIODE,
                MessageFormat.format(RedacMessages.ERREUR_CHAMP_NON_NUMERIQUE, RedacMessages.CHAMP_DATE_DEBUT_PERIODE));

        Integer dateFinPeriode = contratPeriode.getDateFinPeriodeAsInteger();
        valideChampAvecCollecteSansBlocage(dateFinPeriode != null, contratPeriode, RedacMessages.CHAMP_DATE_FIN_PERIODE,
                MessageFormat.format(RedacMessages.ERREUR_CHAMP_NON_NUMERIQUE, RedacMessages.CHAMP_DATE_FIN_PERIODE));

        Integer codeNature = controleChampCodeNature(contratPeriode);

        if (dateDebutPeriode == null || codeNature == null || dateFinPeriode == null) {
            // On ne peut pas continuer le traitement
            return;
        }

        String numeroContrat = contratPeriode.getContratZoneA() + contratPeriode.getContratZoneB() + contratPeriode.getContratZoneC();
        // F78_RG_P4_10 Récupération de la période reçue en cours
        PeriodeRecue periodeRecue = periodeRecueRepository.getPeriodeRecueEncours(numeroContrat, dateDebutPeriode);

        if (periodeRecue == null) {
            valideChampAvecCollecteSansBlocage(false, contratPeriode, "periodeRecue", RedacMessages.ERREUR_AUCUNE_PERIODE);
            // Et on ne peut pas continuer le traitement
            return;
        }

        creePeriodeRecueTraitee(periodeRecue);
        creeCompteRenduIntegration(periodeRecue, contratPeriode);

        if (codeNature == 1) {

            if (!controleChampCodeRejet(contratPeriode) || !controleChampLibelleRejet(contratPeriode)) {
                // Si les controles échouent, on ne continue pas le traitement
                return;
            }

            // F78_RG_P4_12
            creeMessageControle(periodeRecue, contratPeriode.getCodeRejet(), contratPeriode.getDateTraitementSynthese(), NIVEAU_ALERTE, null,
                    contratPeriode.getLibelleRejet());

            modifieEtatPeriode(periodeRecue, EtatPeriode.NIN);
            creeHistoriqueEtatPeriode(periodeRecue, EtatPeriode.NIN, contratPeriode.getApplication(), LIBELLE_IDENTIFIANT_BATCH_702,
                    "Contrat/période rejeté (dateTraitement : " + synthese.getDateTraitement() + ")");

        } else if (codeNature == 0) {
            // F78_RG_P4_13
            modifieEtatPeriode(periodeRecue, EtatPeriode.INT);
            creeHistoriqueEtatPeriode(periodeRecue, EtatPeriode.INT, contratPeriode.getApplication(), LIBELLE_IDENTIFIANT_BATCH_702,
                    "Contrat/période intégré (dateTraitement : " + synthese.getDateTraitement() + ")");

        }

    }

    /**
     * Traitement d'une ligne message issue du fichier en entrée.
     * 
     * @param message
     *            la ligne message à traiter
     */
    private void traiteLigneMessage(Message message) {
        // MAJ des champs pour validation
        message.setNomFichierImport(fichierEntree);
        message.setLigneImport(numeroLigneCourant);

        message.setApplication(synthese.getApplicationConcernee());
        message.setDateTraitementSynthese(synthese.getDateTraitementAsInteger());

        // F78_RG_P4_21

        Integer dateDebutPeriode = message.getDateDebutPeriodeAsInteger();
        valideChampAvecCollecteSansBlocage(dateDebutPeriode != null, message, RedacMessages.CHAMP_DATE_DEBUT_PERIODE,
                MessageFormat.format(RedacMessages.ERREUR_CHAMP_NON_NUMERIQUE, RedacMessages.CHAMP_DATE_DEBUT_PERIODE));

        Integer dateFinPeriode = message.getDateFinPeriodeAsInteger();
        valideChampAvecCollecteSansBlocage(dateFinPeriode != null, message, RedacMessages.CHAMP_DATE_FIN_PERIODE,
                MessageFormat.format(RedacMessages.ERREUR_CHAMP_NON_NUMERIQUE, RedacMessages.CHAMP_DATE_FIN_PERIODE));

        Integer codeNature = controleChampCodeNatureMessage(message);

        if (dateDebutPeriode == null || codeNature == null || dateFinPeriode == null) {
            // On ne peut pas continuer le traitement
            return;
        }

        String numeroContrat = message.getContratZoneA() + message.getContratZoneB() + message.getContratZoneC();
        // F78_RG_P4_14 Récupération de la période reçue en cours
        if (!periodeRecueTraiteeRepository.existe(numeroContrat, dateDebutPeriode)) {
            valideChampAvecCollecteSansBlocage(false, message, "periodeRecue", RedacMessages.ERREUR_AUCUNE_PERIODE);
            // On ne peut pas continuer le traitement
            return;
        }

        PeriodeRecue periodeRecue = periodeRecueRepository.getPeriodeRecueTraitee(numeroContrat, dateDebutPeriode);
        // F78_RG_P4_15
        String niveauAlerte = codeNature == 0 ? "SIGNAL" : codeNature == 1 ? "REJET" : null;
        creeMessageControle(periodeRecue, message.getCodeMessage(), message.getDateTraitementSynthese(), niveauAlerte, message.getIndividu(),
                message.getLibelleMessage());

        // F78_RG_P4_16
        if (periodeRecue.getEtatPeriode() == EtatPeriode.INT) {
            modifieEtatPeriode(periodeRecue, EtatPeriode.INS);
            modifieHistoriqueEtatPeriodePourMessage(periodeRecue.getIdPeriode());
        }
    }

    /**
     * Traitement d'une ligne message issue du fichier en entrée.
     * 
     * @param message
     *            la ligne message à traiter
     */
    private void traiteLigneMessageAgrandies(MessageAgrandies message) {
        // MAJ des champs pour validation
        message.setNomFichierImport(fichierEntree);
        message.setLigneImport(numeroLigneCourant);

        message.setApplication(synthese.getApplicationConcernee());
        message.setDateTraitementSynthese(synthese.getDateTraitementAsInteger());

        // F78_RG_P4_21

        Integer dateDebutPeriode = message.getDateDebutPeriodeAsInteger();
        valideChampAvecCollecteSansBlocage(dateDebutPeriode != null, message, RedacMessages.CHAMP_DATE_DEBUT_PERIODE,
                MessageFormat.format(RedacMessages.ERREUR_CHAMP_NON_NUMERIQUE, RedacMessages.CHAMP_DATE_DEBUT_PERIODE));

        Integer dateFinPeriode = message.getDateFinPeriodeAsInteger();
        valideChampAvecCollecteSansBlocage(dateFinPeriode != null, message, RedacMessages.CHAMP_DATE_FIN_PERIODE,
                MessageFormat.format(RedacMessages.ERREUR_CHAMP_NON_NUMERIQUE, RedacMessages.CHAMP_DATE_FIN_PERIODE));

        Integer codeNature = controleChampCodeNatureMessageAgrandies(message);

        if (dateDebutPeriode == null || codeNature == null || dateFinPeriode == null) {
            // On ne peut pas continuer le traitement
            return;
        }

        String numeroContrat = message.getContratZoneA() + message.getContratZoneB() + message.getContratZoneC();
        // F78_RG_P4_14 Récupération de la période reçue en cours
        if (!periodeRecueTraiteeRepository.existe(numeroContrat, dateDebutPeriode)) {
            valideChampAvecCollecteSansBlocage(false, message, "periodeRecue", RedacMessages.ERREUR_AUCUNE_PERIODE);
            // On ne peut pas continuer le traitement
            return;
        }

        PeriodeRecue periodeRecue = periodeRecueRepository.getPeriodeRecueTraitee(numeroContrat, dateDebutPeriode);
        // F78_RG_P4_15
        String niveauAlerte = codeNature == 0 ? "SIGNAL" : codeNature == 1 ? "REJET" : null;
        creeMessageControle(periodeRecue, message.getCodeMessage(), message.getDateTraitementSynthese(), niveauAlerte, message.getIndividu(),
                message.getLibelleMessage());

        // F78_RG_P4_16
        if (periodeRecue.getEtatPeriode() == EtatPeriode.INT) {
            modifieEtatPeriode(periodeRecue, EtatPeriode.INS);
            modifieHistoriqueEtatPeriodePourMessage(periodeRecue.getIdPeriode());
        }
    }

    /**
     * Mise à jour de l'état d'une période, avec persistance.
     * 
     * @param periodeRecue
     *            la période à mettre à jour
     * @param etatPeriode
     *            l'état de la période
     */
    private void modifieEtatPeriode(PeriodeRecue periodeRecue, EtatPeriode etatPeriode) {
        periodeRecue.setEtatPeriode(etatPeriode);
        periodeRecueRepository.modifieEntiteExistante(periodeRecue);

    }

    /**
     * Creation d'une ligne de message controle pour la periode recue donnée, avec persistance.
     * 
     * @param periodeRecue
     *            periode recue dont la ligne de message controle doit être créée.
     * @param idControle
     *            l'identifiant de contrôle
     * @param dateTraitement
     *            la date de traitement lue dans la ligne de synthèse du fichier en cours de traitement
     * @param niveauAlerte
     *            le niveau d'alerte
     * @param individu
     *            l'individu concerné
     * @param msgUtilisateur
     *            le message utilisateur
     */
    private void creeMessageControle(PeriodeRecue periodeRecue, String idControle, Integer dateTraitement, String niveauAlerte, String individu,
            String msgUtilisateur) {
        MessageControle messageControle = new MessageControle();
        messageControle.setIdPeriode(periodeRecue.getIdPeriode());
        messageControle.setIdentifiantControle(idControle);
        messageControle.setOrigineMessage(ORIGINE_MESSAGE);
        messageControle.setDateTraitement(dateTraitement);
        messageControle.setNiveauAlerte(niveauAlerte);
        messageControle.setIndividu(individu);
        messageControle.setMessageUtilisateur(msgUtilisateur);
        messageControle.setAuditUtilisateurCreation(nomBatch);
        messageControleRepository.create(messageControle);
    }

    /**
     * Creation d'une entité d'historique d'état de période pour une période reçue donnée, avec persistance.
     * 
     * @param periodeRecue
     *            la période reçue dont la ligne d'historique d'état de période doit être créée.
     * @param etat
     *            l'état à renseigner
     * @param origineChangement
     *            l'application cible
     * @param identifiantUtilisateur
     *            l'identifiant de l'utilisateur
     * @param commentaireUtilisateur
     *            le commentaire de l'utilisateur
     */
    private void creeHistoriqueEtatPeriode(PeriodeRecue periodeRecue, EtatPeriode etat, String origineChangement, String identifiantUtilisateur,
            String commentaireUtilisateur) {
        HistoriqueEtatPeriode historiqueEtatPeriode = new HistoriqueEtatPeriode();
        historiqueEtatPeriode.setIdPeriode(periodeRecue.getIdPeriode());
        historiqueEtatPeriode.setDateHeureChangement(timeStampExecution);
        historiqueEtatPeriode.setEtatPeriode(etat);
        historiqueEtatPeriode.setOrigineChangement(origineChangement);
        historiqueEtatPeriode.setAuditUtilisateurCreation(nomBatch);
        if (StringUtils.isNotBlank(identifiantUtilisateur)) {
            historiqueEtatPeriode.setIdentifiantUtilisateur(identifiantUtilisateur);
        }
        if (StringUtils.isNotBlank(commentaireUtilisateur)) {
            historiqueEtatPeriode.setCommentaireUtilisateur(commentaireUtilisateur);
        }
        historiqueEtatPeriodeRepository.create(historiqueEtatPeriode);
    }

    /**
     * Creation d'une entité d'historique d'état de période pour une période reçue donnée, avec persistance.
     * 
     * @param periodeRecue
     *            la période reçue dont la ligne d'historique d'état de période doit être créée.
     * @param etat
     *            l'état à renseigner
     * @param origineChangement
     *            l'application cible
     */
    private void modifieHistoriqueEtatPeriodePourMessage(Long idPeriode) {

        HistoriqueEtatPeriode historiqueEtatPeriode = historiqueEtatPeriodeRepository.getHistoriqueEtatPeriodePourMessage(idPeriode,
                timeStampExecution);

        historiqueEtatPeriode.setEtatPeriode(EtatPeriode.INS);
        historiqueEtatPeriodeRepository.modifieEntiteExistante(historiqueEtatPeriode);
    }

    /**
     * Creation d'une ligne de compte rendu integration pour la periode recue donnée, avec persistance.
     * 
     * @param periodeRecue
     *            la periode recue dont la ligne de compte rendu doit être créée.
     * @param synthese
     *            Ligne de synthese du fichier compte rendu en cours de traitement.
     * 
     */
    private void creeCompteRenduIntegration(PeriodeRecue periodeRecue, Synthese synthese) {
        CompteRenduIntegration compteRenduIntegration = new CompteRenduIntegration();
        compteRenduIntegration.setIdPeriode(periodeRecue.getIdPeriode());
        compteRenduIntegration.setDateTraitement(synthese.getDateTraitementAsInteger());
        compteRenduIntegration.setEmetteurCr(synthese.getEmetteurCRAsInteger());
        compteRenduIntegration.setTypeFlux(synthese.getTypeFluxAsInteger());
        compteRenduIntegration.setCodeNature(synthese.getCompteRenduGlobalAsInteger());
        compteRenduIntegration.setGroupeGestionCotisation("");
        compteRenduIntegration.setReferencePaiement("");
        compteRenduIntegration.setAuditUtilisateurCreation(nomBatch);
        compteRenduIntegrationRepository.create(compteRenduIntegration);
    }

    /**
     * Creation d'une entité de compte rendu d'intégration pour une période reçue et un contratPeriode donnés.
     * 
     * @param periodeRecue
     *            la période reçue
     * @param contratPeriode
     *            le contrat sur période
     */
    private void creeCompteRenduIntegration(PeriodeRecue periodeRecue, ContratPeriode contratPeriode) {
        CompteRenduIntegration compteRenduIntegration = new CompteRenduIntegration();
        compteRenduIntegration.setIdPeriode(periodeRecue.getIdPeriode());
        compteRenduIntegration.setDateTraitement(contratPeriode.getDateTraitementSynthese());
        compteRenduIntegration.setEmetteurCr(contratPeriode.getEmetteurCRSynthese());
        compteRenduIntegration.setCodeNature(contratPeriode.getCodeNatureAsInteger());
        compteRenduIntegration.setTypeFlux(contratPeriode.getTypeFluxSynthese());
        compteRenduIntegration.setGroupeGestionCotisation(contratPeriode.getGroupeGestionCotisation());
        compteRenduIntegration.setReferencePaiement(contratPeriode.getReferencePaiement());
        compteRenduIntegration.setAuditUtilisateurCreation(nomBatch);
        compteRenduIntegrationRepository.create(compteRenduIntegration);

    }

    /**
     * Creation d'une entité PeriodeRecueTraitee pour une période reçue donnée.
     * 
     * @param periodeRecue
     *            la période reçue dont la PeriodeRecueTraitee doit être créée.
     */
    private void creePeriodeRecueTraitee(PeriodeRecue periodeRecue) {
        PeriodeRecueTraitee periodeRecueTraitee = new PeriodeRecueTraitee();
        periodeRecueTraitee.setIdPeriode(periodeRecue.getIdPeriode());
        periodeRecueTraitee.setNumeroContrat(periodeRecue.getNumeroContrat());
        periodeRecueTraitee.setDateDebutPeriode(periodeRecue.getDateDebutPeriode());
        periodeRecueTraitee.setAuditUtilisateurCreation(nomBatch);
        periodeRecueTraiteeRepository.create(periodeRecueTraitee);
    }

    /**
     * Nettoyage des entites presente dans periodeRecueTraiteeRepository
     * 
     * @param stepExecution
     *            le contexte d'exécution de l'étape
     * @return le statut du contexte d'exécution à l'issue du traitement
     */
    @AfterStep
    public ExitStatus viderPeriodeRecueTraitee(StepExecution stepExecution) {

        periodeRecueTraiteeRepository.nettoieToutesEntites();

        return stepExecution.getExitStatus();
    }

    /**
     * Contrôle la validité du champ codeNature dans la ligne contrat/période cf F78_RG_P4_14
     * 
     * @param contratPeriode
     *            La periode/contrat contrôlée
     * @return le code nature de la période/contrat passé en paramètre ( 0 ou 1 ) s'il est valide , null sinon
     */
    private Integer controleChampCodeNature(ContratPeriode contratPeriode) {
        Integer codeNature = contratPeriode.getCodeNatureAsInteger();
        if (codeNature == null) {
            valideChampAvecCollecteSansBlocage(false, contratPeriode, RedacMessages.CHAMP_CODE_NATURE,
                    MessageFormat.format(RedacMessages.ERREUR_CHAMP_NON_NUMERIQUE, RedacMessages.CHAMP_CODE_NATURE));
        } else if (codeNature != 0 && codeNature != 1) {
            String messageErreur = MessageFormat.format(RedacMessages.ERREUR_CHAMP_ERRONE_VALEUR_INATTENDUE, RedacMessages.CHAMP_CODE_NATURE,
                    "0 et de 1");

            valideChampAvecCollecteSansBlocage(false, contratPeriode, RedacMessages.CHAMP_CODE_NATURE, messageErreur);

            LOGGER.error(messageErreur);
            return null;
        }

        return codeNature;

    }

    /**
     * Contrôle la validité du champ codeRejet dans la ligne contrat/période cf F78_RG_P4_15
     * 
     * @param contratPeriode
     *            La periode/contrat contrôlée
     * @return true si valide
     */
    private boolean controleChampCodeRejet(ContratPeriode contratPeriode) {
        String codeRejet = contratPeriode.getCodeRejet();
        boolean validation = StringUtils.isNotBlank(codeRejet) && codeRejet.length() <= 3;

        if (!validation) {

            String messageErreur = MessageFormat.format(RedacMessages.ERREUR_CHAMP_OBLIGATOIRE, RedacMessages.CHAMP_CODE_REJET);

            valideChampAvecCollecteSansBlocage(false, contratPeriode, RedacMessages.CHAMP_CODE_REJET, messageErreur);

            LOGGER.error(messageErreur);
        }

        return validation;
    }

    /**
     * Contrôle la validité du champ libelleRejet dans la ligne contrat/période cf F78_RG_P4_16
     * 
     * @param contratPeriode
     *            La periode/contrat contrôlée
     * @return true si valide
     */
    private boolean controleChampLibelleRejet(ContratPeriode contratPeriode) {
        String libelleRejet = contratPeriode.getLibelleRejet();
        boolean validation = StringUtils.isNotBlank(libelleRejet) && libelleRejet.length() <= 100;

        if (!validation) {

            String messageErreur = MessageFormat.format(RedacMessages.ERREUR_CHAMP_OBLIGATOIRE, RedacMessages.CHAMP_LIBELLE_REJET);
            valideChampAvecCollecteSansBlocage(false, contratPeriode, RedacMessages.CHAMP_LIBELLE_REJET, messageErreur);

            LOGGER.error(messageErreur);
        }

        return validation;
    }

    /**
     * Contrôle la validité du champ codeNatureMessage dans le message cf F78_RG_P4_17
     * 
     * @param message
     *            Le message contrôlée
     * @return le code nature de la période/contrat passé en paramètre ( 0 ou 1 ) s'il est valide , null sinon
     */
    private Integer controleChampCodeNatureMessage(Message message) {
        Integer codeNature = message.getCodeNatureMessageAsInteger();
        if (codeNature == null) {
            valideChampAvecCollecteSansBlocage(false, message, RedacMessages.CHAMP_CODE_NATURE_MESSAGE,
                    MessageFormat.format(RedacMessages.ERREUR_CHAMP_NON_NUMERIQUE, RedacMessages.CHAMP_CODE_NATURE_MESSAGE));
        } else if (codeNature != 0 && codeNature != 1) {

            String messageErreur = MessageFormat.format(RedacMessages.ERREUR_CHAMP_ERRONE_VALEUR_INATTENDUE, RedacMessages.CHAMP_CODE_NATURE_MESSAGE,
                    "0 et de 1");

            valideChampAvecCollecteSansBlocage(false, message, RedacMessages.CHAMP_CODE_NATURE_MESSAGE, messageErreur);

            LOGGER.error(messageErreur);
            return null;
        }

        return codeNature;

    }

    /**
     * Contrôle la validité du champ codeNatureMessage dans le message cf F78_RG_P4_17
     * 
     * @param message
     *            Le message contrôlée
     * @return le code nature de la période/contrat passé en paramètre ( 0 ou 1 ) s'il est valide , null sinon
     */
    private Integer controleChampCodeNatureMessageAgrandies(MessageAgrandies message) {
        Integer codeNature = message.getCodeNatureMessageAsInteger();
        if (codeNature == null) {
            valideChampAvecCollecteSansBlocage(false, message, RedacMessages.CHAMP_CODE_NATURE_MESSAGE,
                    MessageFormat.format(RedacMessages.ERREUR_CHAMP_NON_NUMERIQUE, RedacMessages.CHAMP_CODE_NATURE_MESSAGE));
        } else if (codeNature != 0 && codeNature != 1) {

            String messageErreur = MessageFormat.format(RedacMessages.ERREUR_CHAMP_ERRONE_VALEUR_INATTENDUE, RedacMessages.CHAMP_CODE_NATURE_MESSAGE,
                    "0 et de 1");

            valideChampAvecCollecteSansBlocage(false, message, RedacMessages.CHAMP_CODE_NATURE_MESSAGE, messageErreur);

            LOGGER.error(messageErreur);
            return null;
        }

        return codeNature;

    }

}
