package fr.si2m.red.batch.moteur.item;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import org.springframework.batch.item.ItemStream;
import org.springframework.batch.item.file.FlatFileItemWriter;
import org.springframework.batch.item.file.transform.BeanWrapperFieldExtractor;
import org.springframework.batch.item.file.transform.FormatterLineAggregator;

import fr.si2m.red.batch.moteur.BatchConfiguration;

/**
 * Rédacteur de fichier plat sans conversion des champs des entités éditées dans le fichier.
 * 
 * @author nortaina
 *
 * @param <T>
 *            le type de l'entité à éditer dans le fichier rédigé
 */
public class RedacteurFichierPlatSansConversion<T> extends FlatFileItemWriter<T> implements ItemStream {

    /**
     * Constructeur par défaut pour le rédacteur de fichier plat sans convertisseur de champs.
     * 
     * @param nomsProprietesExtraites
     *            les noms des propriétés extraites telles quelles dans l'entité
     * @param format
     *            le format d'écriture des propriétés extraites
     */
    public RedacteurFichierPlatSansConversion(String[] nomsProprietesExtraites, String format) {
        FormatterLineAggregator<T> aggregateurLigneFormatter = new FormatterLineAggregator<T>();
        aggregateurLigneFormatter.setFormat(format);
        BeanWrapperFieldExtractor<T> extracteurChamps = new BeanWrapperFieldExtractor<T>();
        extracteurChamps.setNames(nomsProprietesExtraites);
        aggregateurLigneFormatter.setFieldExtractor(extracteurChamps);
        setLineAggregator(aggregateurLigneFormatter);
        setEncoding(BatchConfiguration.ENCODAGE_FICHIERS_EXPORTES.name());
        setLineSeparator("\r\n");
    }

    /**
     * Constructeur pour surcharges.
     * 
     */
    protected RedacteurFichierPlatSansConversion() {
        setEncoding(BatchConfiguration.ENCODAGE_FICHIERS_EXPORTES.name());
        setLineSeparator("\r\n");
    }

    @Override
    @SuppressWarnings("unchecked")
    public void write(List<? extends T> listeEnEntree) throws Exception {
        List<T> listeEnSortie = new LinkedList<T>();
        for (Object item : listeEnEntree) {
            if (item instanceof Collection<?>) {
                listeEnSortie.addAll((Collection<T>) item);
            } else {
                listeEnSortie.add((T) item);
            }
        }
        super.write(listeEnSortie);
    }

}
