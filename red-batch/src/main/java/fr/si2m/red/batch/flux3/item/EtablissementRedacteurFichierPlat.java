package fr.si2m.red.batch.flux3.item;


/**
 * Rédacteur du fichier corps du ETABLISSEMENT.
 * 
 * @author benitahy
 *
 * @param <T>
 *            le type de l'entité à éditer dans le fichier rédigé
 */
public class EtablissementRedacteurFichierPlat<T> extends RedacteurFichierPlatAvecSauvegardeNbLignes<T> {

    /**
     * Constructeur par défaut pour le rédacteur du fichier ENTREPRISE.
     * 
     * @param nomsProprietesExtraites
     *            les noms des propriétés extraites telles quelles dans l'entité
     * @param format
     *            le format d'écriture des propriétés extraites
     */
    public EtablissementRedacteurFichierPlat(String[] nomsProprietesExtraites, String format) {
        super(nomsProprietesExtraites, format);
        setShouldDeleteIfExists(true);
    }

    @Override
    public String getCleContexteNbLignes() {
        return "nbEtablissements";
    }

}
