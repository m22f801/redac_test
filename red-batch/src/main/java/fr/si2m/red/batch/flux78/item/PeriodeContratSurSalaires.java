package fr.si2m.red.batch.flux78.item;

import lombok.Getter;
import lombok.Setter;

/**
 * Modèle des périodes de contrat sur salaires publiées pour WQUI.
 * 
 * @author poidij
 *
 */
public class PeriodeContratSurSalaires extends PeriodePublieeWQUI {

    @Getter
    @Setter
    private String libelleTranche;
    @Getter
    @Setter
    private Integer codeNatureBase;
    @Getter
    @Setter
    private String signeMontantSalaire;
    @Getter
    @Setter
    private Long montantSalaire;
    @Getter
    @Setter
    private Integer tauxCotisation;
    @Getter
    @Setter
    private String fillerPartieSpecifique;

}
