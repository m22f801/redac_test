package fr.si2m.red.batch.moteur.item;

import org.springframework.batch.item.ItemProcessor;

import fr.si2m.red.batch.moteur.EtapeCodeRetourModificateur;

/**
 * Gestionnaire de traitement d'une ligne d'information REDAC.
 * 
 * @author poidij
 *
 * @param <I>
 *            type de la ligne à traiter
 * @param <O>
 *            type de la ligne traitée
 */
public abstract class ProcesseurLigne<I, O> extends EtapeCodeRetourModificateur implements ItemProcessor<I, O> {

}
