package fr.si2m.red.batch.flux10.item;

import org.apache.commons.lang3.StringUtils;

import fr.si2m.red.batch.moteur.erreur.ValidateurLigneAvecCollecte;
import fr.si2m.red.parametrage.ParamNatureBase;

/**
 * Validateur de ligne de paramétrage code libellé.
 * 
 * @author poidij
 *
 */
public class ParamNatureBaseValidateurLigne extends ValidateurLigneAvecCollecte<ParamNatureBase> {

    @Override
    protected void valide(final ParamNatureBase natureBase) {
        // Champs obligatoires

        valideChampAvecCollecte(natureBase.getCodeNatureBaseCotisations() != null, natureBase, "CONBCOT",
                "le champ CONBCOT obligatoire, n’est pas renseigné");

        valideChampAvecCollecte(StringUtils.isNotBlank(natureBase.getTauxCalculRempliAsText()), natureBase, "TXCALCU_REMPLI",
                "le champ TXCALCU_REMPLI obligatoire, n’est pas renseigné");

        valideChampAvecCollecte(natureBase.getNumTranche() != null, natureBase, "NUM_TRANCHE",
                "le champ NUM_TRANCHE obligatoire, n’est pas renseigné");

        // Longueur des champs
        if (natureBase.getCodeNatureBaseCotisations() != null) {
            valideChampAvecCollecte(natureBase.getCodeNatureBaseCotisations() < 100, natureBase, "CONBCOT",
                    "le champ CONBCOT dépasse la taille maximale prévue");
        }

        if (StringUtils.isNotBlank(natureBase.getTauxCalculRempliAsText())) {
            valideChampAvecCollecte(natureBase.getTauxCalculRempliAsText().length() <= 1, natureBase, "TXCALCU_REMPLI",
                    "le champ TXCALCU_REMPLI dépasse la taille maximale prévue");
        }

        if (natureBase.getNumTranche() != null) {
            valideChampAvecCollecte(natureBase.getNumTranche() < 10, natureBase, "NUM_TRANCHE",
                    "le champ NUM_TRANCHE dépasse la taille maximale prévue");
        }

        valideChampAvecCollecte(natureBase.getLibelleTranche().length() <= 5, natureBase, "LIB_TRANCHE",
                "le champ LIB_TRANCHE dépasse la taille maximale prévue");

        valideChampAvecCollecte(natureBase.getTauxUtilise().length() <= 30, natureBase, "TAUX_UTILISE",
                "le champ TAUX_UTILISE dépasse la taille maximale prévue");

        valideChampAvecCollecte(natureBase.getMontantUtilise().length() <= 30, natureBase, "MT_UTILISE",
                "le champ MT_UTILISE dépasse la taille maximale prévue");

        valideChampAvecCollecte(natureBase.getComplementDesignation().length() <= 50, natureBase, "CPLT_DESIGN",
                "le champ CPLT_DESIGN dépasse la taille maximale prévue");

    }

}
