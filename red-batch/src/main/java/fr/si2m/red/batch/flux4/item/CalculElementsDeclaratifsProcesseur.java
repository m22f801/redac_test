package fr.si2m.red.batch.flux4.item;

import java.text.MessageFormat;
import java.util.Iterator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import fr.si2m.red.RedacUnexpectedException;
import fr.si2m.red.batch.moteur.item.TransformateurDonneeAvecCollecte;
import fr.si2m.red.contrat.TarifRepository;
import fr.si2m.red.reconciliation.CategorieQuittancementIndividuRepository;
import fr.si2m.red.reconciliation.CompteRenduIntegrationRepository;
import fr.si2m.red.reconciliation.EffectifCategorieContratTravailRepository;
import fr.si2m.red.reconciliation.EffectifCategorieMouvementRepository;
import fr.si2m.red.reconciliation.HistoriqueAssignationPeriodeRepository;
import fr.si2m.red.reconciliation.HistoriqueAttenteRetourEtpPeriodeRepository;
import fr.si2m.red.reconciliation.HistoriqueCommentairePeriodeRepository;
import fr.si2m.red.reconciliation.HistoriqueEtatPeriodeRepository;
import fr.si2m.red.reconciliation.MessageControleRepository;
import fr.si2m.red.reconciliation.PeriodeRecue;
import fr.si2m.red.reconciliation.PeriodeRecueRepository;
import fr.si2m.red.reconciliation.RattachementDeclarationsRecuesRepository;
import fr.si2m.red.reconciliation.TrancheCategorieBaseAssujettieRepository;
import fr.si2m.red.reconciliation.TrancheCategorieRepository;
import lombok.Setter;

/**
 * Base pour les calculateurs d'éléments déclaratifs. Note : Cette classe n'est plus utilisée que pour les contrats sur effectifs. Les contrats sur salaires
 * utilisent désormais un traitement en BDD (modification d'architecture du batch suite aux travaux de perf ). Les TU utilisent encore cette ancienne
 * architecture commune entre contrats sur effectifs/salaires . Code non maintenu sur la partie inutilisée ( calculs sur contrats sur salaires version "Java")
 * par REDAC.
 * 
 * @author nortaina
 *
 */
public abstract class CalculElementsDeclaratifsProcesseur extends TransformateurDonneeAvecCollecte<PeriodeRecue, PeriodeRecue> {

    private static final Logger LOGGER = LoggerFactory.getLogger(CalculElementsDeclaratifsProcesseur.class);

    private static final String ERREUR_TARIF = "La situation tarifaire [numContrat={0}, numCategorieQuittancement={1}, dateDebutPériode={2}] n''existe pas. Période traitée : [idPeriode={3}; numContrat={4}; typePeriode={5}; dateDebutPeriode={6}; dateCreation={7}]";

    @Autowired
    private CategorieQuittancementIndividuRepository categorieQuittancementIndividuRepository;
    @Autowired
    private EffectifCategorieMouvementRepository effectifCategorieMouvementRepository;
    @Autowired
    private TrancheCategorieRepository trancheCategorieRepository;
    @Autowired
    private EffectifCategorieContratTravailRepository effectifCategorieContratTravailRepository;
    @Autowired
    private TrancheCategorieBaseAssujettieRepository trancheCategorieBaseAssujettieRepository;
    @Autowired
    private RattachementDeclarationsRecuesRepository rattachementDeclarationsRecuesRepository;
    @Autowired
    private CompteRenduIntegrationRepository compteRenduIntegrationRepository;
    @Autowired
    private HistoriqueEtatPeriodeRepository historiqueEtatPeriodeRepository;
    @Autowired
    private MessageControleRepository messageControleRepository;
    @Autowired
    private PeriodeRecueRepository periodeRecueRepository;
    @Autowired
    private TarifRepository tarifRepository;
    @Autowired
    private HistoriqueAssignationPeriodeRepository historiqueAssignationPeriodeRepository;
    @Autowired
    private HistoriqueAttenteRetourEtpPeriodeRepository historiqueAttenteRetourEtpPeriodeRepository;
    @Autowired
    private HistoriqueCommentairePeriodeRepository historiqueCommentairePeriodeRepository;

    @Setter
    private String cheminAbsoluFichierLogErreurs;

    @Override
    public final PeriodeRecue process(PeriodeRecue periode) throws Exception {
        try {
            verificationNocatAffiliation(periode);
            calculElementsDeclaratifs(periode);
            return periode;
        } catch (Exception e) {
            if (e instanceof RedacUnexpectedException) {
                traceErreurSansBlocage(e.getMessage(), cheminAbsoluFichierLogErreurs);
            } else {
                LOGGER.info("Exception détectée", e);
            }

            traiteErreurImprevue(periode);
            return null;
        }
    }

    /**
     * Calcule les éléments déclaratifs pour la période donnée.
     * 
     * @param periode
     *            la période
     */
    protected abstract void calculElementsDeclaratifs(PeriodeRecue periode);

    /**
     * Traite les erreurs imprévues
     * 
     * @param idPeriode
     *            l'identifiant de la période
     * @param erreur
     *            l'erreur
     */
    private void traiteErreurImprevue(PeriodeRecue periode) {
        Long idPeriode = periode.getIdPeriode();

        // Purge éléments déclaratifs calculés
        purgeElementDeclaratifs(idPeriode);

        // Détache les adhésions
        int nbDetachements = rattachementDeclarationsRecuesRepository.detacheAdhesionsDernierLotRattachementPourPeriode(idPeriode);

        traceErreurSansBlocage("Erreur détectée lors du calcul d'éléments déclaratifs - détachement de " + nbDetachements
                + " adhésions pour la période " + idPeriode, cheminAbsoluFichierLogErreurs);

        // Si la période est "vide", on supprime toute les entitées pouvant y être rattachée,
        // avant de supprimer la période elle même. On ne lance pas de recalcul.
        if (!rattachementDeclarationsRecuesRepository.existeRattachementDeclarationRecue(idPeriode)) {
            compteRenduIntegrationRepository.supprimeCompteRenduIntegrationPourPeriode(idPeriode);
            historiqueEtatPeriodeRepository.supprimeHistoriqueEtatPeriodePourPeriode(idPeriode);
            messageControleRepository.supprimeMessageControlePourPeriode(idPeriode);
            historiqueAssignationPeriodeRepository.supprimeHistoriqueAssignationPourPeriode(idPeriode);
            historiqueAttenteRetourEtpPeriodeRepository.supprimeHistoriqueAttenteRetourEtpPourPeriode(idPeriode);
            historiqueCommentairePeriodeRepository.supprimeHistoriqueCommentairePourPeriode(idPeriode);
            periodeRecueRepository.supprimePeriodeRecue(idPeriode);
        } else {
            // Revert

            try {
                calculElementsDeclaratifs(periode);
            } catch (Exception e) {
                purgeElementDeclaratifs(idPeriode);
                traceErreurSansBlocage("Erreur pendant le recalcul de la période " + idPeriode, cheminAbsoluFichierLogErreurs);
                LOGGER.info("Erreur pendant le recalcul de la période " + idPeriode, e);

                // pour le que 406 ne tienne pas compte de la période en erreur
                periodeRecueRepository.modifieReconsoliderPeriodeRecue(idPeriode, "N");
            }

        }

    }

    /**
     * Purge les éléments déclaratifs d'une période
     * 
     * @param idPeriode
     *            l'identifiant de la période
     */
    private void purgeElementDeclaratifs(Long idPeriode) {
        categorieQuittancementIndividuRepository.supprimePourPeriode(idPeriode);
        effectifCategorieMouvementRepository.supprimePourPeriode(idPeriode);
        trancheCategorieRepository.supprimePourPeriode(idPeriode);
        effectifCategorieContratTravailRepository.supprimePourPeriode(idPeriode);
        historiqueAssignationPeriodeRepository.supprimeHistoriqueAssignationPourPeriode(idPeriode);
        historiqueAttenteRetourEtpPeriodeRepository.supprimeHistoriqueAttenteRetourEtpPourPeriode(idPeriode);
        historiqueCommentairePeriodeRepository.supprimeHistoriqueCommentairePourPeriode(idPeriode);
        trancheCategorieBaseAssujettieRepository.supprimePourPeriode(idPeriode);
    }

    /**
     * Vérifie la validitée du Nocat des affiliations associées à la période ( cf F04_RG_P5_03)
     * 
     * @param periode
     *            la période courante
     */
    private void verificationNocatAffiliation(PeriodeRecue periode) {

        // récupération des nocat des affiliations
        List<String> nocatAffiliations = rattachementDeclarationsRecuesRepository.getCodePopulationAffiliation(periode.getIdPeriode());

        if (nocatAffiliations != null && !nocatAffiliations.isEmpty()) {
            // suppression valeur vide, si présente
            Iterator<String> itr = nocatAffiliations.iterator();
            while (itr.hasNext()) {
                String code = itr.next();
                if (code.isEmpty()) {
                    itr.remove();
                }
            }
        }

        if (nocatAffiliations != null && !nocatAffiliations.isEmpty()) {
            // récupération des nocat des situations tarifaires de la période
            List<String> nocatValides = tarifRepository.getDistinctNumCategoriePourContratEtDate(periode.getNumeroContrat(),
                    periode.getDateDebutPeriode());
            if (nocatValides != null && !nocatValides.isEmpty()) {
                // suppression valeur vide, si présente
                Iterator<String> itr = nocatValides.iterator();
                while (itr.hasNext()) {
                    String code = itr.next();
                    if (code.isEmpty()) {
                        itr.remove();
                    }
                }
            }

            for (String nocatAffiliation : nocatAffiliations) {
                if (nocatValides == null || !nocatValides.contains(nocatAffiliation)) {
                    // nocat inconnu
                    String logErreur = MessageFormat.format(ERREUR_TARIF, periode.getNumeroContrat(), nocatAffiliation,
                            periode.getDateDebutPeriode().toString(), periode.getIdPeriode(), periode.getNumeroContrat(), periode.getTypePeriode(),
                            periode.getDateDebutPeriode().toString(), periode.getDateCreation().toString());
                    throw new RedacUnexpectedException(logErreur);
                }
            }
        }

    }

}
