package fr.si2m.red.batch.flux78.item;

import fr.si2m.red.batch.moteur.item.RedacteurFichierPlatSansConversion;

/**
 * Rédacteur du fichier de publication contrats sur salaires pour WQUI.
 * 
 * @param <T>
 *            le type de donnée à publier
 */
public class PublicationRedacteurFichierPlat<T> extends RedacteurFichierPlatSansConversion<T> {

    /**
     * Constructeur par défaut pour le rédacteur de fichier plat sans convertisseur de champs.
     * 
     */
    public PublicationRedacteurFichierPlat() {
        super();
        setLineAggregator(new PublicationLineAggregator<T>());
    }

}
