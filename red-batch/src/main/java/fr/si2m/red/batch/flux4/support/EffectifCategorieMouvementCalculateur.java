package fr.si2m.red.batch.flux4.support;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Set;
import java.util.TreeSet;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import fr.si2m.red.reconciliation.CategorieQuittancementIndividu;
import fr.si2m.red.reconciliation.EffectifCategorieContratTravail;
import fr.si2m.red.reconciliation.EffectifCategorieContratTravailRepository;
import fr.si2m.red.reconciliation.EffectifCategorieMouvement;
import fr.si2m.red.reconciliation.PeriodeRecue;

/**
 * Constructeur d'entité {@link EffectifCategorieMouvement} basées sur les RDG de la phase 5 du flux 4.
 * 
 * @author nortaina
 *
 */
@Component
public class EffectifCategorieMouvementCalculateur {

    @PersistenceContext
    private EntityManager entityManager;

    private final String auditNomBatch;

    @Autowired
    private EffectifCategorieContratTravailRepository effectifCategorieContratTravailRepository;

    /**
     * Constructeur par défaut du calculateur.
     * 
     */
    public EffectifCategorieMouvementCalculateur() {
        this.auditNomBatch = "RR405";
    }

    /**
     * Constructeur du calculateur.
     * 
     * @param auditNomBatch
     *            le nom du batch appelant le calculateur
     */
    public EffectifCategorieMouvementCalculateur(String auditNomBatch) {
        this.auditNomBatch = auditNomBatch;
    }

    /**
     * Calcule les entités pour la période reçue.
     * 
     * @param periode
     *            la période reçue
     * @param categoriesQuittancementIndividu
     *            les quittancements d'individu de référence pour la période reçue
     * @return le nombre d'entités persistées
     */
    public long calculeEtPersiste(PeriodeRecue periode, List<CategorieQuittancementIndividu> categoriesQuittancementIndividu) {
        long nbEntitesPersistees = 0L;

        for (CategorieQuittancementIndividu categorieQuittancementIndividu : categoriesQuittancementIndividu) {
            // Récupération des des lignes de l’entité EffectifCategorieContratTravail rattachées à la PeriodeRecue (via RattachementDeclarationsRecues,
            // AdhesionEtablissementMois, Individu et ContratTravail), dont le numCategorie est égal à CategorieQuittancementIndividu.numCategorie et dont
            // totalEffectif est différent de 0
            List<EffectifCategorieContratTravail> listeEffectifCategorieContratTravail = effectifCategorieContratTravailRepository
                    .getPourPeriodeEtCategorieQuittancementAvecTotalEffectifNonNul(periode.getIdPeriode(),
                            categorieQuittancementIndividu.getNumCategorieQuittancement());

            // Création des mouvements
            List<EffectifCategorieMouvement> effectifsCategoriesMouvementPourCategorieIndividu = initialiseMouvements(periode,
                    categorieQuittancementIndividu, listeEffectifCategorieContratTravail);

            // Reconsolidation par rapprochement de lignes juxtaposées
            reconsolideListeMouvements(effectifsCategoriesMouvementPourCategorieIndividu);

            // On enregistre tous les éléments calculés
            for (EffectifCategorieMouvement mouvement : effectifsCategoriesMouvementPourCategorieIndividu) {
                entityManager.persist(mouvement);
                nbEntitesPersistees++;
            }
            entityManager.flush();
            entityManager.clear();
        }

        return nbEntitesPersistees;
    }

    /**
     * Initialise des mouvements pour un quittancement donné, en s'appuyant sur une liste de contrats.
     * 
     * @param periode
     *            la période
     * @param categorieQuittancementIndividu
     *            le quittancement
     * @param listeEffectifCategorieContratTravail
     *            la liste des contrats
     * @return les mouvements par catégorie initialisés
     */
    private List<EffectifCategorieMouvement> initialiseMouvements(PeriodeRecue periode,
            CategorieQuittancementIndividu categorieQuittancementIndividu, List<EffectifCategorieContratTravail> listeEffectifCategorieContratTravail) {
        // Récupérer dans cette ListeEffectifCategorieContratTravail l’ensemble des dateDebutBase et dateFinBase distinctes différentes de dateDebutPeriode
        // et de dateFinPeriode de la PeriodeRecue, triées de la plus ancienne à la plus récente.
        Set<Integer> datesMouvements = new TreeSet<>();
        for (EffectifCategorieContratTravail effectifContratTravail : listeEffectifCategorieContratTravail) {
            if (!effectifContratTravail.getDateDebutBase().equals(periode.getDateDebutPeriode())) {
                datesMouvements.add(effectifContratTravail.getDateDebutBase());
            }
            if (!effectifContratTravail.getDateFinBase().equals(periode.getDateFinPeriode())) {
                datesMouvements.add(effectifContratTravail.getDateFinBase());
            }
        }

        // Le tri naturel des dates dans le TreeSet assure le parcours de la plus ancienne à la plus récente
        Iterator<Integer> itDateMouvement = datesMouvements.iterator();
        List<EffectifCategorieMouvement> effectifsCategoriesMouvementPourCategorieIndividu = new ArrayList<>(datesMouvements.size());
        while (itDateMouvement.hasNext()) {
            Integer dateMouvement = itDateMouvement.next();
            EffectifCategorieMouvement effectifCategorieMouvement = new EffectifCategorieMouvement();
            effectifCategorieMouvement.setIdPeriode(periode.getIdPeriode());
            effectifCategorieMouvement.setNumCategorieQuittancement(categorieQuittancementIndividu.getNumCategorieQuittancement());
            effectifCategorieMouvement.setDateEffetMouvement(dateMouvement);
            effectifCategorieMouvement.setNombreMouvement(calculeNombreMouvement(dateMouvement, listeEffectifCategorieContratTravail).intValue());
            effectifCategorieMouvement.setAuditUtilisateurCreation(this.auditNomBatch);
            effectifsCategoriesMouvementPourCategorieIndividu.add(effectifCategorieMouvement);
        }
        return effectifsCategoriesMouvementPourCategorieIndividu;
    }

    /**
     * Calcule le nombre de mouvements pour une date de mouvement donnée.
     * 
     * @param dateMouvement
     *            la date du mouvement
     * @param listeEffectifCategorieContratTravail
     *            la liste des effectifs de contrat de travail de référence
     * @return le nombre de mouvements pour une date de mouvement donnée
     */
    private Long calculeNombreMouvement(Integer dateMouvement, List<EffectifCategorieContratTravail> listeEffectifCategorieContratTravail) {
        Long sommeTotauxEffectifsDebuts = 0L;
        Long sommeTotauxEffectifsFins = 0L;
        for (EffectifCategorieContratTravail effectifCategorieContratTravail : listeEffectifCategorieContratTravail) {
            if (effectifCategorieContratTravail.getDateDebutBase().equals(dateMouvement)) {
                sommeTotauxEffectifsDebuts += effectifCategorieContratTravail.getTotalEffectif();
            }
            if (effectifCategorieContratTravail.getDateFinBase().equals(dateMouvement)) {
                sommeTotauxEffectifsFins += effectifCategorieContratTravail.getTotalEffectif();
            }
        }
        return sommeTotauxEffectifsDebuts - sommeTotauxEffectifsFins;
    }

    /**
     * Reconsolide une liste de mouvements en supprimant certains mouvements inutiles (après merge de leurs informations dans les mouvements précédents ou
     * suivants à +/- 1jour).
     * 
     * @param effectifsCategoriesMouvementPourCategorieIndividu
     *            la liste à consolider
     */
    private void reconsolideListeMouvements(List<EffectifCategorieMouvement> effectifsCategoriesMouvementPourCategorieIndividu) {
        // On supprime les mouvements à 0 au préalable
        ListIterator<EffectifCategorieMouvement> itMouvements = effectifsCategoriesMouvementPourCategorieIndividu.listIterator();
        while (itMouvements.hasNext()) {
            EffectifCategorieMouvement mouvement = itMouvements.next();
            if (mouvement.getNombreMouvement() == 0) {
                itMouvements.remove();
            }
        }
        // Sur les lignes triées, s'il existe une ligne dans l’entité EffectifCategorieMouvement rattachée à la CategorieQuittancementIndividu en cours dont
        // la dateEffetMouvement est égale à la dateEffetMouvement de la ligne traitée + 1 jour et dont le signe du nombreMouvement est différent du signe
        // de nombreMouvement de la ligne traitée, cette ligne est appelée LigneSuivante
        itMouvements = effectifsCategoriesMouvementPourCategorieIndividu.listIterator();
        while (itMouvements.hasNext()) {
            EffectifCategorieMouvement ligneTraitee = itMouvements.next();
            EffectifCategorieMouvement ligneSuivante = null;
            if (itMouvements.hasNext()) {
                EffectifCategorieMouvement candidateLigneSuivante = itMouvements.next();
                // On repositionne systématiquement le curseur ("next" + "previous" = "peak")
                itMouvements.previous();
                if (candidateLigneSuivante.getDateEffetMouvement() == ligneTraitee.getDateEffetMouvement() + 1
                        && Integer.signum(candidateLigneSuivante.getNombreMouvement()) != Integer.signum(ligneTraitee.getNombreMouvement())) {
                    ligneSuivante = candidateLigneSuivante;
                }
            }

            if (ligneSuivante != null) {
                // On repositione le curseur sur la ligne traitée => "previous" repositionne sur la ligne traitée ici
                itMouvements.previous();
                Integer nbMouvementLigneTraitee = ligneTraitee.getNombreMouvement();
                Integer nbMouvementLigneSuivante = ligneSuivante.getNombreMouvement();
                if (nbMouvementLigneTraitee + nbMouvementLigneSuivante == 0) {
                    // Supprimer les deux lignes de l’entité EffectifCategorieMouvement
                    itMouvements.remove();
                    itMouvements.next();
                    itMouvements.remove();
                } else if (Math.abs(nbMouvementLigneTraitee) > Math.abs(nbMouvementLigneSuivante)) {
                    ligneTraitee.setNombreMouvement(nbMouvementLigneTraitee + nbMouvementLigneSuivante);
                    // Suppression ligne suivante : on fait deux fois "next" après l'appel de "previous", sinon on est encore sur la ligne traitée !
                    itMouvements.next();
                    itMouvements.next();
                    itMouvements.remove();
                } else {
                    ligneSuivante.setNombreMouvement(nbMouvementLigneTraitee + nbMouvementLigneSuivante);
                    // Suppression ligne traitée
                    itMouvements.remove();
                }
            }
        }
    }
}
