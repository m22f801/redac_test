package fr.si2m.red.batch.flux3.item;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

import fr.si2m.red.batch.flux3.ligne.ContratEtablissement;
import fr.si2m.red.batch.flux3.ligne.SituationContrat;
import fr.si2m.red.batch.moteur.item.TransformateurDonnee;
import fr.si2m.red.complement.ExtensionEntrepriseAffilieeRepository;
import fr.si2m.red.contrat.ClientRepository;
import fr.si2m.red.parametrage.ParamSirenFauxRepository;
import lombok.Setter;

/**
 * Processeur des lignes contrat-établissement à exporter à partir de situations de contrats identifiées.
 * 
 * @author poidij
 *
 */
public class ContratEtablissementProcesseurLigne extends TransformateurDonnee<SituationContrat, List<ContratEtablissement>> {

    @Setter
    private ClientRepository clientRepository;

    @Setter
    private ExtensionEntrepriseAffilieeRepository extensionEntrepriseAffilieeRepository;

    @Autowired
    private ParamSirenFauxRepository paramSirenFauxRepository;

    @Override
    public List<ContratEtablissement> process(SituationContrat item) {

        String siretClient = clientRepository.getSiret(item.getNumClient());
        List<String> siretsEtablissementsAffilies = extensionEntrepriseAffilieeRepository.getSiretEtablissementsAffilies(item.getNumContrat());

        // Fusion des SIRET en supprimant les doublons
        Set<String> siretsDedoublonnes = new HashSet<String>();
        siretsDedoublonnes.add(siretClient);
        siretsDedoublonnes.addAll(siretsEtablissementsAffilies);

        // on supprime les siren qui sont présent dans PARAM_SIREN_FAUX
        siretsDedoublonnes = controleSiren(siretsDedoublonnes);

        // RG F03_RG_S06
        // gestion de la situation d'affiliation
        // Pour obtenir les sirets on utilise une listes de Siret des extensions etablissements en dedoublonnant le Siret du contrat

        // Liste des couples situationContrat-siret
        List<ContratEtablissement> contratsEtablissements = new ArrayList<ContratEtablissement>(siretsDedoublonnes.size());
        for (String siret : siretsDedoublonnes) {
            ContratEtablissement contratEtablissement = new ContratEtablissement();
            contratEtablissement.setIdentifiantTechniqueEntreprise(siret);
            contratEtablissement.setIdentifiantTechniqueContrat(item.getNumContrat());
            contratEtablissement.setCodeMiseAJour("R");
            contratEtablissement.setDateDebutSituation(item.getDateDebutSituation());
            contratEtablissement.setDateFinSituation(item.getDateFinSituation());

            contratsEtablissements.add(contratEtablissement);
        }
        return contratsEtablissements;

    }

    /**
     * Controle des siren
     * 
     * @param l'ensemble
     *            des siret à controler (le siren est extrait du siret)
     * @return les siren valides ( dont le siren est non présent dans param_siren_faux)
     */
    private Set<String> controleSiren(Set<String> set) {
        Set<String> siretFiltres = new HashSet<String>();
        Iterator<String> it = set.iterator();
        while (it.hasNext()) {
            String item = it.next();
            if (!paramSirenFauxRepository.existeSiren(StringUtils.substring(item, 0, 9))) {
                siretFiltres.add(item);
            }
        }
        return siretFiltres;
    }
}
