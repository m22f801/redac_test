package fr.si2m.red.batch.flux4.item;

import lombok.Setter;

import org.apache.commons.lang3.StringUtils;

import fr.si2m.red.batch.RedacMessages;
import fr.si2m.red.batch.moteur.erreur.ValidateurLigneAvecCollecte;
import fr.si2m.red.dsn.Affiliation;
import fr.si2m.red.dsn.AffiliationRepository;
import fr.si2m.red.dsn.ContratTravailRepository;

/**
 * Validateur de ligne de paramétrage code libellé.
 * 
 * @author benitahy
 *
 */
public class AffiliationValidateurLigne extends ValidateurLigneAvecCollecte<Affiliation> {

    @Setter
    private ContratTravailRepository contratTravailRepository;

    @Setter
    private AffiliationRepository affiliationRepository;

    @Override
    protected void valide(final Affiliation affiliation) {

        // unicité
        valideChampAvecCollecte(!affiliationRepository.existeUneAffiliation(affiliation.getIdAffiliation()), affiliation, "ID", "La ligne ne respecte pas la règle d'unicité");

        // Champs obligatoires
        valideChampAvecCollecte(StringUtils.isNotBlank(affiliation.getIdAffiliation()), affiliation, "ID_AFFILIATION", "Le champ ID_AFFILIATION obligatoire n'est pas renseigné");
        valideChampAvecCollecte(StringUtils.isNotBlank(affiliation.getIdContratTravail()), affiliation, "ID_CONTRAT_TRAVAIL",
                "Le champ ID_CONTRAT_TRAVAIL obligatoire n'est pas renseigné");

        // Longueur des champs
        valideTailleFixeChampAvecCollecte(affiliation.getIdAffiliation(), 30, affiliation, "ID_AFFILIATION");
        valideTailleFixeChampAvecCollecte(affiliation.getIdContratTravail(), 30, affiliation, "ID_CONTRAT_TRAVAIL");
        valideTailleFixeChampAvecCollecte(affiliation.getCodeOption(), 30, affiliation, "CODE_OPTION");
        valideTailleFixeChampAvecCollecte(affiliation.getCodePopulation(), 30, affiliation, "CODE_POPULATION");
        valideChampIntegerTailleFixeAvecCollecte(affiliation.getNombreEnfantsChargeAsText(), 2, affiliation, "NOMBRE_ENFANTS_CHARGE");
        valideChampIntegerTailleFixeAvecCollecte(affiliation.getNombreAyantDroitAdultesAsText(), 2, affiliation, "NOMBRE_AYDR_ADULTES");
        valideChampIntegerTailleFixeAvecCollecte(affiliation.getNombreAyantDroitAsText(), 2, affiliation, "NOMBRE_AYDR");
        valideChampIntegerTailleFixeAvecCollecte(affiliation.getNombreAyantDroitAutresAsText(), 2, affiliation, "NOMBRE_AYDR_AUTRES");
        valideChampIntegerTailleFixeAvecCollecte(affiliation.getNombreAyantDroitEnfantsAsText(), 2, affiliation, "NOMBRE_AYDR_ENFANTS");

        validationCorrespondanceReferentielle(affiliation);
    }

    /**
     * Validation des liens entre tables
     * 
     * @param affiliation
     *            L'affiliation de travail dont contrat de travail doit être validé.
     */
    private void validationCorrespondanceReferentielle(final Affiliation affiliation) {
        if (StringUtils.isNotBlank(affiliation.getIdContratTravail())) {
            boolean existeUnContratTravail = contratTravailRepository.existeUnContratTravail(affiliation.getIdContratTravail());
            valideChampAvecCollecte(existeUnContratTravail, affiliation, "IdContratTravail", RedacMessages.ERREUR_REFERENCE_FLUX4);
        }
    }
}
