package fr.si2m.red.batch.flux10.item;

import java.text.MessageFormat;

import org.apache.commons.lang3.StringUtils;

import fr.si2m.red.batch.RedacMessages;
import fr.si2m.red.batch.moteur.erreur.ValidateurLigneAvecCollecte;
import fr.si2m.red.parametrage.ParamUtilisateurContratVIP;

/***
 * Validateur de ligne.
 * 
 * @author delortj
 *
 */
public class ParamUtilisateurContratVIPValidateurLigne extends ValidateurLigneAvecCollecte<ParamUtilisateurContratVIP> {

    private static final String CHAMP_CODE_USER = "CODE_USER";
    private static final String CHAMP_NOCO = "NOCO";

    @Override
    protected void valide(final ParamUtilisateurContratVIP paramUtilisateurContratsVIP) {

        // Champ CODE_USER
        String codeUtilisateur = paramUtilisateurContratsVIP.getCodeUtilisateur();
        // Champ CODE_USER obligatoire
        valideChampAvecCollecte(StringUtils.isNotBlank(codeUtilisateur), paramUtilisateurContratsVIP, CHAMP_CODE_USER,
                MessageFormat.format(RedacMessages.ERREUR_CHAMP_OBLIGATOIRE, CHAMP_CODE_USER));
        // Longueur du champ CODE_USER
        if (StringUtils.isNotBlank(codeUtilisateur)) {
            valideChampAvecCollecte(codeUtilisateur.length() <= 50, paramUtilisateurContratsVIP, CHAMP_CODE_USER,
                    MessageFormat.format(RedacMessages.ERREUR_CHAMP_TAILLE_DEPASSEE, CHAMP_CODE_USER));
        }

        // Champ NOCO
        String numContrat = paramUtilisateurContratsVIP.getNumContrat();
        // Champ NOCO obligatoire
        valideChampAvecCollecte(StringUtils.isNotBlank(numContrat), paramUtilisateurContratsVIP, CHAMP_NOCO,
                MessageFormat.format(RedacMessages.ERREUR_CHAMP_OBLIGATOIRE, CHAMP_NOCO));
        // Longueur du champ NOCO
        if (StringUtils.isNotBlank(numContrat)) {
            valideChampAvecCollecte(numContrat.length() <= 15, paramUtilisateurContratsVIP, CHAMP_NOCO,
                    MessageFormat.format(RedacMessages.ERREUR_CHAMP_TAILLE_DEPASSEE, CHAMP_NOCO));
        }

    }
}
