package fr.si2m.red.batch.flux4.support;

import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.job.flow.FlowExecutionStatus;
import org.springframework.batch.core.job.flow.JobExecutionDecider;
import org.springframework.beans.factory.annotation.Autowired;

import fr.si2m.red.reconciliation.PeriodeRecueRepository;

/**
 * Décideur de statut selon l'existence de période à reconsolider.
 * 
 * @author eudesr
 *
 */
public class PeriodeAReconsoliderDecideur implements JobExecutionDecider {

    @Autowired
    private PeriodeRecueRepository periodeRecueRepository;

    @Override
    public FlowExecutionStatus decide(JobExecution jobExecution, StepExecution stepExecution) {
        return new FlowExecutionStatus(periodeRecueRepository.existePeriodeAReconsolider() ? "OK" : "COMPLETED");
    }
}
