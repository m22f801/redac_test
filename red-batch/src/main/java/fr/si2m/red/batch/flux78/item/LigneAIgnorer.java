package fr.si2m.red.batch.flux78.item;

import lombok.Data;
import lombok.EqualsAndHashCode;
import fr.si2m.red.EntiteImportableBatch;

/**
 * Représentation d'une ligne à ignorer pour un traitement de compte-rendu.
 * 
 * @author poidij
 *
 */
@Data
@EqualsAndHashCode(callSuper = false, of = "ligne")
public class LigneAIgnorer extends EntiteImportableBatch {

    /**
     * UID de version.
     */
    private static final long serialVersionUID = 7002037734533624514L;

    /**
     * La ligne en erreur.
     * 
     * @param ligne
     *            la ligne
     * @return la ligne
     * 
     */
    private String ligne;

    @Override
    public boolean isLigneEnCoursImportBatch() {
        return false;
    }

    @Override
    public void setLigneEnCoursImportBatch(boolean ligneEnCoursImportBatch) {
        // Rien
    }

    @Override
    public String getId() {
        return ligne;
    }

}
