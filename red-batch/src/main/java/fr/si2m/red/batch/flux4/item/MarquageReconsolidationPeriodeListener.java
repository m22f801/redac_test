package fr.si2m.red.batch.flux4.item;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.annotation.AfterProcess;
import org.springframework.beans.factory.annotation.Autowired;

import fr.si2m.red.reconciliation.MessageControle;
import fr.si2m.red.reconciliation.PeriodeRecue;
import fr.si2m.red.reconciliation.PeriodeRecueRepository;

/**
 * Marquage d'une période comme reconsolidé en fin de traitement de reconsolidation.<br/>
 * <br/>
 * c.f. RDG F04_RG_P6_11
 * 
 * @author nortaina
 *
 */
public class MarquageReconsolidationPeriodeListener {
    private static final Logger LOGGER = LoggerFactory.getLogger(MarquageReconsolidationPeriodeListener.class);

    @Autowired
    private PeriodeRecueRepository periodeRecueRepository;

    /**
     * Exécution du marquage d'une période donnée comme reconsolidée.
     * 
     * @param periode
     *            la période à marquer comme reconsolidée
     * @param resultatsControles
     *            les résultats des contrôles effectués
     */
    @AfterProcess
    public void afterProcess(PeriodeRecue periode, List<MessageControle> resultatsControles) {
        if (resultatsControles == null) {
            // Erreur sur les contrôles (on est passé dans le catch de CalculMessagesControlesProcesseur) => skip
            // La période ne doit pas passer à "RECONSOLIDER = N", afin de pouvoir être reprise.
            LOGGER.info("Période non reconsolidée pour cause d'erreur : {}. Sera reprise au prochain traitement RR405.", periode.getIdPeriode());
            return;
        }
        periode.setReconsolider(false);
        periodeRecueRepository.modifieEntiteExistante(periode);
        LOGGER.debug("Génération de {} messages de contrôle pour la période reconsolidée {}", resultatsControles.size(), periode.getIdPeriode());
    }

}
