package fr.si2m.red.batch.flux3.item;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;

import fr.si2m.red.batch.flux3.ligne.ContratEntreprise;
import fr.si2m.red.batch.flux3.ligne.SituationContrat;
import fr.si2m.red.batch.moteur.item.TransformateurDonnee;
import fr.si2m.red.complement.ExtensionEntrepriseAffilieeRepository;
import fr.si2m.red.contrat.ClientRepository;
import fr.si2m.red.parametrage.ParamSirenFauxRepository;
import lombok.Setter;

/**
 * Processeur des lignes contrat-entreprise à exporter à partir de situations de contrats identifiées.
 * 
 * @author poidij
 *
 */
public class ContratEntrepriseProcesseurLigne extends TransformateurDonnee<SituationContrat, List<ContratEntreprise>> {

    @Setter
    private ClientRepository clientRepository;

    @Setter
    private ExtensionEntrepriseAffilieeRepository extensionEntrepriseAffilieeRepository;

    @Autowired
    private ParamSirenFauxRepository paramSirenFauxRepository;

    @Override
    public List<ContratEntreprise> process(SituationContrat item) {

        // Liste des couples situationContrat-siren

        String sirenClient = clientRepository.getSiren(item.getNumClient());
        List<String> lesSirenAffiliee = extensionEntrepriseAffilieeRepository.getSirenEntreprisesAffiliees(item.getNumContrat());

        // liste fusionnant les deux listes de siren
        Set<String> fusion = new HashSet<String>();
        fusion.add(sirenClient);
        fusion.addAll(lesSirenAffiliee);

        // on supprime les siren qui sont présent dans PARAM_SIREN_FAUX
        fusion = controleSiren(fusion);

        // RG F03_RG_S04
        // gestion de la situation d'affiliation
        // si il y a conflit entre les sources du siren, on prend le siren de l'entitée Client

        List<ContratEntreprise> listeContratsEntreprise = new ArrayList<ContratEntreprise>();
        for (String e : fusion) {
            ContratEntreprise unContratEntreprise = new ContratEntreprise();
            unContratEntreprise.setIdentifiantTechniqueContrat(item.getNumContrat());

            unContratEntreprise.setCodeMiseAJour("R");
            unContratEntreprise.setDateDebutSituation(item.getDateDebutSituation());
            unContratEntreprise.setDateFinSituation(item.getDateFinSituation());

            if (e != null) {
                unContratEntreprise.setIdentifiantTechniqueEntreprise(e);
            }

            listeContratsEntreprise.add(unContratEntreprise);
        }

        return listeContratsEntreprise;
    }

    /**
     * Controle des siren
     * 
     * @param l'ensemble
     *            des siren à controler
     * @return les siren valides ( non présents dans param_siren_faux)
     */
    private Set<String> controleSiren(Set<String> set) {
        Set<String> sirenFiltres = new HashSet<String>();
        Iterator<String> it = set.iterator();
        while (it.hasNext()) {
            String item = it.next();
            if (!paramSirenFauxRepository.existeSiren(item)) {
                sirenFiltres.add(item);
            }
        }
        return sirenFiltres;
    }

}
