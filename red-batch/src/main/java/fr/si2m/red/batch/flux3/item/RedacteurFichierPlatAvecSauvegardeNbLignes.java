package fr.si2m.red.batch.flux3.item;

import java.util.Collection;
import java.util.List;

import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.annotation.BeforeStep;
import org.springframework.batch.item.ExecutionContext;

import fr.si2m.red.batch.moteur.item.RedacteurFichierPlatSansConversion;

/**
 * Rédacteur de corps de fichier à exporter avec trace de nombre de lignes déjà écrites.
 * 
 * @author nortaina
 *
 * @param <T>
 *            le type de l'entité à éditer dans le fichier rédigé
 */
public abstract class RedacteurFichierPlatAvecSauvegardeNbLignes<T> extends RedacteurFichierPlatSansConversion<T> {

    private StepExecution stepExecution;

    /**
     * Constructeur par défaut pour le rédacteur du fichier.
     * 
     * @param nomsProprietesExtraites
     *            les noms des propriétés extraites telles quelles dans l'entité
     * @param format
     *            le format d'écriture des propriétés extraites
     */
    protected RedacteurFichierPlatAvecSauvegardeNbLignes(String[] nomsProprietesExtraites, String format) {
        super(nomsProprietesExtraites, format);
    }

    /**
     * Met à jour le contexte d'exécution de l'étape.
     * 
     * @param stepExecution
     *            le contexte d'exécution de l'étape
     */
    @BeforeStep
    public void saveStepExecution(StepExecution stepExecution) {
        this.stepExecution = stepExecution;
    }

    @Override
    public void write(List<? extends T> items) throws Exception {
        ExecutionContext stepContext = this.stepExecution.getExecutionContext();
        int nb = stepContext.getInt(getCleContexteNbLignes(), 0);
        for (Object item : items) {
            if (item instanceof Collection<?>) {
                nb += ((Collection<?>) item).size();
            } else {
                nb += 1;
            }
        }
        stepContext.put(getCleContexteNbLignes(), nb);
        super.write(items);
    }

    /**
     * Récupère le nom de la clé pour récupérer le nombre de lignes déjà écrite dans le contexte d'exécution.
     * 
     * @return le nom de la clé
     */
    public abstract String getCleContexteNbLignes();
}
