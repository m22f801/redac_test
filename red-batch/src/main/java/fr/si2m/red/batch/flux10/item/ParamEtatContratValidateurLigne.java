package fr.si2m.red.batch.flux10.item;

import fr.si2m.red.batch.moteur.erreur.ValidateurLigneAvecCollecte;
import fr.si2m.red.parametrage.ParamEtatContrat;

/**
 * Validateur de ligne ParamEtatContrat.
 * 
 * @author poidij
 *
 */
public class ParamEtatContratValidateurLigne extends ValidateurLigneAvecCollecte<ParamEtatContrat> {

    @Override
    protected void valide(final ParamEtatContrat etatContrat) {
        // Champs obligatoires

        // Pas besoin de tester le champ Actif
        valideChampAvecCollecte(etatContrat.getEtatContrat() != null, etatContrat, "COETACO", "le champ COETACO obligatoire, n’est pas renseigné");

        // Longueur des champs

        // On vérifie que le nombre COETACO a au plus 2 chiffres
        if (etatContrat.getEtatContrat() != null) {
            valideChampAvecCollecte(etatContrat.getEtatContrat() < 100, etatContrat, "COETACO", "le champ COETACO dépasse la taille maximale prévue");
        }

        valideChampAvecCollecte(etatContrat.getActifAsText().length() <= 1, etatContrat, "ACTIF", "le champ ACTIF dépasse la taille maximale prévue");

    }

}
