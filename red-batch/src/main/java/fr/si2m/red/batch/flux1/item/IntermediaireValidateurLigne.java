package fr.si2m.red.batch.flux1.item;

import java.text.MessageFormat;

import org.apache.commons.lang3.StringUtils;

import fr.si2m.red.DateRedac;
import fr.si2m.red.batch.RedacMessages;
import fr.si2m.red.batch.moteur.erreur.ValidateurLigneAvecCollecte;
import fr.si2m.red.contrat.Intermediaire;

/**
 * Validateur de ligne {@link Intermediaire}.
 * 
 * @author delortjouvesf
 *
 */
public class IntermediaireValidateurLigne extends ValidateurLigneAvecCollecte<Intermediaire> {

    @Override
    protected void valide(Intermediaire intermediaire) {

        // F01_RG_CI02
        valideTailleMaximaleChampAvecCollecte(intermediaire.getTypeIntermediaire(), 2, intermediaire, "TYPROD");

        // F01_RG_CI03
        valideTailleMaximaleChampAvecCollecte(intermediaire.getNumCompteClient(), 6, intermediaire, "NCINTERM");

        // Champ obligatoire
        // F01_RG_CI04
        valideChampAvecCollecte(StringUtils.isNotBlank(intermediaire.getCodeIntermediaire()), intermediaire, RedacMessages.CHAMP_NCPROD,
                MessageFormat.format(RedacMessages.ERREUR_CHAMP_OBLIGATOIRE, RedacMessages.CHAMP_NCPROD));

        // F01_RG_CI04
        valideTailleMaximaleChampTexteAvecCollecte(intermediaire.getCodeIntermediaire(), 6, intermediaire, RedacMessages.CHAMP_NCPROD);

        // F01_RG_CI05
        valideTailleMaximaleChampTexteAvecCollecte(intermediaire.getDepartement(), 2, intermediaire, "NODPT");

        // F01_RG_CI06
        valideTailleMaximaleChampAvecCollecte(intermediaire.getCodeEtat(), 1, intermediaire, "COETA");

        // F01_RG_CI07
        valideTailleMaximaleChampTexteAvecCollecte(intermediaire.getRaisonSociale(), 38, intermediaire, "NMRSO");

        // F01_RG_CI08
        valideTailleMaximaleChampTexteAvecCollecte(intermediaire.getNomExploitant(), 38, intermediaire, "NMEXP");

        // F01_RG_CI09
        valideTailleMaximaleChampTexteAvecCollecte(intermediaire.getAdresseBatiment(), 38, intermediaire, "ADBAT");

        // F01_RG_CI10
        valideTailleMaximaleChampTexteAvecCollecte(intermediaire.getAdresseRue(), 38, intermediaire, "ADRUE");

        // F01_RG_CI11
        valideTailleMaximaleChampTexteAvecCollecte(intermediaire.getAdresseCommune(), 38, intermediaire, "ADCOM");

        // F01_RG_CI12
        valideTailleMaximaleChampTexteAvecCollecte(intermediaire.getAdresseCodePostal(), 5, intermediaire, "ADCP");

        // F01_RG_CI13
        valideTailleMaximaleChampTexteAvecCollecte(intermediaire.getAdresseVille(), 38, intermediaire, "ADVIL");

        // F01_RG_CI14
        valideTailleMaximaleChampTexteAvecCollecte(intermediaire.getCodePays(), 3, intermediaire, "COPAYS");

        // F01_RG_CI15
        valideTailleMaximaleChampTexteAvecCollecte(intermediaire.getNumSiren(), 9, intermediaire, "NOSIREN");

        // F01_RG_CI16
        valideTailleMaximaleChampTexteAvecCollecte(intermediaire.getCodeFiliale(), 2, intermediaire, "COFIR");

        // F01_RG_CI17
        valideTailleMaximaleChampTexteAvecCollecte(intermediaire.getTelephone(), 15, intermediaire, "NOTEL");

        // F01_RG_CI18
        valideTailleMaximaleChampTexteAvecCollecte(intermediaire.getFax(), 15, intermediaire, "NOTLC");

        // F01_RG_CI19
        valideTailleMaximaleChampTexteAvecCollecte(intermediaire.getReseauDistribution(), 2, intermediaire, "RESDIS");

        // F01_RG_CI20
        valideTailleMaximaleChampTexteAvecCollecte(intermediaire.getNumInspecteur(), 2, intermediaire, "INSPEC");

        // F01_RG_CI21
        intermediaire.setDateDerniereModification(valideChampDateAvecCollecte(intermediaire.getDateDerniereModification(), DateRedac.FORMAT_DATES,
                intermediaire, "DT_DERN_MODIF", false));

        // Ajustement de l'entité si validation OK
        if (verifieDateVide(intermediaire.getDateDerniereModification())) {
            intermediaire.setDateDerniereModification(null);
        }
    }
}
