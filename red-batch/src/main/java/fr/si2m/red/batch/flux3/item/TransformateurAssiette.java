package fr.si2m.red.batch.flux3.item;

import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import com.thoughtworks.xstream.core.util.Base64Encoder;

import fr.si2m.red.DateRedac;
import fr.si2m.red.batch.RedacMessages;
import fr.si2m.red.batch.flux3.ligne.Assiette;
import fr.si2m.red.batch.flux3.ligne.SituationTarifNoco;
import fr.si2m.red.batch.moteur.item.TransformateurDonnee;
import fr.si2m.red.contrat.SituationTarif;
import fr.si2m.red.contrat.SituationTarifRepository;
import fr.si2m.red.parametrage.ParamNatureBase;
import fr.si2m.red.parametrage.ParamNatureBaseComposants;
import fr.si2m.red.parametrage.ParamNatureBaseComposantsRepository;
import fr.si2m.red.parametrage.ParamNatureBaseRepository;
import lombok.Setter;

/**
 * Mapper des entités Assiette à partir de la SituationContrat fournie.
 * 
 * cf F03_RG_S14 - Note : les situations font ici références à la notion de "plage tarifaire" mentionnée dans la SFD
 * 
 * @author poidij , eudesr
 *
 */
public class TransformateurAssiette extends TransformateurDonnee<SituationTarifNoco, List<Assiette>> {

    @Setter
    private ParamNatureBaseRepository paramNatureBaseRepository;

    @Setter
    private ParamNatureBaseComposantsRepository paramNatureBaseComposantsRepository;

    @Setter
    private SituationTarifRepository paramSituationTarifRepository;

    @Setter
    private JpaSituationContratTempRepository paramSituationContratRepository;

    @Override
    public List<Assiette> process(SituationTarifNoco situationTarifNocoNocat) throws Exception {

        List<SituationTarif> situationsTarif = paramSituationTarifRepository.getSituationTarifsPourNoco(situationTarifNocoNocat.getNumContrat());

        // on calcule les dates de debut et fin des situations , pour le contrat
        Integer[] dateDebutSituationSelectionne = new Integer[situationsTarif.size()];
        Integer[] dateFinSituationSelectionne = new Integer[situationsTarif.size()];

        calculDateDebutFinSituation(situationsTarif, dateDebutSituationSelectionne, dateFinSituationSelectionne);

        List<Assiette> resultatAssiettes = new ArrayList<>();

        for (int j = 0; j < situationsTarif.size(); j++) {
            SituationTarif situationTarif = situationsTarif.get(j);
            Integer natureBaseCotisations = situationTarif.getNatureBaseCotisations();
            String tauxCalculRempli = "N";

            if (situationTarif.getTauxCalculCotisationsUniques() != null && situationTarif.getTauxCalculCotisationsUniques() > 0) {
                tauxCalculRempli = "O";
            }

            resultatAssiettes.addAll(remplissageListeAssiette(natureBaseCotisations, tauxCalculRempli, situationTarif,
                    dateDebutSituationSelectionne[j], dateFinSituationSelectionne[j]));
        }

        return resultatAssiettes;

    }

    /**
     * Retourne la concaténation dateDebutTarif/dateDebutContrat identifiant une situation
     * 
     * @param dateDebutTarif
     *            la date de début de validité du tarif
     * @param dateDebutContrat
     *            la date de début de validité du contrat
     * @return la concaténation dateDebutTarif+dateDebutContrat
     */
    private String calculIdentifiantSituation(Integer dateDebutTarif, Integer dateDebutContrat) {
        return (dateDebutTarif == null ? StringUtils.EMPTY : dateDebutTarif.toString())
                + (dateDebutContrat == null ? StringUtils.EMPTY : dateDebutContrat.toString());
    }

    /**
     * Extrait la liste des conbcot/nocat d'une Situation Tarifaire
     * 
     * @param situations
     *            la liste des situation composant la Situation Tarifaire
     * @return la liste des conbcot+nocat concaténés présent dans la situation
     */
    private List<String> transformeListeSituationsConbcotNocat(List<SituationTarif> situations) {
        List<String> extract = new ArrayList<>();
        for (SituationTarif situation : situations) {
            extract.add(situation.getNatureBaseCotisations().toString() + situation.getNumCategorie());
        }
        return extract;
    }

    /**
     * Retourne la date de fin d'une situation la plus proche temporellement
     * 
     * @param situationACloturer
     *            la situationTarif
     * @return la date de fin , minimum entre la date de fin du tarif, et celle du contrat
     */
    private Integer determineDateFin(SituationTarif situationACloturer) {
        Integer dateFin = null;
        if (situationACloturer.getDateFinSituationLigne() != null && situationACloturer.getDateFinSituationContrat() != null) {
            dateFin = Math.min(situationACloturer.getDateFinSituationContrat(), situationACloturer.getDateFinSituationLigne());
        } else if (situationACloturer.getDateFinSituationLigne() == null) {
            dateFin = situationACloturer.getDateFinSituationContrat();
        } else if (situationACloturer.getDateFinSituationContrat() == null) {
            dateFin = situationACloturer.getDateFinSituationLigne();
        }

        return dateFin;
    }

    /**
     * Cloture la situation
     * 
     * @param situations
     *            liste de plage d'index représentant les situations
     * @param situationsTarif
     *            la liste des situations tarifaires , pour le noco donné
     * @param dateFinSituationSelectionne
     *            le tableau des dateFin situation ( indice i = dateFin situation (i) )
     * @param mapconbcotDateDebut
     *            Map(k,v) qui associe à un conbcot et un nocat (k) la dernière Date de Début de situation choisie (v)
     */
    private void clotureSituation(List<int[]> situations, List<SituationTarif> situationsTarif, Integer[] dateFinSituationSelectionne,
            Map<String, ValeurMapSituationPopulation> mapconbcotDateDebut) {
        // On veut récupérer la sousListe "fromIndex" "toIndex" inclus (cf JavaDoc SubList )
        List<String> anciennesPopulation = transformeListeSituationsConbcotNocat(
                situationsTarif.subList(situations.get(0)[0], situations.get(0)[1] + 1));
        List<String> nouvellesPopulation = transformeListeSituationsConbcotNocat(
                situationsTarif.subList(situations.get(1)[0], situations.get(1)[1] + 1));

        for (int k = 0; k < anciennesPopulation.size(); k++) {
            // une population n'est plus présente, on doit la cloturer
            if (!nouvellesPopulation.contains(anciennesPopulation.get(k))) {

                // on détermine l'index de la situation à cloturer
                int indexSituationACloturer = situations.get(0)[0] + k;
                SituationTarif derniereSituation = situationsTarif.get(indexSituationACloturer);
                dateFinSituationSelectionne[indexSituationACloturer] = determineDateFin(derniereSituation);

                // on efface la population de la map
                mapconbcotDateDebut.remove(anciennesPopulation.get(k));
            }
        }

        // on efface la première situation tarifaire
        situations.remove(0);
    }

    /**
     * Calcul des dates Debut et Fin de situation
     * 
     * @param situationsTarif
     *            la liste des situations tarifaires , pour le noco donné
     * @param dateDebutSituationSelectionne
     *            le tableau des dateDebut situation ( indice i = dateDebut situation (i) )
     * @param dateFinSituationSelectionne
     *            le tableau des dateFin situation ( indice i = dateFin situation (i) )
     */
    private void calculDateDebutFinSituation(List<SituationTarif> situationsTarif, Integer[] dateDebutSituationSelectionne,
            Integer[] dateFinSituationSelectionne) {
        /**
         * Map(k,v) qui associe à un conbcot et un nocat (k) la dernière Date de Début de situation choisie (v)
         */
        Map<String, ValeurMapSituationPopulation> mapconbcotDateDebut = new HashMap<>();

        /**
         * Indice représentant la dernière nouvelle situation Tarifaire, que l'on identifie par le couple DateDebutSituationContrat()/DateDebutSituationLigne()
         */
        int precedentChangementSituation = 0;

        /**
         * identifiant d'une situation , concaténation de couple DateDebutSituationContrat()/DateDebutSituationLigne()
         */
        String identifiantSituation = StringUtils.EMPTY;

        /**
         * Liste représentant au plus 2 situations contigüe, avec l'indice de début/fin pour chacune.
         */
        List<int[]> situations = new ArrayList<>();

        for (int i = 0; i < situationsTarif.size(); i++) {
            SituationTarif situationTarifCourante = situationsTarif.get(i);

            /**
             * On identifie une Situation par la concaténation dateDebutTarif et dateDebutContrat de la ligne situationsTarif . Une situation identifiée peut
             * donc corespondre à plusieurs plusieurs objets situationsTarif. Les différentes lignes d'une même Situation sont consécutives.
             */
            String ancienIdentifiant = identifiantSituation;
            identifiantSituation = calculIdentifiantSituation(situationTarifCourante.getDateDebutSituationLigne(),
                    situationTarifCourante.getDateDebutSituationContrat());

            /**
             * TRAITEMENTS - NOUVELLE SITUATION
             */
            if (!ancienIdentifiant.equals(identifiantSituation) && i > 0) {
                // On sauvegarde le range contenant la situation précédente
                situations.add(new int[] { precedentChangementSituation, Math.max(precedentChangementSituation, i - 1) });
                precedentChangementSituation = i;
            }

            // On a 2 situations , on vérifie si une population créé à la première situation n'a pas été cloturée
            // (càd absente) lors de la seconde situation
            if (situations.size() > 1) {
                clotureSituation(situations, situationsTarif, dateFinSituationSelectionne, mapconbcotDateDebut);
            }

            /**
             * TRAITEMENTS POUR UNE SITUATION
             */

            // on test si on connait la population (déjà présente dans la précédente Situation)
            if (mapconbcotDateDebut
                    .get(situationTarifCourante.getNatureBaseCotisations().toString() + situationTarifCourante.getNumCategorie()) == null) {
                // nouvelle population, on choisit la plus grande date ( un contrat est toujours couvert par un tarif )
                dateDebutSituationSelectionne[i] = Math.max(situationTarifCourante.getDateDebutSituationContrat().intValue(),
                        situationTarifCourante.getDateDebutSituationLigne().intValue());

            } else {
                if (situationTarifCourante.getDateDebutSituationContrat()
                        .intValue() > mapconbcotDateDebut.get(
                                situationTarifCourante.getNatureBaseCotisations().toString() + situationTarifCourante.getNumCategorie()).dateDebut
                                        .intValue()
                        && situationTarifCourante.getDateDebutSituationLigne().intValue() > mapconbcotDateDebut.get(
                                situationTarifCourante.getNatureBaseCotisations().toString() + situationTarifCourante.getNumCategorie()).dateDebut
                                        .intValue()) {
                    if (situationTarifCourante.getDateDebutSituationContrat().intValue() <= situationTarifCourante.getDateDebutSituationLigne()
                            .intValue()) {
                        dateDebutSituationSelectionne[i] = situationTarifCourante.getDateDebutSituationLigne();
                    } else {
                        dateDebutSituationSelectionne[i] = situationTarifCourante.getDateDebutSituationContrat();
                    }

                } else if (situationTarifCourante.getDateDebutSituationContrat()
                        .intValue() > mapconbcotDateDebut.get(
                                situationTarifCourante.getNatureBaseCotisations().toString() + situationTarifCourante.getNumCategorie()).dateDebut
                                        .intValue()
                        && situationTarifCourante.getDateDebutSituationLigne().intValue() <= mapconbcotDateDebut.get(
                                situationTarifCourante.getNatureBaseCotisations().toString() + situationTarifCourante.getNumCategorie()).dateDebut
                                        .intValue()) {
                    dateDebutSituationSelectionne[i] = situationTarifCourante.getDateDebutSituationContrat();

                } else if (situationTarifCourante.getDateDebutSituationContrat()
                        .intValue() <= mapconbcotDateDebut.get(
                                situationTarifCourante.getNatureBaseCotisations().toString() + situationTarifCourante.getNumCategorie()).dateDebut
                                        .intValue()
                        && situationTarifCourante.getDateDebutSituationLigne().intValue() > mapconbcotDateDebut.get(
                                situationTarifCourante.getNatureBaseCotisations().toString() + situationTarifCourante.getNumCategorie()).dateDebut
                                        .intValue()) {
                    dateDebutSituationSelectionne[i] = situationTarifCourante.getDateDebutSituationLigne();

                }

                // calcul date de fin pour la population de la situation précédente
                dateFinSituationSelectionne[mapconbcotDateDebut.get(situationTarifCourante.getNatureBaseCotisations().toString()
                        + situationTarifCourante.getNumCategorie()).indiceTableau] = DateRedac.retirerALaDate(dateDebutSituationSelectionne[i], 1,
                                DateRedac.MODIFIER_JOUR, "DateFinTauxAppel");
            }

            // on sauvegarde la date choisie pour le conbcot et le nocat, ainsi que l'indice de la situation dans situationsTarif
            mapconbcotDateDebut.put(situationTarifCourante.getNatureBaseCotisations().toString() + situationTarifCourante.getNumCategorie(),
                    new ValeurMapSituationPopulation(dateDebutSituationSelectionne[i], i));
        }

        /**
         * TRAITEMENTS DERNIERES SITUATIONS
         */

        // cloture sur precedente situation si besoin
        for (int i = 0; i < precedentChangementSituation; i++) {
            if (dateFinSituationSelectionne[i] == null) {
                SituationTarif situationACloturer = situationsTarif.get(i);
                dateFinSituationSelectionne[i] = determineDateFin(situationACloturer);
            }
        }

        // gestion de la date de fin pour la dernière Situation
        Integer dateFinDerniereSituation = null;

        for (int i = precedentChangementSituation; i < situationsTarif.size(); i++) {
            SituationTarif situationACloturer = situationsTarif.get(i);
            // la date de fin sera la même pour toute les lignes suivantes (car on est dans la même dernière Situation )
            if (dateFinDerniereSituation == null) {
                dateFinDerniereSituation = paramSituationContratRepository.getDateFinDerniereSituation(situationACloturer.getNumContrat());
            }
            dateFinSituationSelectionne[i] = dateFinDerniereSituation;
        }

    }

    private Long setTauxBase(String tauxUtilise, SituationTarif situationTarif) {
        Long tauxBase = 0L;
        if (StringUtils.isNotBlank(tauxUtilise)) {
            switch (tauxUtilise) {
            case RedacMessages.CHAMP_TXCALCU:
                tauxBase = situationTarif.getTauxCalculCotisationsUniques();
                break;
            case RedacMessages.CHAMP_TXBASE1:
                tauxBase = situationTarif.getTauxBase1();
                break;
            case RedacMessages.CHAMP_TXBASE2:
                tauxBase = situationTarif.getTauxBase2();
                break;
            case RedacMessages.CHAMP_TXBASE3:
                tauxBase = situationTarif.getTauxBase3();
                break;
            case RedacMessages.CHAMP_TXBASE4:
                tauxBase = situationTarif.getTauxBase4();
                break;
            default:
            }
        }
        return tauxBase;

    }

    private List<Assiette> remplissageListeAssiette(Integer natureBaseCotisations, String tauxCalculRempli, SituationTarif situationTarif,
            Integer dateDebutSituationSelectionne, Integer dateFinSituationSelectionne) throws Exception {
        List<Assiette> assiettes = new ArrayList<Assiette>();

        List<ParamNatureBaseComposants> paramNatureBaseComposants = paramNatureBaseComposantsRepository
                .getParamNatureBaseComposants(natureBaseCotisations, tauxCalculRempli);

        for (ParamNatureBaseComposants paramNatureBaseComposant : paramNatureBaseComposants) {
            ParamNatureBase paramNatureBase = paramNatureBaseRepository.getParamNatureBase(natureBaseCotisations,
                    paramNatureBaseComposant.getTauxCalculRempliAsText(), paramNatureBaseComposant.getNumTranche());
            if (paramNatureBase != null) {
                Assiette assiette = new Assiette();
                assiette.setCodeMiseAJour("R");

                Long tauxBase = setTauxBase(paramNatureBase.getTauxUtilise(), situationTarif);

                assiette.setTauxBaseStandardOuSpecifique(Math.round(tauxBase * situationTarif.getTauxAppel() / 10000));

                if (RedacMessages.CHAMP_MTCUNCU.equals(paramNatureBase.getMontantUtilise())
                        && situationTarif.getMontantCotisationsUniques() != null) {
                    assiette.setMontantBaseSpecifique(
                            Math.round(situationTarif.getMontantCotisationsUniques() * situationTarif.getTauxAppel() / 120000));
                } else {
                    assiette.setMontantBaseSpecifique(Long.valueOf(0));
                }

                // Taux d'appel et dates d'application
                gestionTauxAppelEtDatesApplication(situationTarif, assiette, dateDebutSituationSelectionne, dateFinSituationSelectionne);

                if (paramNatureBaseComposant.getNumTypeComposantBase() != null) {
                    assiette.setTypeBase(paramNatureBaseComposant.getNumTypeComposantBase());
                } else {
                    assiette.setTypeBase(0);
                }

                assiette.setLibelleBaseSpecifique(paramNatureBase.getComplementDesignation());
                assiette.setIdentifiantTechniqueContrat(situationTarif.getNumContrat());

                // note : un hash SHA-1 en Base64 a une longueur 28
                MessageDigest md = MessageDigest.getInstance("SHA-1");
                String numConcat = situationTarif.getNumContrat() + situationTarif.getNumCategorie();
                byte[] bytes = numConcat.getBytes("UTF-8");
                md.update(bytes);
                byte[] digest = md.digest();
                String hash = (new Base64Encoder()).encode(digest);

                assiette.setIdentifiantTechniquePopulation(hash);

                assiettes.add(assiette);
            }
        }

        return assiettes;
    }

    /**
     * Renseigne le taux d'appel et, s'il a changé, sa date de changement.
     * 
     * @param situationTarif
     *            la situation tarifaire à traiter
     * @param assiette
     *            la ligne d'assiette en cours de création
     * @param dateFinSituationSelectionne
     * @param dateDebutSituationSelectionne
     * @param tarif
     *            la ligne de tarif de la situation tarifaire
     */
    private void gestionTauxAppelEtDatesApplication(SituationTarif situationTarif, Assiette assiette, Integer dateDebutSituationSelectionne,
            Integer dateFinSituationSelectionne) {

        // Taux d'appel
        Double tauxAppel = situationTarif.getTauxAppel() == null ? Double.valueOf(0d) : situationTarif.getTauxAppel();
        situationTarif.setTauxAppel(tauxAppel);

        assiette.setDateDebutApplication(dateDebutSituationSelectionne);

        assiette.setDateFinApplication(dateFinSituationSelectionne);

    }

}
