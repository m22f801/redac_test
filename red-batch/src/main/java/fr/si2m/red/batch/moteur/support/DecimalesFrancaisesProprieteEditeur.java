package fr.si2m.red.batch.moteur.support;

import java.text.DecimalFormat;

import org.springframework.beans.propertyeditors.CustomNumberEditor;

/**
 * Editeur de propriétés décimales au format français dans les fichiers batch.
 * 
 * @author nortaina
 *
 */
public class DecimalesFrancaisesProprieteEditeur extends CustomNumberEditor {

    /**
     * Constructeur par défaut.
     */
    public DecimalesFrancaisesProprieteEditeur() {
        super(Double.class, new DecimalFormat("0,00"), true);
    }
}
