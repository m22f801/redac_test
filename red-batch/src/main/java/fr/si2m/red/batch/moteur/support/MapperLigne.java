package fr.si2m.red.batch.moteur.support;

import java.lang.reflect.Field;

import javax.persistence.Column;

import org.apache.commons.lang3.reflect.FieldUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.transform.IncorrectTokenCountException;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;

import fr.si2m.red.EntiteImportableBatch;
import lombok.Setter;

/**
 * Mapper de ligne utilisé par les composants batchs du moteur REDAC.
 * 
 * @author nortaina
 *
 * @param <T>
 *            le type de ligne mappée
 */
public class MapperLigne<T extends EntiteImportableBatch> extends DefaultLineMapper<T> {
    private static final Logger LOGGER = LoggerFactory.getLogger(MapperLigne.class);

    /**
     * L'usine de bean.
     * 
     * @param beanFactory
     *            l'usine de bean
     */
    @Setter
    private BeanFactory beanFactory;
    /**
     * Le nom du prototype de ligne mappée.
     * 
     * @param nomPrototypeLigne
     *            le nom du prototype de ligne mappée
     */
    @Setter
    private String nomPrototypeLigne;
    /**
     * Le nom du fichier en cours de traitement.
     * 
     * @param nomFichierTraite
     *            le nom du fichier en cours de traitement
     */
    @Setter
    private String nomFichierTraite;
    /**
     * Le nom du batch important la donnée.
     * 
     * @param nomBatch
     *            le nom du batch important la donnée
     */
    @Setter
    private String nomBatch;

    @Override
    public T mapLine(String line, int lineNumber) throws Exception {
        try {
            T obj = super.mapLine(line, lineNumber);
            obj.setNomFichierImport(nomFichierTraite);
            obj.setLigneImport(lineNumber);
            obj.setLigneEnCoursImportBatch(true);
            obj.setAuditUtilisateurCreation(nomBatch);
            return obj;
        } catch (Exception e) {
            LOGGER.error("Impossible de mapper la ligne " + lineNumber);
            return createEntiteInvalide(e, lineNumber);
        }
    }

    private T createEntiteInvalide(Exception e, int lineNumber) {
        @SuppressWarnings("unchecked")
        T entiteInvalide = (T) beanFactory.getBean(nomPrototypeLigne);
        entiteInvalide.setNomFichierImport(nomFichierTraite);
        entiteInvalide.setAuditUtilisateurCreation(nomBatch);
        entiteInvalide.setLigneImport(lineNumber);
        entiteInvalide.setLigneSyntaxiquementValide(false);
        entiteInvalide.setLigneEnCoursImportBatch(true);
        if (e instanceof IncorrectTokenCountException) {
            entiteInvalide.enregistreErreurSurImport("Le nombre de colonnes du fichier est différent du nombre de colonnes attendu");
        } else if (e instanceof BindException) {
            BindingResult bindingResult = ((BindException) e).getBindingResult();
            Class<?> classeEntite = entiteInvalide.getClass();
            for (FieldError erreurChamp : bindingResult.getFieldErrors()) {
                Field proprieteEnErreur = FieldUtils.getField(classeEntite, erreurChamp.getField(), true);
                Column colonne = proprieteEnErreur.getAnnotation(Column.class);
                String nomPropriete = colonne != null ? colonne.name() : erreurChamp.getField();
                String messageErreur = "Le format du champ " + nomPropriete + " est incorrect";
                entiteInvalide.enregistreErreurSurImport(messageErreur);
            }
        } else {
            entiteInvalide.enregistreErreurSurImport("La ligne n'a pas pu être lue correctement");
        }
        return entiteInvalide;
    }
}
