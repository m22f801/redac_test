package fr.si2m.red.batch.moteur.erreur;

import org.apache.commons.lang3.BooleanUtils;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.job.flow.FlowExecutionStatus;
import org.springframework.batch.core.job.flow.JobExecutionDecider;

/**
 * Décideur de statut selon l'observation d'erreurs collectées ou non dans le contexte d'exécution.
 * 
 * @author nortaina
 *
 */
public class ErreursCollecteesDecideur implements JobExecutionDecider {

    @Override
    public FlowExecutionStatus decide(JobExecution jobExecution, StepExecution stepExecution) {
        boolean erreursDetecteesStep = BooleanUtils
                .isTrue((Boolean) stepExecution.getExecutionContext().get(ListenerCollecteErreursValidation.CLE_DETECTION_ERREURS));
        boolean erreursDetecteesJob = BooleanUtils
                .isTrue((Boolean) jobExecution.getExecutionContext().get(ListenerCollecteErreursValidation.CLE_DETECTION_ERREURS));
        return new FlowExecutionStatus(erreursDetecteesStep || erreursDetecteesJob ? "KO" : "OK");
    }
}
