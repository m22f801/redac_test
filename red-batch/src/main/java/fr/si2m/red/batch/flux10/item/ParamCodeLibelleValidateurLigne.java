package fr.si2m.red.batch.flux10.item;

import org.apache.commons.lang3.StringUtils;

import fr.si2m.red.batch.moteur.erreur.ValidateurLigneAvecCollecte;
import fr.si2m.red.parametrage.ParamCodeLibelle;

/**
 * Validateur de ligne de paramétrage code libellé.
 * 
 * @author poidij
 *
 */
public class ParamCodeLibelleValidateurLigne extends ValidateurLigneAvecCollecte<ParamCodeLibelle> {

    @Override
    protected void valide(final ParamCodeLibelle codeLibelle) {
        // Champs obligatoires

        valideChampAvecCollecte(StringUtils.isNotBlank(codeLibelle.getTable()), codeLibelle, "TABLE",
                "le champ TABLE obligatoire, n’est pas renseigné");

        valideChampAvecCollecte(StringUtils.isNotBlank(codeLibelle.getChamp()), codeLibelle, "CHAMP",
                "le champ CHAMP obligatoire, n’est pas renseigné");

        valideChampAvecCollecte(StringUtils.isNotBlank(codeLibelle.getCode()), codeLibelle, "CODE", "le champ CODE obligatoire, n’est pas renseigné");

        // Longueur des champs

        if (StringUtils.isNotBlank(codeLibelle.getTable())) {
            valideChampAvecCollecte(codeLibelle.getTable().length() <= 50, codeLibelle, "TABLE", "le champ TABLE dépasse la taille maximale prévue");
        }

        if (StringUtils.isNotBlank(codeLibelle.getChamp())) {
            valideChampAvecCollecte(codeLibelle.getChamp().length() <= 50, codeLibelle, "CHAMP", "le champ CHAMP dépasse la taille maximale prévue");
        }

        if (StringUtils.isNotBlank(codeLibelle.getCode())) {
            valideChampAvecCollecte(codeLibelle.getCode().length() <= 50, codeLibelle, "CODE", "le champ CODE dépasse la taille maximale prévue");
        }

        // on vérifie que le nombre Ordre a au plus 10 chiffres
        if (codeLibelle.getOrdre() != null) {
            valideChampAvecCollecte(codeLibelle.getOrdre() < 10000000000L, codeLibelle, "ORDRE", "le champ ORDRE dépasse la taille maximale prévue");
        }

        valideChampAvecCollecte(codeLibelle.getLibelleCourt().length() <= 20, codeLibelle, "LIBELLE_COURT",
                "le champ LIBELLE_COURT dépasse la taille maximale prévue");

        valideChampAvecCollecte(codeLibelle.getLibelleLong().length() <= 60, codeLibelle, "LIBELLE_LONG",
                "le champ LIBELLE_LONG dépasse la taille maximale prévue");

        valideChampAvecCollecte(codeLibelle.getLibelleEdition().length() <= 40, codeLibelle, "LIBELLE_EDITION",
                "le champ LIBELLE_EDITION dépasse la taille maximale prévue");

    }

}
