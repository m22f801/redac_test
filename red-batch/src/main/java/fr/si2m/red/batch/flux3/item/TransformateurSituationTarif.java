package fr.si2m.red.batch.flux3.item;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import fr.si2m.red.batch.flux3.ligne.SituationContrat;
import fr.si2m.red.batch.moteur.item.TransformateurDonnee;
import fr.si2m.red.contrat.SituationTarif;
import fr.si2m.red.contrat.Tarif;
import fr.si2m.red.contrat.TarifRepository;
import lombok.Setter;

/**
 * Mapper des entités Population à partir de la SituationContrat fournie.
 * 
 * cf F03_RG_S12
 * 
 * @author poidij
 *
 */
public class TransformateurSituationTarif extends TransformateurDonnee<SituationContrat, List<SituationTarif>> {

    @Setter
    private TarifRepository tarifRepository;

    @Override
    public List<SituationTarif> process(SituationContrat situationContrat) throws Exception {
        Set<SituationTarif> situationsTarifs = new HashSet<SituationTarif>();
        // S12 et S14, 1er point
        List<Tarif> tarifs = tarifRepository.getSituationsTarifsDesPopulations(situationContrat.getNumContrat(),
                situationContrat.getDateDebutSituation(), situationContrat.getDateFinSituation());

        for (Tarif tarif : tarifs) {
            SituationTarif situationTarif = new SituationTarif();
            situationTarif.setNumContrat(tarif.getNumContrat());
            situationTarif.setNumCategorie(tarif.getNumCategorie());
            situationTarif.setDateDebutSituationLigne(tarif.getDateDebutSituationLigne());
            situationTarif.setDateDebutSituationContrat(situationContrat.getDateDebutSituation());
            situationTarif.setDateFinSituationContrat(situationContrat.getDateFinSituation());
            situationTarif.setTauxAppel(situationContrat.getTauxAppel());
            situationsTarifs.add(situationTarif);
        }
        return new ArrayList<SituationTarif>(situationsTarifs);
    }
}
