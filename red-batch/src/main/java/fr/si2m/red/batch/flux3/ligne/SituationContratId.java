package fr.si2m.red.batch.flux3.ligne;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * Identifiant de situation de contrat.
 * 
 * @author nortaina
 *
 */
@Data
@EqualsAndHashCode(callSuper = false, of = { "numContrat", "dateDebutSituation" })
@ToString(callSuper = false, of = { "numContrat", "dateDebutSituation" })
@NoArgsConstructor
@AllArgsConstructor
public class SituationContratId implements Serializable {
    /**
     * UID de version.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Numéro de contrat à afficher.
     * 
     * @param numContrat
     *            Numéro de contrat à afficher
     * @return Numéro de contrat à afficher
     */
    private String numContrat;

    /**
     * Date de début de validité des paramètres contrat.
     * 
     * @param dateDebutSituation
     *            Date de début de validité des paramètres contrat
     * @return Date de début de validité des paramètres contrat
     */
    private Integer dateDebutSituation;
}
