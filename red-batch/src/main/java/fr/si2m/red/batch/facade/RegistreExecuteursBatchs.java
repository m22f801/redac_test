package fr.si2m.red.batch.facade;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.ClassPathScanningCandidateComponentProvider;
import org.springframework.core.type.filter.AssignableTypeFilter;

import fr.si2m.red.RedacUnexpectedException;
import fr.si2m.red.batch.moteur.execution.ExecuteurBatch;
import fr.si2m.red.batch.moteur.execution.FacadeExecuteurBatch;

/**
 * Registre des exécuteurs de batchs connus de la façade.<br/>
 * <br/>
 * Les exécuteurs connus doivent à la fois être dans un sous-package de fr.si2m.red.batch et être annoté par {@link FacadeExecuteurBatch}.
 * 
 * @author nortaina
 *
 * @see ExecuteurBatch
 * @see FacadeExecuteurBatch
 */
@SuppressWarnings("unchecked")
public final class RegistreExecuteursBatchs {
    private static final Logger LOGGER = LoggerFactory.getLogger(RegistreExecuteursBatchs.class);

    /**
     * Registre des exécuteurs spécifiques aux batchs du registre.
     */
    private static final Map<String, Class<? extends ExecuteurBatch>> EXECUTEURS_PAR_BATCH = new HashMap<String, Class<? extends ExecuteurBatch>>();
    static {
        LOGGER.info("Scan des exécuteurs batchs");
        ClassPathScanningCandidateComponentProvider scanner = new ClassPathScanningCandidateComponentProvider(false);
        scanner.addIncludeFilter(new AssignableTypeFilter(ExecuteurBatch.class));
        for (BeanDefinition executeurBatchDefinition : scanner.findCandidateComponents("fr.si2m.red.batch")) {
            Class<? extends ExecuteurBatch> executeurBatchClass;
            try {
                executeurBatchClass = (Class<? extends ExecuteurBatch>) Class.forName(executeurBatchDefinition.getBeanClassName());
            } catch (ClassNotFoundException e) {
                throw new RedacUnexpectedException(e);
            }
            FacadeExecuteurBatch parametrageFacade = executeurBatchClass.getAnnotation(FacadeExecuteurBatch.class);
            if (parametrageFacade != null) {
                // Executeur exposé
                String nomBatch = parametrageFacade.nomBatch();
                LOGGER.debug("Scan d'un exécuteur pour le batch [{}] : [{}]", nomBatch, executeurBatchClass.getName());
                EXECUTEURS_PAR_BATCH.put(nomBatch, executeurBatchClass);
            }
        }
        LOGGER.info("{} exécuteurs batchs enregistrés par le moteur", EXECUTEURS_PAR_BATCH.size());
    }

    /**
     * Utilitaire.
     */
    private RegistreExecuteursBatchs() {
    }

    /**
     * Récupère l'exécuteur d'un batch donné avec un certain paramétrage.
     * 
     * @param nomBatch
     *            le nom du batch à exécuter
     * @param parametrageBatch
     *            le paramétrage du batch
     * @return l'exécuteur du batch avec le paramétrage donné
     */
    public static ExecuteurBatch getExecuteurPourBatch(String nomBatch, String[] parametrageBatch) {
        Class<? extends ExecuteurBatch> executeurBatchClass = EXECUTEURS_PAR_BATCH.get(nomBatch);
        if (executeurBatchClass == null) {
            throw new IllegalArgumentException("Le batch " + nomBatch + " n'est pas référencé par le registre de la façade");
        }
        try {
            Constructor<? extends ExecuteurBatch> constructeurExecuteurBatch = executeurBatchClass.getConstructor(String[].class);
            return constructeurExecuteurBatch.newInstance((Object) parametrageBatch);
        } catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException
                | SecurityException e) {
            throw new IllegalStateException("Impossible d'instancier l'exécuteur du batch " + nomBatch + " déclaré dans le registre", e);
        }
    }

    /**
     * Récupère l'ensemble de batchs exécutables par la façade.
     * 
     * @return l'ensemble de batchs exécutables par la façade
     */
    public static Set<String> getBatchsExecutables() {
        return EXECUTEURS_PAR_BATCH.keySet();
    }
}
