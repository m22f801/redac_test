package fr.si2m.red.batch.flux78.item;

import java.text.MessageFormat;
import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.annotation.AfterStep;

import fr.si2m.red.DateRedac;
import fr.si2m.red.EntiteImportableBatch;
import fr.si2m.red.batch.RedacMessages;
import fr.si2m.red.batch.moteur.CodeRetour;
import fr.si2m.red.batch.moteur.erreur.ValidateurLigneAvecCollecte;
import lombok.Setter;

/**
 * Controles de la ligne de synthèse.
 * 
 * @author benitahy
 *
 */
public class ControleLigneSyntheseProcesseur extends ValidateurLigneAvecCollecte<EntiteImportableBatch> {
    private static final Logger LOGGER = LoggerFactory.getLogger(ControleLigneSyntheseProcesseur.class);
    /**
     * Identifiant d'une ligne synthèse KO dans le contexte d'exécution.
     */
    public static final String LIGNE_SYNTHESE_KO = "LIGNE_SYNTHESE_KO";

    private static final String CHAMP_COMPTE_RENDU = "compte-rendu global";
    private static final String CHAMP_TYPE_FLUX = "type de flux";
    private static final String CHAMP_EMETTEUR_CR = "émetteur du CR";
    private static final String CHAMP_DATE_TRAITEMENT = "date de traitement";
    private static final List<Integer> emetteurCRValides = Arrays.asList(0, 1, 2);

    private Integer nbreEnregistrements2 = 0;
    private Integer nbreEnregistrements3 = 0;
    private int compteurEnregistrements1 = 0;
    private int compteurEnregistrements2 = 0;
    private int compteurEnregistrements3 = 0;
    private int numeroLigne = 0;

    private Synthese synthese = new Synthese();
    private boolean fichierBienTrie = true;

    @Setter
    private String fichierEntree;

    @Override
    public void valide(EntiteImportableBatch item) {

        if (item instanceof Synthese) {
            compteurEnregistrements1++;
            synthese = (Synthese) item;
            synthese.setNomFichierImport(fichierEntree);
            numeroLigne++;
            synthese.setLigneImport(numeroLigne);

            if (compteurEnregistrements1 > 1) {
                String messageErreur = "Le fichier contient plus d'une ligne synthèse.";
                synthese.setLigneImport(0);
                valideChampAvecCollecte(false, synthese, "NbreEnregistrements1", messageErreur);
            } else {
                controleLigneSynthese();
            }
        } else if (item instanceof ContratPeriode) {
            numeroLigne++;
            if (compteurEnregistrements1 == 0 && fichierBienTrie) {
                fichierBienTrie = false;
            }
            compteurEnregistrements2++;
        } else if (item instanceof Message || item instanceof MessageAgrandies) {
            numeroLigne++;
            if (compteurEnregistrements1 == 0 && fichierBienTrie) {
                fichierBienTrie = false;
            }
            compteurEnregistrements3++;

        } else if (item instanceof LigneAIgnorer) {
            numeroLigne++;
            item.setNomFichierImport(fichierEntree);
            item.setLigneImport(numeroLigne);
            valideChampAvecCollecteSansBlocage(false, (LigneAIgnorer) item, "typeEnregistrement", "La ligne a un type d'enregistrement erroné");
        }

    }

    /**
     * Contrôle de la ligne de synthèse passée en paramètre.
     * 
     */
    private void controleLigneSynthese() {

        // F78_RG_P4_06
        controleChampEmetteurCR();

        // F78_RG_P4_19 - F78_RG_P4_07
        controleChampDateTraitement();

        // F78_RG_P4_02
        controleChampTypeFlux();

        // F78_RG_P4_03
        controleChampCompteRenduGlobal();

        // F78_RG_P4_04
        controleChampNbEnregistrement2();

        // F78_RG_P4_05
        controleChampNbEnregistrement3();
    }

    /**
     * Contrôle la validité du champ date de traitement de la ligne de synthèse. cf F78_RG_P4_07
     */
    private void controleChampDateTraitement() {
        Integer dateTraitement = synthese.getDateTraitementAsInteger();
        Integer dateValide = null;
        if (dateTraitement == null) {
            String messageErreur = MessageFormat.format(RedacMessages.ERREUR_CHAMP_NON_NUMERIQUE, CHAMP_DATE_TRAITEMENT);
            valideChampAvecCollecte(false, synthese, CHAMP_DATE_TRAITEMENT, messageErreur);
        } else {
            dateValide = valideChampDateAvecCollecte(dateTraitement, DateRedac.FORMAT_DATES, synthese, CHAMP_DATE_TRAITEMENT, false);
        }

        // Si on respecte pas l'année minimale 1900
        // cf F78_RG_P4_24
        valideChampAvecCollecte(!(dateValide != null && dateValide <= 19000101), synthese, CHAMP_DATE_TRAITEMENT,
                MessageFormat.format(RedacMessages.ERREUR_CHAMP_FORMAT_INCORRECT, CHAMP_DATE_TRAITEMENT));
    }

    /**
     * Contrôle la validité du champ émetteur CR de la ligne de synthèse. cf F78_RG_P4_06
     */
    private void controleChampEmetteurCR() {
        Integer emetteurCR = synthese.getEmetteurCRAsInteger();

        if (emetteurCR == null) {
            String messageErreur = MessageFormat.format(RedacMessages.ERREUR_CHAMP_NON_NUMERIQUE, CHAMP_EMETTEUR_CR);
            valideChampAvecCollecte(false, synthese, CHAMP_EMETTEUR_CR, messageErreur);

        } else if (!emetteurCRValides.contains(emetteurCR)) {
            // F78_RG_P4_23
            String messageErreur = MessageFormat.format(RedacMessages.ERREUR_CHAMP_ERRONE_VALEUR_INATTENDUE, CHAMP_EMETTEUR_CR, "0 , 1 ou 2");
            valideChampAvecCollecte(false, synthese, CHAMP_EMETTEUR_CR, messageErreur);

            LOGGER.error(messageErreur);
        }

    }

    /**
     * Contrôle la validité du champ compte rendu global de la ligne de synthèse.
     * 
     */
    private void controleChampCompteRenduGlobal() {
        Integer compteRenduGlobal = synthese.getCompteRenduGlobalAsInteger();

        if (compteRenduGlobal == null) {
            String messageErreur = MessageFormat.format(RedacMessages.ERREUR_CHAMP_NON_NUMERIQUE, CHAMP_COMPTE_RENDU);
            valideChampAvecCollecte(false, synthese, CHAMP_COMPTE_RENDU, messageErreur);

        } else if (compteRenduGlobal != 0 && compteRenduGlobal != 1) {
            String messageErreur = MessageFormat.format(RedacMessages.ERREUR_CHAMP_ERRONE_VALEUR_INATTENDUE, CHAMP_COMPTE_RENDU, "0 ou 1");
            valideChampAvecCollecte(false, synthese, CHAMP_COMPTE_RENDU, messageErreur);
        }

    }

    /**
     * Contrôle la validité du champ type flux de la ligne de synthèse.
     * 
     */
    private void controleChampTypeFlux() {
        Integer typeFlux = synthese.getTypeFluxAsInteger();
        if (typeFlux == null) {
            String messageErreur = MessageFormat.format(RedacMessages.ERREUR_CHAMP_NON_NUMERIQUE, CHAMP_TYPE_FLUX);
            valideChampAvecCollecte(false, synthese, CHAMP_TYPE_FLUX, messageErreur);
        } else if (typeFlux != 1) {
            String messageErreur = MessageFormat.format(RedacMessages.ERREUR_CHAMP_ERRONE_VALEUR_INATTENDUE, CHAMP_TYPE_FLUX, "1");
            valideChampAvecCollecte(false, synthese, CHAMP_TYPE_FLUX, messageErreur);

            LOGGER.error(messageErreur);
        }

    }

    /**
     * Contrôle la validité du champ type flux de la ligne de synthèse.
     * 
     */
    private void controleChampNbEnregistrement2() {
        nbreEnregistrements2 = synthese.getNbEnregistrements2AsInteger();
        if (nbreEnregistrements2 == null) {
            String messageErreur = MessageFormat.format(RedacMessages.ERREUR_CHAMP_NON_NUMERIQUE, RedacMessages.CHAMP_NBRE_ENREGISTREMENTS2);
            valideChampAvecCollecte(false, synthese, RedacMessages.CHAMP_NBRE_ENREGISTREMENTS2, messageErreur);
        } else if (nbreEnregistrements2 < 0) {
            String messageErreur = MessageFormat.format(RedacMessages.ERREUR_CHAMP_ERRONE_VALEUR_INFERIEURE,
                    RedacMessages.CHAMP_NBRE_ENREGISTREMENTS2, "0");
            valideChampAvecCollecte(false, synthese, RedacMessages.CHAMP_NBRE_ENREGISTREMENTS2, messageErreur);

            LOGGER.error(messageErreur);
        }

    }

    /**
     * Contrôle la validité du champ type flux de la ligne de synthèse.
     * 
     */
    private void controleChampNbEnregistrement3() {
        nbreEnregistrements3 = synthese.getNbEnregistrements3AsInteger();
        if (nbreEnregistrements3 == null) {
            String messageErreur = MessageFormat.format(RedacMessages.ERREUR_CHAMP_NON_NUMERIQUE, RedacMessages.CHAMP_NBRE_ENREGISTREMENTS3);
            valideChampAvecCollecte(false, synthese, RedacMessages.CHAMP_NBRE_ENREGISTREMENTS3, messageErreur);
        } else if (nbreEnregistrements3 < 0) {
            String messageErreur = MessageFormat.format(RedacMessages.ERREUR_CHAMP_ERRONE_VALEUR_INFERIEURE,
                    RedacMessages.CHAMP_NBRE_ENREGISTREMENTS3, "0");
            valideChampAvecCollecte(false, synthese, RedacMessages.CHAMP_NBRE_ENREGISTREMENTS3, messageErreur);

        }

    }

    /**
     * Inscrit dans le contexte d'exécution du job des indicateurs de validité des compteurs du nombre d'enregistrement Test également si le fichier en entrée
     * est trié correctement.
     * 
     * @param stepExecution
     *            le contexte d'exécution de l'étape
     * @return le statut du contexte d'exécution à l'issue du traitement
     */
    @AfterStep
    public ExitStatus controleChampNbEnregistrement(StepExecution stepExecution) {

        if (!fichierBienTrie && compteurEnregistrements1 == 0) {
            // la ligne synthèse n'a pas été trouvée en première position ET le
            // nombre de ligne synthèse est 0
            // Le champ ligneImport de synthese étant par défaut à O, on
            // n'affichera pas le numéro de ligne dans la ligne de log
            stepExecution.getJobExecution().getExecutionContext().put(LIGNE_SYNTHESE_KO, false);
        } else if (!fichierBienTrie && compteurEnregistrements1 > 0) {
            // la ligne synthèse n'a pas été trouvée en première position ET il
            // y a plusieurs lignes synthèse
            LOGGER.error("Le tri du fichier a échoué");
            stepExecution.setExitStatus(ExitStatus.FAILED);
        } else {
            // il y a une ligne synthèse, trouvée en première position
            if (compteurEnregistrements2 >= 0 && compteurEnregistrements2 != nbreEnregistrements2) {
                stepExecution.getExecutionContext().put(CLE_FORCE_STATUT_EXECUTION, CodeRetour.ERREUR_FONCTIONNELLE_BLOQUANTE);
                stepExecution.getJobExecution().getExecutionContext().put(RedacMessages.CHAMP_NBRE_ENREGISTREMENTS2, false);
            }

            if (compteurEnregistrements3 >= 0 && compteurEnregistrements3 != nbreEnregistrements3) {
                stepExecution.getExecutionContext().put(CLE_FORCE_STATUT_EXECUTION, CodeRetour.ERREUR_FONCTIONNELLE_BLOQUANTE);
                stepExecution.getJobExecution().getExecutionContext().put(RedacMessages.CHAMP_NBRE_ENREGISTREMENTS3, false);
            }

        }
        return stepExecution.getExitStatus();
    }
}
