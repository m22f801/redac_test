package fr.si2m.red.batch.flux3.item;

import java.io.IOException;
import java.io.Writer;
import java.util.Collection;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.annotation.BeforeStep;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.file.FlatFileFooterCallback;

import lombok.Setter;

/**
 * Editeur de pied de page pour les exports du flux 3.<br/>
 * <br/>
 * Ce rédacteur garde à jour un compteur pour le nombre de lignes de certaines entités à exporter pour le résumé de bas de page puis invoque le véritable
 * rédacteur du corps de fichier.
 * 
 * @author poidij
 *
 * @param <T>
 *            le type de ligne exportée
 */
public class RedacteurPiedDePage<T> implements ItemWriter<T>, FlatFileFooterCallback {

    private static final Logger LOGGER = LoggerFactory.getLogger(RedacteurPiedDePage.class);

    /**
     * Délégué pour l'écriture effective des lignes dans le corps de fichier.
     */
    @Setter
    private ItemWriter<T> delegate;

    /**
     * Indicateur d'ajout du nombre d'entreprises présent dans le contexte d'exécution.
     */
    @Setter
    private boolean ajoutNbEntreprises = false;

    /**
     * Indicateur d'ajout du nombre d'entreprises présent dans le contexte d'exécution.
     */
    @Setter
    private boolean ajoutNbEtablissements = false;

    /**
     * Indicateur d'ajout du nombre de gestionnaires présent dans le contexte d'exécution.
     */
    @Setter
    private boolean ajoutNbGestionnaires = false;

    private int nbDeLignes = 2;
    private int nbDeLignesEntreprises = 0;
    private int nbDeLignesEtablissements = 0;
    private int nbDeLignesGestionnaires = 0;

    @Override
    public void writeFooter(final Writer writer) throws IOException {
        if (ajoutNbEntreprises) {
            nbDeLignes += nbDeLignesEntreprises;
        } else if (ajoutNbEtablissements) {
            nbDeLignes += nbDeLignesEtablissements;
        } else if (ajoutNbGestionnaires) {
            nbDeLignes += nbDeLignesGestionnaires;
        }
        String nbLignesTailleFixe = String.valueOf(nbDeLignes);
        while (nbLignesTailleFixe.length() < 11) {
            nbLignesTailleFixe = "0" + nbLignesTailleFixe;
        }
        writer.write("9999999999|" + nbLignesTailleFixe + "\r\n");
        LOGGER.debug("{} lignes totales affichées en pied-de-page", nbDeLignes);
    }

    @Override
    public void write(List<? extends T> items) throws Exception {
        for (Object item : items) {
            if (item instanceof Collection<?>) {
                nbDeLignes += ((Collection<?>) item).size();
            } else {
                nbDeLignes += 1;
            }
        }

        LOGGER.debug("{} lignes écrites...", nbDeLignes);
        delegate.write(items);
    }

    /**
     * En début d'étape, met à jour les compteurs de nombres de lignes à partir des données sauvegardées dans le contexte d'exécution.
     * 
     * @param stepExecution
     *            le contexte d'exécution
     */
    @BeforeStep
    public void updateStepExecutionData(StepExecution stepExecution) {
        JobExecution jobExecution = stepExecution.getJobExecution();
        ExecutionContext jobContext = jobExecution.getExecutionContext();
        nbDeLignesEntreprises += jobContext.getInt("nbEntreprises", 0);
        nbDeLignesEtablissements += jobContext.getInt("nbEtablissements", 0);
        nbDeLignesGestionnaires += jobContext.getInt("nbGestionnaires", 0);
    }

}
