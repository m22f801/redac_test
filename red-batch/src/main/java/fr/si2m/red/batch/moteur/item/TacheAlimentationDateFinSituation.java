package fr.si2m.red.batch.moteur.item;

import java.util.List;

import lombok.Setter;

import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;

import fr.si2m.red.batch.moteur.EtapeCodeRetourModificateur;
import fr.si2m.red.core.repository.EntiteImportableRepository;

/**
 * Tâche de promotion des domaines temporaires de référentiels lorsque toutes les entités importées ont été validées.
 * 
 * @author nortaina
 *
 */
public class TacheAlimentationDateFinSituation extends EtapeCodeRetourModificateur implements Tasklet {

    @Setter
    private List<EntiteImportableRepository<?>> referentiels;

    @Override
    public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) throws Exception {
        for (EntiteImportableRepository<?> referentiel : referentiels) {
            referentiel.promeutEntitesTemporaires();
        }
        return RepeatStatus.FINISHED;
    }

}
