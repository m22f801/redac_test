package fr.si2m.red.batch.flux10.item;

import org.apache.commons.lang3.StringUtils;

import fr.si2m.red.batch.moteur.erreur.ValidateurLigneAvecCollecte;
import fr.si2m.red.parametrage.ParamGroupeGestion;

/**
 * Validateur de ligne .
 * 
 * @author poidij
 *
 */
public class ParamGroupeGestionValidateurLigne extends ValidateurLigneAvecCollecte<ParamGroupeGestion> {

    @Override
    protected void valide(final ParamGroupeGestion groupeGestion) {
        // Champs obligatoires

        valideChampAvecCollecte(StringUtils.isNotBlank(groupeGestion.getNumGroupeGestion()), groupeGestion, "NMGRPGES",
                "le champ NMGRPGES obligatoire, n’est pas renseigné");

        // Longueur des champs

        if (StringUtils.isNotBlank(groupeGestion.getNumGroupeGestion())) {
            valideChampAvecCollecte(groupeGestion.getNumGroupeGestion().length() <= 2, groupeGestion, "NMGRPGES",
                    "le champ NMGRPGES dépasse la taille maximale prévue");
        }

        if (StringUtils.isNotBlank(groupeGestion.getMailAlerte())) {
            valideChampAvecCollecte(groupeGestion.getMailAlerte().length() <= 250, groupeGestion, "MAIL_ALERTE",
                    "le champ MAIL_ALERTE dépasse la taille maximale prévue");
        }

    }
}
