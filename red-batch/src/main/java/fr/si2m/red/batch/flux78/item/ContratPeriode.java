package fr.si2m.red.batch.flux78.item;

import java.text.MessageFormat;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.si2m.red.batch.EntiteImportableBatchNonPersistee;
import fr.si2m.red.batch.RedacMessages;

/**
 * Etat d'un contrat sur une période donnée, réceptionné du back-office.
 * 
 * @author poidij
 *
 */
@Data
@EqualsAndHashCode(callSuper = false, of = { "contratZoneA", "contratZoneB", "contratZoneC", "dateDebutPeriode", "dateFinPeriode" })
@ToString(of = { "contratZoneA", "contratZoneB", "contratZoneC", "dateDebutPeriode", "dateFinPeriode" })
public class ContratPeriode extends EntiteImportableBatchNonPersistee {
    /**
     * UID de version.
     */
    private static final long serialVersionUID = 6678021836532447710L;

    private static final Logger LOGGER = LoggerFactory.getLogger(ContratPeriode.class);

    /**
     * Le type d'enregistrement.
     * 
     * @param typeEnregistrement
     *            le type d'enregistrement
     * @return le type d'enregistrement
     * 
     */
    private String typeEnregistrement;
    private String contratZoneA;
    private String contratZoneB;
    private String contratZoneC;
    private String dateDebutPeriode;
    private String dateFinPeriode;
    private String codeNature;
    private String codeRejet;
    private String libelleRejet;
    private String groupeGestionCotisation;
    private String referencePaiement;
    private String filler;
    private String application;
    private Integer dateTraitementSynthese;
    private Integer emetteurCRSynthese;
    private Integer typeFluxSynthese;

    /**
     * Date de début de période.
     * 
     * @return la date de début de période
     */
    public Integer getDateDebutPeriodeAsInteger() {
        Integer intValue = null;
        try {
            intValue = Integer.valueOf(getDateDebutPeriode());
        } catch (NumberFormatException e) {
            LOGGER.warn(MessageFormat.format(RedacMessages.ERREUR_VALEUR_CONVERSION_ENTIER, "DateDebutPeriode", getDateDebutPeriode()), e);
        }
        return intValue;
    }

    /**
     * Date de fin de période.
     * 
     * @return la date de fin de période
     */
    public Integer getDateFinPeriodeAsInteger() {
        Integer intValue = null;
        try {
            intValue = Integer.valueOf(getDateFinPeriode());
        } catch (NumberFormatException e) {
            LOGGER.warn(MessageFormat.format(RedacMessages.ERREUR_VALEUR_CONVERSION_ENTIER, "DateFinPeriode", getDateFinPeriode()), e);
        }
        return intValue;
    }

    /**
     * Récupère le code nature.
     * 
     * @return le code nature
     */
    public Integer getCodeNatureAsInteger() {
        Integer intValue = null;
        try {
            intValue = Integer.valueOf(getCodeNature());
        } catch (NumberFormatException e) {
            LOGGER.warn(MessageFormat.format(RedacMessages.ERREUR_VALEUR_CONVERSION_ENTIER, "CodeNature", getCodeNature()), e);
        }
        return intValue;
    }

}
