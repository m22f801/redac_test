package fr.si2m.red.batch.flux4.execution;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.PersistenceContext;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParameter;
import org.springframework.batch.core.JobParameters;
import org.springframework.context.support.GenericXmlApplicationContext;
import org.springframework.transaction.TransactionStatus;

import fr.si2m.red.RedacUnexpectedException;
import fr.si2m.red.batch.RedacMessages;
import fr.si2m.red.batch.moteur.BatchConfiguration;
import fr.si2m.red.batch.moteur.CodeRetour;
import fr.si2m.red.batch.moteur.execution.ExecuteurBatch;
import fr.si2m.red.batch.moteur.execution.FacadeExecuteurBatch;

/**
 * Exécuteur de la phase 1 - contrôle de surface des fichiers à charger - du flux 4.
 * 
 * @author benitahy
 *
 */
@FacadeExecuteurBatch(nomBatch = "flux4_ControleFichiers")
public class Flux4ControleFichiersExecuteurBatch extends ExecuteurBatch {
    /**
     * Ordre du paramètre de lancement "emplacement du répertoire des fichiers".
     */
    public static final int ORDRE_PARAM_CHEMIN_REPERTOIRE_FICHIERS = 0;

    /**
     * Ordre du paramètre de lancement "données d'horodatage".
     */
    public static final int ORDRE_PARAM_HORODATAGE_FICHIERS = 1;

    /**
     * Ordre du paramètre de lancement "Code brique dsn".
     */
    public static final int ORDRE_PARAM_CODE_BRIQUE_DSN = 2;

    /**
     * Ordre du paramètre de lancement "Sytème d'information".
     */
    public static final int ORDRE_PARAM_SYSTEME_INFORMATION = 3;

    /**
     * Ordre du paramètre de lancement "Champ du contexte".
     */
    public static final int ORDRE_PARAM_CONTEXTE = 4;

    /**
     * Ordre du paramètre de lancement "code d'application".
     */
    public static final int ORDRE_PARAM_CODE_APPLICATION = 5;

    /**
     * Ordre du paramètre de lancement "numéro d'environnement".
     */
    public static final int ORDRE_PARAM_NUMERO_ENVIRONNEMENT = 6;

    /**
     * Ordre du paramètre de lancement "nom du batch".
     */
    public static final int ORDRE_PARAM_NOM_BATCH = 7;

    /**
     * Ordre du paramètre de lancement "emplacement du fichier de log d'erreurs".
     */
    public static final int ORDRE_PARAM_EMPLACEMENT_FICHIER_ERREURS = 8;

    /**
     * Indicateur de la ligne de fin de fichier.
     */
    public static final String INDICATEUR_FIN_DE_FICHIER = "9999999999|";
    /**
     * Taille du champ 1 de l'entête (Sytème d'information).
     */
    public static final int TAILLE_SYSTEME_INFORMATION = 15;
    /**
     * Taille maximale d'un entête de fichier.
     */
    public static final int TAILLE_MAXIMALE_ENTETE = 8192;

    private static final Logger LOGGER = LoggerFactory.getLogger(Flux4ControleFichiersExecuteurBatch.class);

    private final String cheminRepertoireFichiers;
    private final String horodatageFichiers;
    private final String codeBriqueDSN;
    private final String systemeInformation;
    private final String contexte;
    private final List<String> listeFichiers = new ArrayList<String>(Arrays.asList(".adhesion_etab_mois.ent", ".contact.ent", ".cotis_etab.ent",
            ".versement.ent", ".compo_verst.ent", ".individu.ent", ".chgt_indiv.ent", ".vers_indiv.ent", ".remuneration.ent", ".contr_trav.ent",
            ".arret_trav.ent", ".affiliation.ent", ".ayant_droit.ent", ".base_assuj.ent", ".compo_base_assuj.ent"));
    private final List<String> listeTypesFichiers = new ArrayList<String>(
            Arrays.asList("ADHESION_ETAB_MOIS", "CONTACT", "COTIS_ETAB", "VERSEMENT", "COMPO_VERST", "INDIVIDU", "CHGT_INDIV", "VERS_INDIV",
                    "REMUNERATION", "CONTR_TRAV", "ARRET_TRAV", "AFFILIATION", "AYANT_DROIT", "BASE_ASSUJ", "COMPO_BASE_ASSUJ"));
    private final String codeApplication;
    private final String numeroEnvironnement;
    private final String nomBatch;

    private boolean fichierInexistant = false;

    @PersistenceContext
    private HorodatageTraitementRepository horodatageTraitementRepository;

    /**
     * Exécuteur de batch du flux 4.
     * 
     * @param parametrageBatch
     *            les paramètres d'exécution du batch - deux paramètres sont attendus en entrée :
     *            <ol>
     *            <li>le chemin absolu vers le fichier à importer</li>
     *            <li>le chemin absolu vers le fichier de logs à éventuellement renseigner</li>
     *            </ol>
     */
    public Flux4ControleFichiersExecuteurBatch(String[] parametrageBatch) {
        super(parametrageBatch);
        this.cheminRepertoireFichiers = parametrageBatch[ORDRE_PARAM_CHEMIN_REPERTOIRE_FICHIERS];
        this.horodatageFichiers = parametrageBatch[ORDRE_PARAM_HORODATAGE_FICHIERS];
        this.codeBriqueDSN = parametrageBatch[ORDRE_PARAM_CODE_BRIQUE_DSN];
        this.systemeInformation = parametrageBatch[ORDRE_PARAM_SYSTEME_INFORMATION];
        this.contexte = parametrageBatch[ORDRE_PARAM_CONTEXTE];
        this.codeApplication = parametrageBatch[ORDRE_PARAM_CODE_APPLICATION];
        this.numeroEnvironnement = parametrageBatch[ORDRE_PARAM_NUMERO_ENVIRONNEMENT];

        this.nomBatch = parametrageBatch[ORDRE_PARAM_NOM_BATCH];

        String cheminAbsoluFichierLogErreurs = parametrageBatch[ORDRE_PARAM_EMPLACEMENT_FICHIER_ERREURS];
        setCheminAbsoluFichierLogErreurs(cheminAbsoluFichierLogErreurs);
    }

    @Override
    protected String getEmplacementFichierConfigurationJob() {
        return "classpath:fr/si2m/red/batch/flux4/job_ControleFichiers.xml";
    }

    @Override
    protected String getIdJobCible() {
        return null;
    }

    @Override
    protected boolean lanceTraitementAvantJob(GenericXmlApplicationContext contexteExecutionJob) {
        videEspacesTemporaires(contexteExecutionJob, HorodatageTraitementRepository.class);
        return true;
    }

    @Override
    protected JobParameters initParametrageJob(GenericXmlApplicationContext contexteExecutionJob) {
        Map<String, JobParameter> parametresJob = new HashMap<String, JobParameter>();

        return new JobParameters(parametresJob);
    }

    @Override
    protected void lanceTraitementFinJob(GenericXmlApplicationContext contexteExecutionJob, CodeRetour codeRetour, JobExecution executionJob) {
        // Logge le message de réussite du traitement batch dans le fichier de log
        if (codeRetour == CodeRetour.TERMINE) {
            traceTraitementOKDansFichierLog("Traitement du flux 4 - Contrôle des Fichiers - réussi");
        } else if (codeRetour == CodeRetour.ERREUR_FONCTIONNELLE_NON_BLOQUANTE) {
            traceTraitementOKDansFichierLog("Traitement du flux 4 - Contrôle des Fichiers - réussi, avec erreur(s) non bloquante(s)");
        } else if (codeRetour == CodeRetour.ERREUR_FONCTIONNELLE_BLOQUANTE) {
            LOGGER.error(MessageFormat.format(RedacMessages.ERREUR_JOB_CODE_RETOUR_40, "Détection d'anomalies bloquantes"));
        }
        LOGGER.info("Fin d'exécution du batch flux 4 - Contrôle de cohérence des fichiers");
    }

    @Override
    public int executeJob() {
        GenericXmlApplicationContext contexteExecutionJob = getContexteExecutionJob();
        CodeRetour codeRetourBatch;
        if (!lanceTraitementAvantJob(contexteExecutionJob)) {
            LOGGER.error(MessageFormat.format(RedacMessages.ERREUR_JOB_CODE_RETOUR_40, "L'exécution du job a été annulée."));
            codeRetourBatch = CodeRetour.ERREUR_FONCTIONNELLE_BLOQUANTE;
        }

        // Récupération d'une liste d'erreurs éventuelles sur les fichiers
        List<String> listeErreurs = new ArrayList<String>();
        try {
            listeErreurs = executeValidation();
        } catch (Exception e) {
            LOGGER.error("Une erreur technique a été détectée lors de l'exécution du batch", e);
            codeRetourBatch = CodeRetour.ERREUR_TECHNIQUE_BLOQUANTE;
        }
        if (!listeErreurs.isEmpty()) {
            traceErreurDansFichierLog(listeErreurs);
            codeRetourBatch = CodeRetour.ERREUR_FONCTIONNELLE_BLOQUANTE;
        } else {
            codeRetourBatch = CodeRetour.TERMINE;
        }

        lanceTraitementFinJob(contexteExecutionJob, codeRetourBatch, null);
        return codeRetourBatch.getCode();
    }

    /**
     * Valide les fichiers à contrôler.
     * 
     * @return la liste des erreurs relevées sur les fichiers
     * @throws IOException
     *             erreur levée lors de la lecture d'un fichier
     */
    private List<String> executeValidation() throws IOException {
        List<String> erreurs = new ArrayList<>();
        Set<String> horodatagesDistinctsEntetesLues = litEtValideFichiers(erreurs);
        if (!fichierInexistant) {
            valideHorodatageUniqueEtNonTraite(erreurs, horodatagesDistinctsEntetesLues);
        }
        return erreurs;
    }

    /**
     * Valide l'ensemble d'horodatages distincts lus dans les fichiers à contrôler.
     * 
     * @param erreurs
     *            la liste des erreurs à éventuellement renseigner
     * @param horodatagesDistinctsEntetesLues
     *            l'ensemble des horodatages distincts présents dans les entêtes des fichiers contrôlés
     */
    private void valideHorodatageUniqueEtNonTraite(List<String> erreurs, Set<String> horodatagesDistinctsEntetesLues) {
        // Vérification de l'unicité des horodatages dans les entêtes
        if (horodatagesDistinctsEntetesLues.isEmpty() || horodatagesDistinctsEntetesLues.size() > 1) {
            String messageErreur = "Tous les fichiers du lot n’ont pas la même date de constitution.";
            erreurs.add(messageErreur);
        } else {
            // Si horodatages uniques, alors on vérifie qu'un traitement n'a pas déjà été lancé pour cette date-heure
            String horodatageUnique = horodatagesDistinctsEntetesLues.iterator().next();

            if (verifieHorodatageDejaTraiteParFlux4(horodatageUnique)) {
                // L'horodatage existe déjà, on trace et on ajoute l'erreur dans la liste d'erreurs
                String messageErreur = "Un chargement à la date " + horodatageUnique + " a déjà été effectué";
                erreurs.add(messageErreur);
            } else {
                insereHorodatageTraitementTemporaire(horodatageUnique);
            }
        }
    }

    /**
     * Lit les fichiers à contrôler, en remplissant une liste d'erreurs éventuellement détectées, puis renvoit l'ensemble des horodatages distincts lus dans
     * leurs entêtes.
     * 
     * @param erreurs
     *            la liste des erreurs à éventuellement renseigner
     * @return l'ensemble des horodatages distincts lus dans les entêtes de fichier
     * @throws IOException
     *             erreur levée lors de la lecture d'un fichier
     */
    private Set<String> litEtValideFichiers(List<String> erreurs) throws IOException {
        Set<String> horodatagesDistinctsEntetesLues = new HashSet<>();
        for (int i = 0; i < listeFichiers.size(); i++) {
            String nomFichier = this.horodatageFichiers + "_" + this.codeApplication + this.codeBriqueDSN + "_" + "E" + this.numeroEnvironnement
                    + listeFichiers.get(i);

            String cheminDuFichier = cheminRepertoireFichiers + File.separator + nomFichier;
            File fichier = new File(cheminDuFichier);
            if (!fichier.exists()) {
                fichierInexistant = true;
                // Fichier inexistant => on s'arrête ici au niveau des contrôles
                String messageErreur = MessageFormat.format(RedacMessages.ERREUR_FICHIER_INEXISTANT, cheminDuFichier);
                erreurs.add(messageErreur);
            } else {

                // Fichier présent => on lance les contrôles de surface
                String typeFichierAttendu = listeTypesFichiers.get(i);
                String horodatageLuDansEntete = litEtValideFichier(fichier, typeFichierAttendu, erreurs);
                if (!StringUtils.isBlank(horodatageLuDansEntete)) {
                    horodatagesDistinctsEntetesLues.add(horodatageLuDansEntete);
                }

            }

        }
        return horodatagesDistinctsEntetesLues;
    }

    /**
     * Vérifie qu'un horodatage a déjà été tracé comme traité ou non dans le référentiel technique du batch flux 4.
     * 
     * @param horodatage
     *            l'horodatage à tester
     * @return true si l'horodatage a déjà été traité par un lancement du flux 4, false sinon
     */
    private boolean verifieHorodatageDejaTraiteParFlux4(String horodatage) {
        GenericXmlApplicationContext contexteExecution = getContexteExecutionJob();
        TransactionStatus txStatus = commenceTransaction(contexteExecution);
        try {
            horodatageTraitementRepository = contexteExecution.getBean(HorodatageTraitementRepository.class);
            boolean existe = horodatageTraitementRepository.existeUnHorodatageTraitement(horodatage);
            LOGGER.info("Horodatage vérifié : " + horodatage + "(préexistence : " + existe + ")");
            return existe;
        } catch (Exception e) {
            throw new RedacUnexpectedException("Une erreur inattendue est survenue pendant la recherche de l'horodatage", e);
        } finally {
            termineTransaction(contexteExecution, txStatus);
        }
    }

    /**
     * Insère un nouvel horadatage temporaire.
     * 
     * @param horodatage
     *            l'horodatage à insérer
     */
    private void insereHorodatageTraitementTemporaire(String horodatage) {
        GenericXmlApplicationContext contexteExecution = getContexteExecutionJob();
        TransactionStatus txStatus = commenceTransaction(contexteExecution);
        try {
            horodatageTraitementRepository = contexteExecution.getBean(HorodatageTraitementRepository.class);
            HorodatageTraitement nouvelHorodatage = new HorodatageTraitement();
            nouvelHorodatage.setHorodatage(horodatage);
            nouvelHorodatage.setLigneEnCoursImportBatch(true);
            nouvelHorodatage.setAuditUtilisateurCreation(this.nomBatch);
            horodatageTraitementRepository.create(nouvelHorodatage);
            LOGGER.info("Horodatage inséré : " + nouvelHorodatage);
        } catch (Exception e) {
            throw new RedacUnexpectedException("Une erreur inattendue est survenue pendant la création temporaire de l'horodatage", e);
        } finally {
            termineTransaction(contexteExecution, txStatus);
        }
    }

    /**
     * Lance les contrôles sur un fichier à valider en remplissant éventuellement une liste d'erreurs détectées et renvoie l'horodatage lu dans l'entête du
     * fichier.
     * 
     * @param fichier
     *            le fichier à lire et valider
     * @param typeFichierAttenduEnEntete
     *            le type de fichier déclaré en entête attendu
     * @param erreurs
     *            la liste d'erreurs à éventuellement renseigner
     * @return l'horodatage lu dans l'entête
     * @throws IOException
     *             erreur levée lors de la lecture d'un fichier
     */
    private String litEtValideFichier(File fichier, String typeFichierAttenduEnEntete, List<String> erreurs) throws IOException {
        String nomFichier = fichier.getName();
        long nombreLignesTotalFichier = 0L;
        BufferedReader lecteurFichier = new BufferedReader(
                new InputStreamReader(new FileInputStream(fichier), BatchConfiguration.ENCODAGE_FICHIERS_IMPORTES));

        // Lecture entête
        lecteurFichier.mark(TAILLE_MAXIMALE_ENTETE);
        String ligneEntete = lecteurFichier.readLine();

        String horodatageLuEnEntete = null;
        if (ligneEntete == null) {
            String messageErreur = MessageFormat.format(RedacMessages.ERREUR_FICHIER_VIDE, nomFichier);
            erreurs.add(messageErreur);
        } else {
            // Contrôle entête
            horodatageLuEnEntete = litEtValideLigneEntete(ligneEntete, typeFichierAttenduEnEntete, nomFichier, erreurs);

            lecteurFichier.reset();
            boolean enteteLu = false;

            // Lecture pied de page en comptant le nombre total de lignes au passage
            String ligneLue;
            String derniereLigneLue = null;
            try {
                while ((ligneLue = lecteurFichier.readLine()) != null) {
                    // RG F04_RG_P1_06 : Pour toutes les lignes du fichier, sauf l'entete et le pied:
                    // Les 14 premiers caractères de chaque ligne (des clés techniques) doivent correspondre à l’horodatage de la ligne d’en-tête du fichier
                    if ((!"9999999999".equals(ligneLue.substring(0, 10))) && enteteLu && (!ligneLue.substring(0, 14).equals(horodatageLuEnEntete))) {
                        String messageErreur = MessageFormat.format(RedacMessages.ERREUR_JOB_CODE_RETOUR_40,
                                "La clé technique " + ligneLue.substring(0, 29) + " du fichier " + nomFichier + " n'est pas conforme à l'horodatage "
                                        + horodatageLuEnEntete + " de la ligne d'entête.");
                        erreurs.add(messageErreur);
                    }
                    nombreLignesTotalFichier++;
                    derniereLigneLue = ligneLue;
                    enteteLu = true;
                }
            } finally {
                IOUtils.closeQuietly(lecteurFichier);
            }

            // Contrôle pied de page
            valideLignePiedDePage(derniereLigneLue, nombreLignesTotalFichier, nomFichier, erreurs);
        }

        return horodatageLuEnEntete;
    }

    /**
     * Lit une ligne d'entête de fichier et valide ses informations avant de retourner l'horodatage indiqué.
     * 
     * @param ligneEntete
     *            la ligne à lire et valider
     * @param typeFichierAttenduEnEntete
     *            le type de fichier attendu devant être spécifié par l'entête
     * @param nomFichier
     *            le nom du fichier
     * @param erreurs
     *            la liste des erreurs à éventuellement renseigner
     * @return l'horodatage présent dans l'entête s'il existe, null sinon
     */
    private String litEtValideLigneEntete(String ligneEntete, String typeFichierAttenduEnEntete, String nomFichier, List<String> erreurs) {
        String horodatageLuEnEntete = null;
        String[] entete = ligneEntete.split("\\|");

        // F04_RG_P1_01 Vérification s'il y a une entête valide
        String systemeInformationFichier = entete[0];
        if (entete.length != 4 || !entete[1].startsWith(typeFichierAttenduEnEntete)
                || systemeInformationFichier.length() != TAILLE_SYSTEME_INFORMATION) {

            String messageErreur = MessageFormat.format(RedacMessages.ERREUR_FICHIER_SANS_ENTETE, nomFichier);
            erreurs.add(messageErreur);

        } else {

            // F04_RG_P1_02 Récupération de tous les valeurs d'horodatage de tous les fichiers
            horodatageLuEnEntete = entete[2];

            // F04_RG_P1_04 Champ Sytème d'information
            if (!StringUtils.equals(this.systemeInformation, systemeInformationFichier.trim())) {
                String messageErreur = "L'entête du fichier " + nomFichier + " contient une valeur erronée pour le champ Système d'Information.";
                erreurs.add(messageErreur);
            }

            // F04_RG_P1_04 Champ Contexte
            String contexteFichier = entete[3];
            if (!StringUtils.equals(this.contexte, contexteFichier.trim())) {
                String messageErreur = "L'entête du fichier " + nomFichier + " contient une valeur erronée pour le champ Contexte.";
                erreurs.add(messageErreur);
            }

        }

        return horodatageLuEnEntete;
    }

    /**
     * Valide un pied de page de fichier.
     * 
     * @param derniereLigne
     *            la dernière ligne du fichier
     * @param nombreLignesTotalFichier
     *            le nombre total de lignes du fichier
     * @param nomFichier
     *            le nom du fichier
     * @param erreurs
     *            les erreurs à éventuellement renseigner
     */
    private void valideLignePiedDePage(String derniereLigne, long nombreLignesTotalFichier, String nomFichier, List<String> erreurs) {
        if (!StringUtils.startsWith(derniereLigne, INDICATEUR_FIN_DE_FICHIER)) {
            // F04_RG_P1_01 Contrôle de fin de fichier
            String messageErreur = MessageFormat.format(RedacMessages.ERREUR_FICHIER_SANS_FIN, nomFichier);
            erreurs.add(messageErreur);
        } else {
            // F04_RG_P1_05 Contrôle du nombre total de lignes du fichier
            try {
                String valeurAlphaNumerique = StringUtils.substringAfter(derniereLigne, INDICATEUR_FIN_DE_FICHIER);
                long nombreLignesFinFichier = Long.parseLong(StringUtils.trim(valeurAlphaNumerique));
                if (nombreLignesTotalFichier != nombreLignesFinFichier) {
                    String messageErreur = "Le nombre de lignes détail renseigné pour le fichier " + nomFichier + " est erroné.";
                    erreurs.add(messageErreur);
                }
            } catch (NumberFormatException e) {
                String messageErreur = "Une erreur est survenue pendant la récupération du nombre de lignes dans le fichier " + nomFichier + ".";
                LOGGER.error(messageErreur, e);
            }
        }
    }

}
