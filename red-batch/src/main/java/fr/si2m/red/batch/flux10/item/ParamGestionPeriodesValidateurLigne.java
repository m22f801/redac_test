package fr.si2m.red.batch.flux10.item;

import fr.si2m.red.batch.moteur.erreur.ValidateurLigneAvecCollecte;
import fr.si2m.red.parametrage.ParamGestionPeriodes;

/***
 * Validateur de ligne.
 * 
 * @author delortj
 *
 */
public class ParamGestionPeriodesValidateurLigne extends ValidateurLigneAvecCollecte<ParamGestionPeriodes> {

    @Override
    protected void valide(final ParamGestionPeriodes gestionPeriodes) {
        // Longueur des champs
        // F10_RG_C53
        if (gestionPeriodes.getDelaiPublicationFinDeclaration5() != null) {
            valideChampAvecCollecte(gestionPeriodes.getDelaiPublicationFinDeclaration5() < 100000000, gestionPeriodes, "DELAI_PUBLI_FIN_DECL5",
                    "le champ DELAI_PUBLI_FIN_DECL5 dépasse la taille maximale prévue");
        }

        // F10_RG_C54
        if (gestionPeriodes.getDelaiPublicationFinDeclaration15() != null) {
            valideChampAvecCollecte(gestionPeriodes.getDelaiPublicationFinDeclaration15() < 100000000, gestionPeriodes, "DELAI_PUBLI_FIN_DECL15",
                    "le champ DELAI_PUBLI_FIN_DECL15 dépasse la taille maximale prévue");
        }

        // F10_RG_C55
        if (gestionPeriodes.getDelaiPublicationCreationDeclaration() != null) {
            valideChampAvecCollecte(gestionPeriodes.getDelaiPublicationCreationDeclaration() < 100000000, gestionPeriodes, "DELAI_PUBLI_CREA_DECL",
                    "le champ DELAI_PUBLI_CREA_DECL dépasse la taille maximale prévue");
        }

        // F10_RG_C56
        if (gestionPeriodes.getDelaiPublicationCreationComplement() != null) {
            valideChampAvecCollecte(gestionPeriodes.getDelaiPublicationCreationComplement() < 1000, gestionPeriodes, "DELAI_PUBLI_CREA_COMPL",
                    "le champ DELAI_PUBLI_CREA_COMPL dépasse la taille maximale prévue");
        }

        // F10_RG_C57
        if (gestionPeriodes.getDelaiPublicationCreationRegulation() != null) {
            valideChampAvecCollecte(gestionPeriodes.getDelaiPublicationCreationRegulation() < 1000, gestionPeriodes, "DELAI_PUBLI_CREA_REGUL",
                    "le champ DELAI_PUBLI_CREA_REGUL dépasse la taille maximale prévue");
        }
    }
}
