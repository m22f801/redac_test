package fr.si2m.red.batch.moteur.item;

import java.io.IOException;
import java.io.Writer;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import lombok.Setter;

import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.file.FlatFileHeaderCallback;

import fr.si2m.red.DateRedac;

/**
 * Editeur d'entête de page pour les exports REDAC.
 * 
 * @author poidij
 *
 * @param <T>
 *            le type de ligne exportée
 */
public class RedacHeaderCallback<T> implements ItemWriter<T>, FlatFileHeaderCallback {

    /**
     * Le délégué en écriture de contenu.
     * 
     * @param delegate
     *            le délégué en écriture de contenu
     */
    @Setter
    private ItemWriter<T> delegate;

    /**
     * Le nom de l'entité explicité en entête de fichier.
     * 
     * @param nomEntite
     *            le nom de l'entité explicité en entête de fichier
     */
    @Setter
    private String nomEntite;

    /**
     * Le Système d’Information Source explicité en entête de fichier.
     * 
     * @param siSource
     *            le Système d’Information Source de l'entité explicité en entête de fichier
     */
    @Setter
    private String siSource;

    /**
     * Le contexte explicité en entête de fichier.
     * 
     * @param nomEntite
     *            le contexte de l'entité explicité en entête de fichier
     */
    @Setter
    private String contexte;

    @Override
    public void writeHeader(Writer writer) throws IOException {
        SimpleDateFormat sdf = new SimpleDateFormat(DateRedac.EXPORT_FORMAT_PAR_DEFAUT);
        String date = sdf.format(new Date());

        String entite = nomEntite;
        while (entite.length() < 25) {
            entite += " ";
        }

        // siSource de longueur 15, tronqué au besoin.
        String siSourceTmp = siSource;
        while (siSourceTmp.length() < 15) {
            siSourceTmp += " ";
        }
        if (siSourceTmp.length() > 15) {
            siSourceTmp = siSourceTmp.substring(0, 15);
        }

        // Contexte de longueur 5, tronqué au besoin.
        String contexteTmp = contexte;
        while (contexteTmp.length() < 5) {
            contexteTmp += " ";
        }
        if (contexteTmp.length() > 5) {
            contexteTmp = contexteTmp.substring(0, 5);
        }

        // longueurs : 15|25|10|5 (et 2 pour le CRLF)
        writer.write(siSourceTmp + "|" + entite + "|" + date + "|" + contexteTmp);
    }

    @Override
    public void write(List<? extends T> items) throws Exception {
        delegate.write(items);
    }

}
