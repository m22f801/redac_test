package fr.si2m.red.batch.flux1.item;

import org.apache.commons.lang3.StringUtils;

import fr.si2m.red.batch.moteur.item.ModificateurChampsLigneValide;
import fr.si2m.red.contrat.Contrat;

/**
 * Gestionnaire de modification des champs d'un {@link Contrat} valide.
 * 
 * @author poidij
 *
 */
public class ContratProcesseurLigne extends ModificateurChampsLigneValide<Contrat> {

    @Override
    public void modifieChamps(final Contrat contrat) {
        // Traitement du champ ELIGDSN pour la partie Mapping
        // F01_RG_BC06
        if (StringUtils.isBlank(contrat.getEligibiliteContratDsn())) {
            contrat.setEligibiliteContratDsn("NON");
        }
    }

}
