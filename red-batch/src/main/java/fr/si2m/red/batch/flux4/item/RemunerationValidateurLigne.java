package fr.si2m.red.batch.flux4.item;

import lombok.Setter;

import org.apache.commons.lang3.StringUtils;

import fr.si2m.red.DateRedac;
import fr.si2m.red.batch.RedacMessages;
import fr.si2m.red.batch.moteur.erreur.ValidateurLigneAvecCollecte;
import fr.si2m.red.dsn.Remuneration;
import fr.si2m.red.dsn.RemunerationRepository;
import fr.si2m.red.dsn.VersementIndividuRepository;

/**
 * Validateur de ligne de paramétrage code libellé.
 * 
 * @author benitahy
 *
 */
public class RemunerationValidateurLigne extends ValidateurLigneAvecCollecte<Remuneration> {

    @Setter
    private VersementIndividuRepository versementIndividuRepository;

    @Setter
    private RemunerationRepository remunerationRepository;

    @Override
    protected void valide(final Remuneration remuneration) {

        // unicité
        valideChampAvecCollecte(!remunerationRepository.existeUneRemuneration(remuneration.getIdRemuneration()), remuneration, "ID", "La ligne ne respecte pas la règle d'unicité");

        // Champs obligatoires
        valideChampAvecCollecte(StringUtils.isNotBlank(remuneration.getIdRemuneration()), remuneration, "ID_REMUNERATION",
                "Le champ ID_REMUNERATION obligatoire n'est pas renseigné");
        valideChampAvecCollecte(StringUtils.isNotBlank(remuneration.getIdVersementIndividu()), remuneration, "ID_VERS_INDIV",
                "Le champ ID_VERS_INDIV obligatoire n'est pas renseigné");

        // Longueur des champs
        valideTailleFixeChampAvecCollecte(remuneration.getIdRemuneration(), 30, remuneration, "ID_REMUNERATION");
        valideTailleFixeChampAvecCollecte(remuneration.getIdVersementIndividu(), 30, remuneration, "ID_VERS_INDIV");
        valideChampDateTailleFixeAvecCollecte(remuneration.getDateDebutPeriodePaieAsText(), DateRedac.FORMAT_DATES_DDMMYYYY, remuneration, "DATE_DEB_PERIODE_PAIE", false);
        valideChampDateTailleFixeAvecCollecte(remuneration.getDateFinPeriodePaieAsText(), DateRedac.FORMAT_DATES_DDMMYYYY, remuneration, "DATE_FIN_PERIODE_PAIE", false);
        valideTailleFixeChampAvecCollecte(remuneration.getNumeroContratTravail(), 20, remuneration, "NUMERO_CONTRAT");
        valideTailleFixeChampAvecCollecte(remuneration.getTypeContratTravail(), 3, remuneration, "TYPE_CONTRAT");
        valideChampNumeriqueDecimaleTailleFixeAvecCollecte(remuneration.getMontantPaieAsText(), 9, 2, remuneration, "MONTANT");

        validationCorrespondanceReferentielle(remuneration);
    }

    /**
     * Validation des liens entre tables.
     * 
     * @param remuneration
     *            La rémunération dont le versement individu doit être validé.
     */
    private void validationCorrespondanceReferentielle(final Remuneration remuneration) {
        if (StringUtils.isNotBlank(remuneration.getIdVersementIndividu())) {
            boolean existeUnVersementIndividu = versementIndividuRepository.existeUnVersementIndividu(remuneration.getIdVersementIndividu());
            valideChampAvecCollecte(existeUnVersementIndividu, remuneration, "IdVersementIndividu", RedacMessages.ERREUR_REFERENCE_FLUX4);
        }
    }
}
