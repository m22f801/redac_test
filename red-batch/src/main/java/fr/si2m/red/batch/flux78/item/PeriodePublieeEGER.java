package fr.si2m.red.batch.flux78.item;

import fr.si2m.red.core.repository.jpa.conversion.ConvertisseurBooleanOuiNon;
import lombok.Data;

/**
 * Modèle des périodes publiées pour EGER.
 * 
 * @author eudesr
 *
 */
@Data
public class PeriodePublieeEGER {

    /**
     * identifiant technique periode
     */
    Long idPeriode;

    /**
     * Date de création de la période
     */
    Integer dateCreation;

    /**
     * Type de la période
     */
    String typePeriode;

    /**
     * Informations Entreprise/contrat/période
     */

    /**
     * date envoi du fichier format JJ/MM/AAAA
     */
    String dateEnvoi;

    /**
     * Siret du souscripteur
     */
    String siret;

    /**
     * Raison Sociale contractée du souscripteur
     */
    String raisonSocialeEntreprise;

    /**
     * Libellé Population administrative
     */
    String libPopulation;

    /**
     * Assureur
     */
    String assureur;

    /**
     * Identifiant Individu (NIR ou NTT)
     */
    String individu;

    /**
     * Numéro de contrat commercial
     */
    String numeroContrat;

    /**
     * Numéro population
     */
    String codePopulation;

    /**
     * date de début de période de déclaration format JJ/MM/AAAA
     */
    String dateDebutPeriode;

    /**
     * date de début de période de déclaration Integer - format AAAAMMJJ
     */
    Integer dateDebutPeriodeAsInteger;

    /**
     * date de fin de période de déclaration format JJ/MM/AAAA
     */
    String dateFinPeriode;

    /**
     * date de fin de période de déclaration Integer - format AAAAMMJJ
     */
    Integer dateFinPeriodeAsInteger;

    /**
     * Données cotisants
     */

    /**
     * Civilité
     */
    String civilite;

    /**
     * Nom marital
     */
    String nomMarital;

    /**
     * Nom de naissance adhérent
     */
    String nomFamilleAdherent;

    /**
     * Prénom adhérent
     */
    String prenomAdherent;

    /**
     * Date naissance format JJ/MM/AAAA
     */
    String dateNaissance;

    /**
     * Code postal naissance
     */
    String codePostalNaissance;

    /**
     * Ville de naissance
     */
    String villeNaissance;

    /**
     * Pays de naissance
     */
    String paysNaissance;

    /**
     * Numéro Sécurité Sociale
     */
    String numeroSecuriteSociale;

    /**
     * Régime Sécurité Sociale - représentation String
     */
    String regimeSecuriteSociale;

    /**
     * Adresse
     */

    /**
     * Complément destinataire
     */
    String complDestinataire;

    /**
     * Complément adresse
     */
    String complAdresse;

    /**
     * Numéro et voie
     */
    String numVoie;

    /**
     * Lieu-dit ou Ville Etrangère
     */
    String lieuDit;

    /**
     * Code Postal
     */
    String codePostal;

    /**
     * Bureau distributeur
     */
    String localite;

    /**
     * Code INSEE pays
     */
    String codePaysInsee;

    /**
     * Données mouvement cotisant
     */

    /**
     * Date d’entrée format JJ/MM/AAAA
     */
    String dateEntree;

    /**
     * Date d’entrée format AAAAMMJJ
     */
    Integer dateEntreeAsInteger;

    /**
     * Date de sortie format JJ/MM/AAAA
     */
    String dateSortie;

    /**
     * Date de sortie format AAAAMMJJ
     */
    Integer dateSortieAsInteger;

    /**
     * Motif de sortie
     */
    String motifSortie;

    /**
     * Date début de période de cotisation format JJ/MM/AAAA
     */
    String debutPeriodeCotisation;

    /**
     * Date début de période de cotisation format AAAAMMJJ
     */
    Integer debutPeriodeCotisationAsInteger;

    /**
     * Date fin de période de cotisation format JJ/MM/AAAA
     */
    String finPeriodeCotisation;

    /**
     * Date fin de période de cotisation format AAAAMMJJ
     */
    Integer finPeriodeCotisationAsInteger;

    /**
     * Montant cotisation - représentation String ( précision 15,2 )
     */
    String montantCotisation;

    /**
     * Montant cotisation ( précision 15,2 )
     */
    Double montantCotisationAsDouble;

    /**
     * assietteTranche A
     */
    String assietteTrancheA;

    /**
     * Taux Tranche A
     */
    String tauxTrancheA;

    /**
     * assietteTranche B
     */
    String assietteTrancheB;

    /**
     * Taux Tranche B
     */
    String tauxTrancheB;

    /**
     * assietteTranche C
     */
    String assietteTrancheC;

    /**
     * Taux Tranche C
     */
    String tauxTrancheC;

    /**
     * assietteTranche D
     */
    String assietteTrancheD;

    /**
     * Taux Tranche D
     */
    String tauxTrancheD;

    /**
     * Flag indiquant si on souhaite afficher la population dans la publication
     */
    String avecPopulationAsText;

    /**
     * L'indicateur d'affichage de la population
     * 
     * @return l'indicateur d'affichage de la population
     */
    public boolean isAvecPopulation() {
        return new ConvertisseurBooleanOuiNon().convertToEntityAttribute(avecPopulationAsText);
    }
}
