package fr.si2m.red.batch.moteur.support;

import java.util.Arrays;

import lombok.Setter;

import org.springframework.batch.item.file.transform.DefaultFieldSet;

/**
 * Implémentation par défaut des champs lues en import par REDAC.
 * 
 * @author nortaina
 *
 */
public class RedacFieldSet extends DefaultFieldSet {

    @Setter
    private boolean alwaysTrim = false;

    /**
     * Create a FieldSet with anonymous tokens. They can only be retrieved by column number.
     * 
     * @param tokens
     *            the token values
     */
    public RedacFieldSet(String[] tokens) {
        super(tokens);
    }

    /**
     * Create a FieldSet with named tokens. The token values can then be retrieved either by name or by column number.
     * 
     * @param tokens
     *            the token values
     * @param names
     *            the names of the tokens
     */
    public RedacFieldSet(String[] tokens, String[] names) {
        super(tokens, names);
    }

    /**
     * Read and trim the {@link String} value at '<code>index</code>' only if {@link #alwaysTrim} is true.
     * 
     * @param index
     *            the position of the value
     * 
     * @return null if the field value is <code>null</code>.
     */
    @Override
    protected String readAndTrim(int index) {
        String value = super.getValues()[index];

        if (value != null) {
            return alwaysTrim ? value.trim() : value;
        } else {
            return null;
        }
    }

    @Override
    public boolean equals(Object object) {
        if (object != null && object.getClass() == getClass()) {
            String[] tokens = super.getValues();
            RedacFieldSet fs = (RedacFieldSet) object;
            if (tokens == null) {
                return fs.getValues() == null;
            } else {
                return Arrays.equals(tokens, fs.getValues());
            }
        }

        return false;
    }

    @Override
    public int hashCode() {
        String[] tokens = super.getValues();
        // this algorithm was taken from java 1.5 jdk Arrays.hashCode(Object[])
        int result = alwaysTrim ? 0 : 1;
        if (tokens != null) {
            for (int i = 0; i < tokens.length; i++) {
                result = 31 * result + (tokens[i] == null ? 0 : tokens[i].hashCode());
            }
        }
        return result;
    }

}
