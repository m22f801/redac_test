package fr.si2m.red.batch.flux3.ligne;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;
import javax.persistence.Transient;

import fr.si2m.red.DateRedac;
import fr.si2m.red.Entite;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * Description d'une ligne d'un fichier CONTRAT à exporter.
 * 
 * @author nortaina
 *
 */

@Entity
@Table(name = "TMP_RR300_SITUATIONS_CONTRATS")
@Data
@IdClass(SituationContratId.class)
@EqualsAndHashCode(callSuper = false, of = { "numContrat", "dateDebutSituation" })
@ToString(callSuper = false, of = { "numContrat", "dateDebutSituation" })
public class SituationContrat extends Entite {

    /**
     * UID de version.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Numéro de contrat à afficher.
     * 
     * @param numContrat
     *            Numéro de contrat à afficher
     * @return Numéro de contrat à afficher
     */
    @Id
    @Column(name = "NOCO")
    private String numContrat;

    /**
     * Date de début de validité des paramètres contrat.
     * 
     * @param dateDebutSituation
     *            Date de début de validité des paramètres contrat
     * @return Date de début de validité des paramètres contrat
     */
    @Id
    @Column(name = "DATE_DEB_SIT")
    private Integer dateDebutSituation;

    /**
     * Code Mise à jour.
     * 
     * @param codeMiseAJour
     *            Code Mise à jour
     * @return Code Mise à jour
     */
    @Column(name = "CODE_MISE_A_JOUR")
    private String codeMiseAJour;

    /**
     * Identifiant de l’entreprise G3C.
     * 
     * @param identifiantEntrepriseG3C
     *            Identifiant de l’entreprise G3C
     * @return Identifiant de l’entreprise G3C
     */
    @Transient
    private String identifiantEntrepriseG3C;

    /**
     * N° de séquence contrat G3C.
     * 
     * @param numSequenceContratG3C
     *            N° de séquence contrat G3C
     * @return N° de séquence contrat G3C
     */
    @Transient
    private String numSequenceContratG3C;

    /**
     * Code Produit.
     * 
     * @param codeProduit
     *            Code Produit
     * @return Code Produit
     */
    @Transient
    private String codeProduit;

    /**
     * Libellé Produit.
     * 
     * @param libelleProduit
     *            Libellé Produit
     * @return Libellé Produit
     */
    @Transient
    private String libelleProduit;

    /**
     * Identifiant de l’entreprise (identifiant technique) G3C.
     * 
     * @param identifiantTechniqueEntrepriseG3C
     *            Identifiant de l’entreprise (identifiant technique) G3C
     * @return Identifiant de l’entreprise (identifiant technique) G3C
     */
    @Transient
    private String identifiantTechniqueEntrepriseG3C;

    /**
     * Type de contrat (Santé, Prévoyance, Retraite par capitalisation, ..).
     * 
     * @param typeContrat
     *            Type de contrat (Santé, Prévoyance, Retraite par capitalisation, ..)
     * @return Type de contrat (Santé, Prévoyance, Retraite par capitalisation, ..)
     */
    @Column(name = "TYPE_CONTRAT")
    private String typeContrat;

    /**
     * N° de famille de garantie au format int et pas Integer pour des raison de nullité dans le situationContratRedacteurFichierPlat
     * 
     * @param numFamilleGarantie
     *            N° de famille de garantie
     * @return N° de famille de garantie
     */
    @Transient
    private int numFamilleGarantie;

    /**
     * Date de début d’effet du contrat.
     * 
     * @param dateDebutEffetContrat
     *            Date de début d’effet du contrat
     * @return Date de début d’effet du contrat
     */
    @Column(name = "DTEFFCO")
    private Integer dateDebutEffetContrat;

    /**
     * Date de radiation du contrat (éventuelle).
     * 
     * @param dateRadiationContrat
     *            Date de radiation du contrat (éventuelle)
     * @return Date de radiation du contrat (éventuelle)
     */
    @Transient
    private Integer dateRadiationContrat;

    /**
     * Code organisme porteur de risque.
     * 
     * @param codeOrganismePorteurRisque
     *            Code organisme porteur de risque
     * @return Code organisme porteur de risque
     */
    @Column(name = "CODE_ORGANISME")
    private String codeOrganismePorteurRisque;

    /**
     * Code organisme délégataire de cotisation (éventuel) (un seul possible).
     * 
     * @param codeOrganismeDelegataireCotisation
     *            Code organisme délégataire de cotisation (éventuel) (un seul possible)
     * @return Code organisme délégataire de cotisation (éventuel) (un seul possible)
     */
    @Transient
    private String codeOrganismeDelegataireCotisation;

    /**
     * Code organisme assureur 1. au format int et pas Integer pour des raison de nullité dans le situationContratRedacteurFichierPlat
     * 
     * @param codeOrganismeAssureur1
     *            Code organisme assureur 1
     * @return Code organisme assureur 1
     */
    @Transient
    private int codeOrganismeAssureur1;

    /**
     * Code organisme assureur 2. au format int et pas Integer pour des raison de nullité dans le situationContratRedacteurFichierPlat
     * 
     * @param codeOrganismeAssureur2
     *            Code organisme assureur 2
     * @return Code organisme assureur 2
     */
    @Transient
    private int codeOrganismeAssureur2;

    /**
     * Numéro de compte encaisseur (spécificité Quatrem).
     * 
     * @param numCompteEncaisseur
     *            Numéro de compte encaisseur (spécificité Quatrem)
     * @return Numéro de compte encaisseur (spécificité Quatrem)
     */
    @Column(name = "NCENC")
    private String numCompteEncaisseur;

    /**
     * Périodicité de Paiement.
     * 
     * @param periodicitePaiement
     *            Périodicité de Paiement
     * @return Périodicité de Paiement
     */
    @Column(name = "COFRQUIT_TR")
    private String periodicitePaiement;

    /**
     * Le taux d'appel (7 chiffres dont 2 décimales).
     * 
     * @param tauxAppel
     *            le taux d'appel (7 chiffres dont 2 décimales)
     * @return le taux d'appel (7 chiffres dont 2 décimales)
     * 
     */
    @Column(name = "TXAPPCOT")
    private Double tauxAppel;

    /**
     * Contrat VIP.
     * 
     * @param contratVIP
     *            Contrat VIP
     * @return Contrat VIP
     */
    @Column(name = "VIP")
    private String contratVIP;

    /**
     * Date de fin de validité des paramètres contrat.
     * 
     * @param dateFinSituation
     *            Date de fin de validité des paramètres contrat
     * @return Date de fin de validité des paramètres contrat
     */
    @Column(name = "DATE_FIN_SIT")
    private Integer dateFinSituation;

    /**
     * indicateur de contrôle de code assiette.
     * 
     * @param indicateurControleAssiette
     *            indicateur de contrôle de code assiette
     * @return indicateur de contrôle de code assiette
     */
    @Column(name = "IND_ASSIETTE_TR")
    private String indicateurControleAssiette;

    /**
     * Ancien numéro de contrat.
     * 
     * @param ancienNumContrat
     *            Ancien numéro de contrat
     * @return Ancien numéro de contrat
     */
    @Transient
    private String ancienNumContrat;

    /**
     * Ancien code organisme délégataire de cotisation.
     * 
     * @param ancienCodeOrganismeDelegataireCotisation
     *            Ancien code organisme délégataire de cotisation
     * @return Ancien code organisme délégataire de cotisation
     */
    @Transient
    private String ancienCodeOrganismeDelegataireCotisation;

    /**
     * Identifiant délégataire de cotisation (N° de séquence locale à une instance de cette interface).
     * 
     * @param identifiantDelegataireCotisation
     *            Identifiant délégataire de cotisation (N° de séquence local à une instance de cette interface)
     * @return Identifiant délégataire de cotisation (N° de séquence local à une instance de cette interface)
     */

    @Transient
    private String ancienCodeOrganismePorteurDeRisque;

    /**
     * Identifiant porteur de risque.
     * 
     * @param ancienCodeOrganismePorteurDeRisque
     *            Identifiant organisme porteur de risque
     * @return Identifiant organisme porteur de risque
     */

    @Column(name = "IDEN_DEL_COT")
    private String identifiantDelegataireCotisation;

    /**
     * Identifiant technique contrat (N° de séquence local à une instance de cette interface).
     * 
     * @param identifiantTechniqueContrat
     *            Identifiant technique contrat (N° de séquence local à une instance de cette interface)
     * @return Identifiant technique contrat (N° de séquence local à une instance de cette interface)
     */
    @Column(name = "IDEN_TEC_CONT")
    private String identifiantTechniqueContrat;

    /**
     * Désignation contrat.
     * 
     * @param designationContrat
     *            Désignation contrat
     * @return Désignation contrat
     */
    @Column(name = "DESIGNATION_FAMILLE")
    private String designationContrat;

    /**
     * Numero Client d'une ligne Contrat.
     * 
     * @param numClient
     *            numero client
     * 
     * @return numero client
     */
    @Column(name = "NUMERO_CLIENT")
    private Long numClient;

    /**
     * Eligibilite DSN - pour une ligne donnée de Contrat.
     * 
     * @param eligibiliteDSN
     *            eligibiliteDSN
     * 
     * @return eligibiliteDSN
     */
    @Column(name = "ELIGIBILITE_DSN")
    private String eligibiliteDSN;

    /**
     * code intermediaire pour une ligne Contrat.
     * 
     * @param codeIntermediaire
     *            codeIntermediaire
     * 
     * @return codeIntermediaire
     */
    @Column(name = "CODE_INTERMEDIAIRE")
    private String codeIntermediaire;

    /**
     * Si la situation de contrat est en mode délégué.
     * 
     * @param modeDelegue
     *            si la situation de contrat est en mode délégué
     * 
     * @return true si la situation de contrat est en mode délégué, false sinon
     */
    @Column(name = "MODE_DELEGUE")
    private boolean modeDelegue;

    /**
     * Récupération du numéro de groupe de gestion du contrat
     * 
     * @param nmGrpges
     *            nmGrpges
     * 
     * @return nmGrpges
     */
    @Column(name = "NMGRPGES")
    private String nmGrpges;

    /**
     * Date de début d’effet du contrat formatée.
     * 
     * @return Date de début d’effet du contrat formatée
     */
    public String getDateDebutEffetContratFormatee() {
        return DateRedac.formate(DateRedac.EXPORT_FORMAT_PAR_DEFAUT, dateDebutEffetContrat);
    }

    /**
     * Date de radiation du contrat (éventuelle) formatée.
     * 
     * @return Date de radiation du contrat (éventuelle) formatée
     */
    public String getDateRadiationContratFormatee() {
        return DateRedac.formate(DateRedac.EXPORT_FORMAT_PAR_DEFAUT, dateRadiationContrat);
    }

    /**
     * Date de début de situation formatée.
     * 
     * @return Date de début de situation formatée
     */
    public String getDateDebutSituationFormatee() {
        return DateRedac.formate(DateRedac.EXPORT_FORMAT_PAR_DEFAUT, dateDebutSituation);
    }

    /**
     * Date de fin de situation formatée.
     * 
     * @return Date de fin de situation formatée
     */
    public String getDateFinSituationFormatee() {
        return DateRedac.formate(DateRedac.EXPORT_FORMAT_PAR_DEFAUT, dateFinSituation);
    }

    @Override
    public SituationContratId getId() {
        return new SituationContratId(numContrat, dateDebutSituation);
    }
}
