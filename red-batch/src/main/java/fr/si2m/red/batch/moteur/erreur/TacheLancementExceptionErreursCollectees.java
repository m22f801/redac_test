package fr.si2m.red.batch.moteur.erreur;

import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;

import fr.si2m.red.batch.moteur.ErreurFonctionnelleBloquanteException;

/**
 * Tâche de lancement d'une exception pour arrêter les traitements batch en cas de collecte d'erreurs positive.
 * 
 * @author nortaina
 *
 */
public class TacheLancementExceptionErreursCollectees implements Tasklet {

    @Override
    public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) throws Exception {
        throw new ErreurFonctionnelleBloquanteException("Détection d'anomalies bloquantes");
    }
}
