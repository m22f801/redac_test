package fr.si2m.red.batch.flux4.execution;

import java.io.File;
import java.text.MessageFormat;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParameter;
import org.springframework.batch.core.JobParameters;
import org.springframework.context.support.GenericXmlApplicationContext;

import fr.si2m.red.EntiteImportableBatch;
import fr.si2m.red.batch.RedacMessages;
import fr.si2m.red.batch.moteur.CodeRetour;
import fr.si2m.red.core.repository.EntiteImportableRepository;

/**
 * Base des exécuteurs de la phase 2 - chargement des données - du flux 4.
 * 
 * @author poidij
 *
 */
public abstract class Flux4ChargementExecuteurBatch extends Flux4ExecuteurBatch {
    /**
     * Ordre du paramètre de lancement "emplacement du répertoire des fichiers".
     */
    public static final int ORDRE_PARAM_CHEMIN_REPERTOIRE_FICHIERS = 0;
    /**
     * Ordre du paramètre de lancement "données d'horodatage".
     */
    public static final int ORDRE_PARAM_HORODATAGE_FICHIERS = 1;
    /**
     * Ordre du paramètre de lancement "type de fichier".
     */
    public static final int ORDRE_PARAM_TYPE_FICHIER = 2;

    /**
     * Ordre du paramètre de lancement "Code brique dsn".
     */
    public static final int ORDRE_PARAM_CODE_BRIQUE_DSN = 3;
    /**
     * Ordre du paramètre de lancement "Sytème d'information".
     */
    public static final int ORDRE_PARAM_SYSTEME_INFORMATION = 4;
    /**
     * Ordre du paramètre de lancement "Champ du contexte".
     */
    public static final int ORDRE_PARAM_CONTEXTE = 5;
    /**
     * Ordre du paramètre de lancement "code application" .
     */
    public static final int ORDRE_PARAM_CODE_APPLICATION = 6;
    /**
     * Ordre du paramètre de lancement "numéro d'environnement" .
     */
    public static final int ORDRE_PARAM_NUMERO_ENVIRONNEMENT = 7;

    /**
     * Ordre du paramètre de lancement "emplacement du fichier de log d'erreurs" .
     */
    public static final int ORDRE_PARAM_EMPLACEMENT_FICHIER_ERREURS = 8;

    /**
     * Taille maximale d'un entête de fichier.
     */
    public static final int TAILLE_MAXIMALE_ENTETE = 8192;

    /**
     * Taille maximale d'un entête de fichier.
     */
    public static final String EXTENSION_FICHIER = ".ent";

    private static final Logger LOGGER = LoggerFactory.getLogger(Flux4ChargementExecuteurBatch.class);

    private final String cheminRepertoireFichiers;
    private final String horodatageFichier;
    private final String typeFichier;
    private final String codeBriqueDSN;
    private final String codeApplication;
    private final String numeroEnvironnement;

    private final String cheminAbsoluFichierImport;
    private final String emplacementRessourceFichierImport;

    /**
     * Exécuteur de batch du flux 4.
     * 
     * @param parametrageBatch
     *            les paramètres d'exécution du batch - deux paramètres sont attendus en entrée :
     *            <ol>
     *            <li>le chemin absolu vers le fichier à importer</li>
     *            <li>le chemin absolu vers le fichier de logs à éventuellement renseigner</li>
     *            </ol>
     */
    protected Flux4ChargementExecuteurBatch(String[] parametrageBatch) {
        super(parametrageBatch);

        this.cheminRepertoireFichiers = parametrageBatch[ORDRE_PARAM_CHEMIN_REPERTOIRE_FICHIERS];
        this.horodatageFichier = parametrageBatch[ORDRE_PARAM_HORODATAGE_FICHIERS];
        this.typeFichier = parametrageBatch[ORDRE_PARAM_TYPE_FICHIER];
        this.codeBriqueDSN = parametrageBatch[ORDRE_PARAM_CODE_BRIQUE_DSN];
        this.codeApplication = parametrageBatch[ORDRE_PARAM_CODE_APPLICATION];
        this.numeroEnvironnement = parametrageBatch[ORDRE_PARAM_NUMERO_ENVIRONNEMENT];

        String nomFichier = this.horodatageFichier + "_" + this.codeApplication + this.codeBriqueDSN + "_" + "E" + this.numeroEnvironnement + "."
                + this.typeFichier.toLowerCase() + EXTENSION_FICHIER;

        this.cheminAbsoluFichierImport = cheminRepertoireFichiers + File.separator + nomFichier;
        this.emplacementRessourceFichierImport = tranformeEmplacementRessourceFichier(cheminAbsoluFichierImport);

        String cheminAbsoluFichierLogErreurs = parametrageBatch[ORDRE_PARAM_EMPLACEMENT_FICHIER_ERREURS];
        setCheminAbsoluFichierLogErreurs(cheminAbsoluFichierLogErreurs);
    }

    /**
     * Récupère le nom des entités importées.
     * 
     * @return le nom des entités importées
     */
    protected abstract String getNomEntitesImportees();

    /**
     * Récupère le référentiel de l'entité importée.
     * 
     * @return le référentiel de l'entité importée
     */
    protected abstract Class<? extends EntiteImportableRepository<? extends EntiteImportableBatch>> getReferentielEntitesImportees();

    @Override
    protected String getIdJobFlux4() {
        return getNomEntitesImportees();
    }

    @Override
    protected boolean lanceTraitementAvantJob(GenericXmlApplicationContext contexteExecutionJob) {

        // Validation de l'existence des fichiers
        if (!verifieExistencePhysiqueRessource(contexteExecutionJob, this.emplacementRessourceFichierImport)) {
            LOGGER.error("Fichier manquant pour démarrer le batch : {}", this.emplacementRessourceFichierImport);
            String messageErreur = MessageFormat.format(RedacMessages.ERREUR_FICHIER_INEXISTANT, this.cheminAbsoluFichierImport);
            traceErreurDansFichierLog(messageErreur);
            return false;
        }

        if (!testeSiFichierContientDonneesUtiles(contexteExecutionJob, this.emplacementRessourceFichierImport, 1)) {
            LOGGER.error("Le fichier ne peut pas être vide : {}", this.emplacementRessourceFichierImport);
            String mesageErreur = MessageFormat.format(RedacMessages.ERREUR_FICHIER_VIDE, this.cheminAbsoluFichierImport);
            traceErreurDansFichierLog(mesageErreur);
            return false;
        }

        videEspacesTemporaires(contexteExecutionJob, getReferentielEntitesImportees());

        return true;
    }

    @Override
    protected JobParameters initParametrageJob(GenericXmlApplicationContext contexteExecutionJob) {
        Map<String, JobParameter> parametresJob = new HashMap<String, JobParameter>();

        // Configuration lecteur du fichier à importer
        LOGGER.debug("Fichier entrée pris en compte : {}", this.emplacementRessourceFichierImport);
        parametresJob.put("fichierEntree", new JobParameter(this.emplacementRessourceFichierImport));

        // Configuration de l'emplacement du fichier de log
        LOGGER.debug("Fichier erreurs pris en compte : {}", getCheminAbsoluFichierLogErreurs());
        parametresJob.put("fichierLogErreurs", new JobParameter(getCheminAbsoluFichierLogErreurs()));

        return new JobParameters(parametresJob);
    }

    @Override
    protected void lanceTraitementFinJob(GenericXmlApplicationContext contexteExecutionJob, CodeRetour codeRetour, JobExecution executionJob) {
        if (codeRetour != CodeRetour.TERMINE && codeRetour != CodeRetour.ERREUR_FONCTIONNELLE_NON_BLOQUANTE) {
            videEspacesTemporaires(contexteExecutionJob, getReferentielEntitesImportees());
        }

        // Logge le message de réussite du traitement batch dans le fichier de log
        if (codeRetour == CodeRetour.TERMINE) {
            traceTraitementOKDansFichierLog("Traitement du flux 4 - " + getNomEntitesImportees() + " - réussi");
        } else if (codeRetour == CodeRetour.ERREUR_FONCTIONNELLE_NON_BLOQUANTE) {
            traceTraitementOKDansFichierLog("Traitement du flux 4 - " + getNomEntitesImportees() + " - réussi, avec erreur(s) non bloquante(s)");
        }

        LOGGER.info("Fin d'exécution du batch flux 4 - import des données {}", getNomEntitesImportees());
    }

}
