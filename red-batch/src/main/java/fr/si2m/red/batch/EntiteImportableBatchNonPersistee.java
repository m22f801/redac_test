/**
 * 
 */
package fr.si2m.red.batch;

import fr.si2m.red.EntiteImportableBatch;

/**
 * Définition d'une entité lisible en import via un batch REDAC mais jamais persistée en l'état dans le référentiel REDAC.
 * 
 * @author nortaina
 *
 */
public abstract class EntiteImportableBatchNonPersistee extends EntiteImportableBatch {
    /**
     * UID de version.
     */
    private static final long serialVersionUID = 6645997450345318199L;

    @Override
    public boolean isLigneEnCoursImportBatch() {
        return true;
    }

    @Override
    public void setLigneEnCoursImportBatch(boolean ligneEnCoursImportBatch) {
        // Rien
    }

    @Override
    public Object getId() {
        return null;
    }
}
