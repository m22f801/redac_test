/**
 * 
 */
package fr.si2m.red.batch.flux10.execution;

import fr.si2m.red.EntiteImportableBatch;
import fr.si2m.red.batch.moteur.execution.FacadeExecuteurBatch;
import fr.si2m.red.core.repository.EntiteImportableRepository;
import fr.si2m.red.parametrage.ParamSirenFaux;
import fr.si2m.red.parametrage.ParamSirenFauxRepository;

/**
 * Exécuteur du batch du flux 10 pour la table ParamCategoriesEffectifs.
 * 
 * @author eudesr
 *
 */
@FacadeExecuteurBatch(nomBatch = "flux10_SirenFaux")
public class Flux10ParamSirenFauxExecuteurBatch extends Flux10ExecuteurBatch {

    /**
     * Exécuteur de batch du flux 10 pour l'import de {@link ParamSirenFaux}.
     * 
     * @param parametrageBatch
     *            les paramètres d'exécution du batch - deux paramètres sont attendus en entrée :
     *            <ol>
     *            <li>le chemin absolu vers le fichier à importer</li>
     *            <li>le chemin absolu vers le fichier de logs à éventuellement renseigner</li>
     *            </ol>
     */
    public Flux10ParamSirenFauxExecuteurBatch(String[] parametrageBatch) {
        super(parametrageBatch);
    }

    /*
     * (non-Javadoc)
     * 
     * @see fr.si2m.red.batch.flux10.execution.Flux10ExecuteurBatch#getNomEntitesImportees()
     */
    @Override
    protected String getNomEntitesImportees() {
        return "ParamSirenFaux";
    }

    /*
     * (non-Javadoc)
     * 
     * @see fr.si2m.red.batch.flux10.execution.Flux10ExecuteurBatch#getReferentielEntitesImportees()
     */
    @Override
    protected Class<? extends EntiteImportableRepository<? extends EntiteImportableBatch>> getReferentielEntitesImportees() {
        return ParamSirenFauxRepository.class;
    }

}
