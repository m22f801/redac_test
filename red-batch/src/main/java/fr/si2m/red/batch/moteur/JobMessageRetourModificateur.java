package fr.si2m.red.batch.moteur;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.BatchStatus;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobExecutionListener;
import org.springframework.batch.core.annotation.AfterJob;
import org.springframework.batch.core.annotation.BeforeJob;

/**
 * Composant pouvant modifier le message de retour REDAC dans le contexte d'exécution d'un job.<br/>
 * <br/>
 * Le message du contexte d'exécution REDAC est constitué du code retour, suivi d'un caractère '|' puis d'un message éventuel (facultatif donc).<br/>
 * Fonctionne de pair avec {@link EtapeCodeRetourModificateur}.<br/>
 * <br/>
 * Ce composant doit IMPERATIVEMENT être placé en tant que listener d'étape job.
 * 
 * @author nortaina
 *
 * @see EtapeCodeRetourModificateur
 */
public class JobMessageRetourModificateur implements JobExecutionListener {
    private static final Logger LOGGER = LoggerFactory.getLogger(JobMessageRetourModificateur.class);

    @BeforeJob
    @Override
    public void beforeJob(JobExecution jobExecution) {
        // Rien
    }

    /**
     * Gestion de la mise à jour du message de retour de type REDAC.
     * 
     * @param jobExecution
     *            le contexte du job
     */
    @AfterJob
    @Override
    public void afterJob(JobExecution jobExecution) {
        LOGGER.debug("Mise à jour du message de retour REDAC...");

        CodeRetour codeRetourBatch;
        String messageRetourBatch;
        if (jobExecution.getStatus() == BatchStatus.COMPLETED) {
            // Job terminé
            CodeRetour codeRetourBatchForce = (CodeRetour) jobExecution.getExecutionContext().get(
                    EtapeCodeRetourModificateur.CLE_FORCE_STATUT_EXECUTION);
            codeRetourBatch = codeRetourBatchForce != null ? codeRetourBatchForce : CodeRetour.TERMINE;
            messageRetourBatch = codeRetourBatchForce == CodeRetour.ERREUR_FONCTIONNELLE_NON_BLOQUANTE ? "Terminé avec erreurs non bloquantes"
                    : "Terminé";
        } else {
            // Job interrompu / bloqué
            codeRetourBatch = CodeRetour.ERREUR_TECHNIQUE_BLOQUANTE;
            messageRetourBatch = "Erreur bloquante inconnue";
            List<Throwable> erreurs = jobExecution.getAllFailureExceptions();
            for (Throwable erreur : erreurs) {
                messageRetourBatch = erreur.getMessage();
                if (erreur instanceof ErreurFonctionnelleBloquanteException) {
                    codeRetourBatch = CodeRetour.ERREUR_FONCTIONNELLE_BLOQUANTE;
                    break;
                }
            }
        }

        LOGGER.debug("Mise à jour du code-message de retour REDAC sauvegardés en base en [{}|{}]", codeRetourBatch.getCode(), messageRetourBatch);
        jobExecution.setExitStatus(new ExitStatus(codeRetourBatch.getCodeAsString(), messageRetourBatch));
    }
}
