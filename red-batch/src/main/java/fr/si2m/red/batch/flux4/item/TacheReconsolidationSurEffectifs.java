package fr.si2m.red.batch.flux4.item;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;

import fr.si2m.red.batch.moteur.EtapeCodeRetourModificateur;
import fr.si2m.red.reconciliation.PeriodeRecueRepository;
import fr.si2m.red.reconciliation.RattachementDeclarationsRecuesRepository;
import lombok.Setter;

/**
 * Tâche de reconsolidation des éléments déclaratifs sur effectifs (CategorieQuittancementIndividu et EffectifCategorieMouvement seulement).
 * 
 * @author poidij
 *
 */
public class TacheReconsolidationSurEffectifs extends EtapeCodeRetourModificateur implements Tasklet {
    private static final Logger LOGGER = LoggerFactory.getLogger(TacheReconsolidationSurEffectifs.class);

    @Setter
    private RattachementDeclarationsRecuesRepository rattachementDeclarationsRecuesRepository;

    @Setter
    private PeriodeRecueRepository periodeRecueRepository;

    @Setter
    private String auditNomBatch;

    @Override
    public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) throws Exception {
        // Calcul des EffectifCategorieContratTravail : réalisé ailleurs

        // Calcul des CategorieQuittancementIndividu
        Map<String, Object> params = new HashMap<>();
        params.put("auditNomBatch", auditNomBatch);

        String sql = "call Inserer_CATEGORIE_QUITTANCEMENT_INDIVIDU_EFFECTIF(:auditNomBatch)";
        LOGGER.info("Appel de la procédure stockée Inserer_CATEGORIE_QUITTANCEMENT_INDIVIDU_EFFECTIF avec les paramètres {}", params);
        periodeRecueRepository.callMysqlProcedure(sql, params);
        LOGGER.info("Fin de la procédure stockée Inserer_CATEGORIE_QUITTANCEMENT_INDIVIDU_EFFECTIF");

        // Calcul des Tranchep_audit_nom_batch
        // F04_RG_P5_44 Calcule le nombre de mouvements pour une date de mouvement donnée - Vue : v_effectifs_mouvements
        sql = "call Inserer_EFFECTIF_CATEGORIE_MVT(:auditNomBatch)";
        LOGGER.info("Appel de la procédure stockée Inserer_EFFECTIF_CATEGORIE_MVT avec les paramètres {}", params);
        periodeRecueRepository.callMysqlProcedure(sql, params);
        LOGGER.info("Fin de la procédure stockée Inserer_EFFECTIF_CATEGORIE_MVT");

        return RepeatStatus.FINISHED;
    }

}
