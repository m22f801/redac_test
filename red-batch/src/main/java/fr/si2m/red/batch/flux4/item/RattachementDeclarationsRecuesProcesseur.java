package fr.si2m.red.batch.flux4.item;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.si2m.red.DateRedac;
import fr.si2m.red.RedacUnexpectedException;
import fr.si2m.red.batch.moteur.item.TransformateurDonneeAvecCollecte;
import fr.si2m.red.contrat.Contrat;
import fr.si2m.red.contrat.ContratRepository;
import fr.si2m.red.dsn.AdhesionEtablissementMois;
import fr.si2m.red.dsn.EtatPeriode;
import fr.si2m.red.reconciliation.HistoriqueEtatPeriode;
import fr.si2m.red.reconciliation.HistoriqueEtatPeriodeRepository;
import fr.si2m.red.reconciliation.PeriodeRecue;
import fr.si2m.red.reconciliation.PeriodeRecueRepository;
import fr.si2m.red.reconciliation.RattachementDeclarationsRecues;
import lombok.Setter;

/**
 * Processeur des lignes contrat-entreprise à exporter à partir de situations de contrats identifiées.
 * 
 * @author poidij
 *
 */
/**
 * @author Dev
 *
 */
/**
 * @author Dev
 *
 */
public class RattachementDeclarationsRecuesProcesseur
        extends TransformateurDonneeAvecCollecte<AdhesionEtablissementMois, List<RattachementDeclarationsRecues>> {

    private static final Logger LOGGER = LoggerFactory.getLogger(RattachementDeclarationsRecuesProcesseur.class);

    @Setter
    private PeriodeRecueRepository periodeRecueRepository;

    @Setter
    private HistoriqueEtatPeriodeRepository historiqueEtatPeriodeRepository;

    @Setter
    private ContratRepository contratRepository;

    @Setter
    private String cheminAbsoluFichierLogErreurs;

    private final String nomBatch;

    /**
     * Constructeur de processeur.
     * 
     * @param nomBatch
     *            le nom du batch utilisant ce processeur
     */
    public RattachementDeclarationsRecuesProcesseur(String nomBatch) {
        super();
        this.nomBatch = nomBatch;
    }

    @Override
    public List<RattachementDeclarationsRecues> process(AdhesionEtablissementMois adhesion) {
        RattachementDeclarationsRecues rattachementDeclarationsRecues;

        // F04_RG_P4_01 : Identification de la période de rattachement
        Integer moisRattachement = adhesion.getMoisRattachement();
        Integer moisDeclare = adhesion.getMoisDeclare();
        String referenceContrat = adhesion.getReferenceContrat();
        String idAdhEtabMois = adhesion.getIdAdhEtabMois();

        try {
            Integer dateDebutPeriodePaye = calculeDateDebutPeriode(referenceContrat, moisDeclare, idAdhEtabMois);
            Integer dateFinPeriodePaye = calculeDateFinPeriode(referenceContrat, moisDeclare, idAdhEtabMois);

            if (moisRattachement.intValue() != moisDeclare.intValue()
                    && (moisRattachement < dateDebutPeriodePaye || dateFinPeriodePaye < moisRattachement)) {
                PeriodeRecue periodeRegularisee = periodeRecueRepository.existePeriodePourRattachement(PeriodeRecue.TYPE_PERIODE_REGULARISATION,
                        referenceContrat, moisRattachement, EtatPeriode.RCP);

                if (periodeRegularisee != null) {
                    // Rattacher Adhésion à cette période
                    rattachementDeclarationsRecues = creeRattachement(adhesion, periodeRegularisee);
                } else {
                    // Créer une Période de type REG
                    PeriodeRecue periodeCree = creerPeriode(adhesion, PeriodeRecue.TYPE_PERIODE_REGULARISATION);

                    // Rattacher l'Adhésion à la Période
                    rattachementDeclarationsRecues = creeRattachement(adhesion, periodeCree);
                }
            } else {
                rattachementDeclarationsRecues = processAdhesionPasEnRegularisation(adhesion);
            }

            List<RattachementDeclarationsRecues> rattachements = new ArrayList<RattachementDeclarationsRecues>();
            rattachements.add(rattachementDeclarationsRecues);

            return rattachements;
        } catch (Exception e) {
            LOGGER.error("Erreur détectée lors du rattachement des périodes", e);
            return new ArrayList<RattachementDeclarationsRecues>(0);
        }

    }

    /**
     * Effectue le traitement d'une Adhésion qui n'est pas en régularisation.
     * 
     * @param adhesion
     *            l'adhésion
     */
    private RattachementDeclarationsRecues processAdhesionPasEnRegularisation(AdhesionEtablissementMois adhesion) {
        RattachementDeclarationsRecues rattachementDeclarationsRecues;

        PeriodeRecue periodeDeclaree = periodeRecueRepository.existePeriodePourRattachement(PeriodeRecue.TYPE_PERIODE_DECLARATION,
                adhesion.getReferenceContrat(), adhesion.getMoisRattachement());
        if (periodeDeclaree != null) {
            if (StringUtils.equals(periodeDeclaree.getEtatPeriode().name(), EtatPeriode.RCP.getCodeEtat())) {
                // Rattacher Adhésion à cette période
                rattachementDeclarationsRecues = creeRattachement(adhesion, periodeDeclaree);
            } else {
                rattachementDeclarationsRecues = processAdhesionPasEnDeclarationRecue(adhesion);
            }
        } else {
            // Créer une Période de type DEC
            PeriodeRecue periodeCree = creerPeriode(adhesion, PeriodeRecue.TYPE_PERIODE_DECLARATION);

            // Rattacher l'Adhésion à la Période
            rattachementDeclarationsRecues = creeRattachement(adhesion, periodeCree);

        }

        return rattachementDeclarationsRecues;
    }

    /**
     * Effectue le traitement d'une Adhésion qui n'est ni en régularisation, ni en déclaration recue
     * 
     * @param adhesion
     */
    private RattachementDeclarationsRecues processAdhesionPasEnDeclarationRecue(AdhesionEtablissementMois adhesion) {
        RattachementDeclarationsRecues rattachementDeclarationsRecues;

        PeriodeRecue periodeComplement = periodeRecueRepository.existePeriodePourRattachement(PeriodeRecue.TYPE_PERIODE_COMPLEMENT,
                adhesion.getReferenceContrat(), adhesion.getMoisRattachement(), EtatPeriode.RCP);
        if (periodeComplement != null) {
            // Rattacher Adhésion à cette période
            rattachementDeclarationsRecues = creeRattachement(adhesion, periodeComplement);

        } else {
            // Créer une Période de type CPL
            PeriodeRecue periodeCree = creerPeriode(adhesion, PeriodeRecue.TYPE_PERIODE_COMPLEMENT);

            // Rattacher l'Adhésion à la Période
            rattachementDeclarationsRecues = creeRattachement(adhesion, periodeCree);
        }

        return rattachementDeclarationsRecues;
    }

    /**
     * Crée une période F04_RG_P4_06
     * 
     * @param referenceContrat
     *            la référence du contrat pour lequel on crée la période
     * @param typePeriode
     *            le type de la période à créer
     * 
     */
    private PeriodeRecue creerPeriode(AdhesionEtablissementMois adhesion, String typePeriode) {
        PeriodeRecue nouvellePeriode = new PeriodeRecue();

        nouvellePeriode.setTypePeriode(typePeriode);
        nouvellePeriode.setDateCreation(DateRedac.convertitEnDateRedac(new Date()));
        nouvellePeriode.setNumeroContrat(StringUtils.substring(adhesion.getReferenceContrat(), 0, 15));

        // Détermination des dates de début et de fin de période
        nouvellePeriode.setDateDebutPeriode(
                calculeDateDebutPeriode(adhesion.getReferenceContrat(), adhesion.getMoisRattachement(), adhesion.getIdAdhEtabMois()));
        nouvellePeriode.setDateFinPeriode(
                calculeDateFinPeriode(adhesion.getReferenceContrat(), adhesion.getMoisRattachement(), adhesion.getIdAdhEtabMois()));

        // Autres champs
        nouvellePeriode.setPremierMoisDecl(false);
        nouvellePeriode.setEtatPeriode(EtatPeriode.RCP);
        nouvellePeriode.setReconsolider(true);
        nouvellePeriode.setDateEcheance(0);

        nouvellePeriode.setAffecteA(periodeRecueRepository.getAffecteADeLaPeriodeAnterieurePourMemeContrat(nouvellePeriode.getNumeroContrat()));
        nouvellePeriode.setATraiterPar(nouvellePeriode.getAffecteA());
        nouvellePeriode.setTraitePar(null);

        nouvellePeriode.setAuditUtilisateurCreation(this.nomBatch);

        PeriodeRecue periodeCree = periodeRecueRepository
                .create(calculeDateEcheance(adhesion.getDateEcheance(), adhesion.getIdAdhEtabMois(), nouvellePeriode));
        creerHistoriqueEtatPeriode(periodeCree);

        return periodeCree;
    }

    /**
     * Calcule la périodicité d'un contrat à partir du numeroContrat et du moisRattachement
     * 
     * @param numeroContrat
     *            le numéro de contrat de la référence contrat de l'adhésion à rattacher à la période
     * @param moisRattachement
     *            le mois de rattachement de l'adhésion à rattacher à la période
     * @return la date de début de période calculée
     */
    private Integer calculePeriodiciteContrat(String numeroContrat, Integer moisRattachement, String idAdhEtabMois) {
        Contrat contrat = contratRepository.getContratValidePourDate(numeroContrat, moisRattachement);

        // Périodicité en mois, calculée à partir du champ codeFractionnement du contrat
        Integer periodicite = 3;

        if (contrat == null) {
            String mesageErreur = "La situation contrat [numContrat=" + numeroContrat + ", moisRattachement=(" + moisRattachement
                    + ")] n'existe pas. " + "Adhésion traitée : [AdhesionEtablissementMois(idAdhEtabMois= " + idAdhEtabMois + ")]";
            traceErreurSansBlocage(mesageErreur, cheminAbsoluFichierLogErreurs);
            throw new RedacUnexpectedException(
                    "La situation contrat [numContrat=" + numeroContrat + ", moisRattachement=(" + moisRattachement + ")] n'existe pas");
        }

        final String codeFractionnement = contrat.getCodeFractionnement();
        if (codeFractionnement != null) {
            switch (codeFractionnement) {
            case "M":
                periodicite = 1;
                break;
            case "T":
                periodicite = 3;
                break;
            case "S":
                periodicite = 6;
                break;
            case "A":
                periodicite = 12;
                break;
            default:
            }
        }
        return periodicite;
    }

    /**
     * Calcule la date de début de période à partir de la periodicite et du moisPivot (= moisRattachement ou moisDeclare selon le besoin)
     * 
     * @param periodicite
     *            la périodicité du contrat de référence de l'adhésion à rattacher à la période
     * @param moisPivot
     *            le mois pour lequel la période est calculée
     * @return la date de début de période calculée
     */
    private Integer calculeDateDebutPeriode(String referenceContrat, Integer moisPivot, String idAdhEtabMois) {

        Integer periodicite = calculePeriodiciteContrat(referenceContrat, moisPivot, idAdhEtabMois);
        Integer moisCible = DateRedac.selectionnePartieDate(moisPivot, DateRedac.MODIFIER_MOIS);
        Integer anneeCible = DateRedac.selectionnePartieDate(moisPivot, DateRedac.MODIFIER_ANNEE);

        Integer moisDebutPeriode = (int) (Math.floor((double) (moisCible - 1) / periodicite)) * periodicite + 1;
        return (anneeCible * 10000) + (moisDebutPeriode * 100) + 1;
    }

    /**
     * Calcule la date de fin de période à partir de la periodicite et du moisRattachement
     * 
     * @param periodicite
     *            la périodicité du contrat de référence de l'adhésion à rattacher à la période
     * @param moisPivot
     *            le mois pour lequel la période est calculée
     * @return la date de début de période calculée
     */
    private Integer calculeDateFinPeriode(String referenceContrat, Integer moisPivot, String idAdhEtabMois) {

        Integer periodicite = calculePeriodiciteContrat(referenceContrat, moisPivot, idAdhEtabMois);
        Integer moisCible = DateRedac.selectionnePartieDate(moisPivot, DateRedac.MODIFIER_MOIS);
        Integer anneeCible = DateRedac.selectionnePartieDate(moisPivot, DateRedac.MODIFIER_ANNEE);

        // Détermination du mois de fin de période
        Integer moisFinPeriode = (int) Math.ceil((double) moisCible / periodicite) * periodicite;

        // Détermination du dernier jour de ce mois pour l'année de rattachement
        Calendar calendar = Calendar.getInstance();
        calendar.set(anneeCible, moisFinPeriode - 1, 1);
        calendar.set(Calendar.DATE, calendar.getActualMaximum(Calendar.DATE));
        int dernierJour = calendar.get(Calendar.DATE);

        // Détermination de la date complète
        return (anneeCible * 10000) + (moisFinPeriode * 100) + dernierJour;
    }

    /**
     * Crée un historique pour la période periode
     * 
     * @param periode
     */
    private void creerHistoriqueEtatPeriode(PeriodeRecue periode) {
        HistoriqueEtatPeriode nouvelHistoriqueEtatPeriode = new HistoriqueEtatPeriode();

        nouvelHistoriqueEtatPeriode.setIdPeriode(periode.getIdPeriode());
        nouvelHistoriqueEtatPeriode.setDateHeureChangement(DateRedac.convertitEnDateHeureRedac(Calendar.getInstance().getTime()));
        nouvelHistoriqueEtatPeriode.setEtatPeriode(EtatPeriode.RCP);
        nouvelHistoriqueEtatPeriode.setOrigineChangement("RDC");

        nouvelHistoriqueEtatPeriode.setAuditUtilisateurCreation(this.nomBatch);

        historiqueEtatPeriodeRepository.create(nouvelHistoriqueEtatPeriode);
    }

    /**
     * Modifie la Période periode avant le rattachement avec l'Adhésion adhesion
     * 
     * @param adhesion
     *            l'adhésion à rattacher
     * @param periode
     *            la période à laquelle rattacher l'adhésion
     */
    private void modifiePeriodePourRattachement(AdhesionEtablissementMois adhesion, PeriodeRecue periode) {
        // F04_RG_P4_02
        if (StringUtils.equals(periode.getTypePeriode(), PeriodeRecue.TYPE_PERIODE_DECLARATION)
                && Integer.compare(periode.getDateDebutPeriode(), adhesion.getMoisDeclare()) == 0) {
            periode.setPremierMoisDecl(true);
        }

        // F04_RG_P4_04
        periode.setReconsolider(true);

        periodeRecueRepository
                .modifieEntitesExistantes(Arrays.asList(calculeDateEcheance(adhesion.getDateEcheance(), adhesion.getIdAdhEtabMois(), periode)));
    }

    /**
     * Recalcule la date d'échéance pour la Période periode sur la base d'une date d'échéance d'adhésion
     * 
     * @param dateEcheanceAdhesion
     *            la date d'échéance d'adhésion
     * @param periode
     *            la période à laquelle rattacher l'adhésion dont la date est fournie
     * @return la période avec la date d'échéance recalculée
     */
    private PeriodeRecue calculeDateEcheance(Integer dateEcheanceAdhesion, String idAdhEtabMois, PeriodeRecue periode) {
        // Cas1: Creation d'une nouvelle periode => dateEcheance=0
        if (periode.getIdPeriode() == null) {
            periode.setDateEcheance(0);
        } else {
            // Cas2: Rattachement de la declaration a la periode => recalcul de la dateEcheance
            // F04_RG_P4_03
            String jourDateEcheance = StringUtils.substring(dateEcheanceAdhesion.toString(), 6);
            Integer dateEcheanceCalculee = validePlageEtFormatDateEcheanceAdhesion(jourDateEcheance, dateEcheanceAdhesion, idAdhEtabMois, periode);

            if (periode.getDateEcheance() == null || periode.getDateEcheance() < dateEcheanceCalculee) {
                periode.setDateEcheance(dateEcheanceCalculee);
            }
        }
        return periode;
    }

    /**
     * Rattache l'Adhésion adhesion à la Période periode
     * 
     * @param adhesion
     *            l'adhésion à rattacher
     * @param periode
     *            la période à laquelle rattacher l'adhésion
     */
    private RattachementDeclarationsRecues creeRattachement(AdhesionEtablissementMois adhesion, PeriodeRecue periode) {
        // F04_RG_P4_05
        modifiePeriodePourRattachement(adhesion, periode);

        RattachementDeclarationsRecues rattachement = new RattachementDeclarationsRecues();
        rattachement.setIdAdhEtabMois(adhesion.getIdAdhEtabMois());
        rattachement.setIdPeriode(periode.getIdPeriode());

        rattachement.setAuditUtilisateurCreation(this.nomBatch);

        return rattachement;
    }

    /**
     * @param jourDateEcheance
     *            le jourDateEcheance a etre controlee
     * @param dateEcheanceAdhesion
     *            la date echeance a etre valide pour l'adhesion
     * @param idAdhEtabMois
     *            l'id de l'adhesion etablissement mois pour laquelle la date echeance est en train d'etre calcule
     * @param periode
     *            la periode rattache a cette adhesion
     * @return le la nouvelle date echeance calculee
     */
    private Integer validePlageEtFormatDateEcheanceAdhesion(String jourDateEcheance, Integer dateEcheanceAdhesion, String idAdhEtabMois,
            PeriodeRecue periode) {
        String mesageErreur = "";
        String jourDateEcheanceValide = "";
        boolean erreurDansCalcul = false;

        // Verification format dateEcheanceAdhesion
        Integer dateEcheanceAdhesionValide = valideChampDateAvecCollecte(dateEcheanceAdhesion, DateRedac.FORMAT_DATES);

        // Forcer la date a etre 15 dans la cas ou elle n'est pas dans la plage entre 1 et 28 ou le format n'est pas bonne
        if ((28 < Integer.parseInt(jourDateEcheance)) || (Integer.parseInt(jourDateEcheance) < 1) || (dateEcheanceAdhesionValide == null)) {
            jourDateEcheanceValide = Integer.toString(15);
            erreurDansCalcul = true;
        } else {
            jourDateEcheanceValide = jourDateEcheance;
        }

        Integer datePlusUnMois = DateRedac.ajouterALaDate(periode.getDateFinPeriode(), 1, DateRedac.MODIFIER_MOIS, "DT_FIN_PERIODE");
        String moisAnneeDateEcheance = StringUtils.substring(datePlusUnMois.toString(), 0, 6);
        Integer dateEcheanceCalculee = Integer.parseInt(moisAnneeDateEcheance + jourDateEcheanceValide);

        // Si le jourDateEcheanceValide etait recalculee a cause d'une mauvaise format ou a cause du jour que n'est pas entre 1 et 28 => Code erreur 20
        if (erreurDansCalcul) {
            // Traiter le cas de la creation en cours de la periode
            mesageErreur = "Erreur lors du calcul de la dateEcheance à partir de l'adhésion [AdhesionEtablissementMois(idAdhEtabMois=" + idAdhEtabMois
                    + ", dateEcheance=" + dateEcheanceAdhesion + ")]  pour la période [idPeriode=" + periode.getIdPeriode() + ", numContrat= "
                    + periode.getNumeroContrat() + ", typePeriode=" + periode.getTypePeriode() + ", dateDebutPeriode="
                    + periode.getDateDebutPeriode().toString() + ", dateFinPeriode=" + periode.getDateFinPeriode().toString() + ", dateCreation="
                    + periode.getDateCreation().toString() + "]. DateEcheance calculée : " + dateEcheanceCalculee;

            traceErreurSansBlocage(mesageErreur, cheminAbsoluFichierLogErreurs);
        }

        return dateEcheanceCalculee;
    }
}
