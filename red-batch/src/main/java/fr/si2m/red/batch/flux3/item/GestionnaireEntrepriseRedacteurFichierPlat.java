package fr.si2m.red.batch.flux3.item;

/**
 * Rédacteur du corps du fichier GESTIONNAIRE.
 * 
 * @author benitahy
 *
 * @param <T>
 *            le type de l'entité à éditer dans le fichier rédigé
 */
public class GestionnaireEntrepriseRedacteurFichierPlat<T> extends RedacteurFichierPlatAvecSauvegardeNbLignes<T> {

    /**
     * Constructeur par défaut pour le rédacteur du fichier ENTREPRISE.
     * 
     * @param nomsProprietesExtraites
     *            les noms des propriétés extraites telles quelles dans l'entité
     * @param format
     *            le format d'écriture des propriétés extraites
     */
    public GestionnaireEntrepriseRedacteurFichierPlat(String[] nomsProprietesExtraites, String format) {
        super(nomsProprietesExtraites, format);
        setShouldDeleteIfExists(true);
    }

    @Override
    public String getCleContexteNbLignes() {
        return "nbGestionnaires";
    }
}
