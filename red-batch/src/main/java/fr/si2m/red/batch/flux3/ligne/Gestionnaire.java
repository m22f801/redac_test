package fr.si2m.red.batch.flux3.ligne;

import lombok.Data;

/**
 * Description d'une ligne d'un fichier GESTIONNAIRES à exporter.
 * 
 * @author nortaina
 *
 */
@Data
public class Gestionnaire {
    /**
     * Code Mise à jour.
     * 
     * @param codeMiseAJour
     *            Code Mise à jour
     * @return Code Mise à jour
     */
    private String codeMiseAJour;

    /**
     * Référence à l’identifiant technique de l’entreprise (n° de séquence local à une instance de cette interface).
     * 
     * @param identifiantTechniqueEntreprise
     *            Référence à l’identifiant technique de l’entreprise (n° de séquence local à une instance de cette interface)
     * @return Référence à l’identifiant technique de l’entreprise (n° de séquence local à une instance de cette interface)
     */
    private String identifiantTechniqueEntreprise;

    /**
     * Type de maille gestionnaire.
     * 
     * @param typeMaille
     *            Type de maille gestionnaire
     * @return Type de maille gestionnaire
     */
    private Integer typeMaille;

    /**
     * Référence de la maille gestionnaire.
     * 
     * @param referenceMaille
     *            Référence de la maille gestionnaire
     * @return Référence de la maille gestionnaire
     */
    private String referenceMaille;

    /**
     * Type d'intervenant.
     * 
     * @param typeIntervenant
     *            Type d'intervenant
     * @return Type d'intervenant
     */
    private String typeIntervenant;

    /**
     * Référence gestionnaire.
     * 
     * @param referenceGestionnaire
     *            Référence gestionnaire
     * @return Référence gestionnaire
     */
    private String referenceGestionnaire;

    /**
     * Libellé.
     * 
     * @param libelle
     *            Libellé
     * @return Libellé
     */
    private String libelle;

    /**
     * Prénom du gestionnaire de compte.
     * 
     * @param prenomGestionnaireCompte
     *            Prénom du gestionnaire de compte
     * @return Prénom du gestionnaire de compte
     */
    private String prenomGestionnaireCompte;

    /**
     * Nom d’usage du gestionnaire.
     * 
     * @param nomGestionnaireCompte
     *            Nom d’usage du gestionnaire
     * @return Nom d’usage du gestionnaire
     */
    private String nomGestionnaireCompte;

    /**
     * Unité de gestion.
     * 
     * @param uniteGestion
     *            Unité de gestion
     * @return Unité de gestion
     */
    private String uniteGestion;

    /**
     * Service.
     * 
     * @param service
     *            Service
     * @return Service
     */
    private String service;

    /**
     * Centre de gestion.
     * 
     * @param centreGestion
     *            Centre de gestion
     * @return Centre de gestion
     */
    private String centreGestion;

    /**
     * Référence à l’identifiant technique contrat.
     * 
     * @param identifiantTechniqueContrat
     *            Référence à l’identifiant technique contrat
     * @return Référence à l’identifiant technique contrat
     */
    private String identifiantTechniqueContrat;

}
