package fr.si2m.red.batch.flux78.item;

import java.io.IOException;
import java.io.Writer;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import lombok.Setter;

import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.file.FlatFileFooterCallback;

import fr.si2m.red.DateRedac;

/**
 * Ce rédacteur d'en-tête est un FlatFileFooterCallback car la ligne d'en-ête doit contenir le nombre de ligne de données : on écrit donc l'en-ête dans un
 * fichier à part, puis on concatène les données dans une étape supplémentaire (cf le wiki REDAC).
 * 
 * @author poidij
 *
 * @param <T>
 *            le type de donnée publiée
 */
public class RedacteurEnTetePublicationWQUI<T> implements ItemWriter<T>, FlatFileFooterCallback {

    @Setter
    private ItemWriter<T> delegate;

    private int nbDeLignes = 0;
    private Set<String> couples = new HashSet<String>();

    @Override
    public void writeFooter(Writer writer) throws IOException {

        int nbCouples = couples.isEmpty() ? 0 : couples.size();

        String nbDeLignesStr = String.valueOf(nbDeLignes);
        while (nbDeLignesStr.length() < 8) {
            nbDeLignesStr = "0" + nbDeLignesStr;
        }

        String nbCouplesStr = String.valueOf(nbCouples);
        while (nbCouplesStr.length() < 8) {
            nbCouplesStr = "0" + nbCouplesStr;
        }

        SimpleDateFormat sdf = new SimpleDateFormat(DateRedac.FORMAT_DATES);
        String date = sdf.format(Calendar.getInstance().getTime());

        StringBuilder filler = new StringBuilder();
        while (filler.length() < 222) {
            filler.append(" ");
        }

        // longueurs : 1|3|8|8|8|222
        writer.write("1" + "DSN" + nbDeLignesStr + nbCouplesStr + date + filler.toString() + "\r\n");
    }

    @SuppressWarnings("unchecked")
    @Override
    public void write(List<? extends T> items) throws Exception {
        for (Object item : items) {
            if (item instanceof Collection<?>) {
                nbDeLignes += ((Collection<?>) item).size();

                Iterator<PeriodePublieeWQUI> itPeriodes = ((Collection<PeriodePublieeWQUI>) item).iterator();
                while (itPeriodes.hasNext()) {
                    PeriodePublieeWQUI itemTraite = itPeriodes.next();
                    getCouple(itemTraite);
                }
            } else {
                nbDeLignes += 1;
                getCouple((PeriodePublieeWQUI) item);
            }
        }

        delegate.write(items);
    }

    /**
     * Enregistre le couple contrat/periode s'il n'est pas déjà enregistré.
     * 
     * @param item
     *            la période publiée pour WQUI
     */
    public void getCouple(PeriodePublieeWQUI item) {
        String periode = item.getIdPeriode().toString();
        String contrat = item.getContratZoneA() + item.getContratZoneB() + item.getContratZoneC();

        couples.add(contrat + "/" + periode);
    }

}
