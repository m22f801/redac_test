package fr.si2m.red.batch.flux3.item;

import fr.si2m.red.batch.flux3.ligne.DelegatairePrestation;
import fr.si2m.red.batch.moteur.item.TransformateurDonnee;
import fr.si2m.red.contrat.Intermediaire;

/**
 * Mapper des entités DelegatairePrestation à partir de la SituationContrat fournie.
 * 
 * cf F03_RG_S11
 * 
 * @author poidij
 *
 */
public class TransformateurDelegatairePrestation extends TransformateurDonnee<Intermediaire, DelegatairePrestation> {

    @Override
    public DelegatairePrestation process(Intermediaire intermediaire) throws Exception {
        DelegatairePrestation delegatairePrestation = new DelegatairePrestation();

        delegatairePrestation.setCodeMiseAJour("R");
        delegatairePrestation.setIdentifiantTechniqueDelegatairePrestation(intermediaire.getCodeIntermediaire());
        delegatairePrestation.setDateDebutApplicationCaracteristiques(20000101);
        delegatairePrestation.setCodeOrganismeDelegatairePrestation(intermediaire.getCodeIntermediaire());
        delegatairePrestation.setLibelleDelegatairePrestation(intermediaire.getRaisonSociale());

        return delegatairePrestation;
    }

}
