package fr.si2m.red.batch.flux4.item;

import lombok.Setter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;

import fr.si2m.red.batch.moteur.EtapeCodeRetourModificateur;
import fr.si2m.red.dsn.AdhesionEtablissementMois;
import fr.si2m.red.dsn.AdhesionEtablissementMoisRepository;

/**
 * Tâche de nettoyage des {@link AdhesionEtablissementMois} correspondant à des DSN de test ou à des contrats délégués.
 * 
 * @author nortaina
 *
 */
public class TacheNettoyageAdhesionEtablissementMois extends EtapeCodeRetourModificateur implements Tasklet {
    private static final Logger LOGGER = LoggerFactory.getLogger(TacheNettoyageAdhesionEtablissementMois.class);

    @Setter
    private AdhesionEtablissementMoisRepository adhesionEtablissementMoisRepository;

    @Override
    public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) throws Exception {
        int adhesionsSupprimees = adhesionEtablissementMoisRepository.supprimeAdhesionsDeTestOuDeContratsDelegues();
        LOGGER.info("Suppression effective de {} adhésions-établissement-mois de test ou correspondant à des contrats délégués", adhesionsSupprimees);
        return RepeatStatus.FINISHED;
    }
}
