package fr.si2m.red.batch.flux10.execution;

import fr.si2m.red.EntiteImportableBatch;
import fr.si2m.red.batch.moteur.execution.FacadeExecuteurBatch;
import fr.si2m.red.core.repository.EntiteImportableRepository;
import fr.si2m.red.parametrage.ParamCategorieEffectifs;
import fr.si2m.red.parametrage.ParamCategorieEffectifsRepository;

/**
 * Exécuteur du batch du flux 10 pour la table ParamCategoriesEffectifs.
 * 
 * @author delortj
 *
 */
@FacadeExecuteurBatch(nomBatch = "flux10_CategoriesEffectifs")
public class Flux10ParamCategoriesEffectifsExecuteurBatch extends Flux10ExecuteurBatch {

    /**
     * Exécuteur de batch du flux 10 pour l'import de {@link ParamCategorieEffectifs}.
     * 
     * @param parametrageBatch
     *            les paramètres d'exécution du batch - deux paramètres sont attendus en entrée :
     *            <ol>
     *            <li>le chemin absolu vers le fichier à importer</li>
     *            <li>le chemin absolu vers le fichier de logs à éventuellement renseigner</li>
     *            </ol>
     */
    public Flux10ParamCategoriesEffectifsExecuteurBatch(String[] parametrageBatch) {
        super(parametrageBatch);
    }

    @Override
    protected String getNomEntitesImportees() {
        return "ParamCategoriesEffectifs";
    }

    @Override
    protected Class<? extends EntiteImportableRepository<? extends EntiteImportableBatch>> getReferentielEntitesImportees() {
        return ParamCategorieEffectifsRepository.class;
    }

}
