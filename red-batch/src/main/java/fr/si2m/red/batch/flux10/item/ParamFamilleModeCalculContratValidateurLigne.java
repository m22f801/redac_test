package fr.si2m.red.batch.flux10.item;

import fr.si2m.red.batch.moteur.erreur.ValidateurLigneAvecCollecte;
import fr.si2m.red.parametrage.ParamFamilleModeCalculContrat;

/**
 * Validateur de ligne .
 * 
 * @author poidij
 *
 */
public class ParamFamilleModeCalculContratValidateurLigne extends ValidateurLigneAvecCollecte<ParamFamilleModeCalculContrat> {

    @Override
    protected void valide(final ParamFamilleModeCalculContrat familleModeCalculContrat) {
        // Champs obligatoires

        valideChampAvecCollecte(familleModeCalculContrat.getNumFamille() != null, familleModeCalculContrat, "NOFAM",
                "le champ NOFAM obligatoire, n’est pas renseigné");

        valideChampAvecCollecte(familleModeCalculContrat.getModeCalculCotisation() != null, familleModeCalculContrat, "MOCALCOT",
                "le champ MOCALCOT obligatoire, n’est pas renseigné");

        // Longueur des champs

        // on vérifie que le nombre NOFAM a au plus 3 chiffres
        if (familleModeCalculContrat.getNumFamille() != null) {
            valideChampAvecCollecte(familleModeCalculContrat.getNumFamille() < 1000, familleModeCalculContrat, "NOFAM",
                    "le champ NOFAM dépasse la taille maximale prévue");
        }

        // on vérifie que le nombre MOCALCOT a au plus 1 chiffre
        if (familleModeCalculContrat.getModeCalculCotisation() != null) {
            valideChampAvecCollecte(familleModeCalculContrat.getModeCalculCotisation() < 10, familleModeCalculContrat, "MOCALCOT",
                    "le champ MOCALCOT dépasse la taille maximale prévue");
        }

        valideChampAvecCollecte(familleModeCalculContrat.getModeControleTypesBasesAssujetties().length() <= 3, familleModeCalculContrat,
                "CTL_TYPE_BASE_ASSUJ", "le champ CTL_TYPE_BASE_ASSUJ dépasse la taille maximale prévue");

        // F10_RG_C106
        valideChampAvecCollecte(familleModeCalculContrat.getModeDeclaration().length() <= 1, familleModeCalculContrat, "DONNEES_A_INTEGRER",
                "le champ DONNEES_A_INTEGRER dépasse la taille maximale prévue");
    }
}
