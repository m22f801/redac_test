package fr.si2m.red.batch.flux1.item;

import org.apache.commons.lang3.StringUtils;

import fr.si2m.red.DateRedac;
import fr.si2m.red.batch.moteur.erreur.ValidateurLigneAvecCollecte;
import fr.si2m.red.contrat.Tarif;
import fr.si2m.red.parametrage.ParamCategorieEffectifsRepository;
import fr.si2m.red.parametrage.ParamNatureBaseRepository;
import lombok.Setter;

/**
 * Validateur de ligne tarif.
 * 
 * @author delortjouvesf
 *
 */
public class TarifValidateurLigne extends ValidateurLigneAvecCollecte<Tarif> {

    @Setter
    private ParamCategorieEffectifsRepository paramCategorieEffectifsRepository;

    @Setter
    private ParamNatureBaseRepository paramNatureBaseRepository;

    private static final String CHAMP_NOCAT = "NOCAT";

    /**
     * Mode de chargement "normal" ou "populationEtendue"
     */
    @Setter
    private String paramModeChargement;

    @Override
    protected void valide(Tarif tarif) {

        // champs obligatoires
        // F01_RG_CT02
        valideChampAvecCollecte(StringUtils.isNotBlank(tarif.getNumContrat()), tarif, "NOCO", "le champ NOCO obligatoire, n’est pas renseigné");
        // F01_RG_CT03
        valideChampAvecCollecte(StringUtils.isNotBlank(tarif.getNumCategorie()), tarif, CHAMP_NOCAT,
                "le champ NOCAT obligatoire, n’est pas renseigné");

        // F01_RG_CT18
        tarif.setDateDebutSituationLigne(
                valideChampDateAvecCollecte(tarif.getDateDebutSituationLigne(), DateRedac.FORMAT_DATES, tarif, "DT_DEBUT_SIT", true));

        // longueur et formats des champs

        // F01_RG_CT02
        valideTailleMaximaleChampTexteAvecCollecte(tarif.getNumContrat(), 15, tarif, "NOCO");

        // F01_RG_CT03
        if (StringUtils.equals("normal", paramModeChargement)) {
            valideTailleMaximaleChampTexteAvecCollecte(tarif.getNumCategorie(), 3, tarif, CHAMP_NOCAT);
        } else {
            valideTailleMaximaleChampTexteAvecCollecte(tarif.getNumCategorie(), 30, tarif, CHAMP_NOCAT);
        }

        // F01_RG_CT04
        valideTailleMaximaleChampAvecCollecte(tarif.getEtatContrat(), 2, tarif, "COETACO");

        // F01_RG_CT05
        tarif.setDateModificationQuittancement(
                valideChampDateAvecCollecte(tarif.getDateModificationQuittancement(), DateRedac.FORMAT_DATES, tarif, "DTMAJQUI", false));

        // F01_RG_CT06
        valideTailleMaximaleChampAvecCollecte(tarif.getModeCalculCotisations(), 1, tarif, "MOCALCOT");

        // F01_RG_CT07
        valideTailleMaximaleChampAvecCollecte(tarif.getModeCalculCotisationsUniques(), 1, tarif, "MOCALCCU");

        // F01_RG_CT08
        valideTailleMaximaleChampAvecCollecte(tarif.getCodeNaturePlafond(), 2, tarif, "CONATPLA");

        // F01_RG_CT09
        if (StringUtils.equals("normal", paramModeChargement)) {
            valideTailleMaximaleChampTexteAvecCollecte(tarif.getLibelleCategorie(), 10, tarif, "LICAT");
        } else {
            valideTailleMaximaleChampTexteAvecCollecte(tarif.getLibelleCategorie(), 60, tarif, "LICAT");
        }

        // F01_RG_CT10
        valideTailleMaximaleChampAvecCollecte(tarif.getTauxCalculCotisationsUniques(), 8, tarif, "TXCALCU");

        // F01_RG_CT11
        valideTailleMaximaleChampAvecCollecte(tarif.getMontantCotisationsUniques(), 16, tarif, "MTCUNCU");

        // F01_RG_CT12
        valideTailleMaximaleChampAvecCollecte(tarif.getNatureTaux(), 2, tarif, "CONATTAU");

        // F01_RG_CT13
        valideTailleMaximaleChampAvecCollecte(tarif.getTauxBase1(), 8, tarif, "TXBASE1");

        // F01_RG_CT14
        valideTailleMaximaleChampAvecCollecte(tarif.getTauxBase2(), 8, tarif, "TXBASE2");

        // F01_RG_CT15
        valideTailleMaximaleChampAvecCollecte(tarif.getTauxBase3(), 8, tarif, "TXBASE3");

        // F01_RG_CT16
        valideTailleMaximaleChampAvecCollecte(tarif.getTauxBase4(), 8, tarif, "TXBASE4");

        // F01_RG_CT17
        valideTailleMaximaleChampAvecCollecte(tarif.getNatureBaseCotisations(), 2, tarif, "CONBCOT");

        if (verifieDateVide(tarif.getDateModificationQuittancement())) {
            tarif.setDateModificationQuittancement(null);
        }

        // F01_RG_NBT01
        if (tarif.getModeCalculCotisations() == 1) {
            boolean categorieEffectiveCorrespondanteExiste = paramCategorieEffectifsRepository
                    .existeUnParamCategorieEffectifs(tarif.getNumCategorie(), tarif.getLibelleCategorie());
            String messageErreurNocatLicat = "Le couple NOCAT/LICAT (" + tarif.getNumCategorie() + "/" + tarif.getLibelleCategorie()
                    + ") n'existe pas dans la table ParamCategoriesEffectifs";
            valideChampAvecCollecteSansBlocage(categorieEffectiveCorrespondanteExiste, tarif, "NOCAT/LICAT", messageErreurNocatLicat);
        }

        // F01_RG_NBT02
        boolean natureBaseCorrespondanteExiste = paramNatureBaseRepository
                .existeUnParametragePourNatureBaseCotisations(tarif.getNatureBaseCotisations());
        String messageErreurConbcot = "Le CONBCOT (" + tarif.getNatureBaseCotisations() + ") n'existe pas dans la table ParamNatureBase";
        valideChampAvecCollecteSansBlocage(natureBaseCorrespondanteExiste, tarif, "CONBCOT", messageErreurConbcot);

    }
}
