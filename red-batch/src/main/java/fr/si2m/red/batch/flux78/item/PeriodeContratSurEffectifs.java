package fr.si2m.red.batch.flux78.item;

import lombok.Getter;
import lombok.Setter;

/**
 * Modèle des périodes de contrat sur effectifs publiées pour WQUI.
 * 
 * @author poidij
 *
 */
public class PeriodeContratSurEffectifs extends PeriodePublieeWQUI {

    @Getter
    @Setter
    private Integer dateEffetIR;
    @Getter
    @Setter
    private Integer nombreIR;
    @Getter
    @Setter
    private String incorporeRadie;
    @Getter
    @Setter
    private String fillerPartieSpecifique;

}
