package fr.si2m.red.batch.flux56.item;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import fr.si2m.red.DateRedac;
import fr.si2m.red.batch.moteur.item.TransformateurDonnee;
import fr.si2m.red.complement.ExtensionContrat;
import fr.si2m.red.complement.ExtensionContratRepository;
import fr.si2m.red.contrat.Contrat;
import fr.si2m.red.contrat.TarifRepository;
import fr.si2m.red.parametrage.ParamFamilleModeCalculContratRepository;
import fr.si2m.red.parametrage.ParamValeurDefaut;
import fr.si2m.red.parametrage.ParamValeurDefautRepository;
import fr.si2m.red.reconciliation.IndicateursDSNPeriode;
import fr.si2m.red.reconciliation.IndicateursDSNPeriodeRepository;
import fr.si2m.red.reconciliation.MessageControleRepository;
import fr.si2m.red.reconciliation.PeriodeRecue;
import fr.si2m.red.reconciliation.PeriodeRecueRepository;
import lombok.Setter;

/**
 * Création d'indicateurs DSN de période à partir de contrats.
 * 
 * @author poidij
 *
 */
public class IndicateursDSNPeriodeCreateur extends TransformateurDonnee<Contrat, List<IndicateursDSNPeriode>> {

    private static final Logger LOGGER = LoggerFactory.getLogger(IndicateursDSNPeriodeCreateur.class);

    @Autowired
    private ParamValeurDefautRepository paramValeurDefautRepository;
    @Autowired
    private ExtensionContratRepository extensionContratRepository;
    @Autowired
    private IndicateursDSNPeriodeRepository indicateursDSNPeriodeRepository;
    @Autowired
    private ParamFamilleModeCalculContratRepository paramFamilleModeCalculContratRepository;
    @Autowired
    private TarifRepository tarifRepository;
    @Autowired
    private PeriodeRecueRepository periodeRecueRepository;
    @Autowired
    private MessageControleRepository messageControleRepository;

    @Setter
    private String nomBatch;
    @Setter
    private String dateCalculIndicateurs;

    @Override
    public List<IndicateursDSNPeriode> process(Contrat contrat) throws Exception {

        // Filtrage des contrats suivant la RG F0506_RG_CI01 - gestion de la periodicité
        Integer periodicite = calculePeriodicite(contrat.getCodeFractionnement());
        Integer moisCalcul = DateRedac.extraitMois(this.dateCalculIndicateurs);
        if (periodicite != null && moisCalcul % periodicite == 0) {
            IndicateursDSNPeriode indicateurs = calculeIndicateurs(contrat, periodicite);
            LOGGER.debug("Calcul des indicateurs {} effectué", indicateurs);
            return Arrays.asList(indicateurs);
        }

        return new ArrayList<IndicateursDSNPeriode>(0);

    }

    /**
     * Calcule les indicateurs pour un contrat sur une période donnée.
     * 
     * @param contrat
     *            le contrat
     * @param periodicite
     *            la périodicité des indicateurs
     * @return les indicateurs calculés
     */
    private IndicateursDSNPeriode calculeIndicateurs(Contrat contrat, Integer periodicite) {
        // Récupération des indicateurs de la période antérieure pour les calculs
        IndicateursDSNPeriode indicateursDSNPeriodeAnterieure = indicateursDSNPeriodeRepository
                .getIndicateursDernierePeriode(contrat.getNumContrat());
        // Récupération des spécificités du contrat pour les calculs
        ExtensionContrat extensionContrat = extensionContratRepository.getExtensionContrat(contrat.getNumContrat());

        IndicateursDSNPeriode indicateursDSNPeriode = new IndicateursDSNPeriode();
        indicateursDSNPeriode.setLigneEnCoursImportBatch(true);
        indicateursDSNPeriode.setAuditUtilisateurCreation(nomBatch);
        indicateursDSNPeriode.setNumContrat(contrat.getNumContrat());

        // RG - F0506_RG_CI02 - calcul de la date de debut de periode
        indicateursDSNPeriode.setDebutPeriode(calculeDateDebutPeriode(periodicite));

        // RG - F0506_RG_CI03 - calcul de la date de fin de periode
        indicateursDSNPeriode.setFinPeriode(calculeDateFinPeriode(periodicite));

        // Calcul de la date de debut de réception des déclarations
        indicateursDSNPeriode.setDateDebutReceptionDSN(calculeDateDebutReceptionDSN(indicateursDSNPeriode, indicateursDSNPeriodeAnterieure, contrat));

        // Calcul des champs liés à la modification des indicateurs
        renseigneChampsModifications(indicateursDSNPeriode, indicateursDSNPeriodeAnterieure, extensionContrat);

        // RG F0506_RG_CI06 - Calcul des champs liés aux indicateurs
        renseigneChampsIndicateurs(indicateursDSNPeriode, indicateursDSNPeriodeAnterieure, extensionContrat, contrat);

        // RG - F0506_RG_CI04 - calcul de mode gestion DSN
        PeriodeRecue dernierePeriode = periodeRecueRepository.getDernierePeriodeContrat(contrat.getNumContrat());

        if (dernierePeriode != null && !"N".equals(indicateursDSNPeriode.getControleTypeBaseAssujettie())
                && messageControleRepository.existeMessageRejetPourBaseAssujettiePourPeriode(dernierePeriode.getIdPeriode())) {
            indicateursDSNPeriode.setModeGestionDSN("N");
        } else {
            indicateursDSNPeriode.setModeGestionDSN(calculeModeGestionDSN(indicateursDSNPeriode.isExploitationDSN(),
                    indicateursDSNPeriode.getDateDebutReceptionDSN(), indicateursDSNPeriode.isEditionConsignesPaiementDSN()));
        }

        // Calcul des champs date
        Integer dateDebutPeriode = indicateursDSNPeriode.getDebutPeriode();
        indicateursDSNPeriode.setDateDebutExploitationDSN(dateDebutPeriode);
        indicateursDSNPeriode.setDateDebutEditionConsignesPaiement(dateDebutPeriode);
        indicateursDSNPeriode.setDateDebutTransfertDSN(dateDebutPeriode);
        indicateursDSNPeriode
                .setDateDebutModeGestionDSN(StringUtils.equals(indicateursDSNPeriode.getModeGestionDSN(), "N") ? null : dateDebutPeriode);
        if (indicateursDSNPeriodeAnterieure != null) {
            if (indicateursDSNPeriodeAnterieure.isExploitationDSN() == indicateursDSNPeriode.isExploitationDSN()) {
                indicateursDSNPeriode.setDateDebutExploitationDSN(indicateursDSNPeriodeAnterieure.getDateDebutExploitationDSN());
            }
            if (indicateursDSNPeriodeAnterieure.isEditionConsignesPaiementDSN() == indicateursDSNPeriode.isEditionConsignesPaiementDSN()) {
                indicateursDSNPeriode.setDateDebutEditionConsignesPaiement(indicateursDSNPeriodeAnterieure.getDateDebutEditionConsignesPaiement());
            }
            if (indicateursDSNPeriodeAnterieure.isTransfertDSN() == indicateursDSNPeriode.isTransfertDSN()) {
                indicateursDSNPeriode.setDateDebutTransfertDSN(indicateursDSNPeriodeAnterieure.getDateDebutTransfertDSN());
            }
            if (StringUtils.equals(indicateursDSNPeriodeAnterieure.getModeGestionDSN(), indicateursDSNPeriode.getModeGestionDSN())) {
                indicateursDSNPeriode.setDateDebutModeGestionDSN(indicateursDSNPeriodeAnterieure.getDateDebutModeGestionDSN());
            }
        }

        return indicateursDSNPeriode;

    }

    /**
     * RG - F0506_RG_CI01 - Calcule la périodicité (mois) à partir d'un code de périodicité.
     * 
     * @param codePeriodicite
     *            le code de périodicité
     * @return la périodicité en nombre de mois, null si non convertible
     */
    private Integer calculePeriodicite(String codePeriodicite) {
        if (codePeriodicite != null) {
            switch (codePeriodicite) {
            case "M":
                return 1;
            case "T":
                return 3;
            case "S":
                return 6;
            case "A":
                return 12;
            default:
            }
        }
        return 3;
    }

    /**
     * RG - F0506_RG_CI02 - calcul de la date de debut de periode.
     * 
     * @return la date REDAC de début de période calculée
     */
    private Integer calculeDateDebutPeriode(Integer periodicite) {
        String anneeCalcul = String.valueOf(DateRedac.extraitAnnee(this.dateCalculIndicateurs));
        Integer moisCalcul = DateRedac.extraitMois(this.dateCalculIndicateurs);
        String moisDebutPeriode = String.valueOf((int) (Math.floor((double) (moisCalcul - 1) / periodicite)) * periodicite + 1);
        if (moisDebutPeriode.length() < 2) {
            moisDebutPeriode = "0" + moisDebutPeriode;
        }
        return Integer.valueOf(anneeCalcul + moisDebutPeriode + "01");
    }

    /**
     * RG - F0506_RG_CI03 - calcul de la date de fin de periode.
     * 
     * @return la date REDAC de fin de période calculée
     */
    private Integer calculeDateFinPeriode(Integer periodicite) {

        Integer moisCalcul = DateRedac.extraitMois(this.dateCalculIndicateurs);
        String moisFinPeriode = String.valueOf((int) Math.ceil((double) moisCalcul / periodicite) * periodicite);
        if (moisFinPeriode.length() < 2) {
            moisFinPeriode = "0" + moisFinPeriode;
        }

        Calendar calendar = Calendar.getInstance();
        Integer anneeFinPeriode = DateRedac.extraitAnnee(this.dateCalculIndicateurs);
        calendar.set(anneeFinPeriode, Integer.parseInt(moisFinPeriode) - 1, 1);
        String jourFinPeriode = String.valueOf(calendar.getActualMaximum(Calendar.DAY_OF_MONTH));

        return Integer.valueOf(String.valueOf(anneeFinPeriode) + moisFinPeriode + jourFinPeriode);
    }

    /**
     * Traduction de la règle de gestion F0506_RG_CI04.
     * 
     * @param exploitationDSN
     *            la date d'exploitation DSN
     * @param datePremReceptDSN
     *            la date de première réception de la DSN
     * @param editionConsignesPaieDSN
     *            l'indicateur de consignes de paie
     */
    private String calculeModeGestionDSN(boolean exploitationDSN, Integer datePremReceptDSN, boolean editionConsignesPaieDSN) {

        if (!exploitationDSN || datePremReceptDSN == null || datePremReceptDSN == 0) {
            return "N";
        }
        if (editionConsignesPaieDSN) {
            return "C";
        } else {
            return "O";
        }
    }

    /**
     * Calcule la date de début de réception des déclarations.
     * 
     * @param indicateurDSNPeriode
     *            l'indicateur à créer
     * @param indicateursDSNPeriodeAnterieure
     *            l'indicateur de la dernière période connue
     * @param contrat
     *            le contrat
     */
    private Integer calculeDateDebutReceptionDSN(IndicateursDSNPeriode indicateurDSNPeriode, IndicateursDSNPeriode indicateursDSNPeriodeAnterieure,
            Contrat contrat) {
        if (indicateursDSNPeriodeAnterieure != null && indicateursDSNPeriodeAnterieure.getDateDebutReceptionDSN() != null
                && indicateursDSNPeriodeAnterieure.getDateDebutReceptionDSN() != 0) {
            return indicateursDSNPeriodeAnterieure.getDateDebutReceptionDSN();
        } else {
            Integer dateDebutPeriode = indicateurDSNPeriode.getDebutPeriode();
            if (periodeRecueRepository.existeDeclarationRecuePremierMoisPourDebutPeriode(contrat.getNumContrat())) {
                return dateDebutPeriode;
            } else {
                return null;
            }
        }
    }

    /**
     * Renseigne les champs relatifs à la modification des indicateurs d'une période.
     * 
     * @param indicateurDSNPeriode
     *            les indicateurs à renseigner
     * @param indicateursDSNPeriodeAnterieure
     *            les indicateurs sur la période antérieure (null si n'existent pas)
     * @param extensionContrat
     *            les spécificités du contrat sur lequel porte la période
     */
    private void renseigneChampsModifications(IndicateursDSNPeriode indicateursDSNPeriode, IndicateursDSNPeriode indicateursDSNPeriodeAnterieure,
            ExtensionContrat extensionContrat) {
        if (extensionContrat != null && extensionContrat.getDateEffetIndicateurs() <= indicateursDSNPeriode.getDebutPeriode()) {
            indicateursDSNPeriode.setDateModifIndicateurs(extensionContrat.getDateDerniereModificationIndicateurs());
            indicateursDSNPeriode.setGestionnaireModifIndicateurs(extensionContrat.getGestionnaireDerniereModificationIndicateurs());
            indicateursDSNPeriode.setMotifChangementIndicateurs(extensionContrat.getMotifChangementIndicateurs());
        } else if (indicateursDSNPeriodeAnterieure != null) {
            indicateursDSNPeriode.setDateModifIndicateurs(indicateursDSNPeriodeAnterieure.getDateModifIndicateurs());
            indicateursDSNPeriode.setGestionnaireModifIndicateurs(indicateursDSNPeriodeAnterieure.getGestionnaireModifIndicateurs());
            indicateursDSNPeriode.setMotifChangementIndicateurs(indicateursDSNPeriodeAnterieure.getMotifChangementIndicateurs());
        } else {
            indicateursDSNPeriode.setDateModifIndicateurs(null);
            indicateursDSNPeriode.setGestionnaireModifIndicateurs(null);
            indicateursDSNPeriode.setMotifChangementIndicateurs(null);
        }
    }

    /**
     * Renseigne les champs indicateurs d'une période.
     * 
     * @param indicateurDSNPeriode
     *            les indicateurs à renseigner
     * @param indicateursDSNPeriodeAnterieure
     *            les indicateurs sur la période antérieure (null si n'existent pas)
     * @param extensionContrat
     *            les spécificités du contrat sur lequel porte la période
     * @param contrat
     *            le contrat sur lequel porte la période
     */
    private void renseigneChampsIndicateurs(IndicateursDSNPeriode indicateurDSNPeriode, IndicateursDSNPeriode indicateursDSNPeriodeAnterieure,
            ExtensionContrat extensionContrat, Contrat contrat) {
        if (extensionContrat != null && extensionContrat.getDateEffetIndicateurs() <= indicateurDSNPeriode.getDebutPeriode()) {
            renseigneChampsIndicateursFromExtensionContrat(indicateurDSNPeriode, extensionContrat);
        } else if (extensionContrat != null && indicateursDSNPeriodeAnterieure != null) {
            renseigneChampsIndicateursFromIndicateursDSNPeriodeAnterieure(indicateurDSNPeriode, indicateursDSNPeriodeAnterieure);
        } else {
            renseigneChampsIndicateursFromContrat(indicateurDSNPeriode, contrat);
        }
    }

    /**
     * Renseigne les indicateurs d'une période à partir d'une extension de contrat.
     * 
     * @param indicateurDSNPeriode
     *            les indicateurs sur la période
     * @param extensionContrat
     *            l'extension de contrat
     */
    private void renseigneChampsIndicateursFromExtensionContrat(IndicateursDSNPeriode indicateurDSNPeriode, ExtensionContrat extensionContrat) {
        indicateurDSNPeriode.setExploitationDSN(extensionContrat.isExploitationDSN());
        indicateurDSNPeriode.setEditionConsignesPaiementDSN(extensionContrat.isEditionConsignesPaiement());
        indicateurDSNPeriode.setTransfertDSN(extensionContrat.isTransfert());
        indicateurDSNPeriode.setSeuilVariationAlertesEnNbEtablissements(extensionContrat.getSeuilVariationAlertesEnNbEtablissements());
        indicateurDSNPeriode
                .setSeuilVariationAlertesEnPourcentageNbEtablissements(extensionContrat.getSeuilVariationAlertesEnPourcentageNbEtablissements());
        indicateurDSNPeriode.setSeuilVariationAlertesEnNbSalaries(extensionContrat.getSeuilVariationAlertesEnNbSalaries());
        indicateurDSNPeriode.setSeuilVariationAlertesEnPourcentageNbSalaries(extensionContrat.getSeuilVariationAlertesEnPourcentageNbSalaries());
        indicateurDSNPeriode.setControleTypeBaseAssujettie(extensionContrat.getControleTypeBaseAssujettie());
        indicateurDSNPeriode.setModeNatureContrat(extensionContrat.getModeNatureContrat());
        indicateurDSNPeriode.setCodeVip(extensionContrat.isVip());
        indicateurDSNPeriode.setModeReaffectationCategorieEffectif(extensionContrat.getModeReaffectationCategorieEffectifs());
    }

    /**
     * Renseigne les indicateurs d'une période à partir des indicateurs de la période antérieure.
     * 
     * @param indicateurDSNPeriode
     *            les indicateurs sur la période
     * @param indicateursDSNPeriodeAnterieure
     *            les indicateurs sur la période antérieure
     */
    private void renseigneChampsIndicateursFromIndicateursDSNPeriodeAnterieure(IndicateursDSNPeriode indicateurDSNPeriode,
            IndicateursDSNPeriode indicateursDSNPeriodeAnterieure) {
        indicateurDSNPeriode.setExploitationDSN(indicateursDSNPeriodeAnterieure.isExploitationDSN());
        indicateurDSNPeriode.setEditionConsignesPaiementDSN(indicateursDSNPeriodeAnterieure.isEditionConsignesPaiementDSN());
        indicateurDSNPeriode.setTransfertDSN(indicateursDSNPeriodeAnterieure.isTransfertDSN());
        indicateurDSNPeriode.setSeuilVariationAlertesEnNbEtablissements(indicateursDSNPeriodeAnterieure.getSeuilVariationAlertesEnNbEtablissements());
        indicateurDSNPeriode.setSeuilVariationAlertesEnPourcentageNbEtablissements(
                indicateursDSNPeriodeAnterieure.getSeuilVariationAlertesEnPourcentageNbEtablissements());
        indicateurDSNPeriode.setSeuilVariationAlertesEnNbSalaries(indicateursDSNPeriodeAnterieure.getSeuilVariationAlertesEnNbSalaries());
        indicateurDSNPeriode
                .setSeuilVariationAlertesEnPourcentageNbSalaries(indicateursDSNPeriodeAnterieure.getSeuilVariationAlertesEnPourcentageNbSalaries());
        indicateurDSNPeriode.setControleTypeBaseAssujettie(indicateursDSNPeriodeAnterieure.getControleTypeBaseAssujettie());
        indicateurDSNPeriode.setModeNatureContrat(indicateursDSNPeriodeAnterieure.getModeNatureContrat());
        indicateurDSNPeriode.setCodeVip(indicateursDSNPeriodeAnterieure.isCodeVIP());
        indicateurDSNPeriode.setModeReaffectationCategorieEffectif(indicateursDSNPeriodeAnterieure.getModeReaffectationCategorieEffectif());
    }

    /**
     * Renseigne les indicateurs d'une période à partir des informations du contrat sur lequel elle porte.
     * 
     * @param indicateurDSNPeriode
     *            les indicateurs sur la période
     * @param contrat
     *            le contrat sur lequel porte la période
     */
    private void renseigneChampsIndicateursFromContrat(IndicateursDSNPeriode indicateurDSNPeriode, Contrat contrat) {
        ParamValeurDefaut paramValeurDefaut = paramValeurDefautRepository.getParamValeurDefaut();

        indicateurDSNPeriode.setExploitationDSN(paramValeurDefaut.isExploitationDSN());
        indicateurDSNPeriode.setEditionConsignesPaiementDSN(paramValeurDefaut.isEditionConsignePaiementModeDSN());
        indicateurDSNPeriode.setTransfertDSN(paramValeurDefaut.isTransfertDeclarationsVersQuatremAutorise());
        indicateurDSNPeriode.setSeuilVariationAlertesEnNbEtablissements(paramValeurDefaut.getSeuilVariationAlertesEnNbEtablissements());
        indicateurDSNPeriode
                .setSeuilVariationAlertesEnPourcentageNbEtablissements(paramValeurDefaut.getSeuilVariationAlertesEnPourcentageNbEtablissements());
        indicateurDSNPeriode.setSeuilVariationAlertesEnNbSalaries(paramValeurDefaut.getSeuilVariationAlertesEnNbSalaries());
        indicateurDSNPeriode.setSeuilVariationAlertesEnPourcentageNbSalaries(paramValeurDefaut.getSeuilVariationAlertesEnPourcentageNbSalaries());

        indicateurDSNPeriode.setModeNatureContrat(paramValeurDefaut.getModeNatureContrat());
        indicateurDSNPeriode.setCodeVip(paramValeurDefaut.isContratVIP());
        indicateurDSNPeriode.setModeReaffectationCategorieEffectif(paramValeurDefaut.getModeReaffectationCategorieEffectifs());

        // F0506_RG_CI05 - Sauf pour CTL_TYPE_BASE_ASSUJ pour lequel on recupere le controle type defaut (controleTypeParDefaut)
        Integer modeCalculCotisation = tarifRepository.getModeCalculCotisation(contrat.getNumContrat());
        String controleTypeParDefaut = paramFamilleModeCalculContratRepository.getModeControleTypesBasesAssujetties(contrat.getNumFamilleProduit(),
                contrat.getGroupeGestion(), modeCalculCotisation);
        indicateurDSNPeriode.setControleTypeBaseAssujettie(controleTypeParDefaut);
    }

}
