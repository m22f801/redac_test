package fr.si2m.red.batch.flux3.item;

import fr.si2m.red.batch.moteur.item.TransformateurDonnee;
import fr.si2m.red.parametrage.ParamContact;

/**
 * Mapper des entités ParamContact à partir du ParamContact fourni.
 * 
 * cf F03_RG_S15
 * 
 * @author poidij
 *
 */
public class TransformateurContact extends TransformateurDonnee<ParamContact, ParamContact> {

    @Override
    public ParamContact process(ParamContact contact) throws Exception {
        // F03_RG_S15 : Pas de transformation de donnée.

        return contact;
    }

}
