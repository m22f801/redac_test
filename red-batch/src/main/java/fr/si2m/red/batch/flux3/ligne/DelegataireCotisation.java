package fr.si2m.red.batch.flux3.ligne;

import lombok.Data;
import fr.si2m.red.DateRedac;

/**
 * Description d'une ligne d'un fichier DELEGATAIRES_COTISATION à exporter.
 * 
 * @author nortaina
 *
 */
@Data
public class DelegataireCotisation {
    /**
     * Code Mise à jour.
     * 
     * @param codeMiseAJour
     *            Code Mise à jour
     * @return Code Mise à jour
     */
    private String codeMiseAJour;

    /**
     * Identifiant technique du délégataire de cotisation (N° de séquence local à une instance de cette interface).
     * 
     * @param identifiantTechniqueDelegataireCotisation
     *            Identifiant technique du délégataire de cotisation (N° de séquence local à une instance de cette interface)
     * @return Identifiant technique du délégataire de cotisation (N° de séquence local à une instance de cette interface)
     */
    private String identifiantTechniqueDelegataireCotisation;

    /**
     * Date début d’application des caractéristiques du délégataire.
     * 
     * @param dateDebutApplicationCaracteristiques
     *            Date début d’application des caractéristiques du délégataire
     * @return Date début d’application des caractéristiques du délégataire
     */
    private Integer dateDebutApplicationCaracteristiques;

    /**
     * Date de fin d’application des caractéristiques du délégataire.
     * 
     * @param dateFinApplicationCaracteristiques
     *            Date de fin d’application des caractéristiques du délégataire
     * @return Date de fin d’application des caractéristiques du délégataire
     */
    private Integer dateFinApplicationCaracteristiques;

    /**
     * Code organisme délégataire de cotisation selon le SI source.
     * 
     * @param codeOrganismeDelegataireCotisation
     *            Code organisme délégataire de cotisation selon le SI source
     * @return Code organisme délégataire de cotisation selon le SI source
     */
    private String codeOrganismeDelegataireCotisation;

    /**
     * Libellé du délégataire de cotisation.
     * 
     * @param libelleDelegataireCotisation
     *            Libellé du délégataire de cotisation
     * @return Libellé du délégataire de cotisation
     */
    private String libelleDelegataireCotisation;

    /**
     * Date début d’application des caractéristiques du délégataire.
     * 
     * @return Date début d’application des caractéristiques du délégataire
     */
    public String getDateDebutApplicationCaracteristiquesFormatee() {
        return DateRedac.formate(DateRedac.EXPORT_FORMAT_PAR_DEFAUT, dateDebutApplicationCaracteristiques);
    }

    /**
     * Date de fin d’application des caractéristiques du délégataire.
     * 
     * @return Date de fin d’application des caractéristiques du délégataire
     */
    public String getDateFinApplicationCaracteristiquesFormatee() {
        return DateRedac.formate(DateRedac.EXPORT_FORMAT_PAR_DEFAUT, dateFinApplicationCaracteristiques);
    }

}
