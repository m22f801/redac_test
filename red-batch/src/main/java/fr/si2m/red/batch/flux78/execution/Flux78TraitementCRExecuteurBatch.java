package fr.si2m.red.batch.flux78.execution;

import java.text.MessageFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParameter;
import org.springframework.batch.core.JobParameters;
import org.springframework.context.support.GenericXmlApplicationContext;

import fr.si2m.red.DateRedac;
import fr.si2m.red.batch.RedacMessages;
import fr.si2m.red.batch.moteur.CodeRetour;
import fr.si2m.red.batch.moteur.execution.ExecuteurBatch;
import fr.si2m.red.batch.moteur.execution.FacadeExecuteurBatch;

/**
 * Exécuteur du batch du flux 78.
 * 
 * @author poidij
 *
 */
@FacadeExecuteurBatch(nomBatch = "flux78_TraitementCR")
public class Flux78TraitementCRExecuteurBatch extends ExecuteurBatch {
    /**
     * Ordre du paramètre de lancement "chemin du fichier".
     */
    public static final int ORDRE_PARAM_CHEMIN_FICHIER = 0;
    /**
     * Ordre du paramètre de lancement "emplacement du fichier de log d'erreurs".
     */
    public static final int ORDRE_PARAM_EMPLACEMENT_FICHIER_ERREURS = 1;

    private static final Logger LOGGER = LoggerFactory.getLogger(Flux78TraitementCRExecuteurBatch.class);

    private final String cheminFichier;
    private final String emplacementRessourceFichier;
    private final Long timeStampExecution;

    /**
     * Exécuteur de batch du flux 1 prenant en compte les fichiers contrats, tarifs, Clients et intermédiaires centralisés dans un répertoire donnés.
     * 
     * @param parametrageBatch
     *            les paramètres d'exécution du batch - deux paramètres sont attendus en entrée :
     *            <ol>
     *            <li>le chemin complet du répertoire où sont regroupés les fichiers à réceptionner pour le flux 1</li>
     *            <li>l'identifiant partagé par les fichiers (exemple : D150722A pour CONTRAT.D150722A.txt)</li>
     *            </ol>
     */
    public Flux78TraitementCRExecuteurBatch(String[] parametrageBatch) {
        super(parametrageBatch);

        this.cheminFichier = parametrageBatch[ORDRE_PARAM_CHEMIN_FICHIER];
        this.emplacementRessourceFichier = tranformeEmplacementRessourceFichier(cheminFichier);
        this.timeStampExecution = DateRedac.convertitEnDateHeureRedac(Calendar.getInstance().getTime());

        String cheminAbsoluFichierLogErreurs = parametrageBatch[ORDRE_PARAM_EMPLACEMENT_FICHIER_ERREURS];
        setCheminAbsoluFichierLogErreurs(cheminAbsoluFichierLogErreurs);
    }

    @Override
    protected String getEmplacementFichierConfigurationJob() {
        return "classpath:fr/si2m/red/batch/flux78/job_TraitementCR.xml";
    }

    @Override
    protected String getIdJobCible() {
        return "jobFlux78_TraitementCR";
    }

    @Override
    protected boolean lanceTraitementAvantJob(GenericXmlApplicationContext contexteExecutionJob) {

        // Test de l'existence du fichier
        if (!verifieExistencePhysiqueRessource(contexteExecutionJob, emplacementRessourceFichier)) {
            LOGGER.error("Fichier manquant pour démarrer le batch : {}", emplacementRessourceFichier);
            String messageErreur = MessageFormat.format(RedacMessages.ERREUR_FICHIER_INEXISTANT, this.cheminFichier);
            traceErreurDansFichierLog(messageErreur);
            return false;
        }

        // Test de la présence de ligne(s) synthèse : cas d'un fichier vide.
        // En effet, si le fichier est vide, on ne rentre pas dans ControleNbEnregistrementsProcesseur, et l'absence de ligne synthèse ne peut donc y être
        // détectée.
        if (!testeSiFichierContientDonneesUtiles(contexteExecutionJob, emplacementRessourceFichier, 0)) {
            LOGGER.error(RedacMessages.PAS_DE_SYNTHESE);
            traceErreurDansFichierLog(RedacMessages.PAS_DE_SYNTHESE);
            return false;
        }

        return true;
    }

    @Override
    protected JobParameters initParametrageJob(GenericXmlApplicationContext contexteExecutionJob) {
        Map<String, JobParameter> parametresJob = new HashMap<String, JobParameter>();

        // Configuration lecteur du fichier en entrée
        LOGGER.debug("Fichier entrée pris en compte : {}", emplacementRessourceFichier);
        parametresJob.put("fichierEntree", new JobParameter(emplacementRessourceFichier));

        // Configuration de l'emplacement du fichier de log
        LOGGER.debug("Fichier erreurs pris en compte : {}", getCheminAbsoluFichierLogErreurs());
        parametresJob.put("fichierLogErreurs", new JobParameter(getCheminAbsoluFichierLogErreurs()));

        // Configuration timeStampExecution du batch
        LOGGER.debug("timeStampExecution pris en compte : {}", timeStampExecution);
        parametresJob.put("timeStampExecution", new JobParameter(timeStampExecution));

        return new JobParameters(parametresJob);
    }

    @Override
    protected void lanceTraitementFinJob(GenericXmlApplicationContext contexteExecutionJob, CodeRetour codeRetour, JobExecution executionJob) {
        // Logge le message de réussite du traitement batch dans le fichier de log
        if (codeRetour == CodeRetour.TERMINE) {
            traceTraitementOKDansFichierLog("Traitement du flux 78 - Comptes-rendus - réussi");
        } else if (codeRetour == CodeRetour.ERREUR_FONCTIONNELLE_NON_BLOQUANTE) {
            traceTraitementOKDansFichierLog("Traitement du flux 78 - Comptes-rendus - réussi, avec erreur(s) non bloquante(s)");
        }

    }

}
