package fr.si2m.red.batch.flux4.support;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.partition.support.Partitioner;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.beans.factory.annotation.Autowired;

import fr.si2m.red.dsn.ParametresPartitionPeriodes;
import fr.si2m.red.reconciliation.PeriodeRecueRepository;
import lombok.Setter;

/**
 * Partitionneur utilisé pour la reconsolidation sur Effectifs
 *
 */
public class ReconsolidationSurEffectifsPartitionneur implements Partitioner {

    private static final Logger LOGGER = LoggerFactory.getLogger(ReconsolidationSurEffectifsPartitionneur.class);

    @Autowired
    private PeriodeRecueRepository periodeRecueRepository;

    @Setter
    private int pagination;

    @Override
    public Map<String, ExecutionContext> partition(int gridSize) {

        Map<String, ExecutionContext> threads = new HashMap<String, ExecutionContext>();
        ParametresPartitionPeriodes paramsGlobaux = periodeRecueRepository.recupereDonneesGlobalesDuPartionning();
        LOGGER.info("paramsGlobaux de partitionnement : " + paramsGlobaux);
        LOGGER.info("Nombre de partitions paramétrées : " + gridSize);

        Long cumulTotal = periodeRecueRepository.recupereNombreTotalElementCalculesPourPartionning();
        LOGGER.info("cumulTotal d'entités à créer : " + (cumulTotal == null ? "0" : cumulTotal));

        // pour 153 périodes et 4 threads, on divise en 39-39-39-36
        if (gridSize > 1 && paramsGlobaux.getNbPeriodes() > 2 * gridSize) {

            String partitionnement = "CASE";

            for (int i = 1; i < gridSize; i++) {
                partitionnement += " WHEN @running_total <= " + (cumulTotal * i / gridSize) + " THEN " + (i);
            }

            partitionnement += " ELSE " + gridSize + " END AS NTILE";

            LOGGER.info("Cas de partitionnement : '" + partitionnement + "'");

            List<Long> listeIdPeriodePartitionning = periodeRecueRepository.recupereDonneesDuPartionning(partitionnement);

            LOGGER.info("Bornes du partitionnement (IdPeriodes) : " + listeIdPeriodePartitionning);

            int nbPartitionsBornees = listeIdPeriodePartitionning.size() / 2;
            if (nbPartitionsBornees != gridSize) {
                LOGGER.info("Nombre de partitions pris en compte (d'après les bornes trouvées) : " + nbPartitionsBornees);
            } else {
                LOGGER.info("Nombre de partitions pris en compte (initial) : " + gridSize);
            }

            for (int i = 0; i < nbPartitionsBornees; i++) {

                // on joint au dernier thread la borne max, afin d'oublier aucune période
                if (nbPartitionsBornees != gridSize && i == nbPartitionsBornees - 1) {
                    ajouteThread(threads, i + 1, listeIdPeriodePartitionning.get(2 * i),
                            listeIdPeriodePartitionning.get(listeIdPeriodePartitionning.size() - 1));
                } else {
                    ajouteThread(threads, i + 1, listeIdPeriodePartitionning.get(2 * i), listeIdPeriodePartitionning.get(2 * i + 1));
                }

            }
        } else {
            LOGGER.info("Nombre de partitions prises en compte : 1");
            ajouteThread(threads, 1, paramsGlobaux.getMinIdPeriode(), paramsGlobaux.getMaxIdPeriode());
        }
        return threads;
    }

    /**
     * Ajout d'un thread
     * 
     * @param threads
     *            map de threads
     * @param i
     *            numérotation du thread
     * @param fromId
     *            id période début
     * @param toId
     *            id période de fin
     */
    private void ajouteThread(Map<String, ExecutionContext> threads, int i, Long fromId, Long toId) {
        ExecutionContext value = new ExecutionContext();

        LOGGER.info("Starting : Thread" + i);
        LOGGER.info("fromId : " + fromId);
        LOGGER.info("toId : " + toId);

        value.putLong("fromId", fromId);
        value.putLong("toId", toId);

        // nomme chaque thread : "Thread_1,2,3,4..."
        value.putString("name", "Thread_" + i);

        threads.put("partition" + i, value);
    }

}