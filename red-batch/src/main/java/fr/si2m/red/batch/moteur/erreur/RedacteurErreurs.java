package fr.si2m.red.batch.moteur.erreur;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.springframework.batch.item.ItemWriter;

import fr.si2m.red.EntiteImportableBatch;
import fr.si2m.red.ErreurLigneFichierImport;
import fr.si2m.red.RedacUnexpectedException;
import fr.si2m.red.batch.moteur.BatchConfiguration;

/**
 * Rédacteur des erreurs dans un fichier plat de récapitulatifs des lignes en erreurs.
 * 
 * @author nortaina
 *
 */
public class RedacteurErreurs implements ItemWriter<EntiteImportableBatch> {

    private final File fichierLogErreurs;

    /**
     * Constructeur du rédacteur des erreurs.
     * 
     * @param cheminAbsoluFichierLogErreurs
     *            l'emplacement du fichier de log d'erreurs vers lequel écrire
     */
    public RedacteurErreurs(String cheminAbsoluFichierLogErreurs) {
        this.fichierLogErreurs = new File(cheminAbsoluFichierLogErreurs);
        if (fichierLogErreurs.getParentFile() != null && !fichierLogErreurs.getParentFile().exists()) {
            boolean repertoireParentCree = fichierLogErreurs.getParentFile().mkdirs();
            if (!repertoireParentCree) {
                throw new RedacUnexpectedException("Impossible de créer le répertoire des logs d'erreurs");
            }
        }
        if (!fichierLogErreurs.exists()) {
            try {
                boolean fichierCree = fichierLogErreurs.createNewFile();
                if (!fichierCree) {
                    throw new RedacUnexpectedException("Impossible de créer le fichier de log d'erreurs");
                }
            } catch (IOException e) {
                throw new RedacUnexpectedException("Erreur lors de la création du log d'erreurs", e);
            }
        }
    }

    @Override
    public void write(List<? extends EntiteImportableBatch> ligneValidees) throws Exception {
        FileOutputStream fluxSortantFichier = new FileOutputStream(fichierLogErreurs, true);
        OutputStreamWriter redacteurFluxSortantFichier = new OutputStreamWriter(fluxSortantFichier, BatchConfiguration.ENCODAGE_FICHIERS_LOGS);
        PrintWriter redacteurFichier = new PrintWriter(redacteurFluxSortantFichier, false);
        try {
            for (EntiteImportableBatch ligneValidee : ligneValidees) {
                // "Erreurs bloquantes"
                for (ErreurLigneFichierImport erreur : ligneValidee.getErreursImport()) {
                    redacteurFichier.write(erreur.toString());
                    redacteurFichier.println();
                }
                // "Erreurs non bloquantes"
                for (ErreurLigneFichierImport avertissement : ligneValidee.getAvertissementsImport()) {
                    redacteurFichier.write(avertissement.toString());
                    redacteurFichier.println();
                }
            }
        } finally {
            redacteurFichier.flush();
            IOUtils.closeQuietly(redacteurFichier);
        }

    }
}
