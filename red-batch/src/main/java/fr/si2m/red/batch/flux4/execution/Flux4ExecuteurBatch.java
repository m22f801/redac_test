package fr.si2m.red.batch.flux4.execution;

import org.springframework.context.support.GenericXmlApplicationContext;

import fr.si2m.red.batch.moteur.execution.ExecuteurBatch;

/**
 * Exécuteur générique du flux 4.
 * 
 * @author nortaina
 *
 */
public abstract class Flux4ExecuteurBatch extends ExecuteurBatch {
    /**
     * Exécuteur de batch du flux 4.
     * 
     * @param parametrageBatch
     *            les paramètres d'exécution du batch
     */
    protected Flux4ExecuteurBatch(String[] parametrageBatch) {
        super(parametrageBatch);
    }

    /**
     * L'identifiant du job du flux 4 ciblé par cet exécuteur.
     * 
     * @return l'identifiant du job du flux 4 ciblé par cet exécuteur
     */
    protected abstract String getIdJobFlux4();

    @Override
    protected String getEmplacementFichierConfigurationJob() {
        return "classpath:fr/si2m/red/batch/flux4/job_" + getIdJobFlux4().replace("_h2", "") + ".xml";
    }

    @Override
    protected String getIdJobCible() {
        return "jobFlux4_" + getIdJobFlux4();
    }

    @Override
    protected boolean lanceTraitementAvantJob(GenericXmlApplicationContext contexteExecutionJob) {
        // Aucun contrôle par défaut avant exécution du job
        return true;
    }

}
