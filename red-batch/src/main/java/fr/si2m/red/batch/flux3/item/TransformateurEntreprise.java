package fr.si2m.red.batch.flux3.item;

import lombok.Setter;
import fr.si2m.red.batch.flux3.ligne.Entreprise;
import fr.si2m.red.batch.moteur.item.TransformateurDonnee;
import fr.si2m.red.contrat.Client;
import fr.si2m.red.contrat.ClientRepository;

/**
 * Mapper des entités Entreprise à partir du numéro de Siren fourni.
 * 
 * @author benitahy
 *
 */
public class TransformateurEntreprise extends TransformateurDonnee<String, Entreprise> {

    @Setter
    private ClientRepository clientRepository;

    @Override
    public Entreprise process(String numSiren) throws Exception {
        Client client = clientRepository.getClientDerniereModification(numSiren);
        if (client == null) {
            return null;
        }
        Entreprise entreprise = new Entreprise();
        entreprise.setNumSiren(client.getNumSiren());
        entreprise.setRaisonSocialeEntreprise(client.getRaisonSociale());
        entreprise.setCodeMiseAJour("R");
        entreprise.setCodeEntrepriseVIP("N");
        entreprise.setIdentifiantTechniqueEntreprise(client.getNumSiren());
        entreprise.setNumIdentificationEntrepriseDansOrganisme(0);
        return entreprise;
    }

}
