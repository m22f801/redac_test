package fr.si2m.red.batch.flux3.ligne;

import lombok.Data;
import fr.si2m.red.DateRedac;

/**
 * Description d'une ligne d'un fichier RISQUES à exporter.
 * 
 * @author nortaina
 *
 */
@Data
public class ContratDelegatairePrestation {
    /**
     * Code Mise à jour.
     * 
     * @param codeMiseAJour
     *            Code Mise à jour
     * @return Code Mise à jour
     */
    private String codeMiseAJour;

    /**
     * Identifiant de l’entreprise.
     * 
     * @param identifiantEntrepriseG3C
     *            Identifiant de l’entreprise
     * @return Identifiant de l’entreprise
     */
    private String identifiantEntrepriseG3C;

    /**
     * N° de séquence contrat G3C.
     * 
     * @param numSequenceContratG3C
     *            N° de séquence contrat G3C
     * @return N° de séquence contrat G3C
     */
    private String numSequenceContratG3C;

    /**
     * Code Risque.
     * 
     * @param codeRisque
     *            Code Risque
     * @return Code Risque
     */
    private String codeRisque;

    /**
     * Code organisme délégataire de prestation G3C.
     * 
     * @param codeOrganismeDelegatairePrestationG3C
     *            Code organisme délégataire de prestation G3C
     * @return Code organisme délégataire de prestation G3C
     */
    private String codeOrganismeDelegatairePrestationG3C;

    /**
     * Date début de validité.
     * 
     * @param dateDebutValidite
     *            Date début de validité
     * @return Date début de validité
     */
    private Integer dateDebutValidite;

    /**
     * Date de fin de validité.
     * 
     * @param dateFinValidite
     *            Date de fin de validité
     * @return Date de fin de validité
     */
    private Integer dateFinValidite;

    /**
     * Identifiant technique de l’entreprise.
     * 
     * @param identifiantTechniqueEntreprise
     *            Identifiant technique de l’entreprise
     * @return Identifiant technique de l’entreprise
     */
    private String identifiantTechniqueEntreprise;

    /**
     * Identifiant technique du contrat.
     * 
     * @param identifiantTechniqueContrat
     *            Identifiant technique du contrat
     * @return Identifiant technique du contrat
     */
    private String identifiantTechniqueContrat;

    /**
     * Identifiant technique du délégataire.
     * 
     * @param identifiantTechniqueDelegataire
     *            Identifiant technique du délégataire
     * @return Identifiant technique du délégataire
     */
    private String identifiantTechniqueDelegataire;

    /**
     * Date début de validité.
     * 
     * @return Date début de validité
     */
    public String getDateDebutValiditeFormatee() {
        return DateRedac.formate(DateRedac.EXPORT_FORMAT_PAR_DEFAUT, dateDebutValidite);
    }

    /**
     * Date fin de validité.
     * 
     * @return Date fin de validité
     */
    public String getDateFinValiditeFormatee() {
        return DateRedac.formate(DateRedac.EXPORT_FORMAT_PAR_DEFAUT, dateFinValidite);
    }

}
