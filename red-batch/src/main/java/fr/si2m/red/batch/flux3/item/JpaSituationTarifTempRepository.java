package fr.si2m.red.batch.flux3.item;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import fr.si2m.red.batch.moteur.item.JpaTempRepository;
import fr.si2m.red.contrat.SituationTarif;
import fr.si2m.red.contrat.SituationTarifRepository;

/**
 * Rédacteur d'identifiants de situations tarifaires exportées dans une table de données temporaires pour consultation ultérieure.
 * 
 * @author poidij
 *
 */
@Repository
public class JpaSituationTarifTempRepository extends JpaTempRepository<SituationTarif> implements SituationTarifRepository {

    @Override
    public Class<SituationTarif> getClassePrototypeEntite() {
        return SituationTarif.class;
    }

    @Override
    public List<SituationTarif> getSituationTarifsPourNoco(String numContrat) {
        String sql = "SELECT t.CONBCOT,t.TXCALCU,t.TXBASE1,t.TXBASE2,t.TXBASE3,t.TXBASE4,t.MTCUNCU,t.DT_FIN_SIT,sti.* FROM TMP_RR300_TARIFS_IDS sti "
                + " INNER JOIN TARIFS t ON t.NOCO = sti.NOCO AND t.NOCAT = sti.NOCAT AND t.DT_DEBUT_SIT = sti.DT_DEBUT_SIT" + " INNER JOIN "
                + " (SELECT NOCO, NOCAT, DT_DEBUT_SIT, TXAPPCOT, MIN(DT_DEBUT_SIT_CONTRAT) AS MIN_DT_DEBUT_SIT_CONTRAT "
                + " FROM TMP_RR300_TARIFS_IDS WHERE NOCO = :numContrat GROUP BY NOCAT, DT_DEBUT_SIT, TXAPPCOT) groupedsti "
                + " ON sti.NOCAT = groupedsti.NOCAT " + " AND sti.DT_DEBUT_SIT = groupedsti.DT_DEBUT_SIT AND sti.TXAPPCOT = groupedsti.TXAPPCOT "
                + " AND sti.DT_DEBUT_SIT_CONTRAT = groupedsti.MIN_DT_DEBUT_SIT_CONTRAT " + " WHERE sti.NOCO= :numContrat"
                + " ORDER BY sti.DT_DEBUT_SIT_CONTRAT,sti.DT_DEBUT_SIT,t.CONBCOT";
        Map<String, Object> params = new HashMap<>();
        params.put("numContrat", numContrat);

        return getJdbcTemplate().query(sql, params, new SituationTarifRowMapper());
    }

}
