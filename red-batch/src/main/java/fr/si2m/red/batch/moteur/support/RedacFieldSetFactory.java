package fr.si2m.red.batch.moteur.support;

import java.text.DateFormat;
import java.text.NumberFormat;

import org.springframework.batch.item.file.transform.FieldSet;
import org.springframework.batch.item.file.transform.FieldSetFactory;

/**
 * Usine de {@link FieldSet} propres à REDAC.
 * 
 * @author nortaina
 *
 */
public class RedacFieldSetFactory implements FieldSetFactory {
    private boolean alwaysTrim;

    private DateFormat dateFormat;

    private NumberFormat numberFormat;

    /**
     * The {@link NumberFormat} to use for parsing numbers. If unset the default locale will be used.
     * 
     * @param numberFormat
     *            the {@link NumberFormat} to use for number parsing
     */
    public void setNumberFormat(NumberFormat numberFormat) {
        this.numberFormat = numberFormat;
    }

    /**
     * The {@link DateFormat} to use for parsing numbers. If unset the default pattern is ISO standard <code>yyyy/MM/dd</code>.
     * 
     * @param dateFormat
     *            the {@link DateFormat} to use for date parsing
     */
    public void setDateFormat(DateFormat dateFormat) {
        this.dateFormat = dateFormat;
    }

    /**
     * Si les champs doivent toujours être élagués ou non.
     * 
     * @param alwaysTrim
     *            si les champs doivent toujours être élagués ou non
     */
    public void setAlwaysTrim(boolean alwaysTrim) {
        this.alwaysTrim = alwaysTrim;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public FieldSet create(String[] values, String[] names) {
        RedacFieldSet fieldSet = new RedacFieldSet(values, names);
        return enhance(fieldSet);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public FieldSet create(String[] values) {
        RedacFieldSet fieldSet = new RedacFieldSet(values);
        return enhance(fieldSet);
    }

    private FieldSet enhance(RedacFieldSet fieldSet) {
        if (dateFormat != null) {
            fieldSet.setDateFormat(dateFormat);
        }
        if (numberFormat != null) {
            fieldSet.setNumberFormat(numberFormat);
        }
        fieldSet.setAlwaysTrim(alwaysTrim);
        return fieldSet;
    }

}
