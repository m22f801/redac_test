package fr.si2m.red.batch.flux4.item;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import fr.si2m.red.batch.flux4.support.EffectifCategorieContratTravailCalculateur;
import fr.si2m.red.reconciliation.PeriodeRecue;

/**
 * Caculateur des éléments déclaratifs sur effectif.
 * 
 * @author nortaina
 *
 */
public class CalculElementsDeclaratifsSurEffectifsProcesseur extends CalculElementsDeclaratifsProcesseur {
    private static final Logger LOGGER = LoggerFactory.getLogger(CalculElementsDeclaratifsSurEffectifsProcesseur.class);

    @Autowired
    private EffectifCategorieContratTravailCalculateur effectifCategorieContratTravailCalculateur;

    @Override
    protected void calculElementsDeclaratifs(PeriodeRecue periode) {
        Long idPeriode = periode.getIdPeriode();
        LOGGER.info("Traitement d'une période avec contrat sur effectif : {}", idPeriode);

        long nbContratsPersistes = effectifCategorieContratTravailCalculateur.calculeEtPersiste(periode);
        LOGGER.info("Calcul terminé de {} entités EffectifCategorieContratTravail : {}", nbContratsPersistes, idPeriode);
    }

}
