package fr.si2m.red.batch.flux78.item;

import org.springframework.batch.item.file.transform.BeanWrapperFieldExtractor;
import org.springframework.batch.item.file.transform.FormatterLineAggregator;
import org.springframework.batch.item.file.transform.PassThroughLineAggregator;

/**
 * Aggrégateur de lignes pour les publications du flux 7/8.
 * 
 * @author poidij
 *
 * @param <T>
 *            le type de donnée générique publiée
 */
public class PublicationLineAggregator<T> extends FormatterLineAggregator<T> {

    private static final String DATE_DEBUT_PERIODE = "dateDebutPeriode";
    private static final String DATE_FIN_PERIODE = "dateFinPeriode";
    private static final String REFERENCE_PAIEMENT = "referencePaiement";
    private static final String MODE_PAIEMENT = "modePaiement";
    private static final String MODE_COTISATION = "montantCotisation";

    private static final String FORMAT_PERIODE_CONTRAT_SUR_SALAIRES = "%01d%07d%05d%-3s%08d%08d%-3s%-25s%08d%08d%01d%-1s%01d%-1s%-50s%-3s%05d%05d%-1s%015d%-1s%-40s%-5s%02d%-1s%013d%07d%-22s";
    private static final String FORMAT_PERIODE_CONTRAT_SUR_EFFECTIFS = "%01d%07d%05d%-3s%08d%08d%-3s%-25s%08d%08d%01d%-1s%01d%-1s%-50s%-3s%05d%05d%-1s%015d%-1s%-40s%08d%04d%-1s%-37s";
    private static final String FORMAT_PERIODE_PUBLIEE_GERD = "%8s%-35s%-35s%-15s%08d%08d%-3s%-3s%-35s%-3s%-13s%-40s%-30s%-1s%-4s%-80s%-80s%-80s%-50s%-50s%-50s%-50s%-5s%-50s%-2s%-100s%-30s%08d%-2s%-2s%08d%-13s%-80s%-80s%08d%08d%08d%08d%08d%-1s%08d%08d%-3s%018d%-3s%018d%-3s%018d%-3s%018d%-3s%018d%-80s%-3s%018d%08d%018d";

    private FormatterLineAggregator<PeriodeContratSurSalaires> periodeContratSurSalairesLineAggregator = new FormatterLineAggregator<PeriodeContratSurSalaires>();
    private FormatterLineAggregator<PeriodeContratSurEffectifs> periodeContratSurEffectifsLineAggregator = new FormatterLineAggregator<PeriodeContratSurEffectifs>();
    private FormatterLineAggregator<PeriodePublieeGERD> periodePublieeGERDLineAggregator = new FormatterLineAggregator<PeriodePublieeGERD>();
    private PassThroughLineAggregator<String> periodePublieeHeaderAggregator = new PassThroughLineAggregator<String>();

    /**
     * Liste ordonnée des fields pour une ligne de contrat sur salaires, fichier WQUI.
     */
    private static final String[] CHAMPS_PERIODE_CONTRAT_SUR_SALAIRES = { "typeEnregistrement", "contratZoneA", "contratZoneB", "contratZoneC",
            DATE_DEBUT_PERIODE, DATE_FIN_PERIODE, "categorieQuittancement", "libelleCategorieQuittancement", "dateDebutPeriodeTarification",
            "dateFinPeriodeTarification", "numTranche", "typeBase", "typeDocument", "periodeComplementaireOuCorrective", REFERENCE_PAIEMENT,
            MODE_PAIEMENT, "effectifDebutPeriode", "effectifFinDecembre", "signeMontantCotisation", MODE_COTISATION, "modeDeclaration",
            "fillerPartieCommune", "libelleTranche", "codeNatureBase", "signeMontantSalaire", "montantSalaire", "tauxCotisation",
            "fillerPartieSpecifique" };

    /**
     * Liste ordonnée des fields pour une ligne de contrat sur effectifs, fichier WQUI.
     */
    private static final String[] CHAMPS_PERIODE_CONTRAT_SUR_EFFECTIFS = { "typeEnregistrement", "contratZoneA", "contratZoneB", "contratZoneC",
            DATE_DEBUT_PERIODE, DATE_FIN_PERIODE, "categorieQuittancement", "libelleCategorieQuittancement", "dateDebutPeriodeTarification",
            "dateFinPeriodeTarification", "numTranche", "typeBase", "typeDocument", "periodeComplementaireOuCorrective", REFERENCE_PAIEMENT,
            MODE_PAIEMENT, "effectifDebutPeriode", "effectifFinDecembre", "signeMontantCotisation", MODE_COTISATION, "modeDeclaration",
            "fillerPartieCommune", "dateEffetIR", "nombreIR", "incorporeRadie", "fillerPartieSpecifique" };

    /**
     * Liste ordonnée des fields pour une ligne de contrat, fichier GERD.
     */
    private static final String[] CHAMPS_PERIODE_PUBLIEE_GERD = { "dateEnvoi", "idEntreprise", "nomEntreprise", "numContrat", DATE_DEBUT_PERIODE,
            DATE_FIN_PERIODE, "codePopulation", "typeFlux", "refMessage", "codeDevise", "nir", "ntt", "numeroAdherent", "codeSexe", "codeCivilite",
            "nomFamilleAdherent", "nomUsageAdherent", "prenomAdherent", "complConstruction", "libelleAdresse", "complVoie", "codeDistribution",
            "codePostal", "ville", "codePays", "mel", "lieuNaissance", "dateNaissance", "departementNaissance", "paysNaissance", "dateModification",
            "ancienIdentifiant", "ancienNomFamille", "ancienPrenoms", "ancienneDateNaissance", "dateAdhesion", "dateRadiation",
            "dateDernierJourTravail", "dateRepriseTravail", "presenceDansEntreprise", "dateDebutCotisation", "dateFinCotisation",
            "natureMontantSalaireTrancheA", "montantSalaireTrancheA", "natureMontantSalaireTrancheB", "montantSalaireTrancheB",
            "natureMontantSalaireTrancheC", "montantSalaireTrancheC", "natureMontantSalaireTrancheD", "montantSalaireTrancheD",
            "natureMontantCotisation", MODE_COTISATION, REFERENCE_PAIEMENT, MODE_PAIEMENT, "montantPaiement", "datePaiement",
            "montantTelereglement" };

    /**
     * Constructeur par défaut.
     * 
     */
    public PublicationLineAggregator() {
        periodeContratSurSalairesLineAggregator.setFormat(FORMAT_PERIODE_CONTRAT_SUR_SALAIRES);
        BeanWrapperFieldExtractor<PeriodeContratSurSalaires> extracteurChampsSurSalaires = new BeanWrapperFieldExtractor<PeriodeContratSurSalaires>();
        extracteurChampsSurSalaires.setNames(CHAMPS_PERIODE_CONTRAT_SUR_SALAIRES);
        periodeContratSurSalairesLineAggregator.setFieldExtractor(extracteurChampsSurSalaires);

        periodeContratSurEffectifsLineAggregator.setFormat(FORMAT_PERIODE_CONTRAT_SUR_EFFECTIFS);
        BeanWrapperFieldExtractor<PeriodeContratSurEffectifs> extracteurChampsSurEffectifs = new BeanWrapperFieldExtractor<PeriodeContratSurEffectifs>();
        extracteurChampsSurEffectifs.setNames(CHAMPS_PERIODE_CONTRAT_SUR_EFFECTIFS);
        periodeContratSurEffectifsLineAggregator.setFieldExtractor(extracteurChampsSurEffectifs);

        periodePublieeGERDLineAggregator.setFormat(FORMAT_PERIODE_PUBLIEE_GERD);
        BeanWrapperFieldExtractor<PeriodePublieeGERD> extracteurChampsGERD = new BeanWrapperFieldExtractor<PeriodePublieeGERD>();
        extracteurChampsGERD.setNames(CHAMPS_PERIODE_PUBLIEE_GERD);
        periodePublieeGERDLineAggregator.setFieldExtractor(extracteurChampsGERD);
    }

    @Override
    public String aggregate(T item) {
        String result = null;
        if (item instanceof PeriodeContratSurSalaires) {
            result = this.periodeContratSurSalairesLineAggregator.aggregate((PeriodeContratSurSalaires) item);
        } else if (item instanceof PeriodeContratSurEffectifs) {
            result = this.periodeContratSurEffectifsLineAggregator.aggregate((PeriodeContratSurEffectifs) item);
        } else if (item instanceof PeriodePublieeGERD) {
            result = this.periodePublieeGERDLineAggregator.aggregate((PeriodePublieeGERD) item);
        } else if (item instanceof String) {
            result = periodePublieeHeaderAggregator.aggregate((String) item);

        }

        return result;
    }
}
