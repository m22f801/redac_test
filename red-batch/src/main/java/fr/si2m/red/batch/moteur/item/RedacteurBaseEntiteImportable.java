package fr.si2m.red.batch.moteur.item;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.util.Assert;
import org.springframework.util.CollectionUtils;

import fr.si2m.red.EntiteImportableBatch;
import fr.si2m.red.batch.moteur.EtapeCodeRetourModificateur;
import fr.si2m.red.core.repository.EntiteImportableRepository;

/**
 * Gestionnaire d'insertion de données en base.
 * 
 * @author nortaina
 *
 * @param <T>
 *            le type de données
 */
public class RedacteurBaseEntiteImportable<T extends EntiteImportableBatch> extends EtapeCodeRetourModificateur implements ItemWriter<T>, InitializingBean {
    private static final Logger LOGGER = LoggerFactory.getLogger(RedacteurBaseEntiteImportable.class);

    private EntiteImportableRepository<T> redacRepository;

    /**
     * Indique si le rédacteur doit vérifier que des doublons existent également en base (à mettre à false si la vérification a déjà été effectuée).
     */
    @Getter
    @Setter
    private boolean verifieDoublonsBase = true;

    /**
     * Récupère le référentiel du type de données ciblées.
     *
     * @return le référentiel du type de données ciblées
     */
    protected EntiteImportableRepository<T> getRedacRepository() {
        return redacRepository;
    }

    /**
     * Met à jour le référentiel du type de données ciblées.
     *
     * @param redacRepository
     *            le référentiel du type de données ciblées à mettre à jour
     */
    public void setRedacRepository(EntiteImportableRepository<T> redacRepository) {
        this.redacRepository = redacRepository;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        Assert.notNull(getRedacRepository(), "Le référentiel REDAC doit obligatoirement être paramétré");
        super.afterPropertiesSet();
    }

    @Override
    public void write(List<? extends T> donnees) {
        if (donnees == null || donnees.isEmpty()) {
            LOGGER.info("Aucune insertion de données effectuée !");
            return;
        }

        // Gestion des doublons au préalable
        List<? extends T> sansDoublons = retireEtMarqueDoublons(donnees);

        // Filtre des données invalides
        List<? extends T> donneesSansDoublonsEtValides = filtreDonneesValides(sansDoublons);

        EntiteImportableRepository<T> referentielCible = getRedacRepository();

        if (!donneesSansDoublonsEtValides.isEmpty()) {
            if (verifieDoublonsBase) {
                List<T> listeDoublonsEnBase = referentielCible.getEntiteExistantes(donneesSansDoublonsEtValides);
                for (T doublon : listeDoublonsEnBase) {
                    enregistreErreurDoublon(doublon);
                }
                donneesSansDoublonsEtValides.removeAll(listeDoublonsEnBase);
            }
            referentielCible.importeEnMasseEntitesTemporaires(donneesSansDoublonsEtValides);
        }

    }

    /**
     * Retire en marquant en erreur les doublons d'une liste.
     * 
     * @param donnees
     *            les données à traiter
     * @return la liste des données sans les doublons
     */
    private List<? extends T> retireEtMarqueDoublons(List<? extends T> donnees) {
        List<? extends T> sansDoublons = new ArrayList<T>(new LinkedHashSet<T>(donnees));
        if (sansDoublons.size() != donnees.size()) {
            LOGGER.error("Détection de doublons parmi les entités de ce morceau...");
            // Recherche des doublons...
            for (T donnee : donnees) {
                if (donnee.isLigneSyntaxiquementValide() && !CollectionUtils.containsInstance(sansDoublons, donnee)) {
                    enregistreErreurDoublon(donnee);
                }
            }
        }
        return sansDoublons;
    }

    /**
     * Filtre sur les données valides d'une liste.
     * 
     * @param donnees
     *            les données à filtrer
     * @return les données valides
     */
    private List<? extends T> filtreDonneesValides(List<? extends T> donnees) {
        List<? extends T> donneesValides = new ArrayList<T>(donnees);
        Iterator<? extends T> it = donneesValides.iterator();
        while (it.hasNext()) {
            T donnee = it.next();
            if (!donnee.isImportValide()) {
                it.remove();
            }
        }
        return donneesValides;
    }

    /**
     * Collecte une erreur d'unicité sur une entité.
     * 
     * @param doublon
     *            l'entité en double
     */
    private void enregistreErreurDoublon(T doublon) {
        doublon.enregistreErreurSurImport("La ligne ne respecte pas la règle d'unicité");
    }
}
