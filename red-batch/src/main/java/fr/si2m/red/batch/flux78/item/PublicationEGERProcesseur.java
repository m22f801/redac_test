package fr.si2m.red.batch.flux78.item;

import java.math.BigInteger;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.lang3.StringUtils;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.annotation.BeforeStep;
import org.springframework.beans.factory.annotation.Autowired;

import fr.si2m.red.DateRedac;
import fr.si2m.red.batch.moteur.item.TransformateurDonneeCodeRetourModificateur;
import fr.si2m.red.dsn.AdhesionEtablissementMois;
import fr.si2m.red.dsn.ContratTravail;
import fr.si2m.red.dsn.ContratTravailRepository;
import fr.si2m.red.dsn.Individu;
import fr.si2m.red.dsn.IndividuRepository;
import fr.si2m.red.parametrage.ParamCodePaysRepository;
import lombok.Setter;

/**
 * Création de lignes de périodes publiées pour EGER.
 * 
 * @author eudesr
 *
 */
public class PublicationEGERProcesseur extends TransformateurDonneeCodeRetourModificateur<PeriodePublieeEGER, PeriodePublieeEGER> {

    /**
     * Le code sexe masuculin.
     */
    public static final String CODE_CIVILITE_M = "M.";

    /**
     * Le code sexe féminin.
     */
    public static final String CODE_CIVILITE_F = "MME";

    @Autowired
    private IndividuRepository individuRepository;
    @Autowired
    private ParamCodePaysRepository paramCodePaysRepository;
    @Autowired
    private ContratTravailRepository contratTravailRepository;

    @PersistenceContext
    private EntityManager entityManager;

    @Setter
    private String cheminAbsoluFichierLogErreurs;
    @Setter
    private Integer dateDuJour;

    DecimalFormat formatMontantPositif;
    DecimalFormat formatMontantNegatif;

    /**
     * Constructeur
     */
    private PublicationEGERProcesseur() {
        super();

        DecimalFormatSymbols decimaleFR = new DecimalFormatSymbols(Locale.FRANCE);
        decimaleFR.setDecimalSeparator(',');

        // initialisation du formater pour le montant Positif
        this.formatMontantPositif = new DecimalFormat();

        this.formatMontantPositif.setDecimalFormatSymbols(decimaleFR);
        this.formatMontantPositif.setMaximumIntegerDigits(12);
        this.formatMontantPositif.setMaximumFractionDigits(2);
        this.formatMontantPositif.setGroupingUsed(false);

        // initialisation du formater pour le montant Negatif
        this.formatMontantNegatif = new DecimalFormat();
        this.formatMontantNegatif.setDecimalFormatSymbols(decimaleFR);
        this.formatMontantNegatif.setMaximumIntegerDigits(11);
        this.formatMontantNegatif.setMaximumFractionDigits(2);
        this.formatMontantNegatif.setGroupingUsed(false);

    }

    @Override
    public PeriodePublieeEGER process(PeriodePublieeEGER periode) throws Exception {

        Individu indiv = individuRepository.getPourPeriode(periode.getIdPeriode(), periode.getIndividu());

        if (indiv == null) {
            String mesageErreur = "L'individu NIR/NTT=" + periode.getIndividu() + " n'existe pas pour la période : [PeriodeRecue(idPeriode="
                    + periode.getIdPeriode() + ", numeroContrat=" + periode.getNumeroContrat() + ", dateDebutPeriode="
                    + periode.getDateDebutPeriodeAsInteger() + ", dateFinPeriode=" + periode.getDateFinPeriodeAsInteger() + ", typePeriode="
                    + periode.getTypePeriode() + ", dateCreation=" + periode.getDateCreation() + ")]";
            traceErreurSansBlocage(mesageErreur, cheminAbsoluFichierLogErreurs);
            return null;
        } else {
            entityManager.detach(indiv);

            AdhesionEtablissementMois adhesion = indiv.getAdhesionEtablissementMois();
            ContratTravail contratTravail = contratTravailRepository.getDernierContratTravailPourIndividu(indiv.getIdIndividu());

            periode.setDateEnvoi(DateRedac.convertionDateRedacSansValidation(this.dateDuJour));

            periode.setSiret(StringUtils.substring(adhesion.getSirenEntreprise(), 0, 14));
            periode.setRaisonSocialeEntreprise(StringUtils.substring(adhesion.getRaisonSociale(), 0, 38));

            // F78_RG_P3_13
            if (periode.isAvecPopulation()) {
                periode.setLibPopulation(StringUtils.substring(periode.getLibPopulation(), 0, 40));
                periode.setCodePopulation(StringUtils.substring(periode.getCodePopulation(), 0, 20));
            } else {
                periode.setLibPopulation(null);
                periode.setCodePopulation(null);
            }

            periode.setNumeroContrat(periode.getNumeroContrat());
            periode.setDateDebutPeriode(DateRedac.convertionDateRedacSansValidation(periode.getDateDebutPeriodeAsInteger()));
            periode.setDateFinPeriode(DateRedac.convertionDateRedacSansValidation(periode.getDateFinPeriodeAsInteger()));

            // données cotisants
            periode.setCivilite(remplissageChampCivilite(indiv));
            if (indiv.getNomFamille() != null && !indiv.getNomFamille().equals(indiv.getNomUsage())) {
                periode.setNomMarital(StringUtils.substring(indiv.getNomUsage(), 0, 38));
            }
            periode.setNomFamilleAdherent(StringUtils.substring(indiv.getNomFamille(), 0, 38));
            periode.setPrenomAdherent(StringUtils.substring(indiv.getPrenom(), 0, 20));
            periode.setDateNaissance(DateRedac.convertionDateRedacSansValidation(indiv.getDateNaissance()));
            periode.setCodePostalNaissance(remplissageCodePostalAvecDepartement(indiv.getDepartementNaissance()));
            periode.setVilleNaissance(indiv.getLieuNaissance());
            periode.setPaysNaissance(paramCodePaysRepository.getCodePaysInsee(indiv.getPaysNaissance()));

            // F78_RG_P3_17
            if (!StringUtils.isEmpty(indiv.getIdentifiantRepertoire())) {
                BigInteger ssNir;

                // remplacer le département "2A" par "19" et le département "2B" par "18" dans le NIR pour réaliser le calcul
                if (indiv.getIdentifiantRepertoire().contains("2A")) {
                    ssNir = new BigInteger(indiv.getIdentifiantRepertoire().replace("2A", "19"));
                } else if (indiv.getIdentifiantRepertoire().contains("2B")) {
                    ssNir = new BigInteger(indiv.getIdentifiantRepertoire().replace("2B", "18"));
                } else {
                    ssNir = new BigInteger(indiv.getIdentifiantRepertoire());
                }

                BigInteger valeurModulo = new BigInteger("97");

                // complément à 97 du nombre formé par les 13 premiers chiffres du Individu.NIR modulo 97 ,
                // soit 97 – (Individu.NIR modulo 97)
                BigInteger cleControle = valeurModulo.subtract(ssNir.mod(valeurModulo));

                periode.setNumeroSecuriteSociale(indiv.getIdentifiantRepertoire() + String.format("%02d", cleControle));
            }

            // adresse
            periode.setComplDestinataire(StringUtils.substring(indiv.getComplementConstruction(), 0, 38));
            periode.setComplAdresse(StringUtils.substring(indiv.getComplementVoie(), 0, 38));
            periode.setNumVoie(StringUtils.substring(indiv.getVoie(), 0, 38));
            periode.setLieuDit(StringUtils.substring(indiv.getCodeDistribution(), 0, 38));
            periode.setCodePostal(indiv.getCodePostal());
            periode.setLocalite(StringUtils.substring(indiv.getLocalite(), 0, 38));
            periode.setCodePaysInsee(paramCodePaysRepository.getCodePaysInsee(indiv.getCodePays()));

            // données mouvement cotisant
            periode.setDateEntree(DateRedac.convertionDateRedacSansValidation(contratTravail != null ? contratTravail.getDateDebutContrat() : null));
            periode.setDateSortie(DateRedac.convertionDateRedacSansValidation(contratTravail != null ? contratTravail.getDateFinContrat() : null));
            periode.setMotifSortie(contratTravail != null && !StringUtils.isEmpty(contratTravail.getMotifRupture()) ? "FCT" : StringUtils.EMPTY);
            periode.setDebutPeriodeCotisation(DateRedac.convertionDateRedacSansValidation(periode.getDateDebutPeriodeAsInteger()));
            periode.setFinPeriodeCotisation(DateRedac.convertionDateRedacSansValidation(periode.getDateFinPeriodeAsInteger()));
            periode.setMontantCotisation(
                    periode.getMontantCotisationAsDouble() > 0d ? formatMontantPositif.format(periode.getMontantCotisationAsDouble())
                            : formatMontantNegatif.format(periode.getMontantCotisationAsDouble()));

            // regime sécurité sociale - non renseigné

            // assiettes & taux non renseignés

            return periode;

        }

    }

    /**
     * Détermine le champ civilité d'un individu
     * 
     * @param individuRecent
     *            l'individu
     * @return le champ civilite
     */
    private String remplissageChampCivilite(Individu individuRecent) {
        String codeCivilite = " ";
        if (StringUtils.equals(individuRecent.getSexe(), "01")) {
            codeCivilite = CODE_CIVILITE_M;
        } else if (StringUtils.equals(individuRecent.getSexe(), "02")) {
            codeCivilite = CODE_CIVILITE_F;
        } else if (StringUtils.isNotBlank(individuRecent.getIdentifiantRepertoire())) {
            if (individuRecent.getIdentifiantRepertoire().startsWith("1")) {
                codeCivilite = CODE_CIVILITE_M;
            } else if (individuRecent.getIdentifiantRepertoire().startsWith("2")) {
                codeCivilite = CODE_CIVILITE_F;
            }
        }

        return codeCivilite;
    }

    /**
     * Détermine le codePostal à partir du département
     * 
     * @param departement
     *            le département
     * @return codePostal
     */
    private String remplissageCodePostalAvecDepartement(String departement) {
        if ("2A".equals(departement) || "2B".equals(departement)) {
            return "20000";
        } else {
            return departement + "000";
        }
    }

    /**
     * Sauvegarde du contexte, necessaire pour Modification du code Retour
     * 
     * @param stepExecution
     */
    @BeforeStep
    protected void saveStepExecution(StepExecution stepExecution) {
        this.stepExecution = stepExecution;
    }

}
