package fr.si2m.red.batch.facade;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.si2m.red.batch.moteur.CodeRetour;
import fr.si2m.red.batch.moteur.execution.ExecuteurBatch;

/**
 * Façade d'exécution des batchs REDAC.
 * 
 * @author nortaina
 * 
 */
public final class BatchFacade {
    private static final Logger LOGGER = LoggerFactory.getLogger(BatchFacade.class);

    /**
     * Utilitaire.
     */
    private BatchFacade() {
    }

    /**
     * Lanceur de batch via lignes de commande.
     * 
     * @param args
     *            les paramètres d'exécution du batch - le premier paramètre correspond à l'identifiant du batch à lancer, les suivants correspondent au
     *            paramétrage du batch
     * 
     */
    public static void main(String[] args) {
        System.exit(executeBatch(args));
    }

    /**
     * Lance un batch.
     * 
     * @param args
     *            les paramètres d'exécution du batch - le premier paramètre correspond à l'identifiant du batch à lancer, les suivants correspondent au
     *            paramétrage du batch
     * 
     * @return le code de retour d'exécution du batch
     */
    public static int executeBatch(String[] args) {
        // Contrôles préalables
        if (args == null || args.length < 1 || !RegistreExecuteursBatchs.getBatchsExecutables().contains(args[0])) {
            LOGGER.error("Le batch exécutable spécifié n'existe pas");
            return CodeRetour.ERREUR_TECHNIQUE_BLOQUANTE.getCode();
        }

        // Préparation des paramètres de lancement
        List<String> manipulateurParametresFacade = new ArrayList<String>(Arrays.asList(args));
        String nomBatch = manipulateurParametresFacade.get(0);
        manipulateurParametresFacade.remove(0);
        String[] parametrageBatch = manipulateurParametresFacade.toArray(new String[manipulateurParametresFacade.size()]);

        // Exécution du batch
        ExecuteurBatch executeurBatch = RegistreExecuteursBatchs.getExecuteurPourBatch(nomBatch, parametrageBatch);
        return executeurBatch.executeJob();
    }
}
