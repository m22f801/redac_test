package fr.si2m.red.batch.flux78.support;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import fr.si2m.red.batch.flux78.item.PeriodePublieeEGER;
import fr.si2m.red.core.repository.jdbc.JdbcRepository;

/**
 * Un mapper de ligne {@link PeriodePublieeEGER} pour supporter la récupération de données via JDBC.
 * 
 * @author eudesr
 *
 */
public class PeriodePublieeEGERRowMapper implements RowMapper<PeriodePublieeEGER> {

    @Override
    public PeriodePublieeEGER mapRow(ResultSet rs, int rowNum) throws SQLException {
        PeriodePublieeEGER periodeEGER = new PeriodePublieeEGER();
        periodeEGER.setIdPeriode(rs.getLong("ID_PERIODE"));
        periodeEGER.setDateCreation(JdbcRepository.getInteger(rs, "DATE_CREATION"));
        periodeEGER.setTypePeriode(rs.getString("TYPE_PERIODE"));
        periodeEGER.setNumeroContrat(rs.getString("NUMERO_CONTRAT"));
        periodeEGER.setDateDebutPeriodeAsInteger(JdbcRepository.getInteger(rs, "DATE_DEBUT_PERIODE"));
        periodeEGER.setDateFinPeriodeAsInteger(JdbcRepository.getInteger(rs, "DATE_FIN_PERIODE"));
        periodeEGER.setLibPopulation(rs.getString("LICAT"));
        periodeEGER.setCodePopulation(rs.getString("NOCAT"));
        periodeEGER.setIndividu(rs.getString("INDIVIDU"));
        periodeEGER.setAssureur(rs.getString("PORTEFEUILLE"));
        periodeEGER.setAvecPopulationAsText(rs.getString("AVEC_POPULATION"));

        Double montant = rs.getDouble("MT_COTISATION");
        if (rs.wasNull()) {
            periodeEGER.setMontantCotisationAsDouble(null);
        } else {
            periodeEGER.setMontantCotisationAsDouble(montant);
        }

        return periodeEGER;
    }
}
