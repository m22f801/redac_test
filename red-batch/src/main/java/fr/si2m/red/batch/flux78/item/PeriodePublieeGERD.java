package fr.si2m.red.batch.flux78.item;

import lombok.Data;

/**
 * Modèle des périodes publiées pour GERD.
 * 
 * @author poidij
 *
 */
@Data
public class PeriodePublieeGERD {

    // Informations Entreprise/contrat/période
    private Long idPeriode;
    private String dateEnvoi;
    private String idEntreprise;
    private String nomEntreprise;
    private String numContrat;
    private Integer dateDebutPeriode;
    private Integer dateFinPeriode;
    private String codePopulation;
    private String typeFlux;
    private String refMessage;
    private String codeDevise;

    // Informations adhérent
    private String nir;
    private String ntt;
    private String numeroAdherent;
    private String codeSexe;
    private String codeCivilite;
    private String nomFamilleAdherent;
    private String nomUsageAdherent;
    private String prenomAdherent;

    // Adresse
    private String complConstruction;
    private String libelleAdresse;
    private String complVoie;
    private String codeDistribution;
    private String codePostal;
    private String ville;
    private String codePays;

    // Caractéristiques adhérent
    private String mel;
    private String lieuNaissance;
    private Integer dateNaissance;
    private String departementNaissance;
    private String paysNaissance;

    // Changement adhérent

    private Integer dateModification;
    private String ancienIdentifiant;
    private String ancienNomFamille;
    private String ancienPrenoms;
    private Integer ancienneDateNaissance;

    // Informations adhésion / radiation
    private Integer dateAdhesion;
    private Integer dateRadiation;
    private Integer dateDernierJourTravail;
    private Integer dateRepriseTravail;
    private String presenceDansEntreprise;

    // Informations cotisations
    private Integer dateDebutCotisation;
    private Integer dateFinCotisation;
    private String natureMontantSalaireTrancheA;
    private Long montantSalaireTrancheA;
    private String natureMontantSalaireTrancheB;
    private Long montantSalaireTrancheB;
    private String natureMontantSalaireTrancheC;
    private Long montantSalaireTrancheC;
    private String natureMontantSalaireTrancheD;
    private Long montantSalaireTrancheD;
    private String natureMontantCotisation;
    private Long montantCotisation;

    // Informations paiement
    private String referencePaiement;
    private String modePaiement;
    private Long montantPaiement;
    private Integer datePaiement;
    private Long montantTelereglement;

}
