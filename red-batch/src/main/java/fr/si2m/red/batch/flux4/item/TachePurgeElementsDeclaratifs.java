package fr.si2m.red.batch.flux4.item;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;

import fr.si2m.red.batch.moteur.EtapeCodeRetourModificateur;
import fr.si2m.red.reconciliation.PeriodeRecueRepository;
import lombok.Setter;

/**
 * Tâche de purge des éléments déclaratifs avant leur calcul pour les périodes à traiter.
 * 
 * @author poidij
 *
 */
public class TachePurgeElementsDeclaratifs extends EtapeCodeRetourModificateur implements Tasklet {
    private static final Logger LOGGER = LoggerFactory.getLogger(TachePurgeElementsDeclaratifs.class);

    @Setter
    private PeriodeRecueRepository periodeRecueRepository;

    @Override
    public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) throws Exception {
        this.saveStepExecution(chunkContext.getStepContext().getStepExecution());

        try {
            String sql = "call Purger_elements_declaratifs_en_masse(:listeIdsPeriodes)";
            Map<String, Object> params = new HashMap<>();
            params.put("listeIdsPeriodes", "ALL");

            LOGGER.info("Appel de la procédure stockée Purger_elements_declaratifs_en_masse avec les paramètres {}", params);
            periodeRecueRepository.callMysqlProcedure(sql, params);
            LOGGER.info("Fin de la procédure stockée Purger_elements_declaratifs_en_masse");
        } catch (Exception e) {
            LOGGER.info("Erreur lors du traitement des erreurs", e);
        }
        return RepeatStatus.FINISHED;
    }

}
