package fr.si2m.red.batch.flux4.item;

import lombok.Setter;

import org.apache.commons.lang3.StringUtils;

import fr.si2m.red.batch.RedacMessages;
import fr.si2m.red.batch.moteur.erreur.ValidateurLigneAvecCollecte;
import fr.si2m.red.dsn.BaseAssujettieRepository;
import fr.si2m.red.dsn.ComposantBaseAssujettie;
import fr.si2m.red.dsn.ComposantBaseAssujettieRepository;

/**
 * Validateur de ligne de paramétrage code libellé.
 * 
 * @author benitahy
 *
 */
public class ComposantBaseAssujettieValidateurLigne extends ValidateurLigneAvecCollecte<ComposantBaseAssujettie> {

    @Setter
    private BaseAssujettieRepository baseAssujettieRepository;

    @Setter
    private ComposantBaseAssujettieRepository composantBaseAssujettieRepository;

    @Override
    protected void valide(final ComposantBaseAssujettie composantBaseAssujettie) {

        // unicité
        valideChampAvecCollecte(!composantBaseAssujettieRepository.existeUnComposantBaseAssujettie(composantBaseAssujettie.getIdComposantBaseAssujettie()),
                composantBaseAssujettie, "ID", "La ligne ne respecte pas la règle d'unicité");

        // Champs obligatoires
        valideChampAvecCollecte(StringUtils.isNotBlank(composantBaseAssujettie.getIdComposantBaseAssujettie()), composantBaseAssujettie, "ID_COMPO_BASE_ASSUJETTIE",
                "Le champ ID_COMPO_BASE_ASSUJETTIE obligatoire n'est pas renseigné");
        valideChampAvecCollecte(StringUtils.isNotBlank(composantBaseAssujettie.getIdBaseAssujettie()), composantBaseAssujettie, "ID_BASE_ASSUJETTIE",
                "Le champ ID_BASE_ASSUJETTIE obligatoire n'est pas renseigné");
        valideChampAvecCollecte(StringUtils.isNotBlank(composantBaseAssujettie.getTypeComposantBaseAssujettie()), composantBaseAssujettie, "TYPE_COMPO_BASE_ASSUJ",
                "Le champ TYPE_COMPO_BASE_ASSUJ obligatoire n'est pas renseigné");
        valideChampAvecCollecte(composantBaseAssujettie.getMontantComposantBaseAssujettie() != null, composantBaseAssujettie, "MONTANT_COMPO_BASE_ASSUJ",
                "Le champ MONTANT_COMPO_BASE_ASSUJ obligatoire n'est pas renseigné");

        // Longueur des champs
        valideTailleFixeChampAvecCollecte(composantBaseAssujettie.getIdComposantBaseAssujettie(), 30, composantBaseAssujettie, "ID_COMPO_BASE_ASSUJETTIE");
        valideTailleFixeChampAvecCollecte(composantBaseAssujettie.getIdBaseAssujettie(), 30, composantBaseAssujettie, "ID_BASE_ASSUJETTIE");
        valideTailleFixeChampAvecCollecte(composantBaseAssujettie.getTypeComposantBaseAssujettie(), 2, composantBaseAssujettie, "TYPE_COMPO_BASE_ASSUJ");
        valideChampNumeriqueDecimaleTailleFixeAvecCollecte(composantBaseAssujettie.getMontantComposantBaseAssujettieAsText(), 16, 2, composantBaseAssujettie,
                "MONTANT_COMPO_BASE_ASSUJ");

        validationCorrespondanceReferentielle(composantBaseAssujettie);
    }

    /**
     * Validation des liens entre tables.
     * 
     * @param composantBaseAssujettie
     *            Le composant base assujettie dont la base assujettie doit être validée.
     */
    private void validationCorrespondanceReferentielle(final ComposantBaseAssujettie composantBaseAssujettie) {
        if (StringUtils.isNotBlank(composantBaseAssujettie.getIdBaseAssujettie())) {
            boolean existeUneBaseAssujettie = baseAssujettieRepository.existeUneBaseAssujettie(composantBaseAssujettie.getIdBaseAssujettie());
            valideChampAvecCollecte(existeUneBaseAssujettie, composantBaseAssujettie, "IdBaseAssujettie", RedacMessages.ERREUR_REFERENCE_FLUX4);
        }
    }
}
