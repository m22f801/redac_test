package fr.si2m.red.batch.flux10.item;

import org.apache.commons.lang3.StringUtils;

import fr.si2m.red.batch.moteur.erreur.ValidateurLigneAvecCollecte;
import fr.si2m.red.parametrage.ParamControleSignalRejet;

/**
 * Validateur de ligne de paramétrage Controle Signal Rejet.
 * 
 * @author delortj
 *
 */
public class ParamControleSignalRejetValidateurLigne extends ValidateurLigneAvecCollecte<ParamControleSignalRejet> {

    @Override
    protected void valide(final ParamControleSignalRejet controleSignalRejet) {
        // Champs obligatoires
        // F10_RG_C87
        valideChampAvecCollecte(StringUtils.isNotBlank(controleSignalRejet.getIdentifiantControle()), controleSignalRejet, "ID_CONTROLE",
                "le champ ID_CONTROLE obligatoire, n’est pas renseigné");
        // F10_RG_C107
        valideChampAvecCollecte(StringUtils.isNotBlank(controleSignalRejet.getNofam()), controleSignalRejet, "NOFAM",
                "le champ NOFAM obligatoire, n’est pas renseigné");

        // Longueur des champs
        // F10_RG_C87
        if (StringUtils.isNotBlank(controleSignalRejet.getIdentifiantControle())) {
            valideChampAvecCollecte(controleSignalRejet.getIdentifiantControle().length() <= 30, controleSignalRejet, "ID_CONTROLE",
                    "le champ ID_CONTROLE dépasse la taille maximale prévue");
        }
        // F10_RG_C107
        if (StringUtils.isNotBlank(controleSignalRejet.getNofam())) {
            valideChampAvecCollecte(controleSignalRejet.getNofam().length() <= 3, controleSignalRejet, "NOFAM",
                    "le champ NOFAM dépasse la taille maximale prévue");
        }
        // F10_RG_C88
        valideChampAvecCollecte(controleSignalRejet.getNiveauAlerte().length() <= 10, controleSignalRejet, "NIVEAU_ALERTE",
                "le champ NIVEAU_ALERTE dépasse la taille maximale prévue");
        // F10_RG_C89
        valideChampAvecCollecte(controleSignalRejet.getMessageAlerte().length() <= 100, controleSignalRejet, "MSG_ALERTE",
                "le champ MSG_ALERTE dépasse la taille maximale prévue");

    }

}
