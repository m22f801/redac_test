package fr.si2m.red.batch.flux1.item;

import fr.si2m.red.DateRedac;
import fr.si2m.red.batch.moteur.item.ModificateurChampsLigneValide;
import fr.si2m.red.contrat.Tarif;
import fr.si2m.red.contrat.TarifRepository;
import lombok.Setter;

/**
 * Modificateur des dates de fin de situation des tarifs.
 * 
 * @author poidij
 *
 */
public class ModificateurDateFinSituationTarif extends ModificateurChampsLigneValide<Tarif> {

    @Setter
    private TarifRepository tarifRepository;

    @Override
    protected void modifieChamps(Tarif item) {
        // F01_RG_T01
        Integer dateDebutSituationTarifSuivante = tarifRepository.getDateDebutDeLaSituationTarifTemporaireSuivante(item.getNumContrat(),
                item.getDateDebutSituationLigne());

        if (dateDebutSituationTarifSuivante != null && dateDebutSituationTarifSuivante != 0) {
            item.setDateFinSituationLigne(
                    DateRedac.retirerALaDate(dateDebutSituationTarifSuivante, 1, DateRedac.MODIFIER_JOUR, "dateDebutSituationTarifSuivante"));
        }
    }

}
