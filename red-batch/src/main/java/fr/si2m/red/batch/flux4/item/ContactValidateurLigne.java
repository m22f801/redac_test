package fr.si2m.red.batch.flux4.item;

import lombok.Setter;

import org.apache.commons.lang3.StringUtils;

import fr.si2m.red.batch.RedacMessages;
import fr.si2m.red.batch.moteur.erreur.ValidateurLigneAvecCollecte;
import fr.si2m.red.dsn.AdhesionEtablissementMoisRepository;
import fr.si2m.red.dsn.Contact;
import fr.si2m.red.dsn.ContactRepository;

/**
 * Validateur de ligne de paramétrage code libellé.
 * 
 * @author benitahy
 *
 */
public class ContactValidateurLigne extends ValidateurLigneAvecCollecte<Contact> {

    @Setter
    private AdhesionEtablissementMoisRepository adhesionEtablissementMoisRepository;

    @Setter
    private ContactRepository contactRepository;

    @Override
    protected void valide(final Contact contact) {

        // unicité
        valideChampAvecCollecte(!contactRepository.existeUnContact(contact.getIdContact()), contact, "ID", "La ligne ne respecte pas la règle d'unicité");

        // Champs obligatoires
        valideChampAvecCollecte(StringUtils.isNotBlank(contact.getIdContact()), contact, "ID_CONTACT", "Le champ ID_CONTACT obligatoire n'est pas renseigné");
        valideChampAvecCollecte(StringUtils.isNotBlank(contact.getIdAdhEtabMois()), contact, "ID_ADH_ETAB_MOIS", "Le champ ID_ADH_ETAB_MOIS obligatoire n'est pas renseigné");

        // Longueur des champs
        valideTailleFixeChampAvecCollecte(contact.getIdContact(), 30, contact, "ID_CONTACT");
        valideTailleFixeChampAvecCollecte(contact.getIdAdhEtabMois(), 30, contact, "ID_ADH_ETAB_MOIS");
        valideTailleFixeChampAvecCollecte(contact.getNomContact(), 80, contact, "NOM_CONTACT");
        valideTailleFixeChampAvecCollecte(contact.getTelContact(), 20, contact, "TEL_CONTACT");
        valideTailleFixeChampAvecCollecte(contact.getMailContact(), 100, contact, "MAIL_CONTACT");
        valideTailleFixeChampAvecCollecte(contact.getTypeContact(), 2, contact, "TYPE_CONTACT");

        validationCorrespondanceReferentielle(contact);
    }

    /**
     * Validation des liens entre tables
     * 
     * @param contact
     *            Le contact dont l'Adhésion doit être validée.
     */
    private void validationCorrespondanceReferentielle(final Contact contact) {
        if (StringUtils.isNotBlank(contact.getIdAdhEtabMois())) {
            boolean existeUneAdhesion = adhesionEtablissementMoisRepository.existeUneAdhesion(contact.getIdAdhEtabMois());
            valideChampAvecCollecte(existeUneAdhesion, contact, "IdAdhEtabMois", RedacMessages.ERREUR_REFERENCE_FLUX4);
        }
    }
}
