package fr.si2m.red.batch.flux3.item;

import lombok.Setter;
import fr.si2m.red.batch.flux3.ligne.Etablissement;
import fr.si2m.red.batch.moteur.item.TransformateurDonnee;
import fr.si2m.red.complement.ExtensionEntrepriseAffiliee;
import fr.si2m.red.complement.ExtensionEntrepriseAffilieeRepository;

/**
 * Mapper des entités Etablissement (Affilié) à partir du numéro de Siret (Siren;nic) fourni.
 * 
 * @author benitahy
 *
 */
public class TransformateurEtablissementAffilie extends TransformateurDonnee<String, Etablissement> {

    /**
     * Référentiel des établissements complémentaires.
     * 
     * @param extensionEntrepriseAffilieeRepository
     */
    @Setter
    private ExtensionEntrepriseAffilieeRepository extensionEntrepriseAffilieeRepository;

    /*
     * (non-Javadoc)
     * 
     * Récupération de la chaîne de caractères renvoyée par le bean qui contient le numéro siren et le numéro nic(numSiret dans la table client) séparés par un
     * point virgule.
     */
    @Override
    public Etablissement process(String numSiret) throws Exception {
        Etablissement etablissement = new Etablissement();
        String[] splitArray = null;
        splitArray = numSiret.split(";");
        if (splitArray.length != 2) {
            return null;
        }

        String numeroSirenEtablissement = splitArray[0];
        String numeroSiretEtablissement = splitArray[1];
        ExtensionEntrepriseAffiliee extensionEntrepriseAffiliee = extensionEntrepriseAffilieeRepository
                .getExtensionEtablissementAffilieeComplementaire(numeroSirenEtablissement, numeroSiretEtablissement);
        if (extensionEntrepriseAffiliee == null) {
            return null;
        }

        etablissement.setCodeMiseAJour("R");
        etablissement.setIdentifiantTechniqueEntreprise(extensionEntrepriseAffiliee.getSiren());
        etablissement.setIdentifiantTechniqueEtablissement(extensionEntrepriseAffiliee.getSiren() + extensionEntrepriseAffiliee.getNic());
        etablissement.setNicSiret(extensionEntrepriseAffiliee.getNic());
        etablissement.setNumIdentificationEtablissementDansOrganisme(0);

        return etablissement;
    }
}
