package fr.si2m.red.batch.flux78.item;

import org.apache.commons.lang3.StringUtils;

import fr.si2m.red.core.repository.jpa.conversion.ConvertisseurNumeriqueDecimal;

/**
 * Utilitaire pour la manipulation des nombres spécifique au flux 7/8.
 * 
 * @author nortaina
 *
 */
public final class NumberUtils {
    private NumberUtils() {
        // Utilitaire
    }

    /**
     * Convertit une valeur de type Double en type Long - en valeur absolue -, en "supprimant" la virgule et en conservant le nombre de chiffres décimaux
     * précisé.
     * 
     * @param doubleValue
     *            la valeur Double à convertir
     * @param longueurPartieDecimale
     *            La longueur de la partie décimale
     * @return la valeur typée Long
     */
    public static Long degradeDoubleEnLongValeurAbsolue(Double doubleValue, int longueurPartieDecimale) {
        Long result;
        if (doubleValue == null) {
            result = 0L;
        } else {
            String valueAsText = new ConvertisseurNumeriqueDecimal().convertToEntityAttribute(Math.abs(doubleValue));
            String valuePartieEntiere = StringUtils.substringBefore(valueAsText, ".");
            String valuePartieDecimale = StringUtils.substringAfter(valueAsText, ".");
            String longValue = valuePartieEntiere
                    + StringUtils.rightPad(StringUtils.substring(valuePartieDecimale, 0, longueurPartieDecimale), 2, "0");
            result = Long.valueOf(longValue);
        }

        return result;
    }

}
