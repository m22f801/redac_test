package fr.si2m.red.batch.flux3.item;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import fr.si2m.red.contrat.SituationTarif;
import fr.si2m.red.core.repository.jdbc.JdbcRepository;

/**
 * Un mapper de ligne {@link SituationTarif} pour supporter la récupération de données Situation Tarifs via JDBC.
 * 
 * @author nortaina
 *
 */
public class SituationTarifRowMapper implements RowMapper<SituationTarif> {

    @Override
    public SituationTarif mapRow(ResultSet rs, int rowNum) throws SQLException {
        SituationTarif situationTarif = new SituationTarif(null, null, null);
        // Mapping des champs de la table TMP_RR300_TARIFS_IDS
        situationTarif.setNumContrat(rs.getString("NOCO"));
        situationTarif.setNumCategorie(rs.getString("NOCAT"));
        situationTarif.setDateDebutSituationLigne(JdbcRepository.getInteger(rs, "DT_DEBUT_SIT"));
        situationTarif.setTauxAppel(rs.getDouble("TXAPPCOT"));
        situationTarif.setDateDebutSituationContrat(JdbcRepository.getInteger(rs, "DT_DEBUT_SIT_CONTRAT"));

        // Mapping des champs issus de la table TARIFS
        situationTarif.setNatureBaseCotisations(JdbcRepository.getInteger(rs, "CONBCOT"));
        situationTarif.setTauxCalculCotisationsUniques(rs.getLong("TXCALCU"));
        situationTarif.setTauxBase1(rs.getLong("TXBASE1"));
        situationTarif.setTauxBase2(rs.getLong("TXBASE2"));
        situationTarif.setTauxBase3(rs.getLong("TXBASE3"));
        situationTarif.setTauxBase4(rs.getLong("TXBASE4"));
        situationTarif.setMontantCotisationsUniques(rs.getLong("MTCUNCU"));
        situationTarif.setDateFinSituationLigne(JdbcRepository.getInteger(rs, "DT_FIN_SIT"));
        situationTarif.setDateFinSituationContrat(JdbcRepository.getInteger(rs, "DT_FIN_SIT_CONTRAT"));

        return situationTarif;
    }
}
