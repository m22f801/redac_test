package fr.si2m.red.batch.flux3.ligne;

import lombok.Data;
import fr.si2m.red.DateRedac;

/**
 * Description d'une ligne d'un fichier ETABLISSEMENT à exporter.
 * 
 * @author nortaina
 *
 */
@Data
public class Etablissement {
    /**
     * Code Mise à jour.
     * 
     * @param codeMiseAJour
     *            Code Mise à jour
     * @return Code Mise à jour
     */
    private String codeMiseAJour;

    /**
     * Identifiant technique de l’Entreprise (N° de séquence local à une instance de cette interface).
     * 
     * @param identifiantTechniqueEntreprise
     *            Identifiant technique de l’Entreprise (N° de séquence local à une instance de cette interface)
     * @return Identifiant technique de l’Entreprise (N° de séquence local à une instance de cette interface)
     */
    private String identifiantTechniqueEntreprise;

    /**
     * Identifiant de l’Etablissement (N° de séquence local à une instance de cette interface).
     * 
     * @param identifiantTechniqueEtablissement
     *            Identifiant de l’Etablissement (N° de séquence local à une instance de cette interface)
     * @return Identifiant de l’Etablissement (N° de séquence local à une instance de cette interface)
     */
    private String identifiantTechniqueEtablissement;

    /**
     * Enseigne de l’Etablissement.
     * 
     * @param enseigneEtablissement
     *            Enseigne de l’Etablissement
     * @return Enseigne de l’Etablissement
     */
    private String enseigneEtablissement;

    /**
     * Date de création de l’Etablissement.
     * 
     * @param dateCreationEtablissement
     *            Date de création de l’Etablissement
     * @return Date de création de l’Etablissement
     */
    private Integer dateCreationEtablissement;

    /**
     * Numéro d’Identification de l’Etablissement dans l’Organisme relativement à l’entreprise.
     * 
     * @param numIdentificationEtablissementDansOrganisme
     *            Numéro d’Identification de l’Etablissement dans l’Organisme relativement à l’entreprise
     * @return Numéro d’Identification de l’Etablissement dans l’Organisme relativement à l’entreprise
     */
    private Integer numIdentificationEtablissementDansOrganisme;

    /**
     * NIC du numéro SIRET.
     * 
     * @param nicSiret
     *            NIC du numéro SIRET
     * @return NIC du numéro SIRET
     */
    private String nicSiret;

    /**
     * Date de première mise en exploitation.
     * 
     * @param datePremiereMiseEnExploitation
     *            Date de première mise en exploitation
     * @return Date de première mise en exploitation
     */
    private Integer datePremiereMiseEnExploitation;

    /**
     * Date de début de mise en sommeil.
     * 
     * @param dateDebutMiseEnSommeil
     *            Date de début de mise en sommeil
     * @return Date de début de mise en sommeil
     */
    private Integer dateDebutMiseEnSommeil;

    /**
     * Date de fin de mise en sommeil.
     * 
     * @param dateFinMiseEnSommeil
     *            Date de fin de mise en sommeil
     * @return Date de fin de mise en sommeil
     */
    private Integer dateFinMiseEnSommeil;

    /**
     * Date de cessation de l’activité.
     * 
     * @param dateCessationActivite
     *            Date de cessation de l’activité
     * @return Date de cessation de l’activité
     */
    private Integer dateCessationActivite;

    /**
     * Code motif de fin d’activité.
     * 
     * @param codeMotifFinActivite
     *            Code motif de fin d’activité
     * @return Code motif de fin d’activité
     */
    private String codeMotifFinActivite;

    /**
     * Code indicateur de cession partielle ou totale.
     * 
     * @param dateFinMiseEnSommeil
     *            Code indicateur de cession partielle ou totale
     * @return Code indicateur de cession partielle ou totale
     */
    private String codeIndicateurCession;

    /**
     * Code indicateur d’Etablissement Siège Social.
     * 
     * @param codeIndicateurEtablissementSiegeSocial
     *            Code indicateur d’Etablissement Siège Social
     * @return Code indicateur d’Etablissement Siège Social
     */
    private String codeIndicateurEtablissementSiegeSocial;

    /**
     * Date de première embauche.
     * 
     * @param datePremiereEmbauche
     *            Date de première embauche
     * @return Date de première embauche
     */
    private Integer datePremiereEmbauche;

    /**
     * Libellé de l’activité déclarée.
     * 
     * @param libelleActiviteDeclaree
     *            Libellé de l’activité déclarée
     * @return Libellé de l’activité déclarée
     */
    private String libelleActiviteDeclaree;

    /**
     * Activité principale exercée de l’Etablissement.
     * 
     * @param activitePrincipaleExercee
     *            Activité principale exercée de l’Etablissement
     * @return Activité principale exercée de l’Etablissement
     */
    private String activitePrincipaleExercee;

    /**
     * Code fonctionnel de l’Activité principale exercée.
     * 
     * @param codeFonctionnelActivitePrincipaleExercee
     *            Code fonctionnel de l’Activité principale exercée
     * @return Code fonctionnel de l’Activité principale exercée
     */
    private String codeFonctionnelActivitePrincipaleExercee;

    /**
     * Date de début d’application du code APE.
     * 
     * @param dateDebutApplicationCodeAPE
     *            Date de début d’application du code APE
     * @return Date de début d’application du code APE
     */
    private Integer dateDebutApplicationCodeAPE;

    /**
     * Date à partir de laquelle l’établissement est principal.
     * 
     * @param dateDebutEtablissementPrincipal
     *            Date à partir de laquelle l’établissement est principal
     * @return Date à partir de laquelle l’établissement est principal
     */
    private Integer dateDebutEtablissementPrincipal;

    /**
     * Date de début d’effet de l’enseigne.
     * 
     * @param dateDebutEffetEnseigne
     *            Date de début d’effet de l’enseigne
     * @return Date de début d’effet de l’enseigne
     */
    private Integer dateDebutEffetEnseigne;

    /**
     * Date de fin d’effet de l’enseigne.
     * 
     * @param dateFinEffetEnseigne
     *            Date de fin d’effet de l’enseigne
     * @return Date de fin d’effet de l’enseigne
     */
    private Integer dateFinEffetEnseigne;

    /**
     * Option décalage de paie de l’établissement .
     * 
     * @param optionDecalagePaieEtablissement
     *            Option décalage de paie de l’établissement
     * @return Option décalage de paie de l’établissement
     */
    private String optionDecalagePaieEtablissement;

    /**
     * Date de début d’application du décalage de paie.
     * 
     * @param dateDebutApplicationDecalagePaie
     *            Date de début d’application du décalage de paie
     * @return Date de début d’application du décalage de paie
     */
    private Integer dateDebutApplicationDecalagePaie;

    /**
     * Type de décalage pratiqué.
     * 
     * @param typeDecalagePratique
     *            Type de décalage pratiqué
     * @return Type de décalage pratiqué
     */
    private String typeDecalagePratique;

    /**
     * Code indicateur d’Etablissement Principal.
     * 
     * @param codeIndicateurEtablissementPrincipal
     *            Code indicateur d’Etablissement Principal
     * @return Code indicateur d’Etablissement Principal
     */
    private String codeIndicateurEtablissementPrincipal;

    /**
     * Date de création de l’Etablissement.
     * 
     * @return Date de création de l’Etablissement
     */
    public String getDateCreationEtablissementFormatee() {
        return DateRedac.formate(DateRedac.EXPORT_FORMAT_PAR_DEFAUT, dateCreationEtablissement);
    }

    /**
     * Date de première mise en exploitation.
     * 
     * @return Date de première mise en exploitation
     */
    public String getDatePremiereMiseEnExploitationFormatee() {
        return DateRedac.formate(DateRedac.EXPORT_FORMAT_PAR_DEFAUT, datePremiereMiseEnExploitation);
    }

    /**
     * Date de début de mise en sommeil.
     * 
     * @return Date de début de mise en sommeil
     */
    public String getDateDebutMiseEnSommeilFormatee() {
        return DateRedac.formate(DateRedac.EXPORT_FORMAT_PAR_DEFAUT, dateDebutMiseEnSommeil);
    }

    /**
     * Date de fin de mise en sommeil.
     * 
     * @return Date de fin de mise en sommeil
     */
    public String getDateFinMiseEnSommeilFormatee() {
        return DateRedac.formate(DateRedac.EXPORT_FORMAT_PAR_DEFAUT, dateFinMiseEnSommeil);
    }

    /**
     * Date de cessation de l’activité.
     * 
     * @return Date de cessation de l’activité
     */
    public String getDateCessationActiviteFormatee() {
        return DateRedac.formate(DateRedac.EXPORT_FORMAT_PAR_DEFAUT, dateCessationActivite);
    }

    /**
     * Date de première embauche.
     * 
     * @return Date de première embauche
     */
    public String getDatePremiereEmbaucheFormatee() {
        return DateRedac.formate(DateRedac.EXPORT_FORMAT_PAR_DEFAUT, datePremiereEmbauche);
    }

    /**
     * Date de début d’application du code APE.
     * 
     * @return Date de début d’application du code APE
     */
    public String getDateDebutApplicationCodeAPEFormatee() {
        return DateRedac.formate(DateRedac.EXPORT_FORMAT_PAR_DEFAUT, dateDebutApplicationCodeAPE);
    }

    /**
     * Date à partir de laquelle l’établissement est principal.
     * 
     * @return Date à partir de laquelle l’établissement est principal
     */
    public String getDateDebutEtablissementPrincipalFormatee() {
        return DateRedac.formate(DateRedac.EXPORT_FORMAT_PAR_DEFAUT, dateDebutEtablissementPrincipal);
    }

    /**
     * Date de début d’effet de l’enseigne.
     * 
     * @return Date de début d’effet de l’enseigne
     */
    public String getDateDebutEffetEnseigneFormatee() {
        return DateRedac.formate(DateRedac.EXPORT_FORMAT_PAR_DEFAUT, dateDebutEffetEnseigne);
    }

    /**
     * Date de fin d’effet de l’enseigne.
     * 
     * @return Date de fin d’effet de l’enseigne
     */
    public String getDateFinEffetEnseigneFormatee() {
        return DateRedac.formate(DateRedac.EXPORT_FORMAT_PAR_DEFAUT, dateFinEffetEnseigne);
    }

    /**
     * Date de début d’application du décalage de paie.
     * 
     * @return Date de début d’application du décalage de paie
     */
    public String getDateDebutApplicationDecalagePaieFormatee() {
        return DateRedac.formate(DateRedac.EXPORT_FORMAT_PAR_DEFAUT, dateDebutApplicationDecalagePaie);
    }

}
