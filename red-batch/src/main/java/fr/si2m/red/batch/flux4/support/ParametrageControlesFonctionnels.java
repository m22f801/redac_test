package fr.si2m.red.batch.flux4.support;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import fr.si2m.red.parametrage.ParamControleSignalRejet;
import fr.si2m.red.parametrage.ParamNatureBaseComposants;
import fr.si2m.red.parametrage.ParamValeurDefaut;
import lombok.Getter;
import lombok.Setter;

/**
 * Paramétrage des contrôles fonctionnels de la phase 6 du flux 4.
 * 
 * @author nortaina
 *
 */
public final class ParametrageControlesFonctionnels {
    /**
     * La liste des contrôles globaux.
     * 
     * @param controlesGlobaux
     *            la liste des contrôles globaux
     * @return la liste des contrôles globaux
     */
    @Setter
    @Getter
    private Map<String, ParamControleSignalRejet> controlesGlobaux = new HashMap<>();

    /**
     * Le paramétrage par défaut pour certaines règles de gestion.
     * 
     * @param controlesActifs
     *            le paramétrage par défaut pour certaines règles de gestion
     * @return le paramétrage par défaut pour certaines règles de gestion
     */
    @Setter
    @Getter
    private ParamValeurDefaut parametrageParDefaut;

    /**
     * Les paramètres de nature de bases de composants, indexés par code de nature de base de cotisations, pour certaines règles de gestion.
     * 
     * @param parametrageNatureBaseComposantsParCodeNatureBaseCotisations
     *            les paramètres de nature de bases de composants, indexés par code de nature de base de cotisations
     * @return les paramètres de nature de bases de composants, indexés par code de nature de base de cotisations
     */
    @Setter
    @Getter
    private Map<Integer, List<ParamNatureBaseComposants>> parametrageNatureBaseComposantsParCodeNatureBaseCotisations;

    /**
     * La taille de pagination pour les itérations sur des éléments déclaratifs.
     * 
     * @param taillePageIterationElementsDeclaratifs
     *            la taille de pagination pour les itérations sur des éléments déclaratifs
     * @return la taille de pagination pour les itérations sur des éléments déclaratifs
     */
    @Setter
    @Getter
    private Integer taillePageIterationElementsDeclaratifs;

    /**
     * Récupère un contrôle s'il est global ( nofam = ALL )
     * 
     * @param identifiantControle
     *            l'identifiant du contrôle global à récupérer
     * 
     * @return le contrôle s'il est existant, null sinon
     */

    public ParamControleSignalRejet getControleGlobal(String identifiantControle) {
        return controlesGlobaux.get(identifiantControle);
    }

}
