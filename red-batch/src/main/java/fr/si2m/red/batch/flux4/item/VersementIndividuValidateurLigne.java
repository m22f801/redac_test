package fr.si2m.red.batch.flux4.item;

import lombok.Setter;

import org.apache.commons.lang3.StringUtils;

import fr.si2m.red.DateRedac;
import fr.si2m.red.batch.RedacMessages;
import fr.si2m.red.batch.moteur.erreur.ValidateurLigneAvecCollecte;
import fr.si2m.red.dsn.IndividuRepository;
import fr.si2m.red.dsn.VersementIndividu;
import fr.si2m.red.dsn.VersementIndividuRepository;

/**
 * Validateur de ligne de paramétrage code libellé.
 * 
 * @author benitahy
 *
 */
public class VersementIndividuValidateurLigne extends ValidateurLigneAvecCollecte<VersementIndividu> {

    @Setter
    private IndividuRepository individuRepository;

    @Setter
    private VersementIndividuRepository versementIndividuRepository;

    @Override
    protected void valide(final VersementIndividu versementIndividu) {

        // unicité
        valideChampAvecCollecte(!versementIndividuRepository.existeUnVersementIndividu(versementIndividu.getIdVersementIndividu()), versementIndividu, "ID",
                "La ligne ne respecte pas la règle d'unicité");

        // Champs obligatoires
        valideChampAvecCollecte(StringUtils.isNotBlank(versementIndividu.getIdVersementIndividu()), versementIndividu, "ID_VERS_INDIV",
                "Le champ ID_VERS_INDIV obligatoire n'est pas renseigné");
        valideChampAvecCollecte(StringUtils.isNotBlank(versementIndividu.getIdIndividu()), versementIndividu, "ID_INDIVIDU", "Le champ ID_INDIVIDU obligatoire n'est pas renseigné");

        // Longueur des champs
        valideTailleFixeChampAvecCollecte(versementIndividu.getIdVersementIndividu(), 30, versementIndividu, "ID_VERS_INDIV");
        valideTailleFixeChampAvecCollecte(versementIndividu.getIdIndividu(), 30, versementIndividu, "ID_INDIVIDU");
        valideChampDateTailleFixeAvecCollecte(versementIndividu.getDateVersementAsText(), DateRedac.FORMAT_DATES_DDMMYYYY, versementIndividu, "DATE_VERSEMENT", false);
        valideChampNumeriqueDecimaleTailleFixeAvecCollecte(versementIndividu.getRemunerationNetteAsText(), 10, 2, versementIndividu, "REMUNERATION_NETTE");
        valideChampIntegerTailleFixeAvecCollecte(versementIndividu.getNumeroVersementAsText(), 2, versementIndividu, "NUMERO_VERSEMENT");

        validationCorrespondanceReferentielle(versementIndividu);
    }

    /**
     * Validation des liens entre tables.
     * 
     * @param versementIndividu
     *            Le versement individu dont l'individu doit être validé.
     */
    private void validationCorrespondanceReferentielle(final VersementIndividu versementIndividu) {
        if (StringUtils.isNotBlank(versementIndividu.getIdIndividu())) {
            boolean existeUnIndividu = individuRepository.existeUnIndividu(versementIndividu.getIdIndividu());
            valideChampAvecCollecte(existeUnIndividu, versementIndividu, "idIndividu", RedacMessages.ERREUR_REFERENCE_FLUX4);
        }
    }
}
