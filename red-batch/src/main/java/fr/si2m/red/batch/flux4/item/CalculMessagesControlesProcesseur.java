package fr.si2m.red.batch.flux4.item;

import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import fr.si2m.red.DateRedac;
import fr.si2m.red.RedacException;
import fr.si2m.red.RedacUnexpectedException;
import fr.si2m.red.batch.flux4.support.ParametrageControlesFonctionnels;
import fr.si2m.red.batch.moteur.item.TransformateurDonneeAvecCollecte;
import fr.si2m.red.complement.ExtensionContrat;
import fr.si2m.red.complement.ExtensionContratRepository;
import fr.si2m.red.contrat.Contrat;
import fr.si2m.red.contrat.ContratRepository;
import fr.si2m.red.contrat.Tarif;
import fr.si2m.red.contrat.TarifRepository;
import fr.si2m.red.dsn.AdhesionEtablissementMois;
import fr.si2m.red.dsn.BaseAssujettie;
import fr.si2m.red.dsn.ComposantBaseAssujettie;
import fr.si2m.red.dsn.ComposantBaseAssujettieRepository;
import fr.si2m.red.parametrage.ParamControleSignalRejet;
import fr.si2m.red.parametrage.ParamControleSignalRejetRepository;
import fr.si2m.red.parametrage.ParamFamilleModeCalculContratRepository;
import fr.si2m.red.parametrage.ParamNatureBaseComposants;
import fr.si2m.red.parametrage.ParamValeurDefaut;
import fr.si2m.red.reconciliation.CategorieQuittancementIndividuRepository;
import fr.si2m.red.reconciliation.CompteRenduIntegrationRepository;
import fr.si2m.red.reconciliation.EffectifCategorieContratTravailRepository;
import fr.si2m.red.reconciliation.EffectifCategorieMouvementRepository;
import fr.si2m.red.reconciliation.HistoriqueAssignationPeriodeRepository;
import fr.si2m.red.reconciliation.HistoriqueAttenteRetourEtpPeriodeRepository;
import fr.si2m.red.reconciliation.HistoriqueCommentairePeriodeRepository;
import fr.si2m.red.reconciliation.HistoriqueEtatPeriodeRepository;
import fr.si2m.red.reconciliation.IndicateursDSNPeriode;
import fr.si2m.red.reconciliation.IndicateursDSNPeriodeRepository;
import fr.si2m.red.reconciliation.MessageControle;
import fr.si2m.red.reconciliation.MessageControleRepository;
import fr.si2m.red.reconciliation.PeriodeRecue;
import fr.si2m.red.reconciliation.PeriodeRecueRepository;
import fr.si2m.red.reconciliation.RattachementDeclarationsRecuesRepository;
import fr.si2m.red.reconciliation.TrancheCategorieBaseAssujettieRepository;
import fr.si2m.red.reconciliation.TrancheCategorieRepository;
import lombok.Setter;

/**
 * Processeur de période pour l'application des contrôles fonctionnels sur périodes reçues.
 * 
 * @author nortaina
 *
 */
public class CalculMessagesControlesProcesseur extends TransformateurDonneeAvecCollecte<PeriodeRecue, List<MessageControle>> {
    private static final Logger LOGGER = LoggerFactory.getLogger(CalculMessagesControlesProcesseur.class);

    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    private ParametrageControlesFonctionnels parametrageControlesFonctionnels;

    @Autowired
    private ContratRepository contratRepository;

    @Autowired
    private TarifRepository tarifRepository;

    @Autowired
    private ExtensionContratRepository extensionContratRepository;

    @Autowired
    private RattachementDeclarationsRecuesRepository rattachementDeclarationsRecuesRepository;

    @Autowired
    private PeriodeRecueRepository periodeRecueRepository;

    @Autowired
    private CategorieQuittancementIndividuRepository categorieQuittancementIndividuRepository;
    @Autowired
    private EffectifCategorieMouvementRepository effectifCategorieMouvementRepository;
    @Autowired
    private TrancheCategorieRepository trancheCategorieRepository;
    @Autowired
    private EffectifCategorieContratTravailRepository effectifCategorieContratTravailRepository;
    @Autowired
    private TrancheCategorieBaseAssujettieRepository trancheCategorieBaseAssujettieRepository;
    @Autowired
    private IndicateursDSNPeriodeRepository indicateurDSNPeriodeRepository;
    @Autowired
    private ParamFamilleModeCalculContratRepository paramFamilleModeCalculContratRepository;
    @Autowired
    private ParamControleSignalRejetRepository paramControleSignalRejetRepository;
    @Autowired
    private CompteRenduIntegrationRepository compteRenduIntegrationRepository;
    @Autowired
    private HistoriqueEtatPeriodeRepository historiqueEtatPeriodeRepository;
    @Autowired
    private MessageControleRepository messageControleRepository;
    @Autowired
    private ComposantBaseAssujettieRepository composantBaseAssujettieRepository;
    @Autowired
    private HistoriqueAssignationPeriodeRepository historiqueAssignationPeriodeRepository;
    @Autowired
    private HistoriqueAttenteRetourEtpPeriodeRepository historiqueAttenteRetourEtpPeriodeRepository;
    @Autowired
    private HistoriqueCommentairePeriodeRepository historiqueCommentairePeriodeRepository;

    private final String auditNomBatch;

    @Setter
    private String cheminAbsoluFichierLogErreurs;

    private static final String ERREUR_TARIF = "La situation tarifaire [numContrat={0}, numCategorieQuittancement={1}, dateFinPeriode={2}] n''existe pas. Période traitée : [PeriodeRecue(idPeriode={3}, dateDebutPeriode={4}, dateFinPeriode={5}, typePeriode={6}, dateCreation={7})]";

    private static final String ERREUR_NATURE_BASE_COTISATIONS = "Le code nature base cotisations {0} de la situation tarifaire [numContrat={1}, numCategorie={2}, dateDebut={3}] est inconnu pour le calcul de la période : [PeriodeRecue(idPeriode={4}, dateDebutPeriode={5}, dateFinPeriode={6}, typePeriode={7}, dateCreation={8})]";

    private static final String ERREUR_CONTRAT = "La situation contrat [numContrat={0}, dateDebutPeriode={1}] n''existe pas. Période traitée : [idPeriode={2}, numContrat={0}, typePeriode={3}, dateDebutPeriode={1}, dateCreation={4}]";

    /**
     * Constructeur par défaut.
     * 
     * @param auditNomBatch
     *            le nom du batch activant ce processeur
     */
    public CalculMessagesControlesProcesseur(String auditNomBatch) {
        this.auditNomBatch = auditNomBatch;
    }

    @Override
    public List<MessageControle> process(PeriodeRecue periode) throws Exception {
        try {
            List<MessageControle> messages = new ArrayList<MessageControle>();
            controleResiliationContrat(periode, messages);
            controlePresenceCotisationEtablissement(periode, messages);
            controleStabiliteNbEtablissements(periode, messages);
            controleStabiliteNbSalaries(periode, messages);
            controleAbsenceModePaiementParticulier(periode, messages);
            controleCoherencePeriodeAffectationComposantVersement(periode, messages);
            controleAbsenceTrouDeclaration(periode, messages);
            controleConformiteTypesBasesAssujettiesDeclares(periode, messages);
            controleAlimentationTranchesEffectifs(periode, messages);
            controleAbsenceChangementDateNaissanceAssure(periode, messages);
            controleAbsenceChangementNirAssure(periode, messages);
            controleChangementTarifEtOuAppelCotisationDurantPeriode(periode, messages);
            controleEcartCotisationDeclareeCalculee(periode, messages);
            return messages;

        } catch (RedacUnexpectedException e) {
            LOGGER.info(e.getMessage(), e);

            // Purge éléments déclaratifs calculés
            Long idPeriode = periode.getIdPeriode();
            purgeElementDeclaratifs(idPeriode);

            // /!\ On ne purge PAS les Messages contrôles en cas d'erreur.

            traceErreurSansBlocage(e.getMessage(), cheminAbsoluFichierLogErreurs);

            // Détachement les adhésions
            int nbDetachements = rattachementDeclarationsRecuesRepository.detacheAdhesionsDernierLotRattachementPourPeriode(idPeriode);

            traceErreurSansBlocage("Erreur détectée lors du calcul des messages de contrôle - détachement de " + nbDetachements
                    + " adhésions pour la période " + idPeriode, cheminAbsoluFichierLogErreurs);

            // Si la période est "vide", on supprime toute les entitées pouvant y être rattachée,
            // avant de supprimer la période elle même.
            if (!rattachementDeclarationsRecuesRepository.existeRattachementDeclarationRecue(idPeriode)) {
                compteRenduIntegrationRepository.supprimeCompteRenduIntegrationPourPeriode(idPeriode);
                historiqueEtatPeriodeRepository.supprimeHistoriqueEtatPeriodePourPeriode(idPeriode);
                messageControleRepository.supprimeMessageControlePourPeriode(idPeriode);
                historiqueAssignationPeriodeRepository.supprimeHistoriqueAssignationPourPeriode(idPeriode);
                historiqueAttenteRetourEtpPeriodeRepository.supprimeHistoriqueAttenteRetourEtpPourPeriode(idPeriode);
                historiqueCommentairePeriodeRepository.supprimeHistoriqueCommentairePourPeriode(idPeriode);
                periodeRecueRepository.supprimePeriodeRecue(idPeriode);
            }

            // Skip : on renvoie null (tant pis pour Sonar) pour que le listener puisse détecter qu'il s'agit d'une période à reprendre
            // Concrètement, ayant détecté le null, le MarquageReconsolidationPeriodeListener ne mettra pas la période à "RECONSOLIDER = N").
            return null;
        }

    }

    /**
     * Purge les éléments déclaratifs d'une période
     * 
     * @param idPeriode
     *            l'identifiant de la période
     */
    private void purgeElementDeclaratifs(Long idPeriode) {
        categorieQuittancementIndividuRepository.supprimePourPeriode(idPeriode);
        effectifCategorieMouvementRepository.supprimePourPeriode(idPeriode);
        trancheCategorieRepository.supprimePourPeriode(idPeriode);
        effectifCategorieContratTravailRepository.supprimePourPeriode(idPeriode);
        trancheCategorieBaseAssujettieRepository.supprimePourPeriode(idPeriode);
    }

    /**
     * Vérifie si le controle doit être réalisé
     * 
     * @param identifiantControle
     *            identifiant du controle à vérifier
     * @param periode
     *            la période courante
     * @return un objet ParamControleSignalRejet non null si le controle est à réaliser, null sinon
     */
    private ParamControleSignalRejet verificationRealisationControle(String identifiantControle, PeriodeRecue periode) {

        // Tentative de récupération du controle dans la liste des controls globaux
        ParamControleSignalRejet paramControleSignalRejetGlobal = parametrageControlesFonctionnels.getControleGlobal(identifiantControle);

        if (paramControleSignalRejetGlobal != null) {
            String niveauAlerteGlobal = paramControleSignalRejetGlobal.getNiveauAlerte();
            if (ParamControleSignalRejet.NIVEAU_ALERTE_SIGNAL.equals(niveauAlerteGlobal)
                    || ParamControleSignalRejet.NIVEAU_ALERTE_REJET.equals(niveauAlerteGlobal)) {
                return paramControleSignalRejetGlobal;
            } else {
                return null;
            }
        } else {
            // Tentative de récupération du controle avec un NOFAM spécifique

            // Contrat correspondant à la situation contrat valide à date de début de période
            Contrat contrat = contratRepository.getContratValidePourDate(periode.getNumeroContrat(), periode.getDateDebutPeriode());
            if (contrat == null) {
                String logErr = MessageFormat.format(ERREUR_CONTRAT, periode.getNumeroContrat(), periode.getDateDebutPeriode(),
                        periode.getIdPeriode(), periode.getTypePeriode(), periode.getDateCreation().toString());
                throw new RedacUnexpectedException(logErr);
            }
            ParamControleSignalRejet paramControleSignalRejetPourNofam = paramControleSignalRejetRepository
                    .getParamControleSignalRejetPourControleEtNofam(identifiantControle, Integer.toString(contrat.getNumFamilleProduit()));

            if (paramControleSignalRejetPourNofam != null) {
                String niveauAlertePourNofam = paramControleSignalRejetPourNofam.getNiveauAlerte();
                if (ParamControleSignalRejet.NIVEAU_ALERTE_SIGNAL.equals(niveauAlertePourNofam)
                        || ParamControleSignalRejet.NIVEAU_ALERTE_REJET.equals(niveauAlertePourNofam)) {
                    return paramControleSignalRejetPourNofam;
                } else {
                    return null;
                }
            } else {
                return null;
            }
        }

    }

    /**
     * Contrôle si un contrat résilié est détectée sur une période donnée (si contrôle à réaliser).<br/>
     * <br/>
     * c.f. RDG F04_RG_P6_02
     * 
     * @param periode
     *            la période à contrôler
     * @param messages
     *            les messages de contrôle à renseigner sur la période
     */
    private void controleResiliationContrat(PeriodeRecue periode, List<MessageControle> messages) {

        ParamControleSignalRejet paramControleSignalRejet = verificationRealisationControle(ParamControleSignalRejet.ID_CONTRATS_RESILIES, periode);

        // On ne réalise pas le controle
        if (paramControleSignalRejet == null) {
            return;
        }

        Contrat contratPlusAncienNonActifSurPeriode = contratRepository.getPlusAncienNonActifSurPeriode(periode.getNumeroContrat(),
                periode.getDateDebutPeriode(), periode.getDateFinPeriode());
        if (contratPlusAncienNonActifSurPeriode == null) {
            // Pas de contrat résilié pour cette période = pas de message
            return;
        }
        LOGGER.debug("Détection d'un contrat résilié pour la période {}", periode.getIdPeriode());
        String paramMessage1 = DateRedac.formate(DateRedac.MESSAGES_FORMAT_PAR_DEFAUT,
                contratPlusAncienNonActifSurPeriode.getDateDebutSituationLigne());
        messages.add(creeMessageControleREDAC(periode, paramControleSignalRejet, paramMessage1, null, null, null));
    }

    /**
     * Contrôle s'il y a une présence de cotisation d'établissement sur une période donnée (si contrôle à réaliser).<br/>
     * <br/>
     * c.f. RDG F04_RG_P6_03
     * 
     * @param periode
     *            la période à contrôler
     * @param messages
     *            les messages de contrôle à renseigner sur la période
     */
    private void controlePresenceCotisationEtablissement(PeriodeRecue periode, List<MessageControle> messages) {

        ParamControleSignalRejet paramControleSignalRejet = verificationRealisationControle(
                ParamControleSignalRejet.ID_PRESENCE_COTISATION_ETABLISSEMENT, periode);

        // On ne réalise pas le controle
        if (paramControleSignalRejet == null) {
            return;
        }

        boolean existeUneCotisationEtablissementRattachee = rattachementDeclarationsRecuesRepository
                .existeUneCotisationEtablissementRattachee(periode.getIdPeriode());
        if (!existeUneCotisationEtablissementRattachee) {
            // Pas de cotisation d'établissement rattaché = pas de message
            return;
        }
        LOGGER.debug("Détection de la présence d'une cotisation établissement pour la période {}", periode.getIdPeriode());

        messages.add(creeMessageControleREDAC(periode, paramControleSignalRejet, null, null, null, null));
    }

    /**
     * Contrôle de stabilité du nombre d'établissements entre les 2 dernières périodes déclarées (si contrôle à réaliser).<br/>
     * <br/>
     * c.f. RDG F04_RG_P6_04
     * 
     * @param periode
     *            la période à contrôler
     * @param messages
     *            les messages de contrôle à renseigner sur la période
     */
    private void controleStabiliteNbEtablissements(PeriodeRecue periode, List<MessageControle> messages) {

        ParamControleSignalRejet paramControleSignalRejet = verificationRealisationControle(ParamControleSignalRejet.ID_VARIATION_NB_ETABLISSEMENTS,
                periode);

        // On ne réalise pas le controle
        if (paramControleSignalRejet == null || !StringUtils.equals(periode.getTypePeriode(), PeriodeRecue.TYPE_PERIODE_DECLARATION)) {
            return;
        }

        PeriodeRecue periodeAnterieure = periodeRecueRepository.getPeriodePrecedenteDeMemeType(periode);
        if (periodeAnterieure == null) {
            // Pas de période antérieure de type déclaration = pas de message
            return;
        }
        // Calcul des seuils
        List<Integer> seuils = getSeuilsVariationsPourEtablissements(periode.getNumeroContrat());
        Integer seuilVariationAlertesEnNbEtablissements = seuils.get(0);
        Integer seuilVariationAlertesEnPourcentageNbEtablissements = seuils.get(1);
        // Calcul de la variation
        int nbEtablissementRattaches = rattachementDeclarationsRecuesRepository.compteNbEtablissementsRattaches(periode.getIdPeriode());
        int nbEtablissementRattachesAnterieurement = rattachementDeclarationsRecuesRepository
                .compteNbEtablissementsRattaches(periodeAnterieure.getIdPeriode());

        int nbEtablissementsEcart = Math.abs(nbEtablissementRattaches - nbEtablissementRattachesAnterieurement);
        double variationNbEtablissementsEnPourcentage = nbEtablissementRattachesAnterieurement == 0 ? 100d
                : nbEtablissementsEcart * 100d / nbEtablissementRattachesAnterieurement;
        if (nbEtablissementsEcart < seuilVariationAlertesEnNbEtablissements
                || variationNbEtablissementsEnPourcentage < seuilVariationAlertesEnPourcentageNbEtablissements) {
            // Pas de dépassement des deux seuils à la fois = pas de message
            return;
        }
        LOGGER.debug("Détection d'une importante variation du nombre d'établissements pour la période {}", periode.getIdPeriode());

        messages.add(creeMessageControleREDAC(periode, paramControleSignalRejet, String.valueOf(nbEtablissementsEcart),
                String.valueOf(seuilVariationAlertesEnNbEtablissements), String.valueOf(seuilVariationAlertesEnPourcentageNbEtablissements), null));
    }

    /**
     * Récupère les seuils de variations de nombre d'établissement pour un contrat donné.
     * 
     * @param numeroContrat
     *            le numéro de contrat
     * @return deux seuils : un sur le nombre d'établissements, l'autre sur le pourcentage de variation
     */
    private List<Integer> getSeuilsVariationsPourEtablissements(String numeroContrat) {
        Integer seuilVariationAlertesEnNbEtablissements;
        Integer seuilVariationAlertesEnPourcentageNbEtablissements;
        ExtensionContrat extensionContrat = extensionContratRepository.getExtensionContrat(numeroContrat);
        if (extensionContrat != null) {
            seuilVariationAlertesEnNbEtablissements = extensionContrat.getSeuilVariationAlertesEnNbEtablissements();
            seuilVariationAlertesEnPourcentageNbEtablissements = extensionContrat.getSeuilVariationAlertesEnPourcentageNbEtablissements();
        } else {
            ParamValeurDefaut parametrageParDefaut = parametrageControlesFonctionnels.getParametrageParDefaut();
            seuilVariationAlertesEnNbEtablissements = parametrageParDefaut.getSeuilVariationAlertesEnNbEtablissements();
            seuilVariationAlertesEnPourcentageNbEtablissements = parametrageParDefaut.getSeuilVariationAlertesEnPourcentageNbEtablissements();
        }
        return Arrays.asList(seuilVariationAlertesEnNbEtablissements, seuilVariationAlertesEnPourcentageNbEtablissements);
    }

    /**
     * Contrôle de stabilité du nombre de salariés entre les 2 dernières périodes déclarées (si contrôle à réaliser).<br/>
     * <br/>
     * c.f. RDG F04_RG_P6_05
     * 
     * @param periode
     *            la période à contrôler
     * @param messages
     *            les messages de contrôle à renseigner sur la période
     */
    private void controleStabiliteNbSalaries(PeriodeRecue periode, List<MessageControle> messages) {

        ParamControleSignalRejet paramControleSignalRejet = verificationRealisationControle(ParamControleSignalRejet.ID_VARIATION_NB_SALARIES,
                periode);

        // On ne réalise pas le controle
        if (paramControleSignalRejet == null || !StringUtils.equals(periode.getTypePeriode(), PeriodeRecue.TYPE_PERIODE_DECLARATION)) {
            return;
        }

        PeriodeRecue periodeAnterieure = periodeRecueRepository.getPeriodePrecedenteDeMemeType(periode);
        if (periodeAnterieure == null) {
            // Pas de période antérieure de type déclaration = pas de message
            return;
        }
        // Calcul des seuils
        List<Integer> seuils = getSeuilsVariationsPourSalaries(periode.getNumeroContrat());
        Integer seuilVariationAlertesEnNbSalaries = seuils.get(0);
        Integer seuilVariationAlertesEnPourcentageNbSalaries = seuils.get(1);
        // Calcul de la variation
        int nbSalariesRattaches = rattachementDeclarationsRecuesRepository.compteNbSalariesRattachesEnDebutPeriode(periode.getIdPeriode(),
                periode.getDateDebutPeriode());
        int nbSalariesRattachesAnterieurement = rattachementDeclarationsRecuesRepository
                .compteNbSalariesRattachesEnFinPeriode(periodeAnterieure.getIdPeriode(), periodeAnterieure.getDateFinPeriode());

        int nbSalariesEcart = Math.abs(nbSalariesRattaches - nbSalariesRattachesAnterieurement);
        double variationNbSalariesEnPourcentage = nbSalariesRattachesAnterieurement == 0 ? 100d
                : nbSalariesEcart * 100d / nbSalariesRattachesAnterieurement;
        if (nbSalariesEcart < seuilVariationAlertesEnNbSalaries || variationNbSalariesEnPourcentage < seuilVariationAlertesEnPourcentageNbSalaries) {
            // Pas de dépassement des deux seuils à la fois = pas de message
            return;
        }
        LOGGER.debug("Détection d'une importante variation du nombre de salariés pour la période {}", periode.getIdPeriode());

        messages.add(creeMessageControleREDAC(periode, paramControleSignalRejet, String.valueOf(nbSalariesEcart),
                String.valueOf(seuilVariationAlertesEnNbSalaries), String.valueOf(seuilVariationAlertesEnPourcentageNbSalaries), null));
    }

    /**
     * Récupère les seuils de variations de nombre de salariés pour un contrat donné.
     * 
     * @param numeroContrat
     *            le numéro de contrat
     * @return deux seuils : un sur le nombre de salariés, l'autre sur le pourcentage de variation
     */
    private List<Integer> getSeuilsVariationsPourSalaries(String numeroContrat) {
        Integer seuilVariationAlertesEnNbSalaries;
        Integer seuilVariationAlertesEnPourcentageNbSalaries;
        ExtensionContrat extensionContrat = extensionContratRepository.getExtensionContrat(numeroContrat);
        if (extensionContrat != null) {
            seuilVariationAlertesEnNbSalaries = extensionContrat.getSeuilVariationAlertesEnNbSalaries();
            seuilVariationAlertesEnPourcentageNbSalaries = extensionContrat.getSeuilVariationAlertesEnPourcentageNbSalaries();
        } else {
            ParamValeurDefaut parametrageParDefaut = parametrageControlesFonctionnels.getParametrageParDefaut();
            seuilVariationAlertesEnNbSalaries = parametrageParDefaut.getSeuilVariationAlertesEnNbSalaries();
            seuilVariationAlertesEnPourcentageNbSalaries = parametrageParDefaut.getSeuilVariationAlertesEnPourcentageNbSalaries();
        }
        return Arrays.asList(seuilVariationAlertesEnNbSalaries, seuilVariationAlertesEnPourcentageNbSalaries);
    }

    /**
     * Contrôle d'absence de mode de paiement particulier (si contrôle à réaliser).<br/>
     * <br/>
     * c.f. RDG F04_RG_P6_06
     * 
     * @param periode
     *            la période à contrôler
     * @param messages
     *            les messages de contrôle à renseigner sur la période
     */
    private void controleAbsenceModePaiementParticulier(PeriodeRecue periode, List<MessageControle> messages) {

        ParamControleSignalRejet paramControleSignalRejet = verificationRealisationControle(ParamControleSignalRejet.ID_MODE_PAIEMENT, periode);

        // On ne réalise pas le controle
        if (paramControleSignalRejet == null) {
            return;
        }

        if (!rattachementDeclarationsRecuesRepository.existeVersementRattacheAvecModePaiementParticulier(periode.getIdPeriode())) {
            // Pas de mode de paiement particulier = pas de message
            return;
        }
        LOGGER.debug("Détection d'un mode de paiement particulier pour la période {}", periode.getIdPeriode());

        messages.add(creeMessageControleREDAC(periode, paramControleSignalRejet, null, null, null, null));
    }

    /**
     * Contrôle de cohérence de la période d'affectation du composant de versement (si contrôle à réaliser).<br/>
     * <br/>
     * c.f. RDG F04_RG_P6_07
     * 
     * @param periode
     *            la période à contrôler
     * @param messages
     *            les messages de contrôle à renseigner sur la période
     */
    private void controleCoherencePeriodeAffectationComposantVersement(PeriodeRecue periode, List<MessageControle> messages) {

        ParamControleSignalRejet paramControleSignalRejet = verificationRealisationControle(ParamControleSignalRejet.ID_PERIODE_VERSEMENT, periode);

        // On ne réalise pas le controle
        if (paramControleSignalRejet == null) {
            return;
        }

        String periodeAffectationInvalide = rattachementDeclarationsRecuesRepository.getPeriodeAffectationComposantVersementRattacheInvalide(
                periode.getIdPeriode(), periode.getDateDebutPeriode(), periode.getDateFinPeriode());
        if (periodeAffectationInvalide == null) {
            // La cohérence est vérifiée = pas de message
            return;
        }
        LOGGER.debug("Détection d'un composant de versement non conforme à la périodicité du contrat pour la période {}", periode.getIdPeriode());

        messages.add(creeMessageControleREDAC(periode, paramControleSignalRejet, periodeAffectationInvalide, null, null, null));
    }

    /**
     * Contrôle d'absence de trou de déclaration (si contrôle à réaliser).<br/>
     * <br/>
     * c.f. RDG F04_RG_P6_08
     * 
     * @param periode
     *            la période à contrôler
     * @param messages
     *            les messages de contrôle à renseigner sur la période
     */
    private void controleAbsenceTrouDeclaration(PeriodeRecue periode, List<MessageControle> messages) {

        ParamControleSignalRejet paramControleSignalRejet = verificationRealisationControle(ParamControleSignalRejet.ID_TROU_DECLARATION, periode);

        // On ne réalise pas le controle
        if (paramControleSignalRejet == null || !StringUtils.equals(periode.getTypePeriode(), PeriodeRecue.TYPE_PERIODE_DECLARATION)) {
            return;
        }

        // Calcul des mois à vérifier
        Set<Integer> moisADeclarer = new TreeSet<>();
        Date dateDebutPeriode = DateRedac.convertitEnDate(periode.getDateDebutPeriode());
        Date dateFinPeriode = DateRedac.convertitEnDate(periode.getDateFinPeriode());
        Calendar calendrier = Calendar.getInstance();
        calendrier.setTime(dateDebutPeriode);
        calendrier.set(Calendar.DAY_OF_MONTH, 1);
        while (calendrier.getTime().before(dateFinPeriode)) {
            moisADeclarer.add(DateRedac.convertitEnDateRedac(calendrier.getTime()));
            calendrier.add(Calendar.MONTH, 1);
        }
        List<AdhesionEtablissementMois> adhesions = rattachementDeclarationsRecuesRepository
                .getAdhesionsEtablissementsMoisRattaches(periode.getIdPeriode());
        for (AdhesionEtablissementMois adhesion : adhesions) {
            moisADeclarer.remove(adhesion.getMoisDeclare());
        }
        if (moisADeclarer.isEmpty()) {
            // Pas de trou de déclaration = pas de message
            return;
        }
        LOGGER.debug("Détection d'un trou de déclaration pour la période {}", periode.getIdPeriode());

        String premierMoisNonDeclare = new SimpleDateFormat("MM/yyyy").format(DateRedac.convertitEnDate(moisADeclarer.iterator().next()));
        messages.add(creeMessageControleREDAC(periode, paramControleSignalRejet, premierMoisNonDeclare, null, null, null));
    }

    /**
     * Contrôle de conformité des types de bases assujetties déclarés (si contrôle à réaliser).<br/>
     * <br/>
     * c.f. RDG F04_RG_P6_09
     * 
     * @param periode
     *            la période à contrôler
     * @param messages
     *            les messages de contrôle à renseigner sur la période
     */
    private void controleConformiteTypesBasesAssujettiesDeclares(PeriodeRecue periode, List<MessageControle> messages) {

        ParamControleSignalRejet paramControleSignalRejet = verificationRealisationControle(ParamControleSignalRejet.ID_TYPES_BASES_ASSUJETTIES,
                periode);

        // On ne réalise pas le controle
        if (paramControleSignalRejet == null) {
            return;
        }

        String typeComposantBaseAssujettieInconnu = null;

        String numContrat = periode.getNumeroContrat();
        Integer dateEffet = periode.getDateDebutPeriode();

        String numCategorieUnique = tarifRepository.getNumCategorieDeclareValable(periode.getNumeroContrat(), periode.getDateDebutPeriode());

        final int taillePage = parametrageControlesFonctionnels.getTaillePageIterationElementsDeclaratifs();
        int pageCourante = 0;
        List<ComposantBaseAssujettie> composantsRattaches = rattachementDeclarationsRecuesRepository
                .getComposantsBasesAssujettiesRattachesPagines(periode.getIdPeriode(), pageCourante * taillePage, taillePage);

        while (!composantsRattaches.isEmpty() && typeComposantBaseAssujettieInconnu == null) {

            for (ComposantBaseAssujettie composant : composantsRattaches) {
                String numCategorie = getNumeroCategoriePourComposantBaseAssujettie(composant, numCategorieUnique);

                Tarif tarif = tarifRepository.getPourNumContratEtNumCategorieEtDateEffet(numContrat, numCategorie, dateEffet);

                validePresenceTarif(tarif, periode, numCategorie);

                boolean tauxCalculRempli = isTauxCalculRempli(tarif.getTauxCalculCotisationsUniques());

                Set<String> typesComposantsConnus = getTypesComposantsConnusPourCodeNatureBaseCotisationsEtTauxCalculRempli(
                        tarif.getNatureBaseCotisations(), tauxCalculRempli);

                if (typesComposantsConnus == null || typesComposantsConnus.isEmpty()) {
                    String logErreur = MessageFormat.format(ERREUR_NATURE_BASE_COTISATIONS, tarif.getNatureBaseCotisations(), tarif.getNumContrat(),
                            numCategorie, tarif.getDateDebutSituationLigne().toString(), periode.getIdPeriode(),
                            periode.getDateDebutPeriode().toString(), periode.getDateFinPeriode().toString(), periode.getTypePeriode(),
                            periode.getDateCreation().toString());
                    throw new RedacUnexpectedException(logErreur);
                }
                String typeComposantBaseAssujettie = composant.getTypeComposantBaseAssujettie();

                if (!typesComposantsConnus.contains(typeComposantBaseAssujettie)) {
                    typeComposantBaseAssujettieInconnu = typeComposantBaseAssujettie;
                    break;
                }
            }
            entityManager.flush();
            entityManager.clear();
            if (typeComposantBaseAssujettieInconnu == null) {
                pageCourante++;
                composantsRattaches = rattachementDeclarationsRecuesRepository.getComposantsBasesAssujettiesRattachesPagines(periode.getIdPeriode(),
                        pageCourante * taillePage, taillePage);
            }
        }
        if (typeComposantBaseAssujettieInconnu == null) {
            // Pas de type inconnu = pas de message
            return;
        }

        LOGGER.debug("Détection d'un type de composant de base assujettie inconnu pour la période {}", periode.getIdPeriode());

        messages.add(creeMessageControleREDACBaseAssujettie(periode, paramControleSignalRejet, typeComposantBaseAssujettieInconnu, null, null, null));
    }

    /**
     * Contrôle sur les périodes des contrats et situations tarifaires.<br/>
     * <br/>
     * c.f. RDG F04_RG_P6_16
     * 
     * @param periode
     *            la période à contrôler
     * @param messages
     *            les messages de contrôle à renseigner sur la période
     */
    private void controleChangementTarifEtOuAppelCotisationDurantPeriode(PeriodeRecue periode, List<MessageControle> messages) {

        ParamControleSignalRejet paramControleSignalRejet = verificationRealisationControle(ParamControleSignalRejet.ID_CHANGEMENT_TARIF, periode);

        boolean txAppCotNonUniquePourPeriode = false;
        boolean changementTarifAuCoursPeriode = false;

        // On ne réalise pas le controle
        if (paramControleSignalRejet == null) {
            return;
        }

        // Contrôle sur le nombre de contrats durant la période

        txAppCotNonUniquePourPeriode = contratRepository.getNombreContratActifPourPeriodeDonnee(periode.getDateDebutPeriode(),
                periode.getDateFinPeriode(), periode.getNumeroContrat()) > 1;

        // Contrôle sur le nombre de tarifs durant la période

        changementTarifAuCoursPeriode = tarifRepository.getNombreTarifPourPeriodeDonnee(periode.getDateDebutPeriode(), periode.getDateFinPeriode(),
                periode.getNumeroContrat()) > 0;

        if (txAppCotNonUniquePourPeriode && changementTarifAuCoursPeriode) {
            messages.add(creeMessageControleREDAC(periode, paramControleSignalRejet, "tarifs, taux d'appel de cotisation", null, null, null));
        } else if (txAppCotNonUniquePourPeriode && !changementTarifAuCoursPeriode) {
            messages.add(creeMessageControleREDAC(periode, paramControleSignalRejet, "taux d'appel de cotisation", null, null, null));
        } else if (changementTarifAuCoursPeriode && !txAppCotNonUniquePourPeriode) {
            messages.add(creeMessageControleREDAC(periode, paramControleSignalRejet, "tarifs", null, null, null));
        }
    }

    /**
     * Contrôle sur l'alimentation des tranches / effectifs (qu'ils ne soient pas tous à 0). <br/>
     * <br/>
     * c.f. RDG F04_RG_P6_15
     * 
     * @param periode
     *            la période à contrôler
     * @param messages
     *            les messages de contrôle à renseigner sur la période
     */
    private void controleAlimentationTranchesEffectifs(PeriodeRecue periode, List<MessageControle> messages) {

        ParamControleSignalRejet paramControleSignalRejet = verificationRealisationControle(ParamControleSignalRejet.ID_ASSIETTES_VIDES, periode);

        // On ne réalise pas le controle
        if (paramControleSignalRejet == null) {
            return;
        }

        // Supposition: Controle OK
        boolean assietesVides = false;

        // Somme des MONTANT_COTISATION de toutes les lignes de BASE_ASSUJETTIE pour lesquelles le contrôle est KO
        Double sommeMontantCotisation = 0.0;

        String numContrat = periode.getNumeroContrat();
        Integer dateEffet = periode.getDateDebutPeriode();

        String numCategorieUnique = tarifRepository.getNumCategorieDeclareValable(periode.getNumeroContrat(), periode.getDateDebutPeriode());

        final int taillePage = parametrageControlesFonctionnels.getTaillePageIterationElementsDeclaratifs();
        int pageCourante = 0;
        List<BaseAssujettie> basesAssujettiesRattaches = rattachementDeclarationsRecuesRepository
                .getAllBasesAssujettiesRattacheesPagines(periode.getIdPeriode(), pageCourante * taillePage, taillePage);

        while (!basesAssujettiesRattaches.isEmpty()) {

            for (BaseAssujettie baseAssujettie : basesAssujettiesRattaches) {

                Double montantCotisationBA = baseAssujettie.getMontantCotisation();

                // Si le montantCotisation de la BaseAssujettie en cours est différent de 0 on verifie si le controle est KO
                if (montantCotisationBA.compareTo(Double.valueOf(0d)) != 0) {

                    // Récupération de la tarif pour y acceder a son natureBaseCotisation et tauxCalculCotisationsUniques
                    String numCategorie = getNumeroCategoriePourBaseAssujettie(baseAssujettie, numCategorieUnique);

                    Tarif tarif = tarifRepository.getPourNumContratEtNumCategorieEtDateEffet(numContrat, numCategorie, dateEffet);

                    validePresenceTarif(tarif, periode, numCategorie);

                    boolean tauxCalculRempli = isTauxCalculRempli(tarif.getTauxCalculCotisationsUniques());

                    // Récupération des numTypeComposantBase autorisés correspondants au natureBaseCotisation :
                    Set<String> typesComposantsConnus = getTypesComposantsConnusPourCodeNatureBaseCotisationsEtTauxCalculRempli(
                            tarif.getNatureBaseCotisations(), tauxCalculRempli);

                    if (typesComposantsConnus == null || typesComposantsConnus.isEmpty()) {
                        String logErreur = MessageFormat.format(ERREUR_NATURE_BASE_COTISATIONS, tarif.getNatureBaseCotisations(),
                                tarif.getNumContrat(), numCategorie, tarif.getDateDebutSituationLigne().toString(), periode.getIdPeriode(),
                                periode.getDateDebutPeriode().toString(), periode.getDateFinPeriode().toString(), periode.getTypePeriode(),
                                periode.getDateCreation().toString());
                        throw new RedacUnexpectedException(logErreur);
                    }
                    List<String> listeNumTCBA = new ArrayList<String>(typesComposantsConnus);

                    // Récupération des ComposantsBaseAssujettie ayant un numTCBA autorisé
                    Double montantCotisationComposantBaseAssujettie = composantBaseAssujettieRepository
                            .getSommeMontantsComposantsBaseAssujettie(baseAssujettie.getIdBaseAssujettie(), listeNumTCBA);

                    // montantCotisationComposantBaseAssujettie = 0 => Controle KO
                    if (montantCotisationComposantBaseAssujettie == null
                            || montantCotisationComposantBaseAssujettie.compareTo(Double.valueOf(0d)) == 0) {
                        assietesVides = true;
                        // Sommer les montants cotisation pour toutes les bases assujetties pour les controles que sont KO
                        sommeMontantCotisation = sommeMontantCotisation + montantCotisationBA;
                    }
                }

            }

            entityManager.flush();
            entityManager.clear();

            pageCourante++;
            basesAssujettiesRattaches = rattachementDeclarationsRecuesRepository
                    .getBasesAssujettiesRattacheesAvecMontantCotisationNonNulPagines(periode.getIdPeriode(), pageCourante * taillePage, taillePage);

        }
        if (!assietesVides) {
            // Pas d'assietes vides = pas de message
            return;
        }

        LOGGER.debug(
                "Détection d'un montant de cotisation base assujettie déclaré mais sans aucun montant de composants de base assujetties déclaré pour la période {}",
                periode.getIdPeriode());

        messages.add(creeMessageControleREDAC(periode, paramControleSignalRejet, String.valueOf(sommeMontantCotisation), null, null, null));

    }

    /**
     * Récupère le numéro de catégorie pour un composant de base assujettie donné.
     * 
     * @param composant
     *            le composant
     * @param numCategorieUnique
     *            le numéro de catégorie unique à appliquer par défaut
     * @return le numéro de catégorie correspondant
     */
    private String getNumeroCategoriePourComposantBaseAssujettie(ComposantBaseAssujettie composant, String numCategorieUnique) {
        String numCategorie = composant.getBaseAssujettie().getAffiliation().getCodePopulation();
        return StringUtils.isBlank(numCategorie) ? numCategorieUnique : numCategorie;
    }

    /**
     * Récupère le numéro de catégorie pour une base assujettie donné.
     * 
     * @param base
     *            assujettie la base assujettie
     * @param numCategorieUnique
     *            le numéro de catégorie unique à appliquer par défaut
     * @return le numéro de catégorie correspondant
     */
    private String getNumeroCategoriePourBaseAssujettie(BaseAssujettie baseAssujettie, String numCategorieUnique) {
        String numCategorie = baseAssujettie.getAffiliation().getCodePopulation();
        return StringUtils.isBlank(numCategorie) ? numCategorieUnique : numCategorie;
    }

    /**
     * Vérifie qu'un taux de calcul est bien rempli.
     * 
     * @param tauxCalcul
     *            le taux de calcul à vérifier
     * @return true si le taux de calcul est rempli, false sinon
     */
    private boolean isTauxCalculRempli(Long tauxCalcul) {
        return tauxCalcul != null && tauxCalcul > 0;
    }

    /**
     * Valide la présence d'un tarif donné.
     * 
     * @param tarif
     *            le tarif à vérifier
     * @param numeroContrat
     *            le numéro de contrat du tarif recherché
     * @param numCategorie
     *            le numéro de catégorie du tarif recherché
     * @param dateEffet
     *            la date d'effet du tarif recherché
     */
    private void validePresenceTarif(Tarif tarif, PeriodeRecue periode, String numCategorie) {
        if (tarif == null) {
            String logErreur = MessageFormat.format(ERREUR_TARIF, periode.getNumeroContrat(), numCategorie, periode.getDateFinPeriode().toString(),
                    periode.getIdPeriode(), periode.getDateDebutPeriode().toString(), periode.getDateFinPeriode().toString(),
                    periode.getTypePeriode(), periode.getDateCreation().toString());
            throw new RedacUnexpectedException(logErreur);
        }
    }

    /**
     * Récupère les types de composants connus dans le paramétrage de l'application pour un code de nature de base de cotisations donné et si le taux de calcul
     * est rempli ou non.
     * 
     * @param codeNatureBaseCotisations
     *            le code de nature de base de cotisations
     * @param tauxCalculRempli
     *            si on veut les types pour les taux de calcul remplis ou non
     * @return les types de composants connus correspondant aux paramètres donnés
     */
    private Set<String> getTypesComposantsConnusPourCodeNatureBaseCotisationsEtTauxCalculRempli(Integer codeNatureBaseCotisations,
            boolean tauxCalculRempli) {
        Set<String> typesComposantsConnus = new HashSet<>();
        List<ParamNatureBaseComposants> naturesBasesComposants = parametrageControlesFonctionnels
                .getParametrageNatureBaseComposantsParCodeNatureBaseCotisations().get(codeNatureBaseCotisations);
        if (naturesBasesComposants == null) {
            return null;
        }
        for (ParamNatureBaseComposants natureBaseComposants : naturesBasesComposants) {
            if (tauxCalculRempli == natureBaseComposants.isTauxCalculRempli()) {
                typesComposantsConnus.add(String.valueOf(natureBaseComposants.getNumTypeComposantBase()));
            }
        }
        return typesComposantsConnus;
    }

    /**
     * Contrôle d'absence de changement de date de naissance d'un assuré (si contrôle à réaliser).<br/>
     * <br/>
     * c.f. RDG F04_RG_P6_10
     * 
     * @param periode
     *            la période à contrôler
     * @param messages
     *            les messages de contrôle à renseigner sur la période
     */
    private void controleAbsenceChangementDateNaissanceAssure(PeriodeRecue periode, List<MessageControle> messages) {

        ParamControleSignalRejet paramControleSignalRejet = verificationRealisationControle(
                ParamControleSignalRejet.ID_CHANGEMENT_DATE_NAISSANCE_ASSURE, periode);

        // On ne réalise pas le controle
        if (paramControleSignalRejet == null) {
            return;
        }

        if (!rattachementDeclarationsRecuesRepository.existeUnChangementDateNaissanceIndividuRattache(periode.getIdPeriode())) {
            // Pas de changement de date de naissance d'assuré = pas de message
            return;
        }
        LOGGER.debug("Détection d'un changement de date de naissance d'un assuré pour la période {}", periode.getIdPeriode());

        messages.add(creeMessageControleREDAC(periode, paramControleSignalRejet, null, null, null, null));
    }

    /**
     * Contrôle d'absence de changement de nir d'un assuré (si contrôle à réaliser).<br/>
     * <br/>
     * c.f. RDG F04_RG_P6_12
     * 
     * @param periode
     *            la période à contrôler
     * @param messages
     *            les messages de contrôle à renseigner sur la période
     */
    private void controleAbsenceChangementNirAssure(PeriodeRecue periode, List<MessageControle> messages) {

        ParamControleSignalRejet paramControleSignalRejet = verificationRealisationControle(ParamControleSignalRejet.ID_CHANGEMENT_NIR_ASSURE,
                periode);

        // On ne réalise pas le controle
        if (paramControleSignalRejet == null) {
            return;
        }

        if (!rattachementDeclarationsRecuesRepository.existeUnChangementNIRIndividuRattache(periode.getIdPeriode())) {
            // Pas de changement de NIR d'assuré = pas de message
            return;
        }
        LOGGER.debug("Détection d'un changement de NIR d'un assuré pour la période {}", periode.getIdPeriode());

        messages.add(creeMessageControleREDAC(periode, paramControleSignalRejet, null, null, null, null));
    }

    /**
     * Contrôle sur les écarts de montant de cotisations déclarées et calculées F04_RG_P6_17
     * 
     * @param periode
     *            la période controlée
     * @param messages
     *            les messages de contrôle à renseigner sur la période
     * @throws Exception
     */
    private void controleEcartCotisationDeclareeCalculee(PeriodeRecue periode, List<MessageControle> messages) {
        ParamControleSignalRejet paramControleSignalRejet = verificationRealisationControle(ParamControleSignalRejet.ID_DIFF_COTIS, periode);

        // On ne réalise pas le controle
        if (paramControleSignalRejet == null) {
            return;
        }

        // récupération valeur par défaut
        ParamValeurDefaut parametrageParDefaut = parametrageControlesFonctionnels.getParametrageParDefaut();

        // déclenche un code 80 , car l'exception != RedacUnexpectedException (cf process() qui gère le code 20 ).
        if (parametrageParDefaut == null) {
            throw new RedacException("Erreur dans la récupération du paramétrage par défaut : aucun paramétrage trouvé");
        }
        if (parametrageParDefaut.getSeuilDifferenceCotisationEnMontant() == null
                || parametrageParDefaut.getSeuilDifferenceCotisationEnPourcentage() == null) {
            throw new RedacException("Erreur dans la récupération du paramétrage par défaut " + parametrageParDefaut.toString());
        }

        // calcul montants cotisation déclaré & estimé
        Double montantCotisationDeclare = categorieQuittancementIndividuRepository.getSommeMontantsCotisationDeclare(periode.getIdPeriode());
        Double montantCotisationEstime = categorieQuittancementIndividuRepository.getSommeMontantsCotisationEstime(periode.getIdPeriode());

        // considérés = 0 si null pour la suite des calculs
        if (montantCotisationDeclare == null) {
            montantCotisationDeclare = 0d;
        }
        if (montantCotisationEstime == null) {
            montantCotisationEstime = 0d;
        }

        // calcul ecart pourcentage et montant cotisation
        double montantEcartAbsolu = Math.abs(montantCotisationDeclare.doubleValue() - montantCotisationEstime.doubleValue());
        double pourcentageEcartAbsolu = Double.compare(montantCotisationEstime.doubleValue(), 0d) == 0 ? 100d
                : Math.abs(((montantCotisationDeclare.doubleValue() / montantCotisationEstime.doubleValue()) - 1) * 100);

        if (montantEcartAbsolu >= parametrageParDefaut.getSeuilDifferenceCotisationEnMontant().doubleValue()
                && pourcentageEcartAbsolu >= parametrageParDefaut.getSeuilDifferenceCotisationEnPourcentage().doubleValue()) {

            LOGGER.debug("Détection d'un écart de montant de cotisations déclarée et calculée {}", periode.getIdPeriode());

            messages.add(creeMessageControleREDAC(periode, paramControleSignalRejet, montantCotisationEstime.toString(),
                    montantCotisationDeclare.toString(), parametrageParDefaut.getSeuilDifferenceCotisationEnPourcentage().toString(),
                    parametrageParDefaut.getSeuilDifferenceCotisationEnMontant().toString()));
        }

    }

    /**
     * Crée un message de contrôle généré par REDAC.
     * 
     * @param periode
     *            la période contrôlée
     * @param paramControleSignalRejet
     *            le contrôle réalisé
     * @param paramMessage1
     *            la chaîne de caractère pour remplacer les emplacements $1 dans le message utilisateur
     * @param paramMessage2
     *            la chaîne de caractère pour remplacer les emplacements $2 dans le message utilisateur
     * @param paramMessage3
     *            la chaîne de caractère pour remplacer les emplacements $3 dans le message utilisateur
     * @param paramMessage4
     *            la chaîne de caractère pour remplacer les emplacements $4 dans le message utilisateur
     * 
     * @return le message de contrôle créé
     */
    private MessageControle creeMessageControleREDAC(PeriodeRecue periode, ParamControleSignalRejet paramControleSignalRejet, String paramMessage1,
            String paramMessage2, String paramMessage3, String paramMessage4) {
        MessageControle message = new MessageControle();
        message.setIdPeriode(periode.getIdPeriode());
        message.setIdentifiantControle(paramControleSignalRejet.getIdentifiantControle());
        message.setOrigineMessage(MessageControle.ORIGINE_MESSAGE_REDAC);
        message.setNiveauAlerte(paramControleSignalRejet.getNiveauAlerte());
        message.setDateTraitement(DateRedac.maintenant());
        message.setParamMessage1(paramMessage1);
        message.setParamMessage2(paramMessage2);
        message.setParamMessage3(paramMessage3);
        message.setParamMessage4(paramMessage4);
        message.setMessageUtilisateur(
                creeMessageUtilisateur(paramControleSignalRejet.getMessageAlerte(), paramMessage1, paramMessage2, paramMessage3, paramMessage4));
        message.setAuditUtilisateurCreation(auditNomBatch);
        return message;
    }

    /**
     * Crée un message utilisateur à partir d'un template et de paramètres.
     * 
     * @param templateMessage
     *            le template de message
     * @param paramMessage1
     *            la chaîne de caractère pour remplacer les emplacements $1
     * @param paramMessage2
     *            la chaîne de caractère pour remplacer les emplacements $2
     * @param paramMessage3
     *            la chaîne de caractère pour remplacer les emplacements $3
     * @param paramMessage4
     *            la chaîne de caractère pour remplacer les emplacements $4
     * @return le message créé
     */
    private String creeMessageUtilisateur(String templateMessage, String paramMessage1, String paramMessage2, String paramMessage3,
            String paramMessage4) {
        String messageUtilisateur = templateMessage;
        messageUtilisateur = StringUtils.replace(messageUtilisateur, "$1", paramMessage1 != null ? paramMessage1 : StringUtils.EMPTY);
        messageUtilisateur = StringUtils.replace(messageUtilisateur, "$2", paramMessage2 != null ? paramMessage2 : StringUtils.EMPTY);
        messageUtilisateur = StringUtils.replace(messageUtilisateur, "$3", paramMessage3 != null ? paramMessage3 : StringUtils.EMPTY);
        messageUtilisateur = StringUtils.replace(messageUtilisateur, "$4", paramMessage4 != null ? paramMessage4 : StringUtils.EMPTY);
        return messageUtilisateur;
    }

    /**
     * Crée un message de contrôle généré par REDAC sur bases assujetties cf F04_RG_P6_09
     * 
     * @param periode
     *            la période contrôlée
     * @param paramControleSignalRejet
     *            le contrôle réalisé
     * @param paramMessage1
     *            la chaîne de caractère pour remplacer les emplacements $1 dans le message utilisateur
     * @param paramMessage2
     *            la chaîne de caractère pour remplacer les emplacements $2 dans le message utilisateur
     * @param paramMessage3
     *            la chaîne de caractère pour remplacer les emplacements $3 dans le message utilisateur
     * @param paramMessage4
     *            la chaîne de caractère pour remplacer les emplacements $4 dans le message utilisateur
     * 
     * @return le message de contrôle créé
     */
    private MessageControle creeMessageControleREDACBaseAssujettie(PeriodeRecue periode, ParamControleSignalRejet paramControleSignalRejet,
            String paramMessage1, String paramMessage2, String paramMessage3, String paramMessage4) {
        MessageControle message = new MessageControle();
        message.setIdPeriode(periode.getIdPeriode());
        message.setIdentifiantControle(paramControleSignalRejet.getIdentifiantControle());
        message.setOrigineMessage(MessageControle.ORIGINE_MESSAGE_REDAC);

        ExtensionContrat extensionContrat = extensionContratRepository.getExtensionContrat(periode.getNumeroContrat());
        IndicateursDSNPeriode indicateurDSNPeriode = indicateurDSNPeriodeRepository.getIndicateursPourPeriode(periode.getNumeroContrat(),
                periode.getDateDebutPeriode(), periode.getDateFinPeriode());

        String controleTypeBaseAssuj = "";
        if (extensionContrat != null && extensionContrat.getDateEffetIndicateurs() <= periode.getDateDebutPeriode()) {
            controleTypeBaseAssuj = extensionContrat.getControleTypeBaseAssujettie();
        } else if (indicateurDSNPeriode != null) {
            controleTypeBaseAssuj = indicateurDSNPeriode.getControleTypeBaseAssujettie();
        } else {
            Contrat contrat = contratRepository.getContratValidePourDate(periode.getNumeroContrat(), periode.getDateFinPeriode());
            if (contrat != null) {
                Integer modeCalculCotisation = tarifRepository.getModeCalculCotisation(contrat.getNumContrat());
                controleTypeBaseAssuj = paramFamilleModeCalculContratRepository.getModeControleTypesBasesAssujetties(contrat.getNumFamilleProduit(),
                        contrat.getGroupeGestion(), modeCalculCotisation);
            }
        }

        if (!StringUtils.equals("N", controleTypeBaseAssuj)) {
            message.setNiveauAlerte("REJET");
        } else {
            message.setNiveauAlerte(paramControleSignalRejet.getNiveauAlerte());
        }

        message.setDateTraitement(DateRedac.maintenant());
        message.setParamMessage1(paramMessage1);
        message.setParamMessage2(paramMessage2);
        message.setParamMessage3(paramMessage3);
        message.setParamMessage4(paramMessage4);
        message.setMessageUtilisateur(
                creeMessageUtilisateur(paramControleSignalRejet.getMessageAlerte(), paramMessage1, paramMessage2, paramMessage3, paramMessage4));
        message.setAuditUtilisateurCreation(auditNomBatch);
        return message;
    }

}
