package fr.si2m.red.batch.flux10.item;

import org.apache.commons.lang3.StringUtils;

import fr.si2m.red.batch.moteur.erreur.ValidateurLigneAvecCollecte;
import fr.si2m.red.parametrage.ParamCategorieEffectifs;

/**
 * Validateur de ligne de paramétrage code libellé.
 * 
 * @author poidij
 *
 */
public class ParamCategorieEffectifsValidateurLigne extends ValidateurLigneAvecCollecte<ParamCategorieEffectifs> {

    @Override
    protected void valide(final ParamCategorieEffectifs categoriesEffectifs) {
        // Champs obligatoires

        // F10_RG_C77
        valideChampAvecCollecte(StringUtils.isNotBlank(categoriesEffectifs.getNumCategorie()), categoriesEffectifs, "NOCAT",
                "le champ NOCAT obligatoire, n’est pas renseigné");
        // F10_RG_C78
        valideChampAvecCollecte(StringUtils.isNotBlank(categoriesEffectifs.getLibelleCategorie()), categoriesEffectifs, "LICAT",
                "le champ LICAT obligatoire, n’est pas renseigné");

        // Longueur des champs
        // F10_RG_C77
        if (categoriesEffectifs.getNumCategorie() != null) {
            valideChampAvecCollecte(categoriesEffectifs.getNumCategorie().length() <= 30, categoriesEffectifs, "NOCAT",
                    "le champ NOCAT dépasse la taille maximale prévue");
        }
        // F10_RG_C78
        if (StringUtils.isNotBlank(categoriesEffectifs.getLibelleCategorie())) {
            valideChampAvecCollecte(categoriesEffectifs.getLibelleCategorie().length() <= 60, categoriesEffectifs, "LICAT",
                    "le champ LICAT dépasse la taille maximale prévue");
        }
        // F10_RG_C79
        if (categoriesEffectifs.getPrioriteAffilies() != null) {
            valideChampAvecCollecte(categoriesEffectifs.getPrioriteAffilies() < 10, categoriesEffectifs, "PRIO_AFFILIE",
                    "le champ PRIO_AFFILIE dépasse la taille maximale prévue");
        }
        // F10_RG_C80
        if (categoriesEffectifs.getPrioriteAyantsDroitAdultes() != null) {
            valideChampAvecCollecte(categoriesEffectifs.getPrioriteAyantsDroitAdultes() < 10, categoriesEffectifs, "PRIO_AYDR_ADULTE",
                    "le champ PRIO_AYDR_ADULTE dépasse la taille maximale prévue");
        }
        // F10_RG_C81
        if (categoriesEffectifs.getSeuilAyantsDroitAdultes() != null) {
            valideChampAvecCollecte(categoriesEffectifs.getSeuilAyantsDroitAdultes() < 100, categoriesEffectifs, "SEUIL_DECPT_AYDR_ADULTE",
                    "le champ SEUIL_DECPT_AYDR_ADULTE dépasse la taille maximale prévue");
        }
        // F10_RG_C82
        if (categoriesEffectifs.getPrioriteAyantsDroitAutres() != null) {
            valideChampAvecCollecte(categoriesEffectifs.getPrioriteAyantsDroitAutres() < 10, categoriesEffectifs, "PRIO_AYDR_AUTRE",
                    "le champ PRIO_AYDR_AUTR dépasse la taille maximale prévue");
        }
        // F10_RG_C83
        if (categoriesEffectifs.getSeuilAyantsDroitAutres() != null) {
            valideChampAvecCollecte(categoriesEffectifs.getSeuilAyantsDroitAutres() < 100, categoriesEffectifs, "SEUIL_DECPT_AYDR_AUTRE",
                    "le champ SEUIL_DECPT_AYDR_AUTRE dépasse la taille maximale prévue");
        }
        // F10_RG_C84
        if (categoriesEffectifs.getPrioriteAyantsDroitEnfants() != null) {
            valideChampAvecCollecte(categoriesEffectifs.getPrioriteAyantsDroitEnfants() < 10, categoriesEffectifs, "PRIO_AYDR_ENFANT",
                    "le champ PRIO_AYDR_ENFANT dépasse la taille maximale prévue");
        }
        // F10_RG_C85
        if (categoriesEffectifs.getSeuilAyantsDroitEnfants() != null) {
            valideChampAvecCollecte(categoriesEffectifs.getSeuilAyantsDroitEnfants() < 100, categoriesEffectifs, "SEUIL_DECPT_AYDR_ENFANT",
                    "le champ SEUIL_DECPT_AYDR_ENFANT dépasse la taille maximale prévue");
        }
    }

}
