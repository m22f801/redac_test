package fr.si2m.red.batch.flux1.item;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;

import fr.si2m.red.batch.moteur.EtapeCodeRetourModificateur;
import fr.si2m.red.contrat.TarifRepository;

/**
 * Tache de calcul de la nature contrat à partir du mocalcot
 * 
 * @author dagnonh
 *
 */
public class TacheAlimentationNatureContratTarif extends EtapeCodeRetourModificateur implements Tasklet {
    private static final Logger LOGGER = LoggerFactory.getLogger(TacheAlimentationNatureContratTarif.class);

    @Autowired
    TarifRepository tarifRepository;

    @Override
    public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) throws Exception {
        this.saveStepExecution(chunkContext.getStepContext().getStepExecution());

        LOGGER.info("Début alimentation du champ NATURE_CONTRAT de l'entité TARIF");
        // F01_RG_T03
        tarifRepository.updateNatureContratTarif();
        LOGGER.info("Fin alimentation du champ NATURE_CONTRAT de l'entité TARIF");

        return RepeatStatus.FINISHED;
    }

}
