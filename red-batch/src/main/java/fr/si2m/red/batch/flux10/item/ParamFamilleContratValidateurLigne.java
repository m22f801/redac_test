package fr.si2m.red.batch.flux10.item;

import org.apache.commons.lang3.StringUtils;

import fr.si2m.red.batch.moteur.erreur.ValidateurLigneAvecCollecte;
import fr.si2m.red.parametrage.ParamFamilleContrat;

/**
 * Validateur de ligne .
 * 
 * @author poidij
 *
 */
public class ParamFamilleContratValidateurLigne extends ValidateurLigneAvecCollecte<ParamFamilleContrat> {

    @Override
    protected void valide(final ParamFamilleContrat familleContrat) {
        // Champs obligatoires

        valideChampAvecCollecte(familleContrat.getNumFamille() != null, familleContrat, "NOFAM", "le champ NOFAM obligatoire, n’est pas renseigné");

        valideChampAvecCollecte(StringUtils.isNotBlank(familleContrat.getPortefeuille()), familleContrat, "PORTEFEUILLE",
                "le champ PORTEFEUILLE obligatoire, n’est pas renseigné");

        valideChampAvecCollecte(StringUtils.isNotBlank(familleContrat.getAvecPopulation()), familleContrat, "AVEC_POPULATION",
                "le champ AVEC_POPULATION obligatoire, n’est pas renseigné");

        valideChampAvecCollecte(StringUtils.isNoneBlank(familleContrat.getGroupeGestion()), familleContrat, "NMGRPGES",
                "le champ NMGRPGES obligatoire, n’est pas renseigné");

        // Longueur des champs

        // on vérifie que le nombre NOFAM a au plus 3 chiffres
        if (familleContrat.getNumFamille() != null) {
            valideChampAvecCollecte(familleContrat.getNumFamille() < 1000, familleContrat, "NOFAM",
                    "le champ NOFAM dépasse la taille maximale prévue");
        }

        valideChampAvecCollecte(familleContrat.getDesignationFamille().length() <= 30, familleContrat, "DESIGNATION_FAMILLE",
                "le champ DESIGNATION_FAMILLE dépasse la taille maximale prévue");

        valideChampAvecCollecte(familleContrat.getTypeContratCodeDSN().length() <= 30, familleContrat, "TYPE_CONT_BRIQUEDSN",
                "le champ TYPE_CONT_BRIQUEDSN dépasse la taille maximale prévue");

        valideChampAvecCollecte(familleContrat.getApplicationAvalEnCharge().length() <= 4, familleContrat, "SI_AVAL",
                "le champ SI_AVAL dépasse la taille maximale prévue");

        valideChampAvecCollecte(familleContrat.getPortefeuille().length() <= 7, familleContrat, "PORTEFEUILLE",
                "le champ PORTEFEUILLE dépasse la taille maximale prévue");

        valideChampAvecCollecte(familleContrat.getTypeConsolidationSalaire().length() <= 5, familleContrat, "TYPE_CONSO_SALAIRE",
                "le champ TYPE_CONSO_SALAIRE dépasse la taille maximale prévue");

        valideChampAvecCollecte(familleContrat.getAvecPopulation().length() <= 3, familleContrat, "AVEC_POPULATION",
                "le champ AVEC_POPULATION dépasse la taille maximale prévue");

        valideChampAvecCollecte(familleContrat.getGroupeGestion().length() <= 2, familleContrat, "NMGRPGES",
                "le champ NMGRPGES dépasse la taille maximale prévue");

    }
}
