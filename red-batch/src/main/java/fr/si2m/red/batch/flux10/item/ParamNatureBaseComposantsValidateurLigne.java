package fr.si2m.red.batch.flux10.item;

import org.apache.commons.lang3.StringUtils;

import fr.si2m.red.batch.moteur.erreur.ValidateurLigneAvecCollecte;
import fr.si2m.red.parametrage.ParamNatureBaseComposants;

/**
 * Validateur de ligne de paramétrage nature de base de composants.
 * 
 * @author delortj
 *
 */
public class ParamNatureBaseComposantsValidateurLigne extends ValidateurLigneAvecCollecte<ParamNatureBaseComposants> {

    @Override
    protected void valide(final ParamNatureBaseComposants natureBaseComposants) {
        // Champs obligatoires

        valideChampAvecCollecte(natureBaseComposants.getCodeNatureBaseCotisations() != null, natureBaseComposants, "CONBCOT",
                "le champ CONBCOT obligatoire, n’est pas renseigné");

        valideChampAvecCollecte(StringUtils.isNotBlank(natureBaseComposants.getTauxCalculRempliAsText()), natureBaseComposants, "TXCALCU_REMPLI",
                "le champ TXCALCU_REMPLI obligatoire, n’est pas renseigné");

        valideChampAvecCollecte(natureBaseComposants.getNumTranche() != null, natureBaseComposants, "NUM_TRANCHE",
                "le champ NUM_TRANCHE obligatoire, n’est pas renseigné");

        valideChampAvecCollecte(natureBaseComposants.getNumTypeComposantBase() != null, natureBaseComposants, "NUM_TCBA",
                "le champ NUM_TCBA obligatoire, n’est pas renseigné");

        // Longueur des champs
        if (natureBaseComposants.getCodeNatureBaseCotisations() != null) {
            valideChampAvecCollecte(natureBaseComposants.getCodeNatureBaseCotisations() < 100, natureBaseComposants, "CONBCOT",
                    "le champ CONBCOT dépasse la taille maximale prévue");
        }

        if (StringUtils.isNotBlank(natureBaseComposants.getTauxCalculRempliAsText())) {
            valideChampAvecCollecte(natureBaseComposants.getTauxCalculRempliAsText().length() <= 1, natureBaseComposants, "TXCALCU_REMPLI",
                    "le champ TXCALCU_REMPLI dépasse la taille maximale prévue");
        }

        if (natureBaseComposants.getNumTranche() != null) {
            valideChampAvecCollecte(natureBaseComposants.getNumTranche() < 10, natureBaseComposants, "NUM_TRANCHE",
                    "le champ NUM_TRANCHE dépasse la taille maximale prévue");
        }

        if (natureBaseComposants.getNumTypeComposantBase() != null) {
            valideChampAvecCollecte(natureBaseComposants.getNumTypeComposantBase() < 100, natureBaseComposants, "NUM_TCBA",
                    "le champ NUM_TCBA dépasse la taille maximale prévue");
        }

        valideChampAvecCollecte(natureBaseComposants.getModeConsolidation().length() <= 1, natureBaseComposants, "MODE_CONSO",
                "le champ MODE_CONSO dépasse la taille maximale prévue");

    }

}
