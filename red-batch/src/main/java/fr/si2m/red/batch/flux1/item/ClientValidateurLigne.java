package fr.si2m.red.batch.flux1.item;

import fr.si2m.red.DateRedac;
import fr.si2m.red.batch.moteur.erreur.ValidateurLigneAvecCollecte;
import fr.si2m.red.contrat.Client;

/**
 * Validateur de ligne contrat.
 * 
 * @author delortjouvesf
 *
 */
public class ClientValidateurLigne extends ValidateurLigneAvecCollecte<Client> {

    @Override
    protected void valide(final Client client) {

        // Validation des champs obligatoires

        // F01_RG_CCl02
        valideChampAvecCollecte(client.getNumClient() != null, client, "NOCLI", "le champ NOCLI obligatoire, n’est pas renseigné");

        // Longueur des champs

        // F01_RG_CCl02
        valideTailleMaximaleChampAvecCollecte(client.getNumClient(), 6, client, "NOCLI");

        // F01_RG_CCl03
        valideTailleMaximaleChampAvecCollecte(client.getTypeClient(), 2, client, "TYCLI");

        // F01_RG_CCl04
        valideTailleMaximaleChampTexteAvecCollecte(client.getRaisonSociale(), 38, client, "LRSO");

        // F01_RG_CCl05
        valideTailleMaximaleChampTexteAvecCollecte(client.getAdresseBatiment(), 38, client, "LBAT");

        // F01_RG_CCl06
        valideTailleMaximaleChampTexteAvecCollecte(client.getAdresseRue(), 38, client, "LRUE");

        // F01_RG_CCl07
        valideTailleMaximaleChampTexteAvecCollecte(client.getAdresseCommune(), 38, client, "LCMN");

        // F01_RG_CCl08
        valideTailleMaximaleChampTexteAvecCollecte(client.getAdresseCodePostal(), 5, client, "COCP");

        // F01_RG_CCl09
        valideTailleMaximaleChampTexteAvecCollecte(client.getBureauDistributeur(), 32, client, "CBD");

        // F01_RG_CCl10
        valideTailleMaximaleChampTexteAvecCollecte(client.getCodePays(), 3, client, "COPAY");

        // F01_RG_CCl11
        valideTailleMaximaleChampTexteAvecCollecte(client.getCodeNaf(), 5, client, "COPROF");

        // F01_RG_CCl12
        valideTailleMaximaleChampTexteAvecCollecte(client.getNumSiren(), 9, client, "NOSIREN");

        // F01_RG_CCl13
        valideTailleMaximaleChampTexteAvecCollecte(client.getNumSiret(), 5, client, "NOSIRET");

        // F01_RG_CCl14
        valideTailleMaximaleChampTexteAvecCollecte(client.getTelephone(), 18, client, "NOTEL");

        // F01_RG_CCl15
        valideTailleMaximaleChampTexteAvecCollecte(client.getFax(), 18, client, "NOTLC");

        // F01_RG_CCl16
        valideTailleMaximaleChampTexteAvecCollecte(client.getEtatClient(), 1, client, "COETAT");

        // F01_RG_CI17
        client.setDateEtat(valideChampDateAvecCollecte(client.getDateEtat(), DateRedac.FORMAT_DATES, client, "DTETAT", false));

        // F01_RG_CCl18
        valideTailleMaximaleChampAvecCollecte(client.getCodePartenaire(), 2, client, "NUPART");

        // F01_RG_CCl19
        valideTailleMaximaleChampTexteAvecCollecte(client.getNumClientMaitre(), 6, client, "NUMAI");

        // F01_RG_CCl20
        valideTailleMaximaleChampTexteAvecCollecte(client.getCodeCcn(), 4, client, "COCCN");

        // F01_RG_CCl21
        valideTailleMaximaleChampTexteAvecCollecte(client.getCodeGrp(), 4, client, "COGRP");

        // F01_RG_CCl22
        valideTailleMaximaleChampTexteAvecCollecte(client.getCodeTns(), 1, client, "COTNS");

        // F01_RG_CCl23
        valideTailleMaximaleChampTexteAvecCollecte(client.getCodeAutoEntrepreneur(), 1, client, "COAUTOENT");

        // F01_RG_CI24
        client.setDateDerniereModification(
                valideChampDateAvecCollecte(client.getDateDerniereModification(), DateRedac.FORMAT_DATES, client, "DT_DERN_MODIF", false));

    }

}
