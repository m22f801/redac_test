package fr.si2m.red.batch.flux78.item;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import fr.si2m.red.Dual;
import lombok.Setter;

/**
 * Un mapper de ligne {@link Dual} pour renvoyer une instance de EntiteImportableBatch (objet Dual) via JDBC.
 * 
 * @author poidij
 *
 */
public class DualRowMapper implements RowMapper<Dual> {

    @Setter
    private String nomBatch;

    @Override
    public Dual mapRow(ResultSet rs, int rowNum) throws SQLException {
        Dual dual = new Dual();
        dual.setAuditUtilisateurCreation(nomBatch);
        dual.setAuditDateCreation(null);
        dual.setAuditUtilisateurDerniereModification("");
        dual.setAuditDateDerniereModification(null);
        return dual;
    }
}
