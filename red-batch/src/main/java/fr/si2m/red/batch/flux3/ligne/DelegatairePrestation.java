package fr.si2m.red.batch.flux3.ligne;

import lombok.Data;
import fr.si2m.red.DateRedac;

/**
 * Description d'une ligne d'un fichier DELEGATAIRES_PRESTATION à exporter.
 * 
 * @author nortaina
 *
 */
@Data
public class DelegatairePrestation {
    /**
     * Code Mise à jour.
     * 
     * @param codeMiseAJour
     *            Code Mise à jour
     * @return Code Mise à jour
     */
    private String codeMiseAJour;

    /**
     * Identifiant technique du délégataire de prestation (N° de séquence local à une instance de cette interface).
     * 
     * @param identifiantTechniqueDelegatairePrestation
     *            Identifiant technique du délégataire de prestation (N° de séquence local à une instance de cette interface)
     * @return Identifiant technique du délégataire de prestation (N° de séquence local à une instance de cette interface)
     */
    private String identifiantTechniqueDelegatairePrestation;

    /**
     * Date début d’application des caractéristiques du délégataire.
     * 
     * @param dateDebutApplicationCaracteristiques
     *            Date début d’application des caractéristiques du délégataire
     * @return Date début d’application des caractéristiques du délégataire
     */
    private Integer dateDebutApplicationCaracteristiques;

    /**
     * Date fin d’application des caractéristiques du délégataire.
     * 
     * @param dateFinApplicationCaracteristiques
     *            Date fin d’application des caractéristiques du délégataire
     * @return Date fin d’application des caractéristiques du délégataire
     */
    private Integer dateFinApplicationCaracteristiques;

    /**
     * Code organisme délégataire de prestation selon le SI source.
     * 
     * @param codeOrganismeDelegatairePrestation
     *            Code organisme délégataire de prestation selon le SI source
     * @return Code organisme délégataire de prestation selon le SI source
     */
    private String codeOrganismeDelegatairePrestation;

    /**
     * Libellé du délégataire de prestation.
     * 
     * @param libelleDelegatairePrestation
     *            Libellé du délégataire de prestation
     * @return Libellé du délégataire de prestation
     */
    private String libelleDelegatairePrestation;

    /**
     * Date début d’application des caractéristiques du délégataire.
     * 
     * @return Date début d’application des caractéristiques du délégataire
     */
    public String getDateDebutApplicationCaracteristiquesFormatee() {
        return DateRedac.formate(DateRedac.EXPORT_FORMAT_PAR_DEFAUT, dateDebutApplicationCaracteristiques);
    }

    /**
     * Date fin d’application des caractéristiques du délégataire.
     * 
     * @return Date fin d’application des caractéristiques du délégataire
     */
    public String getDateFinApplicationCaracteristiquesFormatee() {
        return DateRedac.formate(DateRedac.EXPORT_FORMAT_PAR_DEFAUT, dateFinApplicationCaracteristiques);
    }

}
