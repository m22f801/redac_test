package fr.si2m.red.batch.flux78.item;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.annotation.AfterStep;
import org.springframework.batch.core.annotation.BeforeStep;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;

import fr.si2m.red.DateRedac;
import fr.si2m.red.RedacUnexpectedException;
import fr.si2m.red.batch.moteur.item.TransformateurDonneeCodeRetourModificateur;
import fr.si2m.red.complement.ExtensionContrat;
import fr.si2m.red.complement.ExtensionContratRepository;
import fr.si2m.red.contrat.Contrat;
import fr.si2m.red.contrat.ContratRepository;
import fr.si2m.red.contrat.TarifRepository;
import fr.si2m.red.core.repository.jpa.conversion.ConvertisseurNumeriqueDecimal;
import fr.si2m.red.dsn.ComposantVersement;
import fr.si2m.red.dsn.ComposantVersementRepository;
import fr.si2m.red.dsn.Versement;
import fr.si2m.red.parametrage.ParamFamilleModeCalculContratRepository;
import fr.si2m.red.reconciliation.CategorieQuittancementIndividu;
import fr.si2m.red.reconciliation.CategorieQuittancementIndividuRepository;
import fr.si2m.red.reconciliation.EffectifCategorieMouvement;
import fr.si2m.red.reconciliation.EffectifCategorieMouvementRepository;
import fr.si2m.red.reconciliation.PeriodeRecue;
import fr.si2m.red.reconciliation.RattachementDeclarationsRecuesRepository;
import fr.si2m.red.reconciliation.TrancheCategorie;
import fr.si2m.red.reconciliation.TrancheCategorieRepository;
import lombok.Setter;

/**
 * Création de lignes de périodes publiées pour WQUI.
 * 
 * @author poidij
 *
 */
public class PublicationWQUIProcesseur extends TransformateurDonneeCodeRetourModificateur<PeriodeRecue, List<PeriodePublieeWQUI>> {

    private static final Logger LOGGER = LoggerFactory.getLogger(PublicationWQUIProcesseur.class);

    @Autowired
    private TarifRepository tarifRepository;

    @Autowired
    private CategorieQuittancementIndividuRepository categorieQuittancementIndividuRepository;

    @Autowired
    private TrancheCategorieRepository trancheCategorieRepository;

    @Autowired
    private RattachementDeclarationsRecuesRepository rattachementDeclarationsRecuesRepository;

    @Autowired
    private ComposantVersementRepository composantVersementRepository;

    @Autowired
    private EffectifCategorieMouvementRepository effectifCategorieMouvementRepository;

    @Autowired
    private ExtensionContratRepository extensionContratRepository;
    @Autowired
    private ParamFamilleModeCalculContratRepository paramFamilleModeCalculContratRepository;
    @Autowired
    private ContratRepository contratRepository;

    @PersistenceContext
    private EntityManager entityManager;

    @Setter
    private Resource fichierSortie;
    @Setter
    private Resource fichierSortieTemporaire;
    @Setter
    private String dateDuJour;
    @Setter
    private String cheminAbsoluFichierLogErreurs;

    private PublicationRedacteurFichierPlat<String> publicationRedacteurFichierPlatHeader = new PublicationRedacteurFichierPlat<String>();

    private PublicationRedacteurFichierPlat<PeriodePublieeWQUI> publicationRedacteurFichierPlatData = new PublicationRedacteurFichierPlat<PeriodePublieeWQUI>();

    private int nbDeLignes = 0;

    private Set<String> couples = new LinkedHashSet<String>();

    @Override
    public List<PeriodePublieeWQUI> process(PeriodeRecue periode) throws Exception {

        Integer natureContrat = tarifRepository.getNatureContratPourPeriode(periode.getNumeroContrat(), periode.getDateDebutPeriode(),
                periode.getDateFinPeriode());

        if ((natureContrat == null) || ((natureContrat != 1) && (natureContrat != 2))) {
            String mesageErreur = "La nature du contrat " + natureContrat
                    + " ne correspond ni à un contrat sur effectifs (1) ni à un contrat sur salaires (2). Période traitée : [PeriodeRecue(idPeriode="
                    + periode.getIdPeriode() + ", numeroContrat=" + periode.getNumeroContrat() + ", dateDebutPeriode=" + periode.getDateDebutPeriode()
                    + ", dateFinPeriode=" + periode.getDateFinPeriode() + ", typePeriode=" + periode.getTypePeriode() + ", dateCreation="
                    + periode.getDateCreation() + ")]";
            traceErreurSansBlocage(mesageErreur, cheminAbsoluFichierLogErreurs);
        } else if (natureContrat == 1) {
            traitePeriodePourContratSurEffectifs(periode);
        } else if (natureContrat == 2) {
            traitePeriodePourContratSurSalaires(periode);
        }

        return new ArrayList<PeriodePublieeWQUI>();
    }

    /**
     * Traite une période donnée pour un contrat sur effectifs.
     * 
     * @param periode
     *            la période à traiter.
     */
    private void traitePeriodePourContratSurEffectifs(PeriodeRecue periode) {
        // F78_RG_P2_05 : Période d'un contrat sur effectifs
        List<PeriodePublieeWQUI> listePeriodesPubliees = new ArrayList<PeriodePublieeWQUI>();

        // Volumétrie maîtrisée pour les contrats sur effectifs. Pas besoin de paginer.
        List<CategorieQuittancementIndividu> listeCategoriesQuittancements = categorieQuittancementIndividuRepository
                .getPourPeriode(periode.getIdPeriode());

        for (CategorieQuittancementIndividu categorieQuittancement : listeCategoriesQuittancements) {
            PeriodeContratSurEffectifs periodeEffectifs = new PeriodeContratSurEffectifs();

            periodeEffectifs.setIdPeriode(periode.getIdPeriode());

            // Partie commune
            periodeEffectifs.setTypeEnregistrement(2);

            String numerocontrat = periode.getNumeroContrat();
            periodeEffectifs.setContratZoneA(Integer.valueOf(StringUtils.substring(numerocontrat, 0, 7)));
            periodeEffectifs.setContratZoneB(Integer.valueOf(StringUtils.substring(numerocontrat, 7, 12)));
            periodeEffectifs.setContratZoneC(StringUtils.substring(numerocontrat, 12));
            periodeEffectifs.setDateDebutPeriode(periode.getDateDebutPeriode());
            periodeEffectifs.setDateFinPeriode(periode.getDateFinPeriode());
            periodeEffectifs.setCategorieQuittancement(StringUtils.substring(categorieQuittancement.getNumCategorieQuittancement(), 0, 3));
            periodeEffectifs
                    .setLibelleCategorieQuittancement(StringUtils.substring(categorieQuittancement.getLibelleCategorieQuittancement(), 0, 25));
            periodeEffectifs.setDateDebutPeriodeTarification(periode.getDateDebutPeriode());
            periodeEffectifs.setDateFinPeriodeTarification(periode.getDateFinPeriode());
            periodeEffectifs.setNumTranche(0);
            periodeEffectifs.setTypeBase("1");
            periodeEffectifs.setTypeDocument(1);
            periodeEffectifs.setPeriodeComplementaireOuCorrective(
                    StringUtils.equals(periode.getTypePeriode(), PeriodeRecue.TYPE_PERIODE_DECLARATION) ? "N" : "O");

            remplissageChampsPaiement(periodeEffectifs);

            periodeEffectifs.setEffectifDebutPeriode(categorieQuittancement.getEffectifDebut());
            Integer dateEffectifFin = periode.getDateFinPeriode();
            if (DateRedac.selectionnePartieDate(dateEffectifFin, DateRedac.MODIFIER_JOUR) == 31
                    && DateRedac.selectionnePartieDate(dateEffectifFin, DateRedac.MODIFIER_MOIS) == 12) {
                periodeEffectifs.setEffectifFinDecembre(categorieQuittancement.getEffectifFin());
            } else {
                periodeEffectifs.setEffectifFinDecembre(0L);
            }

            Double montantCotisation = categorieQuittancement.getMontantCotisation();
            periodeEffectifs.setMontantCotisation(NumberUtils.degradeDoubleEnLongValeurAbsolue(montantCotisation, 2));

            periodeEffectifs.setSigneMontantCotisation(montantCotisation != null && montantCotisation.doubleValue() >= 0d ? "+" : "-");

            periodeEffectifs.setModeDeclaration(
                    rechercheModeDeclaration(periode.getNumeroContrat(), periode.getDateDebutPeriode(), periode.getDateFinPeriode(), 1));

            periodeEffectifs.setFillerPartieCommune("");

            // Partie spécifique

            List<EffectifCategorieMouvement> listeDesMouvements = effectifCategorieMouvementRepository
                    .getPourCategorie(categorieQuittancement.getIdPeriode(), categorieQuittancement.getNumCategorieQuittancement());

            if (listeDesMouvements.isEmpty()) {
                // F78_RG_P2_05 : Valeurs par défaut, s'il n'y a pas de mouvement pour la catégorie
                periodeEffectifs.setDateEffetIR(0);
                periodeEffectifs.setNombreIR(0);
                periodeEffectifs.setIncorporeRadie("");
                periodeEffectifs.setFillerPartieSpecifique("");

                listePeriodesPubliees.add(periodeEffectifs);
            } else {
                // F78_RG_P2_05 : Valeurs renseignées pour chaque mouvement de la catégorie
                for (EffectifCategorieMouvement mouvement : listeDesMouvements) {
                    PeriodeContratSurEffectifs periodeEffectifsParMouvement = clonePeriodeEffectif(periodeEffectifs);

                    periodeEffectifsParMouvement.setDateEffetIR(mouvement.getDateEffetMouvement());
                    periodeEffectifsParMouvement.setNombreIR(Math.abs(mouvement.getNombreMouvement()));
                    periodeEffectifsParMouvement.setIncorporeRadie(rechercheIncorporeRadie(mouvement.getNombreMouvement()));
                    periodeEffectifsParMouvement.setFillerPartieSpecifique("");

                    listePeriodesPubliees.add(periodeEffectifsParMouvement);
                }
            }
        }

        redigeLigneData(listePeriodesPubliees);
    }

    private PeriodeContratSurEffectifs clonePeriodeEffectif(PeriodeContratSurEffectifs periodeACloner) {
        PeriodeContratSurEffectifs periodeEffectifs = new PeriodeContratSurEffectifs();

        periodeEffectifs.setCategorieQuittancement(periodeACloner.getCategorieQuittancement());
        periodeEffectifs.setContratZoneA(periodeACloner.getContratZoneA());
        periodeEffectifs.setContratZoneB(periodeACloner.getContratZoneB());
        periodeEffectifs.setContratZoneC(periodeACloner.getContratZoneC());
        periodeEffectifs.setDateDebutPeriode(periodeACloner.getDateDebutPeriode());
        periodeEffectifs.setDateDebutPeriodeTarification(periodeACloner.getDateDebutPeriodeTarification());
        periodeEffectifs.setDateFinPeriode(periodeACloner.getDateFinPeriode());
        periodeEffectifs.setDateFinPeriodeTarification(periodeACloner.getDateFinPeriodeTarification());
        periodeEffectifs.setEffectifDebutPeriode(periodeACloner.getEffectifDebutPeriode());
        periodeEffectifs.setEffectifFinDecembre(periodeACloner.getEffectifFinDecembre());
        periodeEffectifs.setIdPeriode(periodeACloner.getIdPeriode());
        periodeEffectifs.setLibelleCategorieQuittancement(periodeACloner.getLibelleCategorieQuittancement());
        periodeEffectifs.setModePaiement(periodeACloner.getModePaiement());
        periodeEffectifs.setMontantCotisation(periodeACloner.getMontantCotisation());
        periodeEffectifs.setNumTranche(periodeACloner.getNumTranche());
        periodeEffectifs.setPeriodeComplementaireOuCorrective(periodeACloner.getPeriodeComplementaireOuCorrective());
        periodeEffectifs.setReferencePaiement(periodeACloner.getReferencePaiement());
        periodeEffectifs.setSigneMontantCotisation(periodeACloner.getSigneMontantCotisation());
        periodeEffectifs.setTypeBase(periodeACloner.getTypeBase());
        periodeEffectifs.setTypeDocument(periodeACloner.getTypeDocument());
        periodeEffectifs.setTypeEnregistrement(periodeACloner.getTypeEnregistrement());
        periodeEffectifs.setDateEffetIR(periodeACloner.getDateEffetIR());
        periodeEffectifs.setNombreIR(periodeACloner.getNombreIR());
        periodeEffectifs.setIncorporeRadie(periodeACloner.getIncorporeRadie());
        periodeEffectifs.setFillerPartieSpecifique(periodeACloner.getFillerPartieSpecifique());
        periodeEffectifs.setModeDeclaration(periodeACloner.getModeDeclaration());

        return periodeEffectifs;
    }

    /**
     * Traite une période donnée pour un contrat sur salaires.
     * 
     * @param periode
     *            la période à traiter.
     */
    private void traitePeriodePourContratSurSalaires(PeriodeRecue periode) {

        // F78_RG_P2_04 : Période d'un contrat sur salaires

        // Avec des hypothèses pessimistes, on peut potentiellement avoir de l'ordre de 500 000 CategorieQuittancementIndividu à parcourir par entité
        // PeriodeRecue et donc encore plus de TrancheCategorie => On pagine les traitements par 2000 tranches
        final int taillePage = 2000;
        int numeroPage = 0;

        List<TrancheCategorie> listeDesTranches = trancheCategorieRepository.getPourPeriodePagines(periode.getIdPeriode(), numeroPage * taillePage,
                taillePage);
        while (!listeDesTranches.isEmpty()) {
            LOGGER.debug("Traitement de la page {} de {} catégories de tranches en cours...", numeroPage, taillePage);
            List<PeriodePublieeWQUI> listePeriodesPubliees = new ArrayList<>(listeDesTranches.size());

            for (TrancheCategorie tranche : listeDesTranches) {
                CategorieQuittancementIndividu categorieQuittancement = categorieQuittancementIndividuRepository
                        .getCategorieQuittancementPourPeriode(tranche.getIdPeriode(), tranche.getNumCategorieQuittancement());

                PeriodeContratSurSalaires periodeSalaires = new PeriodeContratSurSalaires();
                periodeSalaires.setIdPeriode(periode.getIdPeriode());

                // Partie commune
                periodeSalaires.setTypeEnregistrement(2);

                String numerocontrat = periode.getNumeroContrat();
                periodeSalaires.setContratZoneA(Integer.valueOf(StringUtils.substring(numerocontrat, 0, 7)));
                periodeSalaires.setContratZoneB(Integer.valueOf(StringUtils.substring(numerocontrat, 7, 12)));
                periodeSalaires.setContratZoneC(StringUtils.substring(numerocontrat, 12));
                periodeSalaires.setDateDebutPeriode(periode.getDateDebutPeriode());
                periodeSalaires.setDateFinPeriode(periode.getDateFinPeriode());
                periodeSalaires.setCategorieQuittancement(StringUtils.substring(categorieQuittancement.getNumCategorieQuittancement(), 0, 3));
                periodeSalaires
                        .setLibelleCategorieQuittancement(StringUtils.substring(categorieQuittancement.getLibelleCategorieQuittancement(), 0, 25));
                periodeSalaires.setDateDebutPeriodeTarification(periode.getDateDebutPeriode());
                periodeSalaires.setDateFinPeriodeTarification(periode.getDateFinPeriode());
                periodeSalaires.setNumTranche(tranche.getNumTranche());
                periodeSalaires.setTypeBase("2");
                periodeSalaires.setTypeDocument(1);
                periodeSalaires.setPeriodeComplementaireOuCorrective(
                        StringUtils.equals(periode.getTypePeriode(), PeriodeRecue.TYPE_PERIODE_DECLARATION) ? "N" : "O");

                remplissageChampsPaiement(periodeSalaires);

                periodeSalaires
                        .setEffectifDebutPeriode(categorieQuittancement.getEffectifDebut() == null ? 0L : categorieQuittancement.getEffectifDebut());
                Integer dateSalaireFin = periode.getDateFinPeriode();
                if (DateRedac.selectionnePartieDate(dateSalaireFin, DateRedac.MODIFIER_JOUR) == 31
                        && DateRedac.selectionnePartieDate(dateSalaireFin, DateRedac.MODIFIER_MOIS) == 12) {
                    periodeSalaires
                            .setEffectifFinDecembre(categorieQuittancement.getEffectifFin() == null ? 0L : categorieQuittancement.getEffectifFin());
                } else {
                    periodeSalaires.setEffectifFinDecembre(0L);
                }

                Double montantCotisation = categorieQuittancement.getMontantCotisation();
                periodeSalaires.setMontantCotisation(NumberUtils.degradeDoubleEnLongValeurAbsolue(montantCotisation, 2));

                periodeSalaires.setSigneMontantCotisation(montantCotisation != null && montantCotisation.doubleValue() >= 0d ? "+" : "-");

                periodeSalaires.setModeDeclaration(
                        rechercheModeDeclaration(periode.getNumeroContrat(), periode.getDateDebutPeriode(), periode.getDateFinPeriode(), 2));

                periodeSalaires.setFillerPartieCommune("");

                // Partie spécifique
                periodeSalaires.setLibelleTranche(tranche.getLibelleTranche());
                periodeSalaires.setCodeNatureBase(categorieQuittancement.getCodeNatureBaseCotisations());
                periodeSalaires.setSigneMontantSalaire(tranche.getMontantTranche() >= 0 ? "+" : "-");

                String salaireAsText = new ConvertisseurNumeriqueDecimal().convertToEntityAttribute(tranche.getMontantTranche());
                long salairePartieEntiere = Long.parseLong(StringUtils.substringBefore(salaireAsText, "."));
                periodeSalaires.setMontantSalaire(Math.abs(salairePartieEntiere));

                periodeSalaires.setTauxCotisation(0);
                periodeSalaires.setFillerPartieSpecifique("");

                listePeriodesPubliees.add(periodeSalaires);

                // On libère progressivement la mémoire
                entityManager.detach(tranche);
            }

            // Il n'y a pas d'entité persistée dans cette méthode. On se contente de vider le contexte (flush inutile).
            entityManager.clear();
            LOGGER.debug("Fin du traitement de la page {} de {} catégories de tranches !", numeroPage, taillePage);

            redigeLigneData(listePeriodesPubliees);
            listeDesTranches = null;

            // On récupère la page suivante
            numeroPage++;
            listeDesTranches = trancheCategorieRepository.getPourPeriodePagines(periode.getIdPeriode(), numeroPage * taillePage, taillePage);
        }

    }

    /**
     * Valorisation des champs referencePaiement et modePaiement de la période publiée.
     * 
     * @param periodePubliee
     *            La période publiée - ligne à écrire dans le fichier de sortie
     * @param periode
     *            la période à publier
     */
    private void remplissageChampsPaiement(PeriodePublieeWQUI periodePubliee) {
        // F78_RG_P2_06
        List<Versement> versements = rattachementDeclarationsRecuesRepository.getVersementsPositifsPourPeriode(periodePubliee.getIdPeriode());

        if (!versements.isEmpty()) {
            String modePaiement = rechercheModePaiement(versements);

            String referencePaiement = "";
            if (versements.size() > 1) {
                referencePaiement = "REF MULTIPLES";
            } else if (StringUtils.equals("TLR", modePaiement)) {
                Versement versementUnique = versements.get(0);
                ComposantVersement composantVersement = composantVersementRepository
                        .getComposantVersementPourNumCategorie(versementUnique.getIdVersement(), periodePubliee.getCategorieQuittancement());

                if (composantVersement == null) {
                    composantVersement = composantVersementRepository.getComposantVersementPourNumCategorie(versementUnique.getIdVersement(), "");
                }

                if (composantVersement != null) {
                    referencePaiement = StringUtils.stripEnd(versementUnique.getCodeIdentifiantFonds(), null) + "-"
                            + StringUtils.stripEnd(composantVersement.getCodeIdentifiantSousFonds(), null);
                    entityManager.detach(composantVersement);
                }
            } else {
                referencePaiement = versements.get(0).getReferencePaiement();
            }
            periodePubliee.setModePaiement(modePaiement);
            periodePubliee.setReferencePaiement(StringUtils.substring(referencePaiement, 0, 50));
        }
    }

    /**
     * Recherche le mode de paiement de la période à publier.
     * 
     * @param versements
     *            la liste des versements témoins
     * @return le mode de paiement de la période
     */
    private String rechercheModePaiement(List<Versement> versements) {

        Set<String> listeModePaiement = new HashSet<String>();

        for (Versement versement : versements) {
            listeModePaiement.add(versement.getModePaiement());
        }

        String modePaiement = "";
        if (listeModePaiement.contains("05")) {
            modePaiement = "TLR";
        } else if (listeModePaiement.contains("03")) {
            modePaiement = "PRL";
        } else if (listeModePaiement.contains("04")) {
            modePaiement = "TIP";
        } else if (listeModePaiement.contains("02")) {
            modePaiement = "VRT";
        } else if (listeModePaiement.contains("01")) {
            modePaiement = "CHQ";
        }

        return modePaiement;
    }

    /**
     * Détermine l'indication incorporé/radié de la période.
     * 
     * @param nombreIR
     *            le nombre indicateur IR
     * @return l'indication incorporé/radié de la période
     */
    private String rechercheIncorporeRadie(Integer nombreIR) {
        String incorporeRadie;
        if (nombreIR == null) {
            incorporeRadie = "";
        } else if (nombreIR > 0) {
            incorporeRadie = "I";
        } else {
            incorporeRadie = "R";
        }
        return incorporeRadie;
    }

    /**
     * Restitue le mode de déclaration pour la période donnée cf F78_RG_P2_07
     * 
     * @param numContrat
     *            le numéro de contrat de la période
     * @param dateFinPeriode
     *            la date de fin de la période
     * @return le code corespondant au mode de cotisation
     */
    private String rechercheModeDeclaration(String numContrat, Integer dateDebutPeriode, Integer dateFinPeriode, int natureContrat) {
        ExtensionContrat ext = extensionContratRepository.getExtensionContrat(numContrat);
        if (ext != null && ext.getDateEffetIndicateurs() <= dateDebutPeriode && StringUtils.isNotBlank(ext.getModeDeclaration())) {
            return ext.getModeDeclaration();
        } else {
            Contrat contrat = contratRepository.getContratValidePourDate(numContrat, dateFinPeriode);
            if (contrat != null) {
                return paramFamilleModeCalculContratRepository.getModeDeclaration(contrat.getNumFamilleProduit(), natureContrat);
            }
        }
        return StringUtils.EMPTY;
    }

    /**
     * Rédige directement une ligne de donnée dans le fichier publié.
     * 
     * @param listePeriodesPubliees
     *            la liste des périodes publiées
     */
    public void redigeLigneData(List<PeriodePublieeWQUI> listePeriodesPubliees) {
        try {
            // on rédige les périodes listées.
            publicationRedacteurFichierPlatData.write(listePeriodesPubliees);
        } catch (Exception e) {
            String messageErreur = "Erreur pendant la rédaction d'une ligne de données du fichier WQUI";
            LOGGER.error(messageErreur, e);
            throw new RedacUnexpectedException(messageErreur);
        }

        nbDeLignes += listePeriodesPubliees.size();

        Iterator<PeriodePublieeWQUI> itPeriodes = listePeriodesPubliees.iterator();
        while (itPeriodes.hasNext()) {
            PeriodePublieeWQUI periodeTraitee = itPeriodes.next();
            enregistreCoupleContratPeriode(periodeTraitee);
        }
    }

    /**
     * Rédige directement la ligne d'en-tête dans le fichier publié.
     * 
     */
    public void redigeLigneEnTete() {
        int nbCouples = couples.isEmpty() ? 0 : couples.size();

        String nbDeLignesStr = String.valueOf(nbDeLignes);
        while (nbDeLignesStr.length() < 8) {
            nbDeLignesStr = "0" + nbDeLignesStr;
        }

        String nbCouplesStr = String.valueOf(nbCouples);
        while (nbCouplesStr.length() < 8) {
            nbCouplesStr = "0" + nbCouplesStr;
        }

        String filler = "";
        while (filler.length() < 222) {
            filler += " ";
        }

        // longueurs : 1|3|8|8|8|222
        String header = "1" + "DSN" + nbDeLignesStr + nbCouplesStr + dateDuJour + filler;

        try {
            // Rédige la ligne d'en-tête dans le fichier définitif.
            publicationRedacteurFichierPlatHeader.write(Arrays.asList(header));
        } catch (Exception e) {
            String messageErreur = "Erreur pendant la rédaction de la ligne d'en-tête du fichier WQUI";
            LOGGER.error(messageErreur, e);
            throw new RedacUnexpectedException(messageErreur);
        }
    }

    /**
     * Enregistre le couple contrat/periode s'il n'est pas déjà enregistré.
     * 
     * @param item
     *            la période publiée pour WQUI
     */
    public void enregistreCoupleContratPeriode(PeriodePublieeWQUI item) {
        String periode = item.getIdPeriode().toString();
        String contrat = item.getContratZoneA() + item.getContratZoneB() + item.getContratZoneC();

        couples.add(contrat + "/" + periode);
    }

    /**
     * Initialise le writer.
     * 
     * @param stepExecution
     *            l'exécution de l'étape
     */
    public void initialiseWriter(StepExecution stepExecution) {
        publicationRedacteurFichierPlatData.setResource(fichierSortieTemporaire);
        publicationRedacteurFichierPlatData.open(stepExecution.getExecutionContext());

        publicationRedacteurFichierPlatHeader.setResource(fichierSortie);
        publicationRedacteurFichierPlatHeader.open(stepExecution.getExecutionContext());
    }

    @BeforeStep
    protected void saveStepExecution(StepExecution stepExecution) {
        this.stepExecution = stepExecution;
        initialiseWriter(stepExecution);
    }

    /**
     * Termine le traitement du writer : rédaction de la ligne d'en-tête puis fermeture du writer.
     * 
     */
    @AfterStep
    public void termineWriter() {
        redigeLigneEnTete();

        // Termine l'écriture dans le fichier temporaire.
        publicationRedacteurFichierPlatData.close();

        // Termine l'écriture de la ligne d'en-tête dans le fichier définitif.
        publicationRedacteurFichierPlatHeader.close();
    }
}
