package fr.si2m.red.batch.flux4.execution;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParameter;
import org.springframework.batch.core.JobParameters;
import org.springframework.context.support.GenericXmlApplicationContext;

import fr.si2m.red.batch.flux4.support.ParametrageControlesFonctionnels;
import fr.si2m.red.batch.moteur.CodeRetour;
import fr.si2m.red.batch.moteur.execution.FacadeExecuteurBatch;
import fr.si2m.red.parametrage.ParamControleSignalRejet;
import fr.si2m.red.parametrage.ParamControleSignalRejetRepository;
import fr.si2m.red.parametrage.ParamNatureBaseComposants;
import fr.si2m.red.parametrage.ParamNatureBaseComposantsRepository;
import fr.si2m.red.parametrage.ParamValeurDefautRepository;

/**
 * Exécuteur de la phase 6 - contrôles fonctionnels sur les périodes - du flux 4.
 * 
 * @author nortaina
 *
 */
@FacadeExecuteurBatch(nomBatch = "flux4_ControlesFonctionnelsPeriodes")
public class Flux4ControlesFonctionnelsPeriodesExecuteurBatch extends Flux4ExecuteurBatch {
    private static final Logger LOGGER = LoggerFactory.getLogger(Flux4ControlesFonctionnelsPeriodesExecuteurBatch.class);

    /**
     * Ordre du paramètre de lancement "emplacement du fichier de log d'erreurs".
     */
    public static final int ORDRE_PARAM_EMPLACEMENT_FICHIER_ERREURS = 0;

    /**
     * Exécuteur de batch du flux 4 pour les contrôles fonctionnels sur les périodes.
     * 
     * @param parametrageBatch
     *            les paramètres d'exécution du batch - un paramètre est attendu en entrée :
     *            <ol>
     *            <li>le chemin absolu vers le fichier de logs à éventuellement renseigner</li>
     *            </ol>
     */
    public Flux4ControlesFonctionnelsPeriodesExecuteurBatch(String[] parametrageBatch) {
        super(parametrageBatch);
        String cheminAbsoluFichierLogErreurs = parametrageBatch[ORDRE_PARAM_EMPLACEMENT_FICHIER_ERREURS];
        setCheminAbsoluFichierLogErreurs(cheminAbsoluFichierLogErreurs);
    }

    @Override
    protected String getIdJobFlux4() {
        return "ControlesFonctionnelsPeriodes";
    }

    @Override
    protected boolean lanceTraitementAvantJob(GenericXmlApplicationContext contexteExecutionJob) {
        // Récupération du paramétrage global pour les contrôles fonctionnels
        ParametrageControlesFonctionnels parametrageControles = contexteExecutionJob.getBean(ParametrageControlesFonctionnels.class);

        // Mise à jour du paramétrage avec les contrôles globaux pour le job
        ParamControleSignalRejetRepository referentielControles = contexteExecutionJob.getBean(ParamControleSignalRejetRepository.class);
        Map<String, ParamControleSignalRejet> paramControlesGlobal = referentielControles.getControlesGlobaux();
        parametrageControles.setControlesGlobaux(paramControlesGlobal);

        if (paramControlesGlobal.isEmpty()) {
            LOGGER.warn("Aucun contrôle global sur les périodes reçues");
        }

        // Mise à jour du paramétrage avec les paramètres à utiliser par défaut pour certaines règles de gestion
        ParamValeurDefautRepository referentielValeursParDefaut = contexteExecutionJob.getBean(ParamValeurDefautRepository.class);
        parametrageControles.setParametrageParDefaut(referentielValeursParDefaut.getParamValeurDefaut());

        // Mise à jour du paramétrage avec les paramètres de natures de bases de composants pour certaines règles de gestion
        ParamNatureBaseComposantsRepository paramNatureBaseComposantsRepository = contexteExecutionJob
                .getBean(ParamNatureBaseComposantsRepository.class);
        // Il n'y en a qu'une cinquantaine, donc ça vaut vraiment le coup de tous les garder en mémoire pour les contrôles
        List<ParamNatureBaseComposants> naturesBasesComposants = paramNatureBaseComposantsRepository.liste();
        Map<Integer, List<ParamNatureBaseComposants>> parametrageNatureBaseComposantsParCodeNatureBaseCotisations = new HashMap<>();
        for (ParamNatureBaseComposants natureBaseComposants : naturesBasesComposants) {
            List<ParamNatureBaseComposants> parametragesPourNatureBaseCotisations = parametrageNatureBaseComposantsParCodeNatureBaseCotisations
                    .get(natureBaseComposants.getCodeNatureBaseCotisations());
            if (parametragesPourNatureBaseCotisations == null) {
                parametragesPourNatureBaseCotisations = new ArrayList<>();
                parametrageNatureBaseComposantsParCodeNatureBaseCotisations.put(natureBaseComposants.getCodeNatureBaseCotisations(),
                        parametragesPourNatureBaseCotisations);
            }
            parametragesPourNatureBaseCotisations.add(natureBaseComposants);
        }
        parametrageControles
                .setParametrageNatureBaseComposantsParCodeNatureBaseCotisations(parametrageNatureBaseComposantsParCodeNatureBaseCotisations);

        return true;
    }

    @Override
    protected JobParameters initParametrageJob(GenericXmlApplicationContext contexteExecutionJob) {
        Map<String, JobParameter> parametresJob = new HashMap<String, JobParameter>();
        // Juste pour l'audit dans les tables techniques de batch
        ParametrageControlesFonctionnels parametrageControles = contexteExecutionJob.getBean(ParametrageControlesFonctionnels.class);
        String listeControlesActifs = StringUtils.join(parametrageControles.getControlesGlobaux().keySet(), ",");
        JobParameter controlesActifs = new JobParameter(listeControlesActifs);
        parametresJob.put("controlesActifs", controlesActifs);

        // Configuration de l'emplacement du fichier de log
        LOGGER.debug("Fichier erreurs pris en compte : {}", getCheminAbsoluFichierLogErreurs());
        parametresJob.put("fichierLogErreurs", new JobParameter(getCheminAbsoluFichierLogErreurs()));

        return new JobParameters(parametresJob);
    }

    @Override
    protected void lanceTraitementFinJob(GenericXmlApplicationContext contexteExecutionJob, CodeRetour codeRetour, JobExecution executionJob) {
        // Logge le message de réussite du traitement batch dans le fichier de log
        if (codeRetour == CodeRetour.TERMINE) {
            traceTraitementOKDansFichierLog("Traitement du flux 4 - Contrôles fonctionnels sur les périodes - réussi");
        } else if (codeRetour == CodeRetour.ERREUR_FONCTIONNELLE_NON_BLOQUANTE) {
            traceTraitementOKDansFichierLog(
                    "Traitement du flux 4 - Contrôles fonctionnels sur les périodes - réussi, avec erreur(s) non bloquante(s)");
        }

        LOGGER.info("Fin d'exécution du batch flux 4 - Contrôles fonctionnels sur les périodes");
    }

}
