package fr.si2m.red.batch.flux3.item;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import fr.si2m.red.contrat.SituationTarif;
import fr.si2m.red.parametrage.ResumeGestionnaire;

/**
 * Un mapper de ligne {@link SituationTarif} pour supporter la récupération de données via JDBC.
 * 
 * @author eudesr
 *
 */
public class GestionnaireRowMapper implements RowMapper<ResumeGestionnaire> {

    @Override
    public ResumeGestionnaire mapRow(ResultSet rs, int rowNum) throws SQLException {

        ResumeGestionnaire pg = new ResumeGestionnaire();
        pg.setCodeMiseAJour("R");
        pg.setIdentifiantTechniqueEntreprise(rs.getString("NOSIREN"));
        pg.setTypeMaille(1);
        pg.setReferenceMaille(rs.getString("NOSIREN"));
        pg.setTypeIntervenant("1");
        pg.setReferenceGestionnaire(rs.getString("code_user"));
        pg.setLibelle(rs.getString("code_user"));
        pg.setPrenomGestionnaireCompte(rs.getString("PRENOM"));
        pg.setNomGestionnaireCompte(rs.getString("NOM"));
        pg.setUniteGestion(rs.getString("NIVEAU_1"));
        pg.setService(rs.getString("NIVEAU_2"));
        pg.setCentreGestion(rs.getString("NIVEAU_3"));
        pg.setIdentifiantTechniqueContrat("");
        return pg;
    }
}
