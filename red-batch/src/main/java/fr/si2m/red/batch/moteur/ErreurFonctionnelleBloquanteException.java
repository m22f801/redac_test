package fr.si2m.red.batch.moteur;

/**
 * Exception sur erreur fonctionnelle bloquante lors de l'exécution d'un batch.
 * 
 * @author nortaina
 *
 */
public class ErreurFonctionnelleBloquanteException extends RuntimeException {
    /**
     * UID de version.
     */
    private static final long serialVersionUID = -6518546271382197469L;

    /**
     * Constructeur.
     * 
     * @param message
     *            le message d'erreur
     * 
     */
    public ErreurFonctionnelleBloquanteException(String message) {
        super(message);
    }

    /**
     * Constructeur.
     * 
     * @param message
     *            le message d'erreur
     * @param cause
     *            la cause de l'exception
     */
    public ErreurFonctionnelleBloquanteException(String message, Throwable cause) {
        super(message, cause);
    }

}
