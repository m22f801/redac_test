package fr.si2m.red.batch.flux4.item;

import org.apache.commons.lang3.StringUtils;

import fr.si2m.red.DateRedac;
import fr.si2m.red.batch.RedacMessages;
import fr.si2m.red.batch.moteur.erreur.ValidateurLigneAvecCollecte;
import fr.si2m.red.dsn.ContratTravail;
import fr.si2m.red.dsn.ContratTravailRepository;
import fr.si2m.red.dsn.IndividuRepository;
import lombok.Setter;

/**
 * Validateur de ligne de paramétrage code libellé.
 * 
 * @author benitahy
 *
 */
public class ContratTravailValidateurLigne extends ValidateurLigneAvecCollecte<ContratTravail> {

    @Setter
    private IndividuRepository individuRepository;

    @Setter
    private ContratTravailRepository contratTravailRepository;

    @Override
    protected void valide(final ContratTravail contratTravail) {

        // unicité
        valideChampAvecCollecte(!contratTravailRepository.existeUnContratTravail(contratTravail.getIdContratTravail()), contratTravail, "ID",
                "La ligne ne respecte pas la règle d'unicité");

        // Champs obligatoires
        valideChampAvecCollecte(StringUtils.isNotBlank(contratTravail.getIdContratTravail()), contratTravail, "ID_CONTRAT_TRAVAIL",
                "Le champ ID_CONTRAT_TRAVAIL obligatoire n'est pas renseigné");
        valideChampAvecCollecte(StringUtils.isNotBlank(contratTravail.getIdIndividu()), contratTravail, "ID_INDIVIDU",
                "Le champ ID_INDIVIDU obligatoire n'est pas renseigné");

        // Longueur des champs
        valideTailleFixeChampAvecCollecte(contratTravail.getIdContratTravail(), 30, contratTravail, "ID_CONTRAT_TRAVAIL");
        valideTailleFixeChampAvecCollecte(contratTravail.getIdIndividu(), 30, contratTravail, "ID_INDIVIDU");
        valideChampDateTailleFixeAvecCollecte(contratTravail.getDateDebutContratAsText(), DateRedac.FORMAT_DATES_DDMMYYYY, contratTravail,
                "DATE_DEBUT_CONTRAT", false);
        valideTailleFixeChampAvecCollecte(contratTravail.getStatutConventionnel(), 2, contratTravail, "STATUT_CONVENTIONNEL");
        valideTailleFixeChampAvecCollecte(contratTravail.getCodeStatutRC(), 2, contratTravail, "CODE_STATUT_RC");
        valideTailleFixeChampAvecCollecte(contratTravail.getCodeProfession(), 4, contratTravail, "CODE_PROFESSION");
        valideTailleFixeChampAvecCollecte(contratTravail.getComplementProfession(), 6, contratTravail, "COMPLEMENT_PROFESSION");
        valideTailleFixeChampAvecCollecte(contratTravail.getLibelleEmploi(), 120, contratTravail, "LIBELLE_EMPLOI");
        valideTailleFixeChampAvecCollecte(contratTravail.getNatureContrat(), 2, contratTravail, "NATURE_CONTRAT");
        valideTailleFixeChampAvecCollecte(contratTravail.getDispositifPolitique(), 2, contratTravail, "DISPOSITIF_POLITIQUE");
        valideTailleFixeChampAvecCollecte(contratTravail.getNumeroContratTravail(), 20, contratTravail, "NUMERO_CONTRAT");
        valideChampDateTailleFixeAvecCollecte(contratTravail.getDateFinPrevisionnelleAsText(), DateRedac.FORMAT_DATES_DDMMYYYY, contratTravail,
                "DATE_FIN_PREVISIONNELLE", false);
        valideTailleFixeChampAvecCollecte(contratTravail.getUniteMesure(), 2, contratTravail, "UNITE_MESURE");
        valideChampNumeriqueDecimaleTailleFixeAvecCollecte(contratTravail.getQuotiteCategorieAsText(), 5, 2, contratTravail, "QUOTITE_CATEGORIE");
        valideChampNumeriqueDecimaleTailleFixeAvecCollecte(contratTravail.getQuotiteAsText(), 5, 2, contratTravail, "QUOTITE");
        valideTailleFixeChampAvecCollecte(contratTravail.getModaliteTemps(), 2, contratTravail, "MODALITE_TEMPS");
        valideTailleFixeChampAvecCollecte(contratTravail.getRegimeLocal(), 2, contratTravail, "REGIME_LOCAL");
        valideTailleFixeChampAvecCollecte(contratTravail.getCodeConventionCollective(), 4, contratTravail, "CODE_CONVENTION_COLLECTIVE");
        valideTailleFixeChampAvecCollecte(contratTravail.getRegimeMaladie(), 3, contratTravail, "REGIME_MALADIE");
        valideTailleFixeChampAvecCollecte(contratTravail.getLieutravail(), 14, contratTravail, "LIEU_TRAVAIL");
        valideTailleFixeChampAvecCollecte(contratTravail.getRegimeVieillesse(), 3, contratTravail, "REGIME_VIEILLESSE");
        valideTailleFixeChampAvecCollecte(contratTravail.getMotifRecours(), 2, contratTravail, "MOTIF_RECOURS");
        valideChampNumeriqueDecimaleTailleFixeAvecCollecte(contratTravail.getTauxFraisProfessionnelsAsText(), 4, 2, contratTravail,
                "TAUX_FRAIS_PROFESSIONNELS");
        valideTailleFixeChampAvecCollecte(contratTravail.getDetacheExpatrie(), 2, contratTravail, "DETACHE_EXPATRIE");
        valideTailleFixeChampAvecCollecte(contratTravail.getMotifExclusionDSN(), 2, contratTravail, "MOTIF_EXCLUSION_DSN");
        valideTailleFixeChampAvecCollecte(contratTravail.getStatutEmploi(), 2, contratTravail, "STATUT_EMPLOI");
        valideTailleFixeChampAvecCollecte(contratTravail.getCodeGestionnaireRisque(), 3, contratTravail, "CODE_GESTIONNAIRE_RISQUE");
        valideTailleFixeChampAvecCollecte(contratTravail.getCodeEmploisMultiples(), 2, contratTravail, "CODE_EMPLOIS_MULTIPLES");
        valideTailleFixeChampAvecCollecte(contratTravail.getCodeEmployeursMultiples(), 2, contratTravail, "CODE_EMPLOYEURS_MULTIPLES");
        valideTailleFixeChampAvecCollecte(contratTravail.getPositionnementConventionCollective(), 5, contratTravail,
                "POSITION_CONVENTION_COLLECTIVE");
        valideTailleFixeChampAvecCollecte(contratTravail.getCodeStatutCategorieI(), 2, contratTravail, "CODE_STATUT_CATEGORIEL");
        valideChampNumeriqueDecimaleTailleFixeAvecCollecte(contratTravail.getTauxAccidentTravailAsText(), 4, 2, contratTravail,
                "TAUX_ACCIDENT_TRAVAIL");
        valideTailleFixeChampAvecCollecte(contratTravail.getSalarieTempsPartielCotisantTempsPlein(), 2, contratTravail, "TPS_PARTIEL_COTIS_PLEIN");
        valideTailleFixeChampAvecCollecte(contratTravail.getRemunerationPourboire(), 2, contratTravail, "REMUNERATION_POURBOIRE");
        valideTailleFixeChampAvecCollecte(contratTravail.getSiretEtablissementUtilisateur(), 14, contratTravail, "SIRET_ETABLISSEMENT");
        valideTailleFixeChampAvecCollecte(contratTravail.getEnseigneEtablissement(), 80, contratTravail, "ENSEIGNE_ETABLISSEMENT");
        valideChampDateTailleFixeAvecCollecte(contratTravail.getDateFinContratAsText(), DateRedac.FORMAT_DATES_DDMMYYYY, contratTravail,
                "DATE_FIN_CONTRAT", false);
        valideTailleFixeChampAvecCollecte(contratTravail.getMotifRupture(), 3, contratTravail, "MOTIF_RUPTURE");

        validationCorrespondanceReferentielle(contratTravail);
    }

    /**
     * Validation des liens entre tables.
     * 
     * @param contratTravail
     *            Le contrat de travail dont l'individu doit être validé.
     */
    private void validationCorrespondanceReferentielle(final ContratTravail contratTravail) {
        if (StringUtils.isNotBlank(contratTravail.getIdIndividu())) {
            boolean existeUnIndividu = individuRepository.existeUnIndividu(contratTravail.getIdIndividu());
            valideChampAvecCollecte(existeUnIndividu, contratTravail, "idIndividu", RedacMessages.ERREUR_REFERENCE_FLUX4);
        }
    }
}
