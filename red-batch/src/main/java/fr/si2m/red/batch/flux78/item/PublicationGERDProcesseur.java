package fr.si2m.red.batch.flux78.item;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.annotation.AfterStep;
import org.springframework.batch.core.annotation.BeforeStep;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;

import fr.si2m.red.RedacUnexpectedException;
import fr.si2m.red.batch.moteur.item.TransformateurDonneeCodeRetourModificateur;
import fr.si2m.red.contrat.ContratRepository;
import fr.si2m.red.contrat.ResumeContrat;
import fr.si2m.red.dsn.ArretTravail;
import fr.si2m.red.dsn.ArretTravailRepository;
import fr.si2m.red.dsn.ComposantVersement;
import fr.si2m.red.dsn.ComposantVersementRepository;
import fr.si2m.red.dsn.ContratTravail;
import fr.si2m.red.dsn.ContratTravailRepository;
import fr.si2m.red.dsn.Individu;
import fr.si2m.red.dsn.IndividuRepository;
import fr.si2m.red.dsn.Versement;
import fr.si2m.red.reconciliation.CategorieQuittancementIndividu;
import fr.si2m.red.reconciliation.CategorieQuittancementIndividuRepository;
import fr.si2m.red.reconciliation.PeriodeRecue;
import fr.si2m.red.reconciliation.RattachementDeclarationsRecuesRepository;
import fr.si2m.red.reconciliation.TrancheCategorieRepository;
import lombok.Setter;

/**
 * Création de lignes de périodes publiées pour GERD.
 * 
 * @author poidij
 *
 */
public class PublicationGERDProcesseur extends TransformateurDonneeCodeRetourModificateur<PeriodeRecue, List<PeriodePublieeGERD>> {
    private static final Logger LOGGER = LoggerFactory.getLogger(PublicationGERDProcesseur.class);

    /**
     * Le code sexe masuculin.
     */
    public static final String CODE_SEXE_M = "M";

    /**
     * Le code sexe féminin.
     */
    public static final String CODE_SEXE_F = "F";

    /**
     * Le code pour les montants de nature 940.
     */
    public static final String NATURE_MONTANT_940 = "940";

    /**
     * Le code pour les montants de nature 950.
     */
    public static final String NATURE_MONTANT_950 = "950";

    @Autowired
    private CategorieQuittancementIndividuRepository categorieQuittancementIndividuRepository;
    @Autowired
    private IndividuRepository individuRepository;
    @Autowired
    private ContratTravailRepository contratTravailRepository;
    @Autowired
    private ContratRepository contratRepository;
    @Autowired
    private ArretTravailRepository arretTravailRepository;
    @Autowired
    private TrancheCategorieRepository trancheCategorieRepository;
    @Autowired
    private RattachementDeclarationsRecuesRepository rattachementDeclarationsRecuesRepository;
    @Autowired
    private ComposantVersementRepository composantVersementRepository;

    @PersistenceContext
    private EntityManager entityManager;

    private PublicationRedacteurFichierPlat<PeriodePublieeGERD> publicationRedacteurFichierPlat = new PublicationRedacteurFichierPlat<PeriodePublieeGERD>();

    @Setter
    private Resource fichierSortie;
    @Setter
    private String dateDuJour;

    @Setter
    private String cheminAbsoluFichierLogErreurs;

    @Override
    public List<PeriodePublieeGERD> process(PeriodeRecue periode) {

        // F78_RG_P3_03

        // Avec des hypothèses pessimistes, on peut potentiellement avoir de l'ordre de 500 000 CategorieQuittancementIndividu à parcourir par entité
        // PeriodeRecue => On pagine les traitements par 4000 catégories
        final int taillePage = 4000;
        int numeroPage = 0;

        List<CategorieQuittancementIndividu> listeDesCategorieQuittancement = categorieQuittancementIndividuRepository
                .getPourPeriodePagines(periode.getIdPeriode(), numeroPage * taillePage, taillePage);
        while (!listeDesCategorieQuittancement.isEmpty()) {
            LOGGER.debug("Traitement de la page {} de {} quittancements en cours...", numeroPage, taillePage);
            // On publie par page de "taillePage"
            List<PeriodePublieeGERD> listePeriodesPubliees = new ArrayList<>(listeDesCategorieQuittancement.size());

            for (CategorieQuittancementIndividu categorieQuittancement : listeDesCategorieQuittancement) {
                LOGGER.debug("Traitement du quittancement {} en cours...", categorieQuittancement.getIdTechnique());
                PeriodePublieeGERD periodePubliee = new PeriodePublieeGERD();

                periodePubliee.setIdPeriode(periode.getIdPeriode());

                Individu individuRecent = individuRepository.getPourPeriode(periode.getIdPeriode(), categorieQuittancement.getIndividu());
                if (individuRecent == null) {
                    String mesageErreur = "L'individu NIR/NTT=" + categorieQuittancement.getIndividu()
                            + " n'existe pas pour la période : [PeriodeRecue(idPeriode=" + periode.getIdPeriode() + ", numeroContrat="
                            + periode.getNumeroContrat() + ", dateDebutPeriode=" + periode.getDateDebutPeriode() + ", dateFinPeriode="
                            + periode.getDateFinPeriode() + ", typePeriode=" + periode.getTypePeriode() + ", dateCreation="
                            + periode.getDateCreation() + ")]";
                    traceErreurSansBlocage(mesageErreur, cheminAbsoluFichierLogErreurs);
                } else {
                    entityManager.detach(individuRecent);

                    // Informations Entreprise/contrat/période
                    periodePubliee.setDateEnvoi(dateDuJour);
                    periodePubliee.setIdEntreprise(individuRecent.getAdhesionEtablissementMois().getSirenEntreprise());
                    periodePubliee.setNomEntreprise(StringUtils.substring(individuRecent.getAdhesionEtablissementMois().getRaisonSociale(), 0, 35));
                    periodePubliee.setNumContrat(periode.getNumeroContrat());
                    periodePubliee.setDateDebutPeriode(periode.getDateDebutPeriode());
                    periodePubliee.setDateFinPeriode(periode.getDateFinPeriode());

                    String codePopulation = StringUtils.substring(categorieQuittancement.getNumCategorieQuittancement(), 0, 3);
                    periodePubliee.setCodePopulation(codePopulation);

                    periodePubliee.setTypeFlux(StringUtils.equals(periode.getTypePeriode(), PeriodeRecue.TYPE_PERIODE_DECLARATION) ? "09 " : "18 ");

                    String refMessage = periode.getNumeroContrat() + String.valueOf(periode.getDateDebutPeriode())
                            + String.valueOf(periode.getDateFinPeriode()) + codePopulation;
                    periodePubliee.setRefMessage(refMessage);
                    periodePubliee.setCodeDevise("EUR");

                    // Informations adhérent
                    periodePubliee.setNir(individuRecent.getIdentifiantRepertoire());
                    periodePubliee.setNtt(individuRecent.getNumeroTechniqueTemporaire());
                    periodePubliee.setNumeroAdherent(individuRecent.getMatricule());
                    periodePubliee.setCodeSexe(remplissageChampCodeSexe(individuRecent));
                    periodePubliee.setCodeCivilite(StringUtils.equals(periodePubliee.getCodeSexe(), CODE_SEXE_M) ? "M   " : "MME ");
                    periodePubliee.setNomFamilleAdherent(individuRecent.getNomFamille());
                    periodePubliee.setNomUsageAdherent((individuRecent.getNomUsage() == null) || (individuRecent.getNomUsage().isEmpty())
                            ? individuRecent.getNomFamille() : individuRecent.getNomUsage());
                    periodePubliee.setPrenomAdherent(individuRecent.getPrenom());

                    // Adresse

                    periodePubliee.setComplConstruction(individuRecent.getComplementConstruction());
                    periodePubliee.setLibelleAdresse(individuRecent.getVoie());
                    periodePubliee.setComplVoie(individuRecent.getComplementVoie());
                    periodePubliee.setCodeDistribution(individuRecent.getCodeDistribution());
                    periodePubliee.setCodePostal(individuRecent.getCodePostal());
                    periodePubliee.setVille(individuRecent.getLocalite());
                    periodePubliee.setCodePays(individuRecent.getCodePays());

                    // Caractéristiques adhérent

                    periodePubliee.setMel(individuRecent.getAdresseMail());
                    periodePubliee.setLieuNaissance(individuRecent.getLieuNaissance());
                    periodePubliee
                            .setDateNaissance(individuRecent.getDateNaissance() == null ? Integer.valueOf(0) : individuRecent.getDateNaissance());
                    periodePubliee.setDepartementNaissance(individuRecent.getDepartementNaissance());
                    periodePubliee.setPaysNaissance(individuRecent.getPaysNaissance());

                    // Informations changement adhérent

                    remplissageChampsChangementAdherent(periodePubliee, individuRecent);

                    // Informations contrat-arrêt de travail

                    remplissageChampsTravail(periodePubliee, individuRecent.getIdIndividu());

                    // Informations cotisations
                    remplissageChampsCotisation(periodePubliee, categorieQuittancement);

                    // Informations paiement
                    remplissageChampsPaiement(periodePubliee, categorieQuittancement.getNumCategorieQuittancement());

                    listePeriodesPubliees.add(periodePubliee);

                }
                // On libère progressivement la mémoire
                entityManager.detach(categorieQuittancement);
                LOGGER.debug("Fin du traitement du quittancement {} !", categorieQuittancement.getIdTechnique());

            }

            // Il n'y a pas d'entité persistée dans cette méthode. On se contente de vider le contexte (flush inutile).
            entityManager.clear();
            LOGGER.debug("Fin du traitement de la page {} des quittancements !", numeroPage);

            // on rédige les périodes listées puis on vide la liste.
            try {
                publicationRedacteurFichierPlat.write(listePeriodesPubliees);
            } catch (Exception e) {
                String messageErreur = "Erreur pendant la rédaction de lignes de données du fichier GERD";
                LOGGER.error(messageErreur, e);
                throw new RedacUnexpectedException(messageErreur);
            }

            // Déréférencement pour éventuel besoin lancement GC
            listePeriodesPubliees = null;

            // On récupère la page suivante
            numeroPage++;
            listeDesCategorieQuittancement = categorieQuittancementIndividuRepository.getPourPeriodePagines(periode.getIdPeriode(),
                    numeroPage * taillePage, taillePage);
        }

        // Pour terminer proprement, on renvoit une liste vide plutôt que null
        return new ArrayList<PeriodePublieeGERD>();

    }

    /**
     * Détermine le code sexe d'un individu donné.
     * 
     * @param individuRecent
     *            l'individu
     * @return le code sexe déterminé
     */
    private String remplissageChampCodeSexe(Individu individuRecent) {
        String codeSexe = " ";
        if (StringUtils.equals(individuRecent.getSexe(), "01")) {
            codeSexe = CODE_SEXE_M;
        } else if (StringUtils.equals(individuRecent.getSexe(), "02")) {
            codeSexe = CODE_SEXE_F;
        } else if (StringUtils.isNotBlank(individuRecent.getIdentifiantRepertoire())) {
            if (individuRecent.getIdentifiantRepertoire().startsWith("1")) {
                codeSexe = CODE_SEXE_M;
            } else {
                codeSexe = CODE_SEXE_F;
            }
        }

        return codeSexe;
    }

    /**
     * Valorisation des champs de changement d'adherent pour la période publiée et pour l'individu donné.
     * 
     * @param periodePubliee
     *            la periode a etre publiee
     * @param individuRecent
     *            l'individu
     */

    private void remplissageChampsChangementAdherent(PeriodePublieeGERD periodePubliee, Individu individuRecent) {

        // Avoir deux differentes types de colonnes: soit numerique (Integer) soit alpha-numerique (String) => le besoin d'avoir deux methodes:
        // getColonneChangementsIndividuRattacheInteger() et getColonneChangementsIndividuRattacheString() pour ne pas avoir des problemes de data conversion
        Integer dateModification = rattachementDeclarationsRecuesRepository.getColonneChangementsIndividuRattacheInteger(
                periodePubliee.getIdPeriode(), "dateModification", individuRecent.getIdentifiantRepertoire(),
                individuRecent.getNumeroTechniqueTemporaire());
        periodePubliee.setDateModification(dateModification == null ? Integer.valueOf(0) : dateModification);

        String ancienIdentifiant = rattachementDeclarationsRecuesRepository.getColonneChangementsIndividuRattacheString(periodePubliee.getIdPeriode(),
                "ancienIdentifiant", individuRecent.getIdentifiantRepertoire(), individuRecent.getNumeroTechniqueTemporaire());
        periodePubliee.setAncienIdentifiant(ancienIdentifiant == null ? "" : ancienIdentifiant);

        String ancienNomFamille = rattachementDeclarationsRecuesRepository.getColonneChangementsIndividuRattacheString(periodePubliee.getIdPeriode(),
                "ancienNomFamille", individuRecent.getIdentifiantRepertoire(), individuRecent.getNumeroTechniqueTemporaire());
        periodePubliee.setAncienNomFamille(ancienNomFamille == null ? "" : ancienNomFamille);

        String ancienPrenom = rattachementDeclarationsRecuesRepository.getColonneChangementsIndividuRattacheString(periodePubliee.getIdPeriode(),
                "ancienPrenom", individuRecent.getIdentifiantRepertoire(), individuRecent.getNumeroTechniqueTemporaire());
        periodePubliee.setAncienPrenoms(ancienPrenom == null ? "" : ancienPrenom);

        Integer ancienneDateNaissance = rattachementDeclarationsRecuesRepository.getColonneChangementsIndividuRattacheInteger(
                periodePubliee.getIdPeriode(), "ancienneDateNaissance", individuRecent.getIdentifiantRepertoire(),
                individuRecent.getNumeroTechniqueTemporaire());
        periodePubliee.setAncienneDateNaissance(ancienneDateNaissance == null ? Integer.valueOf(0) : ancienneDateNaissance);

    }

    /**
     * Valorisation des champs de contrat de travail de la période publiée.
     * 
     * @param periodePubliee
     *            La période publiée - ligne à écrire dans le fichier de sortie
     * @param idIndividu
     *            l'identifiant de l'individu
     */
    private void remplissageChampsTravail(PeriodePublieeGERD periodePubliee, String idIndividu) {
        ContratTravail contratTravail = contratTravailRepository.getDernierContratTravailPourIndividu(idIndividu);
        entityManager.detach(contratTravail);

        // F78_RG_P3_12
        ResumeContrat contrat = contratRepository.getContratPourPeriodePublieeGERD(periodePubliee.getIdPeriode());
        Integer dateEffetContrat = contrat.getDteffco();
        if (dateEffetContrat == null) {
            periodePubliee.setDateAdhesion(periodePubliee.getDateDebutPeriode());
        } else {
            periodePubliee.setDateAdhesion(
                    dateEffetContrat >= periodePubliee.getDateDebutPeriode() ? dateEffetContrat : periodePubliee.getDateDebutPeriode());
        }

        periodePubliee.setDateRadiation(contratTravail.getDateFinContrat() == null ? Integer.valueOf(0) : contratTravail.getDateFinContrat());

        // F78_RG_P3_05
        ArretTravail arret = arretTravailRepository.getDernierArretTravailPourContratTravail(contratTravail.getIdContratTravail());
        if (arret != null && arret.getDateDernierJour() > periodePubliee.getDateDebutPeriode()) {
            periodePubliee.setDateDernierJourTravail(arret.getDateDernierJour());
            periodePubliee.setDateRepriseTravail(arret.getDateReprise() != null ? arret.getDateReprise() : 0);
        } else {
            periodePubliee.setDateDernierJourTravail(0);
            periodePubliee.setDateRepriseTravail(0);
        }
        if (arret != null) {
            entityManager.detach(arret);
        }

        periodePubliee.setPresenceDansEntreprise(periodePubliee.getDateRadiation() != null && periodePubliee.getDateRadiation() > 0 ? "S" : "P");
    }

    /**
     * Valorisation des champs de cotisation de la période publiée.
     * 
     * @param periodePubliee
     *            La période publiée - ligne à écrire dans le fichier de sortie
     * @param categorieQuittancement
     *            la catégorie de la période
     */
    private void remplissageChampsCotisation(PeriodePublieeGERD periodePubliee, CategorieQuittancementIndividu categorieQuittancement) {
        periodePubliee.setDateDebutCotisation(periodePubliee.getDateDebutPeriode());
        periodePubliee.setDateFinCotisation(periodePubliee.getDateFinPeriode());

        Double montantSalaireTrancheA = trancheCategorieRepository.getMontantSalaireTranche(periodePubliee.getIdPeriode(),
                categorieQuittancement.getNumCategorieQuittancement(), categorieQuittancement.getIndividu(), 1);

        periodePubliee.setMontantSalaireTrancheA(NumberUtils.degradeDoubleEnLongValeurAbsolue(montantSalaireTrancheA, 2));
        if (montantSalaireTrancheA != null && montantSalaireTrancheA.doubleValue() > 0d) {
            periodePubliee.setNatureMontantSalaireTrancheA(NATURE_MONTANT_940);
        } else {
            periodePubliee.setNatureMontantSalaireTrancheA(NATURE_MONTANT_950);
        }

        Double montantSalaireTrancheB = trancheCategorieRepository.getMontantSalaireTranche(periodePubliee.getIdPeriode(),
                categorieQuittancement.getNumCategorieQuittancement(), categorieQuittancement.getIndividu(), 2);

        periodePubliee.setMontantSalaireTrancheB(NumberUtils.degradeDoubleEnLongValeurAbsolue(montantSalaireTrancheB, 2));
        if (montantSalaireTrancheB != null && montantSalaireTrancheB.doubleValue() > 0d) {
            periodePubliee.setNatureMontantSalaireTrancheB(NATURE_MONTANT_940);
        } else {
            periodePubliee.setNatureMontantSalaireTrancheB(NATURE_MONTANT_950);
        }

        Double montantSalaireTrancheC = trancheCategorieRepository.getMontantSalaireTranche(periodePubliee.getIdPeriode(),
                categorieQuittancement.getNumCategorieQuittancement(), categorieQuittancement.getIndividu(), 3);

        periodePubliee.setMontantSalaireTrancheC(NumberUtils.degradeDoubleEnLongValeurAbsolue(montantSalaireTrancheC, 2));
        if (montantSalaireTrancheC != null && montantSalaireTrancheC.doubleValue() > 0d) {
            periodePubliee.setNatureMontantSalaireTrancheC(NATURE_MONTANT_940);
        } else {
            periodePubliee.setNatureMontantSalaireTrancheC(NATURE_MONTANT_950);
        }

        Double montantSalaireTrancheD = trancheCategorieRepository.getMontantSalaireTranche(periodePubliee.getIdPeriode(),
                categorieQuittancement.getNumCategorieQuittancement(), categorieQuittancement.getIndividu(), 4);

        periodePubliee.setMontantSalaireTrancheD(NumberUtils.degradeDoubleEnLongValeurAbsolue(montantSalaireTrancheD, 2));
        if (montantSalaireTrancheD != null && montantSalaireTrancheD.doubleValue() > 0d) {
            periodePubliee.setNatureMontantSalaireTrancheD(NATURE_MONTANT_940);
        } else {
            periodePubliee.setNatureMontantSalaireTrancheD(NATURE_MONTANT_950);
        }

        Double montantCotisation = categorieQuittancement.getMontantCotisation();

        periodePubliee.setMontantCotisation(NumberUtils.degradeDoubleEnLongValeurAbsolue(montantCotisation, 2));
        if (montantCotisation != null && montantCotisation.doubleValue() > 0d) {
            periodePubliee.setNatureMontantCotisation(NATURE_MONTANT_940);
        } else {
            periodePubliee.setNatureMontantCotisation(NATURE_MONTANT_950);
        }

    }

    /**
     * Valorisation des champs referencePaiement et modePaiement de la période publiée.
     * 
     * @param periodePubliee
     *            La période publiée - ligne à écrire dans le fichier de sortie
     * @param periode
     *            la période à publier
     */
    private void remplissageChampsPaiement(PeriodePublieeGERD periodePubliee, String numCategorieQuittancement) {
        // F78_RG_P3_11
        Versement versement = rattachementDeclarationsRecuesRepository.getDernierVersementPositifPourPeriode(periodePubliee.getIdPeriode());

        if (versement == null) {
            periodePubliee.setReferencePaiement("");
            periodePubliee.setModePaiement("");
            periodePubliee.setDatePaiement(0);
        } else {
            periodePubliee.setDatePaiement(versement.getDatePaiement() != null ? versement.getDatePaiement() : 0);

            String modePaiement = rechercheModePaiement(versement);
            periodePubliee.setModePaiement(modePaiement);

            String referencePaiement = "";
            if ("TLR".equals(modePaiement)) {
                ComposantVersement composantVersement = composantVersementRepository.getComposantVersementPourNumCategorie(versement.getIdVersement(),
                        numCategorieQuittancement);
                if (composantVersement != null) {
                    referencePaiement = StringUtils.stripEnd(versement.getCodeIdentifiantFonds(), null) + "-"
                            + StringUtils.stripEnd(composantVersement.getCodeIdentifiantSousFonds(), null);
                    entityManager.detach(composantVersement);
                }

            } else {
                referencePaiement = versement.getReferencePaiement();
            }

            periodePubliee.setReferencePaiement(StringUtils.substring(referencePaiement, 0, 80));
        }

        Long paiement = NumberUtils.degradeDoubleEnLongValeurAbsolue(
                rattachementDeclarationsRecuesRepository.getSommeMontantsVersement(periodePubliee.getIdPeriode(), null), 2);
        periodePubliee.setMontantPaiement(paiement);

        Long telereglement = NumberUtils.degradeDoubleEnLongValeurAbsolue(
                rattachementDeclarationsRecuesRepository.getSommeMontantsVersement(periodePubliee.getIdPeriode(), "05"), 2);
        periodePubliee.setMontantTelereglement(telereglement);

    }

    /**
     * Recherche le mode de paiement de la période à publier.
     * 
     * @param versement
     *            le versement témoin
     * @return le mode de paiement de la période
     */
    private String rechercheModePaiement(Versement versement) {
        String modePaiement = "";
        switch (versement.getModePaiement()) {
        case "05":
            modePaiement = "TLR";
            break;
        case "03":
            modePaiement = "PRL";
            break;
        case "04":
            modePaiement = "TIP";
            break;
        case "02":
            modePaiement = "VRT";
            break;
        case "01":
            modePaiement = "CHQ";
            break;
        default:
        }

        return modePaiement;
    }

    /**
     * Initialise le writer.
     * 
     * @param stepExecution
     *            l'exécution de l'étape
     */
    public void initialiseWriter(StepExecution stepExecution) {
        publicationRedacteurFichierPlat.setResource(fichierSortie);
        publicationRedacteurFichierPlat.open(stepExecution.getExecutionContext());
    }

    @BeforeStep
    protected void saveStepExecution(StepExecution stepExecution) {
        this.stepExecution = stepExecution;
        initialiseWriter(stepExecution);
    }

    /**
     * Ferme le writer.
     * 
     */
    @AfterStep
    public void fermeWriter() {
        publicationRedacteurFichierPlat.close();
    }

}
