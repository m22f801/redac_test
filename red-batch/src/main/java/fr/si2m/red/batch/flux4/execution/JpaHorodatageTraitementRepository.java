package fr.si2m.red.batch.flux4.execution;

import java.util.List;

import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import fr.si2m.red.core.repository.jpa.JpaEntiteImportableRepository;

/**
 * Base de données des entités {@link HorodatageTraitement}, connectée via JPA.
 * 
 * @author poidij
 *
 */
@Repository
public class JpaHorodatageTraitementRepository extends JpaEntiteImportableRepository<HorodatageTraitement> implements HorodatageTraitementRepository {

    @Override
    @Transactional(propagation = Propagation.MANDATORY)
    public boolean existeUnHorodatageTraitement(String horodatage) {
        TypedQuery<Long> query = getEntityManager().createQuery(
                "SELECT count(*) FROM HorodatageTraitement h WHERE h.horodatage = :horodatage AND h.ligneEnCoursImportBatch IS FALSE", Long.class);
        query.setParameter("horodatage", horodatage);
        List<Long> result = query.getResultList();
        if (result.isEmpty() || result.get(0) == null) {
            return false;
        }
        return result.get(0) > 0;
    }

}
