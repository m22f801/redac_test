package fr.si2m.red.batch.moteur.execution;

import java.io.InputStream;
import java.util.List;

/**
 * Référentiel des fichiers batchs ayant servis lors des jobs d'import REDAC.
 * 
 * @author nortaina
 *
 */
public interface FichierBatchRepository {
    /**
     * Sauvegarde un fichier batch dans le référentiel.
     * 
     * @param idExecutionJob
     *            l'ID d'exécution du job d'import
     * @param nomFichier
     *            le nom du fichier à sauvegarder
     * @param fluxContenuFichier
     *            le flux permettant la lecture du contenu du fichier
     * @param tailleFichier
     *            la taille du contenu
     */
    void sauvegardeFichierImporte(long idExecutionJob, String nomFichier, InputStream fluxContenuFichier, int tailleFichier);

    /**
     * Récupère la liste des fichiers importés par un batch dans le référentiel.
     * 
     * @param idExecutionJob
     *            l'ID d'exécution du job d'import
     * @return la liste des fichiers importés pour l'exécution du job donné
     */
    List<String> listeFichiersImportes(long idExecutionJob);

    /**
     * Récupère un flux pour la lecture d'un fichier importé.
     * 
     * @param idExecutionJob
     *            l'ID d'exécution du job d'import
     * @param nomFichier
     *            le nom du fichier importé à lire
     * @return le flux pour la lecture du fichier s'il existe, null sinon
     */
    InputStream getContenuFichierImporte(long idExecutionJob, String nomFichier);
}
