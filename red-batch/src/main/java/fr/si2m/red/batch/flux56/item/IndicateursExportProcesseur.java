package fr.si2m.red.batch.flux56.item;

import org.springframework.beans.factory.annotation.Autowired;

import fr.si2m.red.batch.moteur.item.TransformateurDonneeAvecCollecte;
import fr.si2m.red.contrat.Contrat;
import fr.si2m.red.contrat.ContratRepository;
import fr.si2m.red.parametrage.ParamFamilleContratRepository;
import fr.si2m.red.reconciliation.IndicateursDSNPeriode;
import lombok.Setter;

/**
 * Filtre des lignes d'indicateurs par famille de contrat.
 * 
 * @author poidij
 *
 */
public class IndicateursExportProcesseur extends TransformateurDonneeAvecCollecte<IndicateursDSNPeriode, IndicateursDSNPeriode> {

    @Autowired
    private ContratRepository contratRepository;
    @Autowired
    private ParamFamilleContratRepository paramFamilleContratRepository;

    @Setter
    private String cheminAbsoluFichierLogErreurs;

    private static final String DATE_FIN_PERIODE = ", dateFinPeriode=";

    @Override
    public IndicateursDSNPeriode process(IndicateursDSNPeriode ligneIndicateur) {

        String numeroContrat = ligneIndicateur.getNumContrat();
        Integer dateFinPeriode = ligneIndicateur.getFinPeriode();

        Contrat contrat = contratRepository.getContratValidePourDate(numeroContrat, dateFinPeriode);

        if (contrat == null) {
            String mesageErreur = "Aucun contrat valide n'a été retrouvé pour l'indicateur [IndicateurDSNPeriode(numContrat="
                    + ligneIndicateur.getNumContrat() + DATE_FIN_PERIODE + dateFinPeriode + ")]";
            traceErreurSansBlocage(mesageErreur, this.cheminAbsoluFichierLogErreurs);
            return null;
        }

        Integer numFamilleProduit = contrat.getNumFamilleProduit();
        String groupeGestion = contrat.getGroupeGestion();
        String applicationSiAval = paramFamilleContratRepository.getApplicationSiAval(numFamilleProduit, groupeGestion);

        // F0506_RG_CI07
        if (applicationSiAval == null) {
            String mesageErreur = "Le couple nofam " + numFamilleProduit + "/ groupe gestion " + groupeGestion
                    + " n'est pas valide pour l'indicateur [IndicateurDSNPeriode(numContrat=" + numeroContrat + DATE_FIN_PERIODE + dateFinPeriode
                    + ")]";
            traceErreurSansBlocage(mesageErreur, this.cheminAbsoluFichierLogErreurs);
            return null;
        }

        return ligneIndicateur;

    }

}
