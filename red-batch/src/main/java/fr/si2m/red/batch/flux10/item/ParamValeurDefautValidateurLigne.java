package fr.si2m.red.batch.flux10.item;

import org.apache.commons.lang3.StringUtils;

import fr.si2m.red.batch.moteur.erreur.ValidateurLigneAvecCollecte;
import fr.si2m.red.parametrage.ParamValeurDefaut;

/**
 * Validateur de ligne .
 * 
 * @author poidij
 *
 */
public class ParamValeurDefautValidateurLigne extends ValidateurLigneAvecCollecte<ParamValeurDefaut> {

    @Override
    protected void valide(final ParamValeurDefaut valeurDefaut) {

        // Champs obligatoires
        valideChampAvecCollecte(StringUtils.isNotBlank(valeurDefaut.getReferenceGestionnaire()), valeurDefaut, "REFERENCE_GEST",
                "le champ REFERENCE_GEST obligatoire, n’est pas renseigné");

        valideChampAvecCollecte(StringUtils.isNotBlank(valeurDefaut.getLibelleGestionnaire()), valeurDefaut, "LIBELLE_GEST",
                "le champ LIBELLE_GEST obligatoire, n’est pas renseigné");

        valideChampAvecCollecte(StringUtils.isNotBlank(valeurDefaut.getPrenomGestionnaire()), valeurDefaut, "PRENOM_GEST",
                "le champ PRENOM_GEST obligatoire, n’est pas renseigné");

        valideChampAvecCollecte(StringUtils.isNotBlank(valeurDefaut.getNomGestionnaire()), valeurDefaut, "NOM_GEST",
                "le champ NOM_GEST obligatoire, n’est pas renseigné");

        valideChampAvecCollecte(StringUtils.isNotBlank(valeurDefaut.getUniteGestionGestionnaire()), valeurDefaut, "UNITE_GESTION_GEST",
                "le champ UNITE_GESTION_GEST obligatoire, n’est pas renseigné");

        valideChampAvecCollecte(StringUtils.isNotBlank(valeurDefaut.getServiceGestionnaire()), valeurDefaut, "SERVICE_GEST",
                "le champ SERVICE_GEST obligatoire, n’est pas renseigné");

        valideChampAvecCollecte(StringUtils.isNotBlank(valeurDefaut.getCentreGestionGestionnaire()), valeurDefaut, "CENTRE_GESTION_GEST",
                "le champ CENTRE_GESTION_GEST obligatoire, n’est pas renseigné");

        // Longueur des champs

        valideChampAvecCollecte(valeurDefaut.getCodeOrganisme().length() <= 9, valeurDefaut, "CODE_ORGANISME",
                "le champ CODE_ORGANISME dépasse la taille maximale prévue");

        valideChampAvecCollecte(valeurDefaut.getExploitationDSNAsText().length() <= 3, valeurDefaut, "EXPLOIT",
                "le champ EXPLOIT dépasse la taille maximale prévue");

        valideChampAvecCollecte(valeurDefaut.getEditionConsignePaiementModeDSNAsText().length() <= 3, valeurDefaut, "EDIT_CONS_PAIMT",
                "le champ EDIT_CONS_PAIMT dépasse la taille maximale prévue");

        valideChampAvecCollecte(valeurDefaut.getTransfertDeclarationsVersQuatremAutoriseAsText().length() <= 3, valeurDefaut, "TRANSFERT",
                "le champ TRANSFERT dépasse la taille maximale prévue");

        if (valeurDefaut.getSeuilVariationAlertesEnNbEtablissements() != null) {
            valideChampAvecCollecte(valeurDefaut.getSeuilVariationAlertesEnNbEtablissements() < 100000000, valeurDefaut, "VAR_ETAB_NB",
                    "le champ VAR_ETAB_NB dépasse la taille maximale prévue");
        }

        if (valeurDefaut.getSeuilVariationAlertesEnPourcentageNbEtablissements() != null) {
            valideChampAvecCollecte(valeurDefaut.getSeuilVariationAlertesEnPourcentageNbEtablissements() < 1000, valeurDefaut, "VAR_ETAB_PC",
                    "le champ VAR_ETAB_PC dépasse la taille maximale prévue");
        }

        if (valeurDefaut.getSeuilVariationAlertesEnNbSalaries() != null) {
            valideChampAvecCollecte(valeurDefaut.getSeuilVariationAlertesEnNbSalaries() < 100000000, valeurDefaut, "VAR_SAL_NB",
                    "le champ VAR_SAL_NB dépasse la taille maximale prévue");
        }

        if (valeurDefaut.getSeuilVariationAlertesEnPourcentageNbSalaries() != null) {
            valideChampAvecCollecte(valeurDefaut.getSeuilVariationAlertesEnPourcentageNbSalaries() < 1000, valeurDefaut, "VAR_SAL_PC",
                    "le champ VAR_SAL_PC dépasse la taille maximale prévue");
        }

        valideChampAvecCollecte(valeurDefaut.getModeNatureContrat().length() <= 1, valeurDefaut, "MODE_NAT_CONT",
                "le champ MODE_NAT_CONT dépasse la taille maximale prévue");

        valideChampAvecCollecte(valeurDefaut.getContratVIPAsText().length() <= 1, valeurDefaut, "VIP",
                "le champ VIP dépasse la taille maximale prévue");

        valideChampAvecCollecte(valeurDefaut.getModeReaffectationCategorieEffectifs().length() <= 5, valeurDefaut, "MODE_REAFF_CAT_EFFECTIF",
                "le champ MODE_REAFF_CAT_EFFECTIF dépasse la taille maximale prévue");

        if (valeurDefaut.getSeuilDifferenceCotisationEnMontant() != null) {
            valideChampAvecCollecte(valeurDefaut.getSeuilDifferenceCotisationEnMontant() < 100000000, valeurDefaut, "SEUIL_DIFF_COT_MT",
                    "le champ SEUIL_DIFF_COT_MT dépasse la taille maximale prévue");
        }

        if (valeurDefaut.getSeuilDifferenceCotisationEnPourcentage() != null) {
            valideChampAvecCollecte(valeurDefaut.getSeuilDifferenceCotisationEnPourcentage() < 1000, valeurDefaut, "SEUIL_DIFF_COT_PC",
                    "le champ SEUIL_DIFF_COT_PC dépasse la taille maximale prévue");
        }

        valideLongueurChampsObligatoires(valeurDefaut);
    }

    private void valideLongueurChampsObligatoires(final ParamValeurDefaut valeurDefaut) {
        // Check référence du gestionnaire
        if (StringUtils.isNotBlank(valeurDefaut.getReferenceGestionnaire())) {
            valideChampAvecCollecte(valeurDefaut.getReferenceGestionnaire().length() <= 10, valeurDefaut, "REFERENCE_GEST",
                    "le champ REFERENCE_GEST dépasse la taille maximale prévue");
        }

        // Check libellé du gestionnaire
        if (StringUtils.isNotBlank(valeurDefaut.getLibelleGestionnaire())) {
            valideChampAvecCollecte(valeurDefaut.getLibelleGestionnaire().length() <= 30, valeurDefaut, "LIBELLE_GEST",
                    "le champ LIBELLE_GEST dépasse la taille maximale prévue");
        }

        // Check prénom du gestionnaire
        if (StringUtils.isNotBlank(valeurDefaut.getPrenomGestionnaire())) {
            valideChampAvecCollecte(valeurDefaut.getPrenomGestionnaire().length() <= 30, valeurDefaut, "PRENOM_GEST",
                    "le champ PRENOM_GEST dépasse la taille maximale prévue");
        }

        // Check nom du gestionnaire
        if (StringUtils.isNotBlank(valeurDefaut.getNomGestionnaire())) {
            valideChampAvecCollecte(valeurDefaut.getNomGestionnaire().length() <= 30, valeurDefaut, "NOM_GEST",
                    "le champ NOM_GEST dépasse la taille maximale prévue");
        }

        // Check unité de gestion du gestionnaire
        if (StringUtils.isNotBlank(valeurDefaut.getUniteGestionGestionnaire())) {
            valideChampAvecCollecte(valeurDefaut.getUniteGestionGestionnaire().length() <= 30, valeurDefaut, "UNITE_GESTION_GEST",
                    "le champ UNITE_GESTION_GEST dépasse la taille maximale prévue");
        }

        // Check service du gestionnaire
        if (StringUtils.isNotBlank(valeurDefaut.getServiceGestionnaire())) {
            valideChampAvecCollecte(valeurDefaut.getServiceGestionnaire().length() <= 30, valeurDefaut, "SERVICE_GEST",
                    "le champ SERVICE_GEST dépasse la taille maximale prévue");
        }

        // Check centre de gestion du gestionnaire
        if (StringUtils.isNotBlank(valeurDefaut.getCentreGestionGestionnaire())) {
            valideChampAvecCollecte(valeurDefaut.getCentreGestionGestionnaire().length() <= 30, valeurDefaut, "CENTRE_GESTION_GEST",
                    "le champ CENTRE_GESTION_GEST dépasse la taille maximale prévue");
        }
    }

}
