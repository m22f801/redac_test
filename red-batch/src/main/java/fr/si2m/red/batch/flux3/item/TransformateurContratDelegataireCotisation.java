package fr.si2m.red.batch.flux3.item;

import org.apache.commons.lang3.StringUtils;

import fr.si2m.red.batch.flux3.ligne.ContratDelegataireCotisation;
import fr.si2m.red.batch.flux3.ligne.SituationContrat;
import fr.si2m.red.batch.moteur.item.TransformateurDonnee;

/**
 * Mapper des entités ContratDelegataireCotisation à partir de la SituationContrat fournie.
 * 
 * cf F03_RG_S08
 * 
 * @author poidij
 *
 */
public class TransformateurContratDelegataireCotisation extends TransformateurDonnee<SituationContrat, ContratDelegataireCotisation> {

    @Override
    public ContratDelegataireCotisation process(SituationContrat situationContrat) throws Exception {
        ContratDelegataireCotisation contratDgc = new ContratDelegataireCotisation();

        if (StringUtils.isBlank(situationContrat.getNumCompteEncaisseur())) {
            return null;
        }

        contratDgc.setCodeMiseAJour("R");
        contratDgc.setIdentifiantTechniqueEntreprise("");
        contratDgc.setIdentifiantTechniqueContrat(situationContrat.getNumContrat());
        contratDgc.setIdentifiantTechniqueDelegataire(situationContrat.getNumCompteEncaisseur());
        contratDgc.setDateDebutValiditeDelegation(situationContrat.getDateDebutSituation());
        contratDgc.setDateFinValiditeDelegation(situationContrat.getDateFinSituation());
        return contratDgc;
    }

}
