package fr.si2m.red.batch.flux4.item;

import lombok.Setter;

import org.apache.commons.lang3.StringUtils;

import fr.si2m.red.DateRedac;
import fr.si2m.red.batch.RedacMessages;
import fr.si2m.red.batch.moteur.erreur.ValidateurLigneAvecCollecte;
import fr.si2m.red.dsn.AdhesionEtablissementMoisRepository;
import fr.si2m.red.dsn.Individu;
import fr.si2m.red.dsn.IndividuRepository;

/**
 * Validateur de ligne de paramétrage code libellé.
 * 
 * @author benitahy
 *
 */
public class IndividuValidateurLigne extends ValidateurLigneAvecCollecte<Individu> {

    @Setter
    private AdhesionEtablissementMoisRepository adhesionEtablissementMoisRepository;

    @Setter
    private IndividuRepository individuRepository;

    @Override
    protected void valide(final Individu individu) {

        // unicité
        valideChampAvecCollecte(!individuRepository.existeUnIndividu(individu.getIdIndividu()), individu, "ID", "La ligne ne respecte pas la règle d'unicité");

        // Champs obligatoires
        valideChampAvecCollecte(StringUtils.isNotBlank(individu.getIdIndividu()), individu, "ID_INDIVIDU", "Le champ ID_INDIVIDU obligatoire n'est pas renseigné");
        valideChampAvecCollecte(StringUtils.isNotBlank(individu.getIdAdhEtabMois()), individu, "ID_ADH_ETAB_MOIS", "Le champ ID_ADH_ETAB_MOIS obligatoire n'est pas renseigné");

        // Longueur des champs
        valideTailleFixeChampAvecCollecte(individu.getIdIndividu(), 30, individu, "ID_INDIVIDU");
        valideTailleFixeChampAvecCollecte(individu.getIdAdhEtabMois(), 30, individu, "ID_ADH_ETAB_MOIS");
        valideTailleFixeChampAvecCollecte(individu.getIdentifiantRepertoire(), 13, individu, "IDENTIFIANT_REPERTOIRE");
        valideTailleFixeChampAvecCollecte(individu.getNomFamille(), 80, individu, "NOM_FAMILLE");
        valideTailleFixeChampAvecCollecte(individu.getNomUsage(), 80, individu, "NOM_USAGE");
        valideTailleFixeChampAvecCollecte(individu.getPrenom(), 80, individu, "PRENOM");
        valideTailleFixeChampAvecCollecte(individu.getSexe(), 2, individu, "SEXE");
        valideChampDateTailleFixeAvecCollecte(individu.getDateNaissanceAsText(), DateRedac.FORMAT_DATES_DDMMYYYY, individu, "DATE_NAISSANCE", false);
        valideTailleFixeChampAvecCollecte(individu.getLieuNaissance(), 30, individu, "LIEU_NAISSANCE");
        valideTailleFixeChampAvecCollecte(individu.getVoie(), 50, individu, "VOIE");
        valideTailleFixeChampAvecCollecte(individu.getCodePostal(), 5, individu, "CODE_POSTAL");
        valideTailleFixeChampAvecCollecte(individu.getLocalite(), 50, individu, "LOCALITE");
        valideTailleFixeChampAvecCollecte(individu.getCodePays(), 2, individu, "CODE_PAYS");
        valideTailleFixeChampAvecCollecte(individu.getCodeDistribution(), 50, individu, "CODE_DISTRIBUTION");
        valideTailleFixeChampAvecCollecte(individu.getDepartementNaissance(), 2, individu, "DEPARTEMENT_NAISSANCE");
        valideTailleFixeChampAvecCollecte(individu.getPaysNaissance(), 2, individu, "PAYS_NAISSANCE");
        valideTailleFixeChampAvecCollecte(individu.getComplementConstruction(), 50, individu, "COMPLEMENT_CONSTRUCTION");
        valideTailleFixeChampAvecCollecte(individu.getComplementVoie(), 50, individu, "COMPLEMENT_VOIE");
        valideTailleFixeChampAvecCollecte(individu.getAdresseMail(), 100, individu, "MAIL");
        valideTailleFixeChampAvecCollecte(individu.getMatricule(), 30, individu, "MATRICULE");
        valideTailleFixeChampAvecCollecte(individu.getNumeroTechniqueTemporaire(), 40, individu, "NTT");
        valideChampIntegerTailleFixeAvecCollecte(individu.getNombreEnfantChargeAsText(), 2, individu, "NOMBRE_ENFANTS");
        valideTailleFixeChampAvecCollecte(individu.getStatutEtranger(), 2, individu, "STATUT_ETRANGER");
        valideTailleFixeChampAvecCollecte(individu.getEmbauche(), 2, individu, "EMBAUCHE");

        validationCorrespondanceReferentielle(individu);
    }

    /**
     * Validation des liens entre tables
     * 
     * @param individu
     *            La cotisationEtablissement dont l'Adhésion doit être validée.
     */
    private void validationCorrespondanceReferentielle(final Individu individu) {
        if (StringUtils.isNotBlank(individu.getIdAdhEtabMois())) {
            boolean existeUneAdhesion = adhesionEtablissementMoisRepository.existeUneAdhesion(individu.getIdAdhEtabMois());
            valideChampAvecCollecte(existeUneAdhesion, individu, "IdAdhEtabMois", RedacMessages.ERREUR_REFERENCE_FLUX4);
        }
    }
}
