package fr.si2m.red.batch.flux78.item;

import lombok.Getter;
import lombok.Setter;

/**
 * Modèle des périodes publiées pour WQUI.
 * 
 * @author poidij
 *
 */
public class PeriodePublieeWQUI {

    @Getter
    @Setter
    private Long idPeriode;
    @Getter
    @Setter
    private Integer typeEnregistrement;
    @Getter
    @Setter
    private Integer contratZoneA;
    @Getter
    @Setter
    private Integer contratZoneB;
    @Getter
    @Setter
    private String contratZoneC;
    @Getter
    @Setter
    private Integer dateDebutPeriode;
    @Getter
    @Setter
    private Integer dateFinPeriode;
    @Getter
    @Setter
    private String categorieQuittancement;
    @Getter
    @Setter
    private String libelleCategorieQuittancement;
    @Getter
    @Setter
    private Integer dateDebutPeriodeTarification;
    @Getter
    @Setter
    private Integer dateFinPeriodeTarification;
    @Getter
    @Setter
    private Integer numTranche;
    @Getter
    @Setter
    private String typeBase;
    @Getter
    @Setter
    private Integer typeDocument;
    @Getter
    @Setter
    private String periodeComplementaireOuCorrective;
    @Getter
    @Setter
    private String referencePaiement;
    @Getter
    @Setter
    private String modePaiement;
    @Getter
    @Setter
    private Long effectifDebutPeriode;
    @Getter
    @Setter
    private Long effectifFinDecembre;
    @Getter
    @Setter
    private String signeMontantCotisation;
    @Getter
    @Setter
    private Long montantCotisation;
    @Getter
    @Setter
    private String modeDeclaration;
    @Getter
    @Setter
    private String fillerPartieCommune;
}
