package fr.si2m.red.batch.flux4.support;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import fr.si2m.red.core.repository.jdbc.JdbcRepository;
import fr.si2m.red.dsn.EtatPeriode;
import fr.si2m.red.reconciliation.PeriodeRecue;

/**
 * Un mapper de ligne {@link PeriodeRecue} pour supporter la récupération de données Périodes Reçues via JDBC.
 * 
 * @author nortaina
 *
 */
public class PeriodeRecueRowMapper implements RowMapper<PeriodeRecue> {

    @Override
    public PeriodeRecue mapRow(ResultSet rs, int rowNum) throws SQLException {
        PeriodeRecue periodeRecue = new PeriodeRecue();
        periodeRecue.setIdPeriode(rs.getLong("ID_PERIODE"));
        periodeRecue.setTypePeriode(rs.getString("TYPE_PERIODE"));
        periodeRecue.setDateCreation(JdbcRepository.getInteger(rs, "DATE_CREATION"));
        periodeRecue.setNumeroContrat(rs.getString("NUMERO_CONTRAT"));
        periodeRecue.setDateDebutPeriode(JdbcRepository.getInteger(rs, "DATE_DEBUT_PERIODE"));
        periodeRecue.setDateFinPeriode(JdbcRepository.getInteger(rs, "DATE_FIN_PERIODE"));
        periodeRecue.setPremierMoisDeclAsText(rs.getString("PREMIER_MOIS_DECL"));
        periodeRecue.setEtatPeriode(EtatPeriode.valueOf(rs.getString("ETAT_PERIODE")));
        periodeRecue.setReconsoliderAsText(rs.getString("RECONSOLIDER"));
        periodeRecue.setDateEcheance(JdbcRepository.getInteger(rs, "DATE_ECHEANCE"));
        periodeRecue.setAffecteA(rs.getString("AFFECTE_A"));
        periodeRecue.setATraiterPar(rs.getString("A_TRAITER_PAR"));
        periodeRecue.setTraitePar(rs.getString("TRAITE_PAR"));
        return periodeRecue;
    }
}
