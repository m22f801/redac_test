package fr.si2m.red.batch.flux3.item;

import fr.si2m.red.batch.moteur.item.RedacteurFichierPlatSansConversion;

/**
 * Rédacteur du fichier des gestionnaires (simples).
 * 
 * @param <T>
 *            le type des entités à exporter
 */
public class GestionnaireEntrepriseAffilieeRedacteurFichierPlat<T> extends RedacteurFichierPlatSansConversion<T> {

    /**
     * Constructeur par défaut pour le rédacteur du fichier ETABLISSEMENT.
     * 
     * @param nomsProprietesExtraites
     *            les noms des propriétés extraites telles quelles dans l'entité
     * @param format
     *            le format d'écriture des propriétés extraites
     */
    public GestionnaireEntrepriseAffilieeRedacteurFichierPlat(String[] nomsProprietesExtraites, String format) {
        super(nomsProprietesExtraites, format);
        setShouldDeleteIfExists(false);
    }
}
