package fr.si2m.red.batch.flux3.item;

import lombok.Setter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.si2m.red.batch.flux3.ligne.DelegataireCotisation;
import fr.si2m.red.batch.moteur.item.TransformateurDonnee;
import fr.si2m.red.contrat.Intermediaire;
import fr.si2m.red.contrat.IntermediaireId;
import fr.si2m.red.contrat.IntermediaireRepository;

/**
 * Mapper des entités DelegataireCotisation à partir du NCENC fourni.
 * 
 * cf F03_RG_S09
 * 
 * @author poidij
 *
 */
public class TransformateurDelegataireCotisation extends TransformateurDonnee<String, DelegataireCotisation> {

    private static final Logger LOGGER = LoggerFactory.getLogger(TransformateurDelegataireCotisation.class);

    @Setter
    private IntermediaireRepository intermediaireRepository;

    @Override
    public DelegataireCotisation process(String ncenc) throws Exception {

        DelegataireCotisation delegataireCotisation = new DelegataireCotisation();
        IntermediaireId id = new IntermediaireId();
        id.setCodeIntermediaire(ncenc);

        Intermediaire intermediaire = intermediaireRepository.get(id);

        if (intermediaire == null) {
            LOGGER.info("L'intermédiaire " + ncenc + " (NCENC) n'existe pas ");
            return null;
        }

        delegataireCotisation.setCodeMiseAJour("R");
        delegataireCotisation.setIdentifiantTechniqueDelegataireCotisation(ncenc);
        delegataireCotisation.setDateDebutApplicationCaracteristiques(20000101);
        delegataireCotisation.setDateFinApplicationCaracteristiques(null);
        delegataireCotisation.setCodeOrganismeDelegataireCotisation(ncenc);
        delegataireCotisation.setLibelleDelegataireCotisation(intermediaire.getRaisonSociale());
        return delegataireCotisation;
    }

}
