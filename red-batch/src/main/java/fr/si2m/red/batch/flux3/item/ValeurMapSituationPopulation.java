/**
 * 
 */
package fr.si2m.red.batch.flux3.item;

/**
 * Classe interne permettant l'association dans la map entre une population (conbcot/nocat) et (dateDebut/indice situation)
 * 
 * @author eudesr
 *
 */
public class ValeurMapSituationPopulation {
    Integer dateDebut;
    int indiceTableau;

    /**
     * Constructeur paramétré
     * 
     * @param dateDebut
     *            la date de début de situation choisie
     * @param indiceTableau
     *            l'indice dans la liste situationsTarif de la situation
     */
    public ValeurMapSituationPopulation(Integer dateDebut, int indiceTableau) {
        super();
        this.dateDebut = dateDebut;
        this.indiceTableau = indiceTableau;
    }

    /**
     * La représentation sous forme de String de la valeur datedebut+indiceTableau concaténés utilisé dans la Map
     * 
     * @return représentation String de la paire datedebut + indiceTableau
     */
    @Override
    public String toString() {
        return dateDebut.toString() + " " + indiceTableau;
    }
}
