package fr.si2m.red.batch.flux4.item;

import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import fr.si2m.red.DateRedac;
import fr.si2m.red.batch.moteur.erreur.ValidateurLigneAvecCollecte;
import fr.si2m.red.dsn.ComposantVersement;
import fr.si2m.red.dsn.ComposantVersementRepository;
import lombok.Setter;

/**
 * Renseigne les dates de début et fin des ComposantVersement en fonction de leur periodeAffectation.
 * 
 * 
 * @author poidij
 *
 */
public class ValidateurComposantVersement extends ValidateurLigneAvecCollecte<ComposantVersement> {

    @Setter
    private ComposantVersementRepository composantVersementRepository;

    @Override
    protected void valide(ComposantVersement composantVersement) {

        if (valideChampPeriodeAffectation(composantVersement)) {
            // Calcul des champs dates de début / fin de période d'affectation
            calculeDatesPeriodeAffectation(composantVersement);

            // On modifie l'entité ici, dans le processor
            composantVersementRepository.modifieEntiteExistante(composantVersement);

        }

    }

    /**
     * Calcule et renseigne les dates de fin et de début d'une période d'affectation d'un composant de versement.
     * 
     * @param composant
     *            le composant de versement à mettre à jour
     */
    private void calculeDatesPeriodeAffectation(ComposantVersement composant) {
        Integer dateDebutPeriode;
        Integer dateFinPeriode;
        // Période affectation = AAAAPNN
        String periodeAffectation = composant.getPeriodeAffectation();
        String anneePeriode = periodeAffectation.substring(0, 4);
        String periodicite = periodeAffectation.substring(4, 5);
        String rangPeriode = periodeAffectation.substring(5);
        if ("M".equals(periodicite)) {
            // Si P est égal à M :
            // La date de début de période d’affectation du composant de versement est égale à 01/NN/AAAA
            dateDebutPeriode = Integer.valueOf(anneePeriode + rangPeriode + "01");

            // La date de fin de période d’affectation du composant de versement est égale au dernier jour du mois NN/AAAA
            Calendar calendrier = Calendar.getInstance();
            calendrier.setTime(DateRedac.convertitEnDate(dateDebutPeriode));
            String dernierJourDuMois = transcritSurDeuxChiffres(calendrier.getActualMaximum(Calendar.DAY_OF_MONTH));
            dateFinPeriode = Integer.valueOf(anneePeriode + rangPeriode + dernierJourDuMois);
        } else if ("A".equals(periodicite) || "E".equals(periodicite)) {
            // Si P est égal à A
            // La date de début de période d’affectation du composant de versement est égale à 01/01/AAAA
            dateDebutPeriode = Integer.valueOf(anneePeriode + "0101");

            // La date de fin de période d’affectation du composant de versement est égale à 31/12/AAAA
            dateFinPeriode = Integer.valueOf(anneePeriode + "1231");
        } else {
            // Si P est égal à S ou T (par défaut)
            int periodiciteNumerique = "S".equals(periodicite) ? 6 : 3;

            // La date de début de période d’affectation du composant de versement est égale à 01/MM/AAAA avec MM = (NN-1)*periodicite+1
            String moisDebutPeriode = transcritSurDeuxChiffres((Integer.parseInt(rangPeriode) - 1) * periodiciteNumerique + 1);
            dateDebutPeriode = Integer.valueOf(anneePeriode + moisDebutPeriode + "01");

            // La date de fin de période d’affectation du composant de versement est égale au dernier jour du mois MM/AAAA avec MM = NN*periodicite
            String moisFinPeriode = transcritSurDeuxChiffres(Integer.parseInt(rangPeriode) * periodiciteNumerique);
            Date dateDebutMoisFinPeriode = DateRedac.convertitEnDate(Integer.valueOf(anneePeriode + moisFinPeriode + "01"));
            Calendar calendrier = Calendar.getInstance();
            calendrier.setTime(dateDebutMoisFinPeriode);
            String dernierJourDuMoisFinPeriode = transcritSurDeuxChiffres(calendrier.getActualMaximum(Calendar.DAY_OF_MONTH));
            dateFinPeriode = Integer.valueOf(anneePeriode + moisFinPeriode + dernierJourDuMoisFinPeriode);

        }
        composant.setDateDebutPeriodeAffectation(dateDebutPeriode);
        composant.setDateFinPeriodeAffectation(dateFinPeriode);
    }

    /**
     * Transcrit un entier sur deux caractères avec un 0 en début de nombre si besoin.
     * 
     * @param nombre
     *            le nombre à transcrire
     * @return le nombre transcrit
     */
    private String transcritSurDeuxChiffres(int nombre) {
        return ("00" + nombre).substring(String.valueOf(nombre).length());
    }

    /**
     * Validation du champ PeriodeAffectation
     * 
     * @param composantVersement
     *            le composantVersement à traiter
     * @return true si le champ PeriodeAffectation est valide, false sinon
     */
    private boolean valideChampPeriodeAffectation(final ComposantVersement composantVersement) {
        // F04_RG_P3_01
        List<String> periodesValides = Arrays.asList("M01", "M02", "M03", "M04", "M05", "M06", "M07", "M08", "M09", "M10", "M11", "M12", "T01", "T02",
                "T03", "T04", "S01", "S02", "A00", "E00");
        String periodeAffectation = composantVersement.getPeriodeAffectation();

        boolean validation = false;
        if (StringUtils.isNotBlank(periodeAffectation)) {
            String annee = periodeAffectation.substring(0, 4);
            String periode = composantVersement.getPeriodeAffectation().substring(4);

            boolean validationAnnee = StringUtils.isNumeric(annee) && 2010 <= Integer.parseInt(annee) && Integer.parseInt(annee) <= 2099;

            validation = validationAnnee && periodesValides.contains(periode);
        }

        return validation;
    }

}
