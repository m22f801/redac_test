package fr.si2m.red.batch.moteur;

import java.nio.charset.Charset;

/**
 * Configuration technique des batchs.
 * 
 * @author nortaina
 *
 */
public final class BatchConfiguration {
    /**
     * Encodage par défaut des fichiers importés.
     */
    public static final Charset ENCODAGE_FICHIERS_IMPORTES = Charset.forName("windows-1252");
    /**
     * Encodage par défaut des fichiers exportés.
     */
    public static final Charset ENCODAGE_FICHIERS_EXPORTES = Charset.forName("windows-1252");
    /**
     * Encodage par défaut des fichiers de logs (même que l'encodage du code Java donc UTF-8).
     */
    public static final Charset ENCODAGE_FICHIERS_LOGS = Charset.forName("UTF-8");

    private BatchConfiguration() {
    }
}
