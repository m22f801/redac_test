package fr.si2m.red.batch.flux10.item;

import org.apache.commons.lang3.StringUtils;

import fr.si2m.red.batch.moteur.erreur.ValidateurLigneAvecCollecte;
import fr.si2m.red.parametrage.ParamCodePays;

/**
 * Validateur de ligne de paramétrage code Pays
 * 
 * @author eudesr
 *
 */
public class ParamCodePaysValidateurLigne extends ValidateurLigneAvecCollecte<ParamCodePays> {

    @Override
    protected void valide(final ParamCodePays paramCodePays) {

        // Champ obligatoire
        valideChampAvecCollecte(StringUtils.isNotBlank(paramCodePays.getCodePays()), paramCodePays, "CODE_PAYS",
                "le champ CODE_PAYS obligatoire, n’est pas renseigné");

        valideChampAvecCollecte(StringUtils.isNotBlank(paramCodePays.getCodeInsee()), paramCodePays, "CODE_INSEE",
                "le champ CODE_INSEE obligatoire, n’est pas renseigné");

        valideChampAvecCollecte(StringUtils.isNotBlank(paramCodePays.getLibellePays()), paramCodePays, "LIBELLE_PAYS",
                "le champ LIBELLE_PAYS obligatoire, n’est pas renseigné");

        // Longueur des champs
        // F10_RG_C111
        if (paramCodePays.getCodePays() != null) {
            valideChampAvecCollecte(paramCodePays.getCodePays().length() <= 2, paramCodePays, "CODE_PAYS",
                    "le champ CODE_PAYS dépasse la taille maximale prévue");
        }
        // F10_RG_C113
        if (StringUtils.isNotBlank(paramCodePays.getCodeInsee())) {
            valideChampAvecCollecte(paramCodePays.getCodeInsee().length() <= 5, paramCodePays, "CODE_INSEE",
                    "le champ CODE_INSEE dépasse la taille maximale prévue");
        }
        // F10_RG_C114
        if (StringUtils.isNotBlank(paramCodePays.getLibellePays())) {
            valideChampAvecCollecte(paramCodePays.getLibellePays().length() <= 100, paramCodePays, "LIBELLE_PAYS",
                    "le champ LIBELLE_PAYS dépasse la taille maximale prévue");
        }

    }

}
