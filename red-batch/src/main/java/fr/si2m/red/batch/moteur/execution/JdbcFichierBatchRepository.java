package fr.si2m.red.batch.moteur.execution;

import java.io.InputStream;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.support.SqlLobValue;

import fr.si2m.red.core.repository.jdbc.JdbcRepository;

/**
 * Implémentation du référentiel des fichiers batchs importés dans la base de données REDAC se basant sur une connexion JDBC.
 * 
 * @author nortaina
 *
 */
public class JdbcFichierBatchRepository extends JdbcRepository implements FichierBatchRepository {

    private static final String PARAM_ID_EXECUTION_JOB = "idExecutionJob";
    private static final String PARAM_NOM_FICHIER = "nomFichier";
    private static final String PARAM_FLUX_CONTENU_FICHIER = "fluxContenuFichier";

    @Override
    public void sauvegardeFichierImporte(long idExecutionJob, String nomFichier, InputStream fluxContenuFichier, int tailleFichier) {
        String sql = "insert into FICHIER_BATCH (JOB_EXECUTION_ID, NOM_FICHIER, CONTENU_FICHIER) values (:" + PARAM_ID_EXECUTION_JOB + ", :"
                + PARAM_NOM_FICHIER + ", :" + PARAM_FLUX_CONTENU_FICHIER + ")";

        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue(PARAM_ID_EXECUTION_JOB, idExecutionJob);
        params.addValue(PARAM_NOM_FICHIER, nomFichier);
        params.addValue(PARAM_FLUX_CONTENU_FICHIER, new SqlLobValue(fluxContenuFichier, tailleFichier), Types.BLOB);
        getJdbcTemplate().update(sql, params);
    }

    @Override
    public List<String> listeFichiersImportes(long idExecutionJob) {
        String sql = "select NOM_FICHIER from FICHIER_BATCH where JOB_EXECUTION_ID=:" + PARAM_ID_EXECUTION_JOB;
        Map<String, Object> params = new HashMap<String, Object>();
        params.put(PARAM_ID_EXECUTION_JOB, idExecutionJob);
        return getJdbcTemplate().queryForList(sql, params, String.class);
    }

    @Override
    public InputStream getContenuFichierImporte(long idExecutionJob, String nomFichier) {
        String sql = "select CONTENU_FICHIER from FICHIER_BATCH where JOB_EXECUTION_ID=:" + PARAM_ID_EXECUTION_JOB + " and NOM_FICHIER=:"
                + PARAM_NOM_FICHIER;
        Map<String, Object> params = new HashMap<String, Object>();
        params.put(PARAM_ID_EXECUTION_JOB, idExecutionJob);
        params.put(PARAM_NOM_FICHIER, nomFichier);
        return getJdbcTemplate().queryForObject(sql, params, new RowMapper<InputStream>() {
            @Override
            public InputStream mapRow(ResultSet rs, int rowNum) throws SQLException {
                return rs.getBinaryStream("CONTENU_FICHIER");
            }
        });
    }
}
