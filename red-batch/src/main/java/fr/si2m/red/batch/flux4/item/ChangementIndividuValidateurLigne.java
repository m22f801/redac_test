package fr.si2m.red.batch.flux4.item;

import lombok.Setter;

import org.apache.commons.lang3.StringUtils;

import fr.si2m.red.DateRedac;
import fr.si2m.red.batch.RedacMessages;
import fr.si2m.red.batch.moteur.erreur.ValidateurLigneAvecCollecte;
import fr.si2m.red.dsn.ChangementIndividu;
import fr.si2m.red.dsn.ChangementIndividuRepository;
import fr.si2m.red.dsn.IndividuRepository;

/**
 * Validateur de ligne de paramétrage code libellé.
 * 
 * @author benitahy
 *
 */
public class ChangementIndividuValidateurLigne extends ValidateurLigneAvecCollecte<ChangementIndividu> {

    @Setter
    private IndividuRepository individuRepository;

    @Setter
    private ChangementIndividuRepository changementIndividuRepository;

    @Override
    protected void valide(final ChangementIndividu changementIndividu) {

        // unicité
        valideChampAvecCollecte(!changementIndividuRepository.existeUnChangementIndividu(changementIndividu.getIdChangementIndividu()), changementIndividu, "ID",
                "La ligne ne respecte pas la règle d'unicité");

        // Champs obligatoires
        valideChampAvecCollecte(StringUtils.isNotBlank(changementIndividu.getIdChangementIndividu()), changementIndividu, "ID_CHGT_INDIVIDU",
                "Le champ ID_CHGT_INDIVIDU obligatoire n'est pas renseigné");
        valideChampAvecCollecte(StringUtils.isNotBlank(changementIndividu.getIdIndividu()), changementIndividu, "ID_INDIVIDU",
                "Le champ ID_INDIVIDU obligatoire n'est pas renseigné");

        // Longueur des champs
        valideTailleFixeChampAvecCollecte(changementIndividu.getIdChangementIndividu(), 30, changementIndividu, "ID_CHGT_INDIVIDU");
        valideTailleFixeChampAvecCollecte(changementIndividu.getIdIndividu(), 30, changementIndividu, "ID_INDIVIDU");
        valideChampDateTailleFixeAvecCollecte(changementIndividu.getDateModificationAsText(), DateRedac.FORMAT_DATES_DDMMYYYY, changementIndividu, "DATE_MODIFICATION", false);
        valideTailleFixeChampAvecCollecte(changementIndividu.getAncienIdentifiant(), 13, changementIndividu, "ANCIEN_IDENTIFIANT");
        valideTailleFixeChampAvecCollecte(changementIndividu.getAncienNomFamille(), 80, changementIndividu, "ANCIEN_NOM_FAMILLE");
        valideTailleFixeChampAvecCollecte(changementIndividu.getAncienPrenom(), 80, changementIndividu, "ANCIEN_PRENOM");
        valideChampDateTailleFixeAvecCollecte(changementIndividu.getAncienneDateNaissanceAsText(), DateRedac.FORMAT_DATES_DDMMYYYY, changementIndividu, "ANCIENNE_DATE_NAISSANCE",
                false);

        validationCorrespondanceReferentielle(changementIndividu);
    }

    /**
     * Validation des liens entre tables
     * 
     * @param changementIndividu
     *            Le changement individu dont l'individu doit être validé.
     */
    private void validationCorrespondanceReferentielle(final ChangementIndividu changementIndividu) {
        if (StringUtils.isNotBlank(changementIndividu.getIdIndividu())) {
            boolean existeUnIndividu = individuRepository.existeUnIndividu(changementIndividu.getIdIndividu());
            valideChampAvecCollecte(existeUnIndividu, changementIndividu, "idIndividu", RedacMessages.ERREUR_REFERENCE_FLUX4);
        }
    }
}
