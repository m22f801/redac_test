package fr.si2m.red.batch.flux10.item;

import org.apache.commons.lang3.StringUtils;

import fr.si2m.red.batch.moteur.erreur.ValidateurLigneAvecCollecte;
import fr.si2m.red.parametrage.ParamSirenFaux;

/**
 * Validateur de ligne de paramétrage siren faux.
 * 
 * @author eudesr
 *
 */
public class ParamSirenFauxValidateurLigne extends ValidateurLigneAvecCollecte<ParamSirenFaux> {

    @Override
    protected void valide(final ParamSirenFaux sirenFaux) {

        // Champs obligatoires

        valideChampAvecCollecte(StringUtils.isNotBlank(sirenFaux.getSiren()), sirenFaux, "SIREN", "le champ SIREN obligatoire, n’est pas renseigné");

        // Longueur des champs

        if (StringUtils.isNotBlank(sirenFaux.getSiren())) {
            valideChampAvecCollecte(sirenFaux.getSiren().length() <= 9, sirenFaux, "SIREN", "le champ SIREN dépasse la taille maximale prévue");
        }

    }

}
