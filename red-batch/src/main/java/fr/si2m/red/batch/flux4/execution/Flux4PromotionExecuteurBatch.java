package fr.si2m.red.batch.flux4.execution;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParameter;
import org.springframework.batch.core.JobParameters;
import org.springframework.context.support.GenericXmlApplicationContext;

import fr.si2m.red.batch.moteur.CodeRetour;
import fr.si2m.red.batch.moteur.execution.FacadeExecuteurBatch;
import fr.si2m.red.dsn.AdhesionEtablissementMoisRepository;

/**
 * Exécuteur de la phase 3 - promotion des données importées - du flux 4.
 * 
 * @author poidij
 *
 */
@FacadeExecuteurBatch(nomBatch = "flux4_Promotion")
public class Flux4PromotionExecuteurBatch extends Flux4ExecuteurBatch {
    /**
     * Ordre du paramètre de lancement "emplacement du fichier de log d'erreurs" .
     */
    public static final int ORDRE_PARAM_EMPLACEMENT_FICHIER_ERREURS = 0;

    private static final Logger LOGGER = LoggerFactory.getLogger(Flux4PromotionExecuteurBatch.class);

    /**
     * Exécuteur de batch du flux 4.
     * 
     * @param parametrageBatch
     *            le paramétrage du batch
     */
    public Flux4PromotionExecuteurBatch(String[] parametrageBatch) {
        super(parametrageBatch);

        String cheminAbsoluFichierLogErreurs = parametrageBatch[ORDRE_PARAM_EMPLACEMENT_FICHIER_ERREURS];
        setCheminAbsoluFichierLogErreurs(cheminAbsoluFichierLogErreurs);
    }

    @Override
    protected String getIdJobFlux4() {
        return "Promotion";
    }

    @Override
    protected JobParameters initParametrageJob(GenericXmlApplicationContext contexteExecutionJob) {
        Map<String, JobParameter> parametresJob = new HashMap<String, JobParameter>();

        // Configuration de l'emplacement du fichier de log
        LOGGER.debug("Fichier erreurs pris en compte : {}", getCheminAbsoluFichierLogErreurs());
        parametresJob.put("fichierLogErreurs", new JobParameter(getCheminAbsoluFichierLogErreurs()));

        return new JobParameters(parametresJob);
    }

    @Override
    protected void lanceTraitementFinJob(GenericXmlApplicationContext contexteExecutionJob, CodeRetour codeRetour, JobExecution executionJob) {
        if (codeRetour != CodeRetour.TERMINE && codeRetour != CodeRetour.ERREUR_FONCTIONNELLE_NON_BLOQUANTE) {
            // Delete cascade à partir de l'entité parente
            videEspacesTemporaires(contexteExecutionJob, AdhesionEtablissementMoisRepository.class);
        }

        // Logge le message de réussite du traitement batch dans le fichier de log
        if (codeRetour == CodeRetour.TERMINE) {
            traceTraitementOKDansFichierLog("Traitement du flux 4 - Promotion - réussi");
        } else if (codeRetour == CodeRetour.ERREUR_FONCTIONNELLE_NON_BLOQUANTE) {
            traceTraitementOKDansFichierLog("Traitement du flux 4 - Promotion - réussi, avec erreur(s) non bloquante(s)");
        }
        LOGGER.info("Fin d'exécution du batch flux 4 - Promotion des données importées");
    }
}
