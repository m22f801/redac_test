package fr.si2m.red.batch.moteur.support;

import java.beans.PropertyEditor;
import java.util.HashMap;
import java.util.Map;

import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.beans.factory.BeanFactory;

/**
 * Gestionnaire de lecture de ligne réceptionnée pour un traitement batch REDAC.<br/>
 * <br/>
 * S'il y a besoin de conversion (mapping des champs de types complexes ou avec des besoins spécifiques), utiliser {@link ConvertisseurLigne}.
 * 
 * @author nortaina
 *
 * @param <T>
 *            type de la ligne à lire
 * 
 * @see ConvertisseurLigne
 */
public final class LecteurLigne<T> extends BeanWrapperFieldSetMapper<T> {

    /**
     * Constructeur par défaut.
     * 
     * @param beanFactory
     *            l'usine de bean à partir de laquelle récupérer le prototype
     * @param nomPrototype
     *            le nom du prototype de l'entité ligne à utiliser pour le mapping
     */
    public LecteurLigne(BeanFactory beanFactory, String nomPrototype) {
        super();

        setBeanFactory(beanFactory);
        setPrototypeBeanName(nomPrototype);

        Map<Class<?>, PropertyEditor> editeursProprietes = new HashMap<Class<?>, PropertyEditor>();
        // Pour la lecture des décimales au format français
        editeursProprietes.put(Double.class, new DecimalesFrancaisesProprieteEditeur());
        setCustomEditors(editeursProprietes);
    }

}
