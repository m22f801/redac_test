package fr.si2m.red.batch.flux3.item;

import lombok.Setter;
import fr.si2m.red.batch.flux3.ligne.Etablissement;
import fr.si2m.red.batch.moteur.item.TransformateurDonnee;
import fr.si2m.red.contrat.Client;
import fr.si2m.red.contrat.ClientRepository;

/**
 * Mapper des entités Etablissement à partir du numéro de Siret (Siren;nic) fourni.
 * 
 * @author benitahy
 *
 */
public class TransformateurEtablissement extends TransformateurDonnee<String, Etablissement> {

    /**
     * Référentiel des établissements.
     * 
     * @param clientRepository
     * 
     */
    @Setter
    private ClientRepository clientRepository;

    @Override
    public Etablissement process(String numSiret) throws Exception {
        String[] splitArray = null;
        splitArray = numSiret.split(";");
        String numeroSirenEtablissement = splitArray[0];
        String numeroSiretEtablissement = splitArray[1];
        Client client = clientRepository.getEtablissementDerniereModification(numeroSirenEtablissement, numeroSiretEtablissement);
        if (client == null) {
            return null;
        }
        Etablissement etablissement = new Etablissement();
        etablissement.setCodeMiseAJour("R");
        etablissement.setIdentifiantTechniqueEntreprise(client.getNumSiren());
        etablissement.setIdentifiantTechniqueEtablissement(client.getNumSiren() + client.getNumSiret());
        etablissement.setNicSiret(client.getNumSiret());
        etablissement.setNumIdentificationEtablissementDansOrganisme(0);
        return etablissement;
    }
}
