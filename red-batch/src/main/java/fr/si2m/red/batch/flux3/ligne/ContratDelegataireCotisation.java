package fr.si2m.red.batch.flux3.ligne;

import lombok.Data;
import fr.si2m.red.DateRedac;

/**
 * Description d'une ligne d'un fichier CONTRAT_DGC à exporter.
 * 
 * @author nortaina
 *
 */
@Data
public class ContratDelegataireCotisation {
    /**
     * Code Mise à jour.
     * 
     * @param codeMiseAJour
     *            Code Mise à jour
     * @return Code Mise à jour
     */
    private String codeMiseAJour;

    /**
     * Identifiant technique de l’entreprise.
     * 
     * @param identifiantTechniqueEntreprise
     *            Identifiant technique de l’entreprise
     * @return Identifiant technique de l’entreprise
     */
    private String identifiantTechniqueEntreprise;

    /**
     * Identifiant technique du contrat.
     * 
     * @param identifiantTechniqueContrat
     *            Identifiant technique du contrat
     * @return Identifiant technique du contrat
     */
    private String identifiantTechniqueContrat;

    /**
     * Identifiant technique du délégataire.
     * 
     * @param identifiantTechniqueDelegataire
     *            Identifiant technique du délégataire
     * @return Identifiant technique du délégataire
     */
    private String identifiantTechniqueDelegataire;

    /**
     * Date début de validité de la délégation.
     * 
     * @param dateDebutValiditeDelegation
     *            Date début de validité de la délégation
     * @return Date début de validité de la délégation
     */
    private Integer dateDebutValiditeDelegation;

    /**
     * Date fin de validité de la délégation.
     * 
     * @param dateFinValiditeDelegation
     *            Date fin de validité de la délégation
     * @return Date fin de validité de la délégation
     */
    private Integer dateFinValiditeDelegation;

    /**
     * Date début de validité de la délégation.
     * 
     * @return Date début de validité de la délégation
     */
    public String getDateDebutValiditeDelegationFormatee() {
        return DateRedac.formate(DateRedac.EXPORT_FORMAT_PAR_DEFAUT, dateDebutValiditeDelegation);
    }

    /**
     * Date fin de validité de la délégation.
     * 
     * @return Date fin de validité de la délégation
     */
    public String getDateFinValiditeDelegationFormatee() {
        return DateRedac.formate(DateRedac.EXPORT_FORMAT_PAR_DEFAUT, dateFinValiditeDelegation);
    }

}
