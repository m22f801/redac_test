package fr.si2m.red.batch.moteur.item;

import java.util.List;

import lombok.Setter;

import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;

import fr.si2m.red.batch.moteur.EtapeCodeRetourModificateur;
import fr.si2m.red.core.repository.EntiteImportableRepository;

/**
 * Tâche de promotion des domaines temporaires de référentiels lorsque toutes les entités importées ont été validées.
 * 
 * @author nortaina
 *
 */
public class TachePromotionEntitesTemporairesImportees extends EtapeCodeRetourModificateur implements Tasklet {

    /**
     * La liste de référentiels dont les données temporaires sont à promouvoir.
     */
    @Setter
    private List<EntiteImportableRepository<?>> referentiels;

    /**
     * Si la promotion doit se faire en mode différentiel et non en annuler/remplacer.
     */
    @Setter
    private boolean modeDifferentiel;

    @Override
    public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) throws Exception {
        // Ce mécanisme séquentiel permet de gérer les contraintes d'intégrité entre entités
        // Le mode différentiel ne fait que promouvoir les entités qui n'existent pas déjà en BDD et il n'existe pas de mise à jour
        if (!modeDifferentiel) {
            for (int i = referentiels.size() - 1; i >= 0; i--) {
                EntiteImportableRepository<?> referentiel = referentiels.get(i);
                referentiel.nettoieEntitesActives();
            }
        }
        for (EntiteImportableRepository<?> referentiel : referentiels) {
            referentiel.promeutEntitesTemporaires();
        }
        for (int i = referentiels.size() - 1; i >= 0; i--) {
            EntiteImportableRepository<?> referentiel = referentiels.get(i);
            referentiel.nettoieEntitesTemporaires();
        }
        return RepeatStatus.FINISHED;
    }
}
