package fr.si2m.red.batch.moteur.erreur;

import java.util.List;

import lombok.AccessLevel;
import lombok.Getter;

import org.apache.commons.lang3.BooleanUtils;
import org.springframework.batch.core.ItemWriteListener;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.annotation.BeforeStep;
import org.springframework.batch.core.listener.ExecutionContextPromotionListener;
import org.springframework.batch.item.ExecutionContext;

import fr.si2m.red.EntiteImportableBatch;

/**
 * Collecte le nombre de paquets d'{@link ErreursLigneFichierImport} écrits dans un fichier de log par une étape de validation pure.
 * 
 * @author nortaina
 * 
 * @see ErreursLigneFichierImport
 */
public class ListenerCollecteErreursValidation extends ExecutionContextPromotionListener implements ItemWriteListener<Object> {
    /**
     * La clé pour vérifier si des erreurs ont été collectées dans le contexte d'exécution.
     */
    protected static final String CLE_DETECTION_ERREURS = "CLE_DETECTION_ERREURS";

    /**
     * L'exécution de l'étape.
     *
     * @return l'exécution de l'étape
     */
    @Getter(AccessLevel.PROTECTED)
    private StepExecution stepExecution;

    /**
     * Constructeur.
     * 
     */
    public ListenerCollecteErreursValidation() {
        super();
        // Permet le passage de la collecte du nombre d'erreurs entre différentes étapes de validation
        setKeys(new String[] { CLE_DETECTION_ERREURS });
    }

    /**
     * Sauvegarde de l'exécution de l'étape pour le rendre disponible lors des traitements.
     * 
     * @param stepExecution
     *            l'exécution de l'étape
     */
    @BeforeStep
    public void saveStepExecution(StepExecution stepExecution) {
        this.stepExecution = stepExecution;
    }

    @Override
    public void afterWrite(List<? extends Object> items) {
        ExecutionContext executionContext = getStepExecution().getExecutionContext();
        boolean erreurBloquanteDejaDetectee = BooleanUtils.isTrue((Boolean) executionContext.get(CLE_DETECTION_ERREURS));
        if (!erreurBloquanteDejaDetectee) {
            // WARNING : ceci est un entonnoir, mais pas encore trouvé de façon plus performante pour réaliser le traitement
            for (Object item : items) {
                if (item instanceof EntiteImportableBatch && !((EntiteImportableBatch) item).isImportValide()) {
                    // Mise à jour du flag d'erreur pour l'ensemble des étapes de validation
                    executionContext.put(CLE_DETECTION_ERREURS, true);
                    marqueJobEnErreur(getStepExecution().getJobExecution().getExecutionContext());
                    break;
                }
            }
        }
    }

    @Override
    public void onWriteError(Exception exception, List<? extends Object> items) {
        // Rien
    }

    @Override
    public void beforeWrite(List<? extends Object> items) {
        // Rien
    }

    /**
     * Ajoute le flag de présence d'erreur au jobContext.
     * 
     * @param jobContext
     *            Le contexte de job à flagger.
     */
    private void marqueJobEnErreur(ExecutionContext jobContext) {
        if (!jobContext.containsKey(CLE_DETECTION_ERREURS)) {
            jobContext.put(CLE_DETECTION_ERREURS, true);
        }
    }

}
