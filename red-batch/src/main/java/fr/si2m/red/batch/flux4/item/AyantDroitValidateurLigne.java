package fr.si2m.red.batch.flux4.item;

import lombok.Setter;

import org.apache.commons.lang3.StringUtils;

import fr.si2m.red.DateRedac;
import fr.si2m.red.batch.RedacMessages;
import fr.si2m.red.batch.moteur.erreur.ValidateurLigneAvecCollecte;
import fr.si2m.red.dsn.AffiliationRepository;
import fr.si2m.red.dsn.AyantDroit;
import fr.si2m.red.dsn.AyantDroitRepository;

/**
 * Validateur de ligne de paramétrage code libellé.
 * 
 * @author benitahy
 *
 */
public class AyantDroitValidateurLigne extends ValidateurLigneAvecCollecte<AyantDroit> {

    @Setter
    private AffiliationRepository affiliationRepository;

    @Setter
    private AyantDroitRepository ayantDroitRepository;

    @Override
    protected void valide(final AyantDroit ayantDroit) {

        // unicité
        valideChampAvecCollecte(!ayantDroitRepository.existeUnAyantDroit(ayantDroit.getIdAyantDroit()), ayantDroit, "ID", "La ligne ne respecte pas la règle d'unicité");

        // Champs obligatoires
        valideChampAvecCollecte(StringUtils.isNotBlank(ayantDroit.getIdAyantDroit()), ayantDroit, "ID_AYANT_DROIT", "Le champ ID_AYANT_DROIT obligatoire n'est pas renseigné");
        valideChampAvecCollecte(StringUtils.isNotBlank(ayantDroit.getIdAffiliation()), ayantDroit, "ID_AFFILIATION", "Le champ ID_AFFILIATION obligatoire n'est pas renseigné");

        // Longueur des champs
        valideTailleFixeChampAvecCollecte(ayantDroit.getIdAyantDroit(), 30, ayantDroit, "ID_AYANT_DROIT");
        valideTailleFixeChampAvecCollecte(ayantDroit.getIdAffiliation(), 30, ayantDroit, "ID_AFFILIATION");
        valideTailleFixeChampAvecCollecte(ayantDroit.getRegimeAlsaceMoselle(), 2, ayantDroit, "REGIME_ALSACE_MOSELLE");
        valideTailleFixeChampAvecCollecte(ayantDroit.getCodeOption(), 30, ayantDroit, "CODE_OPTION");
        valideTailleFixeChampAvecCollecte(ayantDroit.getType(), 2, ayantDroit, "TYPE");
        valideChampDateTailleFixeAvecCollecte(ayantDroit.getDateDebutRattachementOuvrantDroitAsText(), DateRedac.FORMAT_DATES_DDMMYYYY, ayantDroit, "DATE_DEB_RATTACHEMENT", false);
        valideChampDateTailleFixeAvecCollecte(ayantDroit.getDateNaissanceAsText(), DateRedac.FORMAT_DATES_DDMMYYYY, ayantDroit, "DATE_NAISSANCE", false);
        valideTailleFixeChampAvecCollecte(ayantDroit.getNomFamille(), 80, ayantDroit, "NOM_FAMILLE");
        valideTailleFixeChampAvecCollecte(ayantDroit.getNumeroInscriptionRepertoire(), 13, ayantDroit, "NIR");
        valideTailleFixeChampAvecCollecte(ayantDroit.getNirOuvrantDroitRegimeBaseMaladie(), 13, ayantDroit, "NIR_OUVRANT_DROIT");
        valideTailleFixeChampAvecCollecte(ayantDroit.getPrenom(), 80, ayantDroit, "PRENOM");
        valideTailleFixeChampAvecCollecte(ayantDroit.getCodeOrganismeAffiliationAssuranceMaladie(), 30, ayantDroit, "CODE_ORG_ASS_MALADIE");
        valideChampDateTailleFixeAvecCollecte(ayantDroit.getDateFinRattachementOuvrantDroitAsText(), DateRedac.FORMAT_DATES_DDMMYYYY, ayantDroit, "DATE_FIN_RATTACHEMENT", false);

        validationCorrespondanceReferentielle(ayantDroit);
    }

    /**
     * Validation des liens entre tables.
     * 
     * @param ayantDroit
     *            L'ayant droit dont l'Affiliation doit être validée.
     */
    private void validationCorrespondanceReferentielle(final AyantDroit ayantDroit) {
        if (StringUtils.isNotBlank(ayantDroit.getIdAffiliation())) {
            boolean existeUneAffiliation = affiliationRepository.existeUneAffiliation(ayantDroit.getIdAffiliation());
            valideChampAvecCollecte(existeUneAffiliation, ayantDroit, "IdAffiliation", RedacMessages.ERREUR_REFERENCE_FLUX4);
        }
    }
}
