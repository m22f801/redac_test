package fr.si2m.red.batch.moteur.item;

import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;

import fr.si2m.red.batch.moteur.EtapeCodeRetourModificateur;

/**
 * Tâche de terminaison d'un batch lorsqu'aucune erreur n'a été détectée.
 * 
 * @author poidij
 *
 */
public class TacheFinBatchSansErreur extends EtapeCodeRetourModificateur implements Tasklet {

    @Override
    public RepeatStatus execute(final StepContribution contribution, final ChunkContext chunkContext) throws Exception {
        return RepeatStatus.FINISHED;
    }

}
