package fr.si2m.red.batch.flux3.ligne;

import lombok.Data;
import fr.si2m.red.DateRedac;

/**
 * Description d'une ligne d'un fichier CONTRAT_ETABLISSEMENT à exporter.
 * 
 * @author nortaina
 *
 */
@Data
public class ContratEtablissement {
    /**
     * Code Mise à jour.
     * 
     * @param codeMiseAJour
     *            Code Mise à jour
     * @return Code Mise à jour
     */
    private String codeMiseAJour;

    /**
     * Identifiant de l’entreprise.
     * 
     * @param identifiantEntrepriseG3C
     *            Identifiant de l’entreprise
     * @return Identifiant de l’entreprise
     */
    private String identifiantEntrepriseG3C;

    /**
     * N° de séquence contrat G3C.
     * 
     * @param numSequenceContratG3C
     *            N° de séquence contrat G3C
     * @return N° de séquence contrat G3C
     */
    private String numSequenceContratG3C;

    /**
     * Identifiant technique de l’entreprise.
     * 
     * @param identifiantTechniqueEntreprise
     *            Identifiant de l’entreprise
     * @return Identifiant de l’entreprise
     */
    private String identifiantTechniqueEntreprise;

    /**
     * Date de début de validité des paramètres contrat.
     * 
     * @param dateDebutSituation
     *            Date de début de validité des paramètres contrat
     * @return Date de début de validité des paramètres contrat
     */
    private Integer dateDebutSituation;

    /**
     * Date de fin de validité des paramètres contrat.
     * 
     * @param dateFinSituation
     *            Date de fin de validité des paramètres contrat
     * @return Date de fin de validité des paramètres contrat
     */
    private Integer dateFinSituation;

    /**
     * Identifiant technique du contrat.
     * 
     * @param identifiantTechniqueContrat
     *            Identifiant du contrat
     * @return Identifiant du contrat
     */
    private String identifiantTechniqueContrat;

    /**
     * Date de début de situation formatée.
     * 
     * @return Date de début de situation formatée
     */
    public String getDateDebutSituationFormatee() {
        return DateRedac.formate(DateRedac.EXPORT_FORMAT_PAR_DEFAUT, dateDebutSituation);
    }

    /**
     * Date de fin de situation formatée.
     * 
     * @return Date de fin de situation formatée
     */
    public String getDateFinSituationFormatee() {
        return DateRedac.formate(DateRedac.EXPORT_FORMAT_PAR_DEFAUT, dateFinSituation);
    }
}
