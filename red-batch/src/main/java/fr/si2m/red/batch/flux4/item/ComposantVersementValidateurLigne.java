package fr.si2m.red.batch.flux4.item;

import org.apache.commons.lang3.StringUtils;

import fr.si2m.red.batch.RedacMessages;
import fr.si2m.red.batch.moteur.erreur.ValidateurLigneAvecCollecte;
import fr.si2m.red.dsn.ComposantVersement;
import fr.si2m.red.dsn.ComposantVersementRepository;
import fr.si2m.red.dsn.VersementRepository;
import lombok.Setter;

/**
 * Validateur de ligne de paramétrage code libellé.
 * 
 * @author benitahy
 *
 */
public class ComposantVersementValidateurLigne extends ValidateurLigneAvecCollecte<ComposantVersement> {

    @Setter
    private VersementRepository versementRepository;

    @Setter
    private ComposantVersementRepository composantVersementRepository;

    @Override
    protected void valide(final ComposantVersement composantVersement) {

        // unicité
        valideChampAvecCollecte(!composantVersementRepository.existeUnComposantVersement(composantVersement.getIdComposantVersement()),
                composantVersement, "ID", "La ligne ne respecte pas la règle d'unicité");

        // Champs obligatoires
        valideChampAvecCollecte(StringUtils.isNotBlank(composantVersement.getIdComposantVersement()), composantVersement, "ID_COMPO_VERST",
                "Le champ ID_COMPO_VERST obligatoire n'est pas renseigné");
        valideChampAvecCollecte(StringUtils.isNotBlank(composantVersement.getIdVersement()), composantVersement, "ID_VERSEMENT",
                "Le champ ID_VERSEMENT obligatoire n'est pas renseigné");

        // Longueur des champs
        valideTailleFixeChampAvecCollecte(composantVersement.getIdComposantVersement(), 30, composantVersement, "ID_COMPO_VERST");
        valideTailleFixeChampAvecCollecte(composantVersement.getIdVersement(), 30, composantVersement, "ID_VERSEMENT");
        valideChampNumeriqueDecimaleTailleFixeAvecCollecte(composantVersement.getMontantVerseAsText(), 16, 2, composantVersement, "MONTANT_VERSE");
        valideTailleFixeChampAvecCollecte(composantVersement.getTypePopulation(), 30, composantVersement, "TYPE_POPULATION");
        valideTailleFixeChampAvecCollecte(composantVersement.getCodeAffectation(), 30, composantVersement, "CODE_AFFECTATION");
        valideTailleFixeChampAvecCollecte(composantVersement.getPeriodeAffectation(), 7, composantVersement, "PERIODE_AFFECTATION");
        valideTailleFixeChampAvecCollecte(composantVersement.getCodeIdentifiantSousFonds(), 50, composantVersement, "CODE_ID_SOUSFONDS");

        validationCorrespondanceReferentielle(composantVersement);

        // Calcul des champs dates de début / fin de période d'affectation : supprimé dans le cadre du Mantis 0002099 (batch 402)
    }

    /**
     * Validation des liens entre tables.
     * 
     * @param composantVersement
     *            Le cotisationEtablissement dont l'Adhésion doit être validée.
     */
    private void validationCorrespondanceReferentielle(final ComposantVersement composantVersement) {
        if (StringUtils.isNotBlank(composantVersement.getIdVersement())) {
            boolean existeUnVersement = versementRepository.existeUnVersement(composantVersement.getIdVersement());
            valideChampAvecCollecte(existeUnVersement, composantVersement, "IdVersement", RedacMessages.ERREUR_REFERENCE_FLUX4);
        }
    }

}
