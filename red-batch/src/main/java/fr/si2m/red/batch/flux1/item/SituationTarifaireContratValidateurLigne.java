package fr.si2m.red.batch.flux1.item;

import lombok.Setter;
import fr.si2m.red.batch.moteur.erreur.ValidateurLigneAvecCollecte;
import fr.si2m.red.contrat.Contrat;
import fr.si2m.red.contrat.ContratRepository;
import fr.si2m.red.contrat.TarifRepository;
import fr.si2m.red.parametrage.ParamEtatContratRepository;

/**
 * Validateur de ligne contrat.
 * 
 * @author nortaina
 *
 */
public class SituationTarifaireContratValidateurLigne extends ValidateurLigneAvecCollecte<Contrat> {

    /**
     * Référentiel des données tarifs temporaires pour validation des données contrats.
     * 
     * @param tempTarifRepository
     *            le référentiel des données tarifs temporaires pour validation des données contrats
     */
    @Setter
    private ContratRepository contratRepository;
    @Setter
    private TarifRepository tarifRepository;
    @Setter
    private ParamEtatContratRepository paramEtatContratRepository;

    @Override
    protected void valide(final Contrat contrat) {

        // F01_RG_BC09

        // On ne teste les date de début que si le contrat est actif
        if (paramEtatContratRepository.verifieEtatContratActif(contrat.getEtatContrat())) {
            Integer dateDebutMinContrat = contratRepository.getDateDebutMinimumContratTemporaire(contrat.getNumContrat());
            Integer dateDebutMinTarif = tarifRepository.getDateDebutMinimumTarifContratTemporaire(contrat.getNumContrat());

            if (dateDebutMinTarif == null) {
                valideChampAvecCollecte(false, contrat, "Situation Tarifaire", "le contrat n° " + contrat.getNumContrat()
                        + " n'a pas de situation tarifaire pour toutes ses situations (" + dateDebutMinContrat + ")");
            } else {
                valideChampAvecCollecte(dateDebutMinContrat >= dateDebutMinTarif, contrat, "Situation Tarifaire", "le contrat n° " + contrat.getNumContrat()
                        + " n'a pas de situation tarifaire (" + dateDebutMinTarif + ") pour toutes ses situations (" + dateDebutMinContrat + ")");
            }

        }
    }
}
