package fr.si2m.red.batch.moteur.item;

import fr.si2m.red.EntiteImportableBatch;

/**
 * Modificateur des champs d'une ligne validée en import.
 * 
 * @author poidij
 *
 * @param <I>
 *            le type d'entité à mettre à jour
 */
public abstract class ModificateurChampsLigneValide<I extends EntiteImportableBatch> extends ProcesseurLigne<I, I> {

    @Override
    public final I process(I item) throws Exception {
        if (item.isImportValide()) {
            modifieChamps(item);
        }
        return item;
    }

    /**
     * Met à jour les champs d'un élément valide.
     * 
     * @param item
     *            l'élément valide
     */
    protected abstract void modifieChamps(I item);

}
