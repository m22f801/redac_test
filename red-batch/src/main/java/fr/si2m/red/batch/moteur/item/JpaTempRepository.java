package fr.si2m.red.batch.moteur.item;

import javax.persistence.Query;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import fr.si2m.red.Entite;
import fr.si2m.red.core.repository.jpa.JpaRepository;

/**
 * Base pour la gestion de données batch temporaires dans des tables de travail.
 * 
 * @author nortaina
 *
 * @param <T>
 *            le type de données temporaires de travail
 */
public abstract class JpaTempRepository<T extends Entite> extends JpaRepository<T> {

    /**
     * Supprime toutes les données de travail.
     * 
     */
    @Transactional(propagation = Propagation.MANDATORY)
    public void nettoieEntites() {
        Query query = getEntityManager().createNativeQuery("DELETE FROM " + getNomTable());
        query.executeUpdate();
    }

}
