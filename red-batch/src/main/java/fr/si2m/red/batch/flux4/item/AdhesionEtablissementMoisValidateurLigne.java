package fr.si2m.red.batch.flux4.item;

import lombok.Setter;

import org.apache.commons.lang3.StringUtils;

import fr.si2m.red.DateRedac;
import fr.si2m.red.batch.moteur.erreur.ValidateurLigneAvecCollecte;
import fr.si2m.red.dsn.AdhesionEtablissementMois;
import fr.si2m.red.dsn.AdhesionEtablissementMoisRepository;

/**
 * Validateur de ligne de paramétrage code libellé.
 * 
 * @author poidij
 *
 */
public class AdhesionEtablissementMoisValidateurLigne extends ValidateurLigneAvecCollecte<AdhesionEtablissementMois> {

    @Setter
    private AdhesionEtablissementMoisRepository adhesionEtablissementMoisRepository;

    @Override
    protected void valide(final AdhesionEtablissementMois adhesionEtablissementMois) {

        // unicité
        valideChampAvecCollecte(!adhesionEtablissementMoisRepository.existeUneAdhesion(adhesionEtablissementMois.getIdAdhEtabMois()), adhesionEtablissementMois, "ID",
                "La ligne ne respecte pas la règle d'unicité");

        // Champs obligatoires sauf dates
        valideChampAvecCollecte(StringUtils.isNotBlank(adhesionEtablissementMois.getIdAdhEtabMois()), adhesionEtablissementMois, "ID_ADH_ETAB_MOIS",
                "Le champ ID_ADH_ETAB_MOIS obligatoire n'est pas renseigné");
        valideChampAvecCollecte(StringUtils.isNotBlank(adhesionEtablissementMois.getSirenEntreprise()), adhesionEtablissementMois, "SIREN_ENTREPRISE",
                "Le champ SIREN_ENTREPRISE obligatoire n'est pas renseigné");
        valideChampAvecCollecte(StringUtils.isNotBlank(adhesionEtablissementMois.getNicEtablissement()), adhesionEtablissementMois, "NIC_ETABLISSEMENT",
                "Le champ NIC_ETABLISSEMENT obligatoire n'est pas renseigné");
        valideChampAvecCollecte(StringUtils.isNotBlank(adhesionEtablissementMois.getReferenceContrat()), adhesionEtablissementMois, "REFERENCE_CONTRAT",
                "Le champ REFERENCE_CONTRAT obligatoire n'est pas renseigné");
        valideChampAvecCollecte(StringUtils.isNotBlank(adhesionEtablissementMois.getCodeOrganisme()), adhesionEtablissementMois, "CODE_ORGANISME",
                "Le champ CODE_ORGANISME obligatoire n'est pas renseigné");
        valideChampAvecCollecte(StringUtils.isNotBlank(adhesionEtablissementMois.getCodeEssaiReel()), adhesionEtablissementMois, "CODE_ESSAI_REEL",
                "Le champ CODE_ESSAI_REEL obligatoire n'est pas renseigné");

        // Longueur des champs

        valideTailleFixeChampAvecCollecte(adhesionEtablissementMois.getIdAdhEtabMois(), 30, adhesionEtablissementMois, "ID_ADH_ETAB_MOIS");
        valideTailleFixeChampAvecCollecte(adhesionEtablissementMois.getCodeEssaiReel(), 2, adhesionEtablissementMois, "CODE_ESSAI_REEL");
        valideTailleFixeChampAvecCollecte(adhesionEtablissementMois.getTypeEnvoi(), 2, adhesionEtablissementMois, "TYPE_ENVOI");
        valideTailleFixeChampAvecCollecte(adhesionEtablissementMois.getSirenEmetteur(), 9, adhesionEtablissementMois, "SIREN_EMETTEUR");
        valideTailleFixeChampAvecCollecte(adhesionEtablissementMois.getNicEmetteur(), 5, adhesionEtablissementMois, "NIC_EMETEUR");
        valideTailleFixeChampAvecCollecte(adhesionEtablissementMois.getNomEmetteur(), 60, adhesionEtablissementMois, "NOM_EMETTEUR");
        valideTailleFixeChampAvecCollecte(adhesionEtablissementMois.getVoieEmetteur(), 50, adhesionEtablissementMois, "VOIE_EMETTEUR");
        valideTailleFixeChampAvecCollecte(adhesionEtablissementMois.getCodePostalEmetteur(), 5, adhesionEtablissementMois, "CODE_POSTAL_EMETTEUR");
        valideTailleFixeChampAvecCollecte(adhesionEtablissementMois.getLocaliteEmetteur(), 50, adhesionEtablissementMois, "LOCALITE_EMETTEUR");
        valideTailleFixeChampAvecCollecte(adhesionEtablissementMois.getPaysEmetteur(), 2, adhesionEtablissementMois, "PAYS_EMETTEUR");
        valideTailleFixeChampAvecCollecte(adhesionEtablissementMois.getCodeDistributionEmetteur(), 50, adhesionEtablissementMois, "CODE_DISTRIB_EMETTEUR");
        valideTailleFixeChampAvecCollecte(adhesionEtablissementMois.getComplementConstructionEmetteur(), 50, adhesionEtablissementMois, "COMPLEMENT_CONSTRUCTION_EMETTEUR");
        valideTailleFixeChampAvecCollecte(adhesionEtablissementMois.getComplementVoieEmetteur(), 50, adhesionEtablissementMois, "COMPLEMENT_VOIE_EMETTEUR");
        valideTailleFixeChampAvecCollecte(adhesionEtablissementMois.getCiviliteContact(), 2, adhesionEtablissementMois, "CIVILITE_CONTACT");
        valideTailleFixeChampAvecCollecte(adhesionEtablissementMois.getNomContact(), 80, adhesionEtablissementMois, "NOM_CONTACT");
        valideTailleFixeChampAvecCollecte(adhesionEtablissementMois.getMailContact(), 100, adhesionEtablissementMois, "MAIL_CONTACT");
        valideTailleFixeChampAvecCollecte(adhesionEtablissementMois.getTelContact(), 20, adhesionEtablissementMois, "TEL_CONTACT");
        valideTailleFixeChampAvecCollecte(adhesionEtablissementMois.getFaxContact(), 20, adhesionEtablissementMois, "FAX_CONTACT");
        valideTailleFixeChampAvecCollecte(adhesionEtablissementMois.getTypeDeclaration(), 2, adhesionEtablissementMois, "TYPE_DECLARATION");
        valideChampLongTailleFixeAvecCollecte(adhesionEtablissementMois.getNumeroOrdreDeclarationAsText(), 15, adhesionEtablissementMois, "NUMERO_ORDRE_DECLARATION");
        valideChampDateTailleFixeAvecCollecte(adhesionEtablissementMois.getMoisDeclareAsText(), DateRedac.FORMAT_DATES_DDMMYYYY, adhesionEtablissementMois, "MOIS_DECLARE", true);
        valideChampDateTailleFixeAvecCollecte(adhesionEtablissementMois.getMoisRattachementAsText(), DateRedac.FORMAT_DATES_MOIS, adhesionEtablissementMois, "MOIS_RATTACHEMENT",
                true);
        valideChampDateTailleFixeAvecCollecte(adhesionEtablissementMois.getDateFichierAsText(), DateRedac.FORMAT_DATES_DDMMYYYY, adhesionEtablissementMois, "DATE_FICHIER", false);
        valideTailleFixeChampAvecCollecte(adhesionEtablissementMois.getChampDeclaration(), 2, adhesionEtablissementMois, "CHAMP_DECLARATION");
        valideTailleFixeChampAvecCollecte(adhesionEtablissementMois.getIdentifiantMetier(), 15, adhesionEtablissementMois, "IDENTIFIANT_METIER");
        valideTailleFixeChampAvecCollecte(adhesionEtablissementMois.getDeviseDeclaration(), 2, adhesionEtablissementMois, "DEVISE_DECLARATION");
        valideTailleFixeChampAvecCollecte(adhesionEtablissementMois.getSirenEntreprise(), 9, adhesionEtablissementMois, "SIREN_ENTREPRISE");
        valideTailleFixeChampAvecCollecte(adhesionEtablissementMois.getNicEntreprise(), 5, adhesionEtablissementMois, "NIC_ENTREPRISE");
        valideTailleFixeChampAvecCollecte(adhesionEtablissementMois.getCodeAPEN(), 5, adhesionEtablissementMois, "CODE_APEN");
        valideTailleFixeChampAvecCollecte(adhesionEtablissementMois.getVoieEntreprise(), 50, adhesionEtablissementMois, "VOIE_ENTREPRISE");
        valideTailleFixeChampAvecCollecte(adhesionEtablissementMois.getCodePostalEntreprise(), 5, adhesionEtablissementMois, "CODE_POSTAL_ENTREPRISE");
        valideTailleFixeChampAvecCollecte(adhesionEtablissementMois.getLocaliteEntreprise(), 50, adhesionEtablissementMois, "LOCALITE_ENTREPRISE");
        valideTailleFixeChampAvecCollecte(adhesionEtablissementMois.getComplementConstructionEntreprise(), 50, adhesionEtablissementMois, "COMPLEMENT_CONSTRUCTION_ENTREPRISE");
        valideTailleFixeChampAvecCollecte(adhesionEtablissementMois.getComplementVoieEntreprise(), 50, adhesionEtablissementMois, "COMPLEMENT_VOIE_ENTREPRISE");
        valideChampIntegerTailleFixeAvecCollecte(adhesionEtablissementMois.getEffectifMoyenAsText(), 7, adhesionEtablissementMois, "EFFECTIF_MOYEN");
        valideTailleFixeChampAvecCollecte(adhesionEtablissementMois.getPaysEntreprise(), 2, adhesionEtablissementMois, "PAYS_ENTREPRISE");
        valideTailleFixeChampAvecCollecte(adhesionEtablissementMois.getCodeDistributionEntreprise(), 50, adhesionEtablissementMois, "CODE_DISTRIB_ENTREPRISE");
        valideTailleFixeChampAvecCollecte(adhesionEtablissementMois.getImplantationEntreprise(), 2, adhesionEtablissementMois, "IMPLANTATION_ENTREPRISE");
        valideTailleFixeChampAvecCollecte(adhesionEtablissementMois.getRaisonSociale(), 80, adhesionEtablissementMois, "RAISON_SOCIALE");
        valideTailleFixeChampAvecCollecte(adhesionEtablissementMois.getNicEtablissement(), 5, adhesionEtablissementMois, "NIC_ETABLISSEMENT");
        valideTailleFixeChampAvecCollecte(adhesionEtablissementMois.getCodeAPET(), 5, adhesionEtablissementMois, "CODE_APET");
        valideTailleFixeChampAvecCollecte(adhesionEtablissementMois.getVoieEtablissement(), 50, adhesionEtablissementMois, "VOIE_ETABLISSEMENT");
        valideTailleFixeChampAvecCollecte(adhesionEtablissementMois.getCodePostalEtablissement(), 5, adhesionEtablissementMois, "CODE_POSTAL_ETABLISSEMENT");
        valideTailleFixeChampAvecCollecte(adhesionEtablissementMois.getLocaliteEtablissement(), 50, adhesionEtablissementMois, "LOCALITE_ETABLISSEMENT");
        valideTailleFixeChampAvecCollecte(adhesionEtablissementMois.getComplementConstructionEtablissement(), 50, adhesionEtablissementMois,
                "COMPLEMENT_CONSTRUCTION_ETABLISSEMENT");
        valideTailleFixeChampAvecCollecte(adhesionEtablissementMois.getComplementVoieEtablissement(), 50, adhesionEtablissementMois, "COMPLEMENT_VOIE_ETABLISSEMENT");
        valideChampIntegerTailleFixeAvecCollecte(adhesionEtablissementMois.getEffectifFinPeriodeAsText(), 6, adhesionEtablissementMois, "EFFECTIF_FIN_PERIODE");
        valideTailleFixeChampAvecCollecte(adhesionEtablissementMois.getPaysEtablissement(), 2, adhesionEtablissementMois, "PAYS_ETABLISSEMENT");
        valideTailleFixeChampAvecCollecte(adhesionEtablissementMois.getCodeDistributionEtablissement(), 50, adhesionEtablissementMois, "CODE_DISTRIB_ETABLISSEMENT");
        valideTailleFixeChampAvecCollecte(adhesionEtablissementMois.getNatureJuridique(), 2, adhesionEtablissementMois, "NATURE_JURIDIQUE");
        valideTailleFixeChampAvecCollecte(adhesionEtablissementMois.getCodeINSEE(), 5, adhesionEtablissementMois, "CODE_INSEE");
        valideChampDateTailleFixeAvecCollecte(adhesionEtablissementMois.getDateEcheanceAsText(), DateRedac.FORMAT_DATES, adhesionEtablissementMois, "DATE_ECHEANCE", true);
        valideTailleFixeChampAvecCollecte(adhesionEtablissementMois.getCategorieJuridique(), 4, adhesionEtablissementMois, "CATEGORIE_JURIDIQUE");
        valideTailleFixeChampAvecCollecte(adhesionEtablissementMois.getEnseigneEtablissement(), 80, adhesionEtablissementMois, "ENSEIGNE_ETABLISSEMENT");
        valideTailleFixeChampAvecCollecte(adhesionEtablissementMois.getReferenceContrat(), 30, adhesionEtablissementMois, "REFERENCE_CONTRAT");
        valideTailleFixeChampAvecCollecte(adhesionEtablissementMois.getCodeOrganisme(), 9, adhesionEtablissementMois, "CODE_ORGANISME");
        valideTailleFixeChampAvecCollecte(adhesionEtablissementMois.getCodeDelegataireGestion(), 6, adhesionEtablissementMois, "CODE_DELEG_COT");
        valideTailleFixeChampAvecCollecte(adhesionEtablissementMois.getPersonnelCouvert(), 2, adhesionEtablissementMois, "PERSONNEL_COUVERT");

    }

}
