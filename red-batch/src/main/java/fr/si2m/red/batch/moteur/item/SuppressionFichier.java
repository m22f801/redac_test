package fr.si2m.red.batch.moteur.item;

import java.util.Arrays;

import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.core.io.Resource;

import fr.si2m.red.RedacUnexpectedException;

/**
 * Classe de la tâche de suppression de fichiers.
 * 
 * @author poidij
 *
 */
public class SuppressionFichier implements Tasklet {

    /**
     * Liste des ressources à supprimer.
     * 
     */
    private Resource[] resources;

    /**
     * Met à jour la liste des ressources à supprimer.
     * 
     * @param resources
     *            la liste des ressources à supprimer
     */
    public void setResources(final Resource[] resources) {
        this.resources = Arrays.copyOf(resources, resources.length);
    }

    /**
     * Vérifie que les ressources à supprimer sont bien identifiées.
     */
    public void afterPropertiesSet() {
        if (resources == null) {
            throw new IllegalStateException("Les ressources à supprimer n'ont pas été spécifiées");
        }
    }

    @Override
    public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) throws Exception {
        for (Resource resource : resources) {
            boolean deleted = resource.getFile().delete();
            if (!deleted) {
                throw new RedacUnexpectedException("Impossible de supprimer le fichier " + resource.getFilename());
            }
        }
        return RepeatStatus.FINISHED;
    }
}
