package fr.si2m.red.batch.flux10.execution;

import fr.si2m.red.EntiteImportableBatch;
import fr.si2m.red.batch.moteur.execution.FacadeExecuteurBatch;
import fr.si2m.red.core.repository.EntiteImportableRepository;
import fr.si2m.red.parametrage.ParamGroupeGestion;
import fr.si2m.red.parametrage.ParamGroupeGestionRepository;

/**
 * Exécuteur du batch du flux 10 pour la table ParamGroupeGestion.
 * 
 * @author poidij
 *
 */
@FacadeExecuteurBatch(nomBatch = "flux10_GroupeGestion")
public class Flux10ParamGroupeGestionExecuteurBatch extends Flux10ExecuteurBatch {

    /**
     * Exécuteur de batch du flux 10 pour l'import de {@link ParamGroupeGestion}.
     * 
     * @param parametrageBatch
     *            les paramètres d'exécution du batch - deux paramètres sont attendus en entrée :
     *            <ol>
     *            <li>le chemin absolu vers le fichier à importer</li>
     *            <li>le chemin absolu vers le fichier de logs à éventuellement renseigner</li>
     *            </ol>
     */
    public Flux10ParamGroupeGestionExecuteurBatch(String[] parametrageBatch) {
        super(parametrageBatch);
    }

    @Override
    protected String getNomEntitesImportees() {
        return "ParamGroupeGestion";
    }

    @Override
    protected Class<? extends EntiteImportableRepository<? extends EntiteImportableBatch>> getReferentielEntitesImportees() {
        return ParamGroupeGestionRepository.class;
    }

}
