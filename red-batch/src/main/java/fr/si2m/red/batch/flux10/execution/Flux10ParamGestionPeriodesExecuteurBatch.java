package fr.si2m.red.batch.flux10.execution;

import fr.si2m.red.EntiteImportableBatch;
import fr.si2m.red.batch.moteur.execution.FacadeExecuteurBatch;
import fr.si2m.red.core.repository.EntiteImportableRepository;
import fr.si2m.red.parametrage.ParamGestionPeriodes;
import fr.si2m.red.parametrage.ParamGestionPeriodesRepository;

/**
 * Exécuteur du batch du flux 10 pour la table ParamGestionPeriodes.
 * 
 * @author delortj
 *
 */
@FacadeExecuteurBatch(nomBatch = "flux10_GestionPeriodes")
public class Flux10ParamGestionPeriodesExecuteurBatch extends Flux10ExecuteurBatch {

    /**
     * Exécuteur de batch du flux 10 pour l'import de {@link ParamGestionPeriodes}.
     * 
     * @param parametrageBatch
     *            les paramètres d'exécution du batch - deux paramètres sont attendus en entrée :
     *            <ol>
     *            <li>le chemin absolu vers le fichier à importer</li>
     *            <li>le chemin absolu vers le fichier de logs à éventuellement renseigner</li>
     *            </ol>
     */
    public Flux10ParamGestionPeriodesExecuteurBatch(String[] parametrageBatch) {
        super(parametrageBatch);

    }

    @Override
    protected String getNomEntitesImportees() {
        return "ParamGestionPeriodes";
    }

    @Override
    protected Class<? extends EntiteImportableRepository<? extends EntiteImportableBatch>> getReferentielEntitesImportees() {
        return ParamGestionPeriodesRepository.class;
    }

}
