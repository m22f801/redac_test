package fr.si2m.red.batch.moteur.execution;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Tag permettant d'ajouter un exécuteur batch au registre de la façade du moteur.
 * 
 * @author nortaina
 *
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface FacadeExecuteurBatch {
    /**
     * Le nom du batch ciblé par le job de l'exécuteur.
     * 
     */
    String nomBatch();
}
