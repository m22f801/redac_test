package fr.si2m.red.batch.flux4.item;

import lombok.Setter;

import org.apache.commons.lang3.StringUtils;

import fr.si2m.red.DateRedac;
import fr.si2m.red.batch.RedacMessages;
import fr.si2m.red.batch.moteur.erreur.ValidateurLigneAvecCollecte;
import fr.si2m.red.dsn.AdhesionEtablissementMoisRepository;
import fr.si2m.red.dsn.CotisationEtablissement;
import fr.si2m.red.dsn.CotisationEtablissementRepository;

/**
 * Validateur de ligne de paramétrage code libellé.
 * 
 * @author benitahy
 *
 */
public class CotisationEtablissementValidateurLigne extends ValidateurLigneAvecCollecte<CotisationEtablissement> {

    @Setter
    private AdhesionEtablissementMoisRepository adhesionEtablissementMoisRepository;

    @Setter
    private CotisationEtablissementRepository cotisationEtablissementRepository;

    @Override
    protected void valide(final CotisationEtablissement cotisationEtablissement) {

        // unicité
        valideChampAvecCollecte(!cotisationEtablissementRepository.existeUneCotisationEtablissement(cotisationEtablissement.getIdCotisationEtablissement()),
                cotisationEtablissement, "ID", "La ligne ne respecte pas la règle d'unicité");

        // Champs obligatoires
        valideChampAvecCollecte(StringUtils.isNotBlank(cotisationEtablissement.getIdCotisationEtablissement()), cotisationEtablissement, "ID_COTIS_ETAB",
                "Le champ ID_COTIS_ETAB obligatoire n'est pas renseigné");
        valideChampAvecCollecte(StringUtils.isNotBlank(cotisationEtablissement.getIdAdhEtabMois()), cotisationEtablissement, "ID_ADH_ETAB_MOIS",
                "Le champ ID_ADH_ETAB_MOIS obligatoire n'est pas renseigné");

        // Longueur des champs
        valideTailleFixeChampAvecCollecte(cotisationEtablissement.getIdCotisationEtablissement(), 30, cotisationEtablissement, "ID_COTIS_ETAB");
        valideTailleFixeChampAvecCollecte(cotisationEtablissement.getIdAdhEtabMois(), 30, cotisationEtablissement, "ID_ADH_ETAB_MOIS");
        valideChampNumeriqueDecimaleTailleFixeAvecCollecte(cotisationEtablissement.getValeurCotisationAsText(), 16, 2, cotisationEtablissement, "VALEUR_COTISATION");
        valideTailleFixeChampAvecCollecte(cotisationEtablissement.getCodeCotisation(), 3, cotisationEtablissement, "CODE_COTISATION");
        valideChampDateTailleFixeAvecCollecte(cotisationEtablissement.getDateDebutPeriodeRattachementAsText(), DateRedac.FORMAT_DATES_DDMMYYYY, cotisationEtablissement,
                "DATE_DEB_PERIODE_RATTACHEMENT", false);
        valideChampDateTailleFixeAvecCollecte(cotisationEtablissement.getDateFinPeriodeRattachementAsText(), DateRedac.FORMAT_DATES_DDMMYYYY, cotisationEtablissement,
                "DATE_FIN_PERIODE_RATTACHEMENT", false);

        validationCorrespondanceReferentielle(cotisationEtablissement);
    }

    /**
     * Validation des liens entre tables
     * 
     * @param cotisationEtablissement
     *            La cotisationEtablissement dont l'Adhésion doit être validée.
     */
    private void validationCorrespondanceReferentielle(final CotisationEtablissement cotisationEtablissement) {
        if (StringUtils.isNotBlank(cotisationEtablissement.getIdAdhEtabMois())) {
            boolean existeUneAdhesion = adhesionEtablissementMoisRepository.existeUneAdhesion(cotisationEtablissement.getIdAdhEtabMois());
            valideChampAvecCollecte(existeUneAdhesion, cotisationEtablissement, "IdAdhEtabMois", RedacMessages.ERREUR_REFERENCE_FLUX4);
        }
    }

}
