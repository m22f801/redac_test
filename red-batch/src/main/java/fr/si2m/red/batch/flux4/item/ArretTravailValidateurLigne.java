package fr.si2m.red.batch.flux4.item;

import lombok.Setter;

import org.apache.commons.lang3.StringUtils;

import fr.si2m.red.DateRedac;
import fr.si2m.red.batch.RedacMessages;
import fr.si2m.red.batch.moteur.erreur.ValidateurLigneAvecCollecte;
import fr.si2m.red.dsn.ArretTravail;
import fr.si2m.red.dsn.ArretTravailRepository;
import fr.si2m.red.dsn.ContratTravailRepository;

/**
 * Validateur de ligne de paramétrage code libellé.
 * 
 * @author benitahy
 *
 */
public class ArretTravailValidateurLigne extends ValidateurLigneAvecCollecte<ArretTravail> {

    @Setter
    private ContratTravailRepository contratTravailRepository;

    @Setter
    private ArretTravailRepository arretTravailRepository;

    @Override
    protected void valide(final ArretTravail arretTravail) {

        // unicité
        valideChampAvecCollecte(!arretTravailRepository.existeUnArretTravail(arretTravail.getIdArretTravail()), arretTravail, "ID", "La ligne ne respecte pas la règle d'unicité");

        // Champs obligatoires
        valideChampAvecCollecte(StringUtils.isNotBlank(arretTravail.getIdArretTravail()), arretTravail, "ID_ARRET_TRAVAIL",
                "Le champ ID_ARRET_TRAVAIL obligatoire n'est pas renseigné");
        valideChampAvecCollecte(StringUtils.isNotBlank(arretTravail.getIdContratTravail()), arretTravail, "ID_CONTRAT_TRAVAIL",
                "Le champ ID_CONTRAT_TRAVAIL obligatoire n'est pas renseigné");

        // Longueur des champs
        valideTailleFixeChampAvecCollecte(arretTravail.getIdArretTravail(), 30, arretTravail, "ID_ARRET_TRAVAIL");
        valideTailleFixeChampAvecCollecte(arretTravail.getIdContratTravail(), 30, arretTravail, "ID_CONTRAT_TRAVAIL");
        valideTailleFixeChampAvecCollecte(arretTravail.getMotifArret(), 2, arretTravail, "MOTIF_ARRET");
        valideChampDateTailleFixeAvecCollecte(arretTravail.getDateDernierJourAsText(), DateRedac.FORMAT_DATES_DDMMYYYY, arretTravail, "DERNIER_JOUR", false);
        valideChampDateTailleFixeAvecCollecte(arretTravail.getDateFinPrevisionnelleArretAsText(), DateRedac.FORMAT_DATES_DDMMYYYY, arretTravail, "DATE_FIN_PREVISIONNELLE", false);
        valideChampDateTailleFixeAvecCollecte(arretTravail.getDateRepriseAsText(), DateRedac.FORMAT_DATES_DDMMYYYY, arretTravail, "DATE_REPRISE", false);
        valideTailleFixeChampAvecCollecte(arretTravail.getMotifReprise(), 2, arretTravail, "MOTIF_REPRISE");

        validationCorrespondanceReferentielle(arretTravail);
    }

    /**
     * Validation des liens entre tables.
     * 
     * @param arretTravail
     *            L'arrêt de travail dont contrat de travail doit être validé.
     */
    private void validationCorrespondanceReferentielle(final ArretTravail arretTravail) {
        if (StringUtils.isNotBlank(arretTravail.getIdContratTravail())) {
            boolean existeUnContratTravail = contratTravailRepository.existeUnContratTravail(arretTravail.getIdContratTravail());
            valideChampAvecCollecte(existeUnContratTravail, arretTravail, "IdContratTravail", RedacMessages.ERREUR_REFERENCE_FLUX4);
        }
    }
}
