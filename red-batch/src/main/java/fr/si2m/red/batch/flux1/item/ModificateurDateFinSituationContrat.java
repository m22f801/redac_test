package fr.si2m.red.batch.flux1.item;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import lombok.Setter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.si2m.red.batch.flux10.execution.Flux10ExecuteurBatch;
import fr.si2m.red.batch.moteur.item.ModificateurChampsLigneValide;
import fr.si2m.red.contrat.Contrat;
import fr.si2m.red.contrat.ContratRepository;

/**
 * Modificateur des dates de fin de situation des contrats.
 * 
 * @author poidij
 *
 */
public class ModificateurDateFinSituationContrat extends ModificateurChampsLigneValide<Contrat> {

    private static final Logger LOGGER = LoggerFactory.getLogger(Flux10ExecuteurBatch.class);

    @Setter
    private ContratRepository contratRepository;

    @Override
    protected void modifieChamps(Contrat item) {
        // F01_RG_T01
        Integer dateDebutSituationContratSuivante = contratRepository.getDateDebutDeLaSituationContratSuivante(item.getNumContrat(),
                item.getDateDebutSituationLigne());

        if (dateDebutSituationContratSuivante != null && dateDebutSituationContratSuivante != 0) {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
            sdf.setLenient(false);
            String dateInString = dateDebutSituationContratSuivante.toString();
            Date date = null;
            try {
                date = sdf.parse(dateInString);
            } catch (ParseException e) {
                LOGGER.error("Erreur de parsing de la date dateDebutSituationContratSuivante : (" + dateDebutSituationContratSuivante + ")");
                return;
            }

            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            calendar.add(Calendar.DATE, -1);
            //
            Integer dateFinSituationContratCalculee = Integer.valueOf(sdf.format(calendar.getTime()));

            item.setDateFinSituationLigne(dateFinSituationContratCalculee);
        }

    }

}
