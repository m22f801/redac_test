package fr.si2m.red.batch.flux3.execution;

import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParameter;
import org.springframework.batch.core.JobParameters;
import org.springframework.context.support.GenericXmlApplicationContext;

import fr.si2m.red.DateRedac;
import fr.si2m.red.batch.flux3.item.JpaSituationContratTempRepository;
import fr.si2m.red.batch.flux3.item.JpaSituationTarifTempRepository;
import fr.si2m.red.batch.moteur.CodeRetour;
import fr.si2m.red.batch.moteur.execution.ExecuteurBatch;
import fr.si2m.red.batch.moteur.execution.FacadeExecuteurBatch;

/**
 * Exécuteur du batch du flux 3 pour l'export de l'image de référence.
 * 
 * @author delortjouvesf
 *
 */
@FacadeExecuteurBatch(nomBatch = "flux3")
public class Flux3ExecuteurBatch extends ExecuteurBatch {

    private static final Logger LOGGER = LoggerFactory.getLogger(Flux3ExecuteurBatch.class);

    /**
     * Ordre du paramètre de lancement "répertoire de sortie des fichiers".
     */
    public static final int ORDRE_PARAM_REPERTOIRE_SORTIE = 0;

    /**
     * Ordre du paramètre de lancement " SI SOURCE ".
     */
    public static final int ORDRE_PARAM_SI_SOURCE = 1;

    /**
     * Ordre du paramètre de lancement " CONTEXTE ".
     */
    public static final int ORDRE_PARAM_CONTEXTE = 2;

    /**
     * Ordre du paramètre de lancement "timestamp de l'export".
     */
    public static final int ORDRE_PARAM_TIMESTAMP_EXPORT = 3;

    /**
     * Ordre du paramètre de lancement "emplacement du fichier de log d'erreurs".
     */
    public static final int ORDRE_PARAM_EMPLACEMENT_FICHIER_ERREURS = 4;

    private final String emplacementRessourceRepertoireSortie;
    private final String timestampExport;
    private final String siSource;
    private final String contexte;

    /**
     * Constructeur d'exécuteur de batch.
     * 
     * @param parametrageBatch
     *            les paramètres d'exécution du batch - deux paramètres sont attendus en entrée :
     *            <ol>
     *            <li>le chemin absolu vers le fichier à importer</li>
     *            <li>le chemin absolu vers le fichier de logs à éventuellement renseigner</li>
     *            </ol>
     */
    public Flux3ExecuteurBatch(String[] parametrageBatch) {
        super(parametrageBatch);
        String cheminAbsoluFichierLogErreurs = parametrageBatch[ORDRE_PARAM_EMPLACEMENT_FICHIER_ERREURS];
        setCheminAbsoluFichierLogErreurs(cheminAbsoluFichierLogErreurs);

        String repertoireFichiers = parametrageBatch[ORDRE_PARAM_REPERTOIRE_SORTIE];
        this.siSource = parametrageBatch[ORDRE_PARAM_SI_SOURCE];
        this.contexte = parametrageBatch[ORDRE_PARAM_CONTEXTE];
        this.emplacementRessourceRepertoireSortie = tranformeEmplacementRessourceFichier(repertoireFichiers);

        this.timestampExport = parametrageBatch[ORDRE_PARAM_TIMESTAMP_EXPORT];

    }

    @Override
    protected String getEmplacementFichierConfigurationJob() {
        return "classpath:fr/si2m/red/batch/flux3/jobs.xml";
    }

    @Override
    protected String getIdJobCible() {
        return "jobFlux3";
    }

    @Override
    protected boolean lanceTraitementAvantJob(GenericXmlApplicationContext contexteExecutionJob) {
        // Validation de l'existence du répertoire de sortie
        if (!verifieExistencePhysiqueRessource(contexteExecutionJob, emplacementRessourceRepertoireSortie)) {
            LOGGER.error("Répertoire manquant pour démarrer le batch : {}", emplacementRessourceRepertoireSortie);
            return false;
        }

        videTablesDeTravail(contexteExecutionJob, Arrays.asList(JpaSituationContratTempRepository.class, JpaSituationTarifTempRepository.class));

        return true;
    }

    @Override
    protected JobParameters initParametrageJob(GenericXmlApplicationContext contexteExecutionJob) {

        Map<String, JobParameter> parametresJob = new HashMap<String, JobParameter>();
        // Configuration repertoire de sortie
        LOGGER.debug("Répertoire de sortie pris en compte : {}", emplacementRessourceRepertoireSortie);
        parametresJob.put("repertoireSortie", new JobParameter(emplacementRessourceRepertoireSortie));

        LOGGER.debug("SI Source pris en compte : {}", siSource);
        parametresJob.put("siSource", new JobParameter(siSource));

        LOGGER.debug("Contexte pris en compte : {}", contexte);
        parametresJob.put("contexte", new JobParameter(contexte));

        LOGGER.debug("Timestamp à inclure dans les noms de fichier exportés : {}", timestampExport);
        parametresJob.put("racineDateNomFichiers", new JobParameter(timestampExport));

        Integer dateJourRedac = DateRedac.convertitEnDateRedac(Calendar.getInstance().getTime());
        Integer datePalierExport = DateRedac.retirerALaDate(dateJourRedac, 1, "YEAR", "dateFinSituation");
        LOGGER.debug("Résolution de la date palier pour l'export des situations contrats : {}", datePalierExport);
        parametresJob.put("datePalierExport", new JobParameter(String.valueOf(datePalierExport)));

        return new JobParameters(parametresJob);
    }

    @Override
    protected void lanceTraitementFinJob(GenericXmlApplicationContext contexteExecutionJob, CodeRetour codeRetour, JobExecution executionJob) {

        // Logge le message de réussite du traitement batch dans le fichier de log
        if (codeRetour == CodeRetour.TERMINE) {
            traceTraitementOKDansFichierLog("Traitement du flux 3 réussi");
        } else if (codeRetour == CodeRetour.ERREUR_FONCTIONNELLE_NON_BLOQUANTE) {
            traceTraitementOKDansFichierLog("Traitement du flux 3 réussi, avec erreur(s) non bloquante(s)");
        }
    }

}
