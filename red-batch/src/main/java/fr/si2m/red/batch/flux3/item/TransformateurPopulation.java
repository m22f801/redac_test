package fr.si2m.red.batch.flux3.item;

import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.List;

import com.thoughtworks.xstream.core.util.Base64Encoder;

import fr.si2m.red.batch.flux3.ligne.Population;
import fr.si2m.red.batch.moteur.item.TransformateurDonnee;
import fr.si2m.red.contrat.SituationTarif;
import fr.si2m.red.contrat.Tarif;
import fr.si2m.red.contrat.TarifId;
import fr.si2m.red.contrat.TarifRepository;
import lombok.Setter;

/**
 * Mapper des entités Population à partir de la SituationContrat fournie.
 * 
 * cf F03_RG_S12
 * 
 * @author poidij
 *
 */
public class TransformateurPopulation extends TransformateurDonnee<SituationTarif, List<Population>> {

    @Setter
    private TarifRepository tarifRepository;

    @Override
    public List<Population> process(SituationTarif situationTarif) throws Exception {
        List<Population> populations = new ArrayList<Population>();

        TarifId tarifId = new TarifId();
        tarifId.setNumContrat(situationTarif.getNumContrat());
        tarifId.setNumCategorie(situationTarif.getNumCategorie());
        tarifId.setDateDebutSituationLigne(situationTarif.getDateDebutSituationLigne());
        // RG
        Tarif tarif = tarifRepository.get(tarifId);

        if (tarif == null) {
            return new ArrayList<Population>();
        }

        Population population = new Population();
        population.setCodeMiseAJour("R");
        population.setDateDebutApplication(tarif.getDateDebutSituationLigne());
        population.setDateFinApplication(tarif.getDateFinSituationLigne());
        population.setLibellePopulation(tarif.getLibelleCategorie());

        // note : un hash SHA-1 en Base64 a une longueur 28
        MessageDigest md = MessageDigest.getInstance("SHA-1");
        String numConcat = tarif.getNumContrat() + tarif.getNumCategorie();
        byte[] bytes = numConcat.getBytes("UTF-8");
        md.update(bytes);
        byte[] digest = md.digest();
        String hash = (new Base64Encoder()).encode(digest);
        population.setIdentifiantTechniquePopulation(hash);

        population.setIdentifiantTechniqueContrat(tarif.getNumContrat());
        population.setCodePopulation(tarif.getNumCategorie());
        populations.add(population);

        return populations;
    }

}
