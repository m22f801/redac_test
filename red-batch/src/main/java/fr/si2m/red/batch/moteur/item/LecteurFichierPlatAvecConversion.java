package fr.si2m.red.batch.moteur.item;

import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.annotation.Autowired;

import fr.si2m.red.EntiteImportableBatch;

/**
 * Lecteur de fichiers plats pour les batchs d'import REDAC définissant un convertisseur de ligne vers une entité cible.
 * 
 * @author nortaina
 *
 * @param <T>
 *            le type des lignes lues
 */
public class LecteurFichierPlatAvecConversion<T extends EntiteImportableBatch> extends LecteurFichierPlat<T> {

    /**
     * Constructeur pour un lecteur faisant de la conversion de ligne pour bien mapper l'entité ligne ciblée.
     * 
     * @param nomsChamps
     *            les noms ordonnés des champs d'une ligne
     * @param convertisseurLigne
     *            le convertisseur de ligne (champs vers entité de la ligne)
     * @param nomPrototypeLigne
     *            le nom du prototype à utiliser pour une entité ligne
     * @param beanFactory
     *            l'usine de bean pour la récupération du prototype de ligne
     */
    @Autowired
    public LecteurFichierPlatAvecConversion(String[] nomsChamps, FieldSetMapper<T> convertisseurLigne, String nomPrototypeLigne,
            BeanFactory beanFactory) {
        super(nomsChamps, nomPrototypeLigne, beanFactory);
        getMapperLigne().setFieldSetMapper(convertisseurLigne);
    }
}
