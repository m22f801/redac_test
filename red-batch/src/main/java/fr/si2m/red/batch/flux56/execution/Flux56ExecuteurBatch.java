package fr.si2m.red.batch.flux56.execution;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParameter;
import org.springframework.batch.core.JobParameters;
import org.springframework.context.support.GenericXmlApplicationContext;
import org.springframework.transaction.TransactionStatus;

import fr.si2m.red.DateRedac;
import fr.si2m.red.batch.moteur.CodeRetour;
import fr.si2m.red.batch.moteur.execution.ExecuteurBatch;
import fr.si2m.red.batch.moteur.execution.FacadeExecuteurBatch;
import fr.si2m.red.reconciliation.IndicateursDSNPeriodeRepository;

/**
 * Exécuteur du flux 5/6.
 * 
 * @author delortjouvesf
 *
 */
@FacadeExecuteurBatch(nomBatch = "flux56")
public class Flux56ExecuteurBatch extends ExecuteurBatch {

    private static final Logger LOGGER = LoggerFactory.getLogger(Flux56ExecuteurBatch.class);

    /**
     * Ordre du paramètre de lancement "répertoire de sortie".
     */

    public static final int ORDRE_PARAM_REPERTOIRE_SORTIE = 0;

    /**
     * Ordre du paramètre de lancement "date de calcul des indicateurs".
     */
    public static final int ORDRE_PARAM_DATE_CALCUL_INDICATEURS = 1;

    /**
     * Ordre du paramètre de lancement "Si Aval".
     */
    public static final int ORDRE_PARAM_SI_AVAL = 2;

    /**
     * Ordre du paramètre de lancement "timestamp de l'export".
     */
    public static final int ORDRE_PARAM_TIMESTAMP_EXPORT = 3;

    /**
     * Ordre du paramètre de lancement "emplacement du fichier de log d'erreurs".
     */
    public static final int ORDRE_PARAM_EMPLACEMENT_FICHIER_ERREURS = 4;

    private final String cheminRepertoireSortie;
    private final String emplacementRessourceRepertoireSortie;

    private final String dateCalculIndicateurs;

    private final String timestampExport;

    private final String siAval;

    /**
     * Constructeur d'exécuteur de batch.
     * 
     * @param parametrageBatch
     *            les paramètres d'exécution du batch - deux paramètres sont attendus en entrée :
     *            <ol>
     *            <li>une date au format yyyyMMdd</li>
     *            <li>le chemin absolu vers le fichier de logs à éventuellement renseigner</li>
     *            </ol>
     */
    public Flux56ExecuteurBatch(String[] parametrageBatch) {
        super(parametrageBatch);

        this.cheminRepertoireSortie = parametrageBatch[ORDRE_PARAM_REPERTOIRE_SORTIE];
        this.emplacementRessourceRepertoireSortie = tranformeEmplacementRessourceFichier(this.cheminRepertoireSortie);

        this.dateCalculIndicateurs = parametrageBatch[ORDRE_PARAM_DATE_CALCUL_INDICATEURS];
        this.siAval = parametrageBatch[ORDRE_PARAM_SI_AVAL];
        this.timestampExport = parametrageBatch[ORDRE_PARAM_TIMESTAMP_EXPORT];

        String cheminAbsoluFichierLogErreurs = parametrageBatch[ORDRE_PARAM_EMPLACEMENT_FICHIER_ERREURS];
        setCheminAbsoluFichierLogErreurs(cheminAbsoluFichierLogErreurs);

    }

    @Override
    protected String getEmplacementFichierConfigurationJob() {
        return "classpath:fr/si2m/red/batch/flux56/jobs.xml";
    }

    @Override
    protected String getIdJobCible() {
        return "jobFlux56";
    }

    @Override
    protected boolean lanceTraitementAvantJob(GenericXmlApplicationContext contexteExecutionJob) {
        // Validation de l'existence du répertoire de sortie
        if (!verifieExistencePhysiqueRessource(contexteExecutionJob, this.emplacementRessourceRepertoireSortie)) {
            LOGGER.error("Répertoire manquant pour démarrer le batch : {}", this.cheminRepertoireSortie);
            return false;
        }

        // Validation de la date
        if (!DateRedac.verifieDateRedac(this.dateCalculIndicateurs)) {
            LOGGER.error("Date de calcul invalide : {}", this.dateCalculIndicateurs);
            return false;
        }

        // Nettoyage de l'espace de travail
        TransactionStatus txStatus = commenceTransaction(contexteExecutionJob);
        try {
            IndicateursDSNPeriodeRepository indicateursDSNPeriodeRepository = contexteExecutionJob.getBean(IndicateursDSNPeriodeRepository.class);
            indicateursDSNPeriodeRepository.nettoieEntitesTemporaires();
        } finally {
            termineTransaction(contexteExecutionJob, txStatus);
        }

        return true;
    }

    @Override
    protected JobParameters initParametrageJob(GenericXmlApplicationContext contexteExecutionJob) {

        Map<String, JobParameter> parametresJob = new HashMap<String, JobParameter>();

        // Configuration du timestamp pour les exports
        LOGGER.debug("Timestamp à inclure dans les noms de fichier exportés : {}", timestampExport);
        parametresJob.put("timestamp", new JobParameter(timestampExport));

        // Configuration de la date prise en compte pour le calcul des indicateurs
        LOGGER.debug("Date prise en compte au format yyyyMMdd pour le calcul des indicateurs: {}", dateCalculIndicateurs);
        parametresJob.put("dateCalculIndicateurs", new JobParameter(dateCalculIndicateurs));

        // Configuration du SI Aval cible
        LOGGER.debug("Si Aval pris en compte {}", siAval);
        parametresJob.put("siAval", new JobParameter(siAval));

        // Configuration repertoire de sortie
        LOGGER.debug("Répertoire de sortie pris en compte : {}", emplacementRessourceRepertoireSortie);
        parametresJob.put("repertoireSortie", new JobParameter(emplacementRessourceRepertoireSortie));

        // Configuration de l'emplacement du fichier de log
        LOGGER.debug("Fichier erreurs pris en compte : {}", getCheminAbsoluFichierLogErreurs());
        parametresJob.put("fichierLogErreurs", new JobParameter(getCheminAbsoluFichierLogErreurs()));

        return new JobParameters(parametresJob);
    }

    @Override
    protected void lanceTraitementFinJob(GenericXmlApplicationContext contexteExecutionJob, CodeRetour codeRetour, JobExecution executionJob) {
        if (codeRetour == CodeRetour.ERREUR_TECHNIQUE_BLOQUANTE) {
            TransactionStatus txStatus = commenceTransaction(contexteExecutionJob);
            try {
                IndicateursDSNPeriodeRepository indicateursDSNPeriodeRepository = contexteExecutionJob.getBean(IndicateursDSNPeriodeRepository.class);
                indicateursDSNPeriodeRepository.nettoieEntitesTemporaires();
            } finally {
                termineTransaction(contexteExecutionJob, txStatus);
            }
        }

        // Logge le message de réussite du traitement batch dans le fichier de log
        if (codeRetour == CodeRetour.TERMINE) {
            traceTraitementOKDansFichierLog("Traitement du flux 56 réussi - SI_AVAL=" + this.siAval);
        } else if (codeRetour == CodeRetour.ERREUR_FONCTIONNELLE_NON_BLOQUANTE) {
            traceTraitementOKDansFichierLog("Traitement du flux 56 réussi, avec erreur(s) non bloquante(s) - SI_AVAL=" + this.siAval);
        }
    }

}
